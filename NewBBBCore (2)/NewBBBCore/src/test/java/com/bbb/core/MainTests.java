package com.bbb.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
@Configuration
@EnableScheduling
public class MainTests {
    @Test
    public void contextLoads() {
        assertEquals(TimeZone.getDefault().getID(), "UTC");
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
