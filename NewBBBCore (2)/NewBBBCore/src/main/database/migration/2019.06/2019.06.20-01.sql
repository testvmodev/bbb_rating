-- check duplicate inv comp type
SELECT dup_comps.*
FROM (
  SELECT
    ROW_NUMBER() OVER (PARTITION BY name ORDER BY id) AS rowNumber,
    *
  FROM inventory_comp_type
) AS dup_comps
WHERE dup_comps.rowNumber > 1

-- remove dup inv comp type
-- back up before this
WITH dup_comps AS (
  SELECT
  ROW_NUMBER() OVER (PARTITION BY name ORDER BY id) AS rowNumber,
  *
  FROM inventory_comp_type
)
DELETE dup_comps WHERE rowNumber > 1