-- first mapping

-- first do counting
SELECT COUNT(*)
FROM bike

-- export inventory from v1 market place (p2p listing)
-- no image yet
SELECT
  bike.id,
  bike.listing_id,
  listing.user_id AS listing_user_id,
  bike_make.id AS brand_id,
  bike_make.name AS brand_name,
  custom_bike_fields.make AS custom_brand_name,
  bike_model.id AS model_id,
  bike_model.name AS model_name,
  custom_bike_fields.model AS custom_model_name,
  bike.year,
  listing.user_id AS seller_user_id,
  bike.description,
  bike.serialNumber,
  bike.`condition`,
  bike.msrp,
  bike.size,
  bike.is_oversized_shipping,
  bike.snqID,
  bike.lightspeedID,
  bike.lightspeedItemShopID,
  bike.lightspeedSystemID,
  bike.inventory_id AS inv_name,
  bike.sfInventoryID,
  bike.salesForceId,
  listing.active_status AS listing_active_status,
  CASE listing.active_status
    WHEN -3 THEN 'Expired'
    WHEN -2 THEN 'Sold'
    WHEN -1 THEN 'Draft'
    WHEN 1 THEN 'Listed'
    WHEN 2 THEN 'Sale pending'
    WHEN 0 THEN 'De-Listed' -- put default any status for deleted listing
    ELSE NULL
  END AS listing_status_name,
  CASE listing.active_status
    WHEN 0 THEN 1
    ELSE 0
  END AS is_delete,
  listing.askingPrice AS listed_price,
  listing.offerPrice AS sold_price,
  bike_category.id AS category_id,
  bike_category.name AS category_name,
  bike_subcategory.id AS subcategory_id,
  bike_subcategory.name AS subcategory_name,
  category_from_sub.id AS cate_from_sub_id,
  category_from_sub.name AS cate_from_sub_name
FROM bbb.bike
-- from actived bicycle
LEFT JOIN bike_make ON bike.bike_make_id = bike_make.id
LEFT JOIN bike_model ON bike.bike_model_id = bike_model.id
-- end
-- JOIN bike_type ON bike.bike_type_id = bike_type.id --bike type in v2 is bike category in v1
-- from unactived bicycle
LEFT JOIN custom_bike_fields ON bike.customBikeFields_id = custom_bike_fields.id
-- end
LEFT JOIN bike_category ON bike.bike_category_id = bike_category.id
LEFT JOIN bike_subcategory ON bike.bike_subcategory_id = bike_subcategory.id
LEFT JOIN bike_category AS category_from_sub ON bike_subcategory.bike_category_id = category_from_sub.id
JOIN listing ON bike.listing_id = listing.id


----
CREATE TABLE bbbdevelop.dbo.v1_p2p_bike (
  id int NOT NULL,
  listing_id int,
  listing_user_id int,
  brand_id int,
  brand_name nvarchar(256),
  custom_brand_name nvarchar(256),
  model_id int,
  model_name nvarchar(256),
  custom_model_name nvarchar(256),
  [year] int,
  seller_user_id nvarchar(256),
  description text,
  serialNumber nvarchar(256),
  [condition] nvarchar(256),
  msrp float,
  [size] nvarchar(256),
  is_oversized_shipping bit,
  snqID nvarchar(256),
  lightspeedID nvarchar(256),
  lightspeedItemShopID nvarchar(256),
  lightspeedSystemID nvarchar(256),
  inv_name nvarchar(256),
  sfInventoryID nvarchar(256),
  salesForceId nvarchar(256),
  listing_active_status int,
  listing_status_name nvarchar(256),
  is_delete bit,
  listed_price float,
  sold_price float,
  category_id int,
  category_name nvarchar(256),
  subcategory_id int,
  subcategory_name nvarchar(256),
  cate_from_sub_id int,
  cate_from_sub_name nvarchar(256)
)



--- export inventory image
SELECT
  bikes_images.bike_id,
  bike_image.url AS url_original,
  bike_image.thumb AS thumb,
  bike_image.mq AS mq,
  bike_image.hq AS hq
FROM bikes_images
JOIN bike_image ON bike_image.id = bikes_images.bike_image_id

---
CREATE TABLE bbbdevelop.dbo.v1_p2p_bike_image (
  bike_id int NOT NULL,
  url_original nvarchar(256),
  thumb nvarchar(256),
  mq nvarchar(256),
  hq nvarchar(256)
)


-- v1 user detail
SELECT
  id,
  username,
  username_canonical,
  email,
  email_canonical,
  enabled,
  last_login,
  roles,
  first_name,
  last_name,
  phone_number,
  address,
  zip,
  city,
  state,
  partner_status,
  paypal_email,
  snq_id
FROM
  fos_user


CREATE TABLE dbo.v1_fos_user(
id int,
username nvarchar(256),
username_canonical nvarchar(256),
email nvarchar(256),
email_canonical nvarchar(256),
enabled bit,
last_login datetime,
roles nvarchar(256),
first_name nvarchar(256),
last_name nvarchar(256),
phone_number nvarchar(256),
address nvarchar(256),
zip nvarchar(256),
city nvarchar(256),
state nvarchar(256),
partner_status bit,
paypal_email nvarchar(256),
snq_id int
)


-- v1 listing
SELECT
  id,
  user_id,
  title,
  zip,
  listing_date,
  active_status,
  askingPrice,
  offerPrice,
  created_at,
  updated_at,
  recent_price_change,
  flat_shipping,
  shipping,
  expires_at,
  sold_date,
  does_not_expire
FROM listing


CREATE TABLE dbo.v1_p2p_listing(
id int,
user_id int,
title nvarchar(256),
zip nvarchar(256),
listing_date date,
active_status int,
askingPrice int,
offerPrice int,
created_at datetime,
updated_at datetime,
recent_price_change date,
flat_shipping float,
shipping nvarchar(256),
expires_at datetime,
sold_date date,
does_not_expire bit
)

-- bike detail to inv comp
SELECT
  bike.id AS bike_id,
  wheel_size.`size` AS wheel_size,
  wheel_size_id,
  custom_bike_fields.wheelSize AS custom_wheel_size,
  suspension.name AS suspension,
  suspension_id,
  custom_bike_fields.suspension AS custom_suspension,
  frame_size.`size` AS frame_size,
  frame_size_id,
  custom_bike_fields.frameSize AS custom_frame_size,
  frame_material.name AS frame_material,
  frame_material.id AS frame_material_id,
  custom_bike_fields.frameMaterial AS custom_frame_material,
  gender AS gender_id,
  CASE gender
    WHEN 1 THEN 'Women'
    WHEN 2 THEN 'Men'
    WHEN 3 THEN 'All' -- Unisex
    WHEN 4 THEN 'N/A'
    ELSE NULL
    END AS gender_name,
  front_shock,
  front_front_suspension,
  cassette,
  weight,
  color,
  frame_construction,
  fork_brand_model,
  fork_material,
  rear_shock,
  hubs,
  rims,
  tires,
  spoke_brand,
  spoke_nipples,
  component_group,
  brakeset,
  shift_levers,
  crankset,
  pedals,
  bottom_bracket,
  bb_shell_width,
  rear_cogs,
  chain,
  seat_post,
  saddle,
  handlebar,
  extensions,
  handlebar_stem,
  headset,
  front_derailleur,
  rear_derailleur,
  fork_front_suspension,
  wheels,
  brake_type,
  frame_tubing_material
FROM bike
LEFT JOIN custom_bike_fields ON custom_bike_fields.id = bike.customBikeFields_id
LEFT JOIN wheel_size ON wheel_size.id = bike.wheel_size_id
LEFT JOIN suspension ON suspension.id = bike.suspension_id
LEFT JOIN frame_size ON frame_size.id = bike.frame_size_id
LEFT JOIN frame_material ON frame_material.id = bike.frame_material_id


CREATE TABLE dbo.v1_p2p_bike_comp(
  bike_id int,
  wheel_size nvarchar(256),
  wheel_size_id int,
  custom_wheel_size nvarchar(256),
  suspension nvarchar(256),
  suspension_id int,
  custom_suspension nvarchar(256),
  frame_size nvarchar(256),
  frame_size_id int,
  custom_frame_size nvarchar(256),
  frame_material nvarchar(256),
  frame_material_id int,
  custom_frame_material nvarchar(256),
  gender_id int,
  gender_name nvarchar(256),
  front_shock nvarchar(256),
  front_front_suspension nvarchar(256),
  cassette nvarchar(256),
  weight nvarchar(256),
  color nvarchar(256),
  frame_construction nvarchar(256),
  fork_brand_model nvarchar(256),
  fork_material nvarchar(256),
  rear_shock nvarchar(256),
  hubs nvarchar(256),
  rims nvarchar(256),
  tires nvarchar(256),
  spoke_brand nvarchar(256),
  spoke_nipples nvarchar(256),
  component_group nvarchar(256),
  brakeset nvarchar(256),
  shift_levers nvarchar(256),
  crankset nvarchar(256),
  pedals nvarchar(256),
  bottom_bracket nvarchar(256),
  bb_shell_width nvarchar(256),
  rear_cogs nvarchar(256),
  [chain] nvarchar(256),
  seat_post nvarchar(256),
  saddle nvarchar(256),
  handlebar nvarchar(256),
  extensions nvarchar(256),
  handlebar_stem nvarchar(256),
  headset nvarchar(256),
  front_derailleur nvarchar(256),
  rear_derailleur nvarchar(256),
  fork_front_suspension nvarchar(256),
  wheels nvarchar(256),
  brake_type nvarchar(256),
  frame_tubing_material nvarchar(256)
)