SELECT
  bike.*
FROM bbb.bike
LEFT JOIN bike_make ON bike.bike_make_id = bike_make.id
LEFT JOIN bike_model ON bike.bike_model_id = bike_model.id
LEFT JOIN bike_category ON bike.bike_category_id = bike_category.id
LEFT JOIN bike_subcategory ON bike.bike_subcategory_id = bike_subcategory.id
LEFT JOIN bike_category AS category_from_sub ON bike_subcategory.bike_category_id = bike_category.id
JOIN custom_bike_fields ON bike.customBikeFields_id = custom_bike_fields.id
JOIN listing ON bike.listing_id = listing.id
WHERE
bike_category.id IS NOT NULL
AND bike_subcategory.id IS NOT NULL
AND bike_category.id != category_from_sub.id
AND listing.active_status > 0



SELECT COUNT(*) FROM bicycle_type WHERE name IN (
'Road',
'Cruiser',
'Kids',
'Folding',
'Tandem',
'E-Bike',
'Recumbent',
'Cyclocross',
'Fat Bike',
'Gravel',
'Triathlon/TT',
'Adventure',
'Track/Fixie/Singlespeed',
'Parts',
'Children''s',
'Fitness',
'Tires/Tubes',
'Accessories'
)
--> diff name Track/Fixie/Singlespeed

SELECT * FROM bicycle_type WHERE name NOT IN (
'Road','Cruiser','Kids','Folding','Tandem','E-Bike','Recumbent','Cyclocross','Fat Bike','Gravel','Triathlon/TT','Adventure','Track/Fixie/Singlespeed','Parts','Children''s','Fitness','Tires/Tubes','Accessories'
)

-- remap Track/Fixie/Singlespeed
UPDATE v1_p2p_bike
SET v1_p2p_bike.category_name = bicycle_type.name
FROM v1_p2p_bike
JOIN bicycle_type ON bicycle_type.name = 'Track / Fixie / Singlespeed'
WHERE v1_p2p_bike.category_name = 'Track/Fixie/Singlespeed'
-- 8

UPDATE v1_p2p_bike
SET v1_p2p_bike.cate_from_sub_name = bicycle_type.name
FROM v1_p2p_bike
JOIN bicycle_type ON bicycle_type.name = 'Track / Fixie / Singlespeed'
WHERE v1_p2p_bike.cate_from_sub_name = 'Track/Fixie/Singlespeed'
-- 8

UPDATE v1_p2p_bike
SET v1_p2p_bike.subcategory_name = bicycle_type.name
FROM v1_p2p_bike
JOIN bicycle_type ON bicycle_type.name = 'Track / Fixie / Singlespeed'
WHERE v1_p2p_bike.subcategory_name = 'Singlespeed/Fixie/Track'
-- 8

UPDATE v1_p2p_bike
SET v1_p2p_bike.subcategory_name = bicycle_type.name
FROM v1_p2p_bike
JOIN bicycle_type ON bicycle_type.name = 'Triathlon/TT'
WHERE v1_p2p_bike.subcategory_name = 'TT/Triathlon'
-- 116


-- map to bicycle type
SELECT
  v1_p2p_bike.id,
  ISNULL(ISNULL(type1.id, type2.id), type3.id),
  ISNULL(ISNULL(type1.name, type2.name), type3.name)
FROM v1_p2p_bike
  LEFT JOIN bicycle_type AS type1 ON v1_p2p_bike.category_name = type1.name
    AND v1_p2p_bike.category_name != ''
  LEFT JOIN bicycle_type AS type2 ON v1_p2p_bike.subcategory_name = type2.name
    AND v1_p2p_bike.subcategory_name != ''
  LEFT JOIN bicycle_type AS type3 ON v1_p2p_bike.cate_from_sub_name = type3.name
    AND v1_p2p_bike.cate_from_sub_name != ''