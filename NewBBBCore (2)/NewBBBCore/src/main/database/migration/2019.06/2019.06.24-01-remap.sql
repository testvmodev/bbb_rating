-- final mapping

-- temporary save v1 specific info in v2 for mapping
ALTER TABLE bicycle
ADD v1_bike_id int

ALTER TABLE inventory
ADD v1_user_id int, v1_bike_id int

--
ALTER TABLE bicycle_image
ADD v1_bike_id int

--
ALTER TABLE my_listing_draft
ADD v1_bike_id int


-- delete invalid inventory from v1 (maybe still in draft)
DELETE FROM v1_p2p_bike
WHERE year IS NULL


-- generate brand didnt exist
INSERT INTO bicycle_brand (name, is_approved, value_modifier)
SELECT missing_v1_brand.brand_name, 0, 0
FROM (
  SELECT * FROM (
    SELECT ISNULL(NULLIF(brand_name, ''), custom_brand_name) AS brand_name
    FROM v1_p2p_bike
    WHERE ISNULL(NULLIF(brand_name, ''), custom_brand_name) IS NOT NULL
      AND ISNULL(NULLIF(brand_name, ''), custom_brand_name) != ''
  ) AS v1_p2p_bike
  GROUP BY v1_p2p_bike.brand_name
) AS missing_v1_brand
LEFT JOIN bicycle_brand ON bicycle_brand.name = missing_v1_brand.brand_name
WHERE bicycle_brand.id IS NULL
OR bicycle_brand.is_delete = 1
-- 3 + 272


-- generate model didnt exist
INSERT INTO bicycle_model (name, brand_id, is_approved)
SELECT missing_v1_model.model_name, bicycle_brand.id, 0
FROM (
  SELECT * FROM (
    SELECT
      ISNULL(NULLIF(model_name, ''), custom_model_name) AS model_name,
      ISNULL(NULLIF(brand_name, ''), custom_brand_name) AS brand_name
    FROM v1_p2p_bike
  ) AS v1_p2p_bike
  WHERE brand_name IS NOT NULL
    AND brand_name != ''
    AND model_name IS NOT NULL
    AND model_name != ''
  GROUP BY v1_p2p_bike.model_name, v1_p2p_bike.brand_name
) AS missing_v1_model
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_brand
  WHERE bicycle_brand.name = missing_v1_model.brand_name
  AND bicycle_brand.is_delete = 0
  ORDER BY bicycle_brand.is_approved DESC
) AS bicycle_brand
OUTER APPLY (
  SELECT TOP 1 *
  FROM bicycle_model
  WHERE bicycle_model.name = missing_v1_model.model_name
  AND bicycle_model.is_delete = 0
  AND bicycle_model.brand_id = bicycle_brand.id
  ORDER BY bicycle_model.is_approved DESC
) AS bicycle_model
WHERE bicycle_model.id IS NULL OR bicycle_model.is_delete = 1
-- 183 + 517


-- generate year didnt exist
INSERT INTO bicycle_year(id, name, is_approved)
SELECT missing_v1_year.year, convert(nvarchar(10), missing_v1_year.year), 0
FROM (
  SELECT v1_p2p_bike.year
  FROM v1_p2p_bike
  GROUP BY v1_p2p_bike.year
) AS missing_v1_year
LEFT JOIN bicycle_year ON bicycle_year.id = missing_v1_year.year
WHERE missing_v1_year.year IS NOT NULL
AND bicycle_year.id IS NULL
-- 17



-- generate bicycle didnt exist
INSERT INTO bicycle (
  brand_id, model_id, type_id, year_id, retail_price, description, name, size_id, is_oversized, v1_bike_id, active
)
SELECT
  bicycle_brand.id,
  bicycle_model.id,
  ISNULL(ISNULL(type1.id, type2.id), type3.id),
  bicycle_year.id,
  v1_p2p_bike.msrp,
  v1_p2p_bike.description,
  bicycle_year.name + ' ' + bicycle_brand.name + ' ' + bicycle_model.name,
  bicycle_size.id,
  v1_p2p_bike.is_oversized_shipping,
  v1_p2p_bike.id,
  0
FROM v1_p2p_bike
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_brand
  WHERE
    ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name) IS NOT NULL
    AND ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name) != ''
    AND bicycle_brand.name = ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name)
    AND bicycle_brand.is_delete = 0
  ORDER BY bicycle_brand.is_approved DESC
) AS bicycle_brand
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_model
  WHERE
    ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name) IS NOT NULL
    AND ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name) != ''
    AND bicycle_model.name = ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name)
    AND bicycle_model.is_delete = 0
  ORDER BY bicycle_model.is_approved DESC
) AS bicycle_model
JOIN bicycle_year ON bicycle_year.id = v1_p2p_bike.year
LEFT JOIN bicycle ON bicycle.brand_id = bicycle_brand.id
  AND bicycle.model_id = bicycle_model.id
  AND bicycle.year_id = bicycle_year.id
LEFT JOIN bicycle_type AS type1 ON v1_p2p_bike.category_name = type1.name
  AND v1_p2p_bike.category_name != ''
LEFT JOIN bicycle_type AS type2 ON v1_p2p_bike.subcategory_name = type2.name
  AND v1_p2p_bike.subcategory_name != ''
LEFT JOIN bicycle_type AS type3 ON v1_p2p_bike.cate_from_sub_name = type3.name
  AND v1_p2p_bike.cate_from_sub_name != ''
LEFT JOIN bicycle_size ON bicycle_size.name = v1_p2p_bike.size
WHERE (bicycle.id IS NULL OR bicycle.is_delete = 1)
  AND v1_p2p_bike.listing_active_status != 0
  AND v1_p2p_bike.listing_active_status != -1
  AND ISNULL(ISNULL(type1.id, type2.id), type3.id) IS NOT NULL
-- 57 + 138 + 270


INSERT INTO bicycle_image (
  bicycle_id, uri
)
SELECT
  bicycle.id,
  v1_p2p_bike_image.url_original
FROM bicycle
JOIN v1_p2p_bike_image ON bicycle.v1_bike_id = v1_p2p_bike_image.bike_id
LEFT JOIN bicycle_image ON bicycle.id = bicycle_image.bicycle_id
  AND bicycle_image.uri = v1_p2p_bike_image.url_original
WHERE bicycle.v1_bike_id IS NOT NULL
  AND bicycle.image_id IS NULL
  AND v1_p2p_bike_image.url_original IS NOT NULL
  AND v1_p2p_bike_image.url_original != ''
  AND bicycle_image.id IS NULL
-- 249 + 732 (include duplicate) + 815

-- delete duplicate bicycle images
WITH duplicate AS (
 SELECT Id,
     row_number() OVER(PARTITION BY bicycle_id, uri ORDER BY id) AS rn
  FROM bicycle_image
)
-- SELECT duplicate.* FROM duplicate WHERE rn > 1
DELETE duplicate WHERE rn > 1
-- 5218


UPDATE bicycle
SET
  image_default = bicycle_image.uri,
  image_id = bicycle_image.id
FROM bicycle
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_image
  WHERE bicycle_image.bicycle_id = bicycle.id
) AS bicycle_image
WHERE bicycle.v1_bike_id IS NOT NULL
AND bicycle.image_id IS NULL
-- 114 + 268 + 522 (??? 522)


-- check incompatible inv condition
SELECT COUNT(*)
FROM v1_p2p_bike
WHERE v1_p2p_bike.condition NOT IN ('Excellent', 'Very Good', 'Good', 'Fair')
-- ('Excellent', 'Very Good', 'Good', 'Fair', 'Poor')
-- ('Excellent', 'Very Good', 'Good', 'Fair', 'Poor', 'Used')
AND (listing_active_status > 0
OR listing_active_status < -1) -- (Sold/Expired)



-- into inventory
INSERT INTO inventory (
  bicycle_id, bicycle_brand_name, bicycle_model_name, bicycle_year_name, bicycle_type_name,
  description, status, msrp_price, current_listed_price, stage, serial_number,
  type_id, seller_id, condition, discounted_price, initial_list_price, record_type,
  search_guide_recommendation, seller_is_bbb, v1_user_id, v1_bike_id
)
SELECT
  bicycle.id,
  bicycle_brand.name,
  bicycle_model.name,
  bicycle_year.name,
  ISNULL(ISNULL(type1.name, type2.name), type3.name),
  v1_p2p_bike.description,
  CASE v1_p2p_bike.listing_active_status
    WHEN -3 THEN 'Active' -- 'Expired'
    WHEN -2 THEN 'Sold'
    --   WHEN -1 THEN 'Draft'
    WHEN 1 THEN 'Active' -- 'Listed'
    WHEN 2 THEN 'Active' -- 'Sale pending'
    --   WHEN 0 THEN 'De-Listed' -- put default any status for deleted listing
    ELSE NULL
    END,
  v1_p2p_bike.msrp,
  v1_p2p_bike.listed_price,
  CASE v1_p2p_bike.listing_active_status
    WHEN -3 THEN 'Ready to be Listed' -- 'Expired'
    WHEN -2 THEN 'Sold'
    --   WHEN -1 THEN 'Draft'
    WHEN 1 THEN 'Listed'
    WHEN 2 THEN 'Sale Pending'
    --   WHEN 0 THEN 'De-Listed' -- put default any status for deleted listing
    ELSE NULL
    END,
  v1_p2p_bike.serialNumber,
  inventory_type.id,
  users_personal._id,
  v1_p2p_bike.condition,
  v1_p2p_bike.listed_price,
  v1_p2p_bike.listed_price,
  'bike',
  -- generate later by java code from service
  '',
  0,
  v1_p2p_bike.listing_user_id,
  v1_p2p_bike.id
FROM v1_p2p_bike
-- only listing from personal (p2p)
JOIN users_personal ON users_personal.v1_fos_id = v1_p2p_bike.listing_user_id
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_brand
  WHERE
    ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name) IS NOT NULL
    AND ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name) != ''
    AND bicycle_brand.name = ISNULL(NULLIF(v1_p2p_bike.brand_name, ''), v1_p2p_bike.custom_brand_name)
    AND bicycle_brand.is_delete = 0
  ORDER BY bicycle_brand.is_approved DESC
) AS bicycle_brand
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle_model
  WHERE
    ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name) IS NOT NULL
    AND ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name) != ''
    AND bicycle_model.name = ISNULL(NULLIF(v1_p2p_bike.model_name, ''), v1_p2p_bike.custom_model_name)
    AND bicycle_model.is_delete = 0
  ORDER BY bicycle_model.is_approved DESC
) AS bicycle_model
JOIN bicycle_year ON bicycle_year.id = v1_p2p_bike.year
CROSS APPLY (
  SELECT TOP 1 *
  FROM bicycle
  WHERE bicycle.brand_id = bicycle_brand.id
  AND bicycle.model_id = bicycle_model.id
  AND bicycle.year_id = bicycle_year.id
  AND bicycle.is_delete = 0
  ORDER BY bicycle.active
) AS bicycle
LEFT JOIN bicycle_type AS type1 ON v1_p2p_bike.category_name = type1.name
  AND v1_p2p_bike.category_name != ''
LEFT JOIN bicycle_type AS type2 ON v1_p2p_bike.subcategory_name = type2.name
  AND v1_p2p_bike.subcategory_name != ''
LEFT JOIN bicycle_type AS type3 ON v1_p2p_bike.cate_from_sub_name = type3.name
  AND v1_p2p_bike.cate_from_sub_name != ''
JOIN inventory_type ON inventory_type.name = 'PTP'
LEFT JOIN inventory ON inventory.v1_bike_id = v1_p2p_bike.id
WHERE
  inventory.id IS NULL
  AND v1_p2p_bike.listing_active_status != 0 -- not deleted
  AND v1_p2p_bike.listing_active_status != -1 -- not draft
  AND ISNULL(ISNULL(type1.name, type2.name), type3.name) IS NOT NULL
  AND v1_p2p_bike.condition IN ('Excellent', 'Very Good', 'Good', 'Fair')
-- ('Excellent', 'Very Good', 'Good', 'Fair', 'Poor')
-- ('Excellent', 'Very Good', 'Good', 'Fair', 'Poor', 'Used')
-- 82 + 44


-- somehow trigger doesnt generate name INV-*** . regenerate it
UPDATE inventory
SET
    name = 'INV-' + CAST(id AS nvarchar(10))
WHERE name IS NULL
AND v1_bike_id IS NOT NULL



INSERT INTO inventory_bicycle
SELECT inventory.id, inv_brand.id, inv_model.id, inv_year.id, bicycle_type.id, bicycle_size.id
FROM inventory
CROSS APPLY (
  SELECT TOP 1 * FROM bicycle_brand
  WHERE bicycle_brand.name = inventory.bicycle_brand_name
  ORDER BY bicycle_brand.is_approved DESC
) AS inv_brand
CROSS APPLY (
  SELECT TOP 1 * FROM bicycle_model
  WHERE bicycle_model.name = inventory.bicycle_model_name
  AND bicycle_model.brand_id = inv_brand.id
  ORDER BY bicycle_model.is_approved DESC
) AS inv_model
CROSS APPLY (
  SELECT TOP 1 * FROM bicycle_year
  WHERE bicycle_year.name = inventory.bicycle_year_name
) AS inv_year
LEFT JOIN bicycle_type ON inventory.bicycle_type_name = bicycle_type.name
LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name
LEFT JOIN inventory_bicycle ON inventory_bicycle.inventory_id = inventory.id
WHERE inventory_bicycle.inventory_id IS NULL
  AND inventory.v1_bike_id IS NOT NULL
-- 78 + 40


-- inv images
INSERT INTO inventory_image (inventory_id, image)
SELECT
  inventory.id,
  v1_p2p_bike_image.url_original
FROM inventory
JOIN v1_p2p_bike_image ON inventory.v1_bike_id = v1_p2p_bike_image.bike_id
LEFT JOIN inventory_image ON inventory.id = inventory_image.inventory_id
  AND inventory_image.image = v1_p2p_bike_image.url_original
WHERE
  inventory.v1_bike_id IS NOT NULL
  AND inventory.image_default_id IS NULL
  AND inventory_image.id IS NULL
-- 270 + 117

UPDATE inventory
SET
  image_default_id = inventory_image.id,
  image_default = inventory_image.image
FROM inventory
CROSS APPLY (
  SELECT TOP 1 *
  FROM inventory_image
  WHERE inventory_image.inventory_id = inventory.id
) AS inventory_image
WHERE inventory.v1_bike_id IS NOT NULL
  AND inventory.image_default_id IS NULL
-- 62 + 37


-- inventory_location_shipping
INSERT INTO inventory_location_shipping (
  inventory_id,
  is_insurance, flat_rate, is_allow_local_pickup, shipping_type,
  zip_code, country_code, country_name, state_name, state_code, city_name, address_line
  -- county latitude longitude will be generated by java code
)
SELECT
  inventory.id,
  CASE WHEN CHARINDEX('s:9:"INSURANCE"', shipping) > 0 THEN 1 ELSE NULL END,
  v1_p2p_listing.flat_shipping,
  CASE WHEN CHARINDEX('s:6:"PICKUP"', shipping) > 0 THEN 1 ELSE NULL END,
  CASE
    WHEN CHARINDEX('s:3:"BBB"', shipping) > 0 THEN 'BicycleBlueBook.com'
    WHEN CHARINDEX('s:4:"FLAT"', shipping) > 0 THEN 'Flat rate'
    ELSE NULL
    END,
  v1_fos_user.zip,
  -- no country info, assume v1 only have US user
  'US',
  'United States',
  v1_fos_user.state,
  v1_fos_user.state,
  v1_fos_user.city,
  v1_fos_user.address
FROM inventory
LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id
JOIN v1_p2p_bike ON v1_p2p_bike.id = inventory.v1_bike_id
JOIN v1_fos_user ON v1_fos_user.id = inventory.v1_user_id
JOIN v1_p2p_listing ON v1_p2p_listing.id = v1_p2p_bike.listing_id
WHERE inventory_location_shipping.inventory_id IS NULL
  AND inventory.is_delete = 0
-- 78 + 40

-- check missing
SELECT
  COUNT(*)
-- inventory.id
FROM inventory
LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id
WHERE inventory_location_shipping.inventory_id IS NULL
  AND inventory.v1_bike_id IS NOT NULL



-- market listing
INSERT INTO market_listing (
  inventory_id, status, time_listed, time_sold, is_delete,
  market_place_config_id, market_place_id, is_best_offer, paypal_email_seller,
  is_expirable, owner_id
)
SELECT
inventory.id,
v1_p2p_bike.listing_status_name,
CONVERT(DATETIME, v1_p2p_listing.listing_date),
CONVERT(DATETIME, v1_p2p_listing.sold_date),
0,
market.market_place_config_id,
market.market_place_id,
0,
v1_fos_user.paypal_email,
~v1_p2p_listing.does_not_expire,
users_personal._id
FROM inventory
LEFT JOIN market_listing ON inventory.id = market_listing.inventory_id
JOIN v1_p2p_bike ON v1_p2p_bike.id = inventory.v1_bike_id
JOIN v1_p2p_listing ON v1_p2p_bike.listing_id = v1_p2p_listing.id
JOIN v1_fos_user ON v1_fos_user.id = inventory.v1_user_id
JOIN users_personal ON users_personal.v1_fos_id = v1_fos_user.id
CROSS APPLY (
  SELECT TOP 1
  market_place.id AS market_place_id,
  market_place_config.id AS market_place_config_id
  FROM market_place
  JOIN market_place_config ON market_place.id = market_place_config.market_place_id
  WHERE market_place.is_delete = 0
  AND market_place_config.is_delete = 0
  ANd market_place.type = 'BBB'
) AS market
WHERE market_listing.id IS NULL
AND inventory.v1_bike_id IS NOT NULL

-- verify
SELECT
COUNT(market_listing.id)
FROM inventory
JOIN market_listing ON inventory.id = market_listing.inventory_id
WHERE inventory.v1_bike_id IS NOT NULL


-- inv comp
-- note: this will only work well if there arent any duplicate inventory_comp_type.name
INSERT INTO inventory_comp_detail
SELECT
  inventory.id,
  inventory_comp_type.id,
  inv_comp.value
  -- these below for testing before inserting
  --  bike_id,
  --  inv_comp.name,
  --  inv_comp.value,
  --  inventory_comp_type.id,
  --  inventory_comp_type.is_select,
  --  inventory.id
FROM (
  SELECT
    bike_id,
    CASE REPLACE(inv_comp.name, '_', ' ')
      -- manual fix some incompatible components name
      WHEN 'fork front suspension' THEN 'Fork/Front Suspension'
      WHEN 'gender name' THEN 'Gender'
      WHEN 'seat post' THEN 'Seatpost'
      WHEN 'fork brand model' THEN 'Fork Brand & Model'
      ELSE REPLACE(inv_comp.name, '_', ' ')
      END AS name,
    inv_comp.value
  FROM (
    SELECT
      bike_id,
      ISNULL(wheel_size, custom_wheel_size) AS wheel_size, ISNULL(suspension, custom_wheel_size) AS suspension,
      ISNULL(frame_size, custom_frame_size) AS frame_size, ISNULL(frame_material, custom_frame_material) AS frame_material,
      gender_name, front_shock,
      front_front_suspension, cassette, weight, color, frame_construction,
      fork_brand_model, fork_material, rear_shock, hubs, rims, tires, spoke_brand,
      spoke_nipples, component_group, brakeset, shift_levers, crankset, pedals,
      bottom_bracket, bb_shell_width, rear_cogs, chain, seat_post, saddle, handlebar,
      extensions, handlebar_stem, headset, front_derailleur, rear_derailleur,
      fork_front_suspension, wheels, brake_type, frame_tubing_material
    FROM v1_p2p_bike_comp
  ) AS v1_p2p_bike_comp
  UNPIVOT (
    value FOR name IN (
      wheel_size, suspension, frame_size, frame_material, gender_name, front_shock,
      front_front_suspension, cassette, weight, color, frame_construction,
      fork_brand_model, fork_material, rear_shock, hubs, rims, tires, spoke_brand,
      spoke_nipples, component_group, brakeset, shift_levers, crankset, pedals,
      bottom_bracket, bb_shell_width, rear_cogs, chain, seat_post, saddle, handlebar,
      extensions, handlebar_stem, headset, front_derailleur, rear_derailleur,
      fork_front_suspension, wheels, brake_type, frame_tubing_material
    )
  ) inv_comp
) AS inv_comp
JOIN inventory ON inventory.v1_bike_id = inv_comp.bike_id
JOIN inventory_comp_type ON inventory_comp_type.name = inv_comp.name
LEFT JOIN inventory_comp_detail ON inventory.id = inventory_comp_detail.inventory_id
  AND inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id
WHERE
  inv_comp.value IS NOT NULL
  AND inv_comp.value != ''
  AND inventory_comp_detail.inventory_id IS NULL
-- check remain incompatible components name
-- AND inventory_comp_type.id IS NULL
-- 1091 + 159


-- use bike size of v1 as frame size for v2
INSERT INTO inventory_comp_detail
SELECT
  inventory.id,
  inventory_comp_type.id,
  inventory_comp_type_select.value
FROM inventory
       JOIN v1_p2p_bike ON inventory.v1_bike_id = v1_p2p_bike.id
       JOIN inventory_comp_type ON inventory_comp_type.name = 'Frame Size'
       JOIN inventory_comp_type_select ON inventory_comp_type.id = inventory_comp_type_select.inventory_comp_type_id
    AND inventory_comp_type_select.value = v1_p2p_bike.size
       LEFT JOIN inventory_comp_detail ON inventory.id = inventory_comp_detail.inventory_id
    AND inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id
WHERE inventory.v1_bike_id IS NOT NULL
  AND inventory_comp_detail.inventory_id IS NULL



