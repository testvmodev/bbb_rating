ALTER TABLE dbo.inventory
ADD seller_is_bbb bit DEFAULT 0

ALTER TABLE dbo.inventory
ALTER COLUMN seller_is_bbb bit NOT NULL

UPDATE inventory
SET
  inventory.seller_is_bbb = 1
FROM inventory
JOIN inventory_type ON inventory_type.id = inventory.type_id
WHERE (inventory_type.name = 'Trade-in'
OR inventory.salesforce_id IS NOT NULL)
AND inventory.seller_is_bbb IS NULL

UPDATE inventory
SET
inventory.seller_is_bbb = 0 -- default
FROM inventory
JOIN inventory_type ON inventory_type.id = inventory.type_id
WHERE inventory.seller_is_bbb IS NULL