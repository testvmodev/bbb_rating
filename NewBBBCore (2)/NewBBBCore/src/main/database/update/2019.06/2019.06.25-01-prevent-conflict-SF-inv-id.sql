-- next inventory id will start from 600000
DBCC CHECKIDENT ('bbbdevelop.dbo.inventory', RESEED, 59999)

-- update trigger generate inv-*** only when null
ALTER TRIGGER [dbo].[inventory_created_name]
ON [dbo].[inventory]
WITH EXECUTE AS CALLER
FOR INSERT
AS
BEGIN
DECLARE @id INT
SELECT @id =id from INSERTED
UPDATE inventory SET name = 'INV-' + CAST(@id AS nvarchar(10)) WHERE inventory.id = @id
AND name IS NULL
END