package com.bbb.core.controller.inventory;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.inventory.ComponentInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ComponentInventoryController {
    @Autowired
    private ComponentInventoryService service;

    @GetMapping(value = Constants.URL_COMPONENT_INVENTORY_SEARCH)
    public ResponseEntity getInventoriesSearch() {
        return new ResponseEntity<>(service.getInventoriesSearch(), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_MODEL_BRAND)
    public ResponseEntity getAllBicycleModel(
                 @RequestParam(value = "brandIds") List<Long> brandIds) throws ExceptionResponse {
        return new ResponseEntity<>(service.getAllBicycleModel(brandIds), HttpStatus.OK);
    }

}
