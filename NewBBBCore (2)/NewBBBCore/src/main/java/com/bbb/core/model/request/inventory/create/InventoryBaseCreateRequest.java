package com.bbb.core.model.request.inventory.create;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.RecordTypeInventory;
import com.bbb.core.model.database.type.RefundReturnTypeInventory;
import com.bbb.core.model.response.ObjectError;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotNull;
import java.util.List;


public class InventoryBaseCreateRequest implements MessageResponses {
    @NotNull
    private Long bicycleId;
    private List<String> images;
    @NotNull
    private Float msrPrice;
    @NotNull
    private Float bbbValue;
    private Float overrideTradeInPrice;
    @NotNull
    private Float cogsPrice;
    private String serialNumber;
    private float flatPriceChange;
    private boolean isCustomQuote;
    private boolean isCreatePo;
    private String sellerId;
    private ConditionInventory condition;
    private Long typeId;
    private Boolean isPreList;
    private Boolean isCheckInOverride;
    private LocalDate dayHold;
    private Boolean isOverrideCreatePo;
    private Boolean isNonComplianceNoti;
    private RefundReturnTypeInventory refundReturnType;
    private Float priceRole;
    private LocalDate firstListedDate;
    private String masterMultiListing;
    private Boolean isReviseItem;
    private RecordTypeInventory recordType;
    private String note;
    private String site;
    private Long stockLocationId;
    private LocalDateTime dateReceived;
    private Float conditionScore;
    private Float privatePartyValue;
    private Float valueAdditionalComponent;

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Float getMsrPrice() {
        return msrPrice;
    }

    public void setMsrPrice(Float msrPrice) {
        this.msrPrice = msrPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public boolean isCustomQuote() {
        return isCustomQuote;
    }

    public void setCustomQuote(boolean customQuote) {
        isCustomQuote = customQuote;
    }

    public boolean isCreatePo() {
        return isCreatePo;
    }

    public void setCreatePo(boolean createPo) {
        isCreatePo = createPo;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Boolean getPreList() {
        return isPreList;
    }

    public void setPreList(Boolean preList) {
        isPreList = preList;
    }

    public Boolean getCheckInOverride() {
        return isCheckInOverride;
    }

    public void setCheckInOverride(Boolean checkInOverride) {
        isCheckInOverride = checkInOverride;
    }

    public LocalDate getDayHold() {
        return dayHold;
    }

    public void setDayHold(LocalDate dayHold) {
        this.dayHold = dayHold;
    }

    public Boolean getOverrideCreatePo() {
        return isOverrideCreatePo;
    }

    public void setOverrideCreatePo(Boolean overrideCreatePo) {
        isOverrideCreatePo = overrideCreatePo;
    }

    public Boolean getNonComplianceNoti() {
        return isNonComplianceNoti;
    }

    public void setNonComplianceNoti(Boolean nonComplianceNoti) {
        isNonComplianceNoti = nonComplianceNoti;
    }

    public RefundReturnTypeInventory getRefundReturnType() {
        return refundReturnType;
    }

    public void setRefundReturnType(RefundReturnTypeInventory refundReturnType) {
        this.refundReturnType = refundReturnType;
    }

    public Float getPriceRole() {
        return priceRole;
    }

    public void setPriceRole(Float priceRole) {
        this.priceRole = priceRole;
    }

    public LocalDate getFirstListedDate() {
        return firstListedDate;
    }

    public void setFirstListedDate(LocalDate firstListedDate) {
        this.firstListedDate = firstListedDate;
    }

    public String getMasterMultiListing() {
        return masterMultiListing;
    }

    public void setMasterMultiListing(String masterMultiListing) {
        this.masterMultiListing = masterMultiListing;
    }

    public Boolean getReviseItem() {
        return isReviseItem;
    }

    public void setReviseItem(Boolean reviseItem) {
        isReviseItem = reviseItem;
    }

    public RecordTypeInventory getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordTypeInventory recordType) {
        this.recordType = recordType;
    }


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getStockLocationId() {
        return stockLocationId;
    }

    public void setStockLocationId(Long stockLocationId) {
        this.stockLocationId = stockLocationId;
    }

    public LocalDateTime getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Float getConditionScore() {
        return conditionScore;
    }

    public void setConditionScore(Float conditionScore) {
        this.conditionScore = conditionScore;
    }

    public Float getPrivatePartyValue() {
        return privatePartyValue;
    }

    public void setPrivatePartyValue(Float privatePartyValue) {
        this.privatePartyValue = privatePartyValue;
    }

    public Float getValueAdditionalComponent() {
        return valueAdditionalComponent;
    }

    public void setValueAdditionalComponent(Float valueAdditionalComponent) {
        this.valueAdditionalComponent = valueAdditionalComponent;
    }

    public void validate() throws ExceptionResponse{
        if ( msrPrice < 0 ) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, MSR_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( bbbValue < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, TRADE_IN_VALUE_MUST_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( cogsPrice < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, COGS_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( flatPriceChange < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, FLAT_PRICE_CHANGE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( priceRole != null && priceRole < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, PRICE_ROLE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }

    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @NotNull
    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(@NotNull Float bbbValue) {
        this.bbbValue = bbbValue;
    }
}
