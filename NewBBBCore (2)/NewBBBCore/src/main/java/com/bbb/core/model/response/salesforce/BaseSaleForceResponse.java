package com.bbb.core.model.response.salesforce;

public class BaseSaleForceResponse {
    private String status;
    private String mesaage;

    public BaseSaleForceResponse() {}

    public BaseSaleForceResponse(String status, String mesaage) {
        this.status = status;
        this.mesaage = mesaage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMesaage() {
        return mesaage;
    }

    public void setMesaage(String mesaage) {
        this.mesaage = mesaage;
    }
}
