package com.bbb.core.controller.rating;

import com.bbb.core.common.Constants;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.request.rating.RatingCreateRequest;
import com.bbb.core.model.request.rating.RatingGetRequest;
import com.bbb.core.model.request.rating.RatingUpdateRequest;
import com.bbb.core.service.rating.RatingService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController

public class RatingController {
    @Autowired
    private RatingService service;

    @GetMapping(value = Constants.URL_RATING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getRating(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            Pageable pageable
    ) throws Exception {
        RatingGetRequest request = new RatingGetRequest();
        request.setMarketListingId(marketListingId);
        request.setInventoryAuctionId(inventoryAuctionId);
        request.setPageable(pageable);
        return new ResponseEntity<> (service.getRatingForBicycle(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_RATING_PATH)
    public ResponseEntity getRatingForUser(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @PathVariable(value = Constants.ID) String userId
    ) throws Exception {
        return new ResponseEntity<> (service.getRatingForUser(marketListingId, inventoryAuctionId, userId), HttpStatus.OK);
    }

    @GetMapping(value = "/api/rating/test")
    public ResponseEntity getRatingTest(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            Pageable pageable
    ) throws Exception {
        return new ResponseEntity<> (service.getRatingTest(marketListingId, inventoryAuctionId, pageable), HttpStatus.OK);
    }
    @RequestMapping(value = Constants.URL_RATING_CREATE, method = RequestMethod.POST)
    public ResponseEntity createRating(
            @RequestParam(value = "title") String title,
            @RequestParam(value = "comment", required = false) String comment,
            @RequestParam(value = "rate") long rate,
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            HttpServletRequest req
    ) throws Exception {
        RatingCreateRequest request = new RatingCreateRequest();
        request.setComment(comment);
        request.setRate(rate);
        request.setTitle(title);
        request.setUserId(CommonUtils.getUserLogin());
        return new ResponseEntity<>(service.createRating(request, inventoryAuctionId, marketListingId), HttpStatus.CREATED);
    }

    @RequestMapping(value = Constants.URL_RATING, method = RequestMethod.PUT)
    public ResponseEntity updateRating(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "comment", required = false) String comment,
            @RequestParam(value = "rate", required = false) Long rate,
            @RequestParam(value = "ratingId",required = false) Long id
    )throws Exception{
        RatingUpdateRequest request= new RatingUpdateRequest();
        request.setComment(comment);
        request.setRate(rate);
        request.setTitle(title);
        return new ResponseEntity<>(service.updateRating(request, id, inventoryAuctionId, marketListingId),HttpStatus.OK);
    }
}
