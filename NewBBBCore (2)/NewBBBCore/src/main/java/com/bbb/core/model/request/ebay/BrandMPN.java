package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BrandMPN {
    @JacksonXmlProperty(localName = "Brand")
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
