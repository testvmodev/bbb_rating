package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class GetItemResponse {
    @JacksonXmlProperty(localName = "Timestamp")
    private LocalDateTime timestamp;

    @JacksonXmlProperty(localName = "Ack")
    private String ack;

    @JacksonXmlProperty(localName = "CorrelationID")
    private long correlationID;
    @JacksonXmlProperty(localName = "Version")
    private long version;
    @JacksonXmlProperty(localName = "Build")
    private String build;
    @JacksonXmlProperty(localName = "NotificationEventName")
    private String notificationEventName;
    @JacksonXmlProperty(localName = "RecipientUserID")
    private String recipientUserID;
    @JacksonXmlProperty(localName = "EIASToken")
    private String eIASToken;
    @JacksonXmlProperty(localName = "Item")
    private Item item;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public long getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(long correlationID) {
        this.correlationID = correlationID;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getNotificationEventName() {
        return notificationEventName;
    }

    public void setNotificationEventName(String notificationEventName) {
        this.notificationEventName = notificationEventName;
    }

    public String getRecipientUserID() {
        return recipientUserID;
    }

    public void setRecipientUserID(String recipientUserID) {
        this.recipientUserID = recipientUserID;
    }

    public String geteIASToken() {
        return eIASToken;
    }

    public void seteIASToken(String eIASToken) {
        this.eIASToken = eIASToken;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
