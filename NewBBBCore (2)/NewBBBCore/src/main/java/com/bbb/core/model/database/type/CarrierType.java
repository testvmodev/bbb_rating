package com.bbb.core.model.database.type;

public enum CarrierType {
    UPS("UPS"),
    FEDEX("Fedex");
    private final String value;

    CarrierType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static CarrierType findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "UPS":
                return UPS;
            case "Fedex":
                return FEDEX;
            default:
                return null;
        }
    }
}
