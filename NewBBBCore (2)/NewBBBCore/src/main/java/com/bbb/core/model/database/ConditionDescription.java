package com.bbb.core.model.database;

import com.bbb.core.model.database.type.ConditionInventory;

import javax.persistence.*;

@Entity
@Table(name = "condition_description")
public class ConditionDescription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private ConditionInventory condition;
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
