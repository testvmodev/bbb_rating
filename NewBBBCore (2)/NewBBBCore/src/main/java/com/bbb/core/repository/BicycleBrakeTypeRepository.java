package com.bbb.core.repository;

import com.bbb.core.model.database.BrakeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface BicycleBrakeTypeRepository extends CrudRepository<BrakeType, Long> {
    @Lock(value = LockModeType.READ)
    @Query(nativeQuery = true, value = "SELECT * FROM brake_type")
    List<BrakeType> findAll();
}
