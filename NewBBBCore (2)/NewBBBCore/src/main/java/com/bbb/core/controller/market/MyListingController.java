package com.bbb.core.controller.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingShipmentCreateRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingUpdateRequest;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.market.MyListingService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Controller
public class MyListingController {
    @Autowired
    private MyListingService service;

    @GetMapping(Constants.URL_PTP_MY_LISTING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getAllMyListings(
            @RequestParam("sortField") MyListingSortField sortField,
            @RequestParam("sortType") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getAllMyListings(sortField, sortType, pageable));
    }

    @GetMapping(Constants.URL_MARKET_LISTING_PTP_PATH)
    public ResponseEntity getMyListingDetail(
            @PathVariable(Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getMyListingDetail(marketListingId));
    }

    //remove old workflow, create from daft instead
//    @PostMapping(value = Constants.URL_MARKET_LISTING_PTP)
//    public ResponseEntity createMyListingPTP(
//            @RequestBody @Valid PersonalListingRequest request
//    ) throws ExceptionResponse {
//        return new ResponseEntity<>(service.createMyListingPTP(request), HttpStatus.OK);
//    }

    @PutMapping(Constants.URL_MARKET_LISTING_PTP_PATH)
    public ResponseEntity updateMyListing(
            @PathVariable(Constants.ID) long marketListingId,
            @RequestBody PersonalListingUpdateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateMyListing(marketListingId, request));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_PTP_PATH)
    public ResponseEntity deleteMyListing(
            @PathVariable(Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteMyListing(marketListingId));
    }

    //remove old workflow,
//    @PostMapping(value = Constants.URL_MARKET_LISTING_PTP_PATH_IMAGE)
//    public ResponseEntity postImageMarketListingPTP(
//            @PathVariable(value = Constants.ID) long marketListingId,
//            @RequestParam(value = "images") List<MultipartFile> images
//    ) throws ExceptionResponse {
//        return new ResponseEntity<>(service.postImageMarketListingPTP(marketListingId,  images), HttpStatus.OK);
//    }

    @PutMapping(Constants.URL_MARKET_LISTING_PTP_PATH_IMAGE)
    public ResponseEntity addImageImageMarketListingPTP(
            @PathVariable(Constants.ID) long marketListingId,
            @RequestParam("images") List<MultipartFile> images
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.addImageImageMarketListingPTP(marketListingId, images));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_PTP_PATH_IMAGE)
    public ResponseEntity deleteImageMarketListingPTP(
            @PathVariable(Constants.ID) long marketListingId,
            @RequestParam("inventoryImageIds") List<Long> inventoryImageIds
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteImageMarketListingPTP(marketListingId, inventoryImageIds));
    }

    @PostMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT)
    public ResponseEntity createDraftMyListing(
            @RequestBody PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return ResponseEntity.ok(service.createDraftMyListing(request));
    }

    @GetMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_PATH)
    public ResponseEntity getDraftDetail(
            @PathVariable(Constants.ID) long myListingDraftId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getDraftDetail(myListingDraftId));
    }

    @PutMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_PATH)
    public ResponseEntity updateDraftMyListing(
            @PathVariable(value = Constants.ID) long draftId,
            @RequestBody PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return ResponseEntity.ok(service.updateDraftMyListing(draftId, request));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_PATH)
    public ResponseEntity deleteDraftMyListing(
            @PathVariable(value = Constants.ID) long draftId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteDraftMyListing(draftId));
    }

    @PostMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_POST_IMAGE_PATH)
    public ResponseEntity postImageDraftMyListing(
            @PathVariable(value = Constants.ID) long listingDraftId,
            @RequestParam(value = "images") List<MultipartFile> images
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.postImageDraftMyListing(listingDraftId, images));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_IMAGE)
    public ResponseEntity deleteImageDraftMyListing(
            @RequestParam("imageDraftIds") List<Long> imageDraftIds
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteImageDraftMyListing(imageDraftIds));
    }

    @PostMapping(Constants.URL_MARKET_LISTING_PTP_DRAFT_FINISH)
    public ResponseEntity createListingFromDraft(
            @PathVariable(value = Constants.ID) long draftId
    ) throws ExceptionResponse, MethodArgumentNotValidException, NoSuchMethodException {
        return ResponseEntity.ok(service.createListingFromDraft(draftId));
    }

    @PatchMapping(Constants.URL_MARKET_LISTING_PTP_RELIST)
    public ResponseEntity relistMyListing(
            @PathVariable(Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.relistMyListing(marketListingId));
    }

    @PostMapping(Constants.URL_MARKET_LISTING_PTP_SHIPMENT)
    public ResponseEntity createShipment(
            @PathVariable(Constants.ID) long marketListingId,
            @RequestParam(value = "carrier", required = false) String carrier,
            @RequestParam(value = "trackingNumber", required = false) String trackingNumber
    ) throws ExceptionResponse {
        PersonalListingShipmentCreateRequest request = new PersonalListingShipmentCreateRequest();
        request.setMarketListingId(marketListingId);
        request.setCarrier(carrier);
        request.setTrackingNumber(trackingNumber);

        return ResponseEntity.ok(service.createShipment(request));
    }
}
