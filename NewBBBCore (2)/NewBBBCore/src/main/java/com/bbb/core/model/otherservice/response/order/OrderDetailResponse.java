package com.bbb.core.model.otherservice.response.order;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OrderDetailResponse {
    @JsonProperty("_id")
    private String id;
    @JsonProperty("order_code")
    private String orderCode;
    @JsonProperty("payment_id")
    private String paymentId;
    private String status;
    private String stage;
    private String user;
    @JsonProperty("amount")
    private OrderMoneyAmount moneyAmount;
    @JsonProperty("shipping_address")
    private OrderShippingAddress shippingAddress;
    @JsonProperty("billing_address")
    private OrderBillingAddress billingAddress;
    @JsonProperty("line_item")
    List<OrderLineItem> lineItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public OrderMoneyAmount getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(OrderMoneyAmount moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public OrderShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(OrderShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public OrderBillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(OrderBillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<OrderLineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<OrderLineItem> lineItems) {
        this.lineItems = lineItems;
    }
}
