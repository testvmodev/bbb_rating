package com.bbb.core.security;

import com.bbb.core.common.Constants;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.security.authen.ajax.AjaxTokenAuthenticationFailureHandler;
import com.bbb.core.security.authen.ajax.SecretKeyAuthenticationFailureHandler;
import com.bbb.core.security.authen.extractor.TokenExtractor;
import com.bbb.core.security.authen.jwt.JwtAuthenticationProvider;
import com.bbb.core.security.authen.jwt.JwtTokenAuthenticationProcessingFilter;
import com.bbb.core.security.authen.jwt.JwtTokenFactory;
import com.bbb.core.security.authen.jwt.KeyAPIAuthenticationProcessingFilter;
import com.bbb.core.security.match.SkipPathRequestMatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ducnd on 6/25/17.
 */
@EnableWebSecurity
@Configuration
@ConfigurationProperties(prefix = "bbb.security")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private String tokenSecret;
    private String fileSecretKey;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;


    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;

    @Autowired
    private TokenExtractor tokenExtractor;
    @Autowired
    private JwtTokenFactory jwtTokenFactory;


    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public String getFileSecretKey() {
        return fileSecretKey;
    }

    public void setFileSecretKey(String fileSecretKey) {
        this.fileSecretKey = fileSecretKey;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(ajaxLoginProcessingFilter);
        auth.authenticationProvider(jwtAuthenticationProvider);
//        auth.authenticationProvider(registerAuthenticationProvider);
    }

//    public AjaxLoginRegisterProcessingFilter getFilter() {
//        List<String> matchs = new ArrayList<>();
//        matchs.add(Constants.ENPOINT_LOGIN);
//        matchs.add(Constants.ENPOINT_REGISTER);
//        MatchLoginRegister matchLoginRegister = new MatchLoginRegister(matchs);
//        AjaxLoginRegisterProcessingFilter filter =
//                new AjaxLoginRegisterProcessingFilter(matchLoginRegister, authenticationSuccessHandler, new AjaxAuthenticationLoginRegisterFailureHandler(objectMapper), objectMapper);
//        try {
//            filter.setAuthenticationManager(authenticationManagerBean());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return filter;
//    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    private JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter(
            List<Pair<HttpMethod, String>> skipAuthen,
            List<Pair<HttpMethod, String>> processAuthen
    ) {
//        RequestMatcher matcher = new SkipPathRequestMatcher(listNotmap, Constants.END_POINT_MATCH_API);
        JwtTokenAuthenticationProcessingFilter filter = new JwtTokenAuthenticationProcessingFilter(
                skipAuthen, processAuthen, //if doesnt contain in both list, still process by default
                Constants.END_POINT_MATCH_API,
                new AjaxTokenAuthenticationFailureHandler(objectMapper),
                jwtTokenFactory,
                tokenExtractor
        );
        try {
            filter.setAuthenticationManager(authenticationManagerBean());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filter;
    }

    private KeyAPIAuthenticationProcessingFilter buildKeyAPIAuthenticationProcessingFilter(List<Pair<HttpMethod, String>> listNotmap) {
        RequestMatcher requestMatcher = new SkipPathRequestMatcher(listNotmap, Constants.END_POINT_MATCH_API_SECRET_KEY);
        KeyAPIAuthenticationProcessingFilter filter = new KeyAPIAuthenticationProcessingFilter(requestMatcher,
                new SecretKeyAuthenticationFailureHandler(objectMapper));
        try {
            filter.setAuthenticationManager(authenticationManagerBean());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        List<Pair<HttpMethod, String>> apiPass = new ArrayList<>();
        apiPass.addAll(Constants.API_GET_SKIP_AUTHEN);
        apiPass.addAll(Constants.API_POST_SKIP_AUTHEN);

        List<Pair<HttpMethod, String>> apiProces = new ArrayList<>();
        apiProces.addAll(Constants.API_GET_MUST_AUTHEN);

        http
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)

                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, Constants.API_GET_SKIP_AUTHEN.stream().map(s -> s.getSecond()).collect(Collectors.toList()).toArray(new String[0]))
//                .permitAll()
//
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.POST, Constants.API_POST_SKIP_AUTHEN.stream().map(s -> s.getSecond()).collect(Collectors.toList()).toArray(new String[0]))
//                .permitAll()

                .and().authorizeRequests()
                .antMatchers(Constants.END_POINT_MATCH_API).authenticated()

                .and()
                .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(apiPass, apiProces),
                        UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(buildKeyAPIAuthenticationProcessingFilter(apiPass),
                        UsernamePasswordAuthenticationFilter.class);

    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    }
}
