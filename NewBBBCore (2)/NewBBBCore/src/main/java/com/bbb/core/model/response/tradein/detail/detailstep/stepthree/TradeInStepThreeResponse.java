package com.bbb.core.model.response.tradein.detail.detailstep.stepthree;

import com.bbb.core.model.database.type.InboundShippingType;

public class TradeInStepThreeResponse {
    private long tradeInId;
    private ShippingBicycleBlueBookResponse blueBook;
    private ShippingMyAccountResponse myAccount;
    private InboundShippingType shippingType;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public ShippingBicycleBlueBookResponse getBlueBook() {
        return blueBook;
    }

    public void setBlueBook(ShippingBicycleBlueBookResponse blueBook) {
        this.blueBook = blueBook;
    }

    public ShippingMyAccountResponse getMyAccount() {
        return myAccount;
    }

    public void setMyAccount(ShippingMyAccountResponse myAccount) {
        this.myAccount = myAccount;
    }

    public InboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(InboundShippingType shippingType) {
        this.shippingType = shippingType;
    }
}
