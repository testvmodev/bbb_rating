package com.bbb.core.model.database;

import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShippingFeeConfig {
    @Id
    private long bicycleTypeId;
    private long ebayShippingProfileId;
    private float shippingFee;
    private String label;
    private String description;
    private boolean isDelete;
    private LocalDateTime lastUpdate;

    public long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(float shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
