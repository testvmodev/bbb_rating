package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CompletedPackageDetails {
    @JacksonXmlProperty(localName = "Label")
    private Label label;
    @JacksonXmlProperty(localName = "SignatureOption")
    private String signatureOption;

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public String getSignatureOption() {
        return signatureOption;
    }

    public void setSignatureOption(String signatureOption) {
        this.signatureOption = signatureOption;
    }
}
