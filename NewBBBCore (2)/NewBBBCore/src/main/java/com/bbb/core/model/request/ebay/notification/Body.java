package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Body {
    @JacksonXmlProperty(localName = "GetItemResponse")
    @JsonProperty(value = "itemResponse")
    private GetItemResponse itemResponse;
    @JsonProperty(value = "GetItemTransactionsResponse")
    private GetItemTransactionsResponse itemTransactionsResponse;

    public GetItemResponse getItemResponse() {
        return itemResponse;
    }

    public void setItemResponse(GetItemResponse itemResponse) {
        this.itemResponse = itemResponse;
    }

    public GetItemTransactionsResponse getItemTransactionsResponse() {
        return itemTransactionsResponse;
    }

    public void setItemTransactionsResponse(GetItemTransactionsResponse itemTransactionsResponse) {
        this.itemTransactionsResponse = itemTransactionsResponse;
    }
}
