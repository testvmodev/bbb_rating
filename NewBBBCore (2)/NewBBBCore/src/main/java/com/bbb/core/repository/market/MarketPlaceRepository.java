package com.bbb.core.repository.market;

import com.bbb.core.model.database.MarketPlace;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketPlaceRepository extends CrudRepository<MarketPlace, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM market_place " +
            "WHERE is_delete = 0")
    List<MarketPlace> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM market_place " +
            "WHERE market_place.id = :marketPlaceId AND market_place.is_delete = 0")
    MarketPlace findOne(
            @Param(value = "marketPlaceId") long marketPlaceId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM market_place " +
            "WHERE market_place.is_delete = 0 AND market_place.id IN :marketPlaceIds ")
    List<MarketPlace> findAllByIds(
            @Param(value = "marketPlaceIds") List<Long> marketPlaceIds
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 market_place.id FROM market_place " +
            "WHERE " +
            "market_place.type  = :type AND " +
            "market_place.is_delete = 0")
    Long getMarketPlaceId(
            @Param(value = "type") String type
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM market_place WHERE market_place.type = :type AND market_place.is_delete = 0")
    MarketPlace findOneByType(
            @Param(value = "type") String type
    );

}
