package com.bbb.core.model.request.tradein.fedex;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.request.tradein.fedex.detail.ClientDetail;
import com.bbb.core.model.request.tradein.fedex.detail.track.SelectionDetails;
import com.bbb.core.model.request.tradein.fedex.detail.Version;
import com.bbb.core.model.request.tradein.fedex.detail.WebAuthenticationDetail;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "TrackRequest", namespace = FedExValue.TRACK_ROOT_NAMESPACE)
public class FedexTrackRequest {
    @JacksonXmlProperty(localName = "WebAuthenticationDetail")
    private WebAuthenticationDetail webAuthenticationDetail;
    @JacksonXmlProperty(localName = "ClientDetail")
    private ClientDetail clientDetail;
    @JacksonXmlProperty(localName = "Version")
    private Version version;
    @JacksonXmlProperty(localName = "SelectionDetails")
    private SelectionDetails selectionDetails;
    @JacksonXmlProperty(localName = "ProcessingOptions")
    private String processingOptions;

    public WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }

    public void setWebAuthenticationDetail(WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }

    public ClientDetail getClientDetail() {
        return clientDetail;
    }

    public void setClientDetail(ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public SelectionDetails getSelectionDetails() {
        return selectionDetails;
    }

    public void setSelectionDetails(SelectionDetails selectionDetails) {
        this.selectionDetails = selectionDetails;
    }

    public String getProcessingOptions() {
        return processingOptions;
    }

    public void setProcessingOptions(String processingOptions) {
        this.processingOptions = processingOptions;
    }
}
