package com.bbb.core.model.request.market;

public class MarketPlaceConfigCreateRequest {
    private Long marketPlaceId;
    private String name;
    private String ebayAppId;
    private String ebayCertId;
    private String ebayDevId;
    private String ebayToken;
    private Boolean isEbayApiSandbox;

    public Long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(Long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEbayApiSandbox() {
        return isEbayApiSandbox;
    }

    public String getEbayAppId() {
        return ebayAppId;
    }

    public void setEbayAppId(String ebayAppId) {
        this.ebayAppId = ebayAppId;
    }

    public String getEbayCertId() {
        return ebayCertId;
    }

    public void setEbayCertId(String ebayCertId) {
        this.ebayCertId = ebayCertId;
    }

    public String getEbayDevId() {
        return ebayDevId;
    }

    public void setEbayDevId(String ebayDevId) {
        this.ebayDevId = ebayDevId;
    }

    public String getEbayToken() {
        return ebayToken;
    }

    public void setEbayToken(String ebayToken) {
        this.ebayToken = ebayToken;
    }

    public Boolean isEbayApiSandbox() {
        return isEbayApiSandbox;
    }

    public void setEbayApiSandbox(Boolean ebayApiSandbox) {
        isEbayApiSandbox = ebayApiSandbox;
    }
}
