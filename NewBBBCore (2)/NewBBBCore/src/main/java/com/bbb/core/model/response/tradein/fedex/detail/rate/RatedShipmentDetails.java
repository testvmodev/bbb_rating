package com.bbb.core.model.response.tradein.fedex.detail.rate;

import com.bbb.core.model.response.tradein.fedex.detail.ShipmentRateDetail;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RatedShipmentDetails {
    @JacksonXmlProperty(localName = "ShipmentRateDetail")
    private ShipmentRateDetail shipmentRateDetail;

    public ShipmentRateDetail getShipmentRateDetail() {
        return shipmentRateDetail;
    }

    public void setShipmentRateDetail(ShipmentRateDetail shipmentRateDetail) {
        this.shipmentRateDetail = shipmentRateDetail;
    }
}
