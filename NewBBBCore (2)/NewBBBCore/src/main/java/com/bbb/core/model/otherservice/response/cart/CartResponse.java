package com.bbb.core.model.otherservice.response.cart;

import java.util.List;

public class CartResponse {
    private List<CartItemResponse> cart;
    private String message;

    public List<CartItemResponse> getCart() {
        return cart;
    }

    public void setCart(List<CartItemResponse> cart) {
        this.cart = cart;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
