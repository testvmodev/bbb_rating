package com.bbb.core.model.response.common;

public enum OperatorMarketListingFilter {
    EQUAL,
    NOT_EQUAL,
    RANGE,
    NOT_RANGE,
    RANGE_DATE,
    NOT_RANGE_DATE,
    RANGE_IN,
    NOT_RANGE_IN
}
