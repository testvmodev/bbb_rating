package com.bbb.core.model.type;

import com.bbb.core.common.Constants;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class YearMonthConverter implements AttributeConverter<YearMonth, String> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern(Constants.FORMAT_DATE_TIME);

    @Override
    public String convertToDatabaseColumn(YearMonth yearMonth) {
        if (yearMonth == null) {
            return null;
        }
        return yearMonth.toString(Constants.FORMAT_DATE_TIME);
    }

    @Override
    public YearMonth convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return YearMonth.parse(s, FORMATTER);
    }
}
