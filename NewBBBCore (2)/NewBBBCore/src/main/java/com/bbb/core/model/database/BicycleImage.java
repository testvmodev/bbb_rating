package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "bicycle_image")
public class BicycleImage {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long bicycleId;
  private String uri;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getBicycleId() {
    return bicycleId;
  }

  public void setBicycleId(long bicycleId) {
    this.bicycleId = bicycleId;
  }


  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

}
