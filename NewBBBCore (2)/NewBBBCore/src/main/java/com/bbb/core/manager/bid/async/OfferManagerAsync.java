package com.bbb.core.manager.bid.async;

import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.model.database.*;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.InventoryLocationShippingRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class OfferManagerAsync {
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;

    @Async
    public void sendEmailWhenCreateOffer(Inventory inventory, Offer offer, String buyerId) {
        notificationManager.sendMailOfferMade(
                buyerId,
                inventory,
                offer.getMarketListingId() != null ? offer.getMarketListingId() : offer.getInventoryAuctionId(),
                offer.getMarketListingId() != null,
                inventory.getCurrentListedPrice(),
                offer.getOfferPrice()

        );
    }


    @Async
    public void sendEmailOfferRejected(List<Offer> offers) {
        List<Long> marketListingIds = offers.stream().map(Offer::getMarketListingId).filter(Objects::nonNull).collect(Collectors.toList());
        CommonUtils.stream(marketListingIds);
        List<Long> inventoryAuctionIds = offers.stream().map(Offer::getInventoryAuctionId).filter(Objects::nonNull).collect(Collectors.toList());
        List<Long> inventoryIds = new ArrayList<>();
        if (marketListingIds.size() > 0) {
            inventoryIds.addAll(marketListingRepository.findAllInventoryIdByIds(marketListingIds)
                    .stream().map(Integer::longValue).collect(Collectors.toList())
            );
        }
        if (inventoryAuctionIds.size() > 0) {
            inventoryIds.addAll(
                    inventoryAuctionRepository.findAllInventoryIdByIds(inventoryAuctionIds)
                            .stream().map(Integer::longValue).collect(Collectors.toList())
            );
        }

        List<MarketListing> marketListings;
        if (marketListingIds.size() > 0) {
            marketListings = marketListingRepository.findAllByIds(marketListingIds);
        } else {
            marketListings = new ArrayList<>();
        }
        List<InventoryAuction> inventoryAuctions;
        if (inventoryAuctionIds.size() > 0) {
            inventoryAuctions = inventoryAuctionRepository.findAllByIds(inventoryAuctionIds);
        } else {
            inventoryAuctions = new ArrayList<>();
        }

        CommonUtils.stream(inventoryIds);
        List<Inventory> inventories;
        if (inventoryIds.size() > 0) {
            inventories = inventoryRepository.findAllByIds(inventoryIds);
        } else {
            inventories = new ArrayList<>();
        }

        sendEmailOfferRejected(offers, marketListings, inventoryAuctions, inventories);

    }

    private void sendEmailOfferRejected(List<Offer> offers, List<MarketListing> marketListings, List<InventoryAuction> inventoryAuctions, List<Inventory> inventories) {
        if (offers.size() > 0) {
            ExecutorService executorService = Executors.newFixedThreadPool(offers.size() <= 3 ? offers.size() : 3);
            List<Observable<Boolean>> os = new ArrayList<>();
            for (Offer offer : offers) {
                if (offer.getMarketListingId() != null) {
                    MarketListing marketListing = CommonUtils.findObject(marketListings, o -> o.getId() == offer.getMarketListingId());
                    if (marketListing != null) {
                        Inventory inventory = CommonUtils.findObject(inventories, o -> o.getId() == marketListing.getInventoryId());
                        if (inventory != null) {
                            os.add(
                                    createObservableSendMailRejected(offer, inventory, executorService)
                            );
                        }
                    }
                } else {
                    InventoryAuction inventoryAuction = CommonUtils.findObject(inventoryAuctions, o -> o.getId() == offer.getInventoryAuctionId());
                    if (inventoryAuction != null) {
                        Inventory inventory = CommonUtils.findObject(inventories, o -> o.getId() == inventoryAuction.getInventoryId());
                        if (inventory != null) {
                            os.add(
                                    createObservableSendMailRejected(offer, inventory, executorService)
                            );
                        }
                    }
                }
            }
            if (os.size() > 0) {
                Observable.zip(os, oArray -> oArray).subscribe();
            }

        }
    }

    private Observable<Boolean> createObservableSendMailRejected(Offer offer, Inventory inventory, ExecutorService executorService) {
        return Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            notificationManager.sendMailOfferRejected(offer, inventory, true);
            t.onNext(true);
            t.onComplete();
        }).subscribeOn(Schedulers.from(executorService));
    }

    @Async
    public void sendEmailOfferRejectFromSeller(Offer offer, MarketListing marketListing){
        notificationManager.sendMailOfferRejected(offer, inventoryRepository.findOne(marketListing.getInventoryId()), true);
    }



    @Async
    public void sendEmailOfferCounted(Offer offer) {
        if (offer.getMarketListingId() == null) {
            return;
        }
        MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
        if (marketListing == null) {
            return;
        }
        notificationManager.sendMailOfferCounted(offer, inventoryRepository.findOne(marketListing.getInventoryId()));
    }

    @Async
    public void addCardAnSendEmailFromBuyer(Offer offer, long inventoryId) {
//        InventoryLocationShipping inventoryLocationShipping = inventoryLocationShippingRepository.findOne(inventoryId);
        try {
            billingManager.addCart(
                    offer.getBuyerId(),
                    ValueCommons.CART_TYPE_OFFER,
                    offer.getId()
//                    inventoryLocationShipping.isAllowLocalPickup()
            );
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
//            String content = new String(e.getResponseBodyAsByteArray());
//            try {
//                ErrorRestResponse errorRestResponse = objectMapper.readValue(content, ErrorRestResponse.class);
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, errorRestResponse.getMessage()), HttpStatus.BAD_REQUEST);
//            } catch (IOException e1) {
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, e.getMessage()), HttpStatus.BAD_REQUEST);
//            }
        }
        try {
            notificationManager.sendMailOfferAcceptedNormal(offer, inventoryRepository.findOne(inventoryId));
            notificationManager.sendMailOfferBuyerAccepted(offer, inventoryRepository.findOne(inventoryId));
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void addCardAnSendEmailFromSeller(Offer offer, long inventoryId) {
//        InventoryLocationShipping inventoryLocationShipping = inventoryLocationShippingRepository.findOne(inventoryId);
        try {
            billingManager.addCart(
                    offer.getBuyerId(),
                    ValueCommons.CART_TYPE_OFFER,
                    offer.getId()
//                    inventoryLocationShipping.isAllowLocalPickup()
            );
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
        try {
            notificationManager.sendMailOfferAcceptedNormal(offer, inventoryRepository.findOne(inventoryId));
//            notificationManager.sendMailOfferBuyerAccepted(offer, inventoryRepository.findOne(inventoryId));
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendMailRejectFromBuyer(Offer offer) {
        if (offer.getMarketListingId() == null) {
            return;
        }
        MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
        if (marketListing == null) {
            return;
        }
        notificationManager.sendMailOfferBuyerRejected(offer, inventoryRepository.findOne(marketListing.getInventoryId()));
    }

    @Async
    public void sendMailCancelFromSeller(Offer offer, MarketListing marketListing){
        try {
            billingManager.removeCart(offer.getBuyerId(), marketListing.getInventoryId());
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
        try {
            notificationManager.sendMailOfferRejectedAccepted(offer.getBuyerId(), marketListing.getId(), marketListing.getInventoryId());
//            notificationManager.sendMailOfferRejected(offer, inventoryRepository.findOne(marketListing.getInventoryId()), false);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendMailCounteredFromBuyer(Offer offer, MarketListing marketListing){
        notificationManager.sendMailOfferBuyerCountered(offer,  inventoryRepository.findOne(marketListing.getInventoryId()));
    }

}
