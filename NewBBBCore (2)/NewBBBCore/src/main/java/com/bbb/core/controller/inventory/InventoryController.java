package com.bbb.core.controller.inventory;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.request.inventory.InventoryRequest;
import com.bbb.core.model.request.inventory.create.InventoryCreateFullRequest;
import com.bbb.core.model.request.inventory.secret.InventoryUpdateSecretRequest;
import com.bbb.core.model.request.inventory.update.InventoryUpdateFullRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingSaleRequest;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.repository.inventory.InventoryCompDetailRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.security.authen.usercontext.UserContextManager;
import com.bbb.core.service.inventory.InventoryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class InventoryController {
    @Autowired
    private InventoryService service;
    @Autowired
    private UserContextManager userContextManager;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryCompDetailRepository inventoryCompDetailRepository;


    @GetMapping(value = Constants.URL_GET_INVENTORY)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getInventories(
            @RequestParam(value = "bicycleIds", required = false) List<Long> bicycleIds,
            @RequestParam(value = "modelIds", required = false) List<Long> modelIds,
            @RequestParam(value = "brandIds", required = false) List<Long> brandIds,
            @RequestParam(value = "sizeNames", required = false) List<String> sizeNames,
            @RequestParam(value = "typeIds", required = false) List<Long> typeIds,
            @RequestParam(value = "frameMaterialNames", required = false) List<String> frameMaterialNames,
            @RequestParam(value = "brakeTypeNames", required = false) List<String> brakeTypeNames,
            @RequestParam(value = "conditions", required = false) List<ConditionInventory> conditions,
            @RequestParam(value = "startYearId", required = false) Long startYearId,
            @RequestParam(value = "endYearId", required = false) Long endYearId,
            @RequestParam(value = "startPrice", required = false) Float startPrice,
            @RequestParam(value = "endPrice", required = false) Float endPrice,
            @RequestParam(value = "searchContent", required = false) String searchContent,
            @RequestParam(value = "status", required = false) StatusInventory status,
            @RequestParam(value = "stage", required = false) StageInventory stage,
            @RequestParam(value = "marketType", required = false) MarketType marketType,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sortType", required = true, defaultValue = "ASC") Sort sortType,
            @RequestParam(value = "sortField") InventorySortField sortField,
            Pageable page
    ) throws ExceptionResponse {
        InventoryRequest request = new InventoryRequest();
        request.setBicycleIds(bicycleIds);
        request.setModelIds(modelIds);
        request.setBrandIds(brandIds);
        request.setSizeNames(sizeNames);
        request.setTypeIds(typeIds);
        request.setFrameMaterialNames(frameMaterialNames);
        request.setBrakeTypeNames(brakeTypeNames);
        request.setConditions(conditions);
        request.setStartYearId(startYearId);
        request.setEndYearId(endYearId);
        request.setStartPrice(startPrice);
        request.setEndPrice(endPrice);
        request.setSearchContent(searchContent);
        request.setStatus(status);
        request.setStage(stage);
        request.setMarketType(marketType);
        request.setName(name);
        request.setSortType(sortType);
        request.setSortField(sortField);

        return new ResponseEntity<>(service.getInventories(request, page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_INVENTORY)
    public ResponseEntity getInventoryDetail(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return new ResponseEntity(service.getInventoryDetail(id),
                HttpStatus.OK);
    }

//    @PostMapping(value = Constants.URL_INVENTORY_CRATE)
//    public ResponseEntity createInventory(
//            @RequestParam(value = "bicycleId") long bicycleId,
//            @RequestParam(value = "description", required = false) String description,
//            @RequestParam(value = "imageDefault") String imageDefault,
//            @RequestParam(value = "msrpPrice", required = false) Float msrpPrice,
//            @RequestParam(value = "tradeInPrice") Float tradeInPrice,
//            @RequestParam(value = "overrideTradeInPrice", required = false, defaultValue = "0.0") float overrideTradeInPrice,
//            @RequestParam(value = "cogsPrice", required = false) Float cogsPrice,
//            @RequestParam(value = "partnerId") String partnerId,
//            @RequestParam(value = "salerId") String sellerId,
//            @RequestParam(value = "serialNumber") String serialNumber,
//            @RequestParam(value = "condition") ConditionInventory condition,
//            @RequestParam(value = "inventoryTypeId") long inventoryTypeId,
//            @RequestParam(value = "bestOfferAutoAcceptPrice", required = false) Float bestOfferAutoAcceptPrice,
//            @RequestParam(value = "minimumOfferAutoAcceptPrice", required = false) Float minimumOfferAutoAcceptPrice,
//            @RequestParam(value = "createPo", required = false) boolean createPo,
//            @RequestParam(value = "sizeInventoryId") long sizeInventoryId,
//            @RequestParam(value = "color", required = false) String color,
//            @RequestParam(value = "status") StatusInventory status,
//            @RequestParam(value = "stage") StageInventory stage,
//            @RequestParam(value = "flatPriceChange", required = false) Float flatPriceChange,
//            @RequestParam(value = "bestOffer", required = false) boolean bestOffer,
//            @RequestParam(value = "isCustomQuote", required = false) boolean isCustomQuote
//
//    ) throws ExceptionResponse {
//        InventoryCreateRequest request = new InventoryCreateRequest();
//        request.setBicycleId(bicycleId);
//        request.setDescription(description);
//        request.setImageDefault(imageDefault);
//        request.setMsrpPrice(msrpPrice);
//        request.setBbbValue(tradeInPrice);
//        request.setOverrideTradeInPrice(overrideTradeInPrice);
//        request.setCogsPrice(cogsPrice);
//        request.setUserCreatedId(partnerId);
//        request.setSellerId(sellerId);
//        request.setSerialNumber(serialNumber);
//        request.setCondition(condition);
//        request.setInventoryTypeId(inventoryTypeId);
//        request.setCreatePo(createPo);
//        request.setSizeInventoryId(sizeInventoryId);
//        request.setColor(color);
//        request.setStage(stage);
//        request.setStatus(status);
//        request.setFlatPriceChange(flatPriceChange);
//        request.setBestOfferAutoAcceptPrice(bestOfferAutoAcceptPrice);
//        request.setMinimumOfferAutoAcceptPrice(minimumOfferAutoAcceptPrice);
//        request.setBestOffer(bestOffer);
//        request.setCustomQuote(isCustomQuote);
//        return new ResponseEntity<>(service.createInventory(request),
//                HttpStatus.OK);
//    }


    @PutMapping(value = Constants.URL_INVENTORY)
    public ResponseEntity updateInventory(
            @PathVariable(value = Constants.ID) long inventoryId,
            @RequestBody InventoryUpdateFullRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return new ResponseEntity<>(service.updateInventory(inventoryId, request),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_INVENTORY_CREATE)
    public ResponseEntity createInventory(
            @RequestBody(required = true) @Valid InventoryCreateFullRequest request,
            HttpServletRequest httpServletRequest
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.createInventory(request),
                HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_SECRET_INVENTORIES)
    public ResponseEntity getSecretInventories(
            @RequestParam(value = "inventoriesId") List<Long> inventoriesId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getSecretInventories(inventoriesId),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_SECRET_INVENTORY_SALE)
    public ResponseEntity soldInventory(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "offerId", required = false) Long offerId,
            @RequestParam(value = "buyerId") String buyerId,
            @RequestParam(value = "buyerDisplayName") String buyerDisplayName,
            @RequestParam(value = "buyerEmail") String buyerEmail,
            @RequestParam(value = "buyerRole") UserRoleType buyerRole,
            @RequestParam("orderId") String orderId
    ) throws ExceptionResponse {
        MarketListingSaleRequest request = new MarketListingSaleRequest();
        request.setMarketListingId(marketListingId);
        request.setOfferId(offerId);
        request.setBuyerId(buyerId);
        request.setBuyerDisplayName(buyerDisplayName);
        request.setBuyerEmail(buyerEmail);
        request.setBuyerRole(buyerRole);
        request.setOrderId(orderId);
        return new ResponseEntity<>(service.soldInventory(request), HttpStatus.OK);
    }

//    @GetMapping(value = "/updateSize")
//    public ResponseEntity updateSize() {
//        List<String> sizes = new ArrayList<>();
//        sizes.add("All");
//        sizes.add("Carbon Fiber");
//        sizes.add("Aluminum");
//        sizes.add("Steel");
//        sizes.add("Titanium");
//        List<InventoryCompDetail> details = inventoryRepository.findAllInventoryColorNotNull()
//                .stream().map(in -> {
//                    InventoryCompDetail detail = new InventoryCompDetail();
//                    InventoryCompDetailId id = new InventoryCompDetailId();
//                    id.setInventoryId(in.getId());
//                    id.setInventoryCompTypeId(ValueCommons.INVENTORY_COLOR_ID);
//                    detail.setId(id);
//                    detail.setValue(in.getColor());
//                    return detail;
//                }).collect(Collectors.toList());
//        return new ResponseEntity<>(  inventoryCompDetailRepository.save(details),
//                HttpStatus.OK);
//
//    }


//
//    @GetMapping(value = "/importIdSaleForceId")
//    public ResponseEntity importIdSaleForceId() {
//        return new ResponseEntity<>(service.importIdSaleForceId(),
//                HttpStatus.OK);
//    }
//    @GetMapping(value = "importInventoryImage")
//    public ResponseEntity importInventoryImage(){
//        return new ResponseEntity<>(service.importInventoryImage(),
//                HttpStatus.OK);
//    }


    public ResponseEntity updateInventorySecret(
            @PathVariable(Constants.ID) long inventoryId,
            @RequestBody InventoryUpdateSecretRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateInventorySecret(inventoryId, request));
    }
}
