package com.bbb.core.repository.market;

import com.bbb.core.model.database.InventorySale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventorySaleRepository extends JpaRepository<InventorySale, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM inventory_sale " +
            "WHERE inventory_sale.inventory_id = ?1 " +
            "ORDER BY inventory_sale.is_delete ASC, inventory_sale.id DESC "
    )
    InventorySale findOne(long inventoryId);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_sale " +
            "WHERE (1 = :#{@queryUtils.isEmptyList(#inventoryIds) ? 1 : 0} " +
            "OR inventory_sale.inventory_id IN :#{@queryUtils.isEmptyList(#inventoryIds) ? @queryUtils.fakeLongs() : #inventoryIds}) " +
            "AND inventory_sale.is_delete = 0"
    )
    List<InventorySale> findAll(List<Long> inventoryIds);
}
