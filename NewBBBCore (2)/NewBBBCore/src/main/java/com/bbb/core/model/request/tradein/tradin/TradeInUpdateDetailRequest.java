package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.request.tradein.customquote.CompRequest;

import java.util.List;

public class TradeInUpdateDetailRequest {
    private Long bicycleId;
    private ConditionInventory condition;
    private Float value;
    private Long bicycleTypeId;
    private List<CompRequest> compRequests;
    private List<Long> upgradeCompIds;
    private Boolean isClean;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Boolean isSave = false;

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public List<CompRequest> getCompRequests() {
        return compRequests;
    }

    public void setCompRequests(List<CompRequest> compRequests) {
        this.compRequests = compRequests;
    }

    public List<Long> getUpgradeCompIds() {
        return upgradeCompIds;
    }

    public void setUpgradeCompIds(List<Long> upgradeCompIds) {
        this.upgradeCompIds = upgradeCompIds;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public Boolean getSave() {
        return isSave;
    }

    public void setSave(Boolean save) {
        isSave = save;
    }
}
