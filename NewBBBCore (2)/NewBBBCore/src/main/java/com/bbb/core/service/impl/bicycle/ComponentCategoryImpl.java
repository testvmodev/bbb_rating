package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.ComponentCategoryManager;
import com.bbb.core.model.request.component.ComponentCategoryCreateRequest;
import com.bbb.core.model.request.component.ComponentCategoryGetRequest;
import com.bbb.core.model.request.component.ComponentCategoryUpdateRequest;
import com.bbb.core.service.bicycle.ComponentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComponentCategoryImpl implements ComponentCategoryService {

    @Autowired
    private ComponentCategoryManager manager;
    @Override
    public Object getComponentCategories(ComponentCategoryGetRequest request) throws ExceptionResponse {
        return manager.getComponentCategories(request);
    }

    @Override
    public Object getDetailComponentCategory(long cateId) throws ExceptionResponse {
        return manager.getDetailComponentCategory(cateId);
    }

    @Override
    public Object createComponentCategory(ComponentCategoryCreateRequest request) throws ExceptionResponse {
        return manager.createComponentCategory(request);
    }

    @Override
    public Object updateComponentCategory(long cateId, ComponentCategoryUpdateRequest request) throws ExceptionResponse {
        return manager.updateComponentCategory(cateId,request);
    }

    @Override
    public Object deleteComponentCategory(long cateId) throws ExceptionResponse {
        return manager.deleteComponentCategory(cateId);
    }


}
