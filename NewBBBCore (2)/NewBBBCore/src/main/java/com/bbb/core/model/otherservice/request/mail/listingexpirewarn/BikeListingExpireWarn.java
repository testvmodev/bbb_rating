package com.bbb.core.model.otherservice.request.mail.listingexpirewarn;

public class BikeListingExpireWarn {
    private String imageUrl;
    private String fullName;

    public BikeListingExpireWarn() {}

    public BikeListingExpireWarn(String imageUrl, String fullName) {
        this.imageUrl = imageUrl;
        this.fullName = fullName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
