package com.bbb.core.model.request.tracking;

import com.bbb.core.model.database.type.TrackingType;

import javax.validation.constraints.NotNull;

public class RecentMarketListingRequest extends RecentRequest{
    @NotNull
    private TrackingType marketType;

    public TrackingType getMarketType() {
        return marketType;
    }

    public void setMarketType(TrackingType marketType) {
        this.marketType = marketType;
    }
}
