package com.bbb.core.repository.component;

import com.bbb.core.model.response.component.BicycleComponentTypeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleComponentTypeDetailRepository extends JpaRepository<BicycleComponentTypeDetail, Long> {
    @Query(nativeQuery = true, value = "SELECT bicycle_component.id AS id, " +
            "bicycle_component.bicycle_id AS bicycle_id, component_type.name AS component_name, " +
            "bicycle_component.component_value AS component_value, " +
            "bicycle_component.component_type_id AS component_type_id " +
            "FROM " +
            "bicycle_component JOIN component_type ON bicycle_component.component_type_id = component_type.id " +
            "WHERE bicycle_component.bicycle_id = :bicycleId")
    List<BicycleComponentTypeDetail> findAllByBicycleId(
            @Param(value = "bicycleId") long bicycleId
    );
}
