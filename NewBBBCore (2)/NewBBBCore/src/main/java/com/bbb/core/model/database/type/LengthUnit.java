package com.bbb.core.model.database.type;

public enum LengthUnit {
    CM("cm"),
    M("m"),
    IN("in");

    private final String value;

    LengthUnit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static LengthUnit findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "cm":
                return CM;
            case "m":
                return M;
            case "in":
                return IN;
            default:
                return null;
        }
    }
}
