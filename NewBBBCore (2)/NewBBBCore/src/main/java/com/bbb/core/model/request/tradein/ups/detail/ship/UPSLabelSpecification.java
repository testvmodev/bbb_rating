package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSLabelSpecification {
    @JsonProperty("LabelImageFormat")
    private LabelImageFormat labelImageFormat;

    public UPSLabelSpecification(LabelImageFormat imageFormat) {
        this.labelImageFormat = imageFormat;
    }

    public LabelImageFormat getLabelImageFormat() {
        return labelImageFormat;
    }

    public void setLabelImageFormat(LabelImageFormat labelImageFormat) {
        this.labelImageFormat = labelImageFormat;
    }
}
