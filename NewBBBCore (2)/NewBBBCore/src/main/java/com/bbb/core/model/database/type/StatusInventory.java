package com.bbb.core.model.database.type;


public enum StatusInventory {
    IN_ACTIVE("In-Active"),
    ACTIVE("Active"),
    SOLD("Sold"),
    DECLINED("Declined"),
    PENDING("Pending"),
    NONE_COMPLIANT("Non-Compliant"),
    RETURNED("Returned"),
    RESERVED("Reserved");
    private String value;

    StatusInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StatusInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (StatusInventory statusInventory : StatusInventory.values()) {
            if (statusInventory.getValue().equals(value)) {
                return statusInventory;
            }
        }
        return null;
    }
}
