package com.bbb.core.service.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bid.offer.*;
import com.bbb.core.model.request.bid.offer.offerbid.OfferBidRequest;
import com.bbb.core.model.request.bid.offer.offerbid.OffersReceiverRequest;
import com.bbb.core.model.request.bid.offer.test.OfferType;
import com.bbb.core.model.request.sort.OfferBidFieldSort;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OfferService {
    Object getOffers(OfferRequest request, Pageable page);

    Object createOffer(OfferCreateRequest request) throws ExceptionResponse;

    Object updateOffer(long offerId, OfferUpdateRequest request) throws ExceptionResponse;

    Object getDetailOffer(long offerId) throws ExceptionResponse;

    Object payOfferAccepted(long offerId, boolean isPaid) throws ExceptionResponse;

    Object getMarketListingAuctionsService(List<SecretMarketLisitingAuctionRequest> requests) throws ExceptionResponse;

    Object getOffersBid(OfferBidRequest request, Pageable page) throws ExceptionResponse;

    Object getOfferBid(long inventoryAuctionId, OfferBidFieldSort fieldSor, Sort sortType) throws ExceptionResponse;

    Object updateOfferBuyer(long offerId, boolean isAccepted) throws ExceptionResponse;


    Object getOffersReceiverMyListing(OffersReceiverRequest request, Pageable page) throws ExceptionResponse;

    Object getOffersOnlineStore(OffersOnlineStoreRequest request) throws ExceptionResponse;

    Object getOffersMarketListing(long marketListingId) throws ExceptionResponse;

//    Object getMarketListingAuctionsServiceDetail(SecretMarketLisitingAuctionRequest request) throws ExceptionResponse;

    Object getInventoryAuctionBids(
        long inventoryAuctionId,
        OfferBidFieldSort offerBidFieldSort, Sort sortType
    ) throws ExceptionResponse;

    Object offerIntegrationTest(OfferType offerType) throws ExceptionResponse;

    Object removeCartOffer(long offerId) throws ExceptionResponse;

    Object getUserOffers(UserOfferRequest request) throws ExceptionResponse;

    Object getUserBids(UserBidRequest request) throws ExceptionResponse;

    Object getOfferDetailSecret(long offerId) throws ExceptionResponse;
}
