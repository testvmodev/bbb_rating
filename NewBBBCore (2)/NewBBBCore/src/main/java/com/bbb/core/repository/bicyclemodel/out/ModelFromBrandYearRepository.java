package com.bbb.core.repository.bicyclemodel.out;

import com.bbb.core.model.response.model.ModelFromBrandYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelFromBrandYearRepository extends JpaRepository<ModelFromBrandYear, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "model.model_id AS id,  bicycle_model.name AS name, model.bicycle_id AS bicycle_id, " +
            "model.bicycle_name AS bicycle_name, model.bicycle_image_default AS bicycle_image_default," +
            "model.year_id AS year_id, bicycle_year.name AS year_name " +
            "FROM " +
            "(SELECT bicycle.model_id AS model_id, bicycle.id AS bicycle_id, bicycle.name AS bicycle_name, bicycle.image_default AS bicycle_image_default, bicycle.year_id AS year_id " +
            "FROM " +
            "bicycle " +
            "WHERE " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.year_id = :yearId AND " +
            "bicycle.is_delete = 0 AND bicycle.active = 1 ) AS model " +

            "JOIN bicycle_model ON model.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON model.year_id = bicycle_year.id " +
            "WHERE bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1 AND bicycle_year.is_delete = 0 AND bicycle_year.is_approved = 1"
    )
    List<ModelFromBrandYear> findAllByBrandYear(
            @Param(value = "brandId") long brandId,
            @Param(value = "yearId") Long yearId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "model.model_id AS id,  bicycle_model.name AS name, model.bicycle_id AS bicycle_id, model.bicycle_name AS bicycle_name, " +
            "model.bicycle_image_default AS bicycle_image_default," +
            "model.year_id AS year_id, bicycle_year.name AS year_name " +
            "FROM " +
            "(SELECT bicycle.model_id AS model_id, bicycle.id AS bicycle_id, bicycle.name AS bicycle_name, bicycle.image_default AS bicycle_image_default, bicycle.year_id AS year_id " +
            "FROM " +
            "bicycle " +
            "WHERE " +
            "bicycle.brand_id = :brandId AND " +
            "(:yearId IS NULL OR bicycle.year_id = :yearId) AND " +
            "bicycle.is_delete = 0 AND bicycle.active = 1 ) AS model " +
            "JOIN bicycle_model ON model.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON model.year_id = bicycle_year.id " +
            "WHERE bicycle_model.name LIKE :familyName AND "+
            "bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1 AND bicycle_year.is_delete = 0 AND bicycle_year.is_approved = 1"
    )
    List<ModelFromBrandYear> findAllByBrandYear(
            @Param(value = "brandId") long brandId,
            @Param(value = "yearId") Long yearId,
            @Param(value = "familyName") String familyName
    );

}
