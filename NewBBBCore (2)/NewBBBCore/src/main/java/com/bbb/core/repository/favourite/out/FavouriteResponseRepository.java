package com.bbb.core.repository.favourite.out;

import com.bbb.core.model.response.favourite.FavouriteResponse;
import com.bbb.core.model.response.market.marketlisting.MarketListingResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FavouriteResponseRepository extends JpaRepository<FavouriteResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "favourite.id AS favourite_id, " +
            "favourite.favourite_type AS favourite_type, " +
            "market_listing.id AS market_listing_id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "bicycle_model.id  AS model_id, " +
            "bicycle_brand.id AS brand_id, " +
            "bicycle_year.id AS year_id, " +

            "bicycle.name AS bicycle_name, " +
            "bicycle_model.name AS bicycle_model_name, " +
            "bicycle_brand.name AS bicycle_brand_name, " +
            "bicycle_year.name AS bicycle_year_name, " +
            "bicycle_type.name AS bicycle_type_name, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "market_listing.market_place_id AS market_place_id, " +
            "market_listing.status AS status_market_listing, " +
            "market_place.type AS market_type, " +

            "inventory.image_default AS image_default, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.name AS name,  inventory.type_id AS type_inventory_id, " +
            "inventory_type.id AS type_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "inventory.condition AS condition, " +
            "inventory.status AS status_inventory, " +
            "inventory_size.inventory_id AS size_id, " +
            "inventory.bicycle_size_name AS size_name " +

            "FROM favourite " +

            "LEFT JOIN market_listing " +
                "ON favourite.market_listing_id = market_listing.id " +
                "AND market_listing.is_delete = 0 " +
            "LEFT JOIN inventory_auction " +
                "ON favourite.inventory_auction_id = inventory_auction.id " +
                "AND inventory_auction.is_delete = 0 " +
                "AND inventory_auction.is_inactive = 0 " +

            "JOIN inventory " +
                "ON (favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).MARKET_LIST.getValue()} " +
                    "AND market_listing.inventory_id = inventory.id) " +
                "OR (favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).AUCTION.getValue()} " +
                    "AND inventory_auction.inventory_id = inventory.id) " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +

            "LEFT JOIN market_place ON market_listing.market_place_id = market_place.id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) " +
                "AS inventory_size " +
                "ON inventory.id = inventory_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) " +
                "AS brake_type " +
                "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) " +
                "AS frame_material " +
                "ON inventory.id = frame_material.inventory_id " +

            "WHERE favourite.is_delete = 0 " +
            "AND favourite.user_id = :userId " +

            "ORDER BY favourite.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<FavouriteResponse> findAllFavourite(
            @Param("frameMaterialId") long frameMaterialId,
            @Param("brakeTypeId") long brakeTypeId,
            @Param("sizeId") long sizeId,

            @Param("userId") String userId,
            @Param("offset") int offset,
            @Param("size") int size
    );
}
