package com.bbb.core.model.database;

import com.bbb.core.common.Constants;
import com.bbb.core.model.database.type.StatusOffer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "offer")
public class Offer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private String buyerId;
  @Column(columnDefinition = Constants.NUMERIC)
  private float offerPrice;
  private StatusOffer status;
  @CreatedDate
  @Generated(GenerationTime.INSERT)
  private LocalDateTime createdTime;
  @LastModifiedDate
  @Generated(GenerationTime.ALWAYS)
  private LocalDateTime lastUpdate;
  private boolean isDelete;
  private boolean isPaid;
  private Long inventoryAuctionId;
  private Long marketListingId;
  private String reasonCancel;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public void setBuyerId(String buyerId) {
    this.buyerId = buyerId;
  }

  public float getOfferPrice() {
    return offerPrice;
  }

  public void setOfferPrice(float offerPrice) {
    this.offerPrice = offerPrice;
  }

  public StatusOffer getStatus() {
    return status;
  }

  public void setStatus(StatusOffer status) {
    this.status = status;
  }

  public LocalDateTime getCreatedTime() {
    return createdTime;
  }

  @Generated(GenerationTime.INSERT)
  public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
  }

  public LocalDateTime getLastUpdate() {
    return lastUpdate;
  }

  @Generated(GenerationTime.ALWAYS)
  public void setLastUpdate(LocalDateTime lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  @JsonIgnore
  public boolean isDelete() {
    return isDelete;
  }

  public void setDelete(boolean delete) {
    isDelete = delete;
  }

//  @Deprecated
//  public Long getAuctionId() {
//    return auctionId;
//  }

//  @Deprecated
//  public void setAuctionId(Long auctionId) {
//    this.auctionId = auctionId;
//  }

  public boolean getPaid() {
    return isPaid;
  }

  public void setPaid(boolean paid) {
    isPaid = paid;
  }

  public Long getInventoryAuctionId() {
    return inventoryAuctionId;
  }

  public void setInventoryAuctionId(Long inventoryAuctionId) {
    this.inventoryAuctionId = inventoryAuctionId;
  }

  public Long getMarketListingId() {
    return marketListingId;
  }

  public void setMarketListingId(Long marketListingId) {
    this.marketListingId = marketListingId;
  }

  public boolean isPaid() {
    return isPaid;
  }

  public String getReasonCancel() {
    return reasonCancel;
  }

  public void setReasonCancel(String reasonCancel) {
    this.reasonCancel = reasonCancel;
  }
}
