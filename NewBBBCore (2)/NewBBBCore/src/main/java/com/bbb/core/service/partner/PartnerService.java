package com.bbb.core.service.partner;

import com.bbb.core.model.request.partner.PartnerInfoRequest;
import org.springframework.http.HttpStatus;

public interface PartnerService {
    Object updateInfoPartner(String partnerId, PartnerInfoRequest request);
}
