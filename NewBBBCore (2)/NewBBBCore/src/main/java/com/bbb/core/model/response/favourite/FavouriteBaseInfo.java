package com.bbb.core.model.response.favourite;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FavouriteBaseInfo {
    @Id
    private long favouriteId;
    private Long marketListingId;
    private Long inventoryAuctionId;

    public long getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(long favouriteId) {
        this.favouriteId = favouriteId;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }
}
