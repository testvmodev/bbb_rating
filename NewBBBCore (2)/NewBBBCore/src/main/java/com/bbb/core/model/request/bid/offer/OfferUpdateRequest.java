package com.bbb.core.model.request.bid.offer;

import com.bbb.core.model.database.type.StatusOffer;

import javax.validation.constraints.NotNull;

public class OfferUpdateRequest {
    @NotNull
    private StatusOffer status;
    private Float offerPrice;
    private String reasonCancel;

    public StatusOffer getStatus() {
        return status;
    }

    public void setStatus(StatusOffer status) {
        this.status = status;
    }

    public Float getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Float offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getReasonCancel() {
        return reasonCancel;
    }

    public void setReasonCancel(String reasonCancel) {
        this.reasonCancel = reasonCancel;
    }
}
