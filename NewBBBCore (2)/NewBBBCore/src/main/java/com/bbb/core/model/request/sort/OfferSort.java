package com.bbb.core.model.request.sort;

public enum  OfferSort {
    CREATED_DATE, TITLE, INVENTORY_NAME, SELLER_ID, OFFER_PRICE, CURRENT_LISTED_PRICE, OFFER_STATUS, OFFER_TOTAL_BID
}
