package com.bbb.core.model.response.bicycle;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BicycleBaseInfo {
    @Id
    private Long bicycleId;
    private Long bicycleModelId;
    private String bicycleModelName;
    private Long bicycleBrandId;
    private String bicycleBrandName;
    private Long bicycleYearId;
    private String bicycleYearName;
    //now using bicycle type from partner input, not original bicycle
    @JsonIgnore
    private long bicycleTypeId;
    @JsonIgnore
    private String bicycleTypeName;

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public Long getBicycleModelId() {
        return bicycleModelId;
    }

    public void setBicycleModelId(Long bicycleModelId) {
        this.bicycleModelId = bicycleModelId;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public Long getBicycleBrandId() {
        return bicycleBrandId;
    }

    public void setBicycleBrandId(Long bicycleBrandId) {
        this.bicycleBrandId = bicycleBrandId;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public Long getBicycleYearId() {
        return bicycleYearId;
    }

    public void setBicycleYearId(Long bicycleYearId) {
        this.bicycleYearId = bicycleYearId;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }
}
