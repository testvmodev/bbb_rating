package com.bbb.core.repository.valueguide;

import com.bbb.core.model.database.PrivatePartyValueModifier;
import com.bbb.core.model.database.type.ConditionInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrivatePartyValueModifierRepository extends JpaRepository<PrivatePartyValueModifier, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM condition"
    )
    List<PrivatePartyValueModifier> findAll();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM condition " +
            "WHERE name = :#{#condition.getValue()}"
    )
    PrivatePartyValueModifier findOne(
            @Param("condition") ConditionInventory condition
    );
}
