package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "inventory_spec")
public class InventorySpec {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long inventoryId;
    private String name;
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
