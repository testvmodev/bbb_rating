package com.bbb.core.model.request.saleforce;

import com.bbb.core.model.request.saleforce.update.SalesforceUpdateBikeRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ListingFullUpdateRequest {
    @JsonProperty("UpdateBikeRequest")
    @NotNull
    private SalesforceUpdateBikeRequest updateBikeRequest;

    public SalesforceUpdateBikeRequest getUpdateBikeRequest() {
        return updateBikeRequest;
    }

    public void setUpdateBikeRequest(SalesforceUpdateBikeRequest updateBikeRequest) {
        this.updateBikeRequest = updateBikeRequest;
    }
}
