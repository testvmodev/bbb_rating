package com.bbb.core.service.impl.tracking;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.tracking.TrackingManager;
import com.bbb.core.model.request.tracking.RecentMarketListingRequest;
import com.bbb.core.model.request.tracking.RecentRequest;
import com.bbb.core.security.user.UserContext;
import com.bbb.core.service.tracking.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackingServiceImpl implements TrackingService {
    @Autowired
    private TrackingManager manager;
    @Override
    public Object getDeals() throws ExceptionResponse {
        return manager.getDeals();
    }


    @Override
    public Object getRecentTrackings(List<RecentMarketListingRequest> recent) throws ExceptionResponse {
        return manager.getRecentView( recent);
    }

    @Override
    public Object getBicycleRecentTrackings(List<RecentRequest> recent) throws ExceptionResponse {
        return manager.getBicycleRecentTrackings(recent);
    }
}
