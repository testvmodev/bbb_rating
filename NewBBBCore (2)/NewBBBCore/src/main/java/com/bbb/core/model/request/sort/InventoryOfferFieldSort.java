package com.bbb.core.model.request.sort;

public enum  InventoryOfferFieldSort {
    ID,
    TITLE,
    TIME_LEFT,
    MAX_BID_PRICE,
    TOTAL_BID_USER,
    CREATED_TIME
}
