package com.bbb.core.service.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionCreateRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionUpdateRequest;
import com.bbb.core.security.user.UserContext;
import org.springframework.data.domain.Pageable;

public interface InventoryAuctionService {
    Object getInventoryAuctions(InventoryAuctionRequest request, Pageable page) throws ExceptionResponse;

    Object createInventoryAuction(InventoryAuctionCreateRequest request) throws ExceptionResponse;

    Object updateInventoryAuction(long inventoryAuctionId, InventoryAuctionUpdateRequest request) throws ExceptionResponse;

    Object getInventoryAuction(long inventoryAuctionId) throws ExceptionResponse;

    Object getInventoryAuctionAllBid(long inventoryAuctionId) throws ExceptionResponse;
}
