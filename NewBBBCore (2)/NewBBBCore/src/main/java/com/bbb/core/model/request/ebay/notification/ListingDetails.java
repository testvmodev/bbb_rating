package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class ListingDetails {
    @JacksonXmlProperty(localName = "Adult")
    private boolean adult;
    @JacksonXmlProperty(localName = "BindingAuction")
    private boolean bindingAuction;
    @JacksonXmlProperty(localName = "CheckoutEnabled")
    private boolean checkoutEnabled;
    @JacksonXmlProperty(localName = "ConvertedBuyItNowPrice")
    private Currency convertedBuyItNowPrice;
    @JacksonXmlProperty(localName = "ConvertedStartPrice")
    private Currency convertedStartPrice;
    @JacksonXmlProperty(localName = "convertedReservePrice")
    private Currency convertedReservePrice;
    @JacksonXmlProperty(localName = "HasReservePrice")
    private boolean hasReservePrice;
    @JacksonXmlProperty(localName = "StartTime")
    private LocalDateTime startTime;
    @JacksonXmlProperty(localName = "EndTime")
    private LocalDateTime endTime;
    @JacksonXmlProperty(localName = "ViewItemURL")
    private String viewItemURL;
    @JacksonXmlProperty(localName = "HasUnansweredQuestions")
    private String hasUnansweredQuestions;
    @JacksonXmlProperty(localName = "HasPublicMessages")
    private String hasPublicMessages;
    @JacksonXmlProperty(localName = "ViewItemURLForNaturalSearch")
    private String viewItemURLForNaturalSearch;

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public boolean isBindingAuction() {
        return bindingAuction;
    }

    public void setBindingAuction(boolean bindingAuction) {
        this.bindingAuction = bindingAuction;
    }

    public boolean isCheckoutEnabled() {
        return checkoutEnabled;
    }

    public void setCheckoutEnabled(boolean checkoutEnabled) {
        this.checkoutEnabled = checkoutEnabled;
    }

    public Currency getConvertedBuyItNowPrice() {
        return convertedBuyItNowPrice;
    }

    public void setConvertedBuyItNowPrice(Currency convertedBuyItNowPrice) {
        this.convertedBuyItNowPrice = convertedBuyItNowPrice;
    }

    public Currency getConvertedStartPrice() {
        return convertedStartPrice;
    }

    public void setConvertedStartPrice(Currency convertedStartPrice) {
        this.convertedStartPrice = convertedStartPrice;
    }

    public Currency getConvertedReservePrice() {
        return convertedReservePrice;
    }

    public void setConvertedReservePrice(Currency convertedReservePrice) {
        this.convertedReservePrice = convertedReservePrice;
    }

    public boolean isHasReservePrice() {
        return hasReservePrice;
    }

    public void setHasReservePrice(boolean hasReservePrice) {
        this.hasReservePrice = hasReservePrice;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getViewItemURL() {
        return viewItemURL;
    }

    public void setViewItemURL(String viewItemURL) {
        this.viewItemURL = viewItemURL;
    }

    public String getHasUnansweredQuestions() {
        return hasUnansweredQuestions;
    }

    public void setHasUnansweredQuestions(String hasUnansweredQuestions) {
        this.hasUnansweredQuestions = hasUnansweredQuestions;
    }

    public String getHasPublicMessages() {
        return hasPublicMessages;
    }

    public void setHasPublicMessages(String hasPublicMessages) {
        this.hasPublicMessages = hasPublicMessages;
    }

    public String getViewItemURLForNaturalSearch() {
        return viewItemURLForNaturalSearch;
    }

    public void setViewItemURLForNaturalSearch(String viewItemURLForNaturalSearch) {
        this.viewItemURLForNaturalSearch = viewItemURLForNaturalSearch;
    }
}
