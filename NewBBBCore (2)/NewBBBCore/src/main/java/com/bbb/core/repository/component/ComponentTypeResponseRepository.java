package com.bbb.core.repository.component;

import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.component.ComponentTypeResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ComponentTypeResponseRepository extends JpaRepository<ComponentTypeResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "component_type.*, " +
            "component_category.name AS component_category_name " +

            "FROM component_type " +
            "JOIN component_category ON component_type.component_category_id = component_category.id " +
            "WHERE component_type.is_delete = 0 " +
            "AND component_category.is_delete = 0 " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type.name END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type.name END DESC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).CATEGORY_NAME.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_category.name END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).CATEGORY_NAME.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_category.name END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<ComponentTypeResponse> findAll(
            @Param("sortField") ComponentTypeSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "component_type.*," +
            "component_category.name AS component_category_name " +
            "FROM component_type " +
            "JOIN component_category ON component_type.component_category_id = component_category.id " +
            "WHERE component_type.is_delete = 0 " +
            "AND component_category.is_delete = 0 " +
            "AND component_type.id = :typeId ")
    ComponentTypeResponse findOneDetail(@Param("typeId") long typeId);
}
