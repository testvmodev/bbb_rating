package com.bbb.core.controller.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.BicycleMigrationManager;
import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.request.bicycle.BicycleImportRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BicycleMigrationController {
    @Autowired
    private BicycleMigrationManager bicycleMigrationManager;

//    @PostMapping("/secret/api/bicycle/import")
    public ResponseEntity importBicycles(
            String urlBicycle,
            String urlImage,
            String urlSE
    ) throws ExceptionResponse {
        BicycleImportRequest request = new BicycleImportRequest();
        request.setUrlBicycle(urlBicycle);
        request.setUrlImage(urlImage);
        request.setUrlCompSpec(urlSE);

        return ResponseEntity.ok(bicycleMigrationManager.importBicycles(request));
    }

    @GetMapping("/secret/api/bicycle/comp/spec/import")
    public ResponseEntity statusImportCompSpecFromSE() throws ExceptionResponse {
        return ResponseEntity.ok(bicycleMigrationManager.statusImportCompSpec());
    }

    @PostMapping("/secret/api/bicycle/comp/spec/import")
    public ResponseEntity importCompSpecFromSmartEtailing(
            @RequestParam String urlBicycle, @RequestParam String urlSE,
            @RequestParam(defaultValue = "0") int skipRecord
    ) throws ExceptionResponse {
        return ResponseEntity.ok(bicycleMigrationManager.importCompSpec(urlBicycle, urlSE, skipRecord));
    }

    @DeleteMapping("/secret/api/bicycle/comp/spec/import")
    public ResponseEntity stopImportCompSpecFromSE() throws ExceptionResponse {
        return new ResponseEntity(bicycleMigrationManager.stopImportCompSpec(), HttpStatus.ACCEPTED);
    }
}
