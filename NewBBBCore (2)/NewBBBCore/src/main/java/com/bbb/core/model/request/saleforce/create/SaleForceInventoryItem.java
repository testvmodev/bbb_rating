package com.bbb.core.model.request.saleforce.create;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SaleForceInventoryItem {
    @JsonProperty("SalesforceId")
    private String salesforceId;
    @JsonProperty("InventoryName")
    private String inventoryName;
    @JsonProperty("Size")
    private String size;

    public String getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(String salesforceId) {
        this.salesforceId = salesforceId;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
