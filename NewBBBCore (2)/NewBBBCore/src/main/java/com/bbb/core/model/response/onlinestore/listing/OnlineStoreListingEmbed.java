package com.bbb.core.model.response.onlinestore.listing;

import com.bbb.core.model.database.InventorySale;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class OnlineStoreListingEmbed {
    private Long marketListingId;
    private Long inventoryId;
    private Long bicycleId;
//    private Long modelId;
//    private Long brandId;
//    private Long yearId;
    private Long bicycleTypeId;

    private String sizeName;
    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;

    private StatusInventory statusInventory;
    private StageInventory stageInventory;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private ConditionInventory condition;
    private String serialNumber;

    private int views;
    private int offers;

    @Transient
    private InventorySale sale;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

//    public Long getModelId() {
//        return modelId;
//    }
//
//    public void setModelId(Long modelId) {
//        this.modelId = modelId;
//    }
//
//    public Long getBrandId() {
//        return brandId;
//    }
//
//    public void setBrandId(Long brandId) {
//        this.brandId = brandId;
//    }
//
//    public Long getYearId() {
//        return yearId;
//    }
//
//    public void setYearId(Long yearId) {
//        this.yearId = yearId;
//    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public StatusInventory getStatusInventory() {
        return statusInventory;
    }

    public void setStatusInventory(StatusInventory statusInventory) {
        this.statusInventory = statusInventory;
    }

    public StageInventory getStageInventory() {
        return stageInventory;
    }

    public void setStageInventory(StageInventory stageInventory) {
        this.stageInventory = stageInventory;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getOffers() {
        return offers;
    }

    public void setOffers(int offers) {
        this.offers = offers;
    }

    public InventorySale getSale() {
        return sale;
    }

    public void setSale(InventorySale sale) {
        this.sale = sale;
    }
}
