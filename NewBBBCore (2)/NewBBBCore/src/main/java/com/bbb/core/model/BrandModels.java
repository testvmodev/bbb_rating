package com.bbb.core.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bicycle_brand")
public class BrandModels {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "brandModel")
    private List<ModelBrand> modelBrands;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ModelBrand> getModelBrands() {
        return modelBrands;
    }

    public void setModelBrands(List<ModelBrand> modelBrands) {
        this.modelBrands = modelBrands;
    }
}
