package com.bbb.core.security.authen.jwt;

import com.bbb.core.security.user.UserContext;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.*;


/**
 * Created by ducnd on 6/25/17.
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {
    private UserContext userContext;

    public JwtAuthenticationToken(UserContext userContext) {
        super(null);

        this.userContext = userContext;
        this.setAuthenticated(false);
    }


    public JwtAuthenticationToken(UserContext userContext, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.userContext = userContext;
    }

    @Override
    public UserContext getCredentials() {
        return userContext;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    public boolean isEmpty() {
        return userContext == null;
    }

}
