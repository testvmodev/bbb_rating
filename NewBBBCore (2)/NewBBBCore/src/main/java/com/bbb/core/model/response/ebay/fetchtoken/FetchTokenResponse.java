package com.bbb.core.model.response.ebay.fetchtoken;

import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.joda.time.LocalDateTime;

@JacksonXmlRootElement(localName = "FetchTokenResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
public class FetchTokenResponse extends BaseEbayResponse {
    @JacksonXmlProperty(localName = "eBayAuthToken")
    private String eBayAuthToken;
    @JacksonXmlProperty(localName = "HardExpirationTime")
    private LocalDateTime hardExpirationTime;

    public String geteBayAuthToken() {
        return eBayAuthToken;
    }

    public void seteBayAuthToken(String eBayAuthToken) {
        this.eBayAuthToken = eBayAuthToken;
    }

    public LocalDateTime getHardExpirationTime() {
        return hardExpirationTime;
    }

    public void setHardExpirationTime(LocalDateTime hardExpirationTime) {
        this.hardExpirationTime = hardExpirationTime;
    }
}
