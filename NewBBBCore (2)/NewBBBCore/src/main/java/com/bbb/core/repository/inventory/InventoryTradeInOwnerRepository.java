package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryTradeInOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface InventoryTradeInOwnerRepository extends JpaRepository<InventoryTradeInOwner, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM inventory_trade_in_owner " +
            "WHERE inventory_id = :inventoryId")
    InventoryTradeInOwner findByInventoryId(@Param("inventoryId") long inventoryId);
}
