package com.bbb.core.manager.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.BicycleType;
import com.bbb.core.model.request.bicycletype.BicycleTypeGetRequest;
import com.bbb.core.model.request.bicycletype.TypeCreateRequest;
import com.bbb.core.model.request.bicycletype.TypeUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

@Component
public class TypeManager implements MessageResponses {
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private ComponentTypeRepository componentTypeRepository;

    public Object getBicycleType(BicycleTypeGetRequest request) throws ExceptionResponse{
        if (request.getPageable().getPageSize()<=0||request.getPageable().getPageNumber()<0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, PAGE_OR_SIZE_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        ListObjResponse<BicycleType> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(bicycleTypeRepository.countAll(
                request.getName() == null ? null : "%" + request.getName() + "%"
        ));
        if (response.getTotalItem() > 0) {
            response.setData(bicycleTypeRepository.findAllByName(
                    request.getName() == null ? null : "%" + request.getName() + "%",
                    request.getSortField(),
                    request.getSortType(),
                    request.getPageable()
            ));
        }
        return response;
    }

    public Object getDetailBicycleType(long typeId) throws ExceptionResponse{
        BicycleType bicycleType = bicycleTypeRepository.findOne(typeId);
        if (bicycleType == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, TYPE_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        return bicycleType;
    }

    public Object createType(TypeCreateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        if (StringUtils.isBlank(request.getName())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, TYPE_NAME_NOT_NULL),
                    HttpStatus.NOT_FOUND);
        }
        BicycleType bicycleType = new BicycleType();
        bicycleType.setName(request.getName().trim());
        if (request.getValueModifier() == null) {
            bicycleType.setValueModifier(0);
        } else {
            if (request.getValueModifier() < 0 || request.getValueModifier() > 100) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, VALUE_MODIFIER_INVALID),
                        HttpStatus.BAD_REQUEST);
            }
            DecimalFormat format = new DecimalFormat("##.##");
            bicycleType.setValueModifier(Float.valueOf(
                    format.format(request.getValueModifier())));
        }
        bicycleType.setLastUpdate(LocalDateTime.now());
        bicycleType.setImageDefault(request.getImageDefault());
        bicycleType.setSortOrder(0);

        return bicycleTypeRepository.save(bicycleType);
    }

    public Object updateType(long typeId, TypeUpdateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleType bicycleType = bicycleTypeRepository.findOne(typeId);
        if (bicycleType == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, TYPE_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        if (!StringUtils.isBlank(request.getName())){
            if (!request.getName().equals(bicycleType.getName())){
                if (bicycleTypeRepository.findOneByName(request.getName()) != null){
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, TYPE_NAME_IS_EXIST),
                            HttpStatus.NOT_FOUND);
                }
                bicycleType.setName(request.getName());
            }
        }
        if (request.getImageDefault() != null) {
            bicycleType.setImageDefault(request.getImageDefault());
        }
        if (request.getValueModifier() != null){
            bicycleType.setValueModifier(request.getValueModifier());
        }
        if (request.getSortOrder() != 0L){
            bicycleType.setSortOrder(request.getSortOrder());
        }
        return bicycleTypeRepository.save(bicycleType);
    }

    public Object deleteType(long typeId) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleType bicycleType = bicycleTypeRepository.findOne(typeId);
        if (bicycleType == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TYPE_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        int countType = bicycleRepository.getCountType(bicycleType.getId());
        if (countType > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, (countType==1?THERE_IS:THERE_ARE+ countType )+ BICYCLE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        int countComponentType = componentTypeRepository.getCountByType(typeId);
        if (countComponentType > 0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, (countComponentType==1?THERE_IS:THERE_ARE+ countComponentType )+ COMPONENT_TYPE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        bicycleType.setDelete(true);
        bicycleTypeRepository.save(bicycleType);
        return new com.bbb.core.model.response.MessageResponse(SUCCESS);
    }
}
