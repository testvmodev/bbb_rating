package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ReviseStatus {
    @JacksonXmlProperty(localName = "ItemRevised")
    private boolean itemRevised;

    public boolean isItemRevised() {
        return itemRevised;
    }

    public void setItemRevised(boolean itemRevised) {
        this.itemRevised = itemRevised;
    }
}
