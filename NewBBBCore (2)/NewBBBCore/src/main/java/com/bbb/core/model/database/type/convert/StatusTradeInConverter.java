//package com.bbb.core.model.database.type.convert;
//
//import com.bbb.core.model.database.type.StatusTradeIn;
//
//import javax.persistence.AttributeConverter;
//import javax.persistence.Converter;
//
//@Converter(autoApply = true)
//public class StatusTradeInConverter implements AttributeConverter<StatusTradeIn, String> {
//    @Override
//    public String convertToDatabaseColumn(StatusTradeIn statusTradeIn) {
//        if ( statusTradeIn == null){
//            return null;
//        }
//        return statusTradeIn.getValue();
//    }
//
//    @Override
//    public StatusTradeIn convertToEntityAttribute(String value) {
//        return StatusTradeIn.findByValue(value);
//    }
//}
