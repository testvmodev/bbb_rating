package com.bbb.core.common.utils;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.time.*;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.BicycleSpec;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.TradeIn;
import com.bbb.core.model.database.type.UserRoleType;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bicycle.BicycleSpecResponse;
import com.bbb.core.model.response.ebay.Error;
import com.bbb.core.security.user.UserContext;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.YearMonth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class CommonUtils {
    private static final Logger LOG = LoggerFactory.getLogger(CommonUtils.class);

    public static <T> void stream(List<T> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) == null) {
                    if (list.get(j) == null) {
                        list.remove(j);
                        j--;
                    }
                } else if (list.get(i).equals(list.get(j))) {
                    list.remove(j);
                    j--;
                }
            }
        }
    }

    public static <T> void stream(List<T> list, Compare<T, T> comp) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (comp.compare(list.get(i), list.get(j)) == 0) {
                    list.remove(j);
                    j--;
                }
            }
        }
    }

    public static boolean isLogined() {
        return SecurityContextHolder.getContext() != null &&
                SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null &&
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserContext &&
                ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId() != null;
    }

    public static String getUserLogin() {
        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
    }

    public static String getPartnerId() {
//        Partner partner = ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPartner();
//        if (partner == null) {
//            return null;
//        }
//        return partner.getId();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) return null;
        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPartnerId();
    }

    public static String getPartnerAddress(UserDetailFullResponse userDetail) {
//        Partner partner = ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPartner();
        if (userDetail == null || userDetail.getPartner() == null) {
            return null;
        }
        return userDetail.getPartner().getAddress();
    }

    public static String getPartnerName(UserDetailFullResponse userDetail) {
//        Partner partner = ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPartner();
        if (userDetail == null || userDetail.getPartner() == null) {
            return null;
        }
        return userDetail.getPartner().getName();
    }

    public static String getUserToken() {
        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getToken();
    }

    public static String getOnlineStoreId() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null || !(principal instanceof UserContext)) return null;

        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getOnlineStoreId();
    }

    public static String getStorefrontId() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null || !(principal instanceof UserContext)) return null;

        return ((UserContext) principal).getStorefrontId();
    }

    public static UserRoleType getUserPrimaryRole() {
        if (SecurityContextHolder.getContext() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            return null;
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(principal instanceof UserContext)) return null;

        return ((UserContext) principal).getUserRoles().get(0);
    }

    public static List<UserRoleType> getUserRoles() {
        if (SecurityContextHolder.getContext() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            return null;
        }

        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserRoles();
    }

    public static boolean checkHaveAnyRole(List<UserRoleType> checkRoles) {
        return checkHaveAnyRole(CommonUtils.getUserRoles(), checkRoles);
    }

    public static boolean checkHaveAnyRole(List<UserRoleType> userRoles, List<UserRoleType> checkRoles) {
        if (userRoles == null || userRoles.size() < 1) {
            return false;
        }
        if (checkRoles == null || checkRoles.size() < 1) {
            return true;
        }
        for (UserRoleType role : checkRoles) {
            if (checkHaveRole(userRoles, role)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkHaveRole(UserRoleType checkRole) {
        return checkHaveRole(CommonUtils.getUserRoles(), checkRole);
    }

    public static boolean checkHaveRole(List<UserRoleType> userRoles, UserRoleType checkRole) {
        if (userRoles == null || userRoles.size() < 1) {
            return false;
        }
        if (checkRole == null) {
            return true;
        }
        if (userRoles.contains(checkRole)) {
            return true;
        }
        return false;
    }

    public static boolean checkRoleAccessFromAdmin(List<UserRoleType> roles) {
        return roles != null && roles.size() > 0 && (roles.contains(UserRoleType.ADMIN) || roles.contains(UserRoleType.ROOT));
    }

    public static String getUserEmail() {
        if (SecurityContextHolder.getContext() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            return null;
        }

        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail();
    }

    public static String getUserDisplayName() {
        if (SecurityContextHolder.getContext() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            return null;
        }

        return ((UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getDisplayName();
    }

    public static String getKeyHash() {
        return new MessageDigestPasswordEncoder("MD5").encode(Date.from(Instant.now()).toString() + Constants.MD5_SALT)
//        return new Md5PasswordEncoder().encodePassword(Date.from(Instant.now()).toString(), Constants.MD5_SALT)
                + RandomStringUtils.random(3, "adf@slaskdjg@349556fhdfpaosdfhahfklashdflkashdflkasjdhf");
    }

    public static String getKeyHashEndNameFileImage() {
        String key = new MessageDigestPasswordEncoder("MD5").encode(Date.from(Instant.now()).toString() + Constants.MD5_SALT)
//                new Md5PasswordEncoder().encodePassword(Date.from(Instant.now()).toString(), Constants.MD5_SALT)
                + RandomStringUtils.random(3, "adf@slaskdjg@349556fhdfpaosdfhahfklashdflkashdflkasjdhf");
        if (key.length() <= 6) {
            return key;
        } else {
            int start = key.length() - 6;
            return key.substring(start);
        }
    }

    public static BicycleSpecResponse convertToSpecResponse(BicycleSpec sp) {
        BicycleSpecResponse bicycleSpecResponse = new BicycleSpecResponse();
        bicycleSpecResponse.setSpecId(sp.getId());
        bicycleSpecResponse.setSpecName(sp.getName());
        bicycleSpecResponse.setSpecValue(sp.getValue());
        return bicycleSpecResponse;
    }

    public static <T> T findObject(List<T> list, Action1Result<T, Boolean> actionCheck) {
        for (T t : list) {
            if (actionCheck.call(t)) {
                return t;
            }
        }
        return null;
    }

    public static <T> List<T> findObjects(List<T> list, Action1Result<T, Boolean> actionCheck) {
        List<T> newList = new ArrayList<>();
        for (T t : list) {
            if (actionCheck.call(t)) {
                newList.add(t);
            }
        }
        return newList;
    }

    public static <T1, T2> List<T1> findObjects(List<T1> list1, List<T2> list2, Compare<T1, T2> compare) {
        List<T1> newList = new ArrayList<>();
        for (T1 t1 : list1) {
            for (T2 t2 : list2) {
                if (compare.compare(t1, t2) == 0) {
                    newList.add(t1);
                    break;
                }
            }
        }
        return newList;
    }

    public static <T> boolean contain(T[] ts, T t, Compare<T, T> compare) {
        for (T t1 : ts) {
            if (compare.compare(t1, t) == 0) {
                return true;
            }
        }
        return false;
    }

    public static Observable<Object> getObObj(ActionResult<Object> action) {
        return Observable.create(em -> {
            em.onNext(action.call());
            em.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public static <T> Observable<T> getOb(ActionResult<T> action) {
        return Observable.create((ObservableOnSubscribe<T>) em -> {
            em.onNext(action.call());
            em.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public static <T> Observable<List<T>> getObList(ActionResult<List<T>> action) {
        return Observable.create((ObservableOnSubscribe<List<T>>) em -> {
            em.onNext(action.call());
            em.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public static MappingJackson2HttpMessageConverter converterJack() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        updateTimeConvertObjectMapper(converter.getObjectMapper());
        return converter;
    }

    public static void updateTimeConvertObjectMapper(ObjectMapper objectMapper) {
        SimpleModule javaTimeModule = new SimpleModule();

        // date
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer());
        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());

        // time
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
//        javaTimeModule.addDeserializer(Long.class, new LongDeserializer());

        //month
        javaTimeModule.addSerializer(YearMonth.class, new YearMonthSerializer());

        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.registerModule(javaTimeModule);
    }

    public static void updateTimeConvertXmlMapperEbay(XmlMapper objectMapper) {
        SimpleModule javaTimeModule = new SimpleModule();

        // date
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer());
        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());

        // time
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializerEbay());
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializerEbay());
//        javaTimeModule.addDeserializer(Long.class, new LongDeserializer());


        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.registerModule(javaTimeModule);
    }

    public static String toXmlString(Object object, XmlMapper xmlMapper) {
        try {
            return xmlMapper.writeValueAsString(object).replace(" xmlns=\"\"", "");
        } catch (Exception e) {
            LOG.error("Fail to map request object to xml string");
            return null;
        }
    }

    public static <T> T toObjectXml(XmlMapper xmlMapper, String contentXml, Class<T> clazz) {
        try {
            return xmlMapper.readValue(contentXml, clazz);
        } catch (Exception e) {
            LOG.error("Fail to map request object to xml string");
            e.printStackTrace();
            return null;
        }
    }

    public static String readString(InputStream in) throws IOException {
        byte[] b = new byte[1024];
        StringBuilder content = new StringBuilder();
        int le = in.read(b);
        while (le >= 0) {
            content.append(new String(b, 0, le));
            le = in.read(b);
        }
        in.close();
        return content.toString();
    }


    public static <T1, T2> List<T1> findNotContain(List<T1> first, List<T2> second, Compare<T1, T2> compare) {
        List<T1> result = new ArrayList<>();
        for (T1 t1 : first) {
            boolean isContain = false;
            for (T2 t2 : second) {
                if (compare.compare(t1, t2) == 0) {
                    isContain = true;
                    break;
                }
            }
            if (!isContain) {
                result.add(t1);
            }
        }
        return result;
    }

    public static void validRolePartner(TradeIn tradeIn) throws ExceptionResponse {
        if (!checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            String partnerId = CommonUtils.getPartnerId();
            if (partnerId == null || !partnerId.equals(tradeIn.getPartnerId())) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.YOU_DONT_HAVE_PERMISSION), HttpStatus.FORBIDDEN);
            }
        }
    }

    public static <T1, T2> boolean contain(List<T1> t1s, T2 t2, Compare<T1, T2> compare) {
        for (T1 t1 : t1s) {
            if (compare.compare(t1, t2) == 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkEbayInvalid(long errorCode) {
        return EbayValue.EXPIRE_TOKEN_CODE == errorCode || EbayValue.INVALID_TOKEN_CODE == errorCode || EbayValue.INVALID_EBAY_TOKEN_CODE == errorCode;
    }

    public static void checkYearValid(String yearName) throws ExceptionResponse {
        if (!StringUtils.isEmpty(yearName)) {
            try {
                long yearValue = Long.parseLong(yearName);
                if (yearValue < 0) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.YEAR_INVALID),
                            HttpStatus.BAD_REQUEST);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.YEAR_INVALID),
                        HttpStatus.BAD_REQUEST);
            }
        }
    }

    public static void checkEmailFormatValid(String email) throws ExceptionResponse {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        if (!pattern.matcher(email).matches()) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.EMAIL_INVALID),
                    HttpStatus.BAD_REQUEST);
        }
    }

    public static String getMessageError(List<Error> errors) {
        if (errors == null) {
            return null;
        }
        for (Error error : errors) {
            if (error.getSeverityCode().equals(EbayValue.ERROR_EBAY) || error.getErrorCode() == EbayValue.ERROR_CODE_CLOSE_ACUTION) {
                return error.getLongMessage();
            }
        }
        return null;
    }

    public static <T> T findMin(List<T> values, Compare<T, T> compare) {
        T min = values.get(0);
        for (int i = 1; i < values.size(); i++) {
            if (compare.compare(min, values.get(i)) > 0) {
                min = values.get(i);
            }
        }
        return min;
    }

    public static <T> T findMax(List<T> values, Compare<T, T> compare) {
        T max = values.get(0);
        for (int i = 1; i < values.size(); i++) {
            if (compare.compare(max, values.get(i)) < 0) {
                max = values.get(i);
            }
        }
        return max;
    }

    public static String convertToListCallApi(List<String> ids){
        String idsString = "";
        if (ids.size() > 0) {
            idsString = ids.get(0);
            if (ids.size() > 1) {
                for (int i = 1; i < ids.size(); i++) {
                    idsString = idsString + "," + ids.get(i);
                }
            }
        }
        return idsString;
    }

    public static LocalDateTime unixTimestampToLocalDateTime(Long timestamp) throws ExceptionResponse {
        if (timestamp == null) return null;
        if (timestamp < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    MessageResponses.TIME_INVALID,
                    HttpStatus.BAD_REQUEST
            );
        }
        return new DateTime(timestamp * 1000L).toLocalDateTime();
    }

    public static boolean isSellerOf(Inventory inventory) {
        return isSeller(inventory.getSellerId(), inventory.getPartnerId(), inventory.getStorefrontId());
    }

    public static boolean isSeller(String sellerUserId, String sellerPartnerId, String sellerStorefrontId) {
        if (!isLogined()) {
            return false;
        }
        String userId = getUserLogin();

        if (sellerStorefrontId != null) {
            String storefrontId = getStorefrontId();
            if (storefrontId == null || !sellerStorefrontId.equals(storefrontId)) {
                return false;
            }
        }
        if (sellerUserId.equals(userId)) {
            return true;
        }
        if (sellerPartnerId != null) {
            String partnerId = getPartnerId();
            if (partnerId == null || !sellerPartnerId.equals(partnerId)) {
                return false;
            }
        }
        return true;
    }
}
