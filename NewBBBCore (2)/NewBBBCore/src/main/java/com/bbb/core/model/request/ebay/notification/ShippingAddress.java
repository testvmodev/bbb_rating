package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingAddress {
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlProperty(localName = "Street1")
    private String street1;
    @JacksonXmlProperty(localName = "CityName")
    private String cityName;
    @JacksonXmlProperty(localName = "StateOrProvince")
    private String stateOrProvince;
    @JacksonXmlProperty(localName = "Country")
    private String country;
    @JacksonXmlProperty(localName = "CountryName")
    private String countryName;
    @JacksonXmlProperty(localName = "Phone")
    private String phone;
    @JacksonXmlProperty(localName = "PostalCode")
    private String postalCode;
    @JacksonXmlProperty(localName = "AddressID")
    private long addressID;
    @JacksonXmlProperty(localName = "AddressOwner")
    private String addressOwner;
    @JacksonXmlProperty(localName = "AddressUsage")
    private String addressUsage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public long getAddressID() {
        return addressID;
    }

    public void setAddressID(long addressID) {
        this.addressID = addressID;
    }

    public String getAddressOwner() {
        return addressOwner;
    }

    public void setAddressOwner(String addressOwner) {
        this.addressOwner = addressOwner;
    }

    public String getAddressUsage() {
        return addressUsage;
    }

    public void setAddressUsage(String addressUsage) {
        this.addressUsage = addressUsage;
    }
}
