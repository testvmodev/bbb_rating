package com.bbb.core.model.response;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class ContentDoubleLong {
    @EmbeddedId
    private ContentDoubleLongId id;

    public ContentDoubleLongId getId() {
        return id;
    }

    public void setId(ContentDoubleLongId id) {
        this.id = id;
    }
}
