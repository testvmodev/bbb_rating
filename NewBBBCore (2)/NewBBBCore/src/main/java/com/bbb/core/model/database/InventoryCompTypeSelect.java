package com.bbb.core.model.database;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InventoryCompTypeSelect {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Long inventoryCompTypeId;
    private String value;
    private int orderSort;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Long getInventoryCompTypeId() {
        return inventoryCompTypeId;
    }

    public void setInventoryCompTypeId(Long inventoryCompTypeId) {
        this.inventoryCompTypeId = inventoryCompTypeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getOrderSort() {
        return orderSort;
    }

    public void setOrderSort(int orderSort) {
        this.orderSort = orderSort;
    }
}
