package com.bbb.core.repository.favourite;

import com.bbb.core.model.database.Favourite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavouriteRepository extends JpaRepository<Favourite, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM favourite " +
            "WHERE is_delete = 0 " +
            "AND :#{#userId} = user_id " +
            "ORDER BY favourite.created_time DESC " +
            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#size} ROWS ONLY"
    )
    List<Favourite> findAll(
            @Param("userId") String userId,
            @Param("offset") int offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM favourite " +

            "LEFT JOIN market_listing " +
                "ON favourite.market_listing_id = market_listing.id " +
                "AND market_listing.is_delete = 0 " +
            "LEFT JOIN inventory_auction " +
                "ON favourite.inventory_auction_id = inventory_auction.id " +
                "AND inventory_auction.is_delete = 0 " +
                "AND inventory_auction.is_inactive = 0 " +

            "JOIN inventory " +
            "ON (favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).MARKET_LIST.getValue()} " +
                "AND market_listing.inventory_id = inventory.id) " +
            "OR (favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).AUCTION.getValue()} " +
                "AND inventory_auction.inventory_id = inventory.id) " +

            "WHERE favourite.is_delete = 0 " +
            "AND :userId = user_id"
    )
    int totalByUser(
            @Param(value = "userId") String userId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "CASE WHEN EXISTS(SELECT * " +
            "FROM favourite " +
            "WHERE (:marketListingId IS NOT NULL OR :inventoryAuctionId IS NOT NULL) " +
            "AND (:marketListingId IS NULL OR favourite.market_listing_id = :#{#marketListingId}) " +
            "AND (:inventoryAuctionId IS NULL OR favourite.inventory_auction_id = :inventoryAuctionId) " +
            "AND favourite.user_id = :userId " +
            "AND favourite.is_delete = 0) " +
            "THEN CAST(1 AS BIT) " +
            "ELSE CAST(0 AS BIT) " +
            "END "
    )
    boolean isFavourite(
            @Param("userId") String userId,
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId
    );

    @Query(nativeQuery = true, value = "SELECT favourite.market_listing_id " +
            "FROM favourite " +
            "WHERE " +
            "favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).MARKET_LIST.getValue()} AND " +
            "(1 = :#{@queryUtils.isEmptyList(#marketListingIds) ? 1 : 0} " +
                "OR favourite.market_listing_id IN :#{@queryUtils.isEmptyList(#marketListingIds) ? @queryUtils.fakeLongs() : #marketListingIds}) AND " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0")
        //repository wont return as Long even with List<Long>. Its still List<Integer> indeed
    List<Integer> filterMarketListingFavourites(
            @Param(value = "userId") String userId,
            @Param(value = "marketListingIds") List<Integer> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT favourite.inventory_auction_id " +
            "FROM favourite " +
            "WHERE " +
            "favourite.favourite_type = :#{T(com.bbb.core.model.database.type.FavouriteType).AUCTION.getValue()} AND " +
            "favourite.inventory_auction_id IN :inventoryAuctionIds AND " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0")
        //repository wont return as Long even with List<Long>. Its still List<Integer> indeed
    List<Integer> filterInventoryAuctionFavourites(
            @Param(value = "userId") String userId,
            @Param(value = "inventoryAuctionIds") List<Integer> inventoryAuctionIds
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM favourite " +
            "WHERE favourite.is_delete = 0 " +
            "AND (:marketListingId IS NOT NULL OR :inventoryAuctionId IS NOT NULL) " +
            "AND favourite.user_id = :userId " +
            "AND (:marketListingId IS NULL OR favourite.market_listing_id = :#{#marketListingId}) " +
            "AND (:inventoryAuctionId IS NULL OR favourite.inventory_auction_id = :inventoryAuctionId)"
    )
    Favourite findOne(
            @Param("userId") String userId,
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "favourite " +
            "LEFT JOIN market_listing " +
            "ON favourite.market_listing_id = market_listing.id " +
            "AND market_listing.is_delete = 0 " +

            "LEFT JOIN inventory_auction " +
            "ON favourite.inventory_auction_id = inventory_auction.id " +
            "AND inventory_auction.is_delete = 0 " +

            "WHERE " +
            "(:isMarketListingId = 0 OR favourite.market_listing_id IN :marketListingIds) AND " +
            "(:isAuctionId = 0 OR favourite.inventory_auction_id IN :auctionIds) AND " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "(market_listing.id IS NULL OR " +
                ":statusMarketListing IS NULL OR " +
                "market_listing.status = :statusMarketListing" +
            ") AND "+
            "(inventory_auction.id IS NULL OR (inventory_auction.is_inactive = 0 AND inventory_auction.is_delete = 0)) "
    )
    List<Favourite> findAllFavourite(
            @Param(value = "isMarketListingId") boolean isMarketListingId,
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "isAuctionId") boolean isAuctionId,
            @Param(value = "auctionIds") List<Long> auctionIds,
            @Param(value = "userId") String userId,
            @Param(value = "statusMarketListing") String statusMarketListing
    );


    @Query(nativeQuery = true, value = "SELECT favourite.* FROM " +
            "favourite " +
            "JOIN inventory_auction ON favourite.inventory_auction_id = inventory_auction.id " +
            "WHERE " +
            "favourite.inventory_auction_id IN :auctionIds AND " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "inventory_auction.is_inactive = 0 AND inventory_auction.is_delete = 0 "
    )
    List<Favourite> findAllFavouriteInventoryAuction(
            @Param(value = "auctionIds") List<Long> auctionIds,
            @Param(value = "userId") String userId
    );


    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "favourite " +
            "JOIN market_listing ON favourite.market_listing_id = market_listing.id " +
            "WHERE " +
            "favourite.market_listing_id IN :marketListingIds AND " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND " +
            "market_listing.is_delete = 0 "
    )
    List<Favourite> findAllFavouriteMarketListing(
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "userId") String userId
    );
}
