package com.bbb.core.manager.salesforce;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.templaterest.RestSalesforceManager;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.otherservice.request.salesforce.SalesforceSoldRequest;
import com.bbb.core.model.otherservice.response.order.OrderDetailResponse;
import com.bbb.core.model.otherservice.response.order.OrderLineItem;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.salesforce.BaseSaleForceResponse;
import com.bbb.core.model.response.salesforce.SaleForceInfoCallSoldResponse;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.salesforce.SaleForceInfoCallSoldResponseRepository;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class SalesforceAsync {
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private RestSalesforceManager restSalesforceManager;
    @Autowired
    private SaleForceInfoCallSoldResponseRepository saleForceInfoCallSoldResponseRepository;
    @Autowired
    private AuthManager authManager;

    @Async
    public void updateSaleForceSoldFromMarketListing(long marketListingId, OrderDetailResponse orderDetailResponse) {
        SaleForceInfoCallSoldResponse saleForceInfoCallSoldResponse = saleForceInfoCallSoldResponseRepository.findInventoryIdAndSalesforceIdFromMarketListingId(marketListingId);
        if (saleForceInfoCallSoldResponse == null) {
            return;
        }
        sendSoldSalesforce(saleForceInfoCallSoldResponse, orderDetailResponse);
    }

    @Async
    public void updateSaleForceSoldFromOffer(long offerId, OrderDetailResponse orderDetailResponse) {
//        String salesforceId = inventoryRepository.findSalesforceIdFromOfferId(offerId);
        SaleForceInfoCallSoldResponse saleForceInfo = saleForceInfoCallSoldResponseRepository.findInventoryIdAndSalesforceIdFromOfferId(offerId);
        if (saleForceInfo == null) {
            return;
        }
        sendSoldSalesforce(saleForceInfo, orderDetailResponse);
    }

    private void sendSoldSalesforce(SaleForceInfoCallSoldResponse saleForceInfo, OrderDetailResponse orderDetailResponse) {

        OrderLineItem orderLineItem = CommonUtils.findObject(orderDetailResponse.getLineItems(), o -> o.getInventoryId() == saleForceInfo.getInventoryId());
        if (orderLineItem == null) {
            return;
        }
        UserDetailFullResponse buyerInfo = authManager.getUserFullInfo(orderLineItem.getBuyerId());
        UserDetailFullResponse sellerInfo = authManager.getUserFullInfo(orderLineItem.getSellerId());
        SalesforceSoldRequest salesforceSoldRequest = new SalesforceSoldRequest();
        salesforceSoldRequest.setStage(StageInventory.SOLD.getValue());
        salesforceSoldRequest.setInventoryId(saleForceInfo.getSalesforceId());
        salesforceSoldRequest.setTax(orderLineItem.getTax() == null ? 0 : orderLineItem.getTax());
        salesforceSoldRequest.setShippingmethod(ValueCommons.BBV2);
        salesforceSoldRequest.setShippingType(saleForceInfo.getShippingType());
        salesforceSoldRequest.setShipping(orderLineItem.getShipping());

        salesforceSoldRequest.setSellerPaypalEmail(orderLineItem.getPaypalEmailSeller());
        salesforceSoldRequest.setSellerEmail(sellerInfo.getEmail());
        if (sellerInfo.getNormal() != null) {
            salesforceSoldRequest.setSellerLastName(sellerInfo.getNormal().getLastName());
            salesforceSoldRequest.setSellerFirstName(sellerInfo.getNormal().getFirstName());
            salesforceSoldRequest.setSellerCountry(sellerInfo.getNormal().getCountry());
        }

        if (sellerInfo.getPartner() != null) {
            salesforceSoldRequest.setSellerStreet(sellerInfo.getPartner().getAddress());
            salesforceSoldRequest.setSellerState(sellerInfo.getPartner().getState());
            salesforceSoldRequest.setSellerName(sellerInfo.getPartner().getName());

        } else {
            if (sellerInfo.getStorefront() != null) {
                salesforceSoldRequest.setSellerStreet(sellerInfo.getStorefront().getAddress());
                salesforceSoldRequest.setSellerState(sellerInfo.getStorefront().getState());
                salesforceSoldRequest.setSellerName(sellerInfo.getStorefront().getName());
            } else {
                if (sellerInfo.getOnlineStore() != null) {
                    salesforceSoldRequest.setSellerStreet(sellerInfo.getOnlineStore().getAddress());
                    salesforceSoldRequest.setSellerState(sellerInfo.getOnlineStore().getState());
                    salesforceSoldRequest.setSellerName(sellerInfo.getOnlineStore().getName());
                } else {
                    if (sellerInfo.getNormal() != null) {
                        salesforceSoldRequest.setSellerStreet(sellerInfo.getNormal().getAddress());
                        salesforceSoldRequest.setSellerState(sellerInfo.getNormal().getState());
                        salesforceSoldRequest.setSellerName(sellerInfo.getNormal().getFirstName() + " " + sellerInfo.getNormal().getFirstName());
                        salesforceSoldRequest.setSellerLastName(sellerInfo.getNormal().getLastName());
                        salesforceSoldRequest.setSellerFirstName(sellerInfo.getNormal().getFirstName());
                        salesforceSoldRequest.setSellerCountry(sellerInfo.getNormal().getCountry());
                    }
                }
            }

        }
        salesforceSoldRequest.setSalesPrice(saleForceInfo.getCurrentListedPrice());
        salesforceSoldRequest.setSaleID(orderDetailResponse.getId());
        salesforceSoldRequest.setSaleDateTime(new SimpleDateFormat(Constants.FORMAT_DATE_TIME_EBAY).format(LocalDateTime.now().toDate()));
        salesforceSoldRequest.setListingType(ValueCommons.PTP.equals(saleForceInfo.getShippingType()) ? ValueCommons.PTP_V1 : ValueCommons.B2C);
        salesforceSoldRequest.setListingID(saleForceInfo.getListingId());
        salesforceSoldRequest.setInventoryId(saleForceInfo.getSalesforceId());
        salesforceSoldRequest.setInsurance(orderLineItem.getInsurance() == null ? 0 : orderLineItem.getInsurance());
        salesforceSoldRequest.setGrandTotal(salesforceSoldRequest.getShipping() + salesforceSoldRequest.getSalesPrice() + salesforceSoldRequest.getInsurance());
        salesforceSoldRequest.setCommissionFee(saleForceInfo.getCurrentListedPrice() * 0.05);


        salesforceSoldRequest.setBuyerStreet(saleForceInfo.getBuyerStreet());
        salesforceSoldRequest.setBuyerState(saleForceInfo.getBuyerState());
        if (buyerInfo.getNormal() != null) {
            salesforceSoldRequest.setBuyerFirstName(buyerInfo.getNormal().getFirstName());
            salesforceSoldRequest.setBuyerLastName(buyerInfo.getNormal().getLastName());
            salesforceSoldRequest.setBuyerEmail(buyerInfo.getEmail());
            salesforceSoldRequest.setBuyerPaypalEmail(buyerInfo.getNormal().getPaypalEmail());
            salesforceSoldRequest.setBuyerCountry(buyerInfo.getNormal().getCountry());
            salesforceSoldRequest.setBuyerCity(buyerInfo.getNormal().getCity());
        } else {
            if (buyerInfo.getPartner() != null) {
                salesforceSoldRequest.setBuyerStreet(buyerInfo.getPartner().getAddress());
                salesforceSoldRequest.setBuyerState(buyerInfo.getPartner().getState());
//                salesforceSoldRequest.setBu(buyerInfo.getPartner().getName());

            } else {
                if (buyerInfo.getStorefront() != null) {
                    salesforceSoldRequest.setBuyerStreet(buyerInfo.getStorefront().getAddress());
                    salesforceSoldRequest.setBuyerState(buyerInfo.getStorefront().getState());
                } else {
                    if (buyerInfo.getOnlineStore() != null) {
                        salesforceSoldRequest.setBuyerStreet(buyerInfo.getOnlineStore().getAddress());
                        salesforceSoldRequest.setBuyerState(buyerInfo.getOnlineStore().getState());
                    }
                }


            }
        }
        restSalesforceManager.putObject(Constants.MARKET_LISTING_BBBV2 + "/" + saleForceInfo.getSalesforceId(), salesforceSoldRequest, BaseSaleForceResponse.class);
    }
}
