package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Shipper {
    @JacksonXmlProperty(localName = "Contact")
    private Contact contact;
    @JacksonXmlProperty(localName = "Address")
    private Address address;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
