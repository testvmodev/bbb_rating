package com.bbb.core.repository.common;

import com.bbb.core.model.database.embedded.NumberStringId;
import com.bbb.core.model.response.NumberString;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NumberStringRepository extends JpaRepository<NumberString, NumberStringId> {
    //find all value from inventoryCompTypeIds non select
    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_comp_type_id AS id, inventory_comp_detail.value AS value " +
            "FROM inventory_comp_detail " +
            "WHERE " +
            "inventory_comp_detail.inventory_comp_type_id IN :inventoryCompTypeIds " +
            "GROUP BY inventory_comp_detail.inventory_comp_type_id, inventory_comp_detail.value"
    )
    List<NumberString> findAllFromInventoryCompTypeIdsNonSelect(
            @Param(value = "inventoryCompTypeIds") List<Long> inventoryCompTypeIds
    );
}
