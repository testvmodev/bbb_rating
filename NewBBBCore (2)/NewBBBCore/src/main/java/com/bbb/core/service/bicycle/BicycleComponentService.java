package com.bbb.core.service.bicycle;

public interface BicycleComponentService {
    Object importBicycleComponent(long offset, long size);
}
