package com.bbb.core.manager.tradein.async;

import com.bbb.core.manager.inventory.InventoryManager;
import com.bbb.core.model.database.*;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.tradein.TradeInCustomQuoteRepository;
import com.bbb.core.repository.tradein.TradeInImageRepository;
import com.bbb.core.repository.tradein.TradeInUpgradeCompDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TradeInManagerAsync {
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private TradeInUpgradeCompDetailRepository tradeInUpgradeCompDetailRepository;
    @Autowired
    private InventoryManager inventoryManager;
    @Autowired
    private TradeInCustomQuoteRepository tradeInCustomQuoteRepository;
    @Autowired
    private TradeInImageRepository tradeInImageRepository;

//    @Async
//    private void createInventoryFromTradeInCustomQuote(TradeInCustomQuote customQuote, TradeIn tradeIn) {
//        Bicycle bicycle = bicycleRepository.findOneByBrandModelYearIgnoreActive(customQuote.getBrandId(), customQuote.getModelId(), customQuote.getYearId());
//        if (bicycle == null) {
//            return;
//        }
//        List<Long> upgradeIds = tradeInUpgradeCompDetailRepository.findAllIdByTradeInId(tradeIn.getId());
//        inventoryManager.updateInventoryStepOneAsync(tradeIn, bicycle, upgradeIds, customQuote);
//
//    }

    @Async
    public void createInventoryFromScorecard(TradeIn tradeIn, TradeInShipping tradeInShipping) {
//        if (tradeIn.getCustomQuoteId() != null) {
//            TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeIn.getCustomQuoteId());
//            createInventoryFromTradeInCustomQuote(tradeInCustomQuote, tradeIn);
//        } else {
        Bicycle bicycle = bicycleRepository.findOneNotDelete(tradeIn.getBicycleId());
        if (bicycle == null) {
            return;
        }
        List<Long> upgradeIds = tradeInUpgradeCompDetailRepository.findAllIdByTradeInId(tradeIn.getId());
        TradeInCustomQuote tradeInCustomQuote;
        if (tradeIn.getCustomQuoteId() != null) {
            tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeIn.getCustomQuoteId());
        } else {
            tradeInCustomQuote = null;
        }
        inventoryManager.updateInventoryStepOne(tradeIn, bicycle, upgradeIds, tradeInCustomQuote);
//        }

        inventoryManager.updateInventoryStepTwo(tradeIn);

        List<TradeInImage> tradeInImages = tradeIn.getCustomQuoteId() != null ?
                tradeInImageRepository.findAllByTradeInCustomQuoteId(tradeIn.getCustomQuoteId()) :
                tradeInImageRepository.findAllByTradeInId(tradeIn.getId());
        inventoryManager.updateInventoryStepThree(tradeInImages);

        inventoryManager.updateInventoryStepFour(tradeIn, tradeInShipping);
    }
}
