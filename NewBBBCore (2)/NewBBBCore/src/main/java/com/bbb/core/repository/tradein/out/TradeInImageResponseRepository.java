package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.response.tradein.detail.detailstep.TradeInImageResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface TradeInImageResponseRepository extends JpaRepository<TradeInImageResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "trade_in_image.id AS image_id, trade_in_image.image_type_id AS image_type_id, " +
            "trade_in_image_type.name AS image_type_name, trade_in_image.full_link AS full_link " +
            "FROM " +
            "trade_in_image " +
            "JOIN trade_in_image_type ON trade_in_image.image_type_id = trade_in_image_type.id " +
            "WHERE trade_in_image.trade_in_id = :tradeInId"
    )
    List<TradeInImageResponse> findAll(
            @Param(value = "tradeInId") long tradeInId
    );
}
