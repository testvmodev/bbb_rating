package com.bbb.core.controller.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import com.bbb.core.model.request.sort.CustomQuoteSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.tradein.customquote.CustomQuoteReviewRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteCreateRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteUpdateRequest;
import com.bbb.core.service.tradein.TradeInCustomQuoteService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TradeInCustomQuoteController {
    @Autowired
    private TradeInCustomQuoteService service;

    @GetMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTES)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getTradeInCustomQuotes(
            @RequestParam(value = "partnerId", required = false) String partnerId,
            @RequestParam(value = "isArchived", required = false) Boolean isArchived,
            @RequestParam(value = "isIncomplete", required = false) Boolean isIncomplete,
            @RequestParam(value = "customQuoteStatus", required = false) StatusTradeInCustomQuote customQuoteStatus,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "sortField", defaultValue = "LAST_UPDATE") CustomQuoteSortField sortField,
            @RequestParam(value = "sortType", defaultValue = "DESC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        TradeInCustomQuoteRequest request = new TradeInCustomQuoteRequest();
        request.setPartnerId(partnerId);
        request.setArchived(isArchived);
        request.setIncomplete(isIncomplete);
        request.setCustomQuoteStatus(customQuoteStatus);
        request.setContent(content);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);
        return new ResponseEntity<>(service.getTradeInCustomQuotes(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_PATH)
    public ResponseEntity getDetailCustomQuote(
            @PathVariable(value = Constants.ID) long customQuoteId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getDetailCustomQuote(customQuoteId), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE)
    public ResponseEntity createTradeInCustomQuote(
            @RequestBody TradeInCustomQuoteCreateRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {

        if (request.getUpgradeComponentsId() != null) {
            CommonUtils.stream(request.getUpgradeComponentsId());
        }
        return new ResponseEntity<>(service.createCustomQuote(request), HttpStatus.OK);
    }

//    @PatchMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_PATH)
//    public ResponseEntity updateTradeInCustomQuote(
//            @RequestBody TradeInCustomQuoteCreateRequest request
//    ) throws ExceptionResponse {
//        return new ResponseEntity<>(service.updateTradeInCustomQuote(request), HttpStatus.OK);
//    }

    @PutMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_PATH)
    public ResponseEntity updateTradeInCustomQuote(
            @PathVariable(value = Constants.ID) long customQuoteId,
            @RequestBody TradeInCustomQuoteUpdateRequest request
    ) throws ExceptionResponse {
        if (request.getUpgradeComponentsId() != null) {
            CommonUtils.stream(request.getUpgradeComponentsId());
        }
        return new ResponseEntity<>(service.updateTradeInCustomQuote(customQuoteId, request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_COMMON)
    public ResponseEntity getCustomQuoteCommon(
            @PathVariable(value = "id") long tradeInIdCustomQuote
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getCustomQuoteCommon(tradeInIdCustomQuote), HttpStatus.OK);
    }


    @PostMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_UPLOAD_IMAGE)
    public ResponseEntity postTradeInCustomQuote(
            @RequestParam(value = "tradeInIdCustomQuote") long tradeInIdCustomQuote,
            @RequestParam(value = "images") List<MultipartFile> images,
            @RequestParam(value = "isAddImage", defaultValue = "false") boolean isAddImage,
            @RequestParam(value = "isSave", defaultValue = "false") boolean isSave
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.postTradeInCustomQuote(tradeInIdCustomQuote, images, isAddImage, isSave), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE + "/{tradeInCustomQuoteId}")
    public ResponseEntity deleteImageTradeInCustomQuote(
            @PathVariable(value = "tradeInCustomQuoteId") long tradeInCustomQuoteId,
            @RequestParam(value = "imageTypeId") long imageImageTypeId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteImageTradeInCustomQuote(tradeInCustomQuoteId, imageImageTypeId),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_TRADE_IN_CUSTOM_QUOTE_REVIEWED)
    public ResponseEntity updateTradeInCustomQuoteReviewed(
            @RequestParam(value = "tradeInCustomQuoteId") long tradeInCustomQuoteId,
            @RequestParam(value = "tradeInValue", required = false) Float tradeInValue,
            @RequestParam(value = "reviewNote", required = false) String reviewNote
    ) throws ExceptionResponse {
        CustomQuoteReviewRequest request = new CustomQuoteReviewRequest();
        request.setTradeInCustomQuoteId(tradeInCustomQuoteId);
        request.setTradeInValue(tradeInValue);
        request.setReviewNote(reviewNote);
        return new ResponseEntity<>(service.updateTradeInCustomQuoteReviewed(request),
                HttpStatus.OK);
    }


}
