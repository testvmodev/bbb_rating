package com.bbb.core.manager.schedule.jobs.migrate;

import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.manager.tradein.TradeInCustomQuoteManager;
import com.bbb.core.model.database.BicycleType;
import com.bbb.core.model.database.MyListingDraft;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.bbb.core.model.request.saleforce.create.SaleforceInvCompRequest;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.market.MyListingDraftRepository;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import java.util.*;

@Job(defaultTimeType = JobTimeType.FIX_DELAY, defaultTimeSeconds = 300)
@Component
public class RemapListingDraftJob extends ScheduleJob {
    private int maxListingPerRun = 200;
    private List<String> componentColumns = Arrays.asList(
            "wheel_size", "suspension", "frame_size", "frame_material", "gender_name", "front_shock",
            "front_front_suspension", "cassette", "weight", "color", "frame_construction",
            "fork_brand_model", "fork_material", "rear_shock", "hubs", "rims", "tires", "spoke_brand",
            "spoke_nipples", "component_group", "brakeset", "shift_levers", "crankset", "pedals",
            "bottom_bracket", "bb_shell_width", "rear_cogs", "chain", "seat_post", "saddle",
            "handlebar", "extensions", "handlebar_stem", "headset", "front_derailleur",
            "rear_derailleur", "fork_front_suspension", "wheels", "brake_type", "frame_tubing_material"
    );
    private Map<String, String> componentNameMap = new HashMap<>();
    private List<ConditionInventory> v2conditions = Arrays.asList(
            ConditionInventory.EXCELLENT,
            ConditionInventory.VERY_GOOD,
            ConditionInventory.GOOD,
            ConditionInventory.FAIR
    );

    private SessionFactory sessionFactory;
    @Autowired
    private MyListingManager myListingManager;
    @Autowired
    private TradeInCustomQuoteManager tradeInCustomQuoteManager;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private MyListingDraftRepository myListingDraftRepository;

    private Session session;

    @Autowired
    public RemapListingDraftJob(EntityManagerFactory entityManagerFactory) {
        sessionFactory = ConfigDataSource.getSessionFactory(entityManagerFactory);

        //map column name to component type's name
        for (String column : componentColumns) {
            switch (column) {
                case "fork_front_suspension":
                    componentNameMap.put(column, "Fork/Front Suspension");
                    break;
                case "gender_name":
                    componentNameMap.put(column, "Gender");
                    break;
                case "seat_post":
                    componentNameMap.put(column, "Seatpost");
                    break;
                case "fork_brand_model":
                    componentNameMap.put(column, "Fork Brand & Model");
                    break;
                default:
                    componentNameMap.put(column, column.replace("_", " "));
            }
        }
    }

    @Override
    public synchronized void run() {
        if (session == null) {
            session = sessionFactory.openSession();
        }

        NativeQuery bikeQuery = session.createSQLQuery("SELECT TOP " + maxListingPerRun + " " +
                "v1_p2p_bike.*, " +
                "v1_p2p_listing.title, " +
                "v1_p2p_listing.shipping, " +
                "v1_p2p_listing.flat_shipping, " +
                "v1_fos_user.zip, " +
                "v1_fos_user.state, " +
                "v1_fos_user.city, " +
                "v1_fos_user.address, " +
                "v1_fos_user.paypal_email " +
                "FROM v1_p2p_bike " +
                "JOIN v1_p2p_listing ON v1_p2p_listing.id = v1_p2p_bike.listing_id " +
                "JOIN v1_fos_user ON v1_fos_user.id = v1_p2p_listing.user_id " +
                "LEFT JOIN inventory ON inventory.v1_bike_id = v1_p2p_bike.id " +
                "LEFT JOIN my_listing_draft ON my_listing_draft.v1_bike_id = v1_p2p_bike.id " +
                "WHERE v1_p2p_bike.listing_active_status = -1 " + //draft
                "AND inventory.id IS NULL " +
                "AND my_listing_draft.id IS NULL"
        );
        bikeQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<Map<String, Object>> bikeDrafts = bikeQuery.list();
        if (bikeDrafts == null || bikeDrafts.size() < 1) {
            disable();
        }

        NativeQuery compQuery = session.createSQLQuery("SELECT v1_p2p_bike_comp.* " +
                "FROM (" +
                "SELECT TOP " + maxListingPerRun + " " +
                "v1_p2p_bike.* " +
                "FROM v1_p2p_bike " +
                "LEFT JOIN inventory ON inventory.v1_bike_id = v1_p2p_bike.id " +
                "LEFT JOIN my_listing_draft ON my_listing_draft.v1_bike_id = v1_p2p_bike.id " +
                "WHERE v1_p2p_bike.listing_active_status = -1 " + //draft
                "AND inventory.id IS NULL " +
                "AND my_listing_draft.id IS NULL" +
                ") AS v1_p2p_bike " +
                "JOIN v1_p2p_bike_comp ON v1_p2p_bike.id = v1_p2p_bike_comp.bike_id "
        );
        compQuery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List<Map> compDrafts = compQuery.list();

        List<MyListingDraft> myListingDrafts = new ArrayList<>();

        for (Map<String, Object> bike : bikeDrafts) {
            long bikeId = (Integer) bike.get("id");

            PersonalListingRequest personalListingRequest = new PersonalListingRequest();
            String brandName = (String) bike.get("brand_name");
            String customBrandName = (String)bike.get("custom_brand_name");
            if (!StringUtils.isBlank(brandName)) {
                personalListingRequest.setBrandName(brandName);
            } else if (!StringUtils.isBlank(customBrandName)) {
                personalListingRequest.setBrandName(customBrandName);
            }
            String modelName = (String) bike.get("model_name");
            String customModelName = (String)bike.get("custom_model_name");
            if (!StringUtils.isBlank(modelName)) {
                personalListingRequest.setModelName(modelName);
            } else if (!StringUtils.isBlank(customModelName)) {
                personalListingRequest.setModelName(customModelName);
            }
            if (bike.get("year") != null) {
                personalListingRequest.setYearName(String.valueOf(bike.get("year")));
            }

            List<String> bicycleTypeNames = new ArrayList<>();
            String categoryName = (String) bike.get("category_name");
            if (!StringUtils.isBlank(categoryName)) {
                bicycleTypeNames.add(categoryName);
            }
            String subcategoryName = (String) bike.get("subcategory_name");
            if (!StringUtils.isBlank(subcategoryName)) {
                bicycleTypeNames.add(subcategoryName);
            }
            String cateFromSubName = (String) bike.get("cate_from_sub_name");
            if (!StringUtils.isBlank(cateFromSubName)) {
                bicycleTypeNames.add(cateFromSubName);
            }
            if (bicycleTypeNames.size() > 0) {
                BicycleType bicycleType = bicycleTypeRepository.findOneByNames(bicycleTypeNames);
                if (bicycleType != null) {
                    personalListingRequest.setBicycleTypeId(bicycleType.getId());
                } else {
                    //TODO cant map v1 category/subcategory to v2 bicycle type
//                    System.out.println("Skipped " + bikeId);
                    logFail(bikeId);
//                    continue;
                }
            }

            String description = (String) bike.get("description");
            if (!StringUtils.isBlank(description)) {
                personalListingRequest.setDescription(description);
            }
            String conditionValue = (String)bike.get("condition");
            if (!StringUtils.isBlank(conditionValue)) {
                ConditionInventory condition = ConditionInventory.findByValue(conditionValue);
                if (condition == null || !v2conditions.contains(condition)) {
                    logIncompatibleCondition(bikeId, conditionValue);
                }
            }
            Float price = ((Double) bike.get("listed_price")).floatValue();
            personalListingRequest.setSalePrice(price);

            String paypalEmail = (String) bike.get("paypal_email");
            if (!StringUtils.isBlank(paypalEmail)) {
                personalListingRequest.setEmailPaypal(paypalEmail);
            }
            String zipcode = (String) bike.get("zip");
            if (!StringUtils.isBlank(zipcode)) {
                personalListingRequest.setZipCode(zipcode);
            }
            String state = (String) bike.get("state");
            if (!StringUtils.isBlank(state)) {;
                personalListingRequest.setState(state);
            }
            String city = (String) bike.get("city");
            if (!StringUtils.isBlank(city)) {
                personalListingRequest.setCityName(city);
            }
            String address = (String) bike.get("address");
            if (!StringUtils.isBlank(address)) {
                personalListingRequest.setAddressLine(address);
            }
            personalListingRequest.setCountry("US");

            String shipping = (String) bike.get("shipping");
            if (!StringUtils.isBlank(shipping)) {
                if (shipping.contains("s:6:\"PICKUP\"")) {
                    personalListingRequest.setLocalPickupShipping(true);
                } else {
                    personalListingRequest.setLocalPickupShipping(false);
                }
                if (shipping.contains("s:3:\"BBB\"")) {
                    personalListingRequest.setShippingType(OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE);
                    if (shipping.contains("s:9:\"INSURANCE\"")) {
                        personalListingRequest.setRequireInsuranceShipping(true);
                    } else {
                        personalListingRequest.setRequireInsuranceShipping(false);
                    }
                } else if (shipping.contains("s:4:\"FLAT\"") && bike.get("flat_shipping") != null) {
                    personalListingRequest.setShippingType(OutboundShippingType.FLAT_RATE_TYPE);
                    Float flatRate = ((Double) bike.get("flat_shipping")).floatValue();
                    personalListingRequest.setFlatRate(flatRate);
                }
            }


            List<CompRequest> compRequests = new ArrayList<>();
            List<SaleforceInvCompRequest> saleforceInvCompRequests = new ArrayList<>();
            for (Map comp : compDrafts) {
                if ((Integer)comp.get("bike_id") == bikeId) {
                    for (Object column : comp.keySet()) {
                        if (componentColumns.contains(column)) {
                            if (!componentNameMap.containsKey(column)) {
                                continue;
                            }
                            if (comp.get(column) == null || comp.get(column).equals("")) {
                                continue;
                            }
                            SaleforceInvCompRequest saleforceInvComp = new SaleforceInvCompRequest();
                            saleforceInvComp.setCompTypeName(componentNameMap.get(column));
                            saleforceInvComp.setValue((String)comp.get(column));

                            saleforceInvCompRequests.add(saleforceInvComp);
                        }
                    }
                }
            }
            try {
                compRequests = tradeInCustomQuoteManager.validateSaleforceCompType(saleforceInvCompRequests, true);
            } catch (ExceptionResponse e) {
                logFail(bikeId);
                //wrap it then throw with a runtime expception
                throw new RuntimeException("Could not convert components", e);
            }
            personalListingRequest.setCompRequests(compRequests);

            MyListingDraft myListingDraft = new MyListingDraft();
            myListingDraft.setV1BikeId(bikeId);
            try {
                String content = myListingManager.convertRequest(personalListingRequest);
                myListingDraft.setContent(content);
            } catch (ExceptionResponse e) {
                logFail(bikeId);
                //wrap it then throw with a runtime expception
                throw new RuntimeException("Could not convert listing request to json string", e);
            }
            String title = (String) bike.get("title");
            if (!StringUtils.isBlank(title)) {
                myListingDraft.setTitle(title);
            }
            myListingDraft.setCurrentListedPrice(personalListingRequest.getSalePrice());
            myListingDrafts.add(myListingDraft);
        }

        myListingDraftRepository.saveAll(myListingDrafts);
        session.close();
        session = null;
    }

    @Override
    protected void onFail(Throwable throwable) {
        if (session != null) {
            session.close();
            session = null;
        }
    }

    private void logFail(long bikeId) {
        setFail(true);
        if (getCustomLogString() == null) {
            setCustomLogString("");
        }
        setCustomLogString(getCustomLogString() + "\n" + bikeId);
    }

    private void logIncompatibleCondition(long bikeId, String condition) {
        setFail(true);
        if (getCustomLogString() == null) {
            setCustomLogString("");
        }
        setCustomLogString(getCustomLogString() + "\n" + bikeId + " - condition: " + condition);
    }
}
