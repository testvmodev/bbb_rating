package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.Offer;
import com.bbb.core.model.response.inventory.InventoryDetailResponse;

public class OfferDetailResponse extends Offer {
    private InventoryDetailResponse inventory;

    public InventoryDetailResponse getInventory() {
        return inventory;
    }

    public void setInventory(InventoryDetailResponse inventory) {
        this.inventory = inventory;
    }
}
