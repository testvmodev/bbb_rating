package com.bbb.core.model.response.bid.auction;

import com.bbb.core.model.database.type.StatusOffer;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OfferOfAuctionResponse {
    @Id
    private long id;
    private Long auctionId;
    private long inventoryId;
    private Long inventoryAuctionId;
    private StatusOffer status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StatusOffer getStatus() {
        return status;
    }

    public void setStatus(StatusOffer status) {
        this.status = status;
    }


    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }
}
