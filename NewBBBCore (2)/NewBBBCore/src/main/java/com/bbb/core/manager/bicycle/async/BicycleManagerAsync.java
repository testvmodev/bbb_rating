package com.bbb.core.manager.bicycle.async;

import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.database.Tracking;
import com.bbb.core.model.database.type.TrackingType;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.tracking.TrackingRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class BicycleManagerAsync {
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private TrackingRepository trackingRepository;
    @Async
    public void createTrackingBicycle(long bicycleId, String userId){
        Bicycle bicycle = bicycleRepository.findOne(bicycleId);
        if(bicycle == null){
            return;
        }
        bicycle.setTrackingCount(bicycle.getTrackingCount() + 1);
        bicycleRepository.save(bicycle);
        if (userId != null) {
            Tracking tracking = trackingRepository.findOneBicycle(userId, bicycleId);
            if (tracking == null) {
                tracking = new Tracking();
            }
            tracking.setLastViewed(LocalDateTime.now());
            tracking.setBicycleId(bicycleId);
            tracking.setUserId(userId);
            tracking.setType(TrackingType.BICYCLE);
            tracking.setCountTracking(tracking.getCountTracking() + 1);
            trackingRepository.save(tracking);
        }
    }
}
