package com.bbb.core.repository.common;

import com.bbb.core.model.database.LogExceptionApi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogExceptionApiRepository extends JpaRepository<LogExceptionApi, Long> {

}
