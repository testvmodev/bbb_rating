package com.bbb.core.model.database;

import com.bbb.core.model.database.embedded.TradeInCustomQuoteCompDetailId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trade_in_custom_quote_comp_detail")
public class TradeInCustomQuoteCompDetail {
    @EmbeddedId
    private TradeInCustomQuoteCompDetailId id;
    private String value;

    public TradeInCustomQuoteCompDetailId getId() {
        return id;
    }

    public void setId(TradeInCustomQuoteCompDetailId id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
