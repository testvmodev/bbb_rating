package com.bbb.core.repository.schedule;

import com.bbb.core.model.schedule.OfferInventoryAuction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferInventoryAuctionRepository extends JpaRepository<OfferInventoryAuction, Long> {
    @Query(nativeQuery = true,
            value = "SELECT " +
                        "offer.id AS offer_id, " +
                        "offer.offer_price as offer_price, " +
                        "offer.buyer_id AS buyer_id, " +
                        "inventory.id AS inventory_id, " +
//                        "inventory_auction.inventory_id AS inventory_id, " +
//                        "inventory_auction.auction_id AS auction_id, " +
                        "inventory.name AS inventory_name, " +
                        "inventory.image_default AS image_default_inv, " +
                        "inventory.status AS status_name_inv, " +
                        "inventory.stage AS stage_name_inv, " +
                        "inventory.condition AS condition_inv, " +
                        "inventory.current_listed_price AS listing_price, " +
                        "auction.id AS auction_id, " +
                        "auction.name AS name_auction, " +
                        "bicycle.name AS bicycle_name, " +
                        "model.name AS model_name, " +
                        "brand.name AS brand_name, " +
                        "bicycle_year.name AS year_name, " +
                        "bicycle_type.name AS type_name, " +
                        "bicycle_size.name AS size_name, " +
                        "inventory_auction.id AS inventory_auction_id " +
                    "FROM offer " +
                    //find max and pending
                    "JOIN inventory_auction " +
                        "ON (inventory_auction.is_inactive = 0 " +
                        "AND offer.inventory_auction_id = inventory_auction.id ) " +
                    "JOIN " +
                        "(SELECT " + //offer.id, " +
                                "offer.offer_price, " +
                                "offer.inventory_auction_id, " +
//                                "inventory_auction.auction_id, " +
//                                "inventory_auction.inventory_id, " +
                                "MIN(offer.created_time) AS created_time " +
                        "FROM offer JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                        "JOIN " +
                            "(SELECT " + //offer.id AS id_max_price, " +
                                    "offer.inventory_auction_id AS inventory_auction_id_max, " +
                                    "MAX(offer.offer_price) AS max_price " +
                            "FROM offer " +
                            "WHERE offer.inventory_auction_id IS NOT NULL " +
                                "AND offer.inventory_auction_id IN :inventoryAuctionIds " +
                                "AND offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} " +
                            "GROUP BY offer.inventory_auction_id) " +
                            "AS max_table " +
                        "ON (offer.inventory_auction_id = max_table.inventory_auction_id_max " +
                            "AND offer.offer_price = max_table.max_price) " +
                        "GROUP BY offer.offer_price, offer.inventory_auction_id) " +
                        "AS result " +
                    "ON (offer.offer_price = result.offer_price " +
                        "AND offer.inventory_auction_id = result.inventory_auction_id " +
                        "AND offer.created_time = result.created_time) " +
                    //end

//                    "JOIN inventory_auction " +
//                        "ON (inventory_auction.is_inactive = 0 " +
//                            "AND offer.inventory_id = inventory_auction.inventory_id " +
//                            "AND offer.auction_id = inventory_auction.auction_id " +
//                            "AND inventory_auction.id = offer.inventory_auction_id) " +

//                                    "inventory_auction.auction_id AS  auction_id_max, " +
//                                    "inventory_auction.inventory_id AS inventory_id_max, " +
//                                    "MAX(offer.offer_price) AS max_price " +
//                            "FROM offer JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
//                            "WHERE inventory_auction.auction_id IN :auctionIds AND offer.status = 'Pending' " +
//                            "GROUP BY inventory_auction.auction_id, inventory_auction.inventory_id) " +
//                            "AS max_table " +
//                        "ON (inventory_auction.auction_id = max_table.auction_id_max " +
//                            "AND inventory_auction.inventory_id = max_table.inventory_id_max " +
//                            "AND offer.offer_price = max_table.max_price) " +
//                        "GROUP BY offer.offer_price, inventory_auction.auction_id, inventory_auction.inventory_id) " +
//                        "AS result " +
//                    "ON (offer.offer_price = result.offer_price " +
//                        "AND inventory_auction.inventory_id = result.inventory_id " +
//                        "AND inventory_auction.auction_id = result.auction_id " +
//                        "AND inventory_auction.created_time = result.created_time) " +
                    //end


                    "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
                    "JOIN auction ON inventory_auction.auction_id = auction.id " +
                    "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
                    "JOIN bicycle_model AS model ON bicycle.model_id = model.id " +
                    "JOIN bicycle_brand AS brand ON bicycle.brand_id = brand.id " +
                    "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
                    "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
                    "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +

                    "WHERE offer.inventory_auction_id IN :inventoryAuctionIds"
//                    "WHERE inventory_auction.auction_id IN :auctionIds"
    )
    List<OfferInventoryAuction> findAllOfferInventoryAuctionMax(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );

    @Query(nativeQuery = true,
            value = "SELECT " +
                        "offer.id AS offer_id, " +
                        "offer.offer_price AS offer_price, " +
                        "offer.buyer_id AS buyer_id, " +
                        "inventory.id AS inventory_id, " +
//                        "inventory_auction.inventory_id AS inventory_id, " +
//                        "inventory_auction.auction_id AS auction_id, " +
                        "inventory.name AS inventory_name, " +
                        "inventory.image_default AS image_default_inv, " +
                        "inventory.status AS status_name_inv, " +
                        "inventory.stage AS stage_name_inv, " +
                        "inventory.condition AS condition_inv, " +
                        "inventory.current_listed_price AS listing_price, " +
                        "auction.id AS auction_id, " +
                        "auction.name AS name_auction, " +
                        "bicycle.name AS bicycle_name, " +
                        "model.name AS model_name, " +
                        "brand.name AS brand_name, " +
                        "bicycle_year.name AS year_name, " +
                        "bicycle_type.name AS type_name, " +
                        "bicycle_size.name AS size_name, " +
                        "inventory_auction.id AS inventory_auction_id " +
                    "FROM offer " +

                    "JOIN inventory_auction " +
                    "ON offer.inventory_auction_id = inventory_auction.id " +
                    "JOIN inventory " +
                    "ON inventory_auction.inventory_id = inventory.id " +
                    "LEFT JOIN auction ON inventory_auction.auction_id = auction.id " +
//                    "LEFT JOIN inventory_auction " +
//                    "ON offer.auction_id = inventory_auction.auction_id " +
//                        "AND offer.inventory_id = inventory_auction.inventory_id " +
                    "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
                    "JOIN bicycle_model AS model ON bicycle.model_id = model.id " +
                    "JOIN bicycle_brand AS brand ON bicycle.brand_id = brand.id " +
                    "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
                    "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
                    "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +

                    "WHERE offer.status = 'Accepted' " +
                    "AND offer.is_delete = 0 " +
                    "AND offer.is_paid = 0 " +
                    "AND DATEDIFF_BIG(MINUTE, offer.last_update, GETDATE()) >= :expirationTime"
    )
    List<OfferInventoryAuction> findAcceptedOfferNotYetPaid(
            @Param(value = "expirationTime") long expirationTime
    );
}
