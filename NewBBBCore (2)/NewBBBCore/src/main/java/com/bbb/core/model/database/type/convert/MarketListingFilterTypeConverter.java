package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.MarketListingFilterType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MarketListingFilterTypeConverter implements AttributeConverter<MarketListingFilterType, String> {
    @Override
    public String convertToDatabaseColumn(MarketListingFilterType marketListingFilterType) {
        if (marketListingFilterType == null) {
            return null;
        }
        return marketListingFilterType.getValue();
    }

    @Override
    public MarketListingFilterType convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        for (MarketListingFilterType value : MarketListingFilterType.values()) {
            if (value.getValue().equals(s)) {
                return value;
            }
        }
        return null;
    }
}
