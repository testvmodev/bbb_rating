package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellerReturnProfile {
    @JacksonXmlProperty(localName = "ReturnProfileID")
    private long returnProfileID;

    public long getReturnProfileID() {
        return returnProfileID;
    }

    public void setReturnProfileID(long returnProfileID) {
        this.returnProfileID = returnProfileID;
    }
}
