package com.bbb.core.model.request.market.personallisting.update;

import com.bbb.core.model.database.type.OutboundShippingType;

public class PersonalListingUpdateShipping {
    private Boolean isLocalPickupShipping;
    private Boolean isRequireInsuranceShipping;
    private Float flatRate;
    private String zipCode;
    private OutboundShippingType shippingType;
    private String country;
    private String state;
    private String cityName;
    private String addressLine;

    public Boolean getLocalPickupShipping() {
        return isLocalPickupShipping;
    }

    public void setLocalPickupShipping(Boolean localPickupShipping) {
        isLocalPickupShipping = localPickupShipping;
    }

    public Boolean getRequireInsuranceShipping() {
        return isRequireInsuranceShipping;
    }

    public void setRequireInsuranceShipping(Boolean requireInsuranceShipping) {
        isRequireInsuranceShipping = requireInsuranceShipping;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }
}
