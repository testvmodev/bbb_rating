package com.bbb.core.service.impl.inventory;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.inventory.InventoryManager;
import com.bbb.core.model.request.inventory.InventoryCreateRequest;
import com.bbb.core.model.request.inventory.InventoryRequest;
import com.bbb.core.model.request.inventory.create.InventoryCreateFullRequest;
import com.bbb.core.model.request.inventory.secret.InventoryUpdateSecretRequest;
import com.bbb.core.model.request.inventory.update.InventoryUpdateFullRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingSaleRequest;
import com.bbb.core.security.user.UserContext;
import com.bbb.core.service.inventory.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {
    private final InventoryManager manager;

    @Autowired
    public InventoryServiceImpl(InventoryManager manager) {
        this.manager = manager;
    }

    @Override
    public Object getInventories(InventoryRequest request, Pageable page) throws ExceptionResponse {
        return manager.getInventories(request, page);
    }

    @Override
    public Object getInventoryDetail(long id) throws ExceptionResponse {
        return manager.getInventoryDetail(id);
    }

    @Override
    public Object importIdSaleForceId() {
        return manager.importIdSaleForceId();
    }

    @Override
    public Object importInventoryImage() {
        return manager.importInventoryImage();
    }

//    @Override
//    public Object createInventory(InventoryCreateRequest request) throws ExceptionResponse {
//        return manager.createInventory(request);
//    }


    @Override
    public Object createInventory(InventoryCreateFullRequest request) throws ExceptionResponse {
        return manager.createInventory(request);
    }

    @Override
    public Object getSecretInventories(List<Long> inventoriesId) throws ExceptionResponse {
        return manager.getSecretInventories(inventoriesId);
    }

    @Override
    public Object updateInventory(
            long inventoryId, InventoryUpdateFullRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return manager.updateInventory(inventoryId, request) ;
    }

    @Override
    public Object soldInventory(MarketListingSaleRequest request) throws ExceptionResponse {
        return manager.soldInventory(request) ;
    }

    @Override
    public Object updateInventorySecret(
            long inventoryId, InventoryUpdateSecretRequest request
    ) throws ExceptionResponse {
        return manager.updateInventorySecret(inventoryId, request);
    }
}
