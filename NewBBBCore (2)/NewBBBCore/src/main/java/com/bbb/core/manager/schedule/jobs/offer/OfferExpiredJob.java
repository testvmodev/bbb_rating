package com.bbb.core.manager.schedule.jobs.offer;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.ErrorRestResponse;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.otherservice.response.cart.CartResponse;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.model.schedule.OfferInventoryAuction;
import com.bbb.core.model.schedule.OfferMarketListing;
import com.bbb.core.model.schedule.ScheduleAddCartResponse;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.expirationtime.ExpirationTimeConfigRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.schedule.OfferInventoryAuctionRepository;
import com.bbb.core.repository.schedule.OfferMarketListingRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.FIX_DELAY, defaultTimeSeconds = 300)
public class OfferExpiredJob extends ScheduleJob {
    @Autowired
    private ExpirationTimeConfigRepository expirationTimeConfigRepository;
    @Autowired
    private OfferInventoryAuctionRepository offerInventoryAuctionRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private OfferMarketListingRepository offerMarketListingRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private AppConfig appConfig;


    @Override
    public void run() {
        ExpirationTimeConfig expirationTime = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.OFFER_AUCTION);
        if (expirationTime != null) {
            checkExpireOfferAuction(expirationTime);
        }
        ExpirationTimeConfig expirationTimeMarketListing = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.OFFER_MARKET_LISTING);
        if ( expirationTimeMarketListing != null ) {
            checkExpireOfferMarketListing(expirationTimeMarketListing);
        }
    }

    private void checkExpireOfferMarketListing(ExpirationTimeConfig expirationTime){
        List<OfferMarketListing> offerMarketListings =
                offerMarketListingRepository.findAcceptedOfferMarketListingNotYetPaid(
                        (long)((double)expirationTime.getExpireAfterHours() * Constants.HOUR / Constants.MINUTE)
        );
        if ( offerMarketListings.size() == 0 ){
            return;
        }
        List<Long> offerIds = offerMarketListings.stream().map(OfferMarketListing::getOfferId).collect(Collectors.toList());
        List<Offer> offers = offerRepository.findAllByIds(offerIds);
        if (offers.size() == 0){
            return;
        }

        List<Long> marketListingIds = offers.stream().map(Offer::getMarketListingId).collect(Collectors.toList());
        List<Long> inventoryIds = marketListingRepository.findAllInventoryIdByIds(marketListingIds)
                .stream().map(Integer::longValue).collect(Collectors.toList());
        if (inventoryIds.size() == 0){
            return;
        }
        List<MarketListing> marketListings = marketListingRepository.findAllByInventoryId(inventoryIds);
        if (marketListings.size() == 0){
            return;
        }

        //check status cart in order paying
        List<Offer> offersPaying = new ArrayList<>();
        List<Observable<CartSingleStageResponse>> cartObs = new ArrayList<>();
        for (Offer o : offers) {
            MarketListing marketListing = CommonUtils.findObject(marketListings, m -> m.getId() == o.getMarketListingId());
            cartObs.add(Observable.create((ObservableOnSubscribe<CartSingleStageResponse>) c -> {
                CartSingleStageResponse status = billingManager.checkStatusCart(marketListing.getInventoryId(), o.getBuyerId());
                if (status.getStage().equals(ValueCommons.PAY)) {
                    offersPaying.add(o);
                }
                c.onNext(status);
                c.onComplete();
            }).subscribeOn(Schedulers.newThread()));
        }
        List<CartSingleStageResponse> cartStatuses = Observable.merge(cartObs, 4).toList().blockingGet();
        if (offersPaying.size() > 0) {
            for (Offer o : offersPaying) {
                offers.remove(o);
            }

            if (offers.size() == 0){
                return;
            }

            //refresh
            marketListingIds = offers.stream().map(Offer::getMarketListingId).collect(Collectors.toList());
            inventoryIds = marketListingRepository.findAllInventoryIdByIds(marketListingIds)
                    .stream().map(Integer::longValue).collect(Collectors.toList());
            if (inventoryIds.size() == 0){
                return;
            }
            marketListings = marketListingRepository.findAllByInventoryId(inventoryIds);
            if (marketListings.size() == 0){
                return;
            }
        }

        offers = offers.stream().peek(o->o.setStatus(StatusOffer.CLOSED_CART_EXPIRED)).collect(Collectors.toList());
        offers = offerRepository.saveAll(offers);

        List<Observable<ScheduleAddCartResponse>> observables = offerMarketListings.stream()
                .map(this::createObservableRemoveCartAndSendMail)
                .collect(Collectors.toList());
        waitRemoveCartLogIfFail(observables);


        List<MarketToList> marketToLists = marketToListRepository.findAllIdActiveEbayBBB(appConfig.getEbay().isSandbox());
        MarketToList marketToListBBB = CommonUtils.findObject(marketToLists, o->o.getType() == MarketType.BBB);
        MarketToList marketToListEbay = CommonUtils.findObject(marketToLists, o->o.getType() == MarketType.EBAY);
        if(marketToListBBB != null ){
            reListMarketListingBBB(
                    marketListings.stream().filter(o-> o.getMarketPlaceConfigId() == marketToListBBB.getMarketConfigId() &&
                            (o.getStatus() == StatusMarketListing.DE_LISTED || o.getStatus() == StatusMarketListing.SALE_PENDING )).collect(Collectors.toList())
            );
        }
        if(marketToListEbay != null ) {
                reListMarketListingEbay(
                        marketListings.stream().filter(o->
                                o.getMarketPlaceConfigId() == marketToListEbay.getMarketConfigId() &&
                                        o.getStatus() == StatusMarketListing.DE_LISTED
                        ).collect(Collectors.toList())
                );
        }

    }

    private void reListMarketListingBBB(List<MarketListing> marketListingBBB){
        if(marketListingBBB.size() == 0){
            return;
        }
        marketListingBBB = marketListingBBB.stream().peek(marketListing->{
//            marketListing.setTimeListed(LocalDateTime.now());
            marketListing.setCountListingError(0);
            marketListing.setStatus(StatusMarketListing.LISTED);
        }).collect(Collectors.toList());

        marketListingBBB =  marketListingRepository.saveAll(marketListingBBB);
        List<Long> inventoryIds = marketListingBBB.stream().map(MarketListing::getInventoryId).collect(Collectors.toList());
        CommonUtils.stream(inventoryIds);
        List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIds);
        inventories = inventories.stream().peek(o->o.setStage(StageInventory.LISTED)).collect(Collectors.toList());
        inventoryRepository.saveAll(inventories);
    }

    private void reListMarketListingEbay(List<MarketListing> marketListingEbay){
        if(marketListingEbay.size() == 0){
            return;
        }
        for (MarketListing marketListing : marketListingEbay) {
//            if (marketListing.getStatus() == StatusMarketListing.LISTING_ERROR) {
//                marketListing.setStatus(StatusMarketListing.LISTING);
//            } else {
//                marketListing.setCountListingError(marketListing.getCountListingError() + 1);
//            }
            marketListing.setStatus(StatusMarketListing.LISTING);
        }
        marketListingEbay = marketListingRepository.saveAll(marketListingEbay);
        marketListingManagerAsync.listEbay(marketListingEbay, null);
    }

    private void checkExpireOfferAuction(ExpirationTimeConfig expirationTime) {
        List<OfferInventoryAuction> expiredOffers = offerInventoryAuctionRepository.findAcceptedOfferNotYetPaid(
                (int)(expirationTime.getExpireAfterHours() * Constants.HOUR / Constants.MINUTE)
        );

        if (expiredOffers.size() > 0) {
            List<Long> offerIds = expiredOffers.stream()
                    .map(OfferInventoryAuction::getOfferId)
                    .collect(Collectors.toList());
            List<Offer> offers =
                    offerRepository.findAllByIds(offerIds)
                            .stream()
                            .peek(offer -> {
                                offer.setStatus(StatusOffer.CLOSED_CART_EXPIRED);
                                offer.setLastUpdate(null);
                            })
                            .collect(Collectors.toList());

            List<Long> inventoryAuctionIds = offers.stream()
                    .filter(o->o.getInventoryAuctionId() != null)
                    .map(Offer::getInventoryAuctionId).collect(Collectors.toList());
            if (inventoryAuctionIds.size() > 0 ){
                List<InventoryAuction> inventoryAuctions = inventoryAuctionRepository.findAllFromIds(inventoryAuctionIds, true, false);
                if (inventoryAuctions.size() > 0) {
                    List<Long> auctionIds = inventoryAuctions.stream().map(InventoryAuction::getAuctionId).collect(Collectors.toList());
                    CommonUtils.stream(auctionIds);
                    List<Auction> auctions = auctionRepository.findAllById(auctionIds);
                    LocalDateTime nowTime = LocalDateTime.now();

                    inventoryAuctions = inventoryAuctions.stream().peek(
                            in -> {
                                for (Auction auction : auctions) {
                                    if (in.getAuctionId() == auction.getId()) {
                                        if (nowTime.toDate().getTime() - auction.getEndDate().toDate().getTime() > 0) {
                                            in.setInactive(true);
                                        } else {
                                            in.setInactive(false);
                                            in.setWinnerId(null);
                                        }
                                        break;
                                    }
                                }
                                in.setLastUpdate(null);
                            }
                    ).collect(Collectors.toList());
                    inventoryAuctionRepository.saveAll(inventoryAuctions);
                    updateStatusInventoryOfferAuctionExpire(inventoryAuctions);
                }
            }
            offerRepository.saveAll(offers);

            List<Observable<ScheduleAddCartResponse>> observables = expiredOffers.stream()
                    .map(this::createObservableRemoveCartAndSendMail)
                    .collect(Collectors.toList());
            waitRemoveCartLogIfFail(observables);
        }
    }

    private void updateStatusInventoryOfferAuctionExpire(List<InventoryAuction> inventoryAuctions){
        List<Long> inventoryIds = inventoryAuctions.stream().map(InventoryAuction::getInventoryId).collect(Collectors.toList());
        CommonUtils.stream(inventoryIds);
        if ( inventoryIds.size() > 0 ){
            inventoryRepository.updateStage(inventoryIds, StageInventory.READY_LISTED.getValue());
        }

    }


    public void onFail(Throwable throwable) {}

    private Observable<ScheduleAddCartResponse> createObservableRemoveCartAndSendMail(OfferInventoryAuction offer) {
        return Observable.create((ObservableOnSubscribe<ScheduleAddCartResponse>) su -> {
            ScheduleAddCartResponse addCartResponse = new ScheduleAddCartResponse();
            addCartResponse.setOfferId(offer.getOfferId());
            try {
                CartResponse cartAdditionResponse = billingManager.removeCart(offer.getBuyerId(), offer.getInventoryId());
                addCartResponse.setCartAddResponse(cartAdditionResponse);
            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError;
                if (ex.getStatusCode() != HttpStatus.BAD_REQUEST) {
                    msgError =
                            objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                    msgError.setStatusHttp(ex.getStatusCode().value());
                    addCartResponse.setCartErrorResponse(msgError);
                    su.onNext(addCartResponse);
                    su.onComplete();
                    return;
                }
                msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                addCartResponse.setCartErrorResponse(msgError);
            }

            try {
                String bicycleName = offer.getBicycleName();
                if (bicycleName == null) {
                    bicycleName = offer.getInventoryName();
                }
                SendingMailResponse sendingMailResponse;
                if (offer.getAuctionId() == null) {
                    sendingMailResponse = notificationManager.sendMailExpireOfferAccepted(
                            offer.getBuyerId(), null, offer.getInventoryAuctionId(), //offer.getInventoryAuctionId(),
                            offer.getOfferId(),  offer.getImageDefaultInv(), bicycleName);
                } else {
                    sendingMailResponse = notificationManager.sendMailAuctionExpired(offer);
                }
                addCartResponse.setSendingMailResponse(sendingMailResponse);

            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                addCartResponse.setMailErrorResponse(msgError);
            }
            su.onNext(addCartResponse);
            su.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    private Observable<ScheduleAddCartResponse> createObservableRemoveCartAndSendMail(OfferMarketListing offer) {
        return Observable.create((ObservableOnSubscribe<ScheduleAddCartResponse>) su -> {
            ScheduleAddCartResponse addCartResponse = new ScheduleAddCartResponse();
            addCartResponse.setOfferId(offer.getOfferId());
            try {
                CartResponse cartAdditionResponse = billingManager.removeCart(
                        offer.getBuyerId(), offer.getInventoryId());
                addCartResponse.setCartAddResponse(cartAdditionResponse);
            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError;
                if (ex.getStatusCode() != HttpStatus.BAD_REQUEST) {
                    msgError =
                            objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                    msgError.setStatusHttp(ex.getStatusCode().value());
                    addCartResponse.setCartErrorResponse(msgError);
                    su.onNext(addCartResponse);
                    su.onComplete();
                    return;
                }
                msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                addCartResponse.setCartErrorResponse(msgError);
            }

            //send email for buyer
            try {
                String bicycleName = offer.getBicycleName();
                if (bicycleName == null) {
                    bicycleName = offer.getInventoryName();
                }
                SendingMailResponse sendingMailResponse;
                sendingMailResponse = notificationManager.sendMailExpireOfferAccepted(
                        offer.getBuyerId(), offer.getMarketListingId(), null,
                        offer.getOfferId(),  offer.getImageDefaultInv(), bicycleName);
                addCartResponse.setSendingMailResponse(sendingMailResponse);

                if ( offer.getSellerId() != null ) {
                    notificationManager.sendMailExpireOfferAccepted(
                            offer.getSellerId(), offer.getMarketListingId(), null,
                            offer.getOfferId(),  offer.getImageDefaultInv(), bicycleName);
                }
            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                addCartResponse.setMailErrorResponse(msgError);
            }

            su.onNext(addCartResponse);
            su.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    private void waitRemoveCartLogIfFail(List<Observable<ScheduleAddCartResponse>> observables) {
        List<ScheduleAddCartResponse> res = Observable.zip(observables, listRemoveCart -> {
            List<ScheduleAddCartResponse> addCartResponses = new ArrayList<>();
            for (Object o : listRemoveCart) {
                addCartResponses.add((ScheduleAddCartResponse) o);
            }
            return addCartResponses;
        }).blockingFirst();

        List<ErrorRestResponse> errorRestResponses = res.stream().map(o -> {
            if (o.getSendingMailResponse() == null || o.getCartErrorResponse() != null) {
                return o.getCartErrorResponse();
            } else {
                return null;
            }
        }).filter(
                Objects::nonNull
        ).collect(Collectors.toList());

        if (errorRestResponses.size() > 0) {
            setFail(true);
            try {
                setCustomLogString(objectMapper.writeValueAsString(errorRestResponses));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }
}
