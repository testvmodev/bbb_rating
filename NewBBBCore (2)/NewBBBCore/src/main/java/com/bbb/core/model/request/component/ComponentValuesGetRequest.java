package com.bbb.core.model.request.component;

import com.bbb.core.model.request.sort.ComponentValueSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class ComponentValuesGetRequest {
    private long compTypeId;
    private ComponentValueSortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public long getCompTypeId() {
        return compTypeId;
    }

    public void setCompTypeId(long compTypeId) {
        this.compTypeId = compTypeId;
    }

    public ComponentValueSortField getSortField() {
        return sortField;
    }

    public void setSortField(ComponentValueSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
