package com.bbb.core.model.database.type;


public enum TypeCancelTradeIn {
    BY_ADMIN("By admin"),
    BY_PARTNER("By partner");
    private final String value;

    TypeCancelTradeIn(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TypeCancelTradeIn findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "By admin":
                return BY_ADMIN;
            case "By partner":
                return BY_PARTNER;
            default:
                return null;
        }
    }
}
