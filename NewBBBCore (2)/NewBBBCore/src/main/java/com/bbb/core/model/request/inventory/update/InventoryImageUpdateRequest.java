package com.bbb.core.model.request.inventory.update;

import java.util.List;

public class InventoryImageUpdateRequest {
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
