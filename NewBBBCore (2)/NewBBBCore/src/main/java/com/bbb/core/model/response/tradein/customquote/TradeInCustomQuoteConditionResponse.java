package com.bbb.core.model.response.tradein.customquote;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.response.tradein.ConditionDetailResponse;

public class TradeInCustomQuoteConditionResponse extends ConditionDetailResponse {
    private boolean isSelect;
    public TradeInCustomQuoteConditionResponse(ConditionInventory condition, float percent, String message, float priceIncrease) {
        super(condition, percent, message, priceIncrease);
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
