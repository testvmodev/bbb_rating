package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TrackingType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TrackingTypeConverter implements AttributeConverter<TrackingType, String> {
    @Override
    public String convertToDatabaseColumn(TrackingType trackingType) {
        if ( trackingType == null ){
            return null;
        }
        return trackingType.getValue();
    }

    @Override
    public TrackingType convertToEntityAttribute(String s) {
        if ( s == null ) {
            return null;
        }
        for (TrackingType value : TrackingType.values()) {
            if ( value.getValue().equals(s)){
                return value;
            }
        }
        return null;
    }
}
