package com.bbb.core.model.otherservice.response.order;

import com.bbb.core.model.database.type.OutboundShippingType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderLineItem {
    @JsonProperty("cart_type")
    private String cartType;
    @JsonProperty("seller_id")
    private String sellerId;
    @JsonProperty("inventory_id")
    private Long inventoryId;
    @JsonProperty("market_listing_id")
    private Long marketListingId;
    @JsonProperty("buyer_id")
    private String buyerId;
    @JsonProperty("shipping_type")
    private OutboundShippingType shippingType;
    private String currency;
    private Float shipping;
    private Float insurance;
    private Float tax;
    @JsonProperty("local_pickup")
    private Boolean localPickup;
    @JsonProperty("current_listed_price")
    private Float currentListedPrice;
    @JsonProperty("price_offer")
    private Float priceOffer;
    @JsonProperty("paypal_email_seller")
    private String paypalEmailSeller;
    //.....


    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getShipping() {
        return shipping;
    }

    public void setShipping(Float shipping) {
        this.shipping = shipping;
    }

    public Float getInsurance() {
        return insurance;
    }

    public void setInsurance(Float insurance) {
        this.insurance = insurance;
    }

    public Float getTax() {
        return tax;
    }

    public void setTax(Float tax) {
        this.tax = tax;
    }

    public Boolean getLocalPickup() {
        return localPickup;
    }

    public void setLocalPickup(Boolean localPickup) {
        this.localPickup = localPickup;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getPriceOffer() {
        return priceOffer;
    }

    public void setPriceOffer(Float priceOffer) {
        this.priceOffer = priceOffer;
    }

    public String getPaypalEmailSeller() {
        return paypalEmailSeller;
    }

    public void setPaypalEmailSeller(String paypalEmailSeller) {
        this.paypalEmailSeller = paypalEmailSeller;
    }
}
