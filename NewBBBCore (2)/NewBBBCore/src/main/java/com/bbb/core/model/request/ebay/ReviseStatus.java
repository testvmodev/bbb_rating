package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ReviseStatus {
    @JacksonXmlProperty(localName = "ItemRevised")
    private Boolean itemRevised;

    public Boolean getItemRevised() {
        return itemRevised;
    }

    public void setItemRevised(Boolean itemRevised) {
        this.itemRevised = itemRevised;
    }
}
