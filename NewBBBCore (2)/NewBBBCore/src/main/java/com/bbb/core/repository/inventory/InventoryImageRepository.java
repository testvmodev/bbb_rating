package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryImageRepository extends JpaRepository<InventoryImage, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory_image")
    List<InventoryImage> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_image " +
            "WHERE inventory_id = :inventoryId")
    List<InventoryImage> findAllByInventoryId(
            @Param(value = "inventoryId") long inventoryId
    );


    @Query(nativeQuery = true, value = "SELECT * FROM inventory_image " +
            "WHERE inventory_id IN :inventoriesId")
    List<InventoryImage> findAllByInventoriesId(@Param(value = "inventoriesId") List<Long> inventoriesId);


    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_image " +
            "WHERE inventory_id = :inventoryId " +
            "AND id IN :inventoryImageIds")
    List<InventoryImage> findByImageIds(
            @Param("inventoryId") long inventoryId,
            @Param("inventoryImageIds") List<Long> inventoryImageIds
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_image " +
            "WHERE id IN :inventoryImageIds")
    List<InventoryImage> findByImageIds(
            @Param("inventoryImageIds") List<Long> inventoryImageIds
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM inventory_image " +
            "WHERE inventory_id = :inventoryId")
    InventoryImage findOneByInventory(
            @Param("inventoryId") long inventoryId
    );
}
