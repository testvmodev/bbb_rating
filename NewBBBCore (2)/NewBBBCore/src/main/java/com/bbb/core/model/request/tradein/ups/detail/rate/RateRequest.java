package com.bbb.core.model.request.tradein.ups.detail.rate;

import com.bbb.core.model.request.tradein.ups.detail.RequestConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RateRequest {
    @JsonProperty("Request")
    private RequestConfig requestConfig;
    @JsonProperty("Shipment")
    private UPSShipment UPSShipment;

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    public UPSShipment getUPSShipment() {
        return UPSShipment;
    }

    public void setUPSShipment(UPSShipment UPSShipment) {
        this.UPSShipment = UPSShipment;
    }
}
