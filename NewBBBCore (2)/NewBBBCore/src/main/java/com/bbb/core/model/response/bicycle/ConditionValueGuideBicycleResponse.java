package com.bbb.core.model.response.bicycle;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.response.tradein.ConditionDetailResponse;

public class ConditionValueGuideBicycleResponse extends ConditionDetailResponse {
    private Float tradeInValue;
    private Float privatePartyValueAvg;
    private Float privatePartyValueMin;
    private Float privatePartyValueMax;

    public ConditionValueGuideBicycleResponse(
            ConditionInventory condition, float percent, String message,
            float tradeInValue,
            float privatePartyValueAvg, float privatePartyValueMin, float privatePartyValueMax
    ) {
        super(condition, percent, message, null);
        this.tradeInValue = tradeInValue;
        this.privatePartyValueAvg = privatePartyValueAvg;
        this.privatePartyValueMin = privatePartyValueMin;
        this.privatePartyValueMax = privatePartyValueMax;
    }

    public ConditionValueGuideBicycleResponse(
            ConditionInventory condition, float percent, String message,
            float privatePartyValueAvg
    ) {
        super(condition, percent, message, null);
        this.privatePartyValueAvg = privatePartyValueAvg;
    }

    public Float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(Float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    public Float getPrivatePartyValueAvg() {
        return privatePartyValueAvg;
    }

    public void setPrivatePartyValueAvg(Float privatePartyValueAvg) {
        this.privatePartyValueAvg = privatePartyValueAvg;
    }

    public Float getPrivatePartyValueMin() {
        return privatePartyValueMin;
    }

    public void setPrivatePartyValueMin(Float privatePartyValueMin) {
        this.privatePartyValueMin = privatePartyValueMin;
    }

    public Float getPrivatePartyValueMax() {
        return privatePartyValueMax;
    }

    public void setPrivatePartyValueMax(Float privatePartyValueMax) {
        this.privatePartyValueMax = privatePartyValueMax;
    }
}
