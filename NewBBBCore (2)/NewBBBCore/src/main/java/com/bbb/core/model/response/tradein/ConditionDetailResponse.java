package com.bbb.core.model.response.tradein;

import com.bbb.core.model.database.type.ConditionInventory;

public class ConditionDetailResponse {
    private ConditionInventory condition;
    private float percent;
    private String message;
    private Float priceIncrease;

    public ConditionDetailResponse(ConditionInventory condition) {
        this.condition = condition;
    }

    public ConditionDetailResponse(ConditionInventory condition, float percent, String message, Float priceIncrease) {
        this.condition = condition;
        this.percent = percent;
        this.message = message;
        this.priceIncrease = priceIncrease;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Float getPriceIncrease() {
        return priceIncrease;
    }

    public void setPriceIncrease(Float priceIncrease) {
        this.priceIncrease = priceIncrease;
    }
}
