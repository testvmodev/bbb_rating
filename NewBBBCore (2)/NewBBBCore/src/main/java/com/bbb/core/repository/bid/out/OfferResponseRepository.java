package com.bbb.core.repository.bid.out;

import com.bbb.core.model.database.Offer;
import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.request.sort.OfferReceiverFieldSort;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.bid.offer.OfferResponse;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferResponseRepository extends JpaRepository<OfferResponse, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id, " +
            "offer.buyer_id, " +
            "offer.offer_price, " +
            "offer.status, " +
            "offer.created_time, " +
            "offer.last_update, " +
            "offer.inventory_auction_id, " +
            "offer.market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.seller_id, " +
            "inventory.partner_id, " +
            "inventory.current_listed_price, " +
            "inventory.image_default AS image_default " +
            "FROM offer " +
            "LEFT JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
            "LEFT JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id OR market_listing.inventory_id = inventory.id " +

            "WHERE " +
            "offer.is_delete = 0 AND " +
            "(:buyerId IS NULL OR offer.buyer_id = :buyerId) AND " +
            "(:status IS NULL OR :status = offer.status) AND " +
            "(:inventoryAuctionId IS NULL OR :inventoryAuctionId = offer.inventory_auction_id) AND " +
            "(:marketListingId IS NULL OR :marketListingId = offer.market_listing_id) "+
            "ORDER BY " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.created_time END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.created_time END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.title END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.title END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).INVENTORY_NAME.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.name END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).INVENTORY_NAME.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.name END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).SELLER_ID.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN (CASE WHEN inventory.seller_id IS NOT NULL THEN inventory.seller_id ELSE inventory.partner_id END) END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).SELLER_ID.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN (CASE WHEN inventory.seller_id IS NOT NULL THEN inventory.seller_id ELSE inventory.partner_id END) END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).OFFER_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.offer_price END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).OFFER_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.offer_price END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).CURRENT_LISTED_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.current_listed_price END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).CURRENT_LISTED_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.current_listed_price END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).OFFER_STATUS.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.status END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferSort).OFFER_STATUS.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.status END ASC, " +
            "CASE WHEN :fieldSort <> :#{T(com.bbb.core.model.request.sort.OfferSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.created_time END DESC, " +
            "CASE WHEN :fieldSort <> :#{T(com.bbb.core.model.request.sort.OfferSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.created_time END ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<OfferResponse> findAll(
            @Param(value = "buyerId") String buyerId,
            @Param(value = "status") String status,
            @Param(value = "inventoryAuctionId") Long inventoryAuctionId,
            @Param(value = "marketListingId") Long marketListingId,
            @Param(value = "fieldSort") String fieldSort,
            @Param(value = "sort") String sort,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );


//    CREATED_DATE,TITLE,CURRENT_LISTED_PRICE,OFFER_PRICE,OFFER_STATUS
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id, " +
            "offer.buyer_id, " +
            "offer.offer_price, " +
            "offer.status, " +
            "offer.created_time, " +
            "offer.last_update, " +
            "offer.inventory_auction_id, " +
            "offer.market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.seller_id, " +
            "inventory.partner_id, " +
            "inventory.current_listed_price, " +
            "inventory.image_default AS image_default " +
            "FROM offer " +
            "LEFT JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
            "LEFT JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id OR market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +

            "WHERE " +
            "offer.is_delete = 0 AND " +
            "inventory.seller_id = :sellerId AND " +
            "inventory_type.name = :#{T(com.bbb.core.common.ValueCommons).PTP} " +
            "ORDER BY " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.created_time END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CREATED_DATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.created_time END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.title END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.title END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CURRENT_LISTED_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.current_listed_price END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CURRENT_LISTED_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.current_listed_price END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.offer_price END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.offer_price END ASC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_STATUS.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.status END DESC, " +
            "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_STATUS.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.status END ASC, " +
            "CASE WHEN :fieldSort <> :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).LAST_UPDATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.last_update END DESC, " +
            "CASE WHEN :fieldSort <> :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).LAST_UPDATE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.last_update END ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<OfferResponse> findAllReceiverMyListing(
            @Param(value = "sellerId") String sellerId,
            @Param(value = "fieldSort") String fieldSort,
            @Param(value = "sort") String sort,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );


    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id) FROM offer " +
            "WHERE " +
            "offer.is_delete = 0 AND " +
            "(:buyerId IS NULL OR offer.buyer_id = :buyerId) AND " +
            "(:status IS NULL OR :status = offer.status) AND " +
            "(:inventoryAuctionId IS NULL OR :inventoryAuctionId = offer.inventory_auction_id) AND " +
            "(:marketListingId IS NULL OR :marketListingId = offer.market_listing_id) "
    )
    int countTotalNumber(
            @Param(value = "buyerId") String buyerId,
            @Param(value = "status") String status,
            @Param(value = "inventoryAuctionId") Long inventoryAuctionId,
            @Param(value = "marketListingId") Long marketListingId
    );


    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id) FROM " +
            "offer " +
            "LEFT JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
            "LEFT JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id OR market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "WHERE " +
            "offer.is_delete = 0 AND " +
            "inventory.seller_id = :sellerId AND " +
            "inventory_type.name = :#{T(com.bbb.core.common.ValueCommons).PTP} "
    )
    int countTotalNumberReceiverMyListing(
            @Param(value = "sellerId") String sellerId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id, " +
            "offer.buyer_id, " +
            "offer.offer_price, " +
            "offer.status, " +
            "offer.created_time, " +
            "offer.last_update, " +
            "offer.inventory_auction_id, " +
            "offer.market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.seller_id, " +
            "inventory.partner_id, " +
            "inventory.current_listed_price, " +
            "inventory.image_default AS image_default " +
            "FROM offer " +
            "JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +

            "WHERE " +
            "offer.is_delete = 0 " +
            //TODO spring-data bug
            "AND inventory_type.name = 'PTP' " +
//            "AND inventory_type.name = :#{T(com.bbb.core.common.ValueCommons).PTP} " +
            "AND inventory.storefront_id IS NOT NULL " +
            "AND inventory.storefront_id = :storefrontId " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                "offer.status IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapOfferStatusToString(#statuses)}) " +
            "AND (:content IS NULL OR (" +
                "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), inventory.current_listed_price) LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), offer.offer_price) LIKE :#{'%' + #content + '%'})) " +
            "AND (:fromDay IS NULL OR CAST(offer.last_update AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(offer.last_update AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) " +


            "ORDER BY " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CREATED_DATE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.created_time END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CREATED_DATE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.created_time END ASC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).TITLE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.title END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).TITLE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.title END ASC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CURRENT_LISTED_PRICE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.current_listed_price END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).CURRENT_LISTED_PRICE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.current_listed_price END ASC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_PRICE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.offer_price END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_PRICE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.offer_price END ASC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_STATUS.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.status END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).OFFER_STATUS.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.status END ASC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).LAST_UPDATE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.last_update END DESC, " +
            "CASE WHEN :#{#fieldSort.name()} = :#{T(com.bbb.core.model.request.sort.OfferReceiverFieldSort).LAST_UPDATE.name()} AND :#{#sort.name()} = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.last_update END ASC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<OfferResponse> findAllOnlineStoreReceived(
            @Param(value = "storefrontId") String storefrontId,
            @Param("statuses") List<StatusOffer> statuses,
            @Param("content") String content,
            @Param("fromDay") LocalDate fromDay,
            @Param("toDay") LocalDate toDay,
            @Param(value = "fieldSort") OfferReceiverFieldSort fieldSort,
            @Param(value = "sort") Sort sort,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id) " +
            "FROM offer " +
            "JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "WHERE " +
            "offer.is_delete = 0 " +
            //TODO spring-data bug
            "AND inventory_type.name = 'PTP' " +
//            "AND inventory_type.name = :#{T(com.bbb.core.common.ValueCommons).PTP} " +
            "AND inventory.storefront_id IS NOT NULL " +
            "AND inventory.storefront_id = :storefrontId " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                "offer.status IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapOfferStatusToString(#statuses)}) " +
            "AND (:content IS NULL OR (" +
                "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), inventory.current_listed_price) LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), offer.offer_price) LIKE :#{'%' + #content + '%'})) " +
            "AND (:fromDay IS NULL OR CAST(offer.created_time AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(offer.created_time AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) "
    )
    int countOnlineStoreReceived(
            @Param(value = "storefrontId") String storefrontId,
            @Param("statuses") List<StatusOffer> statuses,
            @Param("content") String content,
            @Param("fromDay") LocalDate fromDay,
            @Param("toDay") LocalDate toDay
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id, " +
            "offer.buyer_id, " +
            "offer.offer_price, " +
            "offer.status, " +
            "offer.created_time, " +
            "offer.last_update, " +
            "offer.inventory_auction_id, " +
            "offer.market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.seller_id, " +
            "inventory.partner_id, " +
            "inventory.current_listed_price, " +
            "inventory.image_default AS image_default " +
            "FROM offer " +
            "JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +

            "WHERE " +
            "offer.is_delete = 0 " +
            "AND market_listing.id = :marketListingId "
    )
    List<OfferResponse> findAllOffersMarketListing(
            @Param(value = "marketListingId") long marketListingId
    );
}
