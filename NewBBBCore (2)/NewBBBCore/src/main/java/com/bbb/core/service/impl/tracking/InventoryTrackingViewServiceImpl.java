package com.bbb.core.service.impl.tracking;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.tracking.InventoryTrackingViewManager;
import com.bbb.core.service.tracking.InventoryTrackingViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InventoryTrackingViewServiceImpl implements InventoryTrackingViewService {
    @Autowired
    private InventoryTrackingViewManager manager;

    @Override
    public Object getBestViews(Pageable page) throws ExceptionResponse {
        return manager.getBestViews(page);
    }

    @Override
    public Object getRecentTrackingView(Pageable page) throws ExceptionResponse{
        return manager.getRecentTrackingView(page);
    }
}
