package com.bbb.core.model.request.tradein.ups.detail;

import com.bbb.core.model.request.tradein.ups.detail.ship.Phone;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipAddress {
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Phone")
    private Phone phone;
    @JsonProperty("Address")
    private Address address;

    public ShipAddress() {}

    public ShipAddress(Address address) {
        this.address = address;
    }

    public ShipAddress(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
