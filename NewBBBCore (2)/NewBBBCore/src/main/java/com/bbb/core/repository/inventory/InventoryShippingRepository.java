package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryShipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryShippingRepository extends JpaRepository<InventoryShipping, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM inventory_shipping " +
            "WHERE inventory_id = :inventoryId")
    InventoryShipping findByInventoryId(@Param("inventoryId") long inventoryId);
}
