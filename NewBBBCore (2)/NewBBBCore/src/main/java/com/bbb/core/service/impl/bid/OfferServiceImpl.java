package com.bbb.core.service.impl.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.model.request.bid.offer.*;
import com.bbb.core.model.request.bid.offer.offerbid.OfferBidRequest;
import com.bbb.core.model.request.bid.offer.offerbid.OffersReceiverRequest;
import com.bbb.core.model.request.bid.offer.test.OfferType;
import com.bbb.core.model.request.sort.OfferBidFieldSort;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bid.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {
    @Autowired
    private OfferManager manager;

    @Override
    public Object getOffers(OfferRequest request, Pageable page) {
        return manager.getOffers(request, page);
    }

    @Override
    public Object getOffersReceiverMyListing(OffersReceiverRequest request, Pageable page) throws ExceptionResponse {
        return manager.getOffersReceiver(request, page);
    }

    @Override
    public Object getOffersOnlineStore(OffersOnlineStoreRequest request) throws ExceptionResponse {
        return manager.getOffersOnlineStore(request);
    }

    @Override
    public Object getOffersMarketListing(long marketListingId) throws ExceptionResponse {
        return manager.getOffersMarketListing(marketListingId);
    }

    @Override
    public Object createOffer(OfferCreateRequest request) throws ExceptionResponse {
        return manager.createOffer(request);
    }

    @Override
    public Object updateOffer(long offerId, OfferUpdateRequest request) throws ExceptionResponse {
        return manager.updateOffer(offerId, request);
    }

    @Override
    public Object updateOfferBuyer(long offerId, boolean isAccepted) throws ExceptionResponse {
        return manager.updateOfferBuyer(offerId, isAccepted);
    }

    @Override
    public Object getDetailOffer(long offerId) throws ExceptionResponse {
        return manager.getDetailOffer(offerId);
    }

    @Override
    public Object payOfferAccepted(long offerId, boolean isPaid) throws ExceptionResponse {
        return manager.payOfferAccepted(offerId, isPaid);
    }

    @Override
    public Object getMarketListingAuctionsService(List<SecretMarketLisitingAuctionRequest> requests) throws ExceptionResponse {
        return manager.getMarketListingAuctionsService(requests);
    }

//    @Override
//    public Object getMarketListingAuctionsServiceDetail(SecretMarketLisitingAuctionRequest request) throws ExceptionResponse {
//        return manager.getMarketListingAuctionsServiceDetail(request);
//    }


    @Override
    public Object getOffersBid(OfferBidRequest request, Pageable page) throws ExceptionResponse {
        return manager.getOffersBid(request, page);
    }

    @Override
    public Object getOfferBid(long inventoryAuctionId, OfferBidFieldSort fieldSort, Sort sortType) throws ExceptionResponse {
        return manager.getOfferBid(inventoryAuctionId,  fieldSort, sortType);
    }

    @Override
    public Object getInventoryAuctionBids(long inventoryAuctionId, OfferBidFieldSort offerBidFieldSort, Sort sortType) throws ExceptionResponse {
        return manager.getInventoryAuctionBids(inventoryAuctionId, offerBidFieldSort, sortType);
    }

    @Override
    public Object offerIntegrationTest(OfferType offerType) throws ExceptionResponse {
        return manager.offerIntegrationTest(offerType);
    }

    @Override
    public Object removeCartOffer(long offerId) throws ExceptionResponse {
        return manager.removeCartOffer(offerId);
    }

    @Override
    public Object getUserOffers(UserOfferRequest request) throws ExceptionResponse {
        return manager.getUserOffers(request);
    }

    @Override
    public Object getUserBids(UserBidRequest request) throws ExceptionResponse {
        return manager.getUserBids(request);
    }

    @Override
    public Object getOfferDetailSecret(long offerId) throws ExceptionResponse {
        return manager.getOfferDetailSecret(offerId);
    }
}
