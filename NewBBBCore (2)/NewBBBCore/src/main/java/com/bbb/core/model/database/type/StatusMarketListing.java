package com.bbb.core.model.database.type;

public enum  StatusMarketListing {
    PENDING("Pending"),
    LISTING("Listing"),
    LISTING_ERROR("Listing error"),
    LISTED("Listed"),
    QUEUE("Queue"),
    DE_LISTED("De-Listed"),
    SOLD("Sold"),
    SALE_PENDING("Sale pending"),
    ERROR_SYNC("Error-sync"),
    ERROR("Error"),
    DRAFT("Draft"),
    EXPIRED("Expired");

    private final String value;

    StatusMarketListing(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
