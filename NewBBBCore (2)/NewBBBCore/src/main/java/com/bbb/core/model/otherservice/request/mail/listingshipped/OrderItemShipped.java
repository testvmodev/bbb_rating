package com.bbb.core.model.otherservice.request.mail.listingshipped;

public class OrderItemShipped {
    private String carrier;
    private String tracking;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }
}
