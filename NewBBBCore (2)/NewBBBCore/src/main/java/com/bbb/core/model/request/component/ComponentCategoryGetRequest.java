package com.bbb.core.model.request.component;

import com.bbb.core.model.request.sort.ComponentCategorySortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class ComponentCategoryGetRequest {
    private ComponentCategorySortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public ComponentCategorySortField getSortField() {
        return sortField;
    }

    public void setSortField(ComponentCategorySortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
