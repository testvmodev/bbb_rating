package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.ComponentTypeSelect;
import com.bbb.core.model.request.bicycle.CompTypeSelectCreateRequest;
import com.bbb.core.model.request.bicycle.CompTypeSelectUpdateRequest;
import com.bbb.core.model.request.component.ComponentValuesGetRequest;
import com.bbb.core.model.request.sort.ComponentValueSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ComponentTypeSelectService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ComponentTypeSelectController {
    @Autowired
    private ComponentTypeSelectService service;

    @PostMapping(value = Constants.URL_CREATE_COMPONENT_TYPE_VALUE)
    public ResponseEntity createComponentValues(@RequestBody CompTypeSelectCreateRequest request) throws ExceptionResponse{
        return new ResponseEntity<>(service.createComponentValues(request), HttpStatus.OK);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    @GetMapping(value = Constants.URL_GET_DETAIL_COMPONENT_TYPE_VALUE)
    public ResponseEntity getDetailComponentValues(
            @PathVariable(value = Constants.ID) long id,
            @RequestParam("sortField") ComponentValueSortField sortField,
            @RequestParam(value = "sortType", defaultValue = "ASC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse{
        ComponentValuesGetRequest request = new ComponentValuesGetRequest();
        request.setCompTypeId(id);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);
        return new ResponseEntity<>(service.getDetailComponentValues(request), HttpStatus.OK);
    }

    @GetMapping(Constants.URL_GET_ONE_COMPONENT_VALUE)
    public ResponseEntity getOneComponentValue(
            @PathVariable(value = Constants.ID) long componentValueId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getOneComponentValue(componentValueId));
    }

    @PutMapping(value = Constants.URL_UPDATE_COMPONENT_TYPE_VALUE )
    public ResponseEntity updateComponentValues(@PathVariable(value = Constants.ID) long id,
                                                @RequestBody CompTypeSelectUpdateRequest request) throws ExceptionResponse{
        return new ResponseEntity<>(service.updateComponentValue(id, request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_DELETE_COMPONENT_TYPE_VALUE)
    public ResponseEntity deleteComponentValue(@PathVariable(value = Constants.ID) long id) throws ExceptionResponse{
        return new ResponseEntity<>(service.deleteComponentValue(id), HttpStatus.OK);
    }

}
