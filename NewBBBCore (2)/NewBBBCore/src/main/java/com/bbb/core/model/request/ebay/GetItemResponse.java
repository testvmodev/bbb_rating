package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class GetItemResponse {
    @JacksonXmlProperty(localName = "-xmlns")
    private String xmlns;

    @JacksonXmlProperty(localName = "Timestamp")
    private LocalDateTime imestamp;
    @JacksonXmlProperty(localName = "Ack")
    private String ack;

    @JacksonXmlProperty(localName = "CorrelationID")
    private long correlationID;

    @JacksonXmlProperty(localName = "Version")
    private int version;

    @JacksonXmlProperty(localName = "Build")
    private String build;

    @JacksonXmlProperty(localName = "NotificationEventName")
    private String notificationEventName;

    @JacksonXmlProperty(localName = "RecipientUserID")
    private String recipientUserID;

    @JacksonXmlProperty(localName = "EIASToken")
    private String eIASToken;

}
