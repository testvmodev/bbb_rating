package com.bbb.core.service.favourite;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.favourite.FavouriteAddRequest;
import com.bbb.core.model.request.favourite.FavouriteRemoveRequest;
import org.springframework.data.domain.Pageable;

public interface FavouriteService {
    Object getFavourites(Pageable page);

    Object addFavourite(FavouriteAddRequest favouriteAddRequest) throws ExceptionResponse;

    Object removeFavourite(FavouriteRemoveRequest FavouriteRemoveRequest) throws ExceptionResponse;
}
