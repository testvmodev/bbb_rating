package com.bbb.core.model.request.tradein.customquote;

import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import com.bbb.core.model.request.sort.CustomQuoteSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class TradeInCustomQuoteRequest {
    private String partnerId;
    private Boolean isArchived;
    private Boolean isIncomplete;
    private StatusTradeInCustomQuote customQuoteStatus;
    private String content;
    private CustomQuoteSortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public Boolean getArchived() {
        return isArchived;
    }

    public void setArchived(Boolean archived) {
        isArchived = archived;
    }

    public Boolean getIncomplete() {
        return isIncomplete;
    }

    public void setIncomplete(Boolean incomplete) {
        isIncomplete = incomplete;
    }

    public StatusTradeInCustomQuote getCustomQuoteStatus() {
        return customQuoteStatus;
    }

    public void setCustomQuoteStatus(StatusTradeInCustomQuote customQuoteStatus) {
        this.customQuoteStatus = customQuoteStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CustomQuoteSortField getSortField() {
        return sortField;
    }

    public void setSortField(CustomQuoteSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
