package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.component.ComponentCategoryCreateRequest;
import com.bbb.core.model.request.component.ComponentCategoryGetRequest;
import com.bbb.core.model.request.component.ComponentCategoryUpdateRequest;
import com.bbb.core.model.request.sort.ComponentCategorySortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ComponentCategoryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ComponentCategoryController {
    @Autowired
    private ComponentCategoryService service;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    @GetMapping(value = Constants.URL_GET_COMPONENT_CATEGORY)
    public ResponseEntity getComponentCategories(
            @RequestParam("sortField") ComponentCategorySortField sortField,
            @RequestParam("sortType") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse{
        ComponentCategoryGetRequest request = new ComponentCategoryGetRequest();
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);
        return new ResponseEntity<>(service.getComponentCategories(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_DETAIL_COMPONENT_CATEGORY)
    public ResponseEntity getDetailComponentCategory(@PathVariable(value = Constants.ID) long cateId) throws ExceptionResponse{
        return new ResponseEntity<>(service.getDetailComponentCategory(cateId), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_CREATE_COMPONENT_CATEGORY)
    public ResponseEntity createComponentCategory(@RequestBody  ComponentCategoryCreateRequest request) throws ExceptionResponse{
        return new ResponseEntity<>(service.createComponentCategory(request), HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_UPDATE_COMPONENT_CATEGORY)
    public ResponseEntity updateComponentCategory(
            @PathVariable(value = Constants.ID) long cateId,
            @RequestBody ComponentCategoryUpdateRequest request) throws ExceptionResponse{
        return new ResponseEntity<>(service.updateComponentCategory(cateId,request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_GET_DETAIL_COMPONENT_CATEGORY)
    public ResponseEntity deleteComponentCategory(
            @PathVariable(value = Constants.ID) long cateId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.deleteComponentCategory(cateId), HttpStatus.OK);
    }

}
