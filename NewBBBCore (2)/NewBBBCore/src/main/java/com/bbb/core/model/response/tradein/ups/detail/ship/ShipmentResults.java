package com.bbb.core.model.response.tradein.ups.detail.ship;

import com.bbb.core.model.response.tradein.ups.detail.BillingWeight;
import com.bbb.core.model.response.tradein.ups.detail.NegotiatedRateCharges;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentResults {
    @JsonProperty("PackageResults")
    private PackageResults packageResults;
    @JsonProperty("ShipmentCharges")
    private ShipmentCharges shipmentCharges;
    @JsonProperty("NegotiatedRateCharges")
    private NegotiatedRateCharges negotiatedRateCharges;
    @JsonProperty(value = "BillingWeight")
    private BillingWeight billingWeight;

    public PackageResults getPackageResults() {
        return packageResults;
    }

    public ShipmentCharges getShipmentCharges() {
        return shipmentCharges;
    }

    public NegotiatedRateCharges getNegotiatedRateCharges() {
        return negotiatedRateCharges;
    }

    public void setPackageResults(PackageResults packageResults) {
        this.packageResults = packageResults;
    }

    public void setShipmentCharges(ShipmentCharges shipmentCharges) {
        this.shipmentCharges = shipmentCharges;
    }

    public void setNegotiatedRateCharges(NegotiatedRateCharges negotiatedRateCharges) {
        this.negotiatedRateCharges = negotiatedRateCharges;
    }

    public BillingWeight getBillingWeight() {
        return billingWeight;
    }

    public void setBillingWeight(BillingWeight billingWeight) {
        this.billingWeight = billingWeight;
    }
}
