package com.bbb.core.model.otherservice.response.common;

import com.bbb.core.model.otherservice.response.common.google.ResultsResponse;

import java.util.List;

public class GoogleServiceResponse {
    private List<ResultsResponse> results;
    private String status;

    public List<ResultsResponse> getResults() {
        return results;
    }

    public void setResults(List<ResultsResponse> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
