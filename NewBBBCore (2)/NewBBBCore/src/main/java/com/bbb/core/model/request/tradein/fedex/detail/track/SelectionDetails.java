package com.bbb.core.model.request.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SelectionDetails {
    @JacksonXmlProperty(localName = "PackageIdentifier")
    private PackageIdentifier packageIdentifier;

    public PackageIdentifier getPackageIdentifier() {
        return packageIdentifier;
    }

    public void setPackageIdentifier(PackageIdentifier packageIdentifier) {
        this.packageIdentifier = packageIdentifier;
    }
}
