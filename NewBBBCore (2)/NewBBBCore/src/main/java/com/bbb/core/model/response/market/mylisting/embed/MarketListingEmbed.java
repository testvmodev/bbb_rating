package com.bbb.core.model.response.market.mylisting.embed;

import com.bbb.core.common.ValueCommons;
import com.bbb.core.model.database.InventorySale;
import com.bbb.core.model.database.type.*;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class MarketListingEmbed {
    private Long marketListingId;
    private Long inventoryId;
    private Long bicycleId;
    private Long modelId;
    private Long brandId;
    private Long yearId;
    private Long typeId;

    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    private String sizeName;

    private Long brakeTypeId;
    private String brakeName;
    private Long frameMaterialId;
    private String frameMaterialName;

    private Long marketPlaceId;
    private MarketType marketType;

//    private String imageDefault;
//    private StatusMarketListing statusMarketListing;

    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
//    private Float currentListedPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private Float discountedPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;

//    private String partnerId;
    private StatusInventory statusInventory;
    private StageInventory stageInventory;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private ConditionInventory condition;
    private String serialNumber;
    @Transient
    private InventorySale sale;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public String getBrakeName() {
        return brakeName;
    }

    public void setBrakeName(String brakeName) {
        this.brakeName = brakeName;
    }

    public Long getFrameMaterialId() {
        return frameMaterialId;
    }

    public void setFrameMaterialId(Long frameMaterialId) {
        this.frameMaterialId = frameMaterialId;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public Long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(Long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }

//    public String getImageDefault() {
//        return imageDefault;
//    }
//
//    public void setImageDefault(String imageDefault) {
//        this.imageDefault = imageDefault;
//    }

//    public StatusMarketListing getStatusMarketListing() {
//        return statusMarketListing;
//    }
//
//    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
//        this.statusMarketListing = statusMarketListing;
//    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

//    public Float getCurrentListedPrice() {
//        return currentListedPrice;
//    }
//
//    public void setCurrentListedPrice(Float currentListedPrice) {
//        this.currentListedPrice = currentListedPrice;
//    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public StatusInventory getStatusInventory() {
        return statusInventory;
    }

    public void setStatusInventory(StatusInventory statusInventory) {
        this.statusInventory = statusInventory;
    }

    public StageInventory getStageInventory() {
        return stageInventory;
    }

    public void setStageInventory(StageInventory stageInventory) {
        this.stageInventory = stageInventory;
    }

    public InventorySale getSale() {
        return sale;
    }

    public void setSale(InventorySale sale) {
        this.sale = sale;
    }
}
