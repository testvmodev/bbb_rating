package com.bbb.core.manager.tradein;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.tradein.async.ShippingAccountManagerAsync;
import com.bbb.core.model.database.TradeInShippingAccount;
import com.bbb.core.model.request.tradein.tradin.ShippingAccountCreateRequest;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.tradein.ShippingAccountRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShippingAccountManager {
    @Autowired
    private ShippingAccountRepository shippingAccountRepository;
    @Autowired
    private ShippingAccountManagerAsync shippingAccountManagerAsync;

    public Object getAccounts() {
        return shippingAccountRepository.findAll();
    }

    public Object addAccount(ShippingAccountCreateRequest request) throws ExceptionResponse {
        request.isValid();

        TradeInShippingAccount account = new TradeInShippingAccount();
        account.setCarrierType(request.getCarrierType());
        account.setUsername(request.getUsername());
        account.setPassword(request.getPassword());
        account.setApiKey(request.getApiKey());
        account.setApiPassword(request.getApiPassword());
        account.setAccountNumber(request.getAccountNumber());
        account.setMeterNumber(request.getMeterNumber());
        account.setServiceCode(request.getServiceCode());
        account.setPhone(request.getPhone());
        account.setWarehouseId(request.getWarehouseId());
        account.setSandbox(request.isSandbox());

        return shippingAccountRepository.save(account);
    }

    public Object deleteAccount(long id) throws ExceptionResponse {
        TradeInShippingAccount account = findAccount(id);
        shippingAccountRepository.deleteById(account.getId());
        return new MessageResponse(MessageResponses.SHIPPING_ACCOUNT_DELETED);
    }

    public Object activate(long id) throws ExceptionResponse {
        TradeInShippingAccount account = findAccount(id);

        shippingAccountManagerAsync.deactivateOther(id);

        account.setActive(true);
        shippingAccountRepository.save(account);
        return new MessageResponse(MessageResponses.SHIPPING_ACCOUNT_ACTIVATED);
    }

    private TradeInShippingAccount findAccount(long id) throws ExceptionResponse {
        TradeInShippingAccount account = shippingAccountRepository.findOne(id);
        if (account == null) {
            throw new ExceptionResponse(
                    new ObjectError(
                            ObjectError.ERROR_NOT_FOUND,
                            MessageResponses.SHIPPING_ACCOUNT_NOT_EXIST
                    ),
                    HttpStatus.NOT_FOUND
            );
        }
        return account;
    }
}
