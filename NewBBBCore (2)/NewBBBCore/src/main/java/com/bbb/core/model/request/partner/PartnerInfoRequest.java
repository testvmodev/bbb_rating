package com.bbb.core.model.request.partner;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PartnerInfoRequest {
    private String name;
    private String address;
    private String city;
    private String state;
    @JsonProperty(value = "zip_code")
    private String zipCode;
    private String lat;
    private String lng;
    private String phone;
    @JsonProperty(value = "smart_tailing")
    private String smartTailing;
    @JsonProperty(value = "mailing_address")
    private String mailingAddress;
    @JsonProperty(value = "referal_code")
    private String referalCode;
    private String website;
    @JsonProperty(value = "tell_about")
    private String tellAbout;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSmartTailing() {
        return smartTailing;
    }

    public void setSmartTailing(String smartTailing) {
        this.smartTailing = smartTailing;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTellAbout() {
        return tellAbout;
    }

    public void setTellAbout(String tellAbout) {
        this.tellAbout = tellAbout;
    }
}
