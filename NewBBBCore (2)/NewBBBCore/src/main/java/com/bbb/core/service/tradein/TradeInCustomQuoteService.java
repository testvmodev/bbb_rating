package com.bbb.core.service.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.tradein.customquote.CustomQuoteReviewRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteCreateRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteUpdateRequest;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface TradeInCustomQuoteService {
    Object getTradeInCustomQuotes(TradeInCustomQuoteRequest request) throws ExceptionResponse;

    Object getDetailCustomQuote(long customQuoteId) throws ExceptionResponse;

    Object createCustomQuote(TradeInCustomQuoteCreateRequest request) throws ExceptionResponse, MethodArgumentNotValidException;

    Object postTradeInCustomQuote(long tradeInIdCustomQuote, List<MultipartFile> images, boolean isAddImage, boolean isSave) throws ExceptionResponse;

    Object deleteImageTradeInCustomQuote(long tradeInCustomQuoteId, long imageImageTypeId) throws ExceptionResponse;

    Object updateTradeInCustomQuoteReviewed(CustomQuoteReviewRequest request) throws ExceptionResponse;


    Object updateTradeInCustomQuote(long customQuoteId, TradeInCustomQuoteUpdateRequest request) throws ExceptionResponse;

    Object getCustomQuoteCommon(long tradeInCustomQuoteId) throws ExceptionResponse;


}
