package com.bbb.core.manager.market;

import com.bbb.core.common.*;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.*;
import com.bbb.core.common.utils.s3.*;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bicycle.BrandManager;
import com.bbb.core.manager.bicycle.ModelManager;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.inventory.InventoryManager;
import com.bbb.core.manager.inventory.async.InventoryManagerAsync;
import com.bbb.core.manager.market.async.MyListingManagerAsync;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.notification.SubscriptionManager;
import com.bbb.core.manager.shipping.ShipmentManager;
import com.bbb.core.manager.tradein.TradeInCustomQuoteManager;
import com.bbb.core.model.InitialInventory;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.otherservice.response.common.GoogleServiceResponse;
import com.bbb.core.model.otherservice.response.common.google.AddressComponent;
import com.bbb.core.model.otherservice.response.common.google.GeometryLocation;
import com.bbb.core.model.otherservice.response.order.OrderDetailResponse;
import com.bbb.core.model.otherservice.response.order.OrderLineItem;
import com.bbb.core.model.otherservice.response.order.OrderShippingAddress;
import com.bbb.core.model.otherservice.response.shipping.ShipmentResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.inventory.create.InventoryCompDetailCreateRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingShipmentCreateRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingUpdateRequest;
import com.bbb.core.model.request.market.personallisting.update.PersonalListingUpdateBicycle;
import com.bbb.core.model.request.market.personallisting.update.PersonalListingUpdateShipping;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import com.bbb.core.model.response.market.mylisting.MyListingDetailResponse;
import com.bbb.core.model.response.market.mylisting.MyListingDraftDetailResponse;
import com.bbb.core.model.response.market.mylisting.MyListingResponse;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.BicycleYearRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.inventory.*;
import com.bbb.core.repository.inventory.out.InitialInventoryRepository;
import com.bbb.core.repository.market.*;
import com.bbb.core.repository.market.out.mylisting.MyListingDetailResponseRepository;
import com.bbb.core.repository.market.out.mylisting.MyListingDraftDetailResponseRepository;
import com.bbb.core.repository.market.out.mylisting.MyListingResponseRepository;
import com.bbb.core.repository.reout.BicycleSizeRepository;
import com.bbb.core.repository.reout.BrandModelYearRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MyListingManager implements MessageResponses {
    //region <Beans>
    @Autowired
    private MyListingDraftRepository myListingDraftRepository;
    @Autowired
    private MarketListingManager marketListingManager;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private S3Manager s3Manager;
    @Autowired
    private BicycleSizeRepository bicycleSizeRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private TradeInCustomQuoteManager tradeInCustomQuoteManager;
    @Autowired
    private MyListingImageDraftRepository myListingImageDraftRepository;
    @Autowired
    private BrandModelYearRepository brandModelYearRepository;
    @Autowired
    private BrandManager brandManager;
    @Autowired
    private ModelManager modelManager;
    @Autowired
    private BicycleYearRepository bicycleYearRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;
    @Autowired
    private InventoryManager inventoryManager;
    @Autowired
    private InventoryManagerAsync inventoryManagerAsync;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private MyListingManagerAsync myListingManagerAsync;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InventoryCompDetailRepository inventoryCompDetailRepository;
    @Autowired
    private InitialInventoryRepository initialInventoryRepository;
    @Autowired
    private MyListingResponseRepository myListingResponseRepository;
    @Autowired
    private MyListingDetailResponseRepository myListingDetailResponseRepository;
    @Autowired
    private MyListingDraftDetailResponseRepository myListingDraftDetailResponseRepository;
    @Autowired
    @Qualifier("mvcValidator")
    private Validator validator;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private MarketListingExpireRepository marketListingExpireRepository;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private SubscriptionManager subscriptionManager;
    @Autowired
    private ShipmentManager shipmentManager;
    @Autowired
    private InventorySaleRepository inventorySaleRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private InventoryBicycleRepository inventoryBicycleRepository;
    @Autowired
    private AwsResourceConfig awsResourceConfig;
    //endregion

    //region constants
    private List<StatusMarketListing> allowEditStatuses = Arrays.asList(
            StatusMarketListing.EXPIRED,
            StatusMarketListing.LISTED
    );
    private List<StatusMarketListing> allowRelistStatuses = Arrays.asList(
            StatusMarketListing.EXPIRED,
            StatusMarketListing.SOLD
    );
    //endregion

    //region API
    public Object getAllMyListings(
            MyListingSortField sortField,
            Sort sort,
            Pageable pageable
    ) throws ExceptionResponse {
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        ListObjResponse<MyListingResponse> response = new ListObjResponse<>(
                pageable.getPageNumber(), pageable.getPageSize());
        if (marketPlace == null) {
            return response;
        }
        response.setPageSize(pageable.getPageSize());
        response.setTotalItem(myListingResponseRepository.countAllByUser(
                CommonUtils.getUserLogin(),
                marketPlace.getId()
        ));
        if (response.getTotalItem() > 0) {
            response.setData(myListingResponseRepository.findByUser(
                    CommonUtils.getUserLogin(),
                    marketPlace.getId(),
                    sortField, sort,
                    pageable.getOffset(), pageable.getPageSize()
            ));
            if (response.getData().size() > 0) {
                List<Long> soldInventories = new ArrayList<>();
                for (MyListingResponse res : response.getData()) {
                    if (res.getStatusMarketListing() == StatusMarketListing.DRAFT) {
                        if (!StringUtils.isBlank(res.getDraft().getRawContent())) {
                            res.getDraft().setContent(convertRequest(res.getDraft().getRawContent()));
                        }
                    } else if (res.getStatusMarketListing() == StatusMarketListing.SOLD) {
                        soldInventories.add(res.getDone().getInventoryId());
                    }
                }
                if (soldInventories.size() > 0) {
                    List<InventorySale> sales = inventorySaleRepository.findAll(soldInventories);
                    if (sales.size() > 0) {
                        sales.forEach(sale -> {
                            MyListingResponse res = CommonUtils.findObject(response.getData(),
                                    r -> r.getDone() != null && r.getDone().getInventoryId() == sale.getInventoryId());
                            if (res != null) {
                                res.getDone().setSale(sale);
                            }
                        });
                    }
                }
            }
        }
        response.updateTotalPage();
        return response;
    }

    public Object getMyListingDetail(long marketListingId) throws ExceptionResponse {
        MyListingDetailResponse response = myListingDetailResponseRepository.findOne(marketListingId);
        if (response != null) {
            response.setInventoryImages(inventoryImageRepository.findAllByInventoryId(response.getInventoryId()));
            response.setInventoryComponents(inventoryCompDetailRepository.findByInventory(response.getInventoryId()));
            CartSingleStageResponse cart = billingManager.checkStatusCart(response.getInventoryId()); //checkStatusCart(response.getInventoryId(), CommonUtils.getUserLogin());
            if (cart != null) {
                response.setStageCart(cart.getStage());
            }
        } else {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.MARKET_LISTING_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return response;
    }

//    public Object createMyListingPTP(
//            PersonalListingRequest request
//    ) throws ExceptionResponse, MethodArgumentNotValidException {
//        return createMyListingPTP(request, false, null);
//    }

    public Object createMyListingPTP(
            @Valid PersonalListingRequest request,
            boolean fromDraft,
            List<MyListingImageDraft> imageDrafts,
            String userId
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        //manual trigger validating
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(request, "request");
        validator.validate(request, errors);
        Method currentMethod = new Object() {
        }.getClass().getEnclosingMethod();
        if (errors.hasErrors()) {
            throw new MethodArgumentNotValidException(
                    new MethodParameter(currentMethod, 0),
                    errors);
        }
        //end

        if (!CommonUtils.checkHaveAnyRole(PermissionRoles.P2P_ROLES)) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        request.checkNullSomeEmpty();
        CommonUtils.checkYearValid(request.getYearName());
        //valid email
        CommonUtils.checkEmailFormatValid(request.getEmailPaypal());

        String errorMessage = null;
        int errorCode = ObjectError.ERROR_PARAM;

        if (request.getShippingType() != null &&
                request.getShippingType() == OutboundShippingType.FLAT_RATE_TYPE
        ) {
            if (request.getFlatRate() == null || request.getFlatRate() < 0) {
                errorMessage = FLAT_RATE_MUST_POSITIVE;
            }
        }
        //check auto price
        if (request.getSalePrice() <= 0) {
            errorMessage = SALE_PRICE_MUST_POSITIVE;
        } else if (request.getIsBestOffer()) {
//            if (request.getBestOfferAutoAcceptPrice() == null &&
//                    request.getMinimumOfferAutoAcceptPrice() == null) {
//                request.setMinimumOfferAutoAcceptPrice(request.getSalePrice() * 0.6f);
//                request.setBestOfferAutoAcceptPrice(request.getSalePrice() * 0.95f);
//            } else {
            validatePrices(
                    request.getSalePrice(),
                    request.getIsBestOffer(),
                    request.getBestOfferAutoAcceptPrice(),
                    request.getMinimumOfferAutoAcceptPrice()
            );
        }
        if (errorMessage != null) {
            throw new ExceptionResponse(
                    errorCode, errorMessage, HttpStatus.BAD_REQUEST
            );
        }

        //valid type
        BicycleType bicycleType = bicycleTypeRepository.findOne(request.getBicycleTypeId());
        if (bicycleType == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                    TYPE_ID_NOT_EXIST),
                    HttpStatus.BAD_REQUEST);
        }
        //check size
        BicycleSize bicycleSize = verifyCompMyListingReturnSizeId(request.getCompRequests());
        if (bicycleSize == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BICYCLE_SIZE_NOT_EXIST),
                    HttpStatus.BAD_REQUEST);
        }

        //valid uploaded image if draft
        if (fromDraft) {
            if (imageDrafts == null || imageDrafts.size() < 1) {
                errorMessage = IMAGE_MUST_NOT_EMPTY;
                errorCode = ObjectError.INVALID_ACTION;
            }
            //now require just one
//            else if (imageDrafts.size() < 4) {
//                errorMessage = MUST_ENOUGH_IMAGE_TYPE_REQUIRE;
//                errorCode = ObjectError.INVALID_ACTION;
//            }
            checkError(errorMessage, errorCode);
        }

        if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
            validateTypeEbike(request.geteBikeMileage(), request.geteBikeHours());
        }

        //valid upgrade component component
        boolean validateSuspension = false;
        if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_HYBRID)
                || bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_MOUNTAIN)
        ) {
            validateSuspension = true;
        }
        tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests(), validateSuspension, null);

        //valid location
        GoogleServiceResponse googleResponse = validateShippingLocation(
                request.getAddressLine(),
                request.getCityName(), request.getState(),
                request.getZipCode(), request.getCountry()
        );
        AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
        AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
        AddressComponent city = GoogleServiceUtils.getCity(googleResponse);
        AddressComponent county = GoogleServiceUtils.getAddressLevel2(googleResponse);

        request.setCountryCode(country.getShortName());
        request.setStateCode(state.getShortName());

        Long brandId;
        Long modelId;
        Long yearId;
        long sizeId = bicycleSize.getId();
        long typeId = request.getBicycleTypeId();
        boolean bicycleMightExist = true;
        BrandModelYear brandModelYear =
                brandModelYearRepository.findOne(
                        request.getBrandName(),
                        request.getModelName(),
                        request.getYearName(),
                        System.currentTimeMillis());
        if (brandModelYear == null || brandModelYear.getBrandId() == null) {
            BrandCreateRequest brandRequest = new BrandCreateRequest();
            brandRequest.setName(request.getBrandName());
            brandRequest.setValueModifier(0.0f);
            BicycleBrand bicycleBrand = brandManager.createBrandIgnoreRole(brandRequest);
            brandId = bicycleBrand.getId();
            bicycleMightExist = false;
        } else {
            brandId = brandModelYear.getBrandId();
        }
        if (brandModelYear == null || brandModelYear.getModelId() == null) {
            ModelCreateRequest modelRequest = new ModelCreateRequest();
            modelRequest.setBrandId(brandId);
            modelRequest.setName(request.getModelName());
            BicycleModel bicycleModel = modelManager.createModelIgnoreRole(modelRequest);
            modelId = bicycleModel.getId();
            bicycleMightExist = false;
        } else {
            modelId = brandModelYear.getModelId();
        }

        if (brandModelYear == null || brandModelYear.getYearId() == null) {
            BicycleYear bicycleYear = new BicycleYear();
            bicycleYear.setName(request.getYearName());
            bicycleYear.setId(Long.valueOf(request.getYearName()));
            bicycleYear.setApproved(false);
            bicycleYear.setDelete(false);
            bicycleYear = bicycleYearRepository.save(bicycleYear);
            yearId = bicycleYear.getId();
            bicycleMightExist = false;
        } else {
            yearId = brandModelYear.getYearId();
        }

        Bicycle bicycle = null;
        if (bicycleMightExist) {
            bicycle = bicycleRepository.findOneByBrandModelYear(brandId, modelId, yearId, null);
        }
        if (bicycle == null) {
            bicycle = new Bicycle();
            bicycle.setBrandId(brandId);
            bicycle.setModelId(modelId);
            bicycle.setTypeId(typeId);
            bicycle.setYearId(yearId);
            bicycle.setSizeId(sizeId);
            bicycle.setName(request.getYearName() + " " + request.getBrandName() + " " + request.getModelName());
            if (fromDraft) {
                bicycle.setImageDefault(imageDrafts.get(0).getImage());
            }
            bicycle = bicycleRepository.save(bicycle);
        }

//        UserDetailFullResponse userDetail = authManager.getUserFullInfo(userId);

        Inventory inventory = new Inventory();
        inventory.setBicycleId(bicycle.getId());
        if (!fromDraft) {
            inventory.setStatus(StatusInventory.PENDING);
            inventory.setStage(StageInventory.INCOMPLETE_UPDATE_IMAGES);
        } else {
            inventory.setStatus(StatusInventory.ACTIVE);
            inventory.setStage(StageInventory.LISTED);
        }
        inventory.setCondition(request.getCondition());
        //price
        inventory.setCurrentListedPrice(request.getSalePrice());
        inventory.setBbbValue(request.getSalePrice());
        inventory.setDiscountedPrice(request.getSalePrice());
        inventory.setInitialListPrice(request.getSalePrice());
        inventory.setCogsPrice(request.getSalePrice());
        inventory.setMsrpPrice(bicycle.getRetailPrice());
//        List<Long> compIds = request.getCompRequests().stream().map(CompRequest::getCompId).collect(Collectors.toList());
//        inventoryManager.updatePriceInventory(inventory, compIds, request.getSalePrice(), false);

        //end
        inventory.setRecordType(RecordTypeInventory.BIKE);
        inventory.setSellerId(userId);
        inventory.setDescription(request.getDescription());
        inventory.setBicycleBrandName(request.getBrandName());
        inventory.setBicycleModelName(request.getModelName());
        inventory.setBicycleYearName(request.getYearName());
        inventory.setTitle(inventory.getBicycleYearName() + " " + inventory.getBicycleBrandName() + " " + inventory.getBicycleModelName());
        inventory.setBicycleSizeName(bicycleSize.getName());
        inventory.setBicycleTypeName(bicycleType.getName());
//        if (userDetail.getOnlineStore() != null) {
//            inventory.setOnlineStoreId(userDetail.getOnlineStore().getId());
//        }
        String storefront = CommonUtils.getStorefrontId();
        if (!StringUtils.isBlank(storefront)) {
            inventory.setStorefrontId(storefront);
        }


        InventoryType inventoryType = inventoryTypeRepository.findOneByName(InventoryTypeValue.PTP);
        if (inventoryType != null) {
            inventory.setTypeId(inventoryType.getId());
        }

        if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
            inventory.seteBikeHours(request.geteBikeHours());
            inventory.seteBikeMileage(request.geteBikeMileage());
        }

//        if (request.getCompRequests().size() > 0) {
//            inventory.setCustomQuote(true);
//        }

        //will generate it later
        inventory.setSearchGuideRecommendation("");

        inventoryManager.setValueGuidePrice(inventory, bicycle);

        inventory = inventoryRepository.save(inventory);
        Inventory invFinal = inventory;

        inventoryManagerAsync.createCompsAsync(
                inventory.getId(),
                request.getCompRequests().stream().map(req -> {
                    InventoryCompDetailCreateRequest compReq = new InventoryCompDetailCreateRequest();
                    compReq.setInventoryCompTypeId(req.getCompId());
                    compReq.setValue(req.getValue());
                    return compReq;
                }).collect(Collectors.toList()),
                invId->{
                    invFinal.setSearchGuideRecommendation(inventoryManager.createAndUpdateSearchRecommendation(invId));
                });

        Long marketPlaceId = marketPlaceRepository.getMarketPlaceId(MarketType.BBB.getValue());
        //TODO market place config for production
        Long marketPlaceConfigId = marketPlaceConfigRepository.findOneByMarketPlaceId(marketPlaceId, true).getId();
        MarketListing marketListing = new MarketListing();
        marketListing.setMarketPlaceId(marketPlaceId);
        marketListing.setMarketPlaceConfigId(marketPlaceConfigId);
        marketListing.setInventoryId(invFinal.getId());
        marketListing.setPaypalEmailSeller(request.getEmailPaypal());
        marketListing.setBestOffer(request.getIsBestOffer());
        marketListing.setBestOfferAutoAcceptPrice(request.getBestOfferAutoAcceptPrice());
        marketListing.setMinimumOfferAutoAcceptPrice(request.getMinimumOfferAutoAcceptPrice());
        marketListing.setTimeListed(LocalDateTime.now());
        marketListing.setExpirable(true);
        marketListing.setOwnerId(CommonUtils.getUserLogin());
        if (!fromDraft) {
            marketListing.setStatus(StatusMarketListing.PENDING);
        } else {
            marketListing.setStatus(StatusMarketListing.LISTED);
        }

        marketListing = marketListingRepository.save(marketListing);


        //take images from draft
        if (fromDraft) {
            List<InventoryImage> inventoryImages = new ArrayList();
//            List<String> oldImageKeys = new ArrayList(), newImageKeys = new ArrayList();
            List<Pair<String, String>> renameKeys = new ArrayList();

            for (MyListingImageDraft imageDraft : imageDrafts) {
                Pair<String, String> pair = new Pair();
                String url = imageDraft.getImage();
                String oldKey = url.substring(url.lastIndexOf("/") + 1);
                pair.setFirst(oldKey);
                pair.setSecond(Constants.MARKET_LISTING_PTP_FOLDER + "_" +
                        marketListing.getId() +
                        oldKey.substring(oldKey.lastIndexOf("_"))
                );
                renameKeys.add(pair);


                InventoryImage image = new InventoryImage();
                image.setImage(imageDraft.getImage().replace(pair.getFirst(), pair.getSecond()));
                image.setInventoryId(inventory.getId());
                inventoryImages.add(image);
            }

            s3Manager.renameObjectsAsync(S3Value.FOLDER_MARKET_LISTING_PTP_ORIGINAL, renameKeys);
            s3Manager.renameObjectsAsync(S3Value.FOLDER_MARKET_LISTING_PTP_LARGE, renameKeys);
            s3Manager.renameObjectsAsync(S3Value.FOLDER_MARKET_LISTING_PTP_SMALL, renameKeys);

            inventoryImages = inventoryImageRepository.saveAll(inventoryImages);
            myListingManagerAsync.updateInventoryImage(inventory, inventoryImages.get(0));
//            myListingImageDraftRepository.deleteAll(imageDrafts);
        }


        InventoryBicycle inventoryBicycle = new InventoryBicycle();
        inventoryBicycle.setBrandId(brandId);
        inventoryBicycle.setInventoryId(inventory.getId());
        inventoryBicycle.setModelId(modelId);
        inventoryBicycle.setYearId(yearId);
        inventoryBicycle.setTypeId(typeId);
        inventoryBicycle.setSizeId(sizeId);
        inventoryBicycleRepository.save(inventoryBicycle);

        InventoryLocationShipping inventoryLocationShipping = new InventoryLocationShipping();
        inventoryLocationShipping.setInventoryId(marketListing.getInventoryId());
        inventoryLocationShipping.setInsurance(request.getRequireInsuranceShipping());
        inventoryLocationShipping.setZipCode(request.getZipCode());
        inventoryLocationShipping.setFlatRate(request.getFlatRate());
        inventoryLocationShipping.setAllowLocalPickup(request.getLocalPickupShipping());
        inventoryLocationShipping.setShippingType(request.getShippingType());
        inventoryLocationShipping.setAddressLine(request.getAddressLine());
        inventoryLocationShipping.setCountryName(country.getLongName());
        inventoryLocationShipping.setCountryCode(country.getShortName());
        inventoryLocationShipping.setStateName(state.getLongName());
        inventoryLocationShipping.setStateCode(state.getShortName());
        inventoryLocationShipping.setCityName(city.getLongName());
        if (county != null) {
            inventoryLocationShipping.setCounty(county.getLongName());
        }
        try {
            if (googleResponse.getResults().size() > 0) {
                GeometryLocation geometryLocation = googleResponse.getResults().get(0).getGeometry().getLocation();
                inventoryLocationShipping.setLatitude(geometryLocation.getLat());
                inventoryLocationShipping.setLongitude(geometryLocation.getLng());
            }
        } catch (NullPointerException e) {
            //no geometry location from google map
        }

        inventoryLocationShippingRepository.save(inventoryLocationShipping);

//        MarketListingDetailResponse response = marketListingDetailRepository.findOne(marketListing.getId());

        subscriptionManager.triggerPublishAsync(marketListing);

        return marketListing;
    }

    private BicycleSize verifyCompMyListingReturnSizeId(List<CompRequest> requets) throws ExceptionResponse {
        List<Long> compIds = requets.stream().map(CompRequest::getCompId).collect(Collectors.toList());
        CommonUtils.stream(compIds);
        if (compIds.size() != requets.size()) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, COMPONENT_INVALIDATE), HttpStatus.BAD_REQUEST);
        }
        List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByIds(compIds);
        if (!verifyComp(inventoryCompTypes, ValueCommons.FRAME_MATERIAL_NAME_INV_COMP)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.FRAME_MATERIAL_NAME_INV_COMP +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }

        if (!verifyComp(inventoryCompTypes, ValueCommons.GENDER_NAME_INV_COMP)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.GENDER_NAME_INV_COMP +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if (!verifyComp(inventoryCompTypes, ValueCommons.BRAKE_TYPE_NAME_INV_COMP)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.BRAKE_TYPE_NAME_INV_COMP +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if (!verifyComp(inventoryCompTypes, ValueCommons.INVENTORY_FRAME_SIZE_NAME)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.INVENTORY_FRAME_SIZE_NAME +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if (!verifyComp(inventoryCompTypes, ValueCommons.INVENTORY_WHEEL_SIZE_NAME)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.INVENTORY_WHEEL_SIZE_NAME +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        String valueFrameSize = "";
        for (InventoryCompType inventoryCompType : inventoryCompTypes) {
            if(inventoryCompType.getName().equals(ValueCommons.INVENTORY_FRAME_SIZE_NAME)){
                valueFrameSize = CommonUtils.findObject(requets, q-> q.getCompId() == inventoryCompType.getId()).getValue();
                break;
            }
        }
        if (valueFrameSize.equals("")) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ValueCommons.INVENTORY_FRAME_SIZE_NAME +" " + MUST_NOT_BE_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }

        String sizeBicycle = UnitUtils.convertToSizeBicycle(valueFrameSize);
        return bicycleSizeRepository.findOneByName(sizeBicycle);
    }

    private boolean verifyComp(List<InventoryCompType> inventoryCompTypes, String name) {
        for (InventoryCompType inventoryCompType : inventoryCompTypes) {
            if (inventoryCompType.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public Object updateMyListing(
            long marketListingId,
            PersonalListingUpdateRequest request
    ) throws ExceptionResponse {
        MarketListing marketListing = findMarketListing(marketListingId);

        String errorMessage = null;
        int errorCode = ObjectError.ERROR_PARAM;

        if (!allowEditStatuses.contains(marketListing.getStatus())) {
            errorMessage = MessageResponses.STATUS_MARKET_LISTING_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);

        boolean updateListing = false;
        boolean isRenew = false;
        MarketListingExpire marketListingExpire = marketListingExpireRepository.findOne(marketListing.getId());
        boolean updateExpire = false;
        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());
        boolean updateInventory = false;
        boolean updatePrice = false;
//        List<InventoryCompDetail> updateCompDetails = null;
        boolean updateComps = false;
        List<InventoryCompDetail> removeCompDetails = null;
        boolean removeComps = false;
        Bicycle bicycle = null;
        boolean updateBicycle = false;
        InventoryLocationShipping inventoryLocationShipping = null;
        boolean updateShipping = false;
        InventoryBicycle inventoryBicycle = null;

        if (inventory == null) {
            errorMessage = MessageResponses.INVENTORY_ID_NOT_EXIST;
            errorCode = ObjectError.ERROR_NOT_FOUND;
        }
        checkError(errorMessage, errorCode);

        checkUserHasPermission(inventory);

        //market listing
        if (request.getEmailPaypal() != null) {
            CommonUtils.checkEmailFormatValid(request.getEmailPaypal());
            marketListing.setPaypalEmailSeller(request.getEmailPaypal());
            updateListing = true;
        }

        if (request.getStatusMarketListing() != null) {
            if (request.getStatusMarketListing() == StatusMarketListing.LISTED) {
                if (marketListing.getStatus() != StatusMarketListing.LISTED) {
                    isRenew = true;
                }
                if (marketListing.getStatus() == StatusMarketListing.SOLD) {
                    inventory.setStatus(StatusInventory.ACTIVE);
                    inventory.setStage(StageInventory.LISTED);
                    updateInventory = true;
                }
                if (marketListing.getStatus() != StatusMarketListing.LISTED) {
                    marketListing.setTimeListed(LocalDateTime.now());
                    marketListing.setRenew(true);
                    if (marketListingExpire != null) {
                        marketListingExpire.setLastMailExpired(null);
                        marketListingExpire.setLastMailExpireSoon(null);
                        marketListingExpire.setExpiredFailCount(0);
                        marketListingExpire.setExpireSoonFailCount(0);
                        updateExpire = true;
                    }
                }
                marketListing.setStatus(request.getStatusMarketListing());
                updateListing = true;
            } else {
                checkError(MessageResponses.NOT_ALLOW_CHANGE_LISTING_STATUS, ObjectError.INVALID_ACTION);
            }
        }

        //inventory
        if (request.getDescription() != null) {
            inventory.setDescription(request.getDescription());
            updateInventory = true;
        }

        if (request.getCondition() != null) {
            inventory.setCondition(request.getCondition());
            updateInventory = true;
        }

        if (request.getSalePrice() != null) {
            inventory.setCurrentListedPrice(request.getSalePrice());
            inventory.setBbbValue(request.getSalePrice());
            inventory.setInitialListPrice(request.getSalePrice());
            inventory.setDiscountedPrice(request.getSalePrice());
            updateInventory = true;
            updatePrice = true;
        } else {
            request.setSalePrice(inventory.getBbbValue());
        }

        if (request.getIsBestOffer() != null) {
            marketListing.setBestOffer(request.getIsBestOffer());
            updateListing = true;
        }

        if (request.getBestOfferAutoAcceptPrice() != null) {
            marketListing.setBestOfferAutoAcceptPrice(request.getBestOfferAutoAcceptPrice());
            updateListing = true;
            updatePrice = true;
        } else {
            request.setBestOfferAutoAcceptPrice(marketListing.getBestOfferAutoAcceptPrice());
        }

        if (request.getMinimumOfferAutoAcceptPrice() != null) {
            marketListing.setMinimumOfferAutoAcceptPrice(request.getMinimumOfferAutoAcceptPrice());
            updateListing = true;
            updatePrice = true;
        } else {
            request.setMinimumOfferAutoAcceptPrice(marketListing.getMinimumOfferAutoAcceptPrice());
        }

        if (updatePrice) {
            validatePrices(
                    request.getSalePrice(),
                    request.getIsBestOffer(),
                    request.getBestOfferAutoAcceptPrice(),
                    request.getMinimumOfferAutoAcceptPrice()
            );
        }

        //shipping
        if (request.getShipping() != null) {
            boolean changedAddress = false;
            PersonalListingUpdateShipping shippingRequest = request.getShipping();
            inventoryLocationShipping = inventoryLocationShippingRepository.findOne(marketListing.getInventoryId());
            if (inventoryLocationShipping == null) {
                checkError(INVENTORY_LOCATION_SHIPPING_NOT_FOUND, ObjectError.ERROR_NOT_FOUND);
            }
            updateShipping = false;

            //some field must update even if request field is null
            if (shippingRequest.getShippingType() != inventoryLocationShipping.getShippingType()) {
                updateShipping = true;
            }
            if (shippingRequest.getLocalPickupShipping() != inventoryLocationShipping.isAllowLocalPickup()) {
                inventoryLocationShipping.setAllowLocalPickup(shippingRequest.getLocalPickupShipping());
                updateShipping = true;
            }
            if (shippingRequest.getRequireInsuranceShipping() != inventoryLocationShipping.isInsurance()
            ) {
                inventoryLocationShipping.setInsurance(shippingRequest.getRequireInsuranceShipping());
                updateShipping = true;
            }
            if (shippingRequest.getFlatRate() != inventoryLocationShipping.getFlatRate()) {
                inventoryLocationShipping.setFlatRate(shippingRequest.getFlatRate());
                updateShipping = true;
            }
            if (inventoryLocationShipping.getShippingType() != shippingRequest.getShippingType()) {
                inventoryLocationShipping.setShippingType(shippingRequest.getShippingType());
                updateShipping = true;
                if (inventoryLocationShipping.getShippingType() == OutboundShippingType.FLAT_RATE_TYPE) {
                    if (inventoryLocationShipping.getFlatRate() == null ||
                            inventoryLocationShipping.getFlatRate() < 0
                    ) {
                        checkError(FLAT_RATE_MUST_POSITIVE, ObjectError.ERROR_PARAM);
                    }
                }
            }

            if (shippingRequest.getAddressLine() != null) {
                changedAddress = true;
            } else {
                shippingRequest.setAddressLine(inventoryLocationShipping.getAddressLine());
            }
            if (shippingRequest.getCityName() != null
                    && !shippingRequest.getCityName().equals(inventoryLocationShipping.getCityName())
            ) {
                inventoryLocationShipping.setCityName(shippingRequest.getCityName());
                changedAddress = true;
            }
            if (shippingRequest.getState() != null
                    && (!shippingRequest.getState().equals(inventoryLocationShipping.getStateName())
                    && !shippingRequest.getState().equals(inventoryLocationShipping.getStateCode()))
            ) {
//                inventoryLocationShipping.setStateName(shippingRequest.getSateName());
                changedAddress = true;
            }else {
                shippingRequest.setState(inventoryLocationShipping.getStateName());
            }
            if (shippingRequest.getZipCode() != null
                    && !shippingRequest.getZipCode().equals(inventoryLocationShipping.getZipCode())
            ) {
                inventoryLocationShipping.setZipCode(shippingRequest.getZipCode());
                changedAddress = true;
            }
            if (shippingRequest.getCountry() != null
                    && (!shippingRequest.getCountry().equals(inventoryLocationShipping.getCountryName())
                    && !shippingRequest.getCountry().equals(inventoryLocationShipping.getCountryCode())
            )) {
//                inventoryLocationShipping.setCountryName(shippingRequest.getCountryName());
                changedAddress = true;
            } else {
                shippingRequest.setCountry(inventoryLocationShipping.getCountryName());
            }
            if (shippingRequest.getAddressLine() != null
                    && !shippingRequest.getAddressLine().equals(inventoryLocationShipping.getAddressLine())
            ) {
                changedAddress = true;
            } else {
                shippingRequest.setAddressLine(inventoryLocationShipping.getAddressLine());
            }

            if (changedAddress) {
                GoogleServiceResponse googleResponse = validateShippingLocation(
                        shippingRequest.getAddressLine(),
                        inventoryLocationShipping.getCityName(),
                        shippingRequest.getState(),
                        inventoryLocationShipping.getZipCode(),
                        shippingRequest.getCountry()
                );
                AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
                AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
                AddressComponent city = GoogleServiceUtils.getCity(googleResponse);
                AddressComponent county = GoogleServiceUtils.getAddressLevel2(googleResponse);

                inventoryLocationShipping.setAddressLine(shippingRequest.getAddressLine());
                inventoryLocationShipping.setCountryCode(country.getShortName());
                inventoryLocationShipping.setCountryName(country.getLongName());
                inventoryLocationShipping.setStateCode(state.getShortName());
                inventoryLocationShipping.setStateName(state.getLongName());
                inventoryLocationShipping.setCityName(city.getLongName());
                inventoryLocationShipping.setAddressLine(shippingRequest.getAddressLine());
                if (county != null) {
                    inventoryLocationShipping.setCounty(county.getLongName());
                }
                try {
                    if (googleResponse.getResults().size() > 0) {
                        GeometryLocation geometryLocation = googleResponse.getResults().get(0).getGeometry().getLocation();
                        inventoryLocationShipping.setLatitude(geometryLocation.getLat());
                        inventoryLocationShipping.setLongitude(geometryLocation.getLng());
                    }
                } catch (NullPointerException e) {
                    //no geometry location from google map
                }

                updateShipping = true;
            }
        }

        //components
//        if (request.getRemoveCompTypeIds() != null && request.getRemoveCompTypeIds().size() > 0) {
//            removeCompDetails = inventoryCompDetailRepository.findByInventoryAndTypes(
//                    inventory.getId(), request.getRemoveCompTypeIds()
//            );
//            if (removeCompDetails.size() < request.getRemoveCompTypeIds().size()) {
//                List<Long> notFound = new ArrayList(request.getRemoveCompTypeIds());
//                removeCompDetails.stream().forEach(
//                        exists -> notFound.remove(exists.getId().getInventoryCompTypeId())
//                );
//                errorMessage = MessageResponses.INVENTORY_COMPONENT_NOT_FOUND + " [" +
//                        String.join(",", Lists.transform(notFound, id -> String.valueOf(id))) +
//                        "]";
//            } else {
//                removeComps = true;
//            }
//        }

        checkError(errorMessage, errorCode);


        //bicycle
        if (request.getBicycleRequest() != null) {
            Bicycle currentBicycle = bicycleRepository.findOne(inventory.getBicycleId());
            InitialInventory initialInventory = initialInventoryRepository.initInventory(
                    currentBicycle.getModelId(),
                    currentBicycle.getBrandId(),
                    currentBicycle.getYearId(),
                    currentBicycle.getTypeId(),
                    currentBicycle.getSizeId()
            );
            PersonalListingUpdateBicycle bicycleRequest = request.getBicycleRequest();
            if (bicycleRequest.getBrandName() != null &&
                    !bicycleRequest.getBrandName().equals(initialInventory.getBicycleBrandName())
            ) {
                if (StringUtils.isBlank(bicycleRequest.getBrandName())) {
                    errorMessage = MessageResponses.BRAND_NAME_NOT_EMPTY;
                } else {
                    inventory.setBicycleBrandName(bicycleRequest.getBrandName());
                    updateBicycle = true;
                }
            } else {
                bicycleRequest.setBrandName(initialInventory.getBicycleBrandName());
            }
            if (bicycleRequest.getModelName() != null &&
                    !bicycleRequest.getModelName().equals(initialInventory.getBicycleModelName())
            ) {
                if (StringUtils.isBlank(bicycleRequest.getBrandName())) {
                    errorMessage = MessageResponses.MODEL_NAME_NOT_EMPTY;
                } else {
                    inventory.setBicycleModelName(bicycleRequest.getModelName());
                    updateBicycle = true;
                }
            } else {
                bicycleRequest.setModelName(initialInventory.getBicycleModelName());
            }
            if (bicycleRequest.getYearName() != null &&
                    !bicycleRequest.getYearName().equals(initialInventory.getBicycleYearName())
            ) {
                if (StringUtils.isBlank(bicycleRequest.getYearName())) {
                    errorMessage = YEAR_NAME_NOT_EMPTY;
                } else {
                    CommonUtils.checkYearValid(bicycleRequest.getYearName());
                    inventory.setBicycleYearName(bicycleRequest.getYearName());
                    updateBicycle = true;
                }
            } else {
                bicycleRequest.setYearName(initialInventory.getBicycleYearName());
            }

//            if (bicycleRequest.getBicycleSizeId() != null) {
//                BicycleSize bicycleSize = bicycleSizeRepository.findOne(bicycleRequest.getBicycleSizeId());
//                if (bicycleSize == null) {
//                    errorMessage = BICYCLE_SIZE_NOT_EXIST;
//                } else {
//                    inventory.setBicycleSizeName(bicycleSize.getName());
//                    updateInventory = true;
//                }
//            }

            InventoryCompType inventoryCompType;
            CompRequest frameSizeRequest = null;
            Long sizeId = null;
            boolean validateSuspension = false;

            if (bicycleRequest.getBicycleTypeId() != null) {
                BicycleType bicycleType = bicycleTypeRepository.findOne(bicycleRequest.getBicycleTypeId());
                if (bicycleType == null) {
                    errorMessage = TYPE_ID_NOT_EXIST;
                } else {
                    if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
                        validateTypeEbike(bicycleRequest.geteBikeMileage(), bicycleRequest.geteBikeHours());
                        inventory.seteBikeHours(bicycleRequest.geteBikeHours());
                        inventory.seteBikeMileage(bicycleRequest.geteBikeMileage());
                    } else {
                        inventory.seteBikeHours(null);
                        inventory.seteBikeMileage(null);
                    }
                    inventory.setBicycleTypeName(bicycleType.getName());

                    if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_HYBRID)
                            || bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_MOUNTAIN)
                    ) {
                        validateSuspension = true;
                    }

                    updateInventory = true;

                    //make sure update type if only type change but brand/modeal/year doesnt
                    if (!updateBicycle) {
                        inventoryBicycle = inventoryBicycleRepository.findOne(inventory.getId());
                        inventoryBicycle.setTypeId(bicycleType.getId());
                    }
                }
            } else {
                BicycleType bicycleType = bicycleTypeRepository.findOne(currentBicycle.getTypeId());
                bicycleRequest.setBicycleTypeId(bicycleType.getId());
                if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
                    validateTypeEbike(bicycleRequest.geteBikeMileage(), bicycleRequest.geteBikeHours());
                    if (bicycleRequest.geteBikeHours() != null) {
                        inventory.seteBikeHours(bicycleRequest.geteBikeHours());
                        updateInventory = true;
                    }
                    if (bicycleRequest.geteBikeMileage() != null) {
                        inventory.seteBikeMileage(bicycleRequest.geteBikeMileage());
                        updateInventory = true;
                    }
                }
            }

            if (request.getUpdateComps() != null && request.getUpdateComps().size() > 0) {
                tradeInCustomQuoteManager.validCustomQuoteCompType(request.getUpdateComps(), validateSuspension, null);
                updateComps = true;
            }

            if (updateComps) {
                inventoryCompType = inventoryCompTypeRepository.findByName(ValueCommons.INVENTORY_FRAME_SIZE_NAME);
                for (CompRequest comp : request.getUpdateComps()) {
                    if (comp.getCompId() == inventoryCompType.getId()) {
                        frameSizeRequest = comp;
                        break;
                    }
                }
            }
            if (updateComps && frameSizeRequest != null) {
                String bicycleSizeName = UnitUtils.convertToSizeBicycle(frameSizeRequest.getValue());
                BicycleSize bicycleSize = bicycleSizeRepository.findOneByName(bicycleSizeName);
                sizeId = bicycleSize.getId();
                inventory.setBicycleSizeName(bicycleSize.getName());
                updateInventory = true;
            } else if (!StringUtils.isBlank(inventory.getBicycleSizeName())) {
                BicycleSize bicycleSize = bicycleSizeRepository.findOneByName(inventory.getBicycleSizeName());
                if (bicycleSize != null) {
                    sizeId = bicycleSize.getId();
                }
            }

            checkError(errorMessage, errorCode);

            if (updateBicycle) {
                Long brandId;
                Long modelId;
                Long yearId;
                long typeId = bicycleRequest.getBicycleTypeId();
                boolean bicycleMightExist = true;
                BrandModelYear brandModelYear =
                        brandModelYearRepository.findOne(
                                bicycleRequest.getBrandName(),
                                bicycleRequest.getModelName(),
                                bicycleRequest.getYearName(),
                                System.currentTimeMillis());
                if (brandModelYear == null || brandModelYear.getBrandId() == null) {
                    BrandCreateRequest brandRequest = new BrandCreateRequest();
                    brandRequest.setName(bicycleRequest.getBrandName());
                    brandRequest.setValueModifier(0.0f);
                    BicycleBrand bicycleBrand = brandManager.createBrandIgnoreRole(brandRequest);
                    brandId = bicycleBrand.getId();
                    bicycleMightExist = false;
                } else {
                    brandId = brandModelYear.getBrandId();
                }
                if (brandModelYear == null || brandModelYear.getModelId() == null) {
                    ModelCreateRequest modelRequest = new ModelCreateRequest();
                    modelRequest.setBrandId(brandId);
                    modelRequest.setName(bicycleRequest.getModelName());
                    BicycleModel bicycleModel = modelManager.createModelIgnoreRole(modelRequest);
                    modelId = bicycleModel.getId();
                    bicycleMightExist = false;
                } else {
                    modelId = brandModelYear.getModelId();
                }

                if (brandModelYear == null || brandModelYear.getYearId() == null) {
                    BicycleYear bicycleYear = new BicycleYear();
                    bicycleYear.setName(bicycleRequest.getYearName());
                    bicycleYear.setId(Long.valueOf(bicycleRequest.getYearName()));
                    bicycleYear.setApproved(true);
                    bicycleYear.setDelete(false);
                    bicycleYear = bicycleYearRepository.save(bicycleYear);
                    yearId = bicycleYear.getId();
                    bicycleMightExist = false;
                } else {
                    yearId = brandModelYear.getYearId();
                }

                if (bicycleMightExist) {
                    bicycle = bicycleRepository.findOneByBrandModelYearIgnoreActive(brandId, modelId, yearId);
                    if (bicycle != null && !bicycle.isActive() && bicycle.getId() != currentBicycle.getId()) {
                        bicycle = null;
                    }
                }
                if (bicycle == null) {
                    bicycle = new Bicycle();
                    bicycle.setBrandId(brandId);
                    bicycle.setModelId(modelId);
                    bicycle.setTypeId(typeId);
                    bicycle.setYearId(yearId);
                    bicycle.setSizeId(sizeId);
                    bicycle.setName(bicycleRequest.getYearName() + " " + bicycleRequest.getBrandName() + " " + bicycleRequest.getModelName());
                } else {
                    updateBicycle = false;
                    if (bicycle.getId() != inventory.getBicycleId()) {
                        inventory.setBicycleId(bicycle.getId());
                        updateInventory = true;
                    }
                }

                updateInventory = true;
                inventory.setBicycleBrandName(bicycleRequest.getBrandName());
                inventory.setBicycleModelName(bicycleRequest.getModelName());
                inventory.setBicycleYearName(bicycleRequest.getYearName());
                String title = inventory.getBicycleYearName() + " " +
                        inventory.getBicycleBrandName() + " " +
                        inventory.getBicycleModelName();
                inventory.setTitle(title);
                inventory.setMsrpPrice(bicycle.getRetailPrice());

                inventoryBicycle = new InventoryBicycle();
                inventoryBicycle.setInventoryId(inventory.getId());
                inventoryBicycle.setBrandId(brandId);
                inventoryBicycle.setModelId(modelId);
                inventoryBicycle.setYearId(yearId);
                inventoryBicycle.setTypeId(typeId);
                inventoryBicycle.setSizeId(sizeId);
            }
        } else {
            boolean validateSuspension = false;
            BicycleType bicycleType = bicycleTypeRepository.findOneByName(inventory.getBicycleTypeName());
            if (bicycleType != null) {
                if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_HYBRID)
                        || bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_MOUNTAIN)
                ) {
                    validateSuspension = true;
                }
            }
            if (request.getUpdateComps() != null && request.getUpdateComps().size() > 0) {
                tradeInCustomQuoteManager.validCustomQuoteCompType(request.getUpdateComps(), validateSuspension, null);
                updateComps = true;
            }
        }

        //final saving

        //check manual cart before saving
        CartSingleStageResponse cart = billingManager.checkStatusCart(inventory.getId());
        if (cart != null && cart.getStage() != null &&
                cart.getStage().equalsIgnoreCase(ValueCommons.PAY)
        ) {
            checkError(CANT_SAVE_LISTING_SALE_PENDING_OR_PAY_PENDING.replace(ACTION, "update"), ObjectError.INVALID_ACTION);
        }

        if (updateListing) {
            marketListingRepository.save(marketListing);
        }
        if (marketListingExpire != null && updateExpire) {
            marketListingExpireRepository.save(marketListingExpire);
        }
        if (inventoryLocationShipping != null && updateShipping) {
            inventoryLocationShippingRepository.save(inventoryLocationShipping);
        }
        if (bicycle != null && updateBicycle) {
            bicycle = bicycleRepository.save(bicycle);
            if (bicycle.getId() != inventory.getBicycleId()) {
                inventory.setBicycleId(bicycle.getId());
                updateInventory = true;
            }
        }
        if (removeComps) {
            inventoryCompDetailRepository.deleteAll(removeCompDetails);
        }
        if (updateComps) {
            inventoryCompDetailRepository.deleteAllByInventoryId(inventory.getId());
            inventoryManagerAsync.createCompsSync(
                    inventory.getId(),
                    request.getUpdateComps().stream().map(req -> {
                        InventoryCompDetailCreateRequest createRequest = new InventoryCompDetailCreateRequest();
                        createRequest.setInventoryCompTypeId(req.getCompId());
                        createRequest.setValue(req.getValue());
                        return createRequest;
                    }).collect(Collectors.toList())
            );
        }
//        if (updatePrice) {
//            inventoryManager.updatePriceInventory(
//                    inventory,
//                    inventoryCompDetailRepository.findByInventory(inventory.getId()).stream()
//                            .map(comp -> comp.getId().getInventoryCompTypeId())
//                            .collect(Collectors.toList()),
//                    inventory.getBbbValue(),
//                    false
//            );
//            updateInventory = true;
//        }
        if (updateBicycle || updateInventory) {
            if (bicycle != null) {
                inventoryManager.setValueGuidePrice(inventory, bicycle);
            } else {
                inventoryManager.setValueGuidePrice(inventory);
            }
            updateInventory = true;
        }
        if (inventory != null && updateInventory) {
            inventoryRepository.save(inventory);
        }

        if (inventoryBicycle != null) {
            inventoryBicycleRepository.save(inventoryBicycle);
        }

        billingManager.marketListingUpdateAsync(marketListing.getId());
        subscriptionManager.triggerUpdateAsync(marketListing);
        if (isRenew) {
            subscriptionManager.triggerRenewAsync(marketListing);
        }

        return marketListing;
    }

    public Object deleteMyListing(long marketListingId) throws ExceptionResponse {
        MarketListing marketListing = findMarketListing(marketListingId);

        if (marketListing.getStatus() == StatusMarketListing.SALE_PENDING) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    CANT_SAVE_LISTING_SALE_PENDING_OR_PAY_PENDING.replace(ACTION, "delete"),
                    HttpStatus.BAD_REQUEST
            );
        }

        Inventory inventory = inventoryRepository.findOneNotDelete(marketListing.getInventoryId());
        if (inventory == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    INVENTORY_ID_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }

        checkUserHasPermission(inventory);

        List<Offer> offers = offerRepository.findAllActive(marketListing.getId());

        //check manual cart before saving
        CartSingleStageResponse cart = billingManager.checkStatusCart(inventory.getId());
        if (cart != null && cart.getStage() != null &&
                cart.getStage().equalsIgnoreCase(ValueCommons.PAY)
        ) {
            checkError(CANT_SAVE_LISTING_SALE_PENDING_OR_PAY_PENDING.replace(ACTION, "delete"), ObjectError.INVALID_ACTION);
        }

        offerManager.rejectAll(offers);

        marketListing.setDelete(true);
//        if ( marketListing.getStatus() == StatusMarketListing.LISTED){
//            marketListing.setStatus(StatusMarketListing.DE_LISTED);
//        }

        inventory.setDelete(true);
        inventory.setStatus(StatusInventory.IN_ACTIVE);

        inventoryRepository.save(inventory);
        marketListingRepository.save(marketListing);

        billingManager.marketListingDeleteAsync(marketListing.getInventoryId());

        return new MessageResponse("Success");
    }

    public Object postImageMarketListingPTP(long marketListingId, List<MultipartFile> images) throws ExceptionResponse {
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_NOT_FOUND;
        MarketListing marketListing = marketListingRepository.findOne(marketListingId);
        if (marketListing == null || marketListing.isDelete()) {
            errorMessage = MARKET_LISTING_NOT_EXIST;
        } else if (marketListing.getStatus() != StatusMarketListing.PENDING) {
            errorMessage = STATUS_MARKET_LISTING_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);

        Inventory inventory = inventoryRepository.findOneNotDelete(marketListing.getInventoryId());
        if (inventory == null || inventory.isDelete()) {
            errorMessage = INVENTORY_ID_NOT_EXIST;
        } else if (inventory.getStatus() != StatusInventory.PENDING || inventory.getStage() != StageInventory.INCOMPLETE_UPDATE_IMAGES) {
            errorMessage = STAGE_AND_STATUS_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);
        if (inventory.getStorefrontId() != null) {
            ;
        } else if (!inventory.getSellerId().equals(CommonUtils.getUserLogin())) {
            checkError(MessageResponses.YOU_DONT_HAVE_PERMISSION, ObjectError.NO_PERMISSION, HttpStatus.FORBIDDEN);
        }

        addMyListingImages(marketListing, inventory, images);

        inventory.setStage(StageInventory.LISTED);
        inventory.setStatus(StatusInventory.ACTIVE);
        inventoryRepository.save(inventory);

        marketListing.setStatus(StatusMarketListing.LISTED);
        marketListing = marketListingRepository.save(marketListing);

//        return marketListingDetailRepository.findOne(marketListing.getId());
        return marketListing;
    }

    public Object addImageImageMarketListingPTP(
            long marketListingId, List<MultipartFile> images
    ) throws ExceptionResponse {
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_NOT_FOUND;
        MarketListing marketListing = findMarketListing(marketListingId);

        if (marketListing.getStatus() != StatusMarketListing.LISTED) {
            errorMessage = STATUS_MARKET_LISTING_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);

        Inventory inventory = inventoryRepository.findOneNotDelete(marketListing.getInventoryId());

        if (inventory == null || inventory.isDelete()) {
            errorMessage = INVENTORY_ID_NOT_EXIST;
        } else if (inventory.getStatus() != StatusInventory.ACTIVE || inventory.getStage() != StageInventory.LISTED) {
            errorMessage = STAGE_AND_STATUS_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);

        checkUserHasPermission(inventory);

        boolean saveInven = false;
        if (inventory.getImageDefaultId() == null || inventory.getImageDefault() == null) {
            saveInven = true;
        }
        List<InventoryImage> inventoryImages = addMyListingImages(marketListing, inventory, images);
        if (saveInven) {
            inventoryRepository.save(inventory);
        }

        return inventoryImages;
    }

    public Object deleteImageMarketListingPTP(
            long marketListingId, List<Long> inventoryImageIds
    ) throws ExceptionResponse {
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_NOT_FOUND;

        MarketListing marketListing = findMarketListing(marketListingId);

        if (marketListing.getStatus() != StatusMarketListing.LISTED) {
            errorMessage = STATUS_MARKET_LISTING_INVALID;
            errorCode = ObjectError.INVALID_ACTION;
        }
        checkError(errorMessage, errorCode);

        Inventory inventory = inventoryRepository.findOneNotDelete(marketListing.getInventoryId());
        if (inventory == null || inventory.isDelete()) {
            checkError(INVENTORY_ID_NOT_EXIST, ObjectError.ERROR_NOT_FOUND);
        }

        checkUserHasPermission(inventory);

        if (inventoryImageIds.size() > 0) {
            List<InventoryImage> deleteImages = inventoryImageRepository.findByImageIds(
                    inventory.getId(),
                    inventoryImageIds
            );

            if (deleteImages.size() < inventoryImageIds.size()) {
                checkError(IMAGE_NOT_FOUND, ObjectError.ERROR_NOT_FOUND);
            }

            List<InventoryImage> allImages = inventoryImageRepository.findAllByInventoryId(inventory.getId());
            if (allImages.size() <= deleteImages.size()) {
                checkError(NOT_ALLOW_DELETE_ALL_IMAGE, ObjectError.INVALID_ACTION);
            }

            inventoryImageRepository.deleteAll(deleteImages);

            deleteS3ImagesAsync(
                    deleteImages.stream()
                    .map(InventoryImage::getImage)
                    .collect(Collectors.toList())
            );

            InventoryImage image = inventoryImageRepository.findOneByInventory(inventory.getId());
            inventory.setImageDefaultId(image.getId());
            inventory.setImageDefault(image.getImage());
            inventoryRepository.save(inventory);
        }

        return new MessageResponse("Success");
    }

    public Object getDraftDetail(long myListingDraftId) throws ExceptionResponse {
        MyListingDraftDetailResponse response = myListingDraftDetailResponseRepository.findOne(myListingDraftId);
        if (response != null) {
            if (!response.getCreatedBy().equals(CommonUtils.getUserLogin())
                    && !CommonUtils.checkHaveRole(UserRoleType.ADMIN)
            ) {
                if (response.getStorefrontId() == null
                        || !response.getStorefrontId().equals(CommonUtils.getStorefrontId())) {
                    throw new ExceptionResponse(
                            ObjectError.NO_PERMISSION,
                            MessageResponses.YOU_DONT_HAVE_PERMISSION,
                            HttpStatus.FORBIDDEN
                    );
                }
            }
            response.setImageDrafts(myListingImageDraftRepository.findImages(myListingDraftId));
        } else {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.MY_LISTING_DRAFT_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return response;
    }

    public Object createDraftMyListing(
            PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        if (!CommonUtils.checkHaveAnyRole(PermissionRoles.P2P_ROLES)) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        boolean validate = (request.getNeedValidate() != null && request.getNeedValidate()) ? true : false;
        validateDraftRequest(request, validate);

        List<InventoryImage> images = null;
        if (request.getMyListingImageIds() != null && request.getMyListingImageIds().size() > 0) {
            images = inventoryImageRepository.findByImageIds(request.getMyListingImageIds());
            if (images.size() != request.getMyListingImageIds().size()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_NOT_FOUND,
                        IMAGE_NOT_FOUND,
                        HttpStatus.NOT_FOUND
                );
            }
            List<Long> inventoryIds = images.stream()
                    .map(i -> i.getInventoryId())
                    .distinct()
                    .collect(Collectors.toList());
            List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIds);
            for (Inventory inventory : inventories) {
                checkUserHasPermission(inventory);
            }

            for (InventoryImage image : images) {
                //validate url (but not validate connection)
                try {
                    URL url = new URL(image.getImage());
                } catch (MalformedURLException e) {
                    throw new ExceptionResponse(
                            ObjectError.UNKNOWN_ERROR,
                            MessageResponses.CANT_ACCESS_URL,
                            HttpStatus.INTERNAL_SERVER_ERROR
                    );
                }
            }
        }

        String year = request.getYearName();
        String brand = request.getBrandName();
        String model = request.getModelName();
        String title = (year != null ? (year + " ") : "") +
                (brand != null ? (brand + " ") : "") +
                (model != null ? model : "");

        String content = convertRequest(request);
        if (content.equals("{}")) {
            checkError(EMPTY_DRAFT, ObjectError.ERROR_PARAM);
        }
        request.setMyListingImageIds(null); //only use once while creating draft

        MyListingDraft myListingDraft = new MyListingDraft();
        myListingDraft.setContent(content);
        myListingDraft.setCurrentListedPrice(request.getSalePrice());
        myListingDraft.setTitle(title);
        myListingDraft.setBestOffer(request.getIsBestOffer());
        myListingDraft.setCreatedBy(CommonUtils.getUserLogin());

        String storefront = CommonUtils.getStorefrontId();
        if (!StringUtils.isBlank(storefront)) {
            myListingDraft.setStorefrontId(storefront);
        }

        myListingDraft = myListingDraftRepository.save(myListingDraft);

        //clone image from other listing
        if (images != null) {
            try {
                List<S3UploadFromUrl<Long>> uploadRequests = new ArrayList<>();
                for (InventoryImage image : images) {
                    //already validated url, assume there will be no exception
                    URL url = null;
                    try {
                        url = new URL(image.getImage());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    S3UploadFromUrl upload = new S3UploadFromUrl(
                            S3Value.PREFIX_DRAFT_LISTING_PTP + "_" + myListingDraft.getId(),
                            S3Value.FOLDER_MARKET_LISTING_PTP_ORIGINAL,
                            url, myListingDraft.getId()
                    );
                    uploadRequests.add(upload);
                }
                long myListingDraftId = myListingDraft.getId();
                List<S3UploadResponse<Long>> uploadResponses = S3UploadUtils.upload(uploadRequests, s3Manager);
                List<MyListingImageDraft> imageDrafts = uploadResponses.stream()
                        .map(upload -> {
                            MyListingImageDraft imageDraft = new MyListingImageDraft();
                            imageDraft.setListingDraftId(myListingDraftId);
                            imageDraft.setImage(S3UploadUtils.getFullLinkS3(awsResourceConfig, upload.getFullKey()));

                            return imageDraft;
                        }).collect(Collectors.toList());
                imageDrafts = myListingImageDraftRepository.saveAll(imageDrafts);
            } catch (ExceptionResponse e) {
                //fail to clone images, undo creating listing draft
                myListingDraftRepository.delete(myListingDraft);
                throw e;
            }
        }

        return myListingDraft;
    }

    public Object updateDraftMyListing(
            long draftId, PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        MyListingDraft myListingDraft = findDaftById(draftId);

        checkUserHasPermission(myListingDraft);

        boolean validate = (request.getNeedValidate() != null && request.getNeedValidate()) ? true : false;
        validateDraftRequest(request, validate);

        String content = convertRequest(request);
        if (content.equals("{}")) {
            checkError(EMPTY_DRAFT, ObjectError.ERROR_PARAM);
        }

        String year = request.getYearName();
        String brand = request.getBrandName();
        String model = request.getModelName();
        String title = (year != null ? (year + " ") : "") +
                (brand != null ? (brand + " ") : "") +
                (model != null ? model : "");

        myListingDraft.setContent(content);
        myListingDraft.setCurrentListedPrice(request.getSalePrice());
        myListingDraft.setTitle(title);
        myListingDraft.setBestOffer(request.getIsBestOffer());

        return myListingDraftRepository.save(myListingDraft);
    }

    public Object deleteDraftMyListing(long draftId) throws ExceptionResponse {
        MyListingDraft myListingDraft = findDaftById(draftId);

        checkUserHasPermission(myListingDraft);

        myListingDraft.setDelete(true);

        List<MyListingImageDraft> imageDrafts = myListingImageDraftRepository.findImages(myListingDraft.getId());

        myListingDraftRepository.save(myListingDraft);
        deleteImageDraft(imageDrafts);

        return new MessageResponse("Success");
    }

    public Object postImageDraftMyListing(long listingDraftId, List<MultipartFile> images) throws ExceptionResponse {
        MyListingDraft myListingDraft = findDaftById(listingDraftId);

        checkUserHasPermission(myListingDraft);

        if (images == null || images.size() < 1) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    IMAGE_MUST_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        List<S3UploadResponse<Long>> s3UploadResponses = uploadDraftImages(images, String.valueOf(myListingDraft.getId()));

        List<MyListingImageDraft> myListingImageDrafts = new ArrayList();
        for (S3UploadResponse<Long> s3UploadResponse : s3UploadResponses) {
            MyListingImageDraft imageDraft = new MyListingImageDraft();
            imageDraft.setListingDraftId(myListingDraft.getId());
            imageDraft.setImage(S3UploadUtils.getFullLinkS3(awsResourceConfig, s3UploadResponse.getFullKey()));
            myListingImageDrafts.add(imageDraft);
        }

        return myListingImageDraftRepository.saveAll(myListingImageDrafts);
    }

    public Object deleteImageDraftMyListing(List<Long> imageDraftIds) throws ExceptionResponse {
        List<MyListingImageDraft> myListingImageDrafts = myListingImageDraftRepository.findAll(imageDraftIds);
        if (myListingImageDrafts.size() < imageDraftIds.size()) {
            myListingImageDrafts.forEach(draft -> imageDraftIds.remove(draft.getId()));
            List<String> notFoundIds = imageDraftIds.stream().map(id -> id.toString()).collect(Collectors.toList());
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    DRAFT_IMAGE_NOT_FOUND + ": [" + String.join(",", notFoundIds) + "]",
                    HttpStatus.NOT_FOUND
            );
        }

        List<Long> draftIds = myListingImageDrafts.stream()
                .map(MyListingImageDraft::getListingDraftId)
                .distinct()
                .collect(Collectors.toList());
        List<MyListingDraft> myListingDrafts = myListingDraftRepository.findByIds(draftIds);

        for (MyListingDraft myListingDraft : myListingDrafts) {
            checkUserHasPermission(myListingDraft);
        }

        deleteImageDraft(myListingImageDrafts);

        return new MessageResponse("Success");
    }

    public Object createListingFromDraft(
            long draftId
    ) throws ExceptionResponse, MethodArgumentNotValidException, NoSuchMethodException {
        MyListingDraft myListingDraft = findDaftById(draftId);

        checkUserHasPermission(myListingDraft);

        PersonalListingRequest request = convertRequest(myListingDraft.getContent());

        List<MyListingImageDraft> myListingImageDrafts = myListingImageDraftRepository.findImages(myListingDraft.getId());

        Object response = createMyListingPTP(request, true, myListingImageDrafts, myListingDraft.getCreatedBy());
        myListingDraft.setDelete(true);
        myListingDraftRepository.save(myListingDraft);
        return response;
    }

    public Object relistMyListing(long marketListingId) throws ExceptionResponse {
        MarketListing marketListing = findMarketListing(marketListingId);

        if (!allowRelistStatuses.contains(marketListing.getStatus())) {
            checkError(MessageResponses.STATUS_MARKET_LISTING_INVALID, ObjectError.INVALID_ACTION);
        }

        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());
        if (inventory.getStatus() != StatusInventory.ACTIVE
                && inventory.getStatus() != StatusInventory.SOLD
        ) {
            checkError(MessageResponses.STATUS_INVENTORY_INVALID, ObjectError.INVALID_ACTION);
        }

        inventory.setStatus(StatusInventory.ACTIVE);
        inventory.setStage(StageInventory.LISTED);

        if (marketListing.getStatus() == StatusMarketListing.EXPIRED) {
            marketListing.setRenew(true);
            MarketListingExpire marketListingExpire = marketListingExpireRepository.findOne(marketListing.getId());
            if (marketListingExpire != null) {
                marketListingExpire.setLastMailExpired(null);
                marketListingExpire.setLastMailExpireSoon(null);
                marketListingExpire.setExpiredFailCount(0);
                marketListingExpire.setExpireSoonFailCount(0);
                marketListingExpireRepository.save(marketListingExpire);
            }
        } else if (marketListing.getStatus() == StatusMarketListing.SOLD) {
            marketListing.setRenew(false);
        }
        marketListing.setTimeListed(LocalDateTime.now());
        marketListing.setStatus(StatusMarketListing.LISTED);
        inventoryRepository.save(inventory);
        return marketListingRepository.save(marketListing);
    }

    public Object createShipment(
        PersonalListingShipmentCreateRequest request
    ) throws ExceptionResponse {
        MarketListing marketListing = findMarketListing(request.getMarketListingId());
        String userId = CommonUtils.getUserLogin();
        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());
        InventoryLocationShipping locationShipping;
        InventorySale sale;
        OrderShippingAddress buyerAddress;
        UserDetailFullResponse sellerInfo;

        checkUserHasPermission(inventory);

        if (marketListing.getStatus() != StatusMarketListing.SOLD) {
            checkError(MessageResponses.STATUS_MARKET_LISTING_INVALID, ObjectError.INVALID_ACTION, HttpStatus.BAD_REQUEST);
        }

        if (inventory.getStage() == StageInventory.SHIPPED_TO_CUSTOMER
                || inventory.getStage() == StageInventory.RECEIVED_BY_CUSTOMER
        ) {
            checkError(MessageResponses.P2P_ALREADY_SHIP, ObjectError.INVALID_ACTION, HttpStatus.BAD_REQUEST);
        }

        locationShipping = inventoryLocationShippingRepository.findOne(marketListing.getInventoryId());
        if (locationShipping == null) {
            checkError(MessageResponses.INVENTORY_LOCATION_SHIPPING_NOT_FOUND, ObjectError.ERROR_NOT_FOUND, HttpStatus.NOT_FOUND);
        }

        sale = inventorySaleRepository.findOne(marketListing.getInventoryId());
        if (sale == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.SALE_INFO_NOT_FOUND,
                    HttpStatus.NOT_FOUND
            );
        }

        OrderDetailResponse order = billingManager.getOrderDetail(sale.getOrderId());
        OrderLineItem orderItem = null;
        for (OrderLineItem i : order.getLineItems()) {
            if (i.getMarketListingId() == marketListing.getId()) {
                orderItem = i;
            }
        }
        if (orderItem == null) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.SALE_INFO_NOT_FOUND,
                    HttpStatus.NOT_FOUND
            );
        }

        if (locationShipping.isAllowLocalPickup()) {
            if (orderItem.getLocalPickup() != null && orderItem.getLocalPickup()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MessageResponses.SHIPPING_METHOD_LOCAL_PICKUP,
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        buyerAddress = order.getShippingAddress();

        sellerInfo = authManager.getUserFullInfo(inventory.getSellerId());
        String sellerPersonal = null;
        String sellerCompany = null;
        String phone = null;
        if (inventory.getStorefrontId() != null) {
            sellerCompany = sellerInfo.getStorefront().getName();
            phone = sellerInfo.getStorefront().getPhone();
        } else {
            sellerPersonal = sellerInfo.getDisplayName();
            phone = sellerInfo.getNormal().getPhoneNumber();
        }

        ShipmentResponse shipmentResponse = shipmentManager.createOutboundShipment(
                request,
                marketListing.getInventoryId(),
                sellerPersonal, sellerCompany, phone,
                locationShipping, buyerAddress
        );

        inventory.setStage(StageInventory.SHIPPED_TO_CUSTOMER);
        inventoryRepository.save(inventory);

        notificationManager.sendMailListingShippedAsync(
                sale.getBuyerId(), marketListing, inventory, shipmentResponse);

        return shipmentResponse;
    }
    //endregion

    //region Shared
    private void checkError(String message, int code) throws ExceptionResponse {
        checkError(message, code, HttpStatus.BAD_REQUEST);
    }

    private void checkError(String message, int code, HttpStatus status) throws ExceptionResponse {
        if (message != null) {
            throw new ExceptionResponse(
                    code, message, status
            );
        }
    }

    private MarketListing findMarketListing(long marketListingId) throws ExceptionResponse {
        MarketListing marketListing = marketListingRepository.findOne(marketListingId);
        if (marketListing == null || marketListing.isDelete()) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MARKET_LISTING_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return marketListing;
    }

    private List<InventoryImage> addMyListingImages(
            MarketListing marketListing, Inventory inventory, List<MultipartFile> images
    ) throws ExceptionResponse {
        if (images == null || images.size() < 0) {
            checkError(IMAGE_MUST_NOT_EMPTY, ObjectError.ERROR_NOT_FOUND);
        }

        List<S3UploadResponse<Long>> s3UploadResponses = uploadMyListingImages(images, String.valueOf(marketListing.getId()));

        List<InventoryImage> newInventoryImages = new ArrayList<>();
        for (S3UploadResponse<Long> s3UploadResponse : s3UploadResponses) {
            InventoryImage inventoryImage = new InventoryImage();
            inventoryImage.setInventoryId(inventory.getId());
            inventoryImage.setImage(S3UploadUtils.getFullLinkS3(awsResourceConfig, s3UploadResponse.getFullKey()));
            newInventoryImages.add(inventoryImage);
        }

        List<InventoryImage> inventoryImages = inventoryImageRepository.saveAll(newInventoryImages);

        if (inventory.getImageDefault() == null || inventory.getImageDefaultId() == null) {
            inventory.setImageDefaultId(inventoryImages.get(0).getId());
            inventory.setImageDefault(inventoryImages.get(0).getImage());
        }

        return inventoryImages;
    }

    private void deleteImageDraft(List<MyListingImageDraft> myListingImageDrafts) {
        if (myListingImageDrafts != null && myListingImageDrafts.size() > 0) {
            deleteS3ImagesAsync(myListingImageDrafts.stream()
                    .map(MyListingImageDraft::getImage)
                    .collect(Collectors.toList()));

            myListingImageDraftRepository.deleteAll(myListingImageDrafts);
        }
    }

    private void deleteS3ImagesAsync(List<String> links) {
        List<String> s3ImageKeys = S3Utils.getObjectKeysFromLinks(awsResourceConfig, links);

        if (s3ImageKeys.size() > 0) {
            s3Manager.deleteObjectAsync(S3Value.FOLDER_MARKET_LISTING_PTP_ORIGINAL, s3ImageKeys);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_MARKET_LISTING_PTP_LARGE, s3ImageKeys);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_MARKET_LISTING_PTP_SMALL, s3ImageKeys);
        }
    }

    private MyListingDraft findDaftById(long id) throws ExceptionResponse {
        MyListingDraft myListingDraft = myListingDraftRepository.findOne(id);
        if (myListingDraft == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MY_LISTING_DRAFT_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return myListingDraft;
    }

    public String convertRequest(PersonalListingRequest request) throws ExceptionResponse {
        String content = null;
        try {
            content = objectMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            throw new ExceptionResponse(
                    ObjectError.UNKNOWN_ERROR,
                    MY_LISTING_DRAFT_SAVE_FAIL,
                    HttpStatus.BAD_REQUEST
            );
        }
        return content;
    }

    public PersonalListingRequest convertRequest(String content) throws ExceptionResponse {
        //convert draft request to create my listing request
        PersonalListingRequest request;
        try {
            request = objectMapper.readValue(content, PersonalListingRequest.class);
        } catch (IOException e) {
            if (e instanceof JsonParseException) {
            }
            if (e instanceof JsonMappingException) {
            }
            e.printStackTrace();
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST
            );
        }
        return request;
    }

    private void validateDraftRequest(
            PersonalListingRequest request, boolean fullValidate
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        if (fullValidate) {
            //manual trigger validating
            BeanPropertyBindingResult errors = new BeanPropertyBindingResult(request, "request");
            validator.validate(request, errors);
            Method currentMethod = new Object() {
            }.getClass().getEnclosingMethod();
            if (errors.hasErrors()) {
                throw new MethodArgumentNotValidException(
                        new MethodParameter(currentMethod, 0),
                        errors);
            }
            //end
        }

        if (!StringUtils.isBlank(request.getYearName()) || fullValidate) {
            CommonUtils.checkYearValid(request.getYearName());
        }
        //valid email
        if (fullValidate) {
            CommonUtils.checkEmailFormatValid(request.getEmailPaypal());
        }
        //check auto price
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_PARAM;
        if (fullValidate) {
            validatePrices(
                    request.getSalePrice(),
                    request.getIsBestOffer(),
                    request.getBestOfferAutoAcceptPrice(),
                    request.getMinimumOfferAutoAcceptPrice()
            );
        }
        if (fullValidate) {
            if (request.getShippingType() != null &&
                    request.getShippingType() == OutboundShippingType.FLAT_RATE_TYPE
            ) {
                if (request.getFlatRate() == null || request.getFlatRate() < 0) {
                    errorMessage = FLAT_RATE_MUST_POSITIVE;
                }
            }
        }
        if (errorMessage != null) {
            throw new ExceptionResponse(
                    errorCode, errorMessage, HttpStatus.BAD_REQUEST
            );
        }
        //check size
//        if (request.getBicycleSizeId() != null) {
//            BicycleSize bicycleSize = bicycleSizeRepository.findOne(request.getBicycleSizeId());
//            if (bicycleSize == null) {
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BICYCLE_SIZE_NOT_EXIST),
//                        HttpStatus.BAD_REQUEST);
//            }
//        }
        //valid type
        if (fullValidate || request.getBicycleTypeId() != null) {
            BicycleType bicycleType = bicycleTypeRepository.findOne(request.getBicycleTypeId());
            if (bicycleType == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        TYPE_ID_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }
            if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
                validateTypeEbike(request.geteBikeMileage(), request.geteBikeHours());
            }
        }

        //valid upgrade component component
        if (fullValidate || (request.getCompRequests() != null && request.getCompRequests().size() > 0)) {
            tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests());
        }

        if (fullValidate) {
            BicycleSize bicycleSize = verifyCompMyListingReturnSizeId(request.getCompRequests());
            if (bicycleSize == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BICYCLE_SIZE_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }

            validateShippingLocation(
                    request.getAddressLine(),
                    request.getCityName(), request.getState(),
                    request.getZipCode(), request.getCountry()
            );
        }
    }

    public void validatePrices(Float salePrice, Boolean isBestOffer, Float bestOfferAutoPrice, Float minOfferAutoPrice) throws ExceptionResponse {
        String errorMessage = null;

        if (salePrice == null || salePrice <= 0) {
            errorMessage = SALE_PRICE_MUST_POSITIVE;
        } else if (isBestOffer != null && isBestOffer) {
            if (bestOfferAutoPrice == null && minOfferAutoPrice == null) {
                errorMessage = YOU_MISS_MIN_PRICE_OR_BEST_PRICE;
            } else {
                if (bestOfferAutoPrice != null && minOfferAutoPrice != null) {
                    if (minOfferAutoPrice >= bestOfferAutoPrice) {
                        errorMessage = MIN_PRICE_MUST_BE_LESS_THAN_MAX_PRICE;
                    }
                }
                if (minOfferAutoPrice != null) {
                    if (minOfferAutoPrice <= 0) {
                        errorMessage = MIN_PRICE_MUST_BE_HIGHER_THAN_ZERO;
                    } else if (minOfferAutoPrice >= salePrice) {
                        errorMessage = MIN_OFFER_PRICE_MUST_LESS_THAN_SALE_PRICE;
                    }
                }
                if (bestOfferAutoPrice != null &&
                        bestOfferAutoPrice > salePrice
                ) {
                    errorMessage = BEST_OFFER_PRICE_MUST_EQUAL_OR_LESS_THAN_SALE_PRICE;
                }
            }
        }
        checkError(errorMessage, ObjectError.ERROR_PARAM);
    }

    private List<S3UploadResponse<Long>> uploadMyListingImages(List<MultipartFile> images, String id) {
        return uploadImages(images, Constants.MARKET_LISTING_PTP_FOLDER, id);
    }

    private List<S3UploadResponse<Long>> uploadDraftImages(List<MultipartFile> images, String id) {
        return uploadImages(images, S3Value.PREFIX_DRAFT_LISTING_PTP, id);
    }

    private List<S3UploadResponse<Long>> uploadImages(List<MultipartFile> images, String prefix, String id) {
        List<S3UploadImage<Long>> uploads = new ArrayList<>();
        for (MultipartFile multipartFile : images) {
            S3UploadImage<Long> uploadImage = new S3UploadImage(prefix + "_" + id,
                    S3Value.FOLDER_MARKET_LISTING_PTP_ORIGINAL, multipartFile);
            uploads.add(uploadImage);
        }

        List<S3UploadResponse<Long>> s3UploadResponses = S3UploadUtils.uploads(s3Manager, uploads);

        return s3UploadResponses;
    }

    public GoogleServiceResponse validateShippingLocation(
            String addressLine,
            String cityName, String stateValue,
            String zipCode,
            String countryValue
    ) throws ExceptionResponse {
        String fullLocation = addressLine + ", " + cityName + ", " + stateValue + ", " + zipCode + ", " + countryValue;
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_PARAM;

        GoogleServiceResponse googleResponse = authManager.getMapGoogleSerive(
                IntegratedServices.GOOGLE_SERVICE_CATEGORY_INFO, fullLocation);
        if (googleResponse.getResults().size() < 1) {
            errorMessage = MessageResponses.SHIPPING_LOCATION_INVALID;
        } else {
            AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
            AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
            AddressComponent city = GoogleServiceUtils.getCity(googleResponse);
            String zipCodeResponse = GoogleServiceUtils.getZipCode(googleResponse);

            if (country == null
                    || (!country.getLongName().equalsIgnoreCase(countryValue)
                    && !country.getShortName().equalsIgnoreCase(countryValue))
            ) {
                errorMessage = MessageResponses.SHIPPING_COUNTRY_INVALID;
            } else if (state == null
                    || (!state.getLongName().equalsIgnoreCase(stateValue)
                    && !state.getShortName().equalsIgnoreCase(stateValue))
            ) {
                errorMessage = MessageResponses.SHIPPING_STATE_INVALID;
            } else if (city == null
                    || !city.getLongName().equalsIgnoreCase(cityName)
            ) {
                errorMessage = MessageResponses.SHIPPING_CITY_INVALID;
            } else if (zipCodeResponse == null || !zipCode.equals(zipCodeResponse)) {
                errorMessage = MessageResponses.SHIPPING_ZIP_CODE_INVALID;
            }
        }

        checkError(errorMessage, errorCode);

        return googleResponse;
    }

    private void validateTypeEbike(Float eBikeMileage, Float eBikeHours) throws ExceptionResponse {
        if (eBikeMileage != null && eBikeMileage < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EBIKE_MILEAGE_MUST_BE_POSITIVE,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (eBikeHours != null && eBikeHours < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EBIKE_HOUR_MUST_BE_POSITIVE,
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void checkUserHasPermission(Inventory inventory) throws ExceptionResponse {
        if (!inventory.getSellerId().equals(CommonUtils.getUserLogin())
                && !CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())
        ) {
            if (inventory.getStorefrontId() == null
                    || !inventory.getStorefrontId().equals(CommonUtils.getStorefrontId())) {
                checkError(MessageResponses.YOU_DONT_HAVE_PERMISSION, ObjectError.NO_PERMISSION, HttpStatus.FORBIDDEN);
            }
        }
    }

    private void checkUserHasPermission(MyListingDraft myListingDraft) throws ExceptionResponse {
        if (!myListingDraft.getCreatedBy().equals(CommonUtils.getUserLogin())
                && !CommonUtils.checkHaveRole(UserRoleType.ADMIN)
        ) {
            if (myListingDraft.getStorefrontId() == null
                    || !myListingDraft.getStorefrontId().equals(CommonUtils.getStorefrontId())) {
                checkError(MessageResponses.YOU_DONT_HAVE_PERMISSION, ObjectError.NO_PERMISSION, HttpStatus.FORBIDDEN);
            }
        }
    }
    //endregion
}
