package com.bbb.core.model.response.market.marketlisting;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class UserMarketListingResponse {
    @Id
    private int marketListingId;
    private long marketPlaceId;
    private MarketType marketType;
    private StatusMarketListing statusMarketListing;
    private LocalDateTime timeListed;

    private long inventoryId;
    private String imageDefault;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private String inventoryTitle;
    private ConditionInventory condition;
    private String serialNumber;

    private long bicycleId;
    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    private String bicycleSizeName;

    private Float msrpPrice;
    private Float currentListedPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private Boolean isBestOffer;

    private String partnerId;
    private String sellerId;


    public int getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(int marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public LocalDateTime getTimeListed() {
        return timeListed;
    }

    public void setTimeListed(LocalDateTime timeListed) {
        this.timeListed = timeListed;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getBicycleSizeName() {
        return bicycleSizeName;
    }

    public void setBicycleSizeName(String bicycleSizeName) {
        this.bicycleSizeName = bicycleSizeName;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }
}
