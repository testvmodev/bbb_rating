package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ListingDesigner {
    @JacksonXmlProperty(localName = "LayoutID")
    private long layoutID;
    @JacksonXmlProperty(localName = "ThemeID")
    private long themeID;

    public long getLayoutID() {
        return layoutID;
    }

    public void setLayoutID(long layoutID) {
        this.layoutID = layoutID;
    }

    public long getThemeID() {
        return themeID;
    }

    public void setThemeID(long themeID) {
        this.themeID = themeID;
    }
}
