package com.bbb.core.model.database.type;

public enum StatusOffer {
    PENDING("Pending"),
    COUNTERED("Countered"),
    ACCEPTED("Accepted"),
    REJECTED("Rejected"),
    COMPLETED("Completed"),
    CLOSED_REMOVED_FROM_CART("Closed: Removed From Cart"),
    CLOSED_CART_EXPIRED("Closed: Cart Expired"),
    CANCELLED("Cancelled");
    private final String value;

    StatusOffer(String value) {
        this.value = value;
    }

    public static StatusOffer findByValue(String value) {
        for (StatusOffer statusOffer : StatusOffer.values()) {
            if (statusOffer.getValue().equals(value)) {
                return statusOffer;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
