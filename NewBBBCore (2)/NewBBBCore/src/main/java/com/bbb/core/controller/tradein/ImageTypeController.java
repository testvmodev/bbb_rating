package com.bbb.core.controller.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.service.tradein.TradeInImageTypeService;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImageTypeController {
    @Autowired
    private TradeInImageTypeService service;

    @GetMapping(Constants.URL_GET_TRADE_IN_IMAGE_TYPE)
    public ResponseEntity getImageTypes() {
        return new ResponseEntity<>(service.getImagesTypes(), HttpStatus.OK);
    }
}
