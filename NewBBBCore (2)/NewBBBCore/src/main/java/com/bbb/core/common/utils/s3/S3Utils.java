package com.bbb.core.common.utils.s3;

import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Value;

import java.util.ArrayList;
import java.util.List;

public class S3Utils {
    public static List<String> getObjectKeysFromLinks(AwsResourceConfig awsResourceConfig, List<String> links) {
        List<String> keys = new ArrayList<>();
        links.forEach(link -> {
            if (link.contains(awsResourceConfig.getDefaultS3Bucket()) &&
                    (link.contains(S3Value.FOLDER_MARKET_LISTING_PTP_ORIGINAL) ||
                            link.contains(S3Value.FOLDER_TRADEIN_ORIGINAL))
            ) {
                keys.add(link.substring(link.lastIndexOf("/") + 1));
            }
        });
        return keys;
    }
}
