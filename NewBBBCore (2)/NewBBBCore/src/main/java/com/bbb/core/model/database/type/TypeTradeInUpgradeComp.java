package com.bbb.core.model.database.type;

public enum TypeTradeInUpgradeComp {
    WHEELS("wheels"),
    DRIVE_TRAIN("Drivetrain");
    private final String value;

    TypeTradeInUpgradeComp(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TypeTradeInUpgradeComp findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "wheels":
                return WHEELS;
            case "Drivetrain":
                return DRIVE_TRAIN;
            default:
                return null;
        }
    }
}
