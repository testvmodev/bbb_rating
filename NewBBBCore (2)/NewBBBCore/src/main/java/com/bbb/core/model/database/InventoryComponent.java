package com.bbb.core.model.database;


public class InventoryComponent {

  private long inventoryId;
  private long componentId;


  public long getInventoryId() {
    return inventoryId;
  }

  public void setInventoryId(long inventoryId) {
    this.inventoryId = inventoryId;
  }


  public long getComponentId() {
    return componentId;
  }

  public void setComponentId(long componentId) {
    this.componentId = componentId;
  }

}
