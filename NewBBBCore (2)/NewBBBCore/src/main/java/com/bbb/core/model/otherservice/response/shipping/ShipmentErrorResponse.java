package com.bbb.core.model.otherservice.response.shipping;

import org.joda.time.LocalDateTime;

public class ShipmentErrorResponse {
    private String errorCode;
    private String message;
    private LocalDateTime time;
    private String path;


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
