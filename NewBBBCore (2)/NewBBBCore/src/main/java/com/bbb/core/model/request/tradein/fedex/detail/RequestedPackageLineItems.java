package com.bbb.core.model.request.tradein.fedex.detail;

import com.bbb.core.common.fedex.FedExValue;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RequestedPackageLineItems {
    @JacksonXmlProperty(localName = "SequenceNumber")
    private int sequenceNumber = FedExValue.PACKAGE_PER_SHIP_DEFAULT;
    @JacksonXmlProperty(localName = "GroupPackageCount")
    private int groupPackageCount = FedExValue.PACKAGE_PER_SHIP_DEFAULT;
    @JacksonXmlProperty(localName = "Weight")
    private Weight weight;
    @JacksonXmlProperty(localName = "Dimensions")
    private Dimensions dimensions;

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public int getGroupPackageCount() {
        return groupPackageCount;
    }

    public void setGroupPackageCount(int groupPackageCount) {
        groupPackageCount = groupPackageCount;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }
}
