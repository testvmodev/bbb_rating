package com.bbb.core.model.response.market.marketlisting.detail;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MarketListingInfoBid {
    @Id
    private long inventoryAuctionId;
    private long auctionId;
    private Float maxBidPrice;
    private Float minBidPrice;

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public Float getMaxBidPrice() {
        return maxBidPrice;
    }

    public void setMaxBidPrice(Float maxBidPrice) {
        this.maxBidPrice = maxBidPrice;
    }

    public Float getMinBidPrice() {
        return minBidPrice;
    }

    public void setMinBidPrice(Float minBidPrice) {
        this.minBidPrice = minBidPrice;
    }

    public void setInventoryAuctionId(long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }
}
