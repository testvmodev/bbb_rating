package com.bbb.core.common.fedex;

public enum FedExServices {
    GROUND("FEDEX_GROUND"), //US, 1-5 days, cheap
    GROUND_HOME("GROUND_HOME_DELIVERY"), //residential delivery
    EXPRESS_SAVER("FEDEX_EXPRESS_SAVER"), //3 days
    EXPRESS_NEXT_MORNING("FEDEX_NEXT_DAY_EARLY_MORNING"), //faster + more expensive
    EXPRESS_NEXT_AFTERNOON("FEDEX_NEXT_DAY_AFTERNOON"), //faster + more expensive
    INTERNATIONAL_GROUND("INTERNATIONAL_GROUND"), //US - Canada
    INTERNATIONAL_ECONOMY("INTERNATIONAL_ECONOMY"), //lower cost, worldwide
    INTERNATIONAL_EXPRESS("INTERNATIONAL_PRIORITY_EXPRESS"), //worldwide
    SAME_DAY("SAME_DAY"); //urgent, costly
    //.....

    private final String value;

    FedExServices(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
