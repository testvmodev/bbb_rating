package com.bbb.core.model.database.type;

public enum OutboundShippingType {
    BICYCLE_BLUE_BOOK_TYPE("BicycleBlueBook.com"),
    FLAT_RATE_TYPE("Flat rate");

    private String value;

    OutboundShippingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static OutboundShippingType findByValue(String value) {
        if (value == null) return null;

        for (OutboundShippingType shippingType : OutboundShippingType.values()) {
            if (shippingType.getValue().equals(value)) {
                return shippingType;
            }
        }
        return null;
    }
}
