package com.bbb.core.service.impl.shipping;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.shipping.ShippingProfileManager;
import com.bbb.core.model.request.shipping.ShippingFeeConfigCreateRequest;
import com.bbb.core.model.request.shipping.ShippingFeeConfigUpdateRequest;
import com.bbb.core.service.shipping.ShippingProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShippingProfileServiceImpl implements ShippingProfileService {
    @Autowired
    private ShippingProfileManager manager;

    @Override
    public Object getShippingProfiles(Pageable pageable) {
        return manager.getShippingProfiles(pageable);
    }

    @Override
    public Object addShippingProfile(ShippingFeeConfigCreateRequest request) throws ExceptionResponse {
        return manager.addShippingProfile(request);
    }

    @Override
    public Object updateShippingProfile(long bicycleTypeId, ShippingFeeConfigUpdateRequest request) throws ExceptionResponse {
        return manager.updateShippingProfile(bicycleTypeId, request);
    }

    @Override
    public Object getInventoryShippingProfiles(List<Long> inventoryIds) {
        return manager.getInventoryShippingProfiles(inventoryIds);
    }
}
