package com.bbb.core.service.impl.inventory;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.inventory.ComponentInventoryManager;
import com.bbb.core.service.inventory.ComponentInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComponentInventoryServiceImpl implements ComponentInventoryService {
    @Autowired
    private ComponentInventoryManager manager;
    @Override
    public Object getInventoriesSearch() {
        return manager.getComponentInventoriesSearch();
    }

    @Override
    public Object getAllBicycleModel(List<Long> brandIds) throws ExceptionResponse {
        return manager.getAllBicycleModel(brandIds);
    }


}
