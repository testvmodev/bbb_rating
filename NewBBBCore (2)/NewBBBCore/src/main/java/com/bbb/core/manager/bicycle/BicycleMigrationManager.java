package com.bbb.core.manager.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.database.BicycleComponent;
import com.bbb.core.model.database.BicycleSpec;
import com.bbb.core.model.database.ComponentType;
import com.bbb.core.model.request.bicycle.BicycleImportRequest;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.bicycle.BicycleComponentRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import io.reactivex.Observable;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class BicycleMigrationManager {
    private static final Logger LOG = LoggerFactory.getLogger(BicycleMigrationManager.class);
    private Boolean importingBicycles = false;
    private Boolean importingCompSpecSE = false;
    private Boolean importingBrands = false;
    private Boolean importingModels = false;

    private Boolean stopImportingCompSpecSE = false;

    private Map<Long, CSVRecord> bicycleMapImporting; //bicycle id

    private Map<Long, List<CSVRecord>> imageMapImporting; //bicycle id

    private Map<Long, List<CSVRecord>> compSpecMapImporting; //smart etailing id
    private int totalSERecords;
    private int compSpecDone;
    private int totalComps = 0;
    private int totalSpecs = 0;

    //region Beans
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private BicycleComponentManager bicycleComponentManager;
    @Autowired
    private BicycleComponentRepository bicycleComponentRepository;
    @Autowired
    private ComponentTypeRepository componentTypeRepository;
    //endregion

    @SuppressWarnings("Duplicates")
    @Deprecated
    public Object importBicycles(BicycleImportRequest request) throws ExceptionResponse {
        if (getImportingBicycles()) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    "Importing in progress",
                    HttpStatus.UNPROCESSABLE_ENTITY
            );
        }

        setImportingBicycles(true);

        CSVParser bicycleParser = readCSVFromUrl(request.getUrlBicycle());
        CSVParser imageParser = readCSVFromUrl(request.getUrlImage());
        CSVParser compSpecParser = readCSVFromUrl(request.getUrlCompSpec());

        bicycleMapImporting = new HashMap<>();
        imageMapImporting = new HashMap<>();
        compSpecMapImporting = new HashMap<>();


        boolean isSuccess = Observable.zip(
                CommonUtils.getOb(() -> {
                    while (bicycleParser.iterator().hasNext()) {
                        CSVRecord record = bicycleParser.iterator().next();
                        Long bicycleId = Long.valueOf(record.get(0));

                        bicycleMapImporting.put(bicycleId, record);
                    }
                    return bicycleMapImporting;
                }),

                CommonUtils.getOb(() -> {
                    while (imageParser.iterator().hasNext()) {
                        CSVRecord record = imageParser.iterator().next();
                        Long bicycleId = Long.valueOf(record.get(1));

                        if (!imageMapImporting.containsKey(bicycleId)) {
                            imageMapImporting.put(bicycleId, new ArrayList<>());
                        }

                        imageMapImporting.get(bicycleId).add(record);
                    }
                    return imageMapImporting;
                }),
                CommonUtils.getOb(() -> {
                    while (compSpecParser.iterator().hasNext()) {
                        CSVRecord record = compSpecParser.iterator().next();
                        Long smartEtailingId = Long.valueOf(record.get(1));

                        if (!compSpecMapImporting.containsKey(smartEtailingId)) {
                            compSpecMapImporting.put(smartEtailingId, new ArrayList<>());
                        }

                        compSpecMapImporting.get(smartEtailingId).add(record);
                    }
                    return compSpecMapImporting;
                }),
                (o, o2, o3) -> {return true;}
        ).blockingFirst();
        compSpecMapImporting.size();


        for (Long bicycleId : bicycleMapImporting.keySet()) {
            CSVRecord record = bicycleMapImporting.get(bicycleId);

            //"Id","Brand_Id","Model_Id","Type_Id","Year_Id","RetailPrice","Active","Brand_Name","Model_Name","Type_Name","ImageUri","SmartEtailing_Id"
            Bicycle bicycle = new Bicycle();
            bicycle.setId(Long.valueOf(record.get(0)));
            bicycle.setBrandId(Long.valueOf(record.get(1)));
            bicycle.setModelId(Long.valueOf(record.get(2)));
            bicycle.setTypeId(Long.valueOf(record.get(3)));
            bicycle.setYearId(Long.valueOf(record.get(4)));
            bicycle.setRetailPrice(Float.valueOf(record.get(5)));
            bicycle.setActive(record.get(6).equals("0") ? false : true);

            if (imageMapImporting.containsKey(bicycle.getId())) {
                //"Id","Bicycle_Id","Uri"
                bicycle.setImageDefault(imageMapImporting.get(bicycle.getId()).get(0).get(2));
            } else {
                LOG.debug("Bicycle " + bicycle.getId() + "doesnt have any image");
                continue;
            }

            LOG.debug("Save bicycle " + bicycle.getId());
            bicycleRepository.save(bicycle);
        }

        return null;
    }

    public Object statusImportCompSpec() throws ExceptionResponse {
        if (!getImportingCompSpecSE()) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    "Not importing",
                    HttpStatus.NOT_FOUND
            );
        }

        return "Importing " + compSpecDone + " of " + totalSERecords + ", saved " + totalComps + " comps, " + totalSpecs + " specs";
    }

    @SuppressWarnings("Duplicates")
    public Object importCompSpec(String urlBicycle, String urlSE, int skipRecord) throws ExceptionResponse {
        if (getImportingCompSpecSE()) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    "Importing in progress",
                    HttpStatus.UNPROCESSABLE_ENTITY
            );
        }

        setImportingCompSpecSE(true);
        setStopImportingCompSpecSE(false);
        totalSERecords = 0;
        compSpecDone = 0;
        compSpecMapImporting = new HashMap<>();
        int totalValidBicycles = 0;
        int totalInvalidSERecords = 0;
        final int saveEach = 20;

        try {
            CSVParser bicycleParser = readCSVFromUrl(urlBicycle);
            CSVParser compSpecParser = readCSVFromUrl(urlSE);

            //smart etailing id
            Map<Long, CSVRecord> bicycleMap = new HashMap<>();

            boolean isSuccess = Observable.zip(
                    CommonUtils.getOb(() -> {
                        LOG.info("Downloading bicycles");
                        while (bicycleParser.iterator().hasNext()) {
                            CSVRecord record = bicycleParser.iterator().next();
                            String smartEtailing = record.get(11);
                            if (StringUtils.isBlank(smartEtailing)) {
                                continue;
                            }
                            Long smartEtailingId = Long.valueOf(record.get(11));

                            bicycleMap.put(smartEtailingId, record);
                        }
                        try {
                            bicycleParser.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return bicycleMap;
                    }),

                    CommonUtils.getOb(() -> {
                        LOG.info("Downloading SE");
                        while (compSpecParser.iterator().hasNext()) {
                            CSVRecord record = compSpecParser.iterator().next();
                            Long smartEtailingId = Long.valueOf(record.get(1));

                            if (!compSpecMapImporting.containsKey(smartEtailingId)) {
                                compSpecMapImporting.put(smartEtailingId, new ArrayList<>());
                            }

                            compSpecMapImporting.get(smartEtailingId).add(record);
                            totalSERecords++;
                        }
                        try {
                            compSpecParser.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return compSpecMapImporting;
                    }),

                    (o, o1) -> true
            ).blockingFirst();

            //bicycle id
            List<Pair<Long, String>> contents = new ArrayList<>();
            List<ComponentType> componentTypes = componentTypeRepository.findAll();
            int doneSkip = 0;

            //"Id","Sku","SEBrandName","BBBBrandId","BBBBrandName","SEModelName","BBBModelId","BBBModelName","SEModelYear","SECategoryId","SECategoryName","BBBTypeId","BBBTypeName","RetailPrice","FoundMatch","XmlData"

            for (Long seId : compSpecMapImporting.keySet()) {
                if (getStopImportingCompSpecSE()) {
                    break;
                }

                for (CSVRecord record : compSpecMapImporting.get(seId)) {
                    if (skipRecord > 0) {
                        if (doneSkip < skipRecord) {
                            doneSkip++;
                            continue;
                        }
                    }

                    String xml = record.get(15);
                    if (bicycleMap.containsKey(seId)) {
                        Long bicycleId = Long.valueOf(bicycleMap.get(seId).get(0));
                        contents.add(new Pair<>(bicycleId, xml));
                    } else {
                        LOG.warn("SE data doesnt belong to bicycle, id: " + record.get(0));
                        totalInvalidSERecords += 1;
                    }
                }

                //saving each
                if (contents.size() >= saveEach) {
                    Pair<List<BicycleComponent>, List<BicycleSpec>> parsed = bicycleComponentManager.parseContents(
                            contents, componentTypes
                    );
                    List<BicycleComponent> bicycleComponents = parsed.getFirst();
                    List<BicycleSpec> bicycleSpecs = parsed.getSecond();

                    LOG.info("delete old comp/spec if exists...");
                    List<Long> bicycleIds = contents.stream()
                            .map(Pair::getFirst)
                            .collect(Collectors.toList());
                    CommonUtils.stream(bicycleIds);
                    bicycleComponentRepository.deleteAllByBicycleIds(bicycleIds);
                    LOG.info("saving some comp/spec...");
                    boolean success = Observable.zip(
                            bicycleComponentManager.saveBicycleComponentType(bicycleComponents),
                            bicycleComponentManager.saveBicycleSpec(bicycleSpecs),
                            (o1, o2) -> true
                    ).blockingFirst();

                    compSpecDone += contents.size();
                    totalValidBicycles += bicycleIds.size();
                    totalComps += bicycleComponents.size();
                    totalSpecs += bicycleSpecs.size();
                    contents.clear();
                    LOG.info("compSpecDone: " + compSpecDone + ", totalInvalidSERecords: " + totalInvalidSERecords);
                }
            }

        } catch (Exception e) {
            setImportingCompSpecSE(false);
            throw e;
        }

        setImportingCompSpecSE(false);
        LOG.info("Done importing " + compSpecDone + " valid records, got " + totalComps + " comps, " + totalSpecs + " specs");
        return "Done importing " + compSpecDone + " valid records, got " + totalComps + " comps, " + totalSpecs + " specs";
    }

    public Object stopImportCompSpec() throws ExceptionResponse {
        if (!getImportingCompSpecSE()) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    "Not importing",
                    HttpStatus.BAD_REQUEST
            );
        }

        setStopImportingCompSpecSE(true);
        return "Stopping task..";
    }

//    public Object importBrands() throws ExceptionResponse {
//        if (getImportingBrands()) {
//            throw new ExceptionResponse(
//                    ObjectError.INVALID_ACTION,
//                    "Importing in progress",
//                    HttpStatus.UNPROCESSABLE_ENTITY
//            );
//        }
//
//        setImportingBrands(true);
//
//        return null;
//    }
//
//    public Object importModels() throws ExceptionResponse {
//        if (getImportingModels()) {
//            throw new ExceptionResponse(
//                    ObjectError.INVALID_ACTION,
//                    "Importing in progress",
//                    HttpStatus.UNPROCESSABLE_ENTITY
//            );
//        }
//
//        setImportingModels(true);
//
//        return null;
//    }

    private CSVParser readCSVFromUrl(String url) throws ExceptionResponse {
        try {
            URL data = new URL(url);
            InputStream stream = data.openStream();
            InputStreamReader reader = new InputStreamReader(stream);
            CSVParser parser = CSVFormat.DEFAULT.parse(reader);

            if (parser.iterator().hasNext()) {
                //skip first row which is title
                parser.iterator().next();
            }

            return parser;
        } catch (IOException e) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    e.getMessage(),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private Boolean getImportingBicycles() {
        synchronized (importingBicycles) {
            return importingBicycles;
        }
    }

    private void setImportingBicycles(Boolean importingBicycles) {
        synchronized (importingBicycles) {
            this.importingBicycles = importingBicycles;
        }
    }

    private Boolean getImportingCompSpecSE() {
        synchronized (importingCompSpecSE) {
            return importingCompSpecSE;
        }
    }

    private void setImportingCompSpecSE(Boolean importingCompSpecSE) {
        synchronized (importingCompSpecSE) {
            this.importingCompSpecSE = importingCompSpecSE;
        }
    }

    private Boolean getImportingBrands() {
        synchronized (importingBrands) {
            return importingBrands;
        }
    }

    private void setImportingBrands(Boolean importingBrands) {
        synchronized (importingBrands) {
            this.importingBrands = importingBrands;
        }
    }

    private Boolean getImportingModels() {
        synchronized (importingModels) {
            return importingModels;
        }
    }

    private void setImportingModels(Boolean importingModels) {
        synchronized (importingModels) {
            this.importingModels = importingModels;
        }
    }

    private Boolean getStopImportingCompSpecSE() {
        synchronized (stopImportingCompSpecSE) {
            return stopImportingCompSpecSE;
        }
    }

    private void setStopImportingCompSpecSE(Boolean stopImportingCompSpecSE) {
        synchronized (stopImportingCompSpecSE) {
            this.stopImportingCompSpecSE = stopImportingCompSpecSE;
        }
    }
}
