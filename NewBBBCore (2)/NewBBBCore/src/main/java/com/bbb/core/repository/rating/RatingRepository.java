package com.bbb.core.repository.rating;

import com.bbb.core.model.database.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends JpaRepository<Rating,Long> {

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM rating " +

            "WHERE " +
            "(:marketListingId IS NULL OR rating.market_listing_id = :marketListingId) " +
            "AND (:inventoryAuctionId IS NULL OR rating.inventory_auction_id = :inventoryAuctionId)"
    )
    int countAllRatingResponse(
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId
    );

    @Query(nativeQuery = true, value = "SELECT ROUND(AVG(rating.rate), 1) " +
            "FROM rating " +

            "WHERE " +
            "(:marketListingId IS NULL OR rating.market_listing_id = :marketListingId) " +
            "AND (:inventoryAuctionId IS NULL OR rating.inventory_auction_id = :inventoryAuctionId)"
    )
    float avgAllRating(
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId
    );

    @Query(nativeQuery = true,value = "SELECT * FROM rating " +
            "WHERE user_id= :userId AND inventory_auction_id= :inventoryAuctionId")
    Rating findOneByUserIdAndInventoryAuctionId(String userId, Long inventoryAuctionId);

    @Query(nativeQuery = true,value = "SELECT * FROM rating WHERE id= :id")
    Rating findOneById(long id);

    @Query(nativeQuery = true,value = "SELECT ROUND(AVG(rate),1) FROM rating WHERE inventory_auction_id = :inventoryAuctionId")
    float avgRatingForAuction(Long inventoryAuctionId);

    @Query(nativeQuery = true,value = "SELECT ROUND(AVG(rate),1) FROM rating WHERE market_listing_id = :marketListingId")
    float avgRatingForMarket(Long marketListingId);

    @Query(nativeQuery = true,value = "SELECT * FROM rating " +
            "WHERE user_id= :userId AND market_listing_id= :marketListingId")
    Rating findOneByUserIdAndMarketListingId(String userId, Long marketListingId);

}
