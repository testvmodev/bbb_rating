package com.bbb.core.model.request.tradein.ups;

import com.bbb.core.model.request.tradein.ups.detail.ship.ShipmentRequest;
import com.bbb.core.model.request.tradein.ups.detail.account.UPSSecurity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSShipRequest {
    @JsonProperty("UPSSecurity")
    private UPSSecurity upsSecurity;
    @JsonProperty("ShipmentRequest")
    private ShipmentRequest shipmentRequest;

    public UPSSecurity getUpsSecurity() {
        return upsSecurity;
    }

    public void setUpsSecurity(UPSSecurity upsSecurity) {
        this.upsSecurity = upsSecurity;
    }

    public ShipmentRequest getShipmentRequest() {
        return shipmentRequest;
    }

    public void setShipmentRequest(ShipmentRequest shipmentRequest) {
        this.shipmentRequest = shipmentRequest;
    }
}
