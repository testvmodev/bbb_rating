package com.bbb.core.service.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.MarketListingRequest;
import com.bbb.core.model.request.market.UserMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.Test;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MarketListingService {

    Object getMarketListings(MarketListingRequest request, Pageable page) throws ExceptionResponse;

    Object notificationEbay(String contentNotification) throws Exception;

    Object getMarketListingDetail(long marketListingId, boolean isView) throws ExceptionResponse;

    Object getRecommends() throws ExceptionResponse;

    Object getSecretMarketListing(long marketListingId) throws ExceptionResponse;

    Object getSecretMarketListings(List<Long> marketListingIds) throws ExceptionResponse;

    Object getUserMarketListings(UserMarketListingRequest request) throws ExceptionResponse;

    Object getMarketListingsForMessage(List<Long> marketListingIds) throws ExceptionResponse;
}
