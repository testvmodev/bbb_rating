package com.bbb.core.model.request.bid.offer;

public class SecretMarketLisitingAuctionRequest {
    private SecretOfferMarketListingAuctionTypeRequest type;
    private long id;

    public SecretOfferMarketListingAuctionTypeRequest getType() {
        return type;
    }

    public void setType(SecretOfferMarketListingAuctionTypeRequest type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
