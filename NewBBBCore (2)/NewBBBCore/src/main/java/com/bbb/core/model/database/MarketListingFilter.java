package com.bbb.core.model.database;

import com.bbb.core.model.database.type.MarketListingFilterType;
import com.bbb.core.model.database.type.MarketListingFilterTypeTable;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name = "market_listing_filter")
public class MarketListingFilter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private MarketListingFilterType type;
    private MarketListingFilterTypeTable typeTable;
    private boolean isActive;
    private boolean isDelete;
    private LocalDateTime createdTime;
    private String fieldName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MarketListingFilterType getType() {
        return type;
    }

    public void setType(MarketListingFilterType type) {
        this.type = type;
    }

    public MarketListingFilterTypeTable getTypeTable() {
        return typeTable;
    }

    public void setTypeTable(MarketListingFilterTypeTable typeTable) {
        this.typeTable = typeTable;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
