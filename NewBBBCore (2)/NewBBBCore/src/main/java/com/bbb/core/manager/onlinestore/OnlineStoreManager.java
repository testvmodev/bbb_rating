package com.bbb.core.manager.onlinestore;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.model.database.InventorySale;
import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.onlinestore.*;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.onlinestore.OnlineStoreListingResponse;
import com.bbb.core.model.response.onlinestore.OnlineStoreResponse;
import com.bbb.core.model.response.onlinestore.OnlineStoreSaleReportResponse;
import com.bbb.core.repository.market.InventorySaleRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.bbb.core.repository.onlinestore.dashboard.OnlineStoreDailySaleResponseRepository;
import com.bbb.core.repository.onlinestore.dashboard.OnlineStoreSaleReportResponseRepository;
import com.bbb.core.repository.onlinestore.dashboard.OnlineStoreSummaryStatsResponseRepository;
import com.bbb.core.repository.onlinestore.market.OnlineStoreListingResponseRepository;
import com.bbb.core.repository.onlinestore.market.OnlineStoreListingSummaryResponseRepository;
import com.bbb.core.repository.onlinestore.OnlineStoreResponseRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OnlineStoreManager {
    @Autowired
    private OnlineStoreResponseRepository onlineStoreResponseRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private OnlineStoreListingResponseRepository onlineStoreListingResponseRepository;
    @Autowired
    private InventorySaleRepository inventorySaleRepository;
    @Autowired
    private MyListingManager myListingManager;
    @Autowired
    private OnlineStoreListingSummaryResponseRepository onlineStoreListingSummaryResponseRepository;
    @Autowired
    private OnlineStoreSummaryStatsResponseRepository onlineStoreSummaryStatsResponseRepository;
    @Autowired
    private OnlineStoreSaleReportResponseRepository onlineStoreSaleReportResponseRepository;
    @Autowired
    private OnlineStoreDailySaleResponseRepository onlineStoreDailySaleResponseRepository;

    public Object getTrendingOnlineStores(TrendingOnlineStoreRequest request) throws ExceptionResponse {
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        if (request.getStorefrontIds() != null && request.getStorefrontIds().size() > 0) {
            return onlineStoreResponseRepository.findTrendingSorted(
                    request.getStorefrontIds(), marketPlace.getId(), request.getPageable()
            );
        }
        return onlineStoreResponseRepository.findTrendingSorted(marketPlace.getId(), request.getPageable());
    }

    public Object getOnlineStores(OnlineStoreRequest request) throws ExceptionResponse {
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());

        ListObjResponse<OnlineStoreResponse> response = new ListObjResponse(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(request.getStorefrontIds().size());

        response.setData(onlineStoreResponseRepository.findAll(
                request.getStorefrontIds(), marketPlace.getId(),
                request.getSortForSale(),
                request.getPageable()
        ));

        return response;
    }

    public Object getMyListings(OnlineStoreListingRequest request) throws ExceptionResponse {
        request.validate();

        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        String storefrontId = CommonUtils.getStorefrontId();

        ListObjResponse<OnlineStoreListingResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );

        response.setTotalItem(onlineStoreListingResponseRepository.countListings(
                storefrontId,
                marketPlace.getId(),
                request.getContent(),
                request.getStatuses(),
                request.getFromDay(),
                request.getToDay()
        ));

        if (response.getTotalItem() > 0) {
            response.setData(onlineStoreListingResponseRepository.findListingsSorted(
                    storefrontId,
                    marketPlace.getId(),
                    request.getContent(),
                    request.getStatuses(),
                    request.getFromDay(),
                    request.getToDay(),
                    request.getSortField(),
                    request.getSort(),
                    request.getPageable().getOffset(),
                    request.getPageable().getPageSize()
            ));
            if (response.getData().size() > 0) {
                List<Long> soldInventories = new ArrayList<>();
                for (OnlineStoreListingResponse res : response.getData()) {
                    if (res.getStatusMarketListing() == StatusMarketListing.DRAFT) {
                        if (!StringUtils.isBlank(res.getDraft().getRawContent())) {
                            res.getDraft().setContent(myListingManager.convertRequest(res.getDraft().getRawContent()));
                        }
                    } else if (res.getStatusMarketListing() == StatusMarketListing.SOLD) {
                        soldInventories.add(res.getFinished().getInventoryId());
                    }
                }
                if (soldInventories.size() > 0) {
                    List<InventorySale> sales = inventorySaleRepository.findAll(soldInventories);
                    if (sales.size() > 0) {
                        sales.forEach(sale -> {
                            OnlineStoreListingResponse res = CommonUtils.findObject(response.getData(),
                                    r -> r.getFinished() != null && r.getFinished().getInventoryId() == sale.getInventoryId());
                            if (res != null) {
                                res.getFinished().setSale(sale);
                            }
                        });
                    }
                }
            }
        }

        return response;
    }

    public Object getMyListingSummary() throws ExceptionResponse {
        String storefrontId = verifyOnlineStore();
        return onlineStoreListingSummaryResponseRepository.getSummary(storefrontId);
    }

    public Object getStoreSummaryStats() throws ExceptionResponse {
        String storefrontId = verifyOnlineStore();
        return onlineStoreSummaryStatsResponseRepository.getSummary(storefrontId);
    }

    public Object getStoreSaleReport(OnlineStoreSaleReportRequest request) throws ExceptionResponse {
        String storefrontId = CommonUtils.getStorefrontId();
        if (storefrontId == null) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        OnlineStoreSaleReportResponse response = onlineStoreSaleReportResponseRepository.getReport(storefrontId, request.getMonth());
        List<LocalDate> dates = new ArrayList<>();
        YearMonth month = request.getMonth();
        for (int i = 1; i <= month.toLocalDate(1).dayOfMonth().getMaximumValue(); i++) {
            dates.add(month.toLocalDate(i));
        }
        response.setDays(onlineStoreDailySaleResponseRepository.getSaleDetails(storefrontId, dates));

        return response;
    }

    public Object getStoreSaleReportDetails(OnlineStoreSaleReportDetailRequest request) throws ExceptionResponse {
        String storefrontId = CommonUtils.getStorefrontId();
        if (storefrontId == null) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        int days = Days.daysBetween(request.getFromDay(), request.getToDay()).getDays();
        if (days < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    MessageResponses.FROM_DAY_CANT_HIGHER_THAN_TO_DAY,
                    HttpStatus.BAD_REQUEST
            );
        }
        List<LocalDate> dates = new ArrayList<>();
        for (int i = 0; i <= days; i++) {
            dates.add(request.getFromDay().plusDays(i));
        }

        return onlineStoreDailySaleResponseRepository.getSaleDetails(storefrontId, dates);
    }

    private String verifyOnlineStore() throws ExceptionResponse {
        String storefrontId = CommonUtils.getStorefrontId();
        if (storefrontId == null) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        return storefrontId;
    }
}
