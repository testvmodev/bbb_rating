package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.response.ContentCommon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BicycleBaseRepository extends JpaRepository<Bicycle, Long> { //any Entity
    @Query(nativeQuery = true, value = "SELECT " +
            "CASE WHEN EXISTS (SELECT * " +
                "FROM bicycle_brand " +
                "JOIN bicycle_model ON bicycle_model.id = :modelId " +
                "JOIN bicycle_year ON bicycle_year.id = :yearId " +
                "WHERE bicycle_brand.id = :brandId) " +
            "THEN CAST(1 AS BIT) " +
            "ELSE CAST(0 AS BIT) " +
            "END "
    )
    boolean existsBrandModelYear(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId
    );
}
