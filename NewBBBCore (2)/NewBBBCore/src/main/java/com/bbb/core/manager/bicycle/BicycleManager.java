package com.bbb.core.manager.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.ConverterSqlUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.common.utils.s3.S3UploadImage;
import com.bbb.core.common.utils.s3.S3UploadResponse;
import com.bbb.core.common.utils.s3.S3UploadUtils;
import com.bbb.core.common.utils.s3.S3Utils;
import com.bbb.core.manager.bicycle.async.BicycleManagerAsync;
import com.bbb.core.manager.templaterest.RestValueGuideManager;
import com.bbb.core.manager.tradein.TradeInUtil;
import com.bbb.core.manager.valueguide.ValueGuidePriceManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.otherservice.response.valueguide.PredictionPriceResponse;
import com.bbb.core.model.otherservice.response.valueguide.VGBaseResponse;
import com.bbb.core.model.request.bicycle.*;
import com.bbb.core.model.request.sort.BicycleSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bicycle.*;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.market.home.DealRecommendMarketListingRes;
import com.bbb.core.model.response.market.home.DealRecommendResponse;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.model.response.market.valueguide.ValueGuideRecommendation;
import com.bbb.core.model.response.tradein.ConditionDetailResponse;
import com.bbb.core.repository.*;
import com.bbb.core.repository.bicycle.BicycleComponentRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicycle.BicycleSpecRepository;
import com.bbb.core.repository.bicycle.out.*;
import com.bbb.core.repository.bicyclemodel.BicycleModelRepository;
import com.bbb.core.repository.favourite.FavouriteRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.market.out.honme.DealRecommendMarketListingResRepository;
import com.bbb.core.repository.market.out.valueguide.ValueGuideRecommendationRepository;
import com.bbb.core.repository.reout.BicycleSizeRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.tracking.TrackingRepository;
import com.bbb.core.repository.tradein.TradeInPriceConfigRepository;
import com.bbb.core.repository.valueguide.PrivatePartyValueModifierRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class BicycleManager implements MessageResponses {
    //region Beans
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private BicycleBrandRepository bicycleBrandRepository;
    @Autowired
    private BicycleModelRepository bicycleModelRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private BicycleYearRepository bicycleYearRepository;
    @Autowired
    private BicycleImageRepository bicycleImageRepository;
    @Qualifier("objectMapper")
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private BicycleSizeRepository bicycleSizeRepository;
    @Autowired
    private BicycleFromBrandYearModelResponseRepository bicycleFromBrandYearModelResponseRepository;
    @Autowired
    private YearModelResponseRepository yearModelResponseRepository;
    @Autowired
    private BicycleSpecRepository bicycleSpecRepository;
    @Autowired
    private BicycleComponentResponseRepository bicycleComponentResponseRepository;
    @Autowired
    private InventoryBaseInfoRepository inventoryBaseInfoRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private TrackingRepository trackingRepository;
    @Autowired
    private DealRecommendMarketListingResRepository dealRecommendMarketListingResRepository;
    @Autowired
    private ValueGuideRecommendationRepository valueGuideRecommendationRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
//    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private FavouriteRepository favouriteRepository;
    @Autowired
    private BicycleManagerAsync bicycleManagerAsync;
    @Autowired
    private BicycleYearModelResponseRepository bicycleYearModelResponseRepository;
    @Autowired
    private ValueGuidePriceManager valueGuidePriceManager;
    @Autowired
    private BicycleBaseRepository bicycleBaseRepository;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private TradeInPriceConfigRepository tradeInPriceConfigRepository;
    @Autowired
    private RestValueGuideManager restValueGuideManager;
    @Autowired
    private PrivatePartyValueModifierRepository privatePartyValueModifierRepository;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private BicycleComponentManager bicycleComponentManager;
    @Autowired
    private BicycleComponentRepository bicycleComponentRepository;
    @Autowired
    private S3Manager s3Manager;
    //endregion

    @Autowired
    BicycleManager(EntityManagerFactory entityManagerFactory) {
        sessionFactory = ConfigDataSource.getSessionFactory(entityManagerFactory);
    }

    //region API
    public Object findAllBicycle(BicycleGetRequest request) throws ExceptionResponse {
        BicycleFilterRequest filter = request.getFilter();
        ListObjResponse<Bicycle> response = new ListObjResponse<>();
        if (request.getFilter().getBrandId() != null || !StringUtils.isBlank(filter.getBrandName())) {
            BicycleBrand brand = bicycleBrandRepository.findOneNotDelete(
                    request.getFilter().getBrandId(),
                    filter.getBrandName()
            );
            if (brand == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, BRAND_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
        if (request.getFilter().getTypeId() != null) {
            if (bicycleTypeRepository.findOne(request.getFilter().getTypeId()) == null){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, TYPE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
        if (request.getFilter().getModelId() != null || !StringUtils.isBlank(filter.getModelName())) {
            BicycleModel model = bicycleModelRepository.findOneNotDelete(
                    request.getFilter().getModelId(),
                    filter.getModelName()
            );
            if (model == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, MODEL_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
//        if (request.getFromYearId() != null){
//            if (bicycleYearRepository.findOne(request.getFromYearId()) == null){
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_NOT_EXIST),
//                        HttpStatus.NOT_FOUND
//                );
//            }
//        }
//        if (request.getToYearId() != null){
//            if (bicycleYearRepository.findOne(request.getToYearId()) == null){
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_NOT_EXIST),
//                        HttpStatus.NOT_FOUND
//                );
//            }
//        }
        response.setTotalItem(bicycleRepository.countAll(
                filter.getBrandId(), filter.getBrandName(),
                filter.getModelId(), filter.getModelName(),
                filter.getTypeId(),
                filter.getFromYearId(), filter.getToYearId(),
                filter.getMissImage(), filter.getMissRetailPrice()));
        if (response.getTotalItem() > 0) {
            List<Bicycle> listData = bicycleRepository.findAllBy(
                    filter.getBrandId(), filter.getBrandName(),
                    filter.getModelId(), filter.getModelName(),
                    filter.getTypeId(),
                    filter.getFromYearId(), filter.getToYearId(),
                    filter.getMissImage(), filter.getMissRetailPrice(),
                    request.getSortField(), request.getSortType(),
                    request.getPageable().getOffset(), request.getPageable().getPageSize());
            if (listData.size() == 0) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, NO_PRODUCTS_MATCH),
                        HttpStatus.NOT_FOUND
                );
            }
            response.setData(listData);
        } else {
            response.setData(new ArrayList<>());
        }
        response.setPageSize(request.getPageable().getPageSize());
        response.setPage(request.getPageable().getPageNumber());
        response.updateTotalPage();
        return response;
    }

    public Object deleteBicycle(long id) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        Bicycle bicycle = bicycleRepository.findOneNotDelete(id);
        if (bicycle == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        int countImage = bicycleImageRepository.countImage(id);
        if (countImage>0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, (countImage==1?THERE_IS:THERE_ARE+ countImage)+ BICYCLE_IMAGE_USING_OBJECT),
                    HttpStatus.NOT_FOUND
            );
        }
        int countComp = bicycleComponentRepository.getCountByBicycleId(id);
        if (countComp>0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, (countComp==1?THERE_IS:THERE_ARE+ countComp)+ BICYCLE_COMPONENT_USING_OBJECT),
                    HttpStatus.NOT_FOUND
            );
        }
        bicycle.setDelete(true);
        bicycleRepository.save(bicycle);
        return new MessageResponse(SUCCESS);
    }

    public Object createBicycle(BicycleCreateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleBrand brand = bicycleBrandRepository.findOne(request.getBrandId());
        if (brand == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BRAND_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        BicycleModel model = bicycleModelRepository.findOne(request.getModelId());
        if (model == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MODEL_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (model.getBrandId() != brand.getId()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, MODEL_NOT_BELONG_BRAND),
                    HttpStatus.BAD_REQUEST
            );
        }
        BicycleYear bicycleYear = bicycleYearRepository.findOne(request.getYearId());
        if (bicycleYear == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YEAR_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        BicycleType bicycleType = bicycleTypeRepository.findOne(request.getTypeId());
        if (bicycleType == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TYPE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        BicycleSize bicycleSize = bicycleSizeRepository.findOne(request.getSizeId());
        if (bicycleSize == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_SIZE_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getRetailPrice() < 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, RETAIL_PRICE_MUST_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }
        Bicycle bicycle = new Bicycle();
        bicycle.setBrandId(request.getBrandId());
        bicycle.setModelId(request.getModelId());
        bicycle.setYearId(request.getYearId());
        bicycle.setTypeId(request.getTypeId());
        bicycle.setRetailPrice(request.getRetailPrice());
        bicycle.setDescription(request.getDescription());
        bicycle.setActive(false);
        bicycle.setName(request.getName());
        bicycle.setSizeId(request.getSizeId());
//        Bicycle bicycleSave = bicycleRepository.save(bicycle);
//        bicycle = bicycleSave;
//        if (request.getImages().size() > 0) {
//            List<BicycleImage> bicycleImages =
//                    request.getImages()
//                            .stream()
//                            .map(uri -> {
//                                BicycleImage bicycleImage = new BicycleImage();
//                                bicycleImage.setBicycleId(bicycleSave.getId());
//                                bicycleImage.setUri(uri);
//                                return bicycleImage;
//                            })
//                            .collect(Collectors.toList());
//            bicycleImages = bicycleImageRepository.save(bicycleImages);
//            bicycle.setImageDefault(bicycleImages.get(0).getUri());
//            bicycle.setImageId(bicycleImages.get(0).getId());
//            bicycle = bicycleRepository.save(bicycle);
//        }
        return bicycleRepository.save(bicycle);
    }

    public Object updateBicycle(long bicycleId, BicycleUpdateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        Bicycle bicycle = bicycleRepository.findOne(bicycleId);
        if (bicycle == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        long brandId = bicycle.getBrandId();
        if (request.getBrandId() != null) {
            BicycleBrand brand = bicycleBrandRepository.findOne(request.getBrandId());
            if (brand == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BRAND_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            brandId = request.getBrandId();
            bicycle.setBrandId(request.getBrandId());
        }

        if (request.getModelId() != null) {
            BicycleModel model = bicycleModelRepository.findOne(request.getModelId());
            if (model == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, MODEL_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            if (model.getBrandId() != brandId) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_DELETE, MODEL_NOT_BELONG_BRAND),
                        HttpStatus.BAD_REQUEST
                );

            }
            bicycle.setModelId(request.getModelId());
        }

        if (request.getTypeId() != null) {
            BicycleType bicycleType = bicycleTypeRepository.findOne(request.getTypeId());
            if (bicycleType == null) {

                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TYPE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            bicycle.setTypeId(request.getTypeId());
        }

        if (request.getYearId() != null) {
            BicycleYear bicycleYear = bicycleYearRepository.findOne(request.getYearId());
            if (bicycleYear == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TYPE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            bicycle.setTypeId(request.getYearId());
        }
        if (request.getRetailPrice() != null) {
            if (request.getRetailPrice() < 0) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_DELETE, RETAIL_PRICE_MUST_POSITIVE),
                        HttpStatus.BAD_REQUEST
                );
            }
            bicycle.setRetailPrice(request.getRetailPrice());
        }
        if (request.getDescription() != null) {
            bicycle.setDescription(request.getDescription());
        }
        if (request.getName() != null) {
            bicycle.setName(request.getName());
        }
        if (request.getDelete() != null) {
            bicycle.setDelete(request.getDelete());
        }
        if (request.getActive() != null) {
            bicycle.setActive(request.getActive());
        }
        if (request.getImage() != null) {
            BicycleImage bicycleImage = new BicycleImage();
            bicycleImage.setUri(request.getImage());
            bicycleImage.setBicycleId(bicycle.getId());
            bicycleImage = bicycleImageRepository.save(bicycleImage);
            bicycle.setImageDefault(bicycleImage.getUri());
            bicycle.setImageId(bicycleImage.getId());
        }
        bicycle = bicycleRepository.save(bicycle);
        return bicycle;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getBicycleDetail(long bicycleId, boolean isView) throws ExceptionResponse {
        Bicycle bicycle = bicycleRepository.findOne(bicycleId);
        if (bicycle == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        try {
            String contentJson = objectMapper.writeValueAsString(bicycle);
            BicycleDetailResponse response = objectMapper.readValue(contentJson, BicycleDetailResponse.class);

            List<Observable<Object>> obs = new ArrayList<>();
            obs.add(CommonUtils.getObObj(() -> bicycleImageRepository.finAll(bicycle.getId()).stream().map(BicycleImage::getUri).collect(Collectors.toList())));
            obs.add(CommonUtils.getObObj(() -> inventoryBaseInfoRepository.findOneFromBicycleId(bicycleId)));
            if (bicycle.getModelId() != null) {
//                obs.add(CommonUtils.getObObj(() -> yearModelResponseRepository.findAllYearModel(bicycle.getModelId(), bicycle.getBrandId())));
                obs.add(CommonUtils.getObObj(() -> bicycleYearModelResponseRepository.findFromBrandModel(bicycle.getBrandId(), bicycle.getModelId(), bicycle.getYearId())));
            }
            if (bicycle.getSizeId() != null) {
                obs.add(CommonUtils.getObObj(() -> bicycleSizeRepository.findOne(bicycle.getSizeId())));
            }
            obs.add(CommonUtils.getObObj(() -> bicycleComponentManager.findAllByBicycleId(bicycleId)));
//            obs.add(CommonUtils.getObObj(() -> bicycleSpecRepository.findAllByBicycleId(bicycleId)
//                    .stream().map(CommonUtils::convertToSpecResponse).collect(Collectors.toList())));
            Object[] os = Observable.zip(obs, o -> o)
                    .blockingFirst();
            for (Object o : os) {
                if (o instanceof List) {
                    List oList = (List) o;
                    if (oList.size() == 0) {
                        continue;
                    }
                    if (oList.get(0) instanceof String) {
                        List<String> images = oList;
                        if (bicycle.getImageDefault() != null) {
                            if (!images.contains(bicycle.getImageDefault())) {
                                images.add(bicycle.getImageDefault());
                            }
                        }
                        response.setImages(images);
                        continue;
                    }
//                    if (oList.get(0) instanceof YearModelResponse) {
//                        response.setYearModels(
//                                ((List<YearModelResponse>) oList).stream()
//                                        .map(y -> {
//                                            ContentCommonId commonId = new ContentCommonId();
//                                            commonId.setId(y.getId().getYearId());
//                                            commonId.setName(y.getYearName());
//                                            return commonId;
//                                        }).collect(Collectors.toList())
//                        );
//                        continue;
//                    }
                    if (oList.get(0) instanceof BicycleYearModelResponse) {
                        response.setAdditionalYears((List<BicycleYearModelResponse>) oList);
                        continue;
                    }
                    if (oList.get(0) instanceof BicycleComponentResponse) {
                        response.setComponents((List<BicycleComponentResponse>) oList);
                        continue;
                    }
//                    if (oList.get(0) instanceof BicycleSpecResponse) {
//                        response.setSpec((List<BicycleSpecResponse>) oList);
//                    }
                    continue;
                }
                if (o instanceof BicycleBaseInfo) {
                    BicycleBaseInfo bicycleBaseInfo = (BicycleBaseInfo) o;
                    response.setModelName(
                            bicycleBaseInfo.getBicycleModelName()
                    );
                    response.setModelName(
                            bicycleBaseInfo.getBicycleModelName()
                    );
                    response.setYearName(
                            bicycleBaseInfo.getBicycleYearName()
                    );
                    response.setTypeName(
                            bicycleBaseInfo.getBicycleTypeName()
                    );
                    response.setBrandName(
                            bicycleBaseInfo.getBicycleBrandName()
                    );
                    continue;
                }
                if (o instanceof BicycleSize) {
                    response.setSizeName(((BicycleSize) o).getName());
                }
            }
            response.setSpec(new ArrayList<>());

            try {
                VGBaseResponse<PredictionPriceResponse> vgResponse = restValueGuideManager.getPredictionPrice(
                        response.getBrandName(),
                        response.getModelName(),
                        response.getYearName(),
                        bicycle.getRetailPrice()
                );
                List<Pair<ConditionInventory, Float>> conditions = valueGuidePriceManager.getValueGuidePrices(
                        bicycle.getBrandId(),
                        bicycle.getModelId(),
                        bicycle.getYearId(),
                        vgResponse
                );
                if (vgResponse != null) {
                    if (conditions != null && conditions.size() > 0) {
                        response.setConditions(new ArrayList<>());
                        List<PrivatePartyValueModifier> modifiers = privatePartyValueModifierRepository.findAll();
                        for (Pair<ConditionInventory, Float> conditionInventoryFloatPair : conditions) {
//                    ConditionTradeInBicycleResponse condition = new ConditionTradeInBicycleResponse(
//                            conditionInventoryFloatPair.getFirst(),
//                            TradeInUtil.getPercent(conditionInventoryFloatPair.getFirst()),
//                            TradeInUtil.getMessage(conditionInventoryFloatPair.getFirst()),
//                            conditionInventoryFloatPair.getSecond()
//                    );
                            PrivatePartyValueModifier modifier = CommonUtils.findObject(modifiers, mod -> mod.getName() == conditionInventoryFloatPair.getFirst());
                            float ppvAvg = vgResponse.getData().getPrediction();
                            float ppvMin = vgResponse.getData().getPrediction();
                            float ppvMax = vgResponse.getData().getPrediction();
                            if (modifier != null) {
                                ppvAvg = ppvAvg * (1 + modifier.getValueModifier() / 100);
                                ppvMin = ppvAvg * (1 - modifier.getAdjustmentRange() / 100);
                                ppvMax = ppvAvg * (1 + modifier.getAdjustmentRange() / 100);
                            }

                            ConditionValueGuideBicycleResponse condition = new ConditionValueGuideBicycleResponse(
                                    conditionInventoryFloatPair.getFirst(),
                                    TradeInUtil.getPercent(conditionInventoryFloatPair.getFirst()),
                                    TradeInUtil.getMessage(conditionInventoryFloatPair.getFirst()),
                                    conditionInventoryFloatPair.getSecond(),
                                    ppvAvg, ppvMin, ppvMax
                            );
                            response.getConditions().add(condition);
                        }
                    }
                }
            } catch (HttpStatusCodeException e) {
                e.printStackTrace();
            }

            if (isView) {
                String userId;
                if (CommonUtils.isLogined()) {
                    userId = CommonUtils.getUserLogin();
                } else {
                    userId = null;
                }
                bicycleManagerAsync.createTrackingBicycle(bicycleId, userId);
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new ExceptionResponse(
                new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                HttpStatus.NOT_FOUND
        );
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getBicyclesFromBrandYearModel(BicycleFromBrandYearModelRequest request, Pageable page) throws ExceptionResponse {

        ListObjResponse<BicycleFromBrandYearModelResponse> res = new ListObjResponse<>();
        List<BicycleFromBrandYearModelResponse> response = bicycleFromBrandYearModelResponseRepository
                .findAllBicycle(
                        request.getBrandId(),
                        request.getYearId(),
                        request.getModelId(),
                        page.getOffset(),
                        page.getPageSize());
        mergeYearModel(response);
        res.setData(response);
        res.setTotalItem(
                bicycleFromBrandYearModelResponseRepository.getTotalCountBrandYearModel(
                        request.getBrandId(),
                        request.getYearId(),
                        request.getModelId()
                ));
        res.setPage(page.getPageNumber());
        res.setPageSize(page.getPageSize());
        res.updateTotalPage();
        return res;
    }

    public Object getBicyclesFromContent(String content, Pageable page) {
        ListObjResponse<BicycleFromBrandYearModelResponse> res = new ListObjResponse<>();

        List<BicycleFromBrandYearModelResponse> response =
                bicycleFromBrandYearModelResponseRepository.findAllBicycle(
                        "%" + content + "%",
                        page.getOffset(),
                        page.getPageSize()
                );
        res.setData(response);
        mergeYearModel(response);
        res.setTotalItem(
                bicycleFromBrandYearModelResponseRepository.getTotalCountContent("%" + content + "%")
        );
        res.setPage(page.getPageNumber());
        res.setPageSize(page.getPageSize());
        res.updateTotalPage();
        return res;
    }

    public Object getAllCondition() {
        List<ConditionResponse> conditionResponses = new ArrayList<>();
        conditionResponses.add(new ConditionResponse(ConditionInventory.EXCELLENT, ConditionInventory.EXCELLENT.getValue()));
        conditionResponses.add(new ConditionResponse(ConditionInventory.VERY_GOOD, ConditionInventory.VERY_GOOD.getValue()));
        conditionResponses.add(new ConditionResponse(ConditionInventory.GOOD, ConditionInventory.GOOD.getValue()));
        conditionResponses.add(new ConditionResponse(ConditionInventory.FAIR, ConditionInventory.FAIR.getValue()));
        conditionResponses.add(new ConditionResponse(ConditionInventory.POOR, ConditionInventory.POOR.getValue()));
        conditionResponses.add(new ConditionResponse(ConditionInventory.USED, ConditionInventory.USED.getValue()));
        return conditionResponses;
    }

    public Object getBicycleSearchBaseComponent() throws ExceptionResponse {
        BicycleSearchBaseComponentResponse response = new BicycleSearchBaseComponentResponse();
        response.setBicycleBrands(
                contentCommonRepository.findAllBrandBicycle()
                        .stream().map(ContentCommon::getId)
                        .collect(Collectors.toList())
        );
        response.setBicycleYears(
                contentCommonRepository.findAllYearBicycle()
                        .stream().map(ContentCommon::getId)
                        .collect(Collectors.toList())
        );
        return response;
    }

    public Object getBicycleRecommends(long bicycleId) throws ExceptionResponse {
        Bicycle bicycle = bicycleRepository.findOneNotDelete(bicycleId);
        if (bicycle == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        if (marketPlace == null) {
            return new ArrayList<>();
        }
        List<ValueGuideRecommendation> valueGuideRecommendations = valueGuideRecommendationRepository.findAll(
                bicycleId,
                marketPlace.getId()
        );
        if (valueGuideRecommendations.size() == 0) {
            List<Long> tem = new ArrayList<>();
            tem.add(0L);
            List<DealRecommendMarketListingRes> recommendResponses = dealRecommendMarketListingResRepository.findRecommendationValueGuide(marketPlace.getId(),
                    true, tem, 0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND);
            checkFavourite(recommendResponses);
            return recommendResponses;
        }
        MarketToList marketToList = marketToListRepository.findAllIdActiveBBB(appConfig.getEbay().isSandbox());
        if (marketToList == null ) {
            return new ArrayList<>();
        }
        String content = "";
        String condition = "";
        String typeName = "";
        String sizeName = "";
        String yearName = "";
        String brakeTypeName = "";
        String frameMaterialName = "";
        String brandName = "";


        for (ValueGuideRecommendation valueGuideRecommendation : valueGuideRecommendations) {
            if (valueGuideRecommendation.getCondition() != null) {
                if (condition.equals("")) {
                    condition = valueGuideRecommendation.getCondition().getValue().replaceAll(" ", "_");
                    condition = "\"" + condition + "\"";
                } else {
                    if (!condition.contains(valueGuideRecommendation.getCondition().getValue())) {
                        condition = condition + " OR \"" + valueGuideRecommendation.getCondition().getValue().replaceAll(" ", "_") + "\"";
                    }
                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getTypeName())) {
                if (typeName.equals("")) {
                    typeName = valueGuideRecommendation.getTypeName().replaceAll(" ", "_");
                    typeName = "\"" + typeName + "\"";
                } else {
                    if (!typeName.contains(valueGuideRecommendation.getTypeName())) {
                        typeName = typeName + " OR \"" + valueGuideRecommendation.getTypeName().replaceAll(" ", "_") + "\"";
                    }
                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getSizeName())) {
                if (valueGuideRecommendation.getSizeName().contains("\"")) {
                    valueGuideRecommendation.setSizeName(valueGuideRecommendation.getSizeName().replaceAll("\"", "\"\"\"\""));
                }
                if (sizeName.equals("")) {
                    sizeName = valueGuideRecommendation.getSizeName().replaceAll(" ", "_");
                    sizeName = "\"" + sizeName + "\"";
                } else {
                    if (!sizeName.contains(valueGuideRecommendation.getSizeName())) {
                        sizeName = sizeName + " OR \"" + valueGuideRecommendation.getSizeName().replaceAll(" ", "_") + "\"";
                    }
                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getBrandName())) {
                if (brandName.equals("")) {
                    brandName = valueGuideRecommendation.getBrandName().replaceAll(" ", "_");
                    brandName = "\"" + brandName + "\"";
                } else {
                    if (!brandName.contains(valueGuideRecommendation.getBrandName())) {
                        brandName = brandName + " OR \"" + valueGuideRecommendation.getBrandName().replaceAll(" ", "_") + "\"";
                    }
                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getYearName())) {
                if (yearName.equals("")) {
                    yearName = valueGuideRecommendation.getYearName().replaceAll(" ", "_");
                    yearName = "\"" + yearName + "\"";
                } else {
                    if (!yearName.contains(valueGuideRecommendation.getYearName())) {
                        yearName = yearName + " OR \"" + valueGuideRecommendation.getYearName().replaceAll(" ", "_") + "\"";
                    }
                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getFrameMaterialName())) {
                if (frameMaterialName.equals("")) {
                    frameMaterialName = valueGuideRecommendation.getFrameMaterialName();
                    frameMaterialName = "\"" + frameMaterialName + "\"";
                } else {
                    if (!frameMaterialName.contains(valueGuideRecommendation.getFrameMaterialName())) {
                        frameMaterialName = frameMaterialName + " OR \"" + valueGuideRecommendation.getFrameMaterialName().replaceAll(" ", "_") + "\"";
                    }

                }
            }
            if (!StringUtils.isBlank(valueGuideRecommendation.getBrakeTypeName())) {
                if (brakeTypeName.equals("")) {
                    brakeTypeName = valueGuideRecommendation.getBrakeTypeName();
                    brakeTypeName = "\"" + brakeTypeName + "\"";
                } else {
                    if (!brakeTypeName.contains(valueGuideRecommendation.getBrakeTypeName())) {
                        brakeTypeName = brakeTypeName + " OR \"" + valueGuideRecommendation.getBrakeTypeName().replaceAll(" ", "_") + "\"";
                    }
                }
            }
        }

        if (sizeName != null && !StringUtils.isBlank(sizeName)) {
            content = unionQuery(getQueryRankRecommendation(10, sizeName, marketToList.getMarketPlaceId()), content);
        }
        content = unionQuery(getQueryRankRecommendation(9, typeName, marketToList.getMarketPlaceId()), content);
        content = unionQuery(getQueryRankRecommendation(8, condition, marketToList.getMarketPlaceId()), content);
        content = unionQuery(getQueryRankRecommendation(7, yearName, marketToList.getMarketPlaceId()), content);
        content = unionQuery(getQueryRankRecommendation(6, frameMaterialName, marketToList.getMarketPlaceId()), content);
        content = unionQuery(getQueryRankRecommendation(5, brakeTypeName, marketToList.getMarketPlaceId()), content);
        content = unionQuery(getQueryRankRecommendation(4, brandName, marketToList.getMarketPlaceId()), content);


        content = "SELECT " +
                "market_listing.id AS market_listing_id, " +
                "market_listing.inventory_id AS inventory_id, " +
                "inventory.title AS inventory_title, " +
                "inventory.bicycle_type_name AS bicycle_type_name, " +
                "inventory.current_listed_price AS current_listed_price, " +
                "inventory.discounted_price AS discounted_price, " +
                "inventory.image_default AS image_default, " +
                "inventory.bicycle_size_name AS bicycle_size_name " +
                "FROM " +
                "(SELECT new_market_listing.id , SUM(rank) as sum_rank FROM ( " + content + " ) AS new_market_listing " +
                "group by new_market_listing.id ) AS max_rank " +
                "join market_listing on max_rank.id = market_listing.id " +
                "join inventory on market_listing.inventory_id = inventory.id " +
                "LEFT JOIN (select inventory_comp_detail.value, inventory_comp_detail.inventory_id from inventory_comp_detail where inventory_comp_detail.inventory_comp_type_id = 11) as frame on inventory.id = frame.inventory_id " +
                "LEFT JOIN (select inventory_comp_detail.value, inventory_comp_detail.inventory_id from inventory_comp_detail where inventory_comp_detail.inventory_comp_type_id = 180) as brake on inventory.id = brake.inventory_id " +
                "order by max_rank.sum_rank DESC, market_listing.time_listed DESC " +
                "OFFSET 0 ROWS FETCH NEXT " + ValueCommons.MAX_SIZE_DEAL_RECOMMEND + " ROWS ONLY";

        System.out.println(content);
        Session session = sessionFactory.openSession();
        List<DealRecommendResponse> dealRecommendResponses = session.createSQLQuery(content).addEntity(DealRecommendMarketListingRes.class).list();
        if (dealRecommendResponses.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
            if (dealRecommendResponses.size() == 0) {
                List<Long> tem = new ArrayList<>();
                tem.add(0L);
                List<DealRecommendMarketListingRes> recommendMarketListingRes = dealRecommendMarketListingResRepository.findRecommendationValueGuide(marketPlace.getId(),
                        true, tem, 0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND);
                checkFavourite(recommendMarketListingRes);
                dealRecommendResponses.addAll(recommendMarketListingRes);
            } else {
                List<Long> marketListingIds =
                        dealRecommendResponses.stream().map(o -> ((DealRecommendMarketListingRes) o).getMarketListingId()).collect(Collectors.toList());
                List<DealRecommendMarketListingRes> recommendMarketListingRes = dealRecommendMarketListingResRepository.findRecommendationValueGuide(marketPlace.getId(),
                        false, marketListingIds, 0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND - dealRecommendResponses.size());
                checkFavourite(recommendMarketListingRes);
                dealRecommendResponses.addAll(recommendMarketListingRes);
            }

        }
        session.close();
        return dealRecommendResponses;
    }

    public Object getBicycleYearFromModel(long brandId, long modelId) {
        return bicycleYearModelResponseRepository.findFromBrandModel(brandId, modelId, null);
    }

    public Object getValueGuidePrices(long brandId, long modelId, long yearId) throws ExceptionResponse {
//        if (!bicycleBaseRepository.existsBrandModelYear(brandId, modelId, yearId)) {
//            throw new ExceptionResponse(
//                    ObjectError.ERROR_NOT_FOUND,
//                    MessageResponses.BRAND_MODEL_YEAR_NOT_EXIST,
//                    HttpStatus.NOT_FOUND
//            );
//        }
        Bicycle bicycle =
                bicycleRepository.findOneByBrandModelYear(brandId, modelId, yearId, true);
        if (bicycle == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

//        List<ValueGuideResponse> prices = valueGuidePriceManager.getValueGuidePrices(brandId, modelId, yearId);
        List<Pair<ConditionInventory, Float>> prices = valueGuidePriceManager.getValueGuidePrices(
                bicycle.getBrandId(), bicycle.getModelId(), bicycle.getYearId(),
                bicycle.getRetailPrice()
        );

        if (prices == null || prices.size() < 1) {
            List<ConditionDetailResponse> responses = new ArrayList<>();
//            responses.add(new ConditionDetailResponse(
//                    ConditionInventory.EXCELLENT,
//                    Constants.EXCELLENT_PERCENT,
//                    Constants.EXCELLENT_MSG,
//                    Constants.VALUE_GUIDE_PRICE_BASE_DEFAULT * Constants.EXCELLENT_PERCENT
//            ));
//            responses.add(new ConditionDetailResponse(
//                    ConditionInventory.VERY_GOOD,
//                    Constants.VERY_GOOD_PERCENT,
//                    Constants.VERY_GOOD_MSG,
//                    null
//            ));
//            responses.add(new ConditionDetailResponse(
//                    ConditionInventory.GOOD,
//                    Constants.GOOD_PERCENT,
//                    Constants.GOOD_MSG,
//                    null
//            ));
//            responses.add(new ConditionDetailResponse(
//                    ConditionInventory.FAIR,
//                    Constants.FAIR_PERCENT,
//                    Constants.FAIR_MSG,
//                    null
//            ));
            return responses;
        } else {

            return prices.stream()
                    .map(price -> {
                        ConditionValueGuideBicycleResponse response = convertConditionResponse(price);
                        return response;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    public Object removeImageIncorrect() {
        List<BicycleImage> bicycleImages = bicycleImageRepository.removeAllBicycleImageNoIntoBicycle();
        bicycleImageRepository.deleteAll(bicycleImages);
        return bicycleImages;
    }

    public void importBicycleFromOlbDb(long offset, long size) {
        try {
            Class.forName(Constants.DRIVER_SQL);
            Connection conn = DriverManager.getConnection(Constants.PATH_DB_OLD_BICYCLE, Constants.USERNAME_OLD, Constants.PASSWORD_OLD);
            String query = "SELECT * FROM [Bicycle].[Detail] ORDER BY Id OFFSET " + offset + " ROWS FETCH NEXT " + size + " ROWS ONLY";
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            DateTimeFormatter formatter = DateTimeFormat.forPattern(Constants.FORMAT_DATE_TIME);
            SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
            List<Bicycle> bicycles = new ArrayList<>();
            while (resultSet.next()) {
                long id = resultSet.getLong("Id");
                long brandId = resultSet.getLong("Brand_Id");
                long modelId = resultSet.getLong("Model_Id");
                long typeId = resultSet.getLong("Type_Id");
                long yearId = resultSet.getLong("Year_Id");
                float retailPrice = resultSet.getFloat("RetailPrice");
                String description = resultSet.getString("Description");
                String uriImage = resultSet.getString("ImageUri");
                long imageId = resultSet.getLong("Image_Id");
                Date lastUpdate = resultSet.getDate("LastUpdate");
                String date = format.format(lastUpdate);
                String saleForceId = resultSet.getString("SalesForceId");

                Bicycle bicycle = new Bicycle();
                bicycle.setId(id);
                bicycle.setBrandId(brandId);
                bicycle.setModelId(modelId);
                bicycle.setTypeId(typeId);
                bicycle.setYearId(yearId);
                bicycle.setRetailPrice(retailPrice);
                bicycle.setDescription(description);
                bicycle.setImageDefault(uriImage);
                bicycle.setImageId(imageId);
                bicycle.setLastUpdate(LocalDateTime.parse(date, formatter));
                bicycle.setIdSaleforce(saleForceId);
                bicycles.add(bicycle);
            }
            resultSet.close();
            stmt.close();
            conn.close();
            saveBicycles(bicycles);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region Shared
    private void mergeYearModel(List<BicycleFromBrandYearModelResponse> response) {
        List<Long> modelIds = response.stream().map(BicycleFromBrandYearModelResponse::getModelId).collect(Collectors.toList());
        List<Long> brandIds = response.stream().map(BicycleFromBrandYearModelResponse::getBrandId).collect(Collectors.toList());
        if (modelIds.size() == 0) {
            return;
        }
        CommonUtils.stream(modelIds);
        List<YearModelResponse> yearModelResponses = yearModelResponseRepository.findAllYearModel(modelIds, brandIds);
        for (BicycleFromBrandYearModelResponse bicycleFromBrandYearModelResponse : response) {
            bicycleFromBrandYearModelResponse.setYearModels(
                    yearModelResponses.stream().filter(bi -> !bi.getYearName().equals(bicycleFromBrandYearModelResponse.getYearName()) && bi.getId().getBrandId() == bicycleFromBrandYearModelResponse.getBrandId() &&
                            bi.getId().getModelId() == bicycleFromBrandYearModelResponse.getModelId())
                            .map(y -> {
                                ContentCommonId commonId = new ContentCommonId();
                                commonId.setId(y.getId().getYearId());
                                commonId.setName(y.getYearName());
                                return commonId;
                            })
                            .collect(Collectors.toList())
            );
        }
    }

    private List<Bicycle> saveBicycles(List<Bicycle> bicycles) {
        int totalThread = 4;
        int size = bicycles.size() / totalThread;
        boolean isAdd = false;
        if (size * totalThread < bicycles.size()) {
            isAdd = true;
            totalThread++;
        }
        ExecutorService executorService = Executors.newFixedThreadPool(totalThread);
        List<Observable<List<Bicycle>>> os = new ArrayList<>();
        for (int i = 0; i < totalThread; i++) {
            int start = i * size;
            int end = start + size;
            List<Bicycle> bicyclesSub = bicycles.subList(start, end);
            createObservableSaveBicycles(executorService, os, bicyclesSub);
        }
        return Observable.zip(os, o -> {
            List<Bicycle> bicycleList = new ArrayList<>();
            for (Object anO : o) {
                bicycleList.addAll((List<Bicycle>) anO);
            }
            return bicycleList;
        }).blockingFirst();
    }

    private void createObservableSaveBicycles(ExecutorService executorService, List<Observable<List<Bicycle>>> os, List<Bicycle> bicyclesSub) {
        os.add(
                Observable.create((ObservableOnSubscribe<List<Bicycle>>) su -> {
                    List<Bicycle> bicyclesSave = new ArrayList<>();
                    for (Bicycle bicycle : bicyclesSub) {
                        try {
                            bicyclesSave.add(bicycleRepository.save(bicycle));
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("modelId: " + bicycle.getModelId());
                            System.out.println("brandId: " + bicycle.getBrandId());
                        }
                    }
                    su.onNext(bicyclesSave);
                    su.onComplete();
                }).subscribeOn(Schedulers.from(executorService))
        );
    }

    private String getQueryRankRecommendation(int rank, String contains, long marketPlaceId) {
        return "(SELECT market_listing.id as id, rank = " + rank + " " +
                "FROM market_listing join inventory ON market_listing.inventory_id = inventory.id " +
                "WHERE market_listing.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.LISTED.getValue()) + " AND " +
                "market_listing.market_place_id = " + marketPlaceId + " AND " +
                "CONTAINS(inventory.search_guide_recommendation, '" + contains + "'))";
    }

    private String unionQuery(String contentUnion, String total) {
        if (contentUnion.equals("")) {
            return total;
        }
        if (total.equals("")) {
            return contentUnion;
        }
        return total + " UNION ALL  " + contentUnion;
    }

    private void checkFavourite(List<DealRecommendMarketListingRes> responses) {
        if (responses != null && responses.size() > 0) {
            if (CommonUtils.isLogined()) {
                String userId = CommonUtils.getUserLogin();
                List<Integer> favourites = favouriteRepository.filterMarketListingFavourites(
                        userId,
                        responses.stream().map(res -> res.getMarketListingId().intValue()).collect(Collectors.toList())
                );
                if (favourites.size() > 0) {
                    favourites.forEach(id -> {
                        for (DealRecommendMarketListingRes res : responses) {
                            if (res.getMarketListingId().intValue() == id) {
                                res.setFavourite(true);
                                break;
                            }
                        }
                    });
                }
            }
        }
    }

    private ConditionValueGuideBicycleResponse convertConditionResponse(
            Pair<ConditionInventory, Float> price
    ) {
        ConditionValueGuideBicycleResponse response = null;
        switch (price.getFirst()) {
            case EXCELLENT:
                response = new ConditionValueGuideBicycleResponse(
                        price.getFirst(),
                        Constants.EXCELLENT_PERCENT, Constants.EXCELLENT_MSG,
                        price.getSecond()
                );
                break;
            case VERY_GOOD:
                response = new ConditionValueGuideBicycleResponse(
                        price.getFirst(),
                        Constants.VERY_GOOD_PERCENT,
                        Constants.VERY_GOOD_MSG,
                        price.getSecond()
                );
                break;
            case GOOD:
                response = new ConditionValueGuideBicycleResponse(
                        price.getFirst(),
                        Constants.GOOD_PERCENT,
                        Constants.GOOD_MSG,
                        price.getSecond()
                );
                break;
            case FAIR:
                response = new ConditionValueGuideBicycleResponse(
                        price.getFirst(),
                        Constants.FAIR_PERCENT,
                        Constants.FAIR_MSG,
                        price.getSecond()
                );
                break;
            default:
                return null;
        }
        return response;
    }

    private List<S3UploadResponse<Long>> uploadImageToS3(Bicycle bicycle, List<MultipartFile> files) throws ExceptionResponse {
        if (files == null) {
            return null;
        }
        if (files.size() < 1) {
            return new ArrayList();
        }

        List<S3UploadImage<Long>> requests = new ArrayList<>();
        for (MultipartFile file : files) {
            requests.add(new S3UploadImage<>(
                    Constants.BICYCLE_FOLDER + "_" + bicycle.getId(),
                    S3Value.FOLDER_BICYCLE_ORIGINAL,
                    file,
                    bicycle.getId()
            ));
        }
        return S3UploadUtils.uploads(s3Manager, requests);
    }
    //endregion
}
