package com.bbb.core.model.request.bicycletype;

import com.bbb.core.model.request.sort.BicycleTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class BicycleTypeGetRequest {
    private String name;
    private BicycleTypeSortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BicycleTypeSortField getSortField() {
        return sortField;
    }

    public void setSortField(BicycleTypeSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
