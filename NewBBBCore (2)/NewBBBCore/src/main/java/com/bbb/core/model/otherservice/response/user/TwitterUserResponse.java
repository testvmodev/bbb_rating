package com.bbb.core.model.otherservice.response.user;


import com.fasterxml.jackson.annotation.JsonProperty;

public class TwitterUserResponse {
    private String id;
    @JsonProperty("twitter_token")
    private String twitterToken;
    @JsonProperty("twitter_token_secret")
    private String twitterTokenSecret;
    @JsonProperty("twitter_avatar")
    private String twitterAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTwitterToken() {
        return twitterToken;
    }

    public void setTwitterToken(String twitterToken) {
        this.twitterToken = twitterToken;
    }

    public String getTwitterTokenSecret() {
        return twitterTokenSecret;
    }

    public void setTwitterTokenSecret(String twitterTokenSecret) {
        this.twitterTokenSecret = twitterTokenSecret;
    }

    public String getTwitterAvatar() {
        return twitterAvatar;
    }

    public void setTwitterAvatar(String twitterAvatar) {
        this.twitterAvatar = twitterAvatar;
    }
}
