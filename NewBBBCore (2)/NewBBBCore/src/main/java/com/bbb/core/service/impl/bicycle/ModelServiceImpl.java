package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.ModelManager;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.model.ModelUpdateRequest;
import com.bbb.core.model.request.sort.BicycleModelSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {
    private final ModelManager manager;

    @Autowired
    public ModelServiceImpl(ModelManager manager) {
        this.manager = manager;
    }

    @Override
    public Object getModels(List<Long> brandIds, String nameSearch, BicycleModelSortField sortField, Sort sortType, Pageable pageable)
            throws ExceptionResponse{
        return manager.getModels(brandIds, nameSearch, sortField, sortType, pageable);
    }

    @Override
    public Object getModelDetail(long modelId) throws ExceptionResponse {
        return manager.getModelDetail(modelId);
    }

    @Override
    public Object createModel(ModelCreateRequest model) throws ExceptionResponse{
        return manager.createModel(model);
    }

    @Override
    public Object deleteModel(long id) throws ExceptionResponse {
        return manager.deleteModel(id);
    }

    @Override
    public Object updateBrand(long modelId, ModelUpdateRequest model) throws ExceptionResponse{
        return manager.updateModel(modelId, model);
    }

    @Override
    public Object getModelsFromBrandYear(Long brandId, Long yearId) throws ExceptionResponse {
        return manager.getModelsFromBrandYear(brandId, yearId);
    }

    @Override
    public Object getAllModelsFromBrand(long brandId) throws ExceptionResponse {
        return manager.getAllModelsFromBrand(brandId);
    }
}
