package com.bbb.core.model.otherservice.response.order;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderMoneyAmount {
    @JsonProperty("details")
    private OrderMoneyAmountDetail amountDetail;
    private String currency;
    private Float total;

    public OrderMoneyAmountDetail getAmountDetail() {
        return amountDetail;
    }

    public void setAmountDetail(OrderMoneyAmountDetail amountDetail) {
        this.amountDetail = amountDetail;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }
}
