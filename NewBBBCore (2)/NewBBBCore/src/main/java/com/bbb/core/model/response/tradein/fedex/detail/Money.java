package com.bbb.core.model.response.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Money {
    @JacksonXmlProperty(localName = "Currency")
    private String currency;
    @JacksonXmlProperty(localName = "Amount")
    private float amount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
