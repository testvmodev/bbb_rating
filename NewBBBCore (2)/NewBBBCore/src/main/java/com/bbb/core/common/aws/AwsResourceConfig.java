package com.bbb.core.common.aws;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.bbb.core.common.Constants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.amazonaws.regions.Regions.*;
import static com.amazonaws.regions.Regions.CA_CENTRAL_1;
import static com.amazonaws.regions.Regions.GovCloud;

@Configuration
@ConfigurationProperties(prefix = Constants.CONFIG_PREFIX + ".aws")
public class AwsResourceConfig {
    private String accessKeyId;
    private String secretAccessKey;
    private String defaultS3Bucket;

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getSecretAccessKey() {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    public String getDefaultS3Bucket() {
        return defaultS3Bucket;
    }

    public void setDefaultS3Bucket(String defaultS3Bucket) {
        this.defaultS3Bucket = defaultS3Bucket;
    }

    @Bean
    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(getAccessKeyId(), getSecretAccessKey());
    }

    @Bean
    public AmazonS3ClientBuilder amazonS3ClientBuilder() {
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxErrorRetry(3);
        configuration.setConnectionTimeout(120 * 1000);
        configuration.setSocketTimeout(120 * 1000);
        configuration.setProtocol(Protocol.HTTP);

        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials())).withClientConfiguration(configuration);
    }

    private AmazonS3 buildAmazonS3(Regions region) {
        return amazonS3ClientBuilder().withRegion(region).build();
    }

    @Bean
    public AmazonS3 amazonS3USEAST1() {
        return buildAmazonS3(US_EAST_1);
    }

    @Bean
    public AmazonS3 amazonS3USEAST2() {
        return buildAmazonS3(US_EAST_2);
    }

    @Bean
    public AmazonS3 amazonS3USWEST1() {
        return buildAmazonS3(US_WEST_1);
    }

    @Bean
    public AmazonS3 amazonS3USWEST2() {
        return buildAmazonS3(US_WEST_2);
    }

    @Bean
    public AmazonS3 amazonS3EUCenter1() {
        return buildAmazonS3(EU_CENTRAL_1);
    }

    @Bean
    public AmazonS3 amazonS3EUWest1() {
        return buildAmazonS3(EU_WEST_1);
    }

    @Bean
    public AmazonS3 amazonS3EUWest2() {
        return buildAmazonS3(EU_WEST_2);
    }

    @Bean
    public AmazonS3 amazonS3APSouth1() {
        return buildAmazonS3(AP_SOUTH_1);
    }

    @Bean
    public AmazonS3 amazonS3APSoutheast1() {
        return buildAmazonS3(AP_SOUTHEAST_1);
    }

    @Bean
    public AmazonS3 amazonS3APSoutheast2() {
        return buildAmazonS3(AP_SOUTHEAST_2);
    }

    @Bean
    public AmazonS3 amazonS3APNortheast1() {
        return buildAmazonS3(AP_NORTHEAST_1);
    }

    @Bean
    public AmazonS3 amazonS3ApNortheast2() {
        return buildAmazonS3(AP_NORTHEAST_2);
    }

    @Bean
    public AmazonS3 amazonS3SAEast1() {
        return buildAmazonS3(SA_EAST_1);
    }

    @Bean
    public AmazonS3 amazonS3CNNorth1() {
        return buildAmazonS3(CN_NORTH_1);
    }

    @Bean
    public AmazonS3 amazonS3CACentral1() {
        return buildAmazonS3(CA_CENTRAL_1);
    }

    @Bean
    public AmazonS3 amazonS3GovCloud() {
        return buildAmazonS3(GovCloud);
    }

}
