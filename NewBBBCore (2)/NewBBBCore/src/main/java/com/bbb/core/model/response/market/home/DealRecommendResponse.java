package com.bbb.core.model.response.market.home;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class DealRecommendResponse {
    protected long inventoryId;
    protected String inventoryTitle;
    protected String bicycleTypeName;
    protected Float currentListedPrice;
    protected Float discountedPrice;
    protected String imageDefault;
    @Transient
    protected Boolean isFavourite;
    protected String bicycleSizeName;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getBicycleSizeName() {
        return bicycleSizeName;
    }

    public void setBicycleSizeName(String bicycleSizeName) {
        this.bicycleSizeName = bicycleSizeName;
    }
}
