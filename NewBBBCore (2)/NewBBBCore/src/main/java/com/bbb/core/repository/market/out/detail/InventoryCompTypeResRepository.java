package com.bbb.core.repository.market.out.detail;

import com.bbb.core.model.database.embedded.InventoryCompDetailId;
import com.bbb.core.model.response.market.marketlisting.detail.InventoryCompTypeResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryCompTypeResRepository extends JpaRepository<InventoryCompTypeResponse, InventoryCompDetailId> {
    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.value AS value, " +
            "inventory_comp_type.name AS name " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId")
    List<InventoryCompTypeResponse> findAllCompInventory(
            @Param(value = "inventoryId") long inventoryId
    );


    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.value AS value, " +
            "inventory_comp_type.name AS name " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id IN :inventoryIds")
    List<InventoryCompTypeResponse> findAllCompInventories(
            @Param(value = "inventoryIds") List<Long> inventoryIds
    );
}
