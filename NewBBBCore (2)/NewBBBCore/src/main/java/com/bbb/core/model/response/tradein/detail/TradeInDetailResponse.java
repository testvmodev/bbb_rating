package com.bbb.core.model.response.tradein.detail;

import com.bbb.core.model.database.TradeInUpgradeComp;
import com.bbb.core.model.database.type.ConditionInventory;

import java.util.List;

public class TradeInDetailResponse {
    private long tradeInDetail;
    private double value;
    private ConditionInventory condition;
    private TraBicBaseInfoResponse baseInfo;
    private Boolean isClean;
    private Float eBikeMileage;
    private Float eBikeHours;
    private TradeInOwner owner;
    private TradeInProof proof;
    private List<TradeInUpgradeComp> allUpgradeComponent;


    public long getTradeInDetail() {
        return tradeInDetail;
    }

    public void setTradeInDetail(long tradeInDetail) {
        this.tradeInDetail = tradeInDetail;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public TraBicBaseInfoResponse getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(TraBicBaseInfoResponse baseInfo) {
        this.baseInfo = baseInfo;
    }

    public TradeInOwner getOwner() {
        return owner;
    }

    public void setOwner(TradeInOwner owner) {
        this.owner = owner;
    }

    public TradeInProof getProof() {
        return proof;
    }

    public void setProof(TradeInProof proof) {
        this.proof = proof;
    }

    public List<TradeInUpgradeComp> getAllUpgradeComponent() {
        return allUpgradeComponent;
    }

    public void setAllUpgradeComponent(List<TradeInUpgradeComp> allUpgradeComponent) {
        this.allUpgradeComponent = allUpgradeComponent;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }
}
