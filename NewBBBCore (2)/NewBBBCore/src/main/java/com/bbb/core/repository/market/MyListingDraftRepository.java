package com.bbb.core.repository.market;

import com.bbb.core.model.database.MyListingDraft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MyListingDraftRepository extends JpaRepository<MyListingDraft, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM my_listing_draft " +
            "WHERE id = :id " +
            "AND is_delete = 0"
    )
    MyListingDraft findOne(@Param("id") long id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM my_listing_draft " +
            "WHERE id IN :ids " +
            "AND is_delete = 0"
    )
    List<MyListingDraft> findByIds(
            @Param("ids") List<Long> ids
    );
}
