package com.bbb.core.manager.tradein;

import com.bbb.core.model.database.TradeInImageType;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.repository.tradein.TradeInImageTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TradeInImageTypeManager {
    @Autowired
    private TradeInImageTypeRepository tradeInImageTypeRepository;

    public List<TradeInImageType> getImageTypes() {
        return tradeInImageTypeRepository.findAll();
    }
}
