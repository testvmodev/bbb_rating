package com.bbb.core.manager.templaterest;

import com.bbb.core.common.Constants;
import com.bbb.core.common.config.log.LoggingClientHttpRequestInterceptor;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.response.ListObjResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

public abstract class RestTemplateManager {
    private final static Logger LOG = LoggerFactory.getLogger(RestTemplateManager.class);
    private String baseUrl;
    private RestTemplate restTemplate;

    public RestTemplateManager(String baseUrl) {
        this.baseUrl = baseUrl;
        restTemplate = new RestTemplate();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout((int) (Constants.MINUTE * 3));
        requestFactory.setReadTimeout((int) (Constants.MINUTE * 3));

//        ObjectMapper mapper = new ObjectMapper();
//        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
//
//        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
//        messageConverter.setPrettyPrint(false);
//        messageConverter.setObjectMapper(mapper);

//        restTemplate.getMessageConverters().add(messageConverter);


        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(requestFactory));


        if (restTemplate.getInterceptors() == null) {
            restTemplate.setInterceptors(new ArrayList<>());
        }
        restTemplate.getInterceptors().add(new LoggingClientHttpRequestInterceptor());
        restTemplate.getMessageConverters().add(0, CommonUtils.converterJack());
    }

    public <T> T getObject(String endPoint, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObject(String endPoint, ParameterizedTypeReference<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.GET, entity, clazz);
        return res.getBody();

    }

    public <T> T getObject(String endPoint, Map<String, String> mapParameter, ParameterizedTypeReference<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<T> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObject(String endPoint, Map<String, String> mapParameter, Class<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<T> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObject(String endPoint, String headerName, String headerContent, Map<String, String> mapParameter, Class<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(headerName, headerContent);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<T> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObjectPath(String endPoint, String headerName, String headerContent, Map<String, String> mapParameter, Class<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(headerName, headerContent);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.GET, entity, clazz, mapParameter);
        return res.getBody();
    }

    public <T> List<T> getObject(String endPoint, String headerName, String headerContent, Map<String, String> mapParameter, ParameterizedTypeReference<List<T>> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(headerName, headerContent);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<List<T>> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> ListObjResponse<T> getListObject(String endPoint, String headerName, String headerContent, Map<String, String> mapParameter, ParameterizedTypeReference<ListObjResponse<T>> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(headerName, headerContent);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<ListObjResponse<T>> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObjectNotList(String endPoint, String headerName, String headerContent, Map<String, String> mapParameter, ParameterizedTypeReference<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(headerName, headerContent);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<T> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }


    public <T> T getObject(String endPoint, String token, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(Constants.HEADER_NAME_AUTH, Constants.HEADER_PREFIX + token);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.GET, entity, clazz);
        return res.getBody();
    }

    public <T> T getObject(String endPoint, String token, Map<String, ?> mapParameter, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(Constants.HEADER_NAME_AUTH, Constants.HEADER_PREFIX + token);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl + endPoint);
        for (Map.Entry<String, ?> entry : mapParameter.entrySet()) {
            uriComponentsBuilder = uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
        }
        String uri = uriComponentsBuilder.toUriString().replace("%20", " ");
        ResponseEntity<T> res = restTemplate.exchange(uri, HttpMethod.GET, entity, clazz, mapParameter);
        return res.getBody();

    }

    public <T> T getObjectPath(String endPoint, String token, Map<String, String> mapParameter, Class<T> clazz) {
        endPoint = convertUrlPathVariable(endPoint,  mapParameter);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(Constants.HEADER_NAME_AUTH, Constants.HEADER_PREFIX + token);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.GET, entity, clazz, mapParameter);
        return res.getBody();
    }

    public <T> T postObject(String endPoint, Object object, Class<T> clazz, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(mediaType));
        headers.setContentType(mediaType);
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);

        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz, object);
        return res.getBody();
    }

    public <T> T postObject(String endPoint, Map<String, String> mapHeader, Object object, Class<T> clazz, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(mediaType));
        headers.setContentType(mediaType);
        mapHeader.forEach(headers::set);
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);

        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz, object);
        return res.getBody();
    }


    public <T> T postObject(String endPoint, Class<T> clazz, Map<String, ?> mapObject) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz, mapObject);
        return res.getBody();

    }

    public <T> T postObject(String endPoint, String token, Object object, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(Constants.HEADER_NAME_AUTH, Constants.HEADER_PREFIX + token);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);
        try {
            ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz);
            return res.getBody();
        } catch (HttpClientErrorException ex) {
            LOG.info("postObject bug: " + new String(ex.getResponseBodyAsByteArray()));
            throw ex;
        }

    }

    public <T> T putObject(String endPoint, Object object, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);
        try {
            ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.PUT, entity, clazz);
            return res.getBody();
        } catch (HttpClientErrorException ex) {
            LOG.info("postObject bug: " + new String(ex.getResponseBodyAsByteArray()));
            throw ex;
        }

    }

    public <T> T postObject(String endPoint, String headerName, String contentHeader, Object object, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(headerName, contentHeader);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);
        try {
            ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz);
            return res.getBody();
        } catch (HttpClientErrorException ex) {
            LOG.info("postObject bug: " + new String(ex.getResponseBodyAsByteArray()));
            throw ex;
        }

    }

    public <T> T postObject(String endPoint, String headerName, String contentHeader, Object object, ParameterizedTypeReference<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(headerName, contentHeader);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);
        try {
            ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz);
            return res.getBody();
        } catch (HttpClientErrorException ex) {
            LOG.info("postObject bug: " + new String(ex.getResponseBodyAsByteArray()));
            throw ex;
        }

    }

    public <T> T postObject(String endPoint, String token, Class<T> clazz, Map<String, String> mapObject) {
        endPoint = convertUrlPathVariable(endPoint,  mapObject);
        HttpHeaders headers = new HttpHeaders();
        headers.set(Constants.HEADER_NAME_AUTH, Constants.HEADER_PREFIX + token);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.POST, entity, clazz, mapObject);
        return res.getBody();

    }

    public <T> T patchObject(
            String endPoint, String headerName, String contentHeader, Object object, Class<T> clazz
    ) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(headerName, contentHeader);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Object> entity = new HttpEntity<>(object, headers);
        ResponseEntity<T> res = restTemplate.exchange(baseUrl + endPoint, HttpMethod.PATCH, entity, clazz);
        return res.getBody();
    }
    public static String convertUrlPathVariable(String url, Map<String, String> pars) {
        Set<String> keys = pars.keySet();
        List<String> keyArr = new ArrayList<>(keys);
        for (String key : keyArr) {
            String temKey = "{" + key + "}";
            if (url.contains(temKey)) {
                url = url.replace(temKey, pars.get(key));
                pars.remove(key);
            }
        }
        return url;
    }


}
