package com.bbb.core.model.otherservice.request.shipment;

import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentFromAddress;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentToAddress;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class ShipmentCreateRequest {
    private Long inventoryId;
    private Long tradeInId;
    private ShipmentType type;
    private InboundShippingType inboundShippingType;
    private OutboundShippingType outboundShippingType;
    private ShipmentFromAddress fromAddress;
    private ShipmentToAddress toAddress;
    private Integer length;
    private Integer width;
    private Integer height;
    private Float weight;
    private String manualCarrier;
    private String manualTrackingNumber;
    @JsonProperty("isScheduled")
    private Boolean isScheduled;
    private LocalDate scheduleDate;
//    private LocalDateTime scheduleReadyTime;
//    private LocalDateTime scheduleCloseTime;

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(Long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public ShipmentType getType() {
        return type;
    }

    public void setType(ShipmentType type) {
        this.type = type;
    }

    public InboundShippingType getInboundShippingType() {
        return inboundShippingType;
    }

    public void setInboundShippingType(InboundShippingType inboundShippingType) {
        this.inboundShippingType = inboundShippingType;
    }

    public OutboundShippingType getOutboundShippingType() {
        return outboundShippingType;
    }

    public void setOutboundShippingType(OutboundShippingType outboundShippingType) {
        this.outboundShippingType = outboundShippingType;
    }

    public ShipmentFromAddress getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(ShipmentFromAddress fromAddress) {
        this.fromAddress = fromAddress;
    }

    public ShipmentToAddress getToAddress() {
        return toAddress;
    }

    public void setToAddress(ShipmentToAddress toAddress) {
        this.toAddress = toAddress;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getManualCarrier() {
        return manualCarrier;
    }

    public void setManualCarrier(String manualCarrier) {
        this.manualCarrier = manualCarrier;
    }

    public String getManualTrackingNumber() {
        return manualTrackingNumber;
    }

    public void setManualTrackingNumber(String manualTrackingNumber) {
        this.manualTrackingNumber = manualTrackingNumber;
    }

    @JsonIgnore
    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    //    public LocalDateTime getScheduleReadyTime() {
//        return scheduleReadyTime;
//    }
//
//    public void setScheduleReadyTime(LocalDateTime scheduleReadyTime) {
//        this.scheduleReadyTime = scheduleReadyTime;
//    }
//
//    public LocalDateTime getScheduleCloseTime() {
//        return scheduleCloseTime;
//    }
//
//    public void setScheduleCloseTime(LocalDateTime scheduleCloseTime) {
//        this.scheduleCloseTime = scheduleCloseTime;
//    }
}
