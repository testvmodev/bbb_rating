package com.bbb.core.repository.shipping;

import com.bbb.core.model.database.ShippingFeeConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ShippingFeeConfigRepository extends JpaRepository<ShippingFeeConfig, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM shipping_fee_config " +
            "WHERE bicycle_type_id = ?1 " +
            "AND is_delete = 0"
    )
    ShippingFeeConfig findOne(long bicycleTypeId);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM shipping_fee_config " +
            "WHERE bicycle_type_id IN :#{@queryUtils.isEmptyList(#bicycleTypeIds) ? @queryUtils.fakeLongs() : #bicycleTypeIds} " +
            "AND is_delete = 0"
    )
    List<ShippingFeeConfig> findAll(
            @Param("bicycleTypeIds") List<Long> bicycleTypeIds
    );
}
