package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Header {
    @JacksonXmlProperty(localName = "ebl:RequesterCredentials")
    private RequesterCredentials requesterCredentials;

    public RequesterCredentials getRequesterCredentials() {
        return requesterCredentials;
    }

    public void setRequesterCredentials(RequesterCredentials requesterCredentials) {
        this.requesterCredentials = requesterCredentials;
    }
}
