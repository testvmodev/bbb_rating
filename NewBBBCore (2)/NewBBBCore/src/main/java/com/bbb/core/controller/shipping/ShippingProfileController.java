package com.bbb.core.controller.shipping;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.shipping.ShippingFeeConfigCreateRequest;
import com.bbb.core.model.request.shipping.ShippingFeeConfigUpdateRequest;
import com.bbb.core.service.shipping.ShippingProfileService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ShippingProfileController {
    @Autowired
    private ShippingProfileService service;

    @GetMapping(Constants.URL_SHIPPING_PROFILES)
    public ResponseEntity getShippingProfiles(
            Pageable pageable
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getShippingProfiles(pageable));
    }

    @PostMapping(Constants.URL_SHIPPING_PROFILES)
    public ResponseEntity addShippingProfile(
            @RequestBody @Valid ShippingFeeConfigCreateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.addShippingProfile(request));
    }

    @PutMapping(Constants.URL_SHIPPING_PROFILE)
    public ResponseEntity updateShippingProfile(
            @ApiParam(name = "Id of bicycle type")
            @PathVariable(Constants.ID) long bicycleTypeId,
            @RequestBody ShippingFeeConfigUpdateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateShippingProfile(bicycleTypeId, request));
    }

    @GetMapping(Constants.URL_SECRET_SHIPPING_PROFILES)
    public ResponseEntity getInventoryShippingProfiles(
            @RequestParam(value = "inventoryIds") List<Long> inventoryIds
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getInventoryShippingProfiles(inventoryIds));
    }
}
