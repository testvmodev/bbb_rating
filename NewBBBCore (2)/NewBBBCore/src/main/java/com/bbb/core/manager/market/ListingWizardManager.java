package com.bbb.core.manager.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.ConverterSqlUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.common.utils.Third;
import com.bbb.core.controller.market.MarketListingCommon;
import com.bbb.core.manager.admin.AdminManager;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.manager.market.async.ListingWizardManagerAsync;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.notification.SubscriptionManager;
import com.bbb.core.manager.templaterest.RestEbayManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.admin.UserAdminInfoResponse;
import com.bbb.core.model.request.ebay.*;
import com.bbb.core.model.request.ebay.additem.*;
import com.bbb.core.model.request.ebay.additems.AddItemRequestContainer;
import com.bbb.core.model.request.ebay.additems.AddItemsRequest;
import com.bbb.core.model.request.market.listingwizard.ListingWizardType;
import com.bbb.core.model.request.market.marketlisting.CreateMarketListing;
import com.bbb.core.model.request.market.marketlisting.CreateMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingInvToSalRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.common.OperatorMarketListingFilter;
import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.bbb.core.model.response.ebay.additem.AddItemResponse;
import com.bbb.core.model.response.ebay.additems.AddItemsResponse;
import com.bbb.core.model.response.ebay.enditem.EndItemResponse;
import com.bbb.core.model.response.ebay.enditem.EndItemResponseContainer;
import com.bbb.core.model.response.ebay.enditem.EndItemsResponse;
import com.bbb.core.model.response.inventory.InventoryCompDetailRes;
import com.bbb.core.model.response.market.listingwizard.ListingWizardItem;
import com.bbb.core.model.response.market.listingwizard.ListingWizardQueueResponse;
import com.bbb.core.model.response.market.listingwizard.ListingWizardResponse;
import com.bbb.core.model.response.market.marketlisting.MarketListingInventoryToListing;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeRepository;
import com.bbb.core.repository.inventory.InventoryImageRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.inventory.InventorySpecRepository;
import com.bbb.core.repository.inventory.out.InventoryCompDetailResRepository;
import com.bbb.core.repository.market.*;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.market.out.listingwizard.ListingWizardItemRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.shipping.ShippingFeeConfigRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class ListingWizardManager implements MessageResponses, EbayValue {
    //region Beans
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private MarketListingFilterRepository marketListingFilterRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    //    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private InventoryCompDetailResRepository inventoryCompDetailResRepository;
    @Autowired
    private InventorySpecRepository inventorySpecRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private RestEbayManager restEbayManager;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private EbayCategoryRepository ebayCategoryRepository;
    @Autowired
    private LogEbayMarketListingRepository logEbayMarketListingRepository;
    @Autowired
    private ListingWizardItemRepository listingWizardItemRepository;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private ListingWizardManagerAsync listingWizardManagerAsync;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private EbayListingDurationRepository ebayListingDurationRepository;
    @Autowired
    private SubscriptionManager subscriptionManager;
    @Autowired
    private AdminManager adminManager;
    @Autowired
    private ShippingFeeConfigRepository shippingFeeConfigRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private AppConfig appConfig;
    //endregion

    @Autowired
    ListingWizardManager(EntityManagerFactory entityManagerFactory) {
        sessionFactory = ConfigDataSource.getSessionFactory(entityManagerFactory);
    }


    private String queryCount() {
        return "SELECT COUNT(inventory.id) ";
    }

    private String getQueryOrderBy(Pageable page) {
        return " ORDER BY inventory.last_update DESC OFFSET " + page.getOffset() + " ROWS FETCH NEXT " + page.getPageSize() + " ROWS ONLY";
    }

    private String getQueryOrderByMarketListingLastUpdate(Pageable page) {
        return " ORDER BY market_listing.last_update DESC OFFSET " + page.getOffset() + " ROWS FETCH NEXT " + page.getPageSize() + " ROWS ONLY";
    }

    private String getQueryOrderByMaxLastUpdate(Pageable page) {
        return " ORDER BY last_update_market_listing_inv.last_update DESC OFFSET " + page.getOffset() + " ROWS FETCH NEXT " + page.getPageSize() + " ROWS ONLY";
    }
    private String getQueryOrderByMaxLastUpdateError(Pageable page) {
        return " ORDER BY error_inv.last_update DESC OFFSET " + page.getOffset() + " ROWS FETCH NEXT " + page.getPageSize() + " ROWS ONLY";
    }

    private String getQueryFieldsQueue() {
        return "SELECT " +
                "market_listing.id AS market_listing_id, " +
                "market_listing.inventory_id AS inventory_id, " +
                "inventory.name AS inventory_name, " +
                "inventory.title AS inventory_title, " +
                "inventory.current_listed_price AS current_listed_price, " +
                "inventory.initial_list_price AS initial_list_price, " +
                "inventory.image_default AS image_default, " +
                "inventory.status AS status_market_listing, " +
                "inventory.partner_id AS partner_id, " +
                "inventory.seller_id AS seller_id, " +
                "market_listing.owner_id AS owner_id ";
    }

    private String getQuerySelectMaxLastUpdateMarketListing(){
        return " (SELECT MAX(market_Listing.last_update) AS last_update, market_listing.inventory_id " +
                "FROM market_listing GROUP BY market_listing.inventory_id) AS last_update_market_listing_inv ";
    }

    private String getQueryFieldsActiveErrorListed() {
        return "SELECT " +
                "inventory.id AS inventory_id, " +
                "inventory.name AS inventory_name, " +
                "inventory.title AS inventory_title, " +
                "inventory.current_listed_price AS current_listed_price, " +
                "inventory.image_default AS image_default, " +
                "inventory.initial_list_price AS initial_price, " +
                "inventory.partner_id AS partner_id, " +
                "inventory.seller_id AS seller_id, " +
                "inventory.status AS status, " +
                "inventory.stage AS stage ";
    }

    private String getQueryLeftJoinInventoryAuction() {
        String now = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate());
        return " LEFT JOIN " +
                "( SELECT inventory_auction.inventory_id AS inventory_id FROM " +
                "inventory_auction JOIN auction ON inventory_auction.auction_id = auction.id " +
                "WHERE DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, " + ConverterSqlUtils.convertStringToStringQuery(now) + "), auction.end_date) > 0 AND " +
                "inventory_auction.is_delete = 0 " +
                "GROUP BY inventory_auction.inventory_id ) AS inventory_auction_null " +
                "ON inventory.id = inventory_auction_null.inventory_id ";
    }

    private String getQueryResult(boolean isError) {
        if (isError) {
            return " JOIN  " +
                    "(SELECT market_listing.inventory_id AS inventory_id " +
                    "FROM market_listing " +
                    "WHERE " +
                    "market_listing.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.ERROR.getValue()) + " " +
                    "GROUP BY market_listing.inventory_id) AS market_listing_error " +
                    "ON inventory.id = market_listing_error.inventory_id ";
        } else {
            String notIn = " (SELECT " +
                    "market_listing.inventory_id FROM market_listing WHERE market_listing.status = " +
                    ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.ERROR.getValue()) + " " +
                    "GROUP BY market_listing.inventory_id) ";
            return " JOIN " +
                    "(SELECT market_listing.inventory_id AS inventory_id " +
                    "FROM market_listing " +
                    "WHERE " +
//                    "market_listing.status <> " + ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.ERROR.getValue()) + " " +
                    "market_listing.inventory_id NOT IN " + notIn +
                    "GROUP BY market_listing.inventory_id) AS market_listing_error " +
                    "ON inventory.id = market_listing_error.inventory_id ";
        }
    }

    public String getQueryTableActive(Boolean isError) {

        return " FROM inventory JOIN inventory_type ON inventory.type_id = inventory_type.id " +
                //make sure it has location so it can list & ship
                "LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
                getQueryLeftJoinInventoryAuction() +
                (isError == null ? " " : getQueryResult(isError)) +
                " WHERE " +
//                " inventory.score_card_id IS NOT NULL AND " +
                " inventory.is_delete = 0 AND " +
                "inventory_auction_null.inventory_id IS NULL AND " +
                "inventory_type.name <> " + ConverterSqlUtils.convertStringToStringQuery(ValueCommons.PTP) + " AND " +
                " inventory.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusInventory.ACTIVE.getValue()) +
                " AND inventory.stage = " + ConverterSqlUtils.convertStringToStringQuery(StageInventory.READY_LISTED.getValue());
    }

    private String getQueryTableListed(Boolean isError) {
        return " FROM inventory JOIN inventory_type ON inventory.type_id = inventory_type.id " +
                getQueryLeftJoinInventoryAuction() +
                (isError == null ? " " : getQueryResult(isError)) +
                "LEFT JOIN " + getQuerySelectMaxLastUpdateMarketListing() + " ON inventory.id = last_update_market_listing_inv.inventory_id " +
                "WHERE " +
//                "inventory.score_card_id IS NOT NULL AND " +
                " inventory.is_delete = 0 AND " +
                "inventory_auction_null.inventory_id IS NULL AND " +
                "inventory_type.name <> " + ConverterSqlUtils.convertStringToStringQuery(ValueCommons.PTP) + " AND " +
                " inventory.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusInventory.ACTIVE.getValue()) +
                " AND inventory.stage = " + ConverterSqlUtils.convertStringToStringQuery(StageInventory.LISTED.getValue());
    }

    private String getQueryTableQueued(Boolean isError) {
        return "FROM " +
                "market_listing " +
                "JOIN inventory ON market_listing.inventory_id = inventory.id " +
                "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
                "LEFT JOIN trade_in ON inventory.score_card_id = trade_in.id " +
                getQueryLeftJoinInventoryAuction() +
                "WHERE " +
                "inventory.is_delete = 0 AND " +
                "inventory_type.name <> " + ConverterSqlUtils.convertStringToStringQuery(ValueCommons.PTP) + " AND " +
                "inventory_auction_null.inventory_id IS NULL AND " +
                "market_listing.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.QUEUE.getValue()) + " AND " +
                "market_listing.is_delete = 0 ";
    }

    private String getQueryTableError(List<Long> marketPlaceConfigEbayIds) {
        return " FROM inventory " +
                "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
                "JOIN " +
                "(SELECT DISTINCT market_listing.inventory_id AS inventory_id, market_listing.last_update AS last_update FROM market_listing WHERE market_listing.is_delete = 0 AND market_listing.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusMarketListing.ERROR.getValue()) + " ) AS error_inv " +
                "ON inventory.id = error_inv.inventory_id " +
                getQueryLeftJoinInventoryAuction() +
                "WHERE " +
//                "inventory.score_card_id IS NOT NULL AND " +
                "inventory.is_delete = 0 AND " +
                "inventory_auction_null.inventory_id IS NULL AND " +
                "inventory_type.name <> " + ConverterSqlUtils.convertStringToStringQuery(ValueCommons.PTP) + " ";
    }

    private String getQueryCompsRequest(List<MarketListingInvToSalRequest> request,
                                        List<MarketListingFilter> filters) throws ExceptionResponse {
        //name comp filter
        List<MarketListingInvToSalRequest> comps =
                request
                        .stream()
                        .filter(req -> req.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_COMP_DETAIL)
                        .collect(Collectors.toList());
        String sqlWhereComponent = "";
        if (comps.size() > 0) {
            for (MarketListingInvToSalRequest comp : comps) {
                if (comp.getValues() == null || comp.getValues().size() == 0) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, VALUE_MUST_BE_NOT_NULL),
                            HttpStatus.BAD_REQUEST);
                }
            }
            List<String> compNames = filters.stream().filter(filter -> filter.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_COMP_DETAIL)
                    .map(MarketListingFilter::getName).collect(Collectors.toList());
            List<InventoryCompType> invCompTypes = inventoryCompTypeRepository.findAllByName(
                    compNames
            );

            for (MarketListingInvToSalRequest req : comps) {
                MarketListingFilter markerFilter = CommonUtils.findObject(filters, filter -> filter.getId() == req.getId());
                InventoryCompType type = CommonUtils.findObject(invCompTypes, invCompType -> invCompType.getName().equals(markerFilter.getName()));
                String valueIn;
                if (req.getOperator() == OperatorMarketListingFilter.EQUAL ||
                        req.getOperator() == OperatorMarketListingFilter.RANGE_IN) {
                    valueIn = "inventory_comp_detail.value IN ";
                } else {
                    if (req.getOperator() == OperatorMarketListingFilter.NOT_EQUAL ||
                            req.getOperator() == OperatorMarketListingFilter.NOT_RANGE_IN) {
                        valueIn = "inventory_comp_detail.value NOT IN ";
                    } else {
                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TYPE_OPERATOR_INVALID),
                                HttpStatus.BAD_REQUEST);
                    }
                }
                String tem =
                        " inventory.id = (SELECT TOP 1 inventory_comp_detail.inventory_id FROM inventory_comp_detail WHERE " +
                                "inventory_comp_detail.inventory_id = inventory.id AND " +
                                "inventory_comp_detail.inventory_comp_type_id = " + type.getId() + " AND " +
                                valueIn + ConverterSqlUtils.convertListStringToStringQuery(req.getValues()) + ") ";
                if (sqlWhereComponent.equals("")) {
                    sqlWhereComponent = tem;
                } else {
                    sqlWhereComponent = sqlWhereComponent + " AND " + tem;
                }

            }
        }
        return sqlWhereComponent;
    }

    private String getQueryFieldRequest(List<MarketListingInvToSalRequest> request,
                                        List<MarketListingFilter> filters) throws ExceptionResponse {
        List<MarketListingInvToSalRequest> invs =
                request
                        .stream()
                        .filter(req -> req.getTypeTable() == MarketListingFilterTypeTable.INVENTORY)
                        .collect(Collectors.toList());
        String sqlField = "";
        if (invs.size() > 0) {
            for (MarketListingInvToSalRequest inv : invs) {
                MarketListingFilter markerFilter = CommonUtils.findObject(filters, filter -> filter.getId() == inv.getId());
                if (markerFilter.getType() == MarketListingFilterType.RANGE) {
                    if (inv.getStart() == null && inv.getEnd() == null) {
                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TYPE_OPERATOR_INVALID),
                                HttpStatus.BAD_REQUEST);
                    }
                    String tem;
                    if (inv.getOperator() == OperatorMarketListingFilter.RANGE) {
                        if (inv.getStart() != null) {
                            tem = " CONVERT(REAL, " + markerFilter.getFieldName() + ") >= " + inv.getStart() + " ";
                            if (inv.getEnd() != null) {
                                tem = tem + " AND " + " CONVERT(REAL, " + markerFilter.getFieldName() + ") <= " + inv.getEnd() + " ";
                            }
                        } else {
                            tem = " CONVERT(REAL, " + markerFilter.getFieldName() + ") <= " + inv.getEnd() + " ";
                        }
                    } else {
                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TYPE_OPERATOR_INVALID),
                                HttpStatus.BAD_REQUEST);
                    }
                    if (sqlField.equals("")) {
                        sqlField = tem;
                    } else {
                        sqlField = sqlField + " AND " + tem;
                    }

                } else {
                    if (inv.getValues() == null || inv.getValues().size() == 0) {
                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, VALUE_MUST_BE_NOT_NULL),
                                HttpStatus.BAD_REQUEST);
                    }
                    String tem;
                    if (inv.getOperator() == OperatorMarketListingFilter.RANGE_IN || inv.getOperator() == OperatorMarketListingFilter.EQUAL) {
                        tem = markerFilter.getFieldName() + " IN " + ConverterSqlUtils.convertListStringToStringQuery(inv.getValues()) + " ";
                    } else {
                        tem = markerFilter.getFieldName() + " NOT IN " + ConverterSqlUtils.convertListStringToStringQuery(inv.getValues()) + " ";
                    }
                    if (sqlField.equals("")) {
                        sqlField = tem;
                    } else {
                        sqlField = sqlField + " AND " + tem;
                    }
                }
            }
        }
        //check inventory type
        List<MarketListingInvToSalRequest> invTypes =
                request
                        .stream()
                        .filter(req -> req.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_TYPE)
                        .collect(Collectors.toList());
        if (invTypes.size() > 0) {
            for (MarketListingInvToSalRequest invType : invTypes) {
                if (invType.getValues() != null && invType.getValues().size() > 0) {
                    String tem;
                    MarketListingFilter markerFilter = CommonUtils.findObject(filters, filter -> filter.getId() == invType.getId());
                    if (invType.getOperator() == OperatorMarketListingFilter.RANGE_IN || invType.getOperator() == OperatorMarketListingFilter.EQUAL) {
                        tem = markerFilter.getFieldName() + " IN " + ConverterSqlUtils.convertListStringToStringQuery(invType.getValues()) + " ";
                    } else {
                        tem = markerFilter.getFieldName() + " NOT IN " + ConverterSqlUtils.convertListStringToStringQuery(invType.getValues()) + " ";
                    }
                    if (sqlField.equals("")) {
                        sqlField = tem;
                    } else {
                        sqlField = sqlField + " AND " + tem;
                    }
                }
            }
        }
        return sqlField;
    }

    private ListObjResponse<ListingWizardResponse> getListingWizardError(
            List<MarketListingInvToSalRequest> request,
            List<MarketListingFilter> filters,
            List<MarketToList> marketToLists,
            Pageable page
    ) throws ExceptionResponse {
        ListObjResponse<ListingWizardResponse> response = new ListObjResponse<>();
        String content = getQueryTableError(marketToLists.stream().map(MarketToList::getMarketConfigId).collect(Collectors.toList()));
        String queryComp = getQueryCompsRequest(request, filters);
        String queryField = getQueryFieldRequest(request, filters);
        if (!queryComp.equals("")) {
            content = content + " AND " + queryComp;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }
        Session session = sessionFactory.openSession();
        List list = session.createSQLQuery(getQueryFieldsActiveErrorListed() + content + getQueryOrderByMaxLastUpdateError(page))
                .addEntity(ListingWizardResponse.class)
                .list();
        response.setData(
                list
        );
        response.setTotalItem(
                (Integer) session.createSQLQuery(queryCount() + content).list().get(0)
        );
        updateListingWizadResponse(marketToLists, response);
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        session.close();
        return response;
    }

    private ListObjResponse<ListingWizardQueueResponse> getListingWizardsQueue(
            List<MarketListingInvToSalRequest> request,
            List<MarketListingFilter> filters,
            List<MarketToList> marketToLists,
            Pageable page,
            Boolean isError
    ) throws ExceptionResponse {
        ListObjResponse<ListingWizardQueueResponse> response = new ListObjResponse<>();
        MarketToList marketToList = CommonUtils.findObject(marketToLists, o -> o.getType() == MarketType.EBAY);
        if (marketToList == null) {
            response.setData(new ArrayList<>());
            response.setTotalItem(0);
            response.setPage(page.getPageNumber());
            response.setPageSize(page.getPageSize());
            response.updateTotalPage();
            return response;
        }

        String content = getQueryTableQueued(isError);
        String queryComp = getQueryCompsRequest(request, filters);
        String queryField = getQueryFieldRequest(request, filters);
        if (!queryComp.equals("")) {
            content = content + " AND " + queryComp;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }
        Session session = sessionFactory.openSession();
        List list = session.createSQLQuery(getQueryFieldsQueue() + content + getQueryOrderByMarketListingLastUpdate(page))
                .addEntity(ListingWizardQueueResponse.class)
                .list();
        response.setData(
                list
        );
        response.setTotalItem(
                (Integer) session.createSQLQuery(queryCount() + content).list().get(0)
        );
        if (response.getData().size() > 0) {
            List<String> ids = response.getData().stream().map(ListingWizardQueueResponse::getOwnerId).filter(Objects::nonNull).collect(Collectors.toList());
            CommonUtils.stream(ids);
            if (ids.size() > 0) {
                List<UserAdminInfoResponse> users = adminManager.getUserAdminInfo(ids);
                if (users.size() > 0) {
                    for (ListingWizardQueueResponse datum : response.getData()) {
                        for (UserAdminInfoResponse user : users) {
                            if (datum.getOwnerId() != null && datum.getOwnerId().equals(user.getId())) {
                                datum.setOwnerName(user.getFirstName() + " " + user.getLastName());
                                break;
                            }
                        }

                    }
                }
            }

        }
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        session.close();
        return response;
    }

    private ListObjResponse<ListingWizardResponse> getListingWizardsListed(
            List<MarketListingInvToSalRequest> request,
            List<MarketListingFilter> filters,
            List<MarketToList> marketToLists,
            Pageable page,
            Boolean isError
    ) throws ExceptionResponse {
        String content = getQueryTableListed(isError);
        String queryComp = getQueryCompsRequest(request, filters);
        String queryField = getQueryFieldRequest(request, filters);
        if (!queryComp.equals("")) {
            content = content + " AND " + queryComp;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }

        Session session = sessionFactory.openSession();
        ListObjResponse<ListingWizardResponse> response = new ListObjResponse<>();
        List list = session.createSQLQuery(getQueryFieldsActiveErrorListed() + content + getQueryOrderByMaxLastUpdate(page))
                .addEntity(ListingWizardResponse.class)
                .list();
        response.setData(
                list
        );

        response.setTotalItem(
                (Integer) session.createSQLQuery(queryCount() + content).list().get(0)
        );
        updateListingWizadResponse(marketToLists, response);
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        session.close();
        return response;
    }

    private ListObjResponse<ListingWizardResponse> getListingWizardActives(List<MarketListingInvToSalRequest> request,
                                                                           List<MarketListingFilter> filters,
                                                                           List<MarketToList> marketToLists,
                                                                           Pageable page,
                                                                           Boolean isError)
            throws ExceptionResponse {
//        MarketToList marketToList = CommonUtils.findObject(marketToLists, mar -> mar.getType() == MarketType.EBAY);


        String content = getQueryTableActive(isError);
        String queryComp = getQueryCompsRequest(request, filters);
        String queryField = getQueryFieldRequest(request, filters);
        if (!queryComp.equals("")) {
            content = content + " AND " + queryComp;
        }
        if (!queryField.equals("")) {
            content = content + " AND " + queryField;
        }

        Session session = sessionFactory.openSession();
        ListObjResponse<ListingWizardResponse> response = new ListObjResponse<>();
        List list = session.createSQLQuery(getQueryFieldsActiveErrorListed() + content + getQueryOrderBy(page))
                .addEntity(ListingWizardResponse.class)
                .list();
        response.setData(
                list
        );
        response.setTotalItem(
                (Integer) session.createSQLQuery(queryCount() + content).list().get(0)
        );
        updateListingWizadResponse(marketToLists, response);

        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        session.close();
        return response;
    }

    private void updateListingWizadResponse(List<MarketToList> marketToLists, ListObjResponse<ListingWizardResponse> response) {
        List<Long> inventoryIds = response.getData().stream().map(ListingWizardResponse::getInventoryId).collect(Collectors.toList());
        if (inventoryIds.size() > 0) {
            List<ListingWizardItem> listingWizardItems = listingWizardItemRepository.findAllByInventoryId(inventoryIds);
            for (ListingWizardItem listingWizardItem : listingWizardItems) {
                if (listingWizardItem.getStatus() != StatusMarketListing.LISTED) {
                    listingWizardItem.setTimeListed(null);
                    listingWizardItem.setItemId(null);
                }
                if (listingWizardItem.getStatus() != StatusMarketListing.ERROR) {
                    listingWizardItem.setLastMsgError(null);
                }
            }
            MarketToList ebay = CommonUtils.findObject(marketToLists, m -> m.getType() == MarketType.EBAY);
            if (ebay != null) {
                addItem(response, listingWizardItems, ebay);
//                for (ListingWizardItem listingWizardItem : listingWizardItems) {
//                    if (listingWizardItem.getStatus() == StatusMarketListing.LISTED) {
//                        listingWizardItem.setBasePath(ValueCommons.LINK_EBAY_ITEM_VIEW.replace(ValueCommons.KEY_ITEM_ID_PATCH, listingWizardItem.getItemId() + ""));
//                    }
//                }
            }
            MarketToList bbb = CommonUtils.findObject(marketToLists, m -> m.getType() == MarketType.BBB);
            if (bbb != null) {
                addItem(response, listingWizardItems, bbb);
//                for (ListingWizardItem listingWizardItem : listingWizardItems) {
//                    if (listingWizardItem.getStatus() == StatusMarketListing.LISTED) {
//                        listingWizardItem.setBasePath(EndpointService.BASE_WEB_APP + EndpointService.MARKET_PLACE + EndpointService.BUY_NOW + "/" + listingWizardItem.getMarketListingId());
//                    }
//                }
            }
        }
    }

    private void addItem(ListObjResponse<ListingWizardResponse> response, List<ListingWizardItem> listingWizardItems, MarketToList marketToList) {
        for (ListingWizardResponse datum : response.getData()) {
            ListingWizardItem item = CommonUtils.findObject(listingWizardItems, listing -> listing.getInventoryId() == datum.getInventoryId()
                    && listing.getMarketPlaceConfigId() == marketToList.getMarketConfigId());
            if (item == null) {
                item = new ListingWizardItem();
                item.setInventoryId(datum.getInventoryId());
                item.setMarketPlaceConfigId(marketToList.getMarketConfigId());
                item.setMarketPlaceId(marketToList.getMarketPlaceId());
            }
            if (marketToList.getType() == MarketType.EBAY) {
                if (item.getStatus() == StatusMarketListing.LISTED) {
                    item.setBasePath(ValueCommons.LINK_EBAY_ITEM_VIEW.replace(ValueCommons.KEY_ITEM_ID_PATCH, item.getItemId() + ""));
                }
                datum.setEbay(item);
            } else {
                if (item.getStatus() == StatusMarketListing.LISTED) {
                    item.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + item.getMarketListingId());
                }
                datum.setBbb(item);
            }

        }
    }


    public Object getListingWizards(ListingWizardType typeListingWizard, List<MarketListingInvToSalRequest> request, Pageable page) throws ExceptionResponse {

        List<MarketToList> marketToLists = marketToListRepository.findAllIdActiveEbayBBB(appConfig.getEbay().isSandbox());

        if (marketToLists.size() == 0) {
            ListObjResponse<MarketListingInventoryToListing> response = new ListObjResponse<>();
            response.setData(new ArrayList<>());
            response.setTotalItem(0);
            response.setPageSize(page.getPageSize());
            response.updateTotalPage();
            return response;
        }

//        List<Long> marketConfigIds = marketToLists.stream().map(MarketToList::getMarketConfigId).collect(Collectors.toList());


        //check component
        List<Long> requestIds = request.stream().map(MarketListingInvToSalRequest::getId).collect(Collectors.toList());
        CommonUtils.stream(requestIds);
        if (requestIds.size() != request.size()) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ID_MUST_BE_UNIQUE),
                    HttpStatus.BAD_REQUEST);
        }
        //get name filter
        List<MarketListingFilter> filters;
        if (requestIds.size() > 0) {
            filters = marketListingFilterRepository.findAllById(requestIds);
            if (filters.size() != request.size()) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, ID_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }
            //valide typetable
            for (MarketListingInvToSalRequest marketListingInvToSalRequest : request) {
                for (MarketListingFilter filter : filters) {
                    if (marketListingInvToSalRequest.getId() == filter.getId()) {
                        if (marketListingInvToSalRequest.getTypeTable() != filter.getTypeTable()) {
                            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TYPE_TABLE_INVALID),
                                    HttpStatus.BAD_REQUEST);
                        }
                    }
                }
            }
        } else {
            filters = new ArrayList<>();
        }

        MarketListingInvToSalRequest reqError = CommonUtils.findObject(request, req -> req.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_ERROR);
        Boolean isError = null;
        if (reqError != null && reqError.getValues() != null && reqError.getValues().size() > 0) {
            if (OperatorMarketListingFilter.NOT_EQUAL.equals(reqError.getOperator()) ||
                    OperatorMarketListingFilter.NOT_RANGE_IN.equals(reqError.getOperator())) {
                isError = !reqError.getValues().get(0).equals(ValueCommons.SHOW_ITEM_IN_ERRORS);
            } else {
                isError = reqError.getValues().get(0).equals(ValueCommons.SHOW_ITEM_IN_ERRORS);
            }

        }
        switch (typeListingWizard) {
            case ACTIVE:
                return getListingWizardActives(request, filters, marketToLists, page, isError);
            case LISTED:
                return getListingWizardsListed(request, filters, marketToLists, page, isError);
            case QUEUED:
                return getListingWizardsQueue(request, filters, marketToLists, page, isError);
            case ERROR:
                return getListingWizardError(request, filters, marketToLists, page);
            default:
                return null;
        }
    }

    private void streamRequestCreateAndMarketListing(List<MarketListing> marketListings, List<CreateMarketListing> createMarketListings) {
        for (int i = 0; i < marketListings.size(); i++) {
            if ((marketListings.get(i).getStatus() == StatusMarketListing.LISTING || marketListings.get(i).getStatus() == StatusMarketListing.QUEUE )&& !marketListings.get(i).isDelete()) {
                for (int j = 0; j < createMarketListings.size(); j++) {
                    if (createMarketListings.get(j).getInventoryId() == marketListings.get(i).getInventoryId() &&
                            createMarketListings.get(j).getMarketPlaceConfigId() == marketListings.get(i).getMarketPlaceConfigId()) {
                        marketListings.remove(i);
                        i--;
                        createMarketListings.remove(j);
                        break;
                    }
                }
            }
        }
    }

    private void updateInfoMarketListingEbayFromCreateMarketListing(List<MarketPlace> marketPlaces, List<MarketPlaceConfig> marketPlaceConfigs,
                                                                    List<MarketListing> marketListings, List<CreateMarketListing> createMarketListings) {
        MarketPlace marketPlaceEbay = CommonUtils.findObject(marketPlaces, o -> o.getType() == MarketType.EBAY);
        if (marketPlaceEbay == null) {
            return;
        }
        MarketPlaceConfig marketPlaceConfig =
                CommonUtils.findObject(marketPlaceConfigs, o -> o.getMarketPlaceId() == marketPlaceEbay.getId());
        if (marketPlaceConfig == null) {
            return;
        }
        for (MarketListing marketListing : marketListings) {
            for (CreateMarketListing createMarketListing : createMarketListings) {
                if (marketListing.getMarketPlaceConfigId() == createMarketListing.getMarketPlaceConfigId() &&
                        createMarketListing.getMarketPlaceConfigId().equals(marketPlaceConfig.getId()) &&
                        marketListing.getInventoryId() == createMarketListing.getInventoryId()) {
                    marketListing.setEbayListingDurationId(createMarketListing.getEbayListingDurationId());
                }
            }
        }
    }


    public synchronized Object createMarketListing(CreateMarketListingRequest requestTotal) throws ExceptionResponse {
        List<CreateMarketListing> requests = requestTotal.getItems();
        if (requests.size() == 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, LIST_REQUEST_NOT_EMPTY),
                    HttpStatus.NOT_FOUND
            );
        }
        for (CreateMarketListing request : requests) {
            if (request.getBestOffer() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, IS_BEST_OFFER_MUST_NOT_EMPTY),
                        HttpStatus.NOT_FOUND
                );
            }
        }

        for (CreateMarketListing request : requests) {
            if (request.getBestOffer() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, IS_BEST_OFFER_MUST_NOT_EMPTY),
                        HttpStatus.NOT_FOUND
                );
            }
        }


        List<Long> marketPlaceConfigIds =
                requests.stream().map(CreateMarketListing::getMarketPlaceConfigId).collect(Collectors.toList());
        List<MarketPlaceConfig> marketPlaceConfigs = marketPlaceConfigRepository.findAllByIds(
                marketPlaceConfigIds,
                appConfig.getEbay().isSandbox());

        CommonUtils.stream(marketPlaceConfigIds);

        if (marketPlaceConfigs.size() != marketPlaceConfigIds.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MARKET_PLACE_CONFIG_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }


        List<Long> marketPlaceIds = marketPlaceConfigs.stream().map(MarketPlaceConfig::getMarketPlaceId).collect(Collectors.toList());
        CommonUtils.stream(marketPlaceIds);

        List<MarketPlace> marketPlaces = marketPlaceRepository.findAllByIds(marketPlaceIds);
        if (marketPlaces.size() != marketPlaceIds.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MARKET_PLACE_CONFIG_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }


        List<Long> inventoryIdsRoot = requests.stream().map(CreateMarketListing::getInventoryId).collect(Collectors.toList());
        CommonUtils.stream(inventoryIdsRoot);
        int rootCount = inventoryIdsRoot.size();
        for (int i = 0; i < requests.size() - 1; i++) {
            for (int j = i + 1; j < requests.size(); j++) {
                if (requests.get(i).getMarketPlaceConfigId() == requests.get(j).getMarketPlaceConfigId() &&
                        requests.get(i).getInventoryId() == requests.get(j).getInventoryId()) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_MUST_DUPLICATE),
                            HttpStatus.NOT_FOUND
                    );
                }
            }
        }
        if (inventoryRepository.checkExistInventoryPTP(inventoryIdsRoot)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_MUST_NOT_BE_PTP),
                    HttpStatus.NOT_FOUND
            );
        }
        //check count inventory
        int count = inventoryRepository.countInventoriesListingWizard(inventoryIdsRoot);
        if (count != rootCount) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        //end

        //check ivnentoryId
        List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIdsRoot);
        if (inventories.size() != inventoryIdsRoot.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST_OR_STATUS_OR_STAGE_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }

        //end
        if (requestTotal.getAdjustPrice() != null) {
            for (Inventory inventory : inventories) {
                if (requestTotal.getAdjustPrice() < 0) {
                    inventory.setDiscountedPrice(inventory.getDiscountedPrice() + requestTotal.getAdjustPrice());
                    inventory.setCurrentListedPrice(inventory.getDiscountedPrice() + (inventory.getFlatPriceChange() == null ? 0 : inventory.getFlatPriceChange()));
                } else {
                    inventory.setFlatPriceChange(requestTotal.getAdjustPrice());
                    inventory.setCurrentListedPrice(inventory.getDiscountedPrice() + inventory.getFlatPriceChange());
                }
            }
            //TODO update price ebay
            inventoryRepository.saveAll(inventories);
        }

        List<CreateMarketListing> reqErrorFromImages = new ArrayList<>();
        List<Pair<CreateMarketListing, String>> errorPrices = new ArrayList<>();
        //list listings for each inventory
        for (Inventory inventory : inventories) {
            if ((inventory.getStage() != StageInventory.READY_LISTED &&
                    inventory.getStage() != StageInventory.LISTED) ||
                    inventory.getStatus() != StatusInventory.ACTIVE) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, STAGE_AND_STATUS_INVALID),
                        HttpStatus.NOT_FOUND
                );
            }
            //check min offer and best offer price
            List<CreateMarketListing> requestNews =
                    CommonUtils.findObjects(requests, req -> req.getInventoryId() == inventory.getId());
            for (int i = 0; i < requestNews.size(); i++) {
                CreateMarketListing requestNew = requestNews.get(i);
                if (requestNew.getBestOfferAutoAcceptPrice() == null &&
                        requestNew.getMinimumOfferAutoAcceptPrice() == null) {
                    requestNew.setMinimumOfferAutoAcceptPrice(inventory.getCurrentListedPrice() * 0.6f);
                    requestNew.setBestOfferAutoAcceptPrice(inventory.getCurrentListedPrice() * 0.95f);
                } else {
                    if (requestNew.getBestOfferAutoAcceptPrice() == null || requestNew.getMinimumOfferAutoAcceptPrice() == null) {
//                        errorPrices.add(new Pair<>(requestNew, YOU_MISS_MIN_PRICE_OR_BEST_PRICE));
//                        requestNews.remove(i);
//                        requests.remove(requestNew);
//                        i--;
//                        continue;
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, YOU_MISS_MIN_PRICE_OR_BEST_PRICE),
                                HttpStatus.NOT_FOUND
                        );
                    } else {
                        if (requestNew.getMinimumOfferAutoAcceptPrice() < 0) {
//                            errorPrices.add(new Pair<>(requestNew, MIN_PRICE_MUST_BE_HIGHER_THAN_ZERO));
//                            requestNews.remove(i);
//                            requests.remove(requestNew);
//                            i--;
//                            continue;
                            throw new ExceptionResponse(
                                    new ObjectError(ObjectError.ERROR_PARAM, MIN_PRICE_MUST_BE_HIGHER_THAN_ZERO),
                                    HttpStatus.NOT_FOUND
                            );
                        }
                        if (requestNew.getMinimumOfferAutoAcceptPrice() > requestNew.getBestOfferAutoAcceptPrice()) {
//                            errorPrices.add(new Pair<>(requestNew, MIN_PRICE_MUST_BE_LESS_THAN_MAX_PRICE));
//                            requestNews.remove(i);
//                            requests.remove(requestNew);
//                            i--;
//                            continue;
                            throw new ExceptionResponse(
                                    new ObjectError(ObjectError.ERROR_PARAM, MIN_PRICE_MUST_BE_LESS_THAN_MAX_PRICE),
                                    HttpStatus.NOT_FOUND
                            );
                        }
                    }
                    if (requestNew.getBestOfferAutoAcceptPrice() >= inventory.getCurrentListedPrice()) {
//                        errorPrices.add(new Pair<>(requestNew, BEST_OFFER_PRICE_MUST_EQUAL_OR_LESS_THAN_SALE_PRICE));
//                        requestNews.remove(i);
//                        requests.remove(requestNew);
//                        i--;
//                        continue;
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, BEST_OFFER_PRICE_MUST_EQUAL_OR_LESS_THAN_SALE_PRICE),
                                HttpStatus.NOT_FOUND
                        );
                    }
                }

                //check image default
                if (StringUtils.isBlank(inventory.getImageDefault())) {
                    reqErrorFromImages.add(requestNew);
                    requestNews.remove(i);
                    requests.remove(requestNew);
                    i--;
                }
            }
        }

        //check exist inventory auction
        List<Integer> inventoryIdInAuctions =
                inventoryAuctionRepository.findAllInventoryAuctionAcctive(inventoryIdsRoot);
        if (inventoryIdInAuctions.size() > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_IS_BEING_AUCTION),
                    HttpStatus.NOT_FOUND
            );

        }
        //end


        List<Long> inventoryIds = requests.stream().map(CreateMarketListing::getInventoryId).collect(Collectors.toList());
        CommonUtils.stream(inventoryIds);

        //todo pass delete
        List<MarketListing> marketListings = marketListingRepository.findAllByInventoryIdPassDelete(
                inventoryIdsRoot
        );

        for (CreateMarketListing request : requests) {
            MarketListing marketListingListed = CommonUtils.findObject(marketListings, ma -> ma.getInventoryId() == request.getInventoryId() &&
                    ma.getMarketPlaceConfigId() == request.getMarketPlaceConfigId() &&
                    ma.getStatus() == StatusMarketListing.LISTED);
            if (marketListingListed != null) {
                MarketPlace marketPlace = CommonUtils.findObject(marketPlaces, o -> o.getId() == marketListingListed.getMarketPlaceId());
                String marketPlaceName = marketPlace != null ? marketPlace.getName() : "Ebay";
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, INVENTORY + " " + request.getInventoryId() + " " + LISTED_ON + " " + marketPlaceName),
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        streamRequestCreateAndMarketListing(marketListings, requests);
        updateInfoMarketListingEbayFromCreateMarketListing(marketPlaces, marketPlaceConfigs, marketListings, requests);

        if (requests.size() == 0) {
            return new ArrayList<>();
        }
        for (MarketListing marketListing : marketListings) {
            marketListing.setDelete(false);
        }
        String userId = CommonUtils.getUserLogin();
        List<MarketListing> marketListingError = new ArrayList<>();
        for (CreateMarketListing reqErrorFromImage : reqErrorFromImages) {
            MarketListing marketListing =
                    CommonUtils.findObject(marketListings, market -> market.getInventoryId() ==
                            reqErrorFromImage.getInventoryId() && market.getMarketPlaceConfigId() == reqErrorFromImage.getMarketPlaceConfigId());
            if (marketListing == null) {
                marketListing = new MarketListing();
                marketListing.setMarketPlaceConfigId(reqErrorFromImage.getMarketPlaceConfigId());
                MarketPlaceConfig marketPlaceConfig = CommonUtils.findObject(marketPlaceConfigs, config -> config.getId() == reqErrorFromImage.getMarketPlaceConfigId());
                marketListing.setMarketPlaceId(marketPlaceConfig.getMarketPlaceId());
                marketListing.setInventoryId(reqErrorFromImage.getInventoryId());
                marketListing.setStatus(StatusMarketListing.ERROR);
                marketListing.setBestOffer(reqErrorFromImage.getBestOffer());
                marketListing.setMinimumOfferAutoAcceptPrice(reqErrorFromImage.getMinimumOfferAutoAcceptPrice());
                marketListing.setBestOfferAutoAcceptPrice(reqErrorFromImage.getBestOfferAutoAcceptPrice());
                marketListing.setCountListingError(1);
            }
            marketListing.setStatus(StatusMarketListing.ERROR);
            marketListing.setLastMsgError(INVENTORY_HAVE_NOT_IMAGE);
            marketListing.setOwnerId(userId);
            marketListingError.add(marketListing);
        }
        for (Pair<CreateMarketListing, String> errorPrice : errorPrices) {
            MarketListing marketListing =
                    CommonUtils.findObject(marketListings, market -> market.getInventoryId() ==
                            errorPrice.getFirst().getInventoryId() && market.getMarketPlaceConfigId() == errorPrice.getFirst().getMarketPlaceConfigId());
            if (marketListing == null) {
                marketListing = new MarketListing();
                marketListing.setMarketPlaceConfigId(errorPrice.getFirst().getMarketPlaceConfigId());
                MarketPlaceConfig marketPlaceConfig = CommonUtils.findObject(marketPlaceConfigs, config -> config.getId() == errorPrice.getFirst().getMarketPlaceConfigId());
                marketListing.setMarketPlaceId(marketPlaceConfig.getMarketPlaceId());
                marketListing.setInventoryId(errorPrice.getFirst().getInventoryId());
                marketListing.setStatus(StatusMarketListing.ERROR);
                marketListing.setBestOffer(errorPrice.getFirst().getBestOffer());
                marketListing.setMinimumOfferAutoAcceptPrice(errorPrice.getFirst().getMinimumOfferAutoAcceptPrice());
                marketListing.setBestOfferAutoAcceptPrice(errorPrice.getFirst().getBestOfferAutoAcceptPrice());
                marketListing.setCountListingError(1);
            }
            marketListing.setStatus(StatusMarketListing.ERROR);
            marketListing.setLastMsgError(errorPrice.getSecond());
            marketListing.setOwnerId(userId);
            marketListingError.add(marketListing);
        }
        if (marketListingError.size() > 0) {
            marketListingError = marketListingRepository.saveAll(marketListingError);
        }


        List<Long> ebayCategoryIds = new ArrayList<>();
        List<Long> ebaySuCategoryIds = new ArrayList<>();
        List<Long> ebayListingDurationIds = new ArrayList<>();
        Map<Long, EbayListingDuration> ebayListingDurations = new HashMap<>();

        List<CreateMarketListing> ebays = new ArrayList<>();
        List<CreateMarketListing> bbbs = new ArrayList<>();
        MarketPlaceConfig configBBB = null;
        MarketPlaceConfig configEbay = null;
        for (CreateMarketListing request : requests) {
            MarketPlaceConfig marketPlaceConfig =
                    CommonUtils.findObject(marketPlaceConfigs, o -> o.getId().equals(request.getMarketPlaceConfigId()));
            MarketPlace marketPlace = CommonUtils.findObject(marketPlaces, o -> o.getId().equals(marketPlaceConfig.getMarketPlaceId()));
            switch (marketPlace.getType()) {
                case EBAY:
                    if (request.getEbayCategoryId() != null) {
                        ebayCategoryIds.add(request.getEbayCategoryId());
                        if (request.getEbaySubCategoryId() != null) {
                            ebaySuCategoryIds.add(request.getEbaySubCategoryId());
                        }
                    } else {
                        if (request.getEbaySubCategoryId() != null) {
                            throw new ExceptionResponse(
                                    new ObjectError(ObjectError.ERROR_PARAM, IF_HAS_SUB_CATEGORY_YOU_MUST_HAS_PRIMARY_CATEGORY),
                                    HttpStatus.BAD_REQUEST
                            );
                        }
                    }
                    if (request.getEbayListingDurationId() == null) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, DURATION_EBAY_NOT_EMPTY),
                                HttpStatus.BAD_REQUEST
                        );
                    } else {
                        if (!ebayListingDurationIds.contains(request.getEbayListingDurationId())) {
                            ebayListingDurationIds.add(request.getEbayListingDurationId());
                        }
                    }
                    configEbay = marketPlaceConfig;
                    ebays.add(request);
                    break;
                case BBB:
                    configBBB = marketPlaceConfig;
                    bbbs.add(request);
                    break;
                case POS:
                    break;
                default:
                    break;
            }

        }
        CommonUtils.stream(ebayCategoryIds);
        CommonUtils.stream(ebaySuCategoryIds);
        if (ebayCategoryIds.size() > 0) {
            if (ebayCategoryRepository.countCategoryPrimary(ebayCategoryIds) != ebayCategoryIds.size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, EBAY_CATEGORY_PRIMARY_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        if (ebaySuCategoryIds.size() > 0) {
            if (ebayCategoryRepository.countSubCategory(ebaySuCategoryIds) != ebaySuCategoryIds.size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, EBAY_CATEGORY_PRIMARY_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        if (ebayListingDurationIds.size() > 0) {
            List<EbayListingDuration> durations = ebayListingDurationRepository.findAllNotDelete(ebayListingDurationIds);
            if (durations.size() != ebayListingDurationIds.size()) {
//            if (ebayListingDurationRepository.countNotDelete(ebayListingDurationIds) != ebayListingDurationIds.size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, EBAY_LISTING_DURATION_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
            //set ebay duration from id
            ebayListingDurations = durations.stream()
                    .collect(Collectors.toMap(d -> d.getId(), d -> d));
            for (CreateMarketListing request : requests) {
                if (configEbay != null && request.getMarketPlaceConfigId() == configEbay.getId()) {
                    request.setEbayListingDuration(ebayListingDurations.get(request.getEbayListingDurationId()));
                }
            }
        }
        for (CreateMarketListing request : requests) {
            if (request.getBestOffer() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, IS_BEST_OFFER_MUST_NOT_EMPTY),
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        Pair<List<MarketListing>, Throwable> pair = new Pair<>();
        List<MarketListing> bbbListings = new ArrayList<>();


        if (ebays.size() > 0 && bbbs.size() > 0) {
            MarketPlaceConfig configBBBFinal = configBBB;
            MarketPlaceConfig configEbayFinal = configEbay;
            io.reactivex.Observable.zip(
                    io.reactivex.Observable.create((ObservableOnSubscribe<List<MarketListing>>) t -> {
                        t.onNext(listMarketPlaceBBBPOS(bbbs, configBBBFinal, marketListings, userId));
                        t.onComplete();
                    }).subscribeOn(Schedulers.newThread()),
                    io.reactivex.Observable.create((ObservableOnSubscribe<List<MarketListing>>) t -> {
                        t.onNext(listItemEbay(ebays, configEbayFinal, marketListings, userId));
                        t.onComplete();
                    }).subscribeOn(Schedulers.newThread()),
                    (o1, o2) -> {
                        List<MarketListing> results = new ArrayList<>();
                        results.addAll(o1);
                        results.addAll(o2);
                        bbbListings.addAll(o1);
                        return results;
                    }
            ).blockingSubscribe(pair::setFirst, pair::setSecond);

            triggerBBBPublishEvent(bbbListings);

            if (pair.getFirst() != null) {
                marketListingError.addAll(pair.getFirst());
                return marketListingError;
            } else {
                if (pair.getSecond() instanceof ExceptionResponse) {
                    throw (ExceptionResponse) pair.getSecond();
                }
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, pair.getSecond().getMessage()), HttpStatus.BAD_REQUEST);
            }
        } else {
            if (bbbs.size() > 0) {
                List<MarketListing> listings = listMarketPlaceBBBPOS(bbbs, configBBB, marketListings, userId);
                marketListingError.addAll(listings);
                bbbListings.addAll(listings);
            } else {
                if (ebays.size() > 0) {
                    marketListingError.addAll(listItemEbay(ebays, configEbay, marketListings, userId));
                }
            }

            triggerBBBPublishEvent(bbbListings);
            return marketListingError;
        }
    }

    private List<MarketListing> listMarketPlaceBBBPOS(List<CreateMarketListing> bbbs,
                                                      MarketPlaceConfig marketPlaceConfig,
                                                      List<MarketListing> marketListingsDb,
                                                      String userId) {
        List<Long> inventoriesId = bbbs.stream().map(CreateMarketListing::getInventoryId).collect(Collectors.toList());
        List<Inventory> inventories = inventoryRepository.findAllByIds(inventoriesId)
                .stream().peek(inv -> inv.setStage(StageInventory.LISTED))
                .collect(Collectors.toList());

        List<MarketListing> marketListings = new ArrayList<>();
        for (CreateMarketListing bbb : bbbs) {
            MarketListing marketListing = CommonUtils.findObject(marketListingsDb, ma -> ma.getMarketPlaceConfigId() == bbb.getMarketPlaceConfigId() && ma.getInventoryId() == bbb.getInventoryId());
            if (marketListing == null) {
                marketListing = new MarketListing();
            }
            marketListing.setInventoryId(bbb.getInventoryId());
            marketListing.setStatus(StatusMarketListing.LISTED);
            marketListing.setLastMsgError(null);
            marketListing.setMarketPlaceConfigId(marketPlaceConfig.getId());
            marketListing.setTimeListed(LocalDateTime.now());
            marketListing.setMarketPlaceConfigId(marketPlaceConfig.getId());
            marketListing.setMarketPlaceId(marketPlaceConfig.getMarketPlaceId());
            marketListing.setMinimumOfferAutoAcceptPrice(bbb.getMinimumOfferAutoAcceptPrice());
            marketListing.setBestOfferAutoAcceptPrice(bbb.getBestOfferAutoAcceptPrice());
            marketListing.setBestOffer(bbb.getBestOffer());
            marketListing.setOwnerId(userId);
            marketListings.add(marketListing);
        }
        inventoryRepository.saveAll(inventories);


        return marketListingRepository.saveAll(marketListings);
    }

    private List<MarketListing> listItemEbay(List<CreateMarketListing> ebays, MarketPlaceConfig marketPlaceConfig,
                                             List<MarketListing> marketListingsDb,
                                             String userId) throws ExceptionResponse {
        List<Inventory> inventorys = inventoryRepository.findAllByIds(ebays.stream().map(CreateMarketListing::getInventoryId).collect(Collectors.toList()));

        if (inventorys.size() != ebays.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MARKET_PLACE_CONFIG_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

        for (Inventory inventory : inventorys) {
            if (inventory.getCurrentListedPrice() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, CURRENT_LISTED_PRICE_INVALID),
                        HttpStatus.NOT_FOUND
                );
            }
        }


        AddItemRequest request;
        AddItemsRequest requests;
        List<MarketListing> marketListings = new ArrayList<>();
        Pair<List<Pair<Long, ShippingFeeConfig>>, List<Pair<Long, String>>> shippingConfigFeeInventoryPair = MarketListingUtils.getListPairInventoryIdShipConfigBicycleType(inventorys, contentCommonRepository, shippingFeeConfigRepository);
        List<Pair<Long, ShippingFeeConfig>> shippingConfigFeeInventory = shippingConfigFeeInventoryPair.getFirst();

        List<CreateMarketListing> ebaysValid = new ArrayList<>();
        List<CreateMarketListing> ebaysInValid = new ArrayList<>();
        List<Long> inventoryIdValid = shippingConfigFeeInventory.stream().map(Pair::getFirst).collect(Collectors.toList());
        for (CreateMarketListing ebay : ebays) {
            if (inventoryIdValid.contains(ebay.getInventoryId())) {
                ebaysValid.add(ebay);
            } else {
                ebaysInValid.add(ebay);
            }
        }
        List<MarketListing> marketListingsQueue;
        List<MarketListing> marketListingsFinal;
        if (ebaysValid.size() > 0) {
            if (ebaysValid.size() == 1) {
                request = new AddItemRequest();
                request.setErrorLanguage(ERROR_LANGUAGE);
                request.setWarningLevel(WARING_LEVEL);
                Inventory inventory = CommonUtils.findObject(inventorys, o -> o.getId() == ebaysValid.get(0).getInventoryId());
                Pair<Long, ShippingFeeConfig> pairShippingFee = CommonUtils.findObject(shippingConfigFeeInventory, o -> o.getFirst() == inventory.getId());
                request.setItem(createItemEbay(ebaysValid.get(0), inventorys.get(0),
                        ebaysValid.get(0).getBestOffer() != null && ebaysValid.get(0).getBestOffer() ? ebaysValid.get(0).getMinimumOfferAutoAcceptPrice() : null,
                        ebaysValid.get(0).getBestOffer() != null && ebaysValid.get(0).getBestOffer() ? ebaysValid.get(0).getBestOfferAutoAcceptPrice() : null,
                        pairShippingFee.getSecond().getEbayShippingProfileId()));
                marketListings.add(
                        createMarketListingToList(ebaysValid.get(0), inventorys.get(0), marketPlaceConfig, marketListingsDb, userId)
                );
                requests = null;
            } else {
                requests = new AddItemsRequest();
                requests.setErrorLanguage(ERROR_LANGUAGE);
                requests.setWarningLevel(WARING_LEVEL);

                requests.setAddItemRequestContainers(new ArrayList<>());
                int messageId = 0;
                for (CreateMarketListing ebay : ebaysValid) {
                    Inventory inventory = CommonUtils.findObject(inventorys, inv -> inv.getId() == ebay.getInventoryId());
                    if (inventory != null) {
                        AddItemRequestContainer addItemRequestContainer = new AddItemRequestContainer();
                        addItemRequestContainer.setMessageID(messageId + "");
                        Pair<Long, ShippingFeeConfig> pairShippingFee = CommonUtils.findObject(shippingConfigFeeInventory, o -> o.getFirst() == inventory.getId());
                        if (pairShippingFee == null) {
                            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, SHIPPING_CONFIG_FEE_INVENTORY + " " + inventory + " " + INVALID), HttpStatus.BAD_REQUEST);
                        }
                        addItemRequestContainer.setItem(createItemEbay(
                                ebay, inventory,
                                ebay.getBestOffer() != null && ebay.getBestOffer() ? ebay.getMinimumOfferAutoAcceptPrice() : null,
                                ebay.getBestOffer() != null && ebay.getBestOffer() ? ebay.getBestOfferAutoAcceptPrice() : null,
                                pairShippingFee.getSecond().getEbayShippingProfileId())
                        );
                        requests.getAddItemRequestContainers().add(addItemRequestContainer);
                        marketListings.add(
                                createMarketListingToList(ebay, inventory, marketPlaceConfig, marketListingsDb, userId)
                        );
                    }
                    messageId++;
                }
                request = null;
            }

            //start
            sortMarketListings(marketListingsDb, marketListings);
            marketListings = marketListingRepository.saveAll(marketListings);

            if (EbayValue.MAX_ITEMS_LIST_EBAY == 0) {
                marketListingsFinal = new ArrayList<>();
                marketListingsQueue = marketListings;
            } else {
                if (marketListings.size() > EbayValue.MAX_ITEMS_LIST_EBAY) {
                    marketListingsFinal = marketListings.subList(0, EbayValue.MAX_ITEMS_LIST_EBAY);
                    marketListingsQueue = marketListings.subList(EbayValue.MAX_ITEMS_LIST_EBAY, marketListings.size());
                } else {
                    marketListingsFinal = marketListings;
                    marketListingsQueue = new ArrayList<>();
                }
            }


            if (marketListingsFinal.size() > 0) {
                listEbayAsync(request, requests, marketListingsFinal, inventorys);
            }
        } else {
            marketListingsQueue = new ArrayList<>();
            marketListingsFinal = new ArrayList<>();
        }


        //update marketListing queue
        if (marketListingsQueue.size() > 0) {
            LocalDateTime now = LocalDateTime.now();
            marketListingsQueue = marketListingsQueue.stream().peek(o -> {
                o.setStatus(StatusMarketListing.QUEUE);
                o.setStartQueueTime(now);
            }).collect(Collectors.toList());
            marketListingsQueue = marketListingRepository.saveAll(marketListingsQueue);
        }
        List<Pair<Long, String>> shippingConfigError = shippingConfigFeeInventoryPair.getSecond();
        if (shippingConfigError.size() > 0) {
            List<MarketListing> marketListingError =
                    shippingConfigError.stream().map(o -> {
                        Inventory inv = CommonUtils.findObject(inventorys, iv -> iv.getId() == o.getFirst());
                        CreateMarketListing eb = CommonUtils.findObject(ebays, ob -> ob.getInventoryId() == o.getFirst());
                        MarketListing mk = createMarketListingToList(eb, inv, marketPlaceConfig, marketListingsDb, userId);
                        mk.setStatus(StatusMarketListing.ERROR);
                        mk.setLastMsgError(o.getSecond());
                        return mk;

                    }).collect(Collectors.toList());
            marketListingError = marketListingRepository.saveAll(marketListingError);
            marketListingError.addAll(marketListings);
            return marketListingError;
        }
        return marketListings;
    }


    private void listEbayAsync(AddItemRequest request,
                               AddItemsRequest requests, List<MarketListing> marketListingsFinal, List<Inventory> inventorys) {
        io.reactivex.Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            Third<String, String, AddItemResponse> resEbay;
            Third<String, String, AddItemsResponse> resEbays;
            if (request != null) {
                resEbay = restEbayManager.postTradeIn(request,
                        EbayValue.ADD_ITEM, AddItemResponse.class);
                resEbays = null;
            } else {
                resEbays = restEbayManager.postTradeIn(requests,
                        EbayValue.ADD_ITEMS, AddItemsResponse.class);
                resEbay = null;
            }

            //test
            LogEbayMarketListing logEbayMarketListing = new LogEbayMarketListing();
            logEbayMarketListing.setRequest(resEbay != null ? resEbay.getFirst() : resEbays.getFirst());
            logEbayMarketListing.setResponse(resEbay != null ? resEbay.getSecond() : resEbays.getSecond());

            if (marketListingsFinal.size() == 1) {
                logEbayMarketListing.setType(TypeLogEbayMarketListing.SINGLE);
            } else {
                logEbayMarketListing.setType(TypeLogEbayMarketListing.MULTI);
            }

            String messageError = null;
            if (resEbay != null) {
                if (resEbay.getThird() != null && resEbay.getThird().getErrors() != null && resEbay.getThird().getErrors().size() > 0) {
                    messageError = CommonUtils.getMessageError(resEbay.getThird().getErrors());
                }
            } else {
                if (resEbays.getThird() != null && resEbays.getThird().getErrors() != null && resEbays.getThird().getErrors().size() > 0) {
                    messageError = CommonUtils.getMessageError(resEbays.getThird().getErrors());
                }
            }

            if (marketListingsFinal.size() == 1) {
                logEbayMarketListing.setMarketListingId(marketListingsFinal.get(0).getId());
            }
            if (messageError != null) {
                for (MarketListing marketListing : marketListingsFinal) {
                    //todo update to LISTING_ERROR
                    marketListing.setStatus(StatusMarketListing.ERROR);
                    marketListing.setLastMsgError(messageError);
                    marketListing.setCountListingError(marketListing.getCountListingError() + 1);
                }
                if (marketListingsFinal.size() == 1) {
                    marketListingsFinal.get(0).setLastMsgError(messageError);
                } else {
                    MarketListingCommon.updateMarketListingEbay(marketListingsFinal, resEbays, inventorys);
                }
                marketListingRepository.saveAll(marketListingsFinal);
                logEbayMarketListingRepository.save(logEbayMarketListing);
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, messageError), HttpStatus.BAD_REQUEST);
                return;
            }

            logEbayMarketListingRepository.save(logEbayMarketListing);
            if (resEbay != null) {
                marketListingsFinal.get(0).setItemId(resEbay.getThird().getItemID());
                marketListingsFinal.get(0).setStatus(StatusMarketListing.LISTED);
                marketListingsFinal.get(0).setEndTime(resEbay.getThird().getEndTime());
                marketListingsFinal.get(0).setStartTime(resEbay.getThird().getStartTime());
                if (resEbay.getThird().getTimestamp() == null) {
                    marketListingsFinal.get(0).setTimeListed(LocalDateTime.now());
                } else {
                    marketListingsFinal.get(0).setTimeListed(resEbay.getThird().getTimestamp());
                }

                marketListingsFinal.get(0).setLastMsgError(null);
                inventorys.get(0).setStage(StageInventory.LISTED);
            } else {
                MarketListingCommon.updateMarketListingEbay(marketListingsFinal, resEbays, inventorys);
            }

            inventoryRepository.saveAll(inventorys);
            marketListingRepository.saveAll(marketListingsFinal);

            t.onNext(true);
            t.onComplete();
        }).subscribeOn(Schedulers.newThread())
                .subscribe(success -> {

                }, error -> {
                    for (MarketListing marketListing : marketListingsFinal) {
                        //todo update LISTING_ERROR
                        marketListing.setStatus(StatusMarketListing.ERROR);
                        marketListing.setCountListingError(marketListing.getCountListingError() + 1);
                        marketListing.setLastMsgError(error.getMessage());
                    }
                    marketListingRepository.saveAll(marketListingsFinal);
                });
    }

    private void sortMarketListings(List<MarketListing> marketListingDb, List<MarketListing> marketListingsList) {
        List<MarketListing> marketListings = new ArrayList<>();
        for (MarketListing marketListing : marketListingsList) {
            if (CommonUtils.findObject(marketListingDb, o -> o.getId() == marketListing.getId()) != null) {
                marketListings.add(0, marketListing);
            } else {
                marketListings.add(marketListing);
            }
        }
        marketListingsList.clear();
        marketListingsList.addAll(marketListings);
    }


    private MarketListing createMarketListingToList(CreateMarketListing createMarketListingRequest,
                                                    Inventory inventory, MarketPlaceConfig marketPlaceConfig,
                                                    List<MarketListing> marketListingsDb,
                                                    String userId) {
        MarketListing marketListing = CommonUtils.findObject(marketListingsDb, ma -> ma.getMarketPlaceConfigId() == createMarketListingRequest.getMarketPlaceConfigId() && ma.getInventoryId() == createMarketListingRequest.getInventoryId());
        if (marketListing == null) {
            marketListing = new MarketListing();
        }
        marketListing.setInventoryId(inventory.getId());
        marketListing.setStatus(StatusMarketListing.LISTING);
        marketListing.setMarketPlaceConfigId(marketPlaceConfig.getId());
        marketListing.setMarketPlaceId(marketPlaceConfig.getMarketPlaceId());
        marketListing.setEbayCategoryId(createMarketListingRequest.getEbayCategoryId());
        marketListing.setEbaySubCategoryId(createMarketListingRequest.getEbaySubCategoryId());
        marketListing.setBestOfferAutoAcceptPrice(createMarketListingRequest.getBestOfferAutoAcceptPrice());
        marketListing.setMinimumOfferAutoAcceptPrice(createMarketListingRequest.getMinimumOfferAutoAcceptPrice());
        marketListing.setListingTypeEbay(ListingTypeEbay.FIXED_PRICE_ITEM);
//        marketListing.setListingDuration(createMarketListingRequest.getDurationEbay());
        marketListing.setEbayListingDurationId(createMarketListingRequest.getEbayListingDurationId());
        marketListing.setBestOffer(createMarketListingRequest.getBestOffer());
        marketListing.setOwnerId(userId);
        return marketListing;
    }

    private Item createItemEbay(CreateMarketListing ebay, Inventory inventory, Float minBestOffer, Float maxBestOffer, long shippingProfileId) {
        Item item = new Item();
        item.setCountry(COUNTRY_US);
        item.setCurrency(CURRENCY_USD);
        item.setConditionId(CONDITION_ID_DEFAULT);
        item.setDispatchTimeMax(DISPATCH_TIME_MAX);
        if (minBestOffer != null || maxBestOffer != null) {
            item.setBestOfferDetails(new BestOfferDetails());
            item.getBestOfferDetails().setBestOfferEnabled(true);
            item.setListingDetails(new ListingDetails());
            if (minBestOffer != null) {
                item.getListingDetails().setMinimumBestOfferPrice(new BestOfferAutoAcceptPrice());
                item.getListingDetails().getMinimumBestOfferPrice().setValue(minBestOffer);
            }
            if (maxBestOffer != null) {
                item.getListingDetails().setBestOfferAutoAcceptPrice(new BestOfferAutoAcceptPrice());
                item.getListingDetails().getBestOfferAutoAcceptPrice().setValue(maxBestOffer);
            }

        }
//        item.setListingDuration(ebay.getDurationEbay().getValue());
        item.setListingDuration(ebay.getEbayListingDuration().getValue());
        item.setListingType(FIXED_PRICE_ITEM);
        item.setPaymentMethods(PAY_PAL);
        item.setPayPalEmailAddress(PAY_PAL_EMAIL_DEFAULT);
        item.setPostalCode(POST_CODE_DEFAULT + "");

        if (ebay.getEbayCategoryId() != null) {
            PrimaryCategory primaryCategory = new PrimaryCategory();
            primaryCategory.setCategoryID(ebay.getEbayCategoryId());
            item.setPrimaryCategory(primaryCategory);

//            if (ebay.getEbaySubCategoryId() != null) {
//                PrimaryCategory second = new PrimaryCategory();
//                second.setCategoryID(ebay.getEbaySubCategoryId());
//                item.setSecondaryCategory(second);
//            }
        }


        item.setTitle(inventory.getTitle() + " " + inventory.getName());

        String description = inventory.getDescription();
        description = description.replaceAll("&", "&amp;");
        description = description.replaceAll("[^\\x20-\\x7e]", "");
        item.setDescription(description);


        item.setStartPrice(inventory.getCurrentListedPrice());
        if (inventory.getDiscountedPrice() != inventory.getCurrentListedPrice()) {
            DiscountPriceInfo discountPriceInfo = new DiscountPriceInfo();
            discountPriceInfo.setOriginalRetailPrice(inventory.getCurrentListedPrice());
            item.setDiscountPriceInfo(discountPriceInfo);
        }
        item.setCategoryMappingAllowed(true);

        List<String> images = inventoryImageRepository.findAllByInventoryId(inventory.getId())
                .stream().map(InventoryImage::getImage).collect(Collectors.toList());

        PictureDetails pictureDetails = new PictureDetails();
        pictureDetails.setPictureURL(images);
        item.setPictureDetails(
                pictureDetails
        );

        item.setQuantity(1);


        //itemSpecifics
        ItemSpecifics itemSpecifics = new ItemSpecifics();
        List<NameValueList> nameValueLists = new ArrayList<>();
        List<InventoryCompDetailRes> inventoryCompDetailRes = inventoryCompDetailResRepository.findAllComp(inventory.getId());
        if (inventoryCompDetailRes.size() > 0) {
            for (InventoryCompDetailRes inventoryCompDetailRe : inventoryCompDetailRes) {
                List<String> values = new ArrayList<>();
                values.add(inventoryCompDetailRe.getValue());
                nameValueLists.add(new NameValueList(inventoryCompDetailRe.getName(), values));
            }
        }
        List<InventorySpec> inventorySpecs = inventorySpecRepository.findAll(inventory.getId());
        for (InventorySpec inventorySpec : inventorySpecs) {
            List<String> values = new ArrayList<>();
            values.add(inventorySpec.getValue());
            nameValueLists.add(new NameValueList(inventorySpec.getName(), values));
        }

        if (nameValueLists.size() > 0) {
            itemSpecifics.setNameValueLists(nameValueLists);
        }
        item.setItemSpecifics(itemSpecifics);


        ReturnPolicy returnPolicy = new ReturnPolicy();
        returnPolicy.setReturnsAcceptedOption(RETURN_ACCEPTED_OPTION_DEFAULT);
        returnPolicy.setRefundOption(REFUND_OPTION);
        returnPolicy.setReturnsWithinOption(RETURN_WITH_IN_OPTION);
        returnPolicy.setDescription(DESCRIPTION_RETURN);
        returnPolicy.setShippingCostPaidByOption(SHIPPING_COST_PAID_BY_OPTION);
        item.setReturnPolicy(returnPolicy);

        ShippingDetailsRequest shippingDetails = new ShippingDetailsRequest();
        CalculatedShippingRate calculatedShippingRate = new CalculatedShippingRate();
        calculatedShippingRate.setOriginatingPostalCode(POST_CODE_BBB);
        calculatedShippingRate.setMeasurementUnit(MEASUREMENT_UNIT);
        calculatedShippingRate.setPackageDepth(6);
        calculatedShippingRate.setPackageLength(7);
        calculatedShippingRate.setPackageWidth(7);
        calculatedShippingRate.setShippingPackage("PackageThickEnvelope");
        WeightCalculateShip weightMajor = new WeightCalculateShip();
        weightMajor.setMeasurementSystem(EbayValue.MEASUREMENT_SYSTEM);
        weightMajor.setUnit(EbayValue.UNIT_LBS);
        weightMajor.setValue(2);
        calculatedShippingRate.setWeightMajor(weightMajor);
        WeightCalculateShip weightMinor = new WeightCalculateShip();
        weightMinor.setMeasurementSystem(EbayValue.MEASUREMENT_SYSTEM);
        weightMinor.setUnit(EbayValue.UNIT_LBS);
        weightMajor.setValue(0);
        calculatedShippingRate.setWeightMinor(weightMinor);
        shippingDetails.setCalculatedShippingRate(calculatedShippingRate);
        shippingDetails.setPaymentInstructions(PAYMENT_INTRODUCTION);

        SalesTax salesTax = new SalesTax();
        salesTax.setSalesTaxPercent(8.75f);
        salesTax.setSalesTaxState(STATE_DEFAULT);
        shippingDetails.setSalesTax(salesTax);

        ShippingServiceOptions shippingServiceOptions = new ShippingServiceOptions();
        shippingServiceOptions.setFreeShipping(true);
        shippingServiceOptions.setShippingService(SHIPPING_SERVICE_DEFAULT);
        shippingServiceOptions.setShippingServicePriority(SHIPPING_SERVICE_PRIORITY);
        shippingDetails.setShippingServiceOptions(shippingServiceOptions);

        shippingDetails.setShippingType("Calculated");

        item.setShippingDetails(shippingDetails);


        item.setSite(SITE_US);
        SellerProfiles sellerProfiles = new SellerProfiles();
        SellerShippingProfile sellerShippingProfile = new SellerShippingProfile();
//        sellerShippingProfile.setShippingProfileID(SHIPPING_PROFILE_ID);
        sellerShippingProfile.setShippingProfileID(shippingProfileId);
        sellerProfiles.setSellerShippingProfile(sellerShippingProfile);

        SellerReturnProfile sellerReturnProfile = new SellerReturnProfile();
        sellerReturnProfile.setReturnProfileID(RETURN_PROFILE_ID);
        sellerProfiles.setSellerReturnProfile(sellerReturnProfile);

        SellerPaymentProfile sellerPaymentProfile = new SellerPaymentProfile();
        sellerPaymentProfile.setPaymentProfileID(PAYMENT_PROFILE_ID);
        sellerProfiles.setSellerPaymentProfile(sellerPaymentProfile);
        item.setSellerProfiles(sellerProfiles);
        return item;

    }

    public synchronized Object deListMarketListing(List<Long> marketListingIds) throws ExceptionResponse {
        CommonUtils.stream(marketListingIds);
        if (marketListingIds.size() == 0) {
            return new ArrayList<>();
        }
        List<MarketListing> marketListings = marketListingRepository.findAllByIds(marketListingIds);
        for (MarketListing marketListing : marketListings) {
            if (marketListing.isDelete()) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING + " " + marketListing.getId() + " " + DELETED),
                        HttpStatus.NOT_FOUND);
            }
        }
        if (marketListings.size() != marketListingIds.size()) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }

        List<MarketToList> marketToLists = marketToListRepository.findAllIdActiveEbayBBB(appConfig.getEbay().isSandbox());
        MarketToList bbbType = CommonUtils.findObject(marketToLists, o -> o.getType() == MarketType.BBB);
        MarketToList ebayType = CommonUtils.findObject(marketToLists, o -> o.getType() == MarketType.EBAY);

        if (bbbType == null || ebayType == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, INVALID_PARAM),
                    HttpStatus.NOT_FOUND);
        }
        List<MarketListing> bbbs = new ArrayList<>();
        List<MarketListing> ebays = new ArrayList<>();
        for (int i = 0; i < marketListings.size(); i++) {
            MarketListing marketListing = marketListings.get(i);
            if (marketListing.getStatus() != StatusMarketListing.LISTED) {
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_MARKET_LISTING_INVALID), HttpStatus.BAD_REQUEST);
                marketListings.remove(i);
                marketListingIds.remove(marketListing.getId());
                i--;
                continue;

            }
            if (marketListing.getMarketPlaceId() == bbbType.getMarketPlaceId()) {
                bbbs.add(marketListing);
            } else {
                if (marketListing.getMarketPlaceId() == ebayType.getMarketPlaceId()) {
                    ebays.add(marketListing);
                }
            }
        }


        Pair<BaseEbayResponse, List<Long>> resEbay;
        String messageError = null;
        if (ebays.size() > 0) {
            resEbay = marketListingManagerAsync.endAllItemEbayNoAsync(ebays);
            if (resEbay != null) {
                if (resEbay.getFirst() != null && resEbay.getFirst() instanceof EndItemResponse) {
                    messageError = CommonUtils.getMessageError(resEbay.getFirst().getErrors());
                    //Todo miss check error ebay
//                    if (messageError != null) {
//                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, messageError), HttpStatus.BAD_REQUEST);
//                    }
                } else {
                    if (resEbay.getFirst() != null && resEbay.getFirst() instanceof EndItemsResponse) {
                        for (EndItemResponseContainer endItemResponseContainer : ((EndItemsResponse) resEbay.getFirst()).getEndItemResponseContainers()) {
                            messageError = CommonUtils.getMessageError(endItemResponseContainer.getErrors());
                            if (messageError != null) {
                                break;
                            }
                        }
                        //Todo miss check error ebay
//                        if (messageError != null) {
//                            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, messageError), HttpStatus.BAD_REQUEST);
//                        }

                    }
                }
            } else {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, "ERROR ebay"), HttpStatus.BAD_REQUEST);
            }

        } else {
            resEbay = null;
        }

        if (bbbs.size() > 0) {
            List<Long> marketListingIdsBBB = bbbs.stream().map(MarketListing::getId).collect(Collectors.toList());
            offerManager.rejectOtherOffer(null, marketListingIdsBBB, null);
            for (MarketListing bbb : bbbs) {
                bbb.setStatus(StatusMarketListing.DE_LISTED);
            }
        }

        List<Long> marketListingEndIds = bbbs.stream().map(MarketListing::getId).collect(Collectors.toList());
        if (resEbay != null) {
            if (resEbay.getSecond() != null && resEbay.getSecond().size() > 0) {
                for (int i = 0; i < ebays.size(); i++) {
                    MarketListing ebay = ebays.get(i);
                    if (resEbay.getSecond().contains(ebay.getItemId())) {
                        ebays.remove(i);
                        i--;
                    } else {
                        marketListingEndIds.add(ebay.getId());
                    }
                }
            } else {
                marketListingEndIds.addAll(ebays.stream().map(MarketListing::getId).collect(Collectors.toList()));
            }

        }
        if (marketListingEndIds.size() > 0) {
            marketListingRepository.updateStatusMarketListing(marketListingEndIds, StatusMarketListing.DE_LISTED.getValue());
        }

        ebays = ebays.stream().peek(o -> o.setStatus(StatusMarketListing.DE_LISTED)).collect(Collectors.toList());
        listingWizardManagerAsync.updateWhenInventoryWhenDeListedAsync(bbbs, ebays);
        if (messageError != null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, messageError), HttpStatus.BAD_REQUEST);
        }
        List<MarketListing> marketListingsNew = new ArrayList<>();
        marketListingsNew.addAll(bbbs);
        marketListingsNew.addAll(ebays);
        return marketListingsNew;

    }

    private void triggerBBBPublishEvent(List<MarketListing> bbbListings) {
        if (bbbListings.size() > 0) {
            subscriptionManager.triggerPublishAsync(
                    bbbListings.stream()
                            .filter(ml -> ml.getStatus() == StatusMarketListing.LISTED)
                            .collect(Collectors.toList())
            );
        }
    }

    public Object removeQueueMarketListing(List<Long> marketListingIds) throws ExceptionResponse {
        checkQueueEbay(marketListingIds);
        marketListingRepository.deleteMarketListings(marketListingIds);
        return SUCCESS;
    }

    private void checkQueueEbay(List<Long> marketListingEbayIds) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }
        MarketToList marketToListEbay = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToListEbay == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MARKET_CONFIG_ALREADY_EXISTED),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (marketListingEbayIds == null || marketListingEbayIds.size() == 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YOUR_MUST_TRANSMIT_MARKET_LISTING_ID_QUEUE),
                    HttpStatus.BAD_REQUEST
            );
        }
        CommonUtils.stream(marketListingEbayIds);
        int countMarketQueue = marketListingRepository.countMarketListingQueue(marketListingEbayIds, marketToListEbay.getMarketPlaceId());
        if (countMarketQueue != marketListingEbayIds.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, ALL_STATUS_MARKET_MUST_BE_QUEUE),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    public synchronized Object startAsyncMarketListing(List<Long> marketListingIds, HttpServletRequest servletRequest) throws ExceptionResponse {
        checkQueueEbay(marketListingIds);
        CommonUtils.stream(marketListingIds);
        List<MarketListing> marketListings = marketListingRepository.findAllByIds(marketListingIds);
        if (marketListings.size() > 0) {
            marketListings = marketListings.stream().peek(o -> o.setStatus(StatusMarketListing.LISTING)).collect(Collectors.toList());
            marketListingRepository.updateStatusMarketListing(
                    marketListings.stream().map(MarketListing::getId).collect(Collectors.toList()),
                    StatusMarketListing.LISTING.getValue()
            );
            if (marketListings.size() > EbayValue.MAX_START_ASYNC) {
                int count = marketListings.size() / EbayValue.MAX_START_ASYNC;
                int countThread;
                if (count <= 3) {
                    countThread = count;
                } else {
                    countThread = 3;
                }
                Executor executor = Executors.newFixedThreadPool(countThread);
                List<Observable<Boolean>> observables = new ArrayList<>();
                List<MarketListing> marketListingsFinal = marketListings;
                for (int i = 0; i < count; i++) {
                    int index = i;
                    observables.add(
                            Observable.create(((ObservableOnSubscribe<Boolean>) t -> {
                                marketListingManagerAsync.listEbay(
                                        marketListingsFinal.subList(index * EbayValue.MAX_START_ASYNC,
                                                (index + 1) * EbayValue.MAX_START_ASYNC),
                                        servletRequest.getServletPath());
                                t.onNext(true);
                                t.onComplete();
                            })).subscribeOn(Schedulers.from(executor))
                    );
                }
                if (count * EbayValue.MAX_START_ASYNC < marketListingsFinal.size()){
                    observables.add(
                            Observable.create(((ObservableOnSubscribe<Boolean>) t -> {
                                marketListingManagerAsync.listEbay(
                                        marketListingsFinal.subList(count * EbayValue.MAX_START_ASYNC,
                                                marketListingsFinal.size()),
                                        servletRequest.getServletPath());
                                t.onNext(true);
                                t.onComplete();
                            })).subscribeOn(Schedulers.from(executor))
                    );
                }
                Observable.zip(observables, o->o)
                        .subscribe();
            } else {
                marketListingManagerAsync.listEbayAsyncMarketListing(marketListings, servletRequest.getServletPath());
            }

        }

        return SUCCESS;
    }

//    public Object startAsyncMarketListing(List<Long> marketListingIds, HttpServletRequest servletRequest) throws ExceptionResponse {
//        checkQueueEbay(marketListingIds);
//        List<Long> marketListUpdates;
//        List<Long> marketListingIdsNotAsync;
//        if (marketListingIds.size() > EbayValue.MAX_START_ASYNC) {
//            marketListUpdates = marketListingIds.subList(0, EbayValue.MAX_START_ASYNC);
//            marketListingIdsNotAsync = marketListingIds.subList(EbayValue.MAX_START_ASYNC, marketListingIds.size());
//        } else {
//            marketListUpdates = marketListingIds;
//            marketListingIdsNotAsync = null;
//        }
//        List<MarketListing> marketListings = marketListingRepository.findAllByIds(marketListUpdates);
//        if (marketListings.size() > 0) {
//            marketListings = marketListings.stream().peek(o -> o.setStatus(StatusMarketListing.LISTING)).collect(Collectors.toList());
//            marketListingRepository.updateStatusMarketListing(
//                    marketListings.stream().map(MarketListing::getId).collect(Collectors.toList()),
//                    StatusMarketListing.LISTING.getValue()
//            );
//
//            marketListingManagerAsync.listEbayAsyncMarketListing(marketListings, servletRequest.getServletPath());
//        }
//        if (marketListingIdsNotAsync != null && marketListingIdsNotAsync.size() > 0) {
//            marketListingRepository.updateLastUpdate(marketListingIdsNotAsync);
//        }
//        return SUCCESS;
//    }
}
