package com.bbb.core.model.response.ebay.additem;

import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.bbb.core.model.response.ebay.Fees;
import com.bbb.core.model.response.ebay.additems.AddItemResponseContainer;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.joda.time.LocalDateTime;

import java.util.List;

@JacksonXmlRootElement(localName = "AddItemResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
public class AddItemResponse extends BaseEbayResponse {

    @JacksonXmlProperty(localName = "ItemID")
    private Long itemID;
    @JacksonXmlProperty(localName = "StartTime")
    private LocalDateTime startTime;
    @JacksonXmlProperty(localName = "EndTime")
    private LocalDateTime endTime;
    @JacksonXmlProperty(localName = "Fees")
    private Fees fees;
    @JacksonXmlProperty(localName = "DiscountReason")
    private String discountReason;


    public Long getItemID() {
        return itemID;
    }

    public void setItemID(Long itemID) {
        this.itemID = itemID;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Fees getFees() {
        return fees;
    }

    public void setFees(Fees fees) {
        this.fees = fees;
    }

    public String getDiscountReason() {
        return discountReason;
    }

    public void setDiscountReason(String discountReason) {
        this.discountReason = discountReason;
    }
}
