package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class LabelPart {
    @JacksonXmlProperty(localName = "DocumentPartSequenceNumber")
    private int documentPartSequenceNumber;
    @JacksonXmlProperty(localName = "Image")
    private String image;

    public int getDocumentPartSequenceNumber() {
        return documentPartSequenceNumber;
    }

    public void setDocumentPartSequenceNumber(int documentPartSequenceNumber) {
        this.documentPartSequenceNumber = documentPartSequenceNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
