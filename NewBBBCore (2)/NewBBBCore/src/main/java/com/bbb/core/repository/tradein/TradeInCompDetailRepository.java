package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInCompDetail;
import com.bbb.core.model.database.embedded.TradeInCompDetailId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInCompDetailRepository extends JpaRepository<TradeInCompDetail, TradeInCompDetailId> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_comp_detail " +
            "WHERE trade_in_id = :tradeInId")
    List<TradeInCompDetail> findByTradeIn(
            @Param("tradeInId") long tradeInId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "trade_in_custom_quote_comp_detail.comp_type_id AS inventory_comp_type_id, " +
            "trade_in.id AS trade_in_id, " +
            "trade_in_custom_quote_comp_detail.value " +
            "FROM trade_in " +

            "JOIN trade_in_custom_quote ON trade_in.id = :tradeInId " +
            "AND trade_in.custom_quote_id = trade_in_custom_quote.id " +

            "JOIN trade_in_custom_quote_comp_detail " +
            "ON trade_in_custom_quote.id = trade_in_custom_quote_comp_detail.trade_in_custom_quote_id"
    )
    List<TradeInCompDetail> mapCustomQuoteCompsToTradeIn(
            @Param("tradeInId") long tradeInId
    );
}
