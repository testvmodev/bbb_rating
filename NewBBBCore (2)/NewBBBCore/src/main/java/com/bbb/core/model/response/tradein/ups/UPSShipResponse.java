package com.bbb.core.model.response.tradein.ups;

import com.bbb.core.model.response.tradein.ups.detail.Money;
import com.bbb.core.model.response.tradein.ups.detail.fault.Fault;
import com.bbb.core.model.response.tradein.ups.detail.ship.ShipmentResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSShipResponse {
    @JsonProperty("Fault")
    private Fault fault;
    @JsonProperty("ShipmentResponse")
    private ShipmentResponse shipmentResponse;

    public Fault getFault() {
        return fault;
    }

    public void setFault(Fault fault) {
        this.fault = fault;
    }

    public ShipmentResponse getShipmentResponse() {
        return shipmentResponse;
    }

    public void setShipmentResponse(ShipmentResponse shipmentResponse) {
        this.shipmentResponse = shipmentResponse;
    }

    public boolean isSuccess() {
        try {
            return fault == null && shipmentResponse.getResponseInfo().getResponseStatus().getCode() == 1;
        } catch (Exception e) {
            return false;
        }
    }

    @JsonIgnore
    public String getLabelImage() {
        return shipmentResponse.getShipmentResults().getPackageResults().getShippingLabel().getGraphicImage();
    }

    @JsonIgnore
    public String getTrackingNumber() {
        return shipmentResponse.getShipmentResults().getPackageResults().getTrackingNumber();
    }

    @JsonIgnore
    public Money getShipmentTotalCharge() {
        return shipmentResponse.getShipmentResults().getShipmentCharges().getTotalCharges();
    }
}
