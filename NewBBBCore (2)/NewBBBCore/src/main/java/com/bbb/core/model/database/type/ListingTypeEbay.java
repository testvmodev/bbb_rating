package com.bbb.core.model.database.type;

public enum  ListingTypeEbay {
    AUCTION("Auction"),
    CHINESE("Chinese"),
    CUSTOM_CODE("CustomCode"),
    FIXED_PRICE_ITEM("FixedPriceItem"),
    LEAD_GENERATION("LeadGeneration"),
    PERSONAL_OFFER("PersonalOffer"),
    UNKNOWN("Unknown");
    private final String value;

    ListingTypeEbay(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
