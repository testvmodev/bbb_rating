package com.bbb.core.model.otherservice.request.mail.offer.content;

public class ReceiveSendingMailRequest {
    private String id;
    private String name;

    public ReceiveSendingMailRequest(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public ReceiveSendingMailRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
