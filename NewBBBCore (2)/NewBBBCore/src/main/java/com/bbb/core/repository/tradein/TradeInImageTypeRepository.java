package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInImageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface TradeInImageTypeRepository extends JpaRepository<TradeInImageType, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image_type")
    List<TradeInImageType> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image_type " +
            "WHERE " +
            "(?1 IS NULL OR trade_in_image_type.id = ?1) AND " +
            "(?2 IS NULL OR trade_in_image_type.name LIKE ?2)")
    List<TradeInImageType> findAll(Long id, String name);

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image_type WHERE trade_in_image_type.is_require = 1 ORDER BY trade_in_image_type.id ASC")
    List<TradeInImageType> findAllImageRequire();

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image_type WHERE trade_in_image_type.is_require = 0 ORDER BY trade_in_image_type.id ASC")
    List<TradeInImageType> findAllImageNotRequire();

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM trade_in_image_type WHERE trade_in_image_type.id IN :imageTypeIds")
    int getCountImageType(
            @Param(value = "imageTypeIds") List<Long> imageTypeIds
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM trade_in_image_type")
    int countAll();
}
