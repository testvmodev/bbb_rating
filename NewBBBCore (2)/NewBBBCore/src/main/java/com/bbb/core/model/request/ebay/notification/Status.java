package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class Status {
    @JacksonXmlProperty(localName = "eBayPaymentStatus")
    private String eBayPaymentStatus;
    @JacksonXmlProperty(localName = "CheckoutStatus")
    private String checkoutStatus;
    @JacksonXmlProperty(localName = "LastTimeModified")
    private LocalDateTime lastTimeModified;
    @JacksonXmlProperty(localName = "PaymentMethodUsed")
    private String paymentMethodUsed;
    @JacksonXmlProperty(localName = "CompleteStatus")
    private String completeStatus;
    @JacksonXmlProperty(localName = "BuyerSelectedShipping")
    private boolean buyerSelectedShipping;
    @JacksonXmlProperty(localName = "PaymentHoldStatus")
    private String paymentHoldStatus;
    @JacksonXmlProperty(localName = "IntegratedMerchantCreditCardEnabled")
    private boolean integratedMerchantCreditCardEnabled;
    @JacksonXmlProperty(localName = "InquiryStatus")
    private String inquiryStatus;
    @JacksonXmlProperty(localName = "ReturnStatus")
    private String returnStatus;

    public String geteBayPaymentStatus() {
        return eBayPaymentStatus;
    }

    public void seteBayPaymentStatus(String eBayPaymentStatus) {
        this.eBayPaymentStatus = eBayPaymentStatus;
    }

    public String getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(String checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }

    public LocalDateTime getLastTimeModified() {
        return lastTimeModified;
    }

    public void setLastTimeModified(LocalDateTime lastTimeModified) {
        this.lastTimeModified = lastTimeModified;
    }

    public String getPaymentMethodUsed() {
        return paymentMethodUsed;
    }

    public void setPaymentMethodUsed(String paymentMethodUsed) {
        this.paymentMethodUsed = paymentMethodUsed;
    }

    public String getCompleteStatus() {
        return completeStatus;
    }

    public void setCompleteStatus(String completeStatus) {
        this.completeStatus = completeStatus;
    }

    public boolean isBuyerSelectedShipping() {
        return buyerSelectedShipping;
    }

    public void setBuyerSelectedShipping(boolean buyerSelectedShipping) {
        this.buyerSelectedShipping = buyerSelectedShipping;
    }

    public String getPaymentHoldStatus() {
        return paymentHoldStatus;
    }

    public void setPaymentHoldStatus(String paymentHoldStatus) {
        this.paymentHoldStatus = paymentHoldStatus;
    }

    public boolean isIntegratedMerchantCreditCardEnabled() {
        return integratedMerchantCreditCardEnabled;
    }

    public void setIntegratedMerchantCreditCardEnabled(boolean integratedMerchantCreditCardEnabled) {
        this.integratedMerchantCreditCardEnabled = integratedMerchantCreditCardEnabled;
    }

    public String getInquiryStatus() {
        return inquiryStatus;
    }

    public void setInquiryStatus(String inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }
}
