package com.bbb.core.model.request.sort;

public enum ComponentTypeSortField {
    NAME("name"),
    CATEGORY_NAME("category");

    private final String value;

    ComponentTypeSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
