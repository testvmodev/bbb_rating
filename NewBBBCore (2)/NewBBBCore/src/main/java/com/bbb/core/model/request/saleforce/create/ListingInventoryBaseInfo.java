package com.bbb.core.model.request.saleforce.create;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.RecordTypeInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ListingInventoryBaseInfo {
    @NotBlank
    private String brandName;
    @NotBlank
    private String modelName;
    @NotBlank
    private String yearName;
    @NotBlank
    private String bicycleType;
    @NotBlank
    private String inventoryTypeName;
    @NotBlank
    @JsonProperty("statusInventory")
    private String statusInventoryRaw;
    @NotBlank
    @JsonProperty("stageInventory")
    private String stageInventoryRaw;
    private String description;
    private Float msrpPrice;
    @JsonIgnore
    private Float initialListPrice;
    @JsonIgnore
    private Float cogsPrice;
    @JsonIgnore
    private Float overrideTradeInPrice;
    @JsonIgnore
    private Float discountedPrice;
    @JsonIgnore
    private Float bbbValue;
    @NotNull
    private Float currentListedPrice;
    private String serialNumber;
    @NotNull
    private ConditionInventory condition;
    private RecordTypeInventory recordType;
    private String note;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Boolean isOversized;
    private Boolean isLocalPickupOnly;
    @NotEmpty
    private List<String> images;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getBicycleType() {
        return bicycleType;
    }

    public void setBicycleType(String bicycleType) {
        this.bicycleType = bicycleType;
    }

    public String getInventoryTypeName() {
        return inventoryTypeName;
    }

    public void setInventoryTypeName(String inventoryTypeName) {
        this.inventoryTypeName = inventoryTypeName;
    }

    public String getStatusInventoryRaw() {
        return statusInventoryRaw;
    }

    public void setStatusInventoryRaw(String statusInventoryRaw) {
        this.statusInventoryRaw = statusInventoryRaw;
    }

    public String getStageInventoryRaw() {
        return stageInventoryRaw;
    }

    public void setStageInventoryRaw(String stageInventoryRaw) {
        this.stageInventoryRaw = stageInventoryRaw;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public RecordTypeInventory getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordTypeInventory recordType) {
        this.recordType = recordType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Boolean getOversized() {
        return isOversized;
    }

    public void setOversized(Boolean oversized) {
        isOversized = oversized;
    }

    public Boolean getLocalPickupOnly() {
        return isLocalPickupOnly;
    }

    public void setLocalPickupOnly(Boolean localPickupOnly) {
        isLocalPickupOnly = localPickupOnly;
    }

    @JsonIgnore
    public StatusInventory getStatusInventory() {
        return StatusInventory.findByValue(statusInventoryRaw);
    }

    @JsonIgnore
    public StageInventory getStageInventory() {
        return StageInventory.findByValue(stageInventoryRaw);
    }
}
