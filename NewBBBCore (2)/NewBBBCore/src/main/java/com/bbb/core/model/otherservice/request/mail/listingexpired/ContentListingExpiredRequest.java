package com.bbb.core.model.otherservice.request.mail.listingexpired;

import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;

import java.util.List;

public class ContentListingExpiredRequest {
    private UserMailRequest user;
    private List<ListingExpiredInfo> listings;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public List<ListingExpiredInfo> getListings() {
        return listings;
    }

    public void setListings(List<ListingExpiredInfo> listings) {
        this.listings = listings;
    }
}
