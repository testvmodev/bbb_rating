package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BaseEbayRequest {
    @JacksonXmlProperty(localName = "RequesterCredentials")
    private RequesterCredentials requesterCredentials;
    @JacksonXmlProperty(localName = "ErrorLanguage")
    private String errorLanguage = "en_US";
    @JacksonXmlProperty(localName = "WarningLevel")
    private String warningLevel = "High";

    public String getErrorLanguage() {
        return errorLanguage;
    }

    public void setErrorLanguage(String errorLanguage) {
        this.errorLanguage = errorLanguage;
    }

    public String getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(String warningLevel) {
        this.warningLevel = warningLevel;
    }

    public RequesterCredentials getRequesterCredentials() {
        return requesterCredentials;
    }

    public void setRequesterCredentials(RequesterCredentials requesterCredentials) {
        this.requesterCredentials = requesterCredentials;
    }
}
