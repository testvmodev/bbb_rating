package com.bbb.core.model.otherservice.response.mail;

import com.bbb.core.model.otherservice.request.mail.offer.content.ReceiveSendingMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SendingMailResponse {
    private ReceiveSendingMailRequest receive;
    @JsonProperty("mail_to")
    private List<String> mailTo;

    public ReceiveSendingMailRequest getReceive() {
        return receive;
    }

    public void setReceive(ReceiveSendingMailRequest receive) {
        this.receive = receive;
    }

    public List<String> getMailTo() {
        return mailTo;
    }

    public void setMailTo(List<String> mailTo) {
        this.mailTo = mailTo;
    }
}
