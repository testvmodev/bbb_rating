package com.bbb.core.manager.partner;

import com.bbb.core.model.request.partner.PartnerInfoRequest;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.repository.tradein.TradeInRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PartnerManager {
    @Autowired
    private TradeInRepository tradeInRepository;

    public Object updateInfoPartner(String partnerId, PartnerInfoRequest request) {
        tradeInRepository.updatePartnerAddress(partnerId, request.getName(), request.getAddress());
        return new MessageResponse("Success");
    }
}
