package com.bbb.core.model.request.rating;

import org.springframework.data.domain.Pageable;

public class RatingGetRequest {
    private Long marketListingId;
    private Long inventoryAuctionId;
    private Pageable pageable;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
