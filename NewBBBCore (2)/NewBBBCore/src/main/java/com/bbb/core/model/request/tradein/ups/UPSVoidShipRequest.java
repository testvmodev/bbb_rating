package com.bbb.core.model.request.tradein.ups;

import com.bbb.core.model.request.tradein.ups.detail.account.UPSSecurity;
import com.bbb.core.model.request.tradein.ups.detail.voidship.VoidShipmentRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSVoidShipRequest {
    @JsonProperty("UPSSecurity")
    private UPSSecurity upsSecurity;
    @JsonProperty("VoidShipmentRequest")
    private VoidShipmentRequest voidShipmentRequest;

    public UPSSecurity getUpsSecurity() {
        return upsSecurity;
    }

    public void setUpsSecurity(UPSSecurity upsSecurity) {
        this.upsSecurity = upsSecurity;
    }

    public VoidShipmentRequest getVoidShipmentRequest() {
        return voidShipmentRequest;
    }

    public void setVoidShipmentRequest(VoidShipmentRequest voidShipmentRequest) {
        this.voidShipmentRequest = voidShipmentRequest;
    }
}
