package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInShippingAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ShippingAccountRepository extends JpaRepository<TradeInShippingAccount, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_shipping_account " +
            "WHERE id = ?1")
    TradeInShippingAccount findOne(long id);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE trade_in_shipping_account " +
            "SET is_active = 0 " +
            "WHERE is_active = 1 AND id <> ?1")
    void deactivateOther(long exclude);

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_shipping_account")
    List<TradeInShippingAccount> findAll();

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM trade_in_shipping_account " +
            "WHERE is_active = 1")
    TradeInShippingAccount findActiveAccount();

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM trade_in_shipping_account " +
            "WHERE carrier_type = :carrierType " +
            "ORDER BY is_active DESC")
    TradeInShippingAccount findOnePreferActive(@Param(value = "carrierType") String carrierType);
}
