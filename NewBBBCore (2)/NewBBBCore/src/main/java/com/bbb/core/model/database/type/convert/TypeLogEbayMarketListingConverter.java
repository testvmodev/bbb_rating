package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TypeLogEbayMarketListing;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TypeLogEbayMarketListingConverter implements AttributeConverter<TypeLogEbayMarketListing, String> {
    @Override
    public String convertToDatabaseColumn(TypeLogEbayMarketListing typeLogEbayMarketListing) {
        if (typeLogEbayMarketListing == null) {
            return null;
        }
        return typeLogEbayMarketListing.getValue();
    }

    @Override
    public TypeLogEbayMarketListing convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        for (TypeLogEbayMarketListing value : TypeLogEbayMarketListing.values()) {
            if (value.getValue().equals(s)) {
                return value;
            }
        }
        return null;
    }
}
