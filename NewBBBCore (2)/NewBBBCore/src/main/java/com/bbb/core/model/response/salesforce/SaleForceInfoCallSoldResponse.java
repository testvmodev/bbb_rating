package com.bbb.core.model.response.salesforce;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SaleForceInfoCallSoldResponse {
    @Id
    private long inventoryId;
    private String salesforceId;
    private String shippingType;
    private float currentListedPrice;
    private String inventoryTypeName;
    private long listingId;
    private String buyerStreet;
    private String buyerState;
    private String buyerPaypalEmail;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(String salesforceId) {
        this.salesforceId = salesforceId;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getInventoryTypeName() {
        return inventoryTypeName;
    }

    public void setInventoryTypeName(String inventoryTypeName) {
        this.inventoryTypeName = inventoryTypeName;
    }

    public long getListingId() {
        return listingId;
    }

    public void setListingId(long listingId) {
        this.listingId = listingId;
    }

    public String getBuyerStreet() {
        return buyerStreet;
    }

    public void setBuyerStreet(String buyerStreet) {
        this.buyerStreet = buyerStreet;
    }

    public String getBuyerState() {
        return buyerState;
    }

    public void setBuyerState(String buyerState) {
        this.buyerState = buyerState;
    }

    public String getBuyerPaypalEmail() {
        return buyerPaypalEmail;
    }

    public void setBuyerPaypalEmail(String buyerPaypalEmail) {
        this.buyerPaypalEmail = buyerPaypalEmail;
    }
}
