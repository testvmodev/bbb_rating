package com.bbb.core.model.database;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_upgrade_comp_detail")
public class TradeInUpgradeCompDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long tradeInId;
    private long tradeInUpgradeCompId;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(value = GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public long getTradeInUpgradeCompId() {
        return tradeInUpgradeCompId;
    }

    public void setTradeInUpgradeCompId(long tradeInUpgradeCompId) {
        this.tradeInUpgradeCompId = tradeInUpgradeCompId;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(value = GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(value = GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
