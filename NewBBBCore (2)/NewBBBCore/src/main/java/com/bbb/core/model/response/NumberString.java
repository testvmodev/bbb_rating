package com.bbb.core.model.response;

import com.bbb.core.model.database.embedded.NumberStringId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class NumberString {
    @EmbeddedId
    private NumberStringId id;

    public NumberStringId getId() {
        return id;
    }

    public void setId(NumberStringId id) {
        this.id = id;
    }
}
