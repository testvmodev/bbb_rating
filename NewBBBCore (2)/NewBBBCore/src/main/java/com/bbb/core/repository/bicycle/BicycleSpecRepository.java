package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.BicycleSpec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface BicycleSpecRepository extends JpaRepository<BicycleSpec, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_spec WHERE bicycle_spec.bicycle_id = :bicycleId")
    List<BicycleSpec> findAllByBicycleId(@Param(value = "bicycleId") long bicycleId);
}
