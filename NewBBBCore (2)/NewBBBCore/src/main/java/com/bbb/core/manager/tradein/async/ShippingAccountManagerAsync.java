package com.bbb.core.manager.tradein.async;

import com.bbb.core.model.database.TradeInShippingAccount;
import com.bbb.core.repository.tradein.ShippingAccountRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShippingAccountManagerAsync {
    @Autowired
    private ShippingAccountRepository shippingAccountRepository;

    @Async
    public void deactivateOther(long id) {
        shippingAccountRepository.deactivateOther(id);
    }
}
