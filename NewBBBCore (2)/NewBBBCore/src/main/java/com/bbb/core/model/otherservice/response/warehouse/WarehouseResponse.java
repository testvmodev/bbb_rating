package com.bbb.core.model.otherservice.response.warehouse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WarehouseResponse {
    @JsonProperty("_id")
    private String id;
    private String name;
    @JsonProperty("shipping")
    private ShippingAddressResponse shippingAddress;
    @JsonProperty("billing")
    private BillingAddressResponse billingAddress;
    private Float taxRate;
    private Float shippingRate;
    private Float restockingFee;
    private String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShippingAddressResponse getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddressResponse shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public BillingAddressResponse getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddressResponse billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Float getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Float taxRate) {
        this.taxRate = taxRate;
    }

    public Float getShippingRate() {
        return shippingRate;
    }

    public void setShippingRate(Float shippingRate) {
        this.shippingRate = shippingRate;
    }

    public Float getRestockingFee() {
        return restockingFee;
    }

    public void setRestockingFee(Float restockingFee) {
        this.restockingFee = restockingFee;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
