package com.bbb.core.controller.bid;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.request.bid.auction.AuctionCreateRequest;
import com.bbb.core.model.request.bid.auction.AuctionRequest;
import com.bbb.core.model.request.bid.auction.AuctionUpdateRequest;
import com.bbb.core.security.authen.usercontext.UserContextManager;
import com.bbb.core.service.bid.AuctionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
public class AuctionController {
    @Autowired
    private AuctionService service;
    @Autowired
    private UserContextManager userContextManager;

    @GetMapping(value = Constants.URL_GET_AUCTION)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getAuctions(
//            @RequestParam(value = "startDate") @DateTimeFormat(pattern = Constants.FORMAT_DATE_TIME) LocalDateTime startDate,
//            @RequestParam(value = "endDate") @DateTimeFormat(pattern = Constants.FORMAT_DATE_TIME) LocalDateTime endDate,
            @RequestParam(value = "createdById", required = false) String createdById,
            Pageable page
    ) throws ExceptionResponse {
        AuctionRequest request = new AuctionRequest();
        request.setCreatedById(createdById);
        return new ResponseEntity<>(service.getAuction(request, page), HttpStatus.OK);
    }


    @PostMapping(value = Constants.URL_AUCTION_CREATE)
    public ResponseEntity createAuction(
            @RequestParam(value = "startDate") Long startDate,
            @RequestParam(value = "endDate") Long endDate,
            @RequestParam(value = "name") String name,
            HttpServletRequest httpServletRequest
    )
            throws ExceptionResponse {
        AuctionCreateRequest request = new AuctionCreateRequest();
        request.setStartDate(CommonUtils.unixTimestampToLocalDateTime(startDate));
        request.setEndDate(CommonUtils.unixTimestampToLocalDateTime(endDate));
//        request.setCreateById(userContextManager.getUserContext(httpServletRequest).getId());
        request.setName(name);
        return new ResponseEntity<>(service.createAuction(request), HttpStatus.OK);
    }


    @PutMapping(value = Constants.URL_AUCTION)
    public ResponseEntity updateAuction(
            @RequestBody AuctionUpdateRequest request,
            @PathVariable(value = Constants.ID) long auctionId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateAuction(auctionId, request), HttpStatus.OK);
    }

}


