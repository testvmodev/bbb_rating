package com.bbb.core.model.request.bicycletype;

public class TypeUpdateRequest{

    private String name;
    private Float valueModifier;
    private String imageDefault;
    private long sortOrder;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(Float valueModifier) {
        this.valueModifier = valueModifier;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(long sortOrder) {
        this.sortOrder = sortOrder;
    }
}
