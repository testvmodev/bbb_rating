package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.component.ComponentTypeCreateRequest;
import com.bbb.core.model.request.component.ComponentTypeGetRequest;
import com.bbb.core.model.request.component.ComponentTypeUpdateRequest;
import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ComponentTypeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.bbb.core.common.Constants.URL_GET_BICYCLE;

@RestController
public class ComponentTypeController {
    @Autowired
    private ComponentTypeService service;

//    @GetMapping("/importComponentType")
//    public ResponseEntity importComponentType() {
//        return new ResponseEntity(componentTypeService.importComponentType(), HttpStatus.OK);
//    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    @GetMapping(value = Constants.URL_GET_COMPONENT_TYPE)
    public ResponseEntity getComponentTypes(
            @RequestParam(value = "sortType",defaultValue = "ASC") Sort sortType,
            @RequestParam(value = "sortField" , defaultValue = "NAME") ComponentTypeSortField sortField,
            Pageable pageable
    ) throws ExceptionResponse{
        ComponentTypeGetRequest request = new ComponentTypeGetRequest();
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);
        return new ResponseEntity<>(service.getComponentTypes(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_DETAIL_COMPONENT_TYPE)
    public ResponseEntity getComponentType(@PathVariable(value = Constants.ID) long typeId) throws ExceptionResponse{
        return new ResponseEntity<>(service.getComponentType(typeId), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_CREATE_COMPONENT_TYPE)
    public ResponseEntity createComponentType(
            @RequestBody ComponentTypeCreateRequest request) throws ExceptionResponse{
        return new ResponseEntity<>(service.createComponentType(request), HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_UPDATE_COMPONENT_TYPE)
    public ResponseEntity updateComponentType(
            @PathVariable(value = Constants.ID) long typeId,
            @RequestBody ComponentTypeUpdateRequest request
            ) throws ExceptionResponse{
        return new ResponseEntity<>(service.updateComponentType(typeId, request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_GET_DETAIL_COMPONENT_TYPE)
    public ResponseEntity deleteComponentType(@PathVariable(value = Constants.ID) long typeId) throws ExceptionResponse{
        return new ResponseEntity<>(service.deleteComponentType(typeId), HttpStatus.OK);
    }
}
