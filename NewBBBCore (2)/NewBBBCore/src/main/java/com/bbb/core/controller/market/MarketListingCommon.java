package com.bbb.core.controller.market;

import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Third;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.response.ebay.additems.AddItemResponseContainer;
import com.bbb.core.model.response.ebay.additems.AddItemsResponse;
import org.joda.time.LocalDateTime;

import java.util.List;

public class MarketListingCommon {
    public static void updateMarketListingEbay(List<MarketListing> marketListings,
                                        Third<String, String, AddItemsResponse> resEbays,
                                        List<Inventory> inventorys) {
        int index = 0;
        for (MarketListing marketListing : marketListings) {
            int indexFinal = index;
            AddItemResponseContainer addItemResponseContainer = CommonUtils.findObject(
                    resEbays.getThird().getAddItemResponseContainers(),
                    o -> Integer.valueOf(o.getCorrelationID()) == indexFinal
            );
            marketListing.setItemId(addItemResponseContainer.getItemID());
            String messageErrorElement = CommonUtils.getMessageError(addItemResponseContainer.getErrors());

            if (messageErrorElement == null) {
                marketListing.setStatus(StatusMarketListing.LISTED);
                marketListing.setLastMsgError(null);
                Inventory inventory = CommonUtils.findObject(inventorys, inv -> inv.getId() == marketListing.getInventoryId());
                marketListing.setEndTime(addItemResponseContainer.getEndTime());
                marketListing.setStartTime(addItemResponseContainer.getStartTime());
                if ( resEbays.getThird().getTimestamp() == null ) {
                    marketListing.setTimeListed(LocalDateTime.now());
                }else {
                    marketListing.setTimeListed(resEbays.getThird().getTimestamp());
                }

                if (inventory != null) {
                    inventory.setStage(StageInventory.LISTED);
                }
            } else {
                //todo update to LISTING_ERROR
                marketListing.setStatus(StatusMarketListing.ERROR);
                marketListing.setCountListingError(marketListing.getCountListingError() + 1);
                marketListing.setLastMsgError(messageErrorElement);
            }

            index++;
        }
    }
}
