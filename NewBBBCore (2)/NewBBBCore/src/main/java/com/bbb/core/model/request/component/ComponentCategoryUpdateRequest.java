package com.bbb.core.model.request.component;

public class ComponentCategoryUpdateRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
