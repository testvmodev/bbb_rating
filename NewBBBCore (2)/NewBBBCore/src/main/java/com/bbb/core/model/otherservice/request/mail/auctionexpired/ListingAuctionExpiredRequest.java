package com.bbb.core.model.otherservice.request.mail.auctionexpired;

public class ListingAuctionExpiredRequest {
    private long id;
    private Long auction_inv_id;
    private BikeAuctionExpiredRequest bike;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BikeAuctionExpiredRequest getBike() {
        return bike;
    }

    public void setBike(BikeAuctionExpiredRequest bike) {
        this.bike = bike;
    }

    public Long getAuction_inv_id() {
        return auction_inv_id;
    }

    public void setAuction_inv_id(Long auction_inv_id) {
        this.auction_inv_id = auction_inv_id;
    }
}
