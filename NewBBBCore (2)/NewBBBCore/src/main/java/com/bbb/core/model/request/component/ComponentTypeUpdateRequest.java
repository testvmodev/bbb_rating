package com.bbb.core.model.request.component;

public class ComponentTypeUpdateRequest {
    private String name;
    private Long sortOrder;
    private Long componentCategoryId;
    private Integer forOnlyBicycleTypeId;
    private Boolean isSelect;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Long getComponentCategoryId() {
        return componentCategoryId;
    }

    public void setComponentCategoryId(Long componentCategoryId) {
        this.componentCategoryId = componentCategoryId;
    }

    public Integer getForOnlyBicycleTypeId() {
        return forOnlyBicycleTypeId;
    }

    public void setForOnlyBicycleTypeId(Integer forOnlyBicycleTypeId) {
        this.forOnlyBicycleTypeId = forOnlyBicycleTypeId;
    }

    public Boolean isSelect() {
        return isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }
}
