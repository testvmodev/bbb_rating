package com.bbb.core.repository.inventory.out;

import com.bbb.core.model.InitialInventory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface InitialInventoryRepository extends CrudRepository<InitialInventory, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "model.name AS bicycle_model_name, model.id, " +
            "brand.name AS bicycle_brand_name, brand.id, " +
            "y.name AS bicycle_year_name, y.id, " +
            "t.name AS bicycle_type_name, t.id, " +
            "s.name AS bicycle_size_name, s.id " +
            "FROM bicycle_model AS model " +
            "JOIN bicycle_brand AS brand ON brand.id = :brandId " +
            "JOIN bicycle_year AS y ON y.id = :yearId " +
            "JOIN bicycle_type AS t ON t.id = :typeId " +
            "LEFT JOIN bicycle_size AS s ON s.id = :sizeId " +
            "WHERE model.id = :modelId"
    )
    InitialInventory initInventory(@Param("modelId") long modelId,
                                   @Param("brandId") long brandId,
                                   @Param("yearId") long yearId,
                                   @Param("typeId") long typeId,
                                   @Param("sizeId") Long sizeId
    );
}
