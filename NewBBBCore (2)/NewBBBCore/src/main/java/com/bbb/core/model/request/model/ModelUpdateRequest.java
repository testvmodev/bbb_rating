package com.bbb.core.model.request.model;

public class ModelUpdateRequest extends ModelCreateRequest {
    private Boolean isApproved;

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }
}
