package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusInventory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusInventoryConverter implements AttributeConverter<StatusInventory, String> {
    @Override
    public String convertToDatabaseColumn(StatusInventory inventoryStatus) {
        if (inventoryStatus == null) {
            return null;
        }
        return inventoryStatus.getValue();
    }

    @Override
    public StatusInventory convertToEntityAttribute(String value) {
        return StatusInventory.findByValue(value);
    }
}
