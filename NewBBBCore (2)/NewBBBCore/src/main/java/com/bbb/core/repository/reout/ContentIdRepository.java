package com.bbb.core.repository.reout;

import com.bbb.core.model.response.ContentId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContentIdRepository extends CrudRepository<ContentId, Long> {
//    @Query(nativeQuery = true,
//            value = "SELECT offer.id AS id " +
//                    "FROM " +
//                    "offer " +
//                    "JOIN " +
//                        "( " +
//                            "SELECT  offer.offer_price, offer.auction_id, offer.inventory_id, MIN(offer.created_time) AS created_time " +
//                            "FROM offer " +
//                            "JOIN " +
//                                "(SELECT offer.auction_id AS  auction_id_max, offer.inventory_id AS inventory_id_max, MAX(offer.offer_price) AS max_price  " +
//                                    "FROM " +
//                                    "offer " +
//                                    "WHERE offer.auction_id IS NOT NULL AND offer.auction_id IN :auctionIds AND offer.status = 'Pending' " +
//                                    "GROUP BY offer.auction_id, offer.inventory_id" +
//                                ") AS " +
//                            "max_table " +
//                        "ON (offer.auction_id = max_table.auction_id_max AND offer.inventory_id = max_table.inventory_id_max AND offer.offer_price = max_table.max_price) " +
//                        "GROUP BY offer.offer_price, offer.auction_id,  offer.inventory_id" +
//                        ") AS " +
//                    "result " +
//                    "ON (offer.auction_id = result.auction_id AND offer.inventory_id = result.inventory_id AND offer.offer_price = result.offer_price AND offer.created_time = result.created_time)"
//    )
//    List<ContentId> findAllOfferIdMax(
//            @Param(value = "auctionIds") List<Long> auctionIds
//    );

}
