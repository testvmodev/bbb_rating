package com.bbb.core.repository.market;

import com.bbb.core.model.database.InventoryLocationShipping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryLocationShippingRepository extends CrudRepository<InventoryLocationShipping, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_location_shipping " +
            "WHERE inventory_location_shipping.inventory_id = :inventoryId")
    InventoryLocationShipping findOne(
            @Param("inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_location_shipping " +
            "WHERE inventory_location_shipping.inventory_id IN :inventoryIds")
    List<InventoryLocationShipping> findAll(
            @Param("inventoryIds") List<Long> inventoryIds
    );
}
