package com.bbb.core.model.request.onlinestore;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class TrendingOnlineStoreRequest {
    private List<String> storefrontIds;
    @JsonIgnore
    private Pageable pageable;

    public List<String> getStorefrontIds() {
        return storefrontIds;
    }

    public void setStorefrontIds(List<String> storefrontIds) {
        this.storefrontIds = storefrontIds;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
