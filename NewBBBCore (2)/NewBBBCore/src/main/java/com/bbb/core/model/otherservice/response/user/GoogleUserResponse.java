package com.bbb.core.model.otherservice.response.user;


import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleUserResponse {
    private String id;
    @JsonProperty("google_token")
    private String googleToken;
    @JsonProperty("google_avatar")
    private String googleAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(String googleToken) {
        this.googleToken = googleToken;
    }

    public String getGoogleAvatar() {
        return googleAvatar;
    }

    public void setGoogleAvatar(String googleAvatar) {
        this.googleAvatar = googleAvatar;
    }
}
