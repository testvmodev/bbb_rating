package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest;
import com.bbb.core.model.response.tradein.TradeInResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInResponseRepository extends JpaRepository<TradeInResponse, Long> {
    //todo 12 is status id cancel
    @Query(nativeQuery = true, value = "SELECT " +
            "trade_in.id AS trade_in_id, " +
//            "(CASE WHEN trade_in.owner_address IS NOT NULL THEN trade_in.owner_address ELSE trade_in_custom_quote.employee_location END) AS owner_address, " +
            "trade_in.partner_address AS partner_address, " +
            "trade_in.partner_name AS partner_name, " +
            "trade_in.created_time AS created_time, " +
            "trade_in.owner_name AS customer_name, " +
            "trade_in.title AS title_bicycle, " +
            "trade_in.status_id AS status_id, " +
            "trade_in_status.name AS status_name," +
            "trade_in.is_cancel AS is_cancel, " +
            "trade_in.is_save AS is_save, " +
            "trade_in.custom_quote_id AS custom_quote_id, " +
            "trade_in_shipping.shipping_type AS shipping_type," +
            "inv.id AS inventory_id, " +
            "inv.status AS inventory_status, " +
            "inv.stage AS inventory_stage " +

            "FROM " +
            "trade_in " +
            "JOIN trade_in_status ON trade_in.status_id = trade_in_status.id " +
            "LEFT JOIN bicycle ON trade_in.bicycle_id = bicycle.id " +
            "LEFT JOIN trade_in_custom_quote ON trade_in.custom_quote_id = trade_in_custom_quote.id " +
            "LEFT JOIN trade_in_shipping ON trade_in.id = trade_in_shipping.trade_in_id " +
            "OUTER APPLY " +
                "(SELECT TOP 1 * FROM inventory " +
                "WHERE inventory.score_card_id = trade_in.id " +
                "AND inventory.is_delete = 0) AS inv " +
            "WHERE " +
            "(:partnerId IS NULL OR trade_in.partner_id = :partnerId) AND " +
            "(:statusId IS NULL OR (:statusId = 12 AND trade_in.is_cancel = 1 ) OR (trade_in.status_id = :statusId AND trade_in.is_cancel = 0)) AND " +
            "(:isArchived IS NULL OR trade_in.is_archived = :isArchived) AND " +
            "(:content IS NULL OR " +
            "(trade_in.title LIKE :content OR " +
            "CONVERT(NVARCHAR(255), trade_in.id) LIKE :content OR " +
//            "trade_in.owner_address LIKE :content OR " +
//            "trade_in_custom_quote.employee_location LIKE :content OR " +
            "trade_in.partner_name LIKE :content OR " +
            "trade_in.owner_name LIKE :content) " +
            ") AND " +
            "((:isIncomplete IS NULL) OR " +
            "(:isIncomplete = 1 AND trade_in.status_id IN :#{T(com.bbb.core.manager.tradein.TradeInUtil).getIncompleteStatues()}) OR " +
            "(:isIncomplete = 0 AND trade_in.status_id = :#{T(com.bbb.core.model.database.type.StatusTradeIn).COMPLETED.getValue()})) " +

            "ORDER BY " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).DATE} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.created_time END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).DATE} " +
            //default asc
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.created_time END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).ID} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.id END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).ID} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.id END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).CUSTOMER} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.owner_name END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).CUSTOMER} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.owner_name END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).LOCATION} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.partner_address END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).LOCATION} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.partner_address END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).BICYCLE} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.title END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).BICYCLE} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.title END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).STATUS} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_status.name END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).STATUS} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_status.name END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).CANCEL} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.is_cancel END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.tradein.tradin.TypeSearchTradeInRequest).CANCEL} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in.is_cancel END ASC " +

            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<TradeInResponse> findAllSorted(
            @Param(value = "statusId") Long statusId,
            @Param(value = "isArchived") Boolean isArchived,
            @Param(value = "content") String content,
            @Param(value = "partnerId") String partnerId,
            @Param("isIncomplete") Boolean isIncomplete,
            @Param("sortField") TypeSearchTradeInRequest sortField,
            @Param("sortType") Sort sortType,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );


    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(trade_in.id)" +
            "FROM " +
            "trade_in " +
            "JOIN trade_in_status ON trade_in.status_id = trade_in_status.id " +
            "LEFT JOIN bicycle ON trade_in.bicycle_id = bicycle.id " +
            "LEFT JOIN trade_in_custom_quote ON trade_in.custom_quote_id = trade_in_custom_quote.id " +
            "WHERE " +
            "(:partnerId IS NULL OR trade_in.partner_id = :partnerId) AND " +
            "(:statusId IS NULL OR (:statusId = 12 AND trade_in.is_cancel = 1 ) OR (trade_in.status_id = :statusId AND trade_in.is_cancel = 0)) AND " +
            "(:isArchived IS NULL OR trade_in.is_archived = :isArchived) AND " +
            "(:content IS NULL OR " +
            "(trade_in.title LIKE :content OR " +
            "CONVERT(NVARCHAR(255), trade_in.id) LIKE :content OR " +
            //            "trade_in.owner_address LIKE :content OR " +
//            "trade_in_custom_quote.employee_location LIKE :content OR " +
            "trade_in.partner_name LIKE :content OR " +
            "trade_in.owner_name LIKE :content) " +
            ") AND " +
            "((:isIncomplete IS NULL) OR " +
            "(:isIncomplete = 1 AND trade_in.status_id IN :#{T(com.bbb.core.manager.tradein.TradeInUtil).getIncompleteStatues()}) OR " +
            "(:isIncomplete = 0 AND trade_in.status_id = :#{T(com.bbb.core.model.database.type.StatusTradeIn).COMPLETED.getValue()})) "
    )
    int getCount(
            @Param(value = "statusId") Long statusId,
            @Param(value = "isArchived") Boolean isArchived,
            @Param(value = "content") String content,
            @Param(value = "partnerId") String partnerId,
            @Param("isIncomplete") Boolean isIncomplete
    );

}
