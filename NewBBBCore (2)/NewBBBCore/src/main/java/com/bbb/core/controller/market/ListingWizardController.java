package com.bbb.core.controller.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.listingwizard.ListingWizardType;
import com.bbb.core.model.request.market.marketlisting.CreateMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingInvToSalRequest;
import com.bbb.core.service.market.ListingWizardService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class ListingWizardController {
    @Autowired
    private ListingWizardService service;
    @PostMapping(value = Constants.URL_LISTING_WIZARD_INVENTORIES)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getListingWizards(
            @RequestBody @Valid List<MarketListingInvToSalRequest> request,
            @RequestParam(value = "type") ListingWizardType type,
            Pageable page
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getListingWizards(type, request, page), HttpStatus.OK);
    }
    @PostMapping(value = Constants.URL_LISTING_WIZARD)
    public ResponseEntity createMarketListing(
            @RequestBody @Valid CreateMarketListingRequest requests
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.createMarketListing(requests), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_LISTING_WIZARD_DE_LISTED)
    public ResponseEntity deListMarketListing(
            @RequestParam(value = "marketListingIds") List<Long> marketListingIds
    ) throws  ExceptionResponse{
        return new ResponseEntity<>(service.deListMarketListing(marketListingIds), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_LISTING_WIZARD_START_ASYNC)
    public ResponseEntity startAsyncMarketListing(
            @RequestParam(value = "marketListingIds") List<Long> marketListingIds,
            HttpServletRequest servletRequest
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.startAsyncMarketListing(marketListingIds, servletRequest), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_LISTING_WIZARD_REMOVE_QUEUE)
    public ResponseEntity removeQueueMarketListing(
            @RequestParam(value = "marketListingIds") List<Long> marketListingIds
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.removeQueueMarketListing(marketListingIds), HttpStatus.OK);
    }

}
