package com.bbb.core.model.request.tradein.fedex;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.request.tradein.fedex.detail.ClientDetail;
import com.bbb.core.model.request.tradein.fedex.detail.Version;
import com.bbb.core.model.request.tradein.fedex.detail.WebAuthenticationDetail;
import com.bbb.core.model.response.tradein.fedex.detail.ship.TrackingId;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "DeleteShipmentRequest", namespace = FedExValue.SHIP_ROOT_NAMESPACE)
public class FedexDeleteShipRequest {
    @JacksonXmlProperty(localName = "WebAuthenticationDetail")
    private WebAuthenticationDetail webAuthenticationDetail;
    @JacksonXmlProperty(localName = "ClientDetail")
    private ClientDetail clientDetail;
    @JacksonXmlProperty(localName = "Version")
    private Version version;
    @JacksonXmlProperty(localName = "TrackingId")
    private TrackingId trackingId;
    @JacksonXmlProperty(localName = "DeletionControl")
    private String deletionControl;

    public WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }

    public void setWebAuthenticationDetail(WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }

    public ClientDetail getClientDetail() {
        return clientDetail;
    }

    public void setClientDetail(ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public TrackingId getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(TrackingId trackingId) {
        this.trackingId = trackingId;
    }

    public String getDeletionControl() {
        return deletionControl;
    }

    public void setDeletionControl(String deletionControl) {
        this.deletionControl = deletionControl;
    }
}
