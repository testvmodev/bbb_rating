package com.bbb.core.model.response.bid.offer.offerbid;

import com.bbb.core.model.database.type.StatusOffer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class UserBidDetail {
    @Id
    private long offerId;
    private String buyerId;
    private float offerPrice;
    private StatusOffer offerStatus;
    private LocalDateTime createdTime;
    private LocalDateTime lastUpdate;
    @JsonIgnore
    private long inventoryAuctionId;

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public float getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(float offerPrice) {
        this.offerPrice = offerPrice;
    }

    public StatusOffer getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(StatusOffer offerStatus) {
        this.offerStatus = offerStatus;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }
}
