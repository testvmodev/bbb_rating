package com.bbb.core.model.database.type;

public enum MarketListingFilterType {
    RANGE("Range"),
    RANGE_IN("Range in"),
    RANGE_DATE_TIME("Range date time"),
    EQUAL("Equal");

    private final String value;

    MarketListingFilterType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
