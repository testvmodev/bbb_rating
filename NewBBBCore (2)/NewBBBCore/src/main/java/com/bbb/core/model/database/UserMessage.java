package com.bbb.core.model.database;


public class UserMessage {

  private long id;
  private long fromUserId;
  private long toUserId;
  private String subject;
  private String body;
  private java.sql.Timestamp createdTime;
  private String isRead;
  private long messageType;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(long fromUserId) {
    this.fromUserId = fromUserId;
  }


  public long getToUserId() {
    return toUserId;
  }

  public void setToUserId(long toUserId) {
    this.toUserId = toUserId;
  }


  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }


  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }


  public java.sql.Timestamp getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(java.sql.Timestamp createdTime) {
    this.createdTime = createdTime;
  }


  public String getIsRead() {
    return isRead;
  }

  public void setIsRead(String isRead) {
    this.isRead = isRead;
  }


  public long getMessageType() {
    return messageType;
  }

  public void setMessageType(long messageType) {
    this.messageType = messageType;
  }

}
