package com.bbb.core.model.request.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ResponsibleParty {
    @JacksonXmlProperty(localName = "AccountNumber")
    private String accountNumber;

    public ResponsibleParty() {}

    public ResponsibleParty(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
