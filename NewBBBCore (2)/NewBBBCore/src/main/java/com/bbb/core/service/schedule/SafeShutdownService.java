package com.bbb.core.service.schedule;

public interface SafeShutdownService {
    Object safeShutdown();
}
