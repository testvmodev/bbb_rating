package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.database.type.InboundShippingType;

public class TradeInShippingCreateRequest {
    private long tradeInId;
    private boolean isSave;
    private ShippingBicycleBlueBookRequest blueBook;
    private ShippingMyAccountRequest myAccount;
    private InboundShippingType shippingType;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public ShippingBicycleBlueBookRequest getBlueBook() {
        return blueBook;
    }

    public void setBlueBook(ShippingBicycleBlueBookRequest blueBook) {
        this.blueBook = blueBook;
    }

    public ShippingMyAccountRequest getMyAccount() {
        return myAccount;
    }

    public void setMyAccount(ShippingMyAccountRequest myAccount) {
        this.myAccount = myAccount;
    }

    public InboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(InboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }
}
