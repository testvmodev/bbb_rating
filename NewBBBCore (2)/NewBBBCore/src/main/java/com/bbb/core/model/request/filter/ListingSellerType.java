package com.bbb.core.model.request.filter;

public enum ListingSellerType {
    ALL,
    PERSONAL,
    ONLINE_STORE
}
