package com.bbb.core.model.request.mail;

import com.bbb.core.model.request.mail.offeraccept.content.ReceiveSendingMailRequest;

import java.util.ArrayList;
import java.util.List;

public class SendingMailRequest<T> {
    private ReceiveSendingMailRequest receive;
    private String exchange_type;
    private String template_key;
    private String subject;
    private List<String> mail_to = new ArrayList<>();
    private List<String> cc = new ArrayList<>();
    private List<String> bcc = new ArrayList<>();
    private T content_mail;

    public ReceiveSendingMailRequest getReceive() {
        return receive;
    }

    public void setReceive(ReceiveSendingMailRequest receive) {
        this.receive = receive;
    }

    public String getExchange_type() {
        return exchange_type;
    }

    public void setExchange_type(String exchange_type) {
        this.exchange_type = exchange_type;
    }

    public String getTemplate_key() {
        return template_key;
    }

    public void setTemplate_key(String template_key) {
        this.template_key = template_key;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getMail_to() {
        return mail_to;
    }

    public void setMail_to(List<String> mail_to) {
        this.mail_to = mail_to;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public T getContent_mail() {
        return content_mail;
    }

    public void setContent_mail(T content_mail) {
        this.content_mail = content_mail;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }
}
