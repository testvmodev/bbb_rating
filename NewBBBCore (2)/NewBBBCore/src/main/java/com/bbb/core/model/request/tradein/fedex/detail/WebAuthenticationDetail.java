package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class WebAuthenticationDetail {
    @JacksonXmlProperty(localName = "UserCredential")
    private UserCredential userCredential;

    public UserCredential getUserCredential() {
        return userCredential;
    }

    public void setUserCredential(UserCredential userCredential) {
        this.userCredential = userCredential;
    }
}
