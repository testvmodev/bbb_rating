package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TradeInRepository extends JpaRepository<TradeIn, Long> {
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE trade_in SET trade_in.is_archived = :isArchive WHERE trade_in.id = :tradeInId ")
    void updateUserArchive(
            @Param(value = "tradeInId") long tradeInId,
            @Param(value = "isArchive") boolean isArchive
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE trade_in " +
            "SET trade_in.is_admin_archived = :isAdminArchive " +
            "WHERE trade_in.id = :tradeInId ")
    void updateAdminArchive(
            @Param(value = "tradeInId") long tradeInId,
            @Param(value = "isAdminArchive") boolean isAdminArchive
    );

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM trade_in WHERE trade_in.id = :tradeInId")
    TradeIn findOne(
            @Param(value = "tradeInId") long tradeInId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM trade_in WHERE trade_in.custom_quote_id = :customQuoteId")
    TradeIn findOneByCustomQuoteId(
            @Param(value = "customQuoteId") long customQuoteId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM " +
            "trade_in_custom_quote " +
            "JOIN trade_in ON trade_in_custom_quote.id = trade_in.custom_quote_id " +
            "WHERE " +
            "trade_in_custom_quote.brand_id = :brandId AND " +
            "trade_in_custom_quote.model_id = :modelId AND " +
            "trade_in_custom_quote.year_id = :yearId AND " +
            "trade_in_custom_quote.status = :statusCustomQuote AND " +
            "trade_in_custom_quote.user_created_id = :userIdCreated"
    )
    TradeIn findOneFromCustomQuote(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId,
            @Param(value = "statusCustomQuote") String statusCustomQuote,
            @Param("userIdCreated") String userIdCreated
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE trade_in " +
            "SET trade_in.partner_name = :partnerName, " +
            "trade_in.partner_address = :partnerAddress " +
            "WHERE trade_in.partner_id = :partnerId ")
    void updatePartnerAddress(
            @Param(value = "partnerId") String partnerId,
            @Param(value = "partnerName") String partnerName,
            @Param(value = "partnerAddress") String partnerAddress
    );
}
