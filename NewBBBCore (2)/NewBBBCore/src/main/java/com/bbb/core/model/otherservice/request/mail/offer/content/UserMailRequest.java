package com.bbb.core.model.otherservice.request.mail.offer.content;

public class UserMailRequest {
    private String firstName;

    public UserMailRequest(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
