package com.bbb.core.model.request.tradein.customquote;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TradeInCustomQuoteUpdateRequest extends TradeInCustomQuoteCreateRequest {
    private Boolean isSubmit;

    public Boolean getSubmit() {
        return isSubmit;
    }

    public void setSubmit(Boolean submit) {
        isSubmit = submit;
    }

    @Override
    @JsonIgnore
    @Deprecated
    public Boolean getDraft() {
        return null;
    }

    @Override
    @JsonIgnore
    @Deprecated
    public void setDraft(Boolean draft) {
    }
}
