package com.bbb.core.model.response.component;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class ComponentTypeResponse {
    @Id
    private long id;
    private String name;
    private long sortOrder;
    private long componentCategoryId;
    private boolean isSelect;
    private String componentCategoryName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public long getComponentCategoryId() {
        return componentCategoryId;
    }

    public void setComponentCategoryId(long componentCategoryId) {
        this.componentCategoryId = componentCategoryId;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getComponentCategoryName() {
        return componentCategoryName;
    }

    public void setComponentCategoryName(String componentCategoryName) {
        this.componentCategoryName = componentCategoryName;
    }
}
