package com.bbb.core.model.request.tradein.fedex.detail;

import com.bbb.core.common.fedex.FedExValue;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Address {
    @JacksonXmlProperty(localName = "StreetLines")
    private String streetLines;
    @JacksonXmlProperty(localName = "City")
    private String city;
    @JacksonXmlProperty(localName = "StateOrProvinceCode")
    private String stateProvinceCode;
    @JacksonXmlProperty(localName = "PostalCode")
    private String postalCode;
    @JacksonXmlProperty(localName = "CountryCode")
    private String countryCode = FedExValue.COUNTRY_CODE_DEFAULT;

    public String getStreetLines() {
        return streetLines;
    }

    public void setStreetLines(String streetLines) {
        this.streetLines = streetLines;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    public void setStateProvinceCode(String stateProvinceCode) {
        this.stateProvinceCode = stateProvinceCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
