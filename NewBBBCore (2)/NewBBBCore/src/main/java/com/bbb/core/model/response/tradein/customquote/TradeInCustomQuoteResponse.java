package com.bbb.core.model.response.tradein.customquote;

import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class TradeInCustomQuoteResponse {
    @Id
    private long customQuoteId;
    private long tradeInId;
    private String employeeLocation;
    private LocalDateTime createdTime;
    private String employeeName;
//    @Transient
    private String titleBicycle;
    private String brandName;
    private String modelName;
    private String yearName;
    private StatusTradeInCustomQuote status;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Boolean isClean;
    private String partnerId;
    private String userCreatedId;
    private LocalDateTime lastUpdate;

    public long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public String getEmployeeLocation() {
        return employeeLocation;
    }

    public void setEmployeeLocation(String employeeLocation) {
        this.employeeLocation = employeeLocation;
    }

    public StatusTradeInCustomQuote getStatus() {
        return status;
    }

    public void setStatus(StatusTradeInCustomQuote status) {
        this.status = status;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTitleBicycle() {
        return titleBicycle;
    }

    public void setTitleBicycle(String titleBicycle) {
        this.titleBicycle = titleBicycle;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(String userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
