package com.bbb.core.model.request.sort;

public enum Sort {
    ASC("ASC"),
    DESC("DESC");
    private final String value;

    Sort(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Sort findByValue(String value) {
        for (Sort sort : Sort.values()) {
            if (sort.getValue().equals(value)) {
                return sort;
            }
        }
        return null;
    }
}
