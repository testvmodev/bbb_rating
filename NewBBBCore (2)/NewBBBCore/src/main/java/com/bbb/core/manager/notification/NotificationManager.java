package com.bbb.core.manager.notification;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.templaterest.RestNotificationManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.otherservice.request.mail.SendingMailRequest;
import com.bbb.core.model.otherservice.request.mail.auctionaccepted.BikeAuctionAcceptedRequest;
import com.bbb.core.model.otherservice.request.mail.auctionaccepted.ContentAuctionAcceptedRequest;
import com.bbb.core.model.otherservice.request.mail.auctionaccepted.ListingAuctionAcceptedRequest;
import com.bbb.core.model.otherservice.request.mail.auctionexpired.BikeAuctionExpiredRequest;
import com.bbb.core.model.otherservice.request.mail.auctionexpired.ContentAuctionExpiredRequest;
import com.bbb.core.model.otherservice.request.mail.auctionexpired.ListingAuctionExpiredRequest;
import com.bbb.core.model.otherservice.request.mail.customquote.complete.ContentCustomQuoteComplete;
import com.bbb.core.model.otherservice.request.mail.listingexpired.ContentListingExpiredRequest;
import com.bbb.core.model.otherservice.request.mail.listingexpired.ListingExpiredInfo;
import com.bbb.core.model.otherservice.request.mail.listingexpirewarn.BikeListingExpireWarn;
import com.bbb.core.model.otherservice.request.mail.listingexpirewarn.ContentListingExpireWarnRequest;
import com.bbb.core.model.otherservice.request.mail.listingexpirewarn.ListingExpireWarnInfo;
import com.bbb.core.model.otherservice.request.mail.listingshipped.BikeListingShipped;
import com.bbb.core.model.otherservice.request.mail.listingshipped.ContentListingShipped;
import com.bbb.core.model.otherservice.request.mail.listingshipped.ListingInfoShipped;
import com.bbb.core.model.otherservice.request.mail.listingshipped.OrderItemShipped;
import com.bbb.core.model.otherservice.request.mail.offer.buyeraccepted.ContentOfferBuyerAccepted;
import com.bbb.core.model.otherservice.request.mail.offer.buyercountered.ContentOfferBuyerCountered;
import com.bbb.core.model.otherservice.request.mail.offer.buyerreject.ContentOfferBuyerRejected;
import com.bbb.core.model.otherservice.request.mail.offer.content.*;
import com.bbb.core.model.otherservice.request.mail.offer.counted.ContentMailOfferCounted;
import com.bbb.core.model.otherservice.request.mail.offer.expireaccept.ContentMailOfferExpireAccept;
import com.bbb.core.model.otherservice.request.mail.offer.expireaccept.ListingExpireRequest;
import com.bbb.core.model.otherservice.request.mail.offer.made.ContentOfferMadeRequest;
import com.bbb.core.model.otherservice.request.mail.offer.reject.ContentOfferRejected;
import com.bbb.core.model.otherservice.request.mail.offer.rejectedaccepted.ContentOfferRejectAccepted;
import com.bbb.core.model.otherservice.request.mail.scorecard.complete.ContentScorecardComplete;
import com.bbb.core.model.otherservice.request.mail.startauction.AuctionStartRequest;
import com.bbb.core.model.otherservice.request.mail.startauction.ContentStartAuctionRequest;
import com.bbb.core.model.otherservice.request.mail.startauction.ListingStartAuctionRequest;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.otherservice.response.shipping.ShipmentResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailResponse;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionResponse;
import com.bbb.core.model.schedule.OfferInventoryAuction;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class NotificationManager {
    @Autowired
    private RestNotificationManager restNotificationManager;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private InventoryRepository inventoryRepository;

    public SendingMailResponse sendMailOfferAccepted(Offer offer, long inventoryId, String imageInventory, String bicycleName, String listingPrice) {
        return sendMailOfferAccepted(offer.getBuyerId(), offer.getOfferPrice(), inventoryId, imageInventory, bicycleName, listingPrice);
    }

    public SendingMailResponse sendMailOfferAccepted(String buyerId, float offerPrice, long inventoryId,
                                                     String imageInventory, String bicycleName, String listingPrice) {
        UserDetailResponse userResponse = authManager.getUser(buyerId);
        SendingMailRequest<ContentMailRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(buyerId, userResponse.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_ACCEPTED);
        request.getMailTo().add(userResponse.getEmail());

        ContentMailRequest contentMailRequest = new ContentMailRequest();
        contentMailRequest.setUser(new UserMailRequest(userResponse.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setAskingPrice(listingPrice + "");
        listing.setId(inventoryId);
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(bicycleName);
        bike.setImageUrl(imageInventory);
        listing.setBike(bike);
        contentMailRequest.setListing(listing);
        OfferMailRequest offerMailRequest = new OfferMailRequest(offerPrice + "");
        contentMailRequest.setOffer(offerMailRequest);
        BuyerMailRequest buyer = new BuyerMailRequest(userResponse.getEmail());
        contentMailRequest.setBuyer(buyer);
        contentMailRequest.setSeller(new SellerMailRequest(IntegratedServices.BICYCLE_BOOT_NAME));
        contentMailRequest.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + inventoryId);
        contentMailRequest.setUrlTransactionComplete(IntegratedServices.BASE_WEB_APP + IntegratedServices.CART);
        request.setContentMail(contentMailRequest);

        return restNotificationManager.postObject(IntegratedServices.URL_SEND_MAIL, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), request, SendingMailResponse.class);
    }

    public SendingMailResponse sendMailAuctionAccepted(OfferInventoryAuction offerInventoryAuction) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(offerInventoryAuction.getBuyerId());
        SendingMailRequest<ContentAuctionAcceptedRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(offerInventoryAuction.getBuyerId(), userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_AUCTION_ACCEPTED);
        request.getMailTo().add(userFullInfo.getEmail());

        ContentAuctionAcceptedRequest contentAuctionAcceptedRequest = new ContentAuctionAcceptedRequest();
        contentAuctionAcceptedRequest.setLinkCard(IntegratedServices.getPathViewCard());
        ListingAuctionAcceptedRequest listingAuctionAccepted = new ListingAuctionAcceptedRequest();
        BikeAuctionAcceptedRequest bikeAuctionAcceptedRequest = new BikeAuctionAcceptedRequest();
        bikeAuctionAcceptedRequest.setBrandName(offerInventoryAuction.getBrandName());
        bikeAuctionAcceptedRequest.setModelName(offerInventoryAuction.getModelName());
        bikeAuctionAcceptedRequest.setYearName(offerInventoryAuction.getYearName());
        bikeAuctionAcceptedRequest.setTypeName(offerInventoryAuction.getTypeName());
        bikeAuctionAcceptedRequest.setSizeName(offerInventoryAuction.getSizeName());
        if (offerInventoryAuction.getStageNameInv() != null) {
            bikeAuctionAcceptedRequest.setStatus(StageInventory.SALE_PENDING.getValue());
        }
        if (offerInventoryAuction.getConditionInv() != null) {
            bikeAuctionAcceptedRequest.setCondition(offerInventoryAuction.getConditionInv().getValue());
        }
        bikeAuctionAcceptedRequest.setInventoryName(offerInventoryAuction.getInventoryName());
        bikeAuctionAcceptedRequest.setPrice(offerInventoryAuction.getOfferPrice());
        if (offerInventoryAuction.getBicycleName() != null) {
            bikeAuctionAcceptedRequest.setFullName(offerInventoryAuction.getBicycleName());
        } else {
            bikeAuctionAcceptedRequest.setFullName(offerInventoryAuction.getInventoryName());
        }
        bikeAuctionAcceptedRequest.setImage(offerInventoryAuction.getImageDefaultInv());
        listingAuctionAccepted.setBike(bikeAuctionAcceptedRequest);
        contentAuctionAcceptedRequest.setListing(listingAuctionAccepted);
        request.setContentMail(contentAuctionAcceptedRequest);


        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public SendingMailResponse sendMailStartAuction(
            Auction auction,
            List<InventoryAuctionResponse> inventoryAuctionResponses,
            List<String> emails) {
        ContentStartAuctionRequest contentStartAuctionRequest = new ContentStartAuctionRequest();
        AuctionStartRequest auctionStartRequest = new AuctionStartRequest();
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
        auctionStartRequest.setId(auction.getId());
        auctionStartRequest.setStartDate(format.format(auction.getStartDate().toDate()));
        auctionStartRequest.setEndDate(format.format(auction.getEndDate().toDate()));
        auctionStartRequest.setName(auction.getName());
        contentStartAuctionRequest.setAuction(auctionStartRequest);
        List<ListingStartAuctionRequest> listingStartAuctionRequests = new ArrayList<>();
        for (InventoryAuctionResponse inventoryAuction : inventoryAuctionResponses) {
            ListingStartAuctionRequest listingStartAuctionRequest = new ListingStartAuctionRequest();
            listingStartAuctionRequest.setAuctionInvId(inventoryAuction.getId() + "");
            listingStartAuctionRequest.setInvId(String.valueOf(inventoryAuction.getInventoryId()));
            listingStartAuctionRequest.setTitle(inventoryAuction.getTitle());
            listingStartAuctionRequest.setStartPricing(inventoryAuction.getMin());
            listingStartAuctionRequest.setImageUrl(inventoryAuction.getImageDefault());
            listingStartAuctionRequest.setListedPrice(Double.valueOf(inventoryAuction.getCurrentListedPrice()));
            listingStartAuctionRequest.setMsrcPrice(Double.valueOf(inventoryAuction.getMsrpPrice()));
            if (inventoryAuction.getBicycleName() != null) {
                listingStartAuctionRequest.setFullName(inventoryAuction.getBicycleName());
            } else {
                listingStartAuctionRequest.setFullName(inventoryAuction.getInventoryName());
            }
            listingStartAuctionRequests.add(listingStartAuctionRequest);
        }
        contentStartAuctionRequest.setListings(listingStartAuctionRequests);
        contentStartAuctionRequest.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.AUCTION);

        SendingMailRequest<ContentStartAuctionRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest("BBB id group", "Bicycle blue book"));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_AUCTION_STARTED);
        request.setContentMail(contentStartAuctionRequest);
        request.setBcc(emails);
        String subject = IntegratedServices.SUBJECT_START_AUCTION;
        subject = subject.replace(IntegratedServices.PATH_AUCTION_NAME, auction.getName());
        request.setSubject(subject);

        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public SendingMailResponse sendMailExpireOfferAccepted(
            String userId, Long marketListingId, Long inventoryAuctionId, //Long inventoryAuctionId,
            float offerPrice, String imageInventory, String bicycleName
    ) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(userId);

        SendingMailRequest<ContentMailOfferExpireAccept> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(userId, userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_EXPIRE);
        request.getMailTo().add(userFullInfo.getEmail());
//        request.getMailTo().add("duc.nguyen@vmodev.com");

        ContentMailOfferExpireAccept content = new ContentMailOfferExpireAccept();
        content.setUser(new UserMailRequest(userFullInfo.getDisplayName()));
        ListingExpireRequest listing = new ListingExpireRequest();
        listing.setId(marketListingId != null ? marketListingId : inventoryAuctionId);
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(bicycleName);
        bike.setImageUrl(imageInventory);
        listing.setBike(bike);
        content.setListing(listing);

        OfferMailRequest offer = new OfferMailRequest(offerPrice + "");
        content.setOffer(offer);
        if (marketListingId!= null ){
            content.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + marketListingId);
        }else {
            content.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.AUCTION + "/" + inventoryAuctionId);
        }

        request.setContentMail(content);

        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }


    public SendingMailResponse sendMailAuctionExpired(OfferInventoryAuction offerInventoryAuction) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(offerInventoryAuction.getBuyerId());

        SendingMailRequest<ContentAuctionExpiredRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(offerInventoryAuction.getBuyerId(), userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_AUCTION_EXPIRE);

        request.getMailTo().add(userFullInfo.getEmail());

        ContentAuctionExpiredRequest content = new ContentAuctionExpiredRequest();
        content.setUser(new UserMailRequest(userFullInfo.getDisplayName()));
        ListingAuctionExpiredRequest listing = new ListingAuctionExpiredRequest();
        listing.setId(offerInventoryAuction.getInventoryId());
        listing.setAuction_inv_id(offerInventoryAuction.getInventoryAuctionId());
        BikeAuctionExpiredRequest bike = new BikeAuctionExpiredRequest();
        if (offerInventoryAuction.getBicycleName() != null) {
            bike.setFullName(offerInventoryAuction.getBicycleName());
        } else {
            bike.setFullName(offerInventoryAuction.getInventoryName());
        }
        bike.setInventoryName(offerInventoryAuction.getInventoryName());

        bike.setImageUrl(offerInventoryAuction.getImageDefaultInv());
        listing.setBike(bike);
        content.setListing(listing);

        OfferMailRequest offerMailRequest = new OfferMailRequest(offerInventoryAuction.getOfferPrice() + "");
        content.setOffer(offerMailRequest);
        content.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.AUCTION);
        request.setContentMail(content);

        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public SendingMailResponse sendMailListingExpireWarn(MarketListing marketListing, Inventory inventory) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(inventory.getSellerId());

        SendingMailRequest<ContentListingExpireWarnRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_LISTING_EXPIRE_WARN);
        request.getMailTo().add(userFullInfo.getEmail());
        request.setSubject(
                ValueCommons.YOUR_LISTING + " " + inventory.getTitle() + ValueCommons.LIStING_ID + " " +
                        ValueCommons.ADDRESS_EMAIL_BICYCLE_BLUE_BOOK + " " + ValueCommons.WILL_SOON_EXPIRE
        );

        ContentListingExpireWarnRequest content = new ContentListingExpireWarnRequest();
        content.setUser(new UserMailRequest(userFullInfo.getDisplayName()));
        content.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + marketListing.getId());

        ListingExpireWarnInfo listing = new ListingExpireWarnInfo();
        listing.setId(marketListing.getId());
        listing.setBike(new BikeListingExpireWarn(inventory.getImageDefault(), inventory.getTitle()));

        content.setListing(listing);
        request.setContentMail(content);

        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public SendingMailResponse sendMailListingExpired(MarketListing marketListing, Inventory inventory) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(inventory.getSellerId());

        SendingMailRequest<ContentListingExpiredRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_LISTING_EXPIRED);
        request.getMailTo().add(userFullInfo.getEmail());

        ContentListingExpiredRequest content = new ContentListingExpiredRequest();
        content.setUser(new UserMailRequest(userFullInfo.getDisplayName()));
        List<ListingExpiredInfo> listing = Collections.singletonList(
                new ListingExpiredInfo(marketListing.getId(), inventory.getImageDefault())
        );
        content.setListings(listing);

        request.setContentMail(content);

        return restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }


    public void sendEmailOfferAccepted(Inventory inventory, Offer offer) {
        String bicycleName = null;
        if (inventory.getTitle() != null) {
            bicycleName = inventory.getTitle();
        } else {
            Bicycle bicycle = bicycleRepository.findOneNotDelete(inventory.getBicycleId());
            if (bicycle != null) {
                bicycleName = bicycle.getName();
            }
        }
        String linkImage = inventory.getImageDefault();
        sendMailOfferAccepted(offer, inventory.getId(), linkImage, bicycleName,
                inventory.getCurrentListedPrice() == null ? null : inventory.getCurrentListedPrice().toString());
    }


    public void sendMailOfferRejectedAccepted(
            String buyerId,
            Long marketListingId,
            long inventoryId) {
        Inventory inventory = inventoryRepository.findOne(inventoryId);
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(buyerId);

        SendingMailRequest<ContentOfferRejectAccepted> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(buyerId, userFullInfo.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_REJECTED_ACCEPTED);
        request.getMailTo().add(userFullInfo.getEmail());


        ContentOfferRejectAccepted content = new ContentOfferRejectAccepted();
        content.setReason("Seller cancelled your offer.");
        content.setUser(new UserMailRequest(userFullInfo.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(marketListingId);
        BikeMailRequest bike = new BikeMailRequest();
        if (inventory != null) {
            bike.setFullName(inventory.getTitle());
            bike.setImageUrl(inventory.getImageDefault());
        }
        listing.setBike(bike);
        content.setListing(listing);
        content.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + marketListingId);
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public void sendMailOfferMade(String buyerId, Inventory inventory, long id, boolean isMarketListing, Float currentListedPrice, float offerPrice) {
        //todo miss send mail when seller is null
        if (inventory.getSellerId() == null && inventory.getStorefrontId() == null) {
            return;
        }
//        Inventory inventory = inventoryRepository.findOne(inventoryId);


        SendingMailRequest<ContentOfferMadeRequest> request = new SendingMailRequest<>();
        request.setExchangeType("default");

        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_MADE);


        ContentOfferMadeRequest content = new ContentOfferMadeRequest();
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(id);
        if (currentListedPrice != null) {
            listing.setAskingPrice(currentListedPrice + "");
        }
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);
        content.setBasePath(
                isMarketListing ?
                        IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + id :
                        IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.AUCTION + "/" + id
        );
        content.setOffer(new OfferMailRequest(offerPrice + ""));
        request.setContentMail(content);
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(buyerId);
        if (inventory.getStorefrontId() != null) {

        } else {
            UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());
            request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfoSeller.getDisplayName()));
            request.getMailTo().add(userFullInfoSeller.getEmail());
            content.setUser(new UserMailRequest(userFullInfoSeller.getDisplayName()));

            content.setBuyer(new BuyerMailRequest(userFullInfoBuyer.getEmail()));

            restNotificationManager.postObject(
                    IntegratedServices.URL_SEND_MAIL,
                    Constants.HEADER_SECRET_KEY,
                    ConfigDataSource.getSecretKey(),
                    request,
                    SendingMailResponse.class
            );
        }


    }

    public void sendMailOfferRejected(Offer offer, Inventory inventory, boolean isForBuyer) {
        //todo miss send mail when seller is null
        if (inventory.getSellerId() == null) {
            return;
        }
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());

        SendingMailRequest<ContentOfferRejected> request = new SendingMailRequest<>();
        request.setExchangeType("default");
        request.setReceive(new ReceiveSendingMailRequest(isForBuyer ? offer.getBuyerId() : inventory.getSellerId(), isForBuyer ? userFullInfoBuyer.getDisplayName() : userFullInfoSeller.getDisplayName()));
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_REJECT);
        request.getMailTo().add(isForBuyer ? userFullInfoBuyer.getEmail() : userFullInfoSeller.getEmail());

        ContentOfferRejected content = new ContentOfferRejected();
        content.setUser(new UserMailRequest(isForBuyer ? userFullInfoBuyer.getDisplayName() : userFullInfoSeller.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(offer.getMarketListingId() != null ? offer.getMarketListingId() : offer.getInventoryAuctionId());
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);
        content.setSeller(new SellerMailRequest(userFullInfoSeller.getEmail()));
        content.setBasePath(
                offer.getMarketListingId() != null ?
                        IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId() :
                        IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.AUCTION + "/" + offer.getInventoryAuctionId()
        );
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }


    public void sendMailOfferCounted(Offer offer, Inventory inventory) {
        //todo miss send mail when seller is null
        if (inventory.getSellerId() == null) {
            return;
        }
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());

        SendingMailRequest<ContentMailOfferCounted> request = new SendingMailRequest<>();
        request.setExchangeType("default");
        request.setReceive(new ReceiveSendingMailRequest(offer.getBuyerId(), userFullInfoBuyer.getDisplayName()));
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_COUNTERED);
        request.getMailTo().add(userFullInfoBuyer.getEmail());

        ContentMailOfferCounted content = new ContentMailOfferCounted();
        content.setUser(new UserMailRequest(userFullInfoBuyer.getDisplayName()));
        content.setOffer(new OfferMailRequest(offer.getOfferPrice() + ""));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(offer.getMarketListingId());
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        listing.setAskingPrice(inventory.getCurrentListedPrice() + "");
        content.setListing(listing);
        content.setBasePath(
                IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId()
        );
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public void sendMailOfferBuyerRejected(Offer offer, Inventory inventory) {
        if (inventory.getSellerId() == null) {
            return;
        }
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());

        SendingMailRequest<ContentOfferBuyerRejected> request = new SendingMailRequest<>();
        request.setExchangeType("default");
        request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfoSeller.getDisplayName()));
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_BUYER_REJECT);
        request.getMailTo().add(userFullInfoSeller.getEmail());

        ContentOfferBuyerRejected content = new ContentOfferBuyerRejected();
        content.setUser(new UserMailRequest(userFullInfoSeller.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(offer.getMarketListingId());
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);
        content.setBuyer(new BuyerMailRequest(userFullInfoBuyer.getEmail()));
        content.setOffer(new OfferMailRequest(offer.getOfferPrice() + ""));
        content.setBasePath(
                IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId()
        );
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    @SuppressWarnings("Duplicates")
    public void sendMailOfferBuyerAccepted(Offer offer, Inventory inventory){
        if ( inventory.getSellerId() == null ){
            return;
        }
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());

        SendingMailRequest<ContentOfferBuyerAccepted> request = new SendingMailRequest<>();
        request.setExchangeType("default");
        request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfoSeller.getDisplayName()));
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_BUYER_ACCEPTED);
        request.getMailTo().add(userFullInfoSeller.getEmail());


        ContentOfferBuyerAccepted content = new ContentOfferBuyerAccepted();
        content.setUser(new UserMailRequest(userFullInfoSeller.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(offer.getMarketListingId());
        listing.setAskingPrice(inventory.getCurrentListedPrice()+"");
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);
        content.setOffer(new OfferMailRequest(offer.getOfferPrice() + ""));
        content.setBuyer(new BuyerMailRequest(userFullInfoBuyer.getDisplayName()));
        content.setBasePath(
                IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId()
        );
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    public void sendMailOfferBuyerCountered(Offer offer, Inventory inventory){
        if ( inventory.getSellerId() == null ){
            return;
        }
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());

        SendingMailRequest<ContentOfferBuyerCountered> request = new SendingMailRequest<>();
        request.setExchangeType("default");
        request.setReceive(new ReceiveSendingMailRequest(inventory.getSellerId(), userFullInfoSeller.getDisplayName()));
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_BUYER_COUNTERED);
        request.getMailTo().add(userFullInfoSeller.getEmail());


        ContentOfferBuyerCountered content = new ContentOfferBuyerCountered();
        content.setUser(new UserMailRequest(userFullInfoSeller.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setId(offer.getMarketListingId());
        listing.setAskingPrice(inventory.getCurrentListedPrice()+"");
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);
        content.setOffer(new OfferMailRequest(offer.getOfferPrice() + ""));
        content.setBasePath(
                IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId()
        );
        request.setContentMail(content);
        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }


    public void sendMailOfferAcceptedNormal(Offer offer, Inventory inventory) {
        if ( inventory.getSellerId() == null ){
            return;
        }
        UserDetailFullResponse userFullInfoSeller = authManager.getUserFullInfo(inventory.getSellerId());
        UserDetailFullResponse userFullInfoBuyer = authManager.getUserFullInfo(offer.getBuyerId());

        SendingMailRequest<ContentMailRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(offer.getBuyerId(), userFullInfoBuyer.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_OFFER_ACCEPTED);
        request.getMailTo().add(userFullInfoBuyer.getEmail());

        ContentMailRequest contentMailRequest = new ContentMailRequest();
        contentMailRequest.setUser(new UserMailRequest(userFullInfoBuyer.getDisplayName()));
        ListingMailRequest listing = new ListingMailRequest();
        listing.setAskingPrice(inventory.getCurrentListedPrice() + "");
        listing.setId(offer.getMarketListingId());
        BikeMailRequest bike = new BikeMailRequest();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        contentMailRequest.setListing(listing);
        OfferMailRequest offerMailRequest = new OfferMailRequest(offer.getOfferPrice() + "");
        contentMailRequest.setOffer(offerMailRequest);
        BuyerMailRequest buyer = new BuyerMailRequest(userFullInfoBuyer.getEmail());
        contentMailRequest.setBuyer(buyer);
        contentMailRequest.setSeller(new SellerMailRequest(userFullInfoSeller.getEmail()));
        contentMailRequest.setBasePath(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + offer.getMarketListingId());
        contentMailRequest.setUrlTransactionComplete(IntegratedServices.BASE_WEB_APP + IntegratedServices.CART);
        request.setContentMail(contentMailRequest);

        restNotificationManager.postObject(IntegratedServices.URL_SEND_MAIL, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), request, SendingMailResponse.class);
    }

    @Async
    public void sendMailListingShippedAsync(String buyerId, MarketListing marketListing, Inventory inventory, ShipmentResponse shipment) {
        UserDetailFullResponse buyer = authManager.getUserFullInfo(buyerId);

        SendingMailRequest<ContentListingShipped> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(buyerId, buyer.getDisplayName()));
        request.setExchangeType("default");
        request.setTemplateKey(IntegratedServices.TEMPLATE_LISTING_SHIPPED);
        request.getMailTo().add(buyer.getEmail());

        ContentListingShipped content = new ContentListingShipped();
        content.setUser(new UserMailRequest(buyer.getDisplayName()));

        ListingInfoShipped listing = new ListingInfoShipped();
        listing.setId(marketListing.getId());
        BikeListingShipped bike = new BikeListingShipped();
        bike.setFullName(inventory.getTitle());
        bike.setImageUrl(inventory.getImageDefault());
        listing.setBike(bike);
        content.setListing(listing);

        content.setBasePath(
                IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE + IntegratedServices.BUY_NOW + "/" + marketListing.getId());

        OrderItemShipped orderItem = new OrderItemShipped();
        orderItem.setCarrier(shipment.getCarrierType() != null ? shipment.getCarrierType().getValue() : shipment.getManualCarrier());
        orderItem.setTracking(shipment.getTrackingNumber());
        content.setOrderItem(orderItem);

        request.setContentMail(content);

        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    @Async
    public void sendMailScorecardCompleteAsync(
            TradeIn tradeIn, TradeInShipping tradeInShipping,
            List<String> emails
    ) {
        UserDetailFullResponse user = authManager.getUserFullInfo(tradeIn.getUserCreatedId());
        if (emails == null) {
            emails = new ArrayList<>();
        }

        ContentScorecardComplete content = new ContentScorecardComplete();
        content.setTpName(user.getPartner().getName());
        content.setBikeName(tradeIn.getTitle());
        content.setDeliveryMethod(tradeInShipping.getShippingType().getValue());

        SendingMailRequest<ContentScorecardComplete> request = new SendingMailRequest<>();
        request.setExchangeType(IntegratedServices.DEFAULT_MAIL_EXCHANGE_TYPE);
        request.setReceive(new ReceiveSendingMailRequest(Constants.BBB_ID_GROUP, Constants.BICYCLE_BLUE_BOOK));
        request.setTemplateKey(IntegratedServices.TEMPLATE_SCORECARD_AVAILABLE);
        request.setContentMail(content);
        request.setBcc(emails);

        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }

    @Async
    public void sendMailCustomQuoteCompleteAsync(
            TradeInCustomQuote tradeInCustomQuote, TradeIn tradeIn,
            List<String> emails
    ) {
        UserDetailFullResponse user = authManager.getUserFullInfo(tradeIn.getUserCreatedId());

        ContentCustomQuoteComplete content = new ContentCustomQuoteComplete();
        content.setTpName(user.getPartner().getName());
        content.setBikeName(tradeIn.getTitle());
        content.setEmployeeEmails(tradeInCustomQuote.getEmployeeEmail());

        SendingMailRequest<ContentCustomQuoteComplete> request = new SendingMailRequest<>();
        request.setExchangeType(IntegratedServices.DEFAULT_MAIL_EXCHANGE_TYPE);
        request.setReceive(new ReceiveSendingMailRequest(Constants.BBB_ID_GROUP, Constants.BICYCLE_BLUE_BOOK));
        request.setTemplateKey(IntegratedServices.TEMPLATE_CUSTOM_QUOTE_AVAILABLE);
        request.setContentMail(content);
//        request.setMailTo(Collections.singletonList(user.getEmail()));
        request.setBcc(emails);

        restNotificationManager.postObject(
                IntegratedServices.URL_SEND_MAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                SendingMailResponse.class
        );
    }
}
