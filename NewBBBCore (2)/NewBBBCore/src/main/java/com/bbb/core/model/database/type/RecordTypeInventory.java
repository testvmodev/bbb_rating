package com.bbb.core.model.database.type;

public enum RecordTypeInventory {
    BIKE("bike"),
    COMPONENT("component");

    private final String value;

    RecordTypeInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static RecordTypeInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "bike":
                return BIKE;
            case "component":
                return COMPONENT;
            default:
                return null;
        }
    }
}
