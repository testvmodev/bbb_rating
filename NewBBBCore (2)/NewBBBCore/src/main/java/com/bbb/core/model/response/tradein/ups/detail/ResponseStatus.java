package com.bbb.core.model.response.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseStatus {
    @JsonProperty("Code")
    private Long code;

    public Long getCode() {
        return code;
    }
}
