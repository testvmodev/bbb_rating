package com.bbb.core.model.otherservice.request.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CartStagesRequest {
    @JsonProperty("inventory_ids")
    private List<Long> inventoryIds;
    private String user;

    public CartStagesRequest() {}

    public CartStagesRequest(List<Long> inventoryIds, String user) {
        this.inventoryIds = inventoryIds;
        this.user = user;
    }

    public List<Long> getInventoryIds() {
        return inventoryIds;
    }

    public void setInventoryIds(List<Long> inventoryIds) {
        this.inventoryIds = inventoryIds;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
