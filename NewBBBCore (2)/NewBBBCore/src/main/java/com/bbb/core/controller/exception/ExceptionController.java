package com.bbb.core.controller.exception;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.config.http.CacheServletInputStream;
import com.bbb.core.common.config.http.CacheServletRequestWrapper;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.log.LogManager;
import com.bbb.core.model.database.LogExceptionApi;
import com.bbb.core.model.request.log.LogServiceRequest;
import com.bbb.core.model.request.log.RawData;
import com.bbb.core.model.request.log.RawHeaders;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.common.LogExceptionApiRepository;
import org.apache.catalina.connector.ClientAbortException;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;

//ResponseEntityExceptionHandler
@ControllerAdvice
public class ExceptionController implements MessageResponses {
    private static final String HOST_BBB_CORE = "BBB-Core-Service";
    @Autowired
    private AppConfig appConfig;

    @Autowired
    private LogExceptionApiRepository logExceptionApiRepository;

    @Autowired
    private LogManager logManager;

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity handleWrongHttpMethod(HttpServletRequest req, HttpRequestMethodNotSupportedException ex) {
        ObjectError objectError = new ObjectError(
                ObjectError.ERROR_REST_TEMPLATE,
                ex.getMessage()
        );
        objectError.setTime(LocalDateTime.now());
        objectError.setPath(req.getServletPath());
        return new ResponseEntity(objectError, HttpStatus.METHOD_NOT_ALLOWED);
    }

    //    IllegalStateException
    @ExceptionHandler(ExceptionResponse.class)
    public final ResponseEntity handleExceptionResponse(HttpServletRequest req, ExceptionResponse ex) {
        ex.getErrorObject().setTime(LocalDateTime.now());
        ex.getErrorObject().setPath(req.getServletPath());

        return new ResponseEntity<>(ex.getErrorObject(), ex.getStatus());
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public final ResponseEntity handleNumberFormatException(HttpServletRequest req, MethodArgumentTypeMismatchException ex) {
        ObjectError objectError = new ObjectError(
                ObjectError.ERROR_PARAM,
                FORMAT_VALUE_INVALID + ". Parameter: " + ex.getName() + (ex.getValue() == null ? "" : ", value: " + ex.getValue())
        );
        objectError.setTime(LocalDateTime.now());
        objectError.setPath(req.getServletPath());
        postLogService(req, objectError, HttpStatus.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(objectError, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public final ResponseEntity handleMethodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException manve) {
        List<FieldError> fieldErrors = manve.getBindingResult().getFieldErrors();

        for (FieldError fieldError : fieldErrors) {
            System.out.println(fieldError.getField());
            ObjectError objectError = new ObjectError(ObjectError.ERROR_PARAM, fieldError.getField() + " is required");
            objectError.setTime(LocalDateTime.now());
            objectError.setPath(request.getServletPath());
            postLogService(request, objectError, HttpStatus.UNPROCESSABLE_ENTITY);
            return new ResponseEntity<>(objectError, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        ObjectError objectError = new ObjectError(ObjectError.ERROR_PARAM, "Error param");
        objectError.setTime(LocalDateTime.now());
        objectError.setPath(request.getServletPath());

        postLogService(request, objectError, HttpStatus.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(objectError, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public final ResponseEntity handleMissingServletRequestParameterException(HttpServletRequest request, MissingServletRequestParameterException ex) {
        ObjectError objectError = new ObjectError(ObjectError.ERROR_PARAM, ex.getMessage());
        objectError.setTime(LocalDateTime.now());
        objectError.setPath(request.getServletPath());

        postLogService(request, objectError, HttpStatus.UNPROCESSABLE_ENTITY);
        return new ResponseEntity<>(objectError, HttpStatus.UNPROCESSABLE_ENTITY);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ObjectError> exceptionHandler(HttpServletRequest req, Exception ex) {
        ObjectError error = new ObjectError(ObjectError.ERROR_PARAM, "The request could not be understood by the server due to malformed syntax.");
        error.setTime(LocalDateTime.now());
        error.setPath(req.getServletPath());
        ex.printStackTrace();

        postLogService(req, error, HttpStatus.BAD_REQUEST);
        canSaveExceptionWhenException(req, error.getPath(), ex);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ObjectError> httpMessageNotReadableException(HttpServletRequest req, HttpMessageNotReadableException ex) {
        ObjectError error = new ObjectError(ObjectError.ERROR_PARAM, ex.getMessage() + " (Input data invalid)");
        error.setTime(LocalDateTime.now());
        error.setPath(req.getServletPath());
        ex.printStackTrace();
        postLogService(req, error, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ObjectError> httpDataIntegrityViolationException(HttpServletRequest req, DataIntegrityViolationException ex) {
        ObjectError error = new ObjectError(ObjectError.ERROR_PARAM, ex.getMessage());
        error.setTime(LocalDateTime.now());
        error.setPath(req.getServletPath());
        ex.printStackTrace();
        postLogService(req, error, HttpStatus.INTERNAL_SERVER_ERROR);
        canSaveExceptionWhenException(req, req.getServletPath(), ex);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);

    }
    @ExceptionHandler(HttpServerErrorException.class)
    public ResponseEntity<ObjectError> httpServerErrorException(HttpServletRequest req, HttpServerErrorException ex) { //DataIntegrityViolationException
        ObjectError error = new ObjectError(ObjectError.ERROR_PARAM, ex.getMessage());
        error.setTime(LocalDateTime.now());
        error.setPath(req.getServletPath());
        ex.printStackTrace();
        postLogService(req, error, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    private void postLogService(HttpServletRequest req, ObjectError obError, HttpStatus status) {
        if (appConfig.getEnableServiceLog() == null || !appConfig.getEnableServiceLog()){
            return;
        }
        LogServiceRequest request = new LogServiceRequest();
        request.setEnv(appConfig.getEnv().getValue());
        request.setFacility("java");
        request.setHost(HOST_BBB_CORE);
        request.setShortMessage(obError.getMessage());
        switch (status) {
            case BAD_REQUEST:
                request.setLevel(Constants.LOG_400);
                break;
            case CONFLICT:
                request.setLevel(Constants.LOG_409);
                break;
            case INTERNAL_SERVER_ERROR:
                request.setLevel(Constants.LOG_500);
                break;
            default:
                request.setLevel(Constants.LOG_400);
                break;
        }
        request.setPath(obError.getPath());
        request.setStatus(status.value());
        request.setRawResponse(obError);
        String query = req.getQueryString();
        String body = getRequestBody(req);
        request.setRawHeaders(new RawHeaders(req.getRemoteAddr(), req.getHeader("User-Agent")));

        request.setRawData(new RawData(body, query));
        logManager.postLog(request);
    }

    private void canSaveExceptionWhenException(HttpServletRequest req, String pathApi, Exception e){
        if (appConfig.getLogApiException() != null && appConfig.getLogApiException()){
            saveLogException(req, pathApi, e);
        }
    }

    private void saveLogException(HttpServletRequest req, String pathApi, Exception e){
        if (e instanceof ClientAbortException) {
            //not server fault
            return;
        }
        try {
            String query = req.getQueryString();
            String body = getRequestBody(req);

            LogExceptionApi logExceptionApi = new LogExceptionApi();
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logExceptionApi.setContent(errors.toString());
            logExceptionApi.setPath(pathApi);
            logExceptionApi.setExceptionName(e.getClass().getSimpleName());
            logExceptionApi.setQuery(query);
            logExceptionApi.setBody(body);
            logExceptionApiRepository.save(logExceptionApi);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getRequestBody(HttpServletRequest req) {
        String body = null;
        try {
            ServletInputStream in = req.getInputStream();
            if (in instanceof CacheServletInputStream) {
                CacheServletInputStream cache = (CacheServletInputStream) in;
                body = cache.getContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }
}
