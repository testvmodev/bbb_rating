package com.bbb.core.repository.market;

import com.bbb.core.model.database.MarketListingExpire;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MarketListingExpireRepository extends CrudRepository<MarketListingExpire, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM market_listing_expire " +
            "WHERE id = :id"
    )
    MarketListingExpire findOne(
            @Param("id") long id
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "market_listing_expire.last_mail_expired, " +
            "market_listing_expire.last_mail_expire_soon, " +
            "market_listing_expire.expired_fail_count, " +
            "market_listing_expire.expire_soon_fail_count " +
            "FROM market_listing " +

            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
//            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
//                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "LEFT JOIN market_listing_expire ON market_listing.id = market_listing_expire.id " +

            "WHERE market_listing.is_delete = 0 " +
            "AND is_expirable = 1 " +
            "AND ((is_renew IS NULL AND :isRenew = 0) OR is_renew = :isRenew) " +
            "AND market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} " +
            "AND DATEDIFF(HOUR, market_listing.time_listed, GETDATE()) >= (:expireTime - 24) " + //before 1 day
            "AND (last_mail_expire_soon IS NULL " +
            "OR DATEDIFF(HOUR, last_mail_expire_soon, GETDATE()) > (:expireTime - 24))" //old mail sent of previous listing time
    )
    List<MarketListingExpire> findExpireSoon(
            @Param("expireTime") float expireTime,
            @Param("isRenew") boolean isRenew
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "market_listing_expire.last_mail_expired, " +
            "market_listing_expire.last_mail_expire_soon, " +
            "market_listing_expire.expired_fail_count, " +
            "market_listing_expire.expire_soon_fail_count " +
            "FROM market_listing " +

            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
//            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
//                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "LEFT JOIN market_listing_expire ON market_listing.id = market_listing_expire.id " +

            "WHERE market_listing.is_delete = 0 " +
            "AND is_expirable = 1 " +
            "AND ((is_renew IS NULL AND :isRenew = 0) OR is_renew = :isRenew) " +
            "AND market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} " +
            "AND DATEDIFF(HOUR, time_listed, GETDATE()) >= :expireTime " +
            "AND (last_mail_expired IS NULL " +
            "OR DATEDIFF(HOUR, last_mail_expired, GETDATE()) > :expireTime)" //old mail sent of previous listing time
    )
    List<MarketListingExpire> findExpired(
            @Param("expireTime") float expireTime,
            @Param("isRenew") boolean isRenew
    );
}
