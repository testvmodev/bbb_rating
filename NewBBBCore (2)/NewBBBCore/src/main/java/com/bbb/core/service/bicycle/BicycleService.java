package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycle.*;
import org.springframework.data.domain.Pageable;

public interface BicycleService {
    Object getBicycles(BicycleGetRequest request) throws ExceptionResponse;

    Object deleteBicycle(long id) throws ExceptionResponse;

    Object createBicycle(BicycleCreateRequest request) throws ExceptionResponse;

    Object updateBicycle(long bicycleId, BicycleUpdateRequest bicycleUpdateRequest) throws ExceptionResponse;

    Object getBicycleDetail(long bicycleId) throws ExceptionResponse;

    Object getBicyclesFromBrandYearModel(BicycleFromBrandYearModelRequest request, Pageable page) throws ExceptionResponse;

    Object getBicyclesFromContent(String content, Pageable page);

    void importBicycleFromOlbDb(long offset, long size);

    Object getAllCondition() throws ExceptionResponse;

    Object removeImageIncorrect();

    Object getBicycleSearchBaseComponent() throws ExceptionResponse;

    Object getBicycleRecommends(long bicycleId) throws ExceptionResponse;

    Object getBicycleYearFromModel(long brandId, long modelId) throws ExceptionResponse;

    Object getValueGuidePrices(long brandId, long modelId, long yearId) throws ExceptionResponse;
}
