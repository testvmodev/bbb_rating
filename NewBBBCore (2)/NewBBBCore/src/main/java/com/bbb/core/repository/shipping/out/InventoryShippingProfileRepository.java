package com.bbb.core.repository.shipping.out;

import com.bbb.core.model.response.shipping.InventoryShippingProfileResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryShippingProfileRepository extends JpaRepository<InventoryShippingProfileResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "shipping_fee_config.* , " +
            "inventory.id AS inventory_id " +
            "FROM inventory " +
            "JOIN bicycle_type ON inventory.bicycle_type_name = bicycle_type.name " +
            "JOIN shipping_fee_config ON bicycle_type.id = shipping_fee_config.bicycle_type_id " +
            "WHERE inventory.id IN :#{@queryUtils.isEmptyList(#inventoryIds) ? @queryUtils.fakeLongs() : #inventoryIds}"
    )
    List<InventoryShippingProfileResponse> findAll(
            @Param("inventoryIds") List<Long> inventoryIds
    );
}
