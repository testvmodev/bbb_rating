package com.bbb.core.controller.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.request.tradein.tradin.ShippingAccountCreateRequest;
import com.bbb.core.service.tradein.ShippingAccountService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShippingAccountController {
    @Autowired
    private ShippingAccountService service;

    @GetMapping(Constants.URL_SHIPPING_ACCOUNT)
    public ResponseEntity getAccounts() {
        return ResponseEntity.ok(service.getAccounts());
    }

    @PostMapping(Constants.URL_SHIPPING_ACCOUNT)
    public ResponseEntity addAccount(
            @RequestParam("carrierType") CarrierType carrierType,
            @ApiParam(value = "UPS only", required = false)
            @RequestParam(value = "username", required = false) String username,
            @ApiParam(value = "UPS only", required = false)
            @RequestParam(value = "password", required = false) String password,
            @RequestParam("apiKey") String apiKey,
            @ApiParam(value = "FedEx only", required = false)
            @RequestParam(value = "apiPassword", required = false) String apiPassword,
            @RequestParam("accountNumber") String accountNumber,
            @ApiParam(value = "FedEx only", required = false)
            @RequestParam(value = "meterNumber", required = false) String meterNumber,
            @RequestParam("serviceCode") String serviceCode,
            @RequestParam("phone") String phone,
            @RequestParam("warehouse") String warehouseId,
            @RequestParam("isSandbox") boolean isSandbox
    ) throws ExceptionResponse {
        ShippingAccountCreateRequest request = new ShippingAccountCreateRequest();
        request.setCarrierType(carrierType);
        request.setUsername(username);
        request.setPassword(password);
        request.setApiKey(apiKey);
        request.setApiPassword(apiPassword);
        request.setAccountNumber(accountNumber);
        request.setMeterNumber(meterNumber);
        request.setServiceCode(serviceCode);
        request.setPhone(phone);
        request.setWarehouseId(warehouseId);
        request.setSandbox(isSandbox);

        return ResponseEntity.ok(service.addAccount(request));
    }

    @DeleteMapping(Constants.URL_SHIPPING_ACCOUNT_PATH)
    public ResponseEntity deleteAccount(@PathVariable(Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteAccount(id));
    }

    @PatchMapping(Constants.URL_SHIPPING_ACCOUNT_ACTIVATE)
    public ResponseEntity activate(@PathVariable(Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.activate(id));
    }
}
