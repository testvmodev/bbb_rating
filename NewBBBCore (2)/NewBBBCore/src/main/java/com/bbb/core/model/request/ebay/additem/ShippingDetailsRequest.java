package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingDetailsRequest {
    @JacksonXmlProperty(localName = "CalculatedShippingRate")
    private CalculatedShippingRate calculatedShippingRate;
    @JacksonXmlProperty(localName = "PaymentInstructions")
    private String paymentInstructions;
    @JacksonXmlProperty(localName = "SalesTax")
    private SalesTax salesTax;
    @JacksonXmlProperty(localName = "ShippingServiceOptions")
    private ShippingServiceOptions shippingServiceOptions;
    @JacksonXmlProperty(localName = "ShippingType")
    private String shippingType;

    public CalculatedShippingRate getCalculatedShippingRate() {
        return calculatedShippingRate;
    }

    public void setCalculatedShippingRate(CalculatedShippingRate calculatedShippingRate) {
        this.calculatedShippingRate = calculatedShippingRate;
    }

    public String getPaymentInstructions() {
        return paymentInstructions;
    }

    public void setPaymentInstructions(String paymentInstructions) {
        this.paymentInstructions = paymentInstructions;
    }

    public SalesTax getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(SalesTax salesTax) {
        this.salesTax = salesTax;
    }

    public ShippingServiceOptions getShippingServiceOptions() {
        return shippingServiceOptions;
    }

    public void setShippingServiceOptions(ShippingServiceOptions shippingServiceOptions) {
        this.shippingServiceOptions = shippingServiceOptions;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }
}
