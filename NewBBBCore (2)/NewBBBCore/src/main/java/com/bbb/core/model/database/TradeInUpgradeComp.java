package com.bbb.core.model.database;

import com.bbb.core.model.database.type.TypeTradeInUpgradeComp;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_upgrade_comp")
public class TradeInUpgradeComp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private float percentValue;
    private boolean isUp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPercentValue() {
        return percentValue;
    }

    public void setPercentValue(float percentValue) {
        this.percentValue = percentValue;
    }

    public boolean isUp() {
        return isUp;
    }

    public void setUp(boolean up) {
        isUp = up;
    }
}
