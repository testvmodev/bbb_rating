package com.bbb.core.manager.schedule;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.schedule.ScheduleUtils;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.ScheduleJobConfig;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.request.schedule.ScheduleJobConfigUpdateRequest;
import com.bbb.core.model.request.schedule.detail.JobConfigCronSupport;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.schedule.ScheduleJobConfigDetailResponse;
import com.bbb.core.repository.schedule.ScheduleJobConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ScheduleJobConfigManager {
    @Autowired
    private ScheduleManager scheduleManager;
    @Autowired
    private ScheduleJobConfigRepository scheduleJobConfigRepository;

    public Object getJobConfigs(Boolean isEnable) {
        return scheduleJobConfigRepository.findAll();
    }

    public Object getRunningJobs() {
        return scheduleManager.getRunningJobConfig();
    }

    public Object getInqueueJobs() {
        return scheduleManager.getInqueueJob().stream()
                .map(job -> {
                    ScheduleJobConfigDetailResponse res = new ScheduleJobConfigDetailResponse(job.getConfig());
                    res.setQueueTime(job.getLastQueueTime());
                    res.setNextRun(job.getLastQueueToRunAt());
                    return res;
                }).collect(Collectors.toList());
    }

    public Object getJobDetail(long id) throws ExceptionResponse {
        ScheduleJobConfig config = findJobConfig(id);
        ScheduleJob job = scheduleManager.getJob(config.getJobType());
        if (job == null) {
            config.setDelete(true);
            scheduleJobConfigRepository.save(config);
            throw new ExceptionResponse(
                    ObjectError.ERROR_DELETE,
                    MessageResponses.JOB_CONFIG_OUTDATED,
                    HttpStatus.NOT_FOUND
            );
        }

        ScheduleJobConfigDetailResponse response = new ScheduleJobConfigDetailResponse(config);
        if (config.isEnable()) {
            response.setRunning(job.isRunning);
            response.setQueueTime(job.getLastQueueTime());
            if (job.isRunning) {
                response.setStartTime(job.startTime);
            } else {
                response.setNextRun(job.getLastQueueToRunAt());
            }
        }
        return response;
    }

    public Object updateJobConfig(long id, ScheduleJobConfigUpdateRequest request) throws ExceptionResponse {
        ScheduleJobConfig config = findJobConfig(id);
        String errorMessage = null;

        if (request.getEnable() != null) {
            config.setEnable(request.getEnable());
        }
        if (request.getJobTimeType() != null) {
            config.setJobTimeType(request.getJobTimeType());
        }
        if (request.getJobTimeSeconds() != null) {
            config.setJobTimeSeconds(request.getJobTimeSeconds());
        }
        if (request.getLogFailEnable() != null) {
            config.setLogFailEnable(request.getLogFailEnable());
        }
        if (request.getLogCancelEnable() != null) {
            config.setLogCancelEnable(request.getLogCancelEnable());
        }
        if (request.getLogSuccessEnable() != null) {
            config.setLogSuccessEnable(request.getLogSuccessEnable());
        }
        if (request.getJobTimeCron() != null) {
            request.getJobTimeCron().validate();

            if (request.getJobTimeCron().getFullCronString() != null) {
                config.setJobTimeCron(request.getJobTimeCron().getFullCronString());
            } else if (!request.getJobTimeCron().getNoCronBuilder()) {
                JobConfigCronSupport support = request.getJobTimeCron().getSupport();
                config.setJobTimeCron(ScheduleUtils.buildCron(
                        support.getSeconds(),
                        support.getMinutes(),
                        support.getHours(),
                        support.getDaysOfWeek(),
                        support.getDaysOfMonth(),
                        support.getMonths()
                ));
            }
        }

        if (config.getJobTimeType() == JobTimeType.CRON) {
            if (config.getJobTimeCron() == null ||
                    !CronSequenceGenerator.isValidExpression(config.getJobTimeCron())) {
                errorMessage = MessageResponses.NO_VALID_CRON;
            }
        } else {
            if (config.getJobTimeSeconds() == null || config.getJobTimeSeconds() < 60) {
                errorMessage = MessageResponses.JOB_TIME_SECOND_INVALID;
            }
        }

        if (errorMessage != null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    errorMessage,
                    HttpStatus.BAD_REQUEST
            );
        }

        config = scheduleJobConfigRepository.save(config);
        scheduleManager.updateConfig(config);

        return config;
    }

    private ScheduleJobConfig findJobConfig(long id) throws ExceptionResponse {
        ScheduleJobConfig config =  scheduleJobConfigRepository.findById(id).orElse(null);
        if (config == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.JOB_CONFIG_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return config;
    }
}
