package com.bbb.core.model.request.market;

import com.bbb.core.model.database.type.*;
import com.bbb.core.model.request.filter.ListingSellerType;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.request.sort.Sort;

import java.util.List;

public class MarketListingRequest {
    private List<Long> bicycleIds;
    private List<Long> modelIds;
    private List<Long> brandIds;
    private List<Long> typeIds;
    private List<String> sizeNames;
    private Long startYearId;
    private Long endYearId;
    private List<String> typeBicycleNames;
    private Float startPrice;
    private Float endPrice;
    private List<String> frameMaterialNames;
    private List<String> brakeTypeNames;
    private StatusMarketListing statusMarketListing;
    private MarketType marketType;
    private String name;
    private List<ConditionInventory> conditions;
    private String searchContent;
    private Sort sortType;
    private InventorySortField sortField;
    private List<String> genders;
    private List<String> suspensions;
    private List<String> wheelSizes;
    private ListingSellerType sellerType;
    private String zipCode;
    private String cityName;
    private String state;
    private String country;
    private Float latitude;
    private Float longitude;
    private Float radius;
    private String content;
    private String storefrontId;
    private List<Long> marketListingIds;

    public List<Long> getBicycleIds() {
        return bicycleIds;
    }

    public void setBicycleIds(List<Long> bicycleIds) {
        this.bicycleIds = bicycleIds;
    }

    public List<Long> getModelIds() {
        return modelIds;
    }

    public void setModelIds(List<Long> modelIds) {
        this.modelIds = modelIds;
    }

    public List<Long> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Long> brandIds) {
        this.brandIds = brandIds;
    }

    public List<String> getSizeNames() {
        return sizeNames;
    }

    public void setSizeNames(List<String> sizeNames) {
        this.sizeNames = sizeNames;
    }

    public Long getStartYearId() {
        return startYearId;
    }

    public void setStartYearId(Long startYearId) {
        this.startYearId = startYearId;
    }

    public Long getEndYearId() {
        return endYearId;
    }

    public void setEndYearId(Long endYearId) {
        this.endYearId = endYearId;
    }

    public List<String> getTypeBicycleNames() {
        return typeBicycleNames;
    }

    public void setTypeBicycleNames(List<String> typeBicycleNames) {
        this.typeBicycleNames = typeBicycleNames;
    }

    public List<String> getFrameMaterialNames() {
        return frameMaterialNames;
    }

    public void setFrameMaterialNames(List<String> frameMaterialNames) {
        this.frameMaterialNames = frameMaterialNames;
    }

    public List<String> getBrakeTypeNames() {
        return brakeTypeNames;
    }

    public void setBrakeTypeNames(List<String> brakeTypeNames) {
        this.brakeTypeNames = brakeTypeNames;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }


    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ConditionInventory> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionInventory> conditions) {
        this.conditions = conditions;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public InventorySortField getSortField() {
        return sortField;
    }

    public void setSortField(InventorySortField sortField) {
        this.sortField = sortField;
    }

    public List<Long> getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(List<Long> typeIds) {
        this.typeIds = typeIds;
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    public List<String> getSuspensions() {
        return suspensions;
    }

    public void setSuspensions(List<String> suspensions) {
        this.suspensions = suspensions;
    }

    public List<String> getWheelSizes() {
        return wheelSizes;
    }

    public void setWheelSizes(List<String> wheelSizes) {
        this.wheelSizes = wheelSizes;
    }

    public ListingSellerType getSellerType() {
        return sellerType;
    }

    public void setSellerType(ListingSellerType sellerType) {
        this.sellerType = sellerType;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getRadius() {
        return radius;
    }

    public void setRadius(Float radius) {
        this.radius = radius;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public List<Long> getMarketListingIds() {
        return marketListingIds;
    }

    public void setMarketListingIds(List<Long> marketListingIds) {
        this.marketListingIds = marketListingIds;
    }
}
