package com.bbb.core.common;

import java.util.Arrays;
import java.util.List;

public interface ValueCommons {
    long INVENTORY_SIZE_ID = 193;
    String INVENTORY_SIZE_NAME = "Size";
    long INVENTORY_BRAKE_TYPE_ID = 180;
    String INVENTORY_BRAKE_TYPE_NAME = "Brake Type";
    long INVENTORY_FRAME_MATERIAL_ID = 11;
    String INVENTORY_FRAME_MATERIAL_NAME = "Frame Material";
    String INVENTORY_FRAME_SIZE_NAME = "Frame Size";
    String INVENTORY_WHEEL_SIZE_NAME = "Wheel Size";
    long INVENTORY_COLOR_ID = 183;
    String INVENTORY_COLOR_NAME = "Color";

    String INVENTORY_SHIFTERS_NAME = "Shifters";
    String INVENTORY_FRONT_DERAILLEUR_NAME = "Front Derailleur";
    String INVENTORY_REAR_DERAILLEUR_NAME = "Rear Derailleur";
    String INVENTORY_FRONT_BRAKE_NAME = "Front Brake";
    String INVENTORY_REAR_BRAKE_NAME = "Rear Brake";
    String INVENTORY_CRANKSET_NAME = "Crankset";
    String INVENTORY_FRONT_SOCK_NAME = "Front Shock";
    String INVENTORY_REAR_SOCK_NAME = "Rear Shock";
    String INVENTORY_WHEELS_NAME = "Wheels";
    String INV_COMP_HANDLEBAR_NAME = "Handlebar";
    String INV_COMP_STEM_NAME = "Stem";
    String INV_COMP_CASSETTE_NAME = "Cassette";
    String INV_COMP_FORK_NAME = "Fork";
    String INV_COMP_VALUE_RIGID = "rigid";
    String INV_COMP_VALUE_SHOCK = "shock";

    String SIZE_NAME_INV_COMP = "Size";
    String SUSPENSION_NAME_INV_COMP = "Suspension";
    String FRAME_MATERIAL_NAME_INV_COMP = "Frame Material";
    String BRAKE_TYPE_NAME_INV_COMP = "Brake Type";
    String BOTTOM_BRACKET_NAME_INV_COMP = "Bottom Bracket";
    String GENDER_NAME_INV_COMP = "Gender";
    String COLOR_INV_COMP = "Color";
    String HEAD_SET_INV_COMP = "Headset";
    String TYPE_BICYCLE_INV = "Type bicycle";
    String SIZE_BICYCLE_INV = "Type bicycle";
    String BRAND = "Brand";
    String MODEL = "Model";
    String YEAR = "Year";
    String CURRENT_LISTED_PRICE = "Current listed price";
    String CONDITION = "Condition";
    String PARTNER = "Partner";

    String SELLER_ID_DEFAULT = "5ce251129b79920012a7ba4a"; //display name: BicycleBlueBook
    String LOCATION_DEFAULT = "95133";


    String S_WORKS = "S-WORKS";
    String MEN_S = "MEN'S";
    String WOMEN_S = "WOMEN'S";
    String BOY_S = "BOY'S";
    String GIRL_S = "GIRL'S";


    String BICYCLE_TYPE_E_BIKE = "E-Bike";
    String BICYCLE_TYPE_MOUNTAIN = "Mountain";
    String BICYCLE_TYPE_HYBRID = "Hybrid";
    String BICYCLE_TYPE_ROAD = "Road";

    String COMPONENT_CATEGORY_SUSPENSION = "Suspension";
    String COMPONENT_CATEGORY_FRAMESET = "Frameset";


    String PTP = "PTP";
    String PTP_V1 = "P2P";
    String B2C = "B2C";
    String BBV2 = "BBV2";
    String SHOW_ITEM_WITHOUT_ERRORS = "Show items without errors";
    String SHOW_ITEM_IN_ERRORS = "Show items in errors";


    //deal recommend
    int MAX_SIZE_DEAL_RECOMMEND = 16;

    String CART_TYPE_OFFER = "offer";
    String CART_TYPE_AUCTION = "auction";


    List<String> EMAIL_ADMINS = Arrays.asList(
            "thuy.ho@vmodev.com"
    ); //for production env: dealersupport@bicyclebluebook.com

    String IN_SHOPPING_CART = "IN_SHOPPING_CART";
    String NOT_IN_CART = "NOT_IN_CART";
    String PAY = "PAY";
    String YOUR_LISTING = "Your listing";
    String LIStING_ID = "listing ID:";
    String WILL_SOON_EXPIRE = "will soon expire.";
    String ADDRESS_EMAIL_BICYCLE_BLUE_BOOK = "BicycleBlueBook.com";
    String FOR_SALE = "For sale";
    String KEY_ITEM_ID_PATCH = "{item_id}";
    String LINK_EBAY_ITEM_VIEW = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item={item_id}#ht_500wt_1156";
}
