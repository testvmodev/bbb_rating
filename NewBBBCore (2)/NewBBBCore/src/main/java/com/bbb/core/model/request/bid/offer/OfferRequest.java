package com.bbb.core.model.request.bid.offer;

import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.request.sort.OfferSort;
import com.bbb.core.model.request.sort.Sort;

public class OfferRequest {
//    private String buyerId;
    private StatusOffer status;
    private Sort sort;
    private OfferSort fieldSort;
    private Long inventoryAuctionId;
    private Long marketListingId;


//    public OfferRequest(String buyerId,  StatusOffer status, Long auctionId, Long inventoryId) {
//        this.buyerId = buyerId;
//        this.status = status;
//        this.auctionId = auctionId;
//        this.inventoryId = inventoryId;
//    }

    public OfferRequest(StatusOffer status, Long inventoryAuctionId, Long marketListingId, Sort sort, OfferSort fieldSort) {
//        this.buyerId = buyerId;
        this.status = status;
        this.inventoryAuctionId = inventoryAuctionId;
        this.marketListingId = marketListingId;
        this.sort = sort;
        this.fieldSort = fieldSort;
    }

//    public String getBuyerId() {
//        return buyerId;
//    }
//
//    public void setBuyerId(String buyerId) {
//        this.buyerId = buyerId;
//    }


    public StatusOffer getStatus() {
        return status;
    }

    public void setStatus(StatusOffer status) {
        this.status = status;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public OfferSort getFieldSort() {
        return fieldSort;
    }

    public void setFieldSort(OfferSort fieldSort) {
        this.fieldSort = fieldSort;
    }
}
