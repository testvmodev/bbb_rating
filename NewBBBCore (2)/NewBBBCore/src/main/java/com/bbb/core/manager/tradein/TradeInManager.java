package com.bbb.core.manager.tradein;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.fedex.FedExManager;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.common.utils.Single;
import com.bbb.core.common.utils.s3.S3DownloadImage;
import com.bbb.core.common.utils.s3.S3UploadImage;
import com.bbb.core.common.utils.s3.S3UploadResponse;
import com.bbb.core.common.utils.s3.S3UploadUtils;
import com.bbb.core.common.ups.UPSManager;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bicycle.BicycleComponentManager;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.shipping.ShipmentManager;
import com.bbb.core.manager.templaterest.RestValueGuideManager;
import com.bbb.core.manager.tradein.async.TradeInManagerAsync;
import com.bbb.core.manager.valueguide.ValueGuidePriceManager;
import com.bbb.core.model.database.embedded.TradeInCompDetailId;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.shipping.ShipmentDetailResponse;
import com.bbb.core.model.otherservice.response.shipping.ShipmentResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.otherservice.response.valueguide.PredictionPriceResponse;
import com.bbb.core.model.otherservice.response.valueguide.VGBaseResponse;
import com.bbb.core.model.otherservice.response.warehouse.WarehouseResponse;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.model.request.tradein.tradin.*;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.tradein.*;
import com.bbb.core.model.response.tradein.bicycle.ConditionTradeInBicycleResponse;
import com.bbb.core.model.response.tradein.bicycle.TradeInBicycleDetailResponse;
import com.bbb.core.model.response.tradein.customquote.TradeInCustomQuoteCommonRes;
import com.bbb.core.model.response.tradein.detail.detailstep.stepthree.ShippingBicycleBlueBookResponse;
import com.bbb.core.model.response.tradein.detail.detailstep.stepthree.ShippingMyAccountResponse;
import com.bbb.core.model.response.tradein.detail.detailstep.stepthree.TradeInStepThreeResponse;
import com.bbb.core.model.response.tradein.fedex.FedexDeleteShipResponse;
import com.bbb.core.model.response.tradein.fedex.FedexRateResponse;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.bicycle.BicycleBaseInfo;
import com.bbb.core.model.response.tradein.detail.detailstep.TradeInImageGroupResponse;
import com.bbb.core.model.response.tradein.detail.detailstep.TradeInImageResponse;
import com.bbb.core.model.response.tradein.detail.detailstep.TradeInStepOneTwoResponse;
import com.bbb.core.model.response.tradein.ups.UPSRateResponse;
import com.bbb.core.model.database.*;
import com.bbb.core.model.request.bicycle.BicycleFromBrandYearModelRequest;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.tradein.detail.TradeInDetailResponse;
import com.bbb.core.model.response.tradein.detail.TradeInOwner;
import com.bbb.core.model.response.tradein.detail.TradeInProof;
import com.bbb.core.model.response.tradein.ups.UPSVoidShipResponse;
import com.bbb.core.repository.BicycleImageRepository;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicycle.BicycleSpecRepository;
import com.bbb.core.repository.bicycle.out.BicycleComponentResponseRepository;
import com.bbb.core.repository.bicycle.out.InventoryBaseInfoRepository;
import com.bbb.core.repository.tradein.*;
import com.bbb.core.repository.tradein.out.*;
import com.bbb.core.repository.valueguide.PrivatePartyValueModifierRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TradeInManager extends TradeInBaseManager implements MessageResponses {
    //region Beans
    @Autowired
    private TradeInRepository tradeInRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private TradeInImageTypeRepository tradeInImageTypeRepository;
    @Autowired
    private TradeInImageRepository tradeInImageRepository;
    @Autowired
    private TradeInShippingRepository tradeInShippingRepository;
    @Autowired
    private UPSManager upsManager;
    @Autowired
    private BicycleSpecRepository bicycleSpecRepository;
    @Autowired
    private BicycleComponentResponseRepository bicycleComponentResponseRepository;
    @Autowired
    private TradeInBicycleYearResponseRepository tradeInBicycleYearResponseRepository;
    @Autowired
    private BicycleImageRepository bicycleImageRepository;
    @Autowired
    private TradeInPriceConfigRepository tradeInPriceConfigRepository;
    @Autowired
    private TradeInReasonDeclineRepository tradeInReasonDeclineRepository;
    @Autowired
    private TradeInDeclineDetailRepository tradeInDeclineDetailRepository;
    @Autowired
    private TraBicBaseInfoResponseRepository traBicBaseInfoResponseRepository;
    @Autowired
    private TradeInStatusRepository tradeInStatusRepository;
    @Autowired
    private TradeInUpgradeCompRepository tradeInUpgradeCompRepository;
    @Autowired
    private TradeInUpgradeCompDetailRepository tradeInUpgradeCompDetailRepository;
    @Autowired
    private ShippingAccountRepository shippingAccountRepository;
    @Autowired
    private FedExManager fedexManager;
    @Autowired
    private TradeInResponseRepository tradeInResponseRepository;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private InventoryBaseInfoRepository inventoryBaseInfoRepository;
    @Autowired
    private DeclineTradeInRepository declineTradeInRepository;
    @Autowired
    private TradeInImageResponseRepository tradeInImageResponseRepository;
    @Autowired
    private TradeInCustomQuoteRepository tradeInCustomQuoteRepository;
    @Autowired
    private InventoryBaseInfoRepository getInventoryBaseInfoRepository;
    @Autowired
    private TradeInManagerAsync tradeInManagerAsync;
    @Autowired
    private TradeInCancelRepository tradeInCancelRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private ValueGuidePriceManager valueGuidePriceManager;
    @Autowired
    private ShipmentManager shipmentManager;
    @Autowired
    private TradeInCustomQuoteManager tradeInCustomQuoteManager;
    @Autowired
    private TradeInCompDetailRepository tradeInCompDetailRepository;
    @Autowired
    private TradeInUpgradeCompModifierRepository tradeInUpgradeCompModifierRepository;
    @Autowired
    private PrivatePartyValueModifierRepository privatePartyValueModifierRepository;
    @Autowired
    private RestValueGuideManager restValueGuideManager;
    @Autowired
    private MyListingManager myListingManager;
    @Autowired
    @Qualifier("mvcValidator")
    private Validator validator;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private AwsResourceConfig awsResourceConfig;
    @Autowired
    private BicycleComponentManager bicycleComponentManager;
    @Autowired
    private NotificationManager notificationManager;
    //endregion


    public Object postTradeIn(long tradeInId, List<MultipartFile> images, boolean isSave) throws ExceptionResponse {
        TradeIn tradeIn = findTradeInCheckExist(tradeInId);

        validateRoleForTradeIn(tradeIn);

        if (tradeIn.getStatusId() != StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue() &&
                tradeIn.getStatusId() != StatusTradeIn.SHIPPING.getValue() &&
                tradeIn.getStatusId() != StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED.getValue()
        ) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_TRADE_IN_INVALID),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeIn.isCancel()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CANCELED),
                    HttpStatus.BAD_REQUEST
            );
        }

        List<TradeInImageType> tradeInImageTypesRequireFull = tradeInImageTypeRepository.findAllImageRequire();
        List<TradeInImageType> tradeInImageTypesNotRequireFull = tradeInImageTypeRepository.findAllImageNotRequire();

        if (images == null || images.size() == 0) {
            if (!isSave) {
                List<Long> tradeInImageIdsUpload = tradeInImageRepository.getAllTradeImageImageUpload(tradeIn.getId());
                if (tradeInImageTypesRequireFull.size() > tradeInImageIdsUpload.size()) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_MUST_UPLOAD_IMAGE_REQUIRE),
                            HttpStatus.BAD_REQUEST
                    );
                }
                tradeIn.setStatusId((long) StatusTradeIn.SHIPPING.getValue());
//                List<TradeInImage> newTradeInImages = tradeInImageRepository.findAllByTradeInId(tradeInId);
//                inventoryManager.updateInventoryStepThreeAsync(newTradeInImages);
                return tradeInRepository.save(tradeIn);
            }
            if (isSave) {
                tradeIn.setSave(true);
                tradeIn = tradeInRepository.save(tradeIn);
                return tradeIn;
            }
        } else {
            List<Long> tradeInImagesUploaded = tradeInImageRepository.getAllTradeImageImageUpload(tradeIn.getId());
            if (tradeInImagesUploaded.size() + images.size() > Constants.MAX_TRADE_IN_IMAGE) {
                throw new ExceptionResponse(
                        ObjectError.INVALID_ACTION,
                        MAX_IMAGE_UPLOAD_EXCEED,
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        List<TradeInImageType> imageTypes = new ArrayList<>();
        imageTypes.addAll(tradeInImageTypesRequireFull);
        imageTypes.addAll(tradeInImageTypesNotRequireFull);
        Random rd = new Random();

        List<S3UploadImage<Long>> uploads = new ArrayList<>();
        for (MultipartFile multipartFile : images) {
            int index = rd.nextInt(imageTypes.size());
            S3UploadImage<Long> uploadImage = new S3UploadImage<>(Constants.TRADE_IN_FOLDER + "_" + tradeIn.getId() + "_" + imageTypes.get(index).getId(),
                    S3Value.FOLDER_TRADEIN_ORIGINAL, multipartFile, imageTypes.get(index).getId());
            uploads.add(uploadImage);
        }


        List<S3UploadResponse<Long>> s3UploadResponses = S3UploadUtils.uploads(s3Manager, uploads);
        List<TradeInImage> newTradeInImages = new ArrayList<>();
        for (S3UploadResponse<Long> s3UploadRespons : s3UploadResponses) {
            TradeInImage tradeInImage = new TradeInImage();
            tradeInImage.setTradeInId(tradeInId);
            tradeInImage.setImageTypeId(s3UploadRespons.getOtherData());
            tradeInImage.setFullLink(S3UploadUtils.getFullLinkS3(awsResourceConfig, s3UploadRespons.getFullKey()));
            tradeInImage.setKeyHash(s3UploadRespons.getFullKey());
            newTradeInImages.add(tradeInImage);
        }
        tradeInImageRepository.saveAll(newTradeInImages);

        if (tradeIn.getStatusId() != StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED.getValue()) {
            if (isSave) {
                tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue());
            } else {
                List<Long> imageRequestUploads = tradeInImageRepository.getAllTradeImageImageUpload(tradeInId);
                if (tradeInImageTypesRequireFull.size() > imageRequestUploads.size()) {
                    tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue());
                } else {
                    tradeIn.setStatusId((long) StatusTradeIn.SHIPPING.getValue());
//                    List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInId(tradeInId);
//                    inventoryManager.updateInventoryStepThreeAsync(tradeInImages);
                }
            }
            tradeIn.setSave(isSave);
            tradeInRepository.save(tradeIn);
        }
//        tradeIn.setStatusId((long) StatusTradeIn.SHIPPING.getValue());

        if (!isSave) {
            List<Long> tradeInImageIdsUpload = tradeInImageRepository.getAllTradeImageImageUpload(tradeIn.getId());
            if (tradeInImageTypesRequireFull.size() > tradeInImageIdsUpload.size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_MUST_UPLOAD_IMAGE_REQUIRE),
                        HttpStatus.BAD_REQUEST
                );
            }
            tradeIn.setStatusId((long) StatusTradeIn.SHIPPING.getValue());
            return tradeInRepository.save(tradeIn);
        }
        if (isSave) {
            tradeIn.setSave(true);
            tradeIn = tradeInRepository.save(tradeIn);
            return tradeIn;
        }
//        only after step 4
//        inventoryManager.updateInventoryStepThreeAsync(newTradeInImages);

        return newTradeInImages;
    }


    public S3DownloadImage getTradeInImage(String keyHash) throws ExceptionResponse {
        try {
            if (tradeInImageRepository.findByKeyHash(keyHash) == null) {
                throw new IOException("Unknown key");
            }

            S3Object s3Object = s3Manager.download(keyHash);
            S3ObjectInputStream s3InputStream = s3Object.getObjectContent();
            BufferedImage s3Image = ImageIO.read(s3InputStream);
            ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
            MediaType mediaType;

            String contentType = s3Object.getObjectMetadata().getContentType();
            if (contentType.equals((MediaType.IMAGE_PNG_VALUE))) {
                ImageIO.write(s3Image, S3Value.PNG_EXTENTION, byteOutput);
                mediaType = MediaType.IMAGE_PNG;
            } else if (contentType.equals(MediaType.IMAGE_JPEG_VALUE) || contentType.equals(S3Value.IMAGE_COMMON_TYPE)) {
                ImageIO.write(s3Image, S3Value.JPG_EXTENTION, byteOutput);
                mediaType = MediaType.IMAGE_JPEG;
            } else {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.FORMAT_TYPE, IMAGE_EXTENSION_NOT_SUPPORT),
                        HttpStatus.BAD_REQUEST
                );
            }
            return new S3DownloadImage(byteOutput.toByteArray(), mediaType);
        } catch (AmazonS3Exception | IOException e) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, IMAGE_NOT_FOUND),
                    HttpStatus.NOT_FOUND
            );
        }
    }

    public Object calcShipping(TradeInCalculateShipRequest request) throws ExceptionResponse {
        TradeInShippingAccount shippingAccount = shippingAccountRepository.findActiveAccount();
        if (shippingAccount.getCarrierType() == CarrierType.UPS) {
            WarehouseResponse warehouseResponse = authManager.getWarehouseDetail(shippingAccount.getWarehouseId(), CommonUtils.getUserToken());

            UPSRateResponse upsResponse = upsManager.rateShip(request, shippingAccount, warehouseResponse);
            if (!upsResponse.isSuccess()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.UNKNOWN_ERROR, upsResponse.getFaultMessage()),
                        HttpStatus.BAD_REQUEST
                );
            }

            TradeInCalculateShipResponse response = new TradeInCalculateShipResponse();
            response.setCarrierType(shippingAccount.getCarrierType());
            response.setTotalCharge(Float.valueOf(upsResponse.getRatedTotalCharge().getMonetaryValue()));
            response.setCurrency(upsResponse.getRatedTotalCharge().getCurrencyCode());
            return response;
        } else if (shippingAccount.getCarrierType() == CarrierType.FEDEX) {
            WarehouseResponse warehouseResponse = authManager.getWarehouseDetail(shippingAccount.getWarehouseId(), CommonUtils.getUserToken());

            FedexRateResponse fedexResponse = fedexManager.rateShip(request, shippingAccount, warehouseResponse);
            if (!fedexResponse.isSuccess()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.UNKNOWN_ERROR, fedexResponse.getNotifications().get(0).getMessage()),
                        HttpStatus.BAD_REQUEST
                );
            }

            TradeInCalculateShipResponse response = new TradeInCalculateShipResponse();
            response.setCarrierType(shippingAccount.getCarrierType());
            response.setTotalCharge(fedexResponse.getTotalCharge().getAmount());
            response.setCurrency(fedexResponse.getTotalCharge().getCurrency());
            return response;
        } else {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.UNKNOWN_ERROR, CARRIER_NOT_SUPPORT),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    public Object createShipping(TradeInShippingCreateRequest request) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(request.getTradeInId());
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

        validateRoleForTradeIn(tradeIn);

        if (tradeIn.getStatusId() != StatusTradeIn.SHIPPING.getValue()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_STATUS_MUST_IS_SHIPPING),
                    HttpStatus.NOT_FOUND
            );
        }
        if (tradeIn.isCancel()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CANCELED),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (!request.isSave() && request.getShippingType() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, SHIPPING_TYPE_MUST_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }

        verifyImageToCreateShipping(tradeIn);
        TradeInShipping tradeInShipping = tradeInShippingRepository.findOneByTradeInId(tradeIn.getId());
        if (tradeInShipping == null) {
            tradeInShipping = new TradeInShipping();
        }

        tradeInShipping.setTradeInId(request.getTradeInId());
        if (request.getShippingType() != null) {
            UserDetailFullResponse userDetailFullResponse = null;

            switch (request.getShippingType()) {
                case BICYCLE_BLUE_BOOK_TYPE:
                    request.getBlueBook().checkValid();

                    String userId = CommonUtils.getUserLogin();
                    userDetailFullResponse = authManager.getUserFullInfo(userId);
                    if (userDetailFullResponse.getPartner() == null ||
                            userDetailFullResponse.getPartner().getPhone() == null) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.NO_PERMISSION, NOT_PARTNER_OR_NO_PHONE),
                                HttpStatus.FORBIDDEN
                        );
                    }

                    tradeInShipping.setLength(request.getBlueBook().getLength());
                    tradeInShipping.setWidth(request.getBlueBook().getWidth());
                    tradeInShipping.setHeight(request.getBlueBook().getHeight());
                    tradeInShipping.setWeight(request.getBlueBook().getWeight());
                    tradeInShipping.setFromAddress(request.getBlueBook().getFromAddress());
                    tradeInShipping.setFromCity(request.getBlueBook().getFromCity());
                    tradeInShipping.setFromState(request.getBlueBook().getFromState());
                    tradeInShipping.setFromZipCode(request.getBlueBook().getFromZipCode());

                    tradeInShipping.setScheduled(request.getBlueBook().getScheduled());
                    tradeInShipping.setScheduleDate(request.getBlueBook().getScheduleDate());
                    break;
                case MY_ACCOUNT_TYPE:
                    request.getMyAccount().checkValid();

                    tradeInShipping.setCarrier(request.getMyAccount().getCarrier());
                    tradeInShipping.setCarrierTrackingNumber(request.getMyAccount().getCarrierTrackingNumber());
                    break;
            }

            if (!request.isSave()) {
                ShipmentResponse shipmentResponse = shipmentManager.createInboundShipment(request, userDetailFullResponse);
            }
        } else {
            if (request.getBlueBook() != null) {
                tradeInShipping.setLength(request.getBlueBook().getLength());
                tradeInShipping.setWidth(request.getBlueBook().getWidth());
                tradeInShipping.setHeight(request.getBlueBook().getHeight());
                tradeInShipping.setWeight(request.getBlueBook().getWeight());

                tradeInShipping.setFromAddress(request.getBlueBook().getFromAddress());
                tradeInShipping.setFromCity(request.getBlueBook().getFromCity());
                tradeInShipping.setFromState(request.getBlueBook().getFromState());
                tradeInShipping.setFromZipCode(request.getBlueBook().getFromZipCode());
            }
            if (request.getMyAccount() != null) {
                tradeInShipping.setCarrier(request.getMyAccount().getCarrier());
                tradeInShipping.setCarrierTrackingNumber(request.getMyAccount().getCarrierTrackingNumber());
            }
        }

        tradeInShipping.setShippingType(request.getShippingType());

        tradeInShipping = tradeInShippingRepository.save(tradeInShipping);
        if (!request.isSave()) {
            tradeIn.setStatusId((long) StatusTradeIn.COMPLETED.getValue());
        } else {
            tradeIn.setStatusId((long) StatusTradeIn.SHIPPING.getValue());
        }
        tradeIn.setSave(request.isSave());
        tradeIn = tradeInRepository.save(tradeIn);

//        inventoryManager.updateInventoryStepFourAsync(tradeIn, tradeInShipping);
        tradeInManagerAsync.createInventoryFromScorecard(tradeIn, tradeInShipping);
        notificationManager.sendMailScorecardCompleteAsync(tradeIn, tradeInShipping, ValueCommons.EMAIL_ADMINS);

        return tradeInShipping;
    }

    private byte[] rotateCloseWise(byte[] b) {
        InputStream in = new ByteArrayInputStream(b);
        try {
            BufferedImage img = ImageIO.read(in);

            double rads = Math.toRadians(-90);
            double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
            int w = img.getWidth();
            int h = img.getHeight();
            int newWidth = (int) Math.floor(w * cos + h * sin);
            int newHeight = (int) Math.floor(h * cos + w * sin);

            BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = rotated.createGraphics();
            AffineTransform at = new AffineTransform();
            at.translate((newWidth - w) / 2, (newHeight - h) / 2);

            int x = w / 2;
            int y = h / 2;

            at.rotate(rads, x, y);
            g2d.setTransform(at);
            g2d.drawImage(img, 0, 0, null);
            g2d.setColor(Color.RED);
            g2d.drawRect(0, 0, newWidth - 1, newHeight - 1);
            g2d.dispose();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(rotated, "png", baos);
            baos.flush();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    private void verifyImageToCreateShipping(TradeIn tradeIn) throws ExceptionResponse {
        List<TradeInImageType> tradeInImageTypes = tradeInImageTypeRepository.findAllImageRequire();
        List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInId(tradeIn.getId());
        if (tradeInImages.size() < tradeInImageTypes.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_MUST_UPLOAD_IMAGE_REQUIRE),
                    HttpStatus.NOT_FOUND
            );

        }
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getTradeInBicycleDetail(BicycleFromBrandYearModelRequest request) throws ExceptionResponse {
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
        Bicycle bicycle = bicycleRepository.findOneByBrandModelYear(
                request.getBrandId(), request.getModelId(), request.getYearId(),
                true
        );
        if (bicycle == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

        TradeInBicycleDetailResponse response = new TradeInBicycleDetailResponse();
        response.setBicycleId(bicycle.getId());
        response.setMsrp(bicycle.getRetailPrice());
        response.setImageDefault(bicycle.getImageDefault());
        Single<List<Pair<ConditionInventory, Float>>> listValueHolder = new Single<>();
        boolean isSucss = Observable.zip(
                CommonUtils.getOb(() ->
                        bicycleSpecRepository.findAllByBicycleId(bicycle.getId())
                                .stream()
                                .map(CommonUtils::convertToSpecResponse).collect(Collectors.toList())
                ),
                CommonUtils.getOb(() ->
                        bicycleComponentManager.findAllByBicycleId(bicycle.getId())
                ),
                CommonUtils.getOb(() -> bicycleImageRepository.finAll(bicycle.getId())
                        .stream().map(BicycleImage::getUri).collect(Collectors.toList())
                ),
                CommonUtils.getOb(() ->
                        tradeInBicycleYearResponseRepository.findAll(request.getBrandId(), request.getModelId())
                                .stream().filter(bi -> !(bicycle.getId() == bi.getBicycleId())).collect(Collectors.toList())
                ),
                CommonUtils.getOb(() -> tradeInUpgradeCompRepository.findAll()),
                CommonUtils.getOb(() -> valueGuidePriceManager.getValueGuidePrices(
                        bicycle.getBrandId(), bicycle.getModelId(), bicycle.getYearId(),
                        bicycle.getRetailPrice()
                )),
                CommonUtils.getOb(() -> tradeInUpgradeCompModifierRepository.findAll()),
                (specs, coms, images, other, up, valueGuidePrices, upgradeCompModifiers) -> {
                    response.setSpec(specs);
                    response.setComponents(coms);
                    response.setImages(images);
                    if (StringUtils.isBlank(response.getImageDefault()) && response.getImages().size() > 0) {
                        response.setImageDefault(response.getImages().get(0));
                    }
                    if (other.size() > 0) {
                        other.sort((o1, o2) -> Long.compare(o2.getYearId(), o1.getYearId()));
                    }
                    response.setOtherBicycles(other);


                    response.setUpgradeComps(up.stream().map(o -> {
                        TradeInUpgradeCompResponse re = new TradeInUpgradeCompResponse();
                        re.setId(o.getId());
                        re.setName(o.getName());
                        re.setPercentValue(o.getPercentValue());
                        re.setUp(o.isUp());
                        return re;
                    }).collect(Collectors.toList()));
                    listValueHolder.setValue(valueGuidePrices);
                    response.setUpgradeCompModifiers(upgradeCompModifiers);
                    return true;
                }
        ).blockingFirst();

        if (listValueHolder.getValue() != null && listValueHolder.getValue().size() > 0) {
            response.setConditions(new ArrayList<>());
            List<TradeInPriceConfig> tradeInPriceConfigs = tradeInPriceConfigRepository.findAll();
            Map<ConditionInventory, PrivatePartyValueModifier> modifiers = privatePartyValueModifierRepository.findAll()
                    .stream()
                    .collect(Collectors.toMap(PrivatePartyValueModifier::getName, mod -> mod));
            BicycleBaseInfo baseInfo = inventoryBaseInfoRepository.findOneFromBicycleId(bicycle.getId());
            VGBaseResponse<PredictionPriceResponse> vgResponse = null;
            try {
                vgResponse = restValueGuideManager.getPredictionPrice(
                        baseInfo.getBicycleBrandName(),
                        baseInfo.getBicycleModelName(),
                        baseInfo.getBicycleYearName(),
                        bicycle.getRetailPrice()
                );
            } catch (HttpStatusCodeException e) {
                e.printStackTrace();
            }


            for (Pair<ConditionInventory, Float> conditionInventoryFloatPair : listValueHolder.getValue()) {
                PrivatePartyValueModifier modifier = modifiers.get(conditionInventoryFloatPair.getFirst());
                Float ppv = null;
                if (modifier != null && vgResponse != null
                        && vgResponse.getData() != null && vgResponse.getData().getPrediction() != null
                ) {
                    ppv = vgResponse.getData().getPrediction() * (1 + modifier.getValueModifier() / 100);
                }

                ConditionTradeInBicycleResponse condition = new ConditionTradeInBicycleResponse(
                        conditionInventoryFloatPair.getFirst(),
                        TradeInUtil.getPercent(conditionInventoryFloatPair.getFirst()),
                        TradeInUtil.getMessage(conditionInventoryFloatPair.getFirst()),
                        conditionInventoryFloatPair.getSecond()
                );
                condition.setTradeInPriceConfigs(
                        TradeInUtil.getPriceConfig(
                                tradeInPriceConfigs.stream().map(TradeInPriceConfig::clone).collect(Collectors.toList()),
                                condition.getTradeInValue(),
                                ppv
                ));


                response.getConditions().add(condition);
            }

        } else {
            response.setConditions(null);
        }

        return response;
    }


    public Object getAllTradeInReasonDecline() {
        return tradeInReasonDeclineRepository.findAll();
    }

    public Object declineTradeIn(DeclineTradeInRequest request) throws ExceptionResponse {
        if (request.getPriceSuggest() <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, RETAIL_PRICE__MUST_POSITIVE),
                    HttpStatus.NOT_FOUND
            );
        }

        String userId = CommonUtils.getUserLogin();
        UserDetailFullResponse user = authManager.getUserFullInfo(userId);
        String partnerId = CommonUtils.getPartnerId();
        if (partnerId == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, EMAIL_INVALID),
                    HttpStatus.BAD_REQUEST);
        }

        TradeInReasonDecline reason = tradeInReasonDeclineRepository.findOne(request.getReasonDeclineId());
        if (reason == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, REASON_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        TradeIn tradeIn;
        List<TradeInCompDetail> comps = null;
        if (request.getCustomQuoteId() != null) {
            TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(request.getCustomQuoteId());
            if (tradeInCustomQuote == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CUSTOM_QUOTE_NOT_FOUND),
                        HttpStatus.NOT_FOUND
                );
            }
            tradeIn = tradeInRepository.findOneByCustomQuoteId(request.getCustomQuoteId());
            validateRoleForTradeIn(tradeIn);
            if (tradeIn.getStatusId() != StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED.getValue()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                        HttpStatus.NOT_FOUND
                );
            }

        } else {
            if (request.getBicycleId() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            Bicycle bicycle = bicycleRepository.findOne(request.getBicycleId());
            if (bicycle == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
            if (request.getBicycleTypeId() != null) {
                BicycleType bicycleType = bicycleTypeRepository.findOne(request.getBicycleTypeId());
                if (bicycleType == null) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                            TYPE_ID_NOT_EXIST),
                            HttpStatus.BAD_REQUEST);
                }
            }
            validateUpgradeComps(request.getUpgradeCompIds());

            tradeIn = new TradeIn();
            tradeIn.setPartnerId(partnerId);
            tradeIn.setUserCreatedId(CommonUtils.getUserLogin());
            tradeIn.setPartnerAddress(CommonUtils.getPartnerAddress(user));
            tradeIn.setPartnerName(CommonUtils.getPartnerName(user));
            tradeIn.setBicycleId(request.getBicycleId());
            tradeIn.setTitle(bicycle.getName());

            if (request.getCompRequests() != null) {
                tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests());
                comps = new ArrayList<>();
            }
        }

        tradeIn.setValue(request.getPriceSuggest());
        tradeIn.setStatusId((long) StatusTradeIn.DECLINED_DETAIL.getValue());
        tradeIn.setCondition(request.getCondition());
        tradeIn.setBicycleTypeId(request.getBicycleTypeId());
        tradeIn = tradeInRepository.save(tradeIn);

        insertUpgradeComps(request.getUpgradeCompIds(), tradeIn.getId());

        long tradeInId = tradeIn.getId();
        if (comps != null) {
            comps = request.getCompRequests().stream()
                    .map(c -> {
                        TradeInCompDetail comp = new TradeInCompDetail();
                        comp.setId(new TradeInCompDetailId(tradeInId, c.getCompId()));
                        comp.setValue(c.getValue());

                        return comp;
                    }).collect(Collectors.toList());
            tradeInCompDetailRepository.saveAll(comps);
        }

        TradeInDeclineDetail detail = new TradeInDeclineDetail();
        detail.setReasonDeclineId(request.getReasonDeclineId());
        detail.setComment(request.getComment());
        detail.setPriceExpected(request.getPriceExpected());
        detail.setTradeInId(tradeIn.getId());

        return tradeInDeclineDetailRepository.save(detail);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Object createTradeIn(@Valid TradeInCreateRequestTwo request) throws ExceptionResponse, MethodArgumentNotValidException {
        String userId = CommonUtils.getUserLogin();
        String partnerId = CommonUtils.getPartnerId();
        if (partnerId == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST);
        }
        UserDetailFullResponse user = authManager.getUserFullInfo(userId);

        if (request.getCustomQuoteId() != null) {
            return updateTradeInFromCustomQuote(request.getCustomQuoteId());
        }

        if (request.getSave() == null) {
            request.setSave(false);
        }

        //manual trigger validating
        if (!request.getSave()) {
            BeanPropertyBindingResult errors = new BeanPropertyBindingResult(request, "request");
            validator.validate(request, errors);
            Method currentMethod = new Object() {
            }.getClass().getEnclosingMethod();
            if (errors.hasErrors()) {
                throw new MethodArgumentNotValidException(
                        new MethodParameter(currentMethod, 0),
                        errors);
            }
        }
        //end

        Bicycle bicycle = null;
        BicycleType bicycleType = null;
        boolean validateSuspension = false;

        if (request.getBicycleId() == null) {
            if (!request.getSave()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_ID_MUST_BE_NOT_NULL),
                        HttpStatus.NOT_FOUND
                );
            }
        } else {
            bicycle = bicycleRepository.findOne(request.getBicycleId());
        }
        if (request.getCondition() == null && !request.getSave()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, CONDITION_MUST_BE_NOT_NULL),
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getValue() == null) {
            if (!request.getSave()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, VALUE_MUST_BE_NOT_NULL),
                        HttpStatus.NOT_FOUND
                );
            }
        } else if (request.getValue() <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_VALUE_MUST_POSITIVE),
                    HttpStatus.NOT_FOUND
            );
        }

        if (bicycle == null) {
            if (!request.getSave()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        } else if (request.getBicycleTypeId() != null) {
            bicycleType = bicycleTypeRepository.findOne(request.getBicycleTypeId());
            if (bicycleType == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        TYPE_ID_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }


            if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_HYBRID)
                    || bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_MOUNTAIN)
            ) {
                validateSuspension = true;
            }

            if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
                if (request.geteBikeMileage() != null && request.geteBikeMileage() < 0) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            EBIKE_MILEAGE_MUST_BE_POSITIVE,
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (request.geteBikeHours() != null && request.geteBikeHours() < 0) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            EBIKE_HOUR_MUST_BE_POSITIVE,
                            HttpStatus.BAD_REQUEST
                    );
                }
            }
        }

        List<String> required = TradeInUtil.getRequiredScorecardComponents();
        if (request.getSave()) {
            tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests(), false, null);
        } else {
            tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests(), validateSuspension, required);
        }

        validateUpgradeComps(request.getUpgradeCompIds());
//        List<TradeInUpgradeComp> tradeInUpgradeComps;
//        if (request.getUpgradeCompIds() != null && request.getUpgradeCompIds().size() > 0) {
//            tradeInUpgradeComps = tradeInUpgradeCompRepository.findAllByIds(request.getUpgradeCompIds());
//            if (request.getUpgradeCompIds().size() < tradeInUpgradeComps.size()) {
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_UPGRADE_COMP_INVALID),
//                        HttpStatus.NOT_FOUND
//                );
//            }
//            List<TradeInUpgradeComp> newUpgrades = tradeInUpgradeComps.stream().map(tr -> {
//                TradeInUpgradeComp tradeInUpgradeComp = new TradeInUpgradeComp();
//                tradeInUpgradeComp.setId(tr.getId());
//                String name = tr.getName();
//                if (name.contains(" ")) {
//                    int indexSpace = name.indexOf(" ");
//                    name = name.substring(indexSpace + 1);
//                }
//                tradeInUpgradeComp.setName(name);
//                tradeInUpgradeComp.setUp(tr.isUp());
//                tradeInUpgradeComp.setPercentValue(tr.getPercentValue());
//                return tradeInUpgradeComp;
//            }).collect(Collectors.toList());
//            Map<String, List<TradeInUpgradeComp>> mapUpgradeComps =
//                    newUpgrades.stream().collect(Collectors.groupingBy(TradeInUpgradeComp::getName));
//            for (Map.Entry<String, List<TradeInUpgradeComp>> entry : mapUpgradeComps.entrySet()) {
//                Boolean isUp = null;
//                for (TradeInUpgradeComp tradeInUpgradeComp : entry.getValue()) {
//                    if (isUp == null) {
//                        isUp = tradeInUpgradeComp.isUp();
//                    } else {
//                        if (!isUp.equals(tradeInUpgradeComp.isUp())) {
//                            throw new ExceptionResponse(
//                                    new ObjectError(ObjectError.ERROR_PARAM, UPGRADE_COMPONENT_MUST_SAME_RISE),
//                                    HttpStatus.BAD_REQUEST
//                            );
//                        }
//                    }
//
//                }
//            }
//            CommonUtils.stream(request.getUpgradeCompIds());
//            int countUps = tradeInUpgradeCompRepository.getTotalNumber(request.getUpgradeCompIds());
//            if (countUps != request.getUpgradeCompIds().size()) {
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_PARAM, UPGRADE_COMPONENT_INVALID),
//                        HttpStatus.BAD_REQUEST
//                );
//            }
//        } else {
//            tradeInUpgradeComps = new ArrayList<>();
//        }

        TradeIn tradeIn = null;
        if (bicycle != null) {
            tradeIn = tradeInRepository.findOneFromCustomQuote(
                    bicycle.getBrandId(), bicycle.getModelId(), bicycle.getYearId(),
                    StatusTradeInCustomQuote.PROVIDED_VALUE.getValue(),
                    userId
            );
        }
        if (tradeIn == null) {
            tradeIn = new TradeIn();
        } else {
            if (!tradeIn.getPartnerId().equals(partnerId)) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        if (request.getSave()) {
            tradeIn.setStatusId((long)StatusTradeIn.INCOMPLETE_DETAIL.getValue());
        } else {
            tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_SUMMARY.getValue());
        }
        tradeIn.setBicycleId(request.getBicycleId());
        tradeIn.setBicycleTypeId(bicycleType != null ? bicycleType.getId() : null);
        tradeIn.setCondition(request.getCondition());
        tradeIn.setPartnerId(partnerId);
        tradeIn.setPartnerAddress(CommonUtils.getPartnerAddress(user));
        tradeIn.setPartnerName(CommonUtils.getPartnerName(user));
        tradeIn.setUserCreatedId(CommonUtils.getUserLogin());
        tradeIn.setClean(request.getClean());
        if (bicycleType != null && bicycleType.getName().equals(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
            tradeIn.seteBikeMileage(request.geteBikeMileage());
            tradeIn.seteBikeHours(request.geteBikeHours());
        }

//        final float valueTradeIn = request.getValue();
        tradeIn.setValue(request.getValue());
        if (bicycle != null) {
            BicycleBaseInfo bicycleBaseInfo = getInventoryBaseInfoRepository.findOneFromBicycleId(bicycle.getId());
            tradeIn.setTitle(bicycleBaseInfo.getBicycleYearName() + " " + bicycleBaseInfo.getBicycleBrandName() + " " + bicycleBaseInfo.getBicycleModelName());
        }
        tradeIn = tradeInRepository.save(tradeIn);


//        long tradeInId = tradeIn.getId();
//        if (request.getUpgradeCompIds() != null && request.getUpgradeCompIds().size() > 0) {
//            tradeInUpgradeCompDetailRepository.save(
//                    request.getUpgradeCompIds().stream().map(upgradeId -> {
//                        TradeInUpgradeCompDetail detail = new TradeInUpgradeCompDetail();
//                        detail.setTradeInId(tradeInId);
//                        detail.setTradeInUpgradeCompId(upgradeId);
//                        return detail;
//                    }).collect(Collectors.toList())
//            );
//
//            float current = valueTradeIn;
//            for (TradeInUpgradeComp tradeInUpgradeComp : tradeInUpgradeComps) {
//                if (tradeInUpgradeComp.isUp()) {
//                    current = current + tradeInUpgradeComp.getPercentValue() * valueTradeIn;
//                } else {
//                    current = current - tradeInUpgradeComp.getPercentValue() * valueTradeIn;
//                }
//
//            }
//            tradeIn.setValue(current);
//            tradeIn = tradeInRepository.save(tradeIn);
//        }
//        switch (request.getCondition()) {
//            case EXCELLENT:
//                tradeIn.setValue(tradeIn.getValue() + valueTradeIn * Constants.EXCELLENT_PERCENT);
//                break;
//            case VERY_GOOD:
//                tradeIn.setValue(tradeIn.getValue() + valueTradeIn * Constants.VERY_GOOD_PERCENT);
//                break;
//            case GOOD:
//                tradeIn.setValue(tradeIn.getValue() + valueTradeIn * Constants.GOOD_PERCENT);
//                break;
//            case FAIR:
//                tradeIn.setValue(tradeIn.getValue() + valueTradeIn * Constants.FAIR_PERCENT);
//                break;
//            default:
//                break;
//        }

        //only update after step 4
//        inventoryManager.updateInventoryStepOneAsync(tradeIn, bicycle, request.getUpgradeCompIds(), null);
        insertUpgradeComps(request.getUpgradeCompIds(), tradeIn.getId());

        if (request.getCompRequests() != null && request.getCompRequests().size() > 0) {
            insertCompDetail(tradeIn.getId(), request.getCompRequests());
        }

        return tradeIn;
    }

    private TradeIn updateTradeInFromCustomQuote(long customQuoteId) throws ExceptionResponse {
        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(customQuoteId);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CUSTOM_QUOTE_NOT_FOUND),
                    HttpStatus.NOT_FOUND
            );
        }
        if (tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.PROVIDED_VALUE) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                    HttpStatus.NOT_FOUND
            );
        }
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(customQuoteId);

        if (tradeIn.isCancel()) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    TRADE_IN_CANCELED,
                    HttpStatus.CONFLICT
            );
        }

        validateRoleForTradeIn(tradeIn);

        if (tradeIn.getStatusId() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_TRADE_IN_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_SUMMARY.getValue());
        tradeIn = tradeInRepository.save(tradeIn);

        tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.COMPLETED);
        tradeInCustomQuote = tradeInCustomQuoteRepository.save(tradeInCustomQuote);

        //only update after step 4
//        tradeInManagerAsync.createInventoryFromTradeInCustomQuote(tradeInCustomQuote, tradeIn);
        return tradeIn;
    }

    public Object updateStepDetail(long tradeInId, TradeInUpdateDetailRequest request) throws ExceptionResponse {
        TradeIn tradeIn = findTradeInCheckExist(tradeInId);

        validateRoleForTradeIn(tradeIn);
        checkNotComplete(tradeIn);
        checkNotDecline(tradeIn);
        checkNotCancel(tradeIn);

        if (request.getSave() == null) {
            request.setSave(false);
        }

        Bicycle bicycle = null;
        if (request.getBicycleId() != null) {
            bicycle = bicycleRepository.findOne(request.getBicycleId());
            if (bicycle == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }

            BicycleBaseInfo bicycleBaseInfo = getInventoryBaseInfoRepository.findOneFromBicycleId(bicycle.getId());
            tradeIn.setTitle(bicycleBaseInfo.getBicycleYearName() + " " + bicycleBaseInfo.getBicycleBrandName() + " " + bicycleBaseInfo.getBicycleModelName());
        } else if (!request.getSave() && tradeIn.getBicycleId() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_ID_MUST_BE_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (request.getCondition() != null) {
            tradeIn.setCondition(request.getCondition());
        } else if (!request.getSave() && tradeIn.getCondition() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, CONDITION_MUST_BE_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (request.getValue() != null) {
            if (request.getValue() <= 0) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_VALUE_MUST_POSITIVE),
                        HttpStatus.NOT_FOUND
                );
            }
            tradeIn.setValue(request.getValue());
        } else if (!request.getSave() && tradeIn.getValue() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, VALUE_MUST_BE_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (request.geteBikeMileage() != null && request.geteBikeMileage() < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EBIKE_MILEAGE_MUST_BE_POSITIVE,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.geteBikeHours() != null && request.geteBikeHours() < 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EBIKE_HOUR_MUST_BE_POSITIVE,
                    HttpStatus.BAD_REQUEST
            );
        }
        BicycleType bicycleType = null;
        boolean validateSuspension = false;
        if (request.getBicycleTypeId() != null) {
            bicycleType = bicycleTypeRepository.findOne(request.getBicycleTypeId());

            if (bicycleType == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        TYPE_ID_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }
            if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_HYBRID)
                    || bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_MOUNTAIN)
            ) {
                validateSuspension = true;
            }
            if (bicycleType.getName().equals(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
//                if (request.geteBikeHours() == null && tradeIn.geteBikeHours() == null) {
//                    throw new ExceptionResponse(
//                            ObjectError.ERROR_PARAM,
//                            EBIKE_MILEAGE_MUST_BE_POSITIVE,
//                            HttpStatus.BAD_REQUEST
//                    );
//                }
//                if (request.geteBikeMileage() == null && tradeIn.geteBikeMileage() == null) {
//                    throw new ExceptionResponse(
//                            ObjectError.ERROR_PARAM,
//                            EBIKE_HOUR_MUST_BE_POSITIVE,
//                            HttpStatus.BAD_REQUEST
//                    );
//                }

                if (request.geteBikeHours() != null) {
                    tradeIn.seteBikeHours(request.geteBikeHours());
                }
                if (request.geteBikeMileage() != null) {
                    tradeIn.seteBikeMileage(request.geteBikeMileage());
                }
            } else {
                //remove ebike hours, mileage
                tradeIn.seteBikeHours(null);
                tradeIn.seteBikeMileage(null);
            }

            tradeIn.setBicycleTypeId(bicycleType.getId());
        }
        List<String> required = TradeInUtil.getRequiredScorecardComponents();
        if (request.getCompRequests() != null && request.getCompRequests().size() > 0) {
            if (request.getSave()) {
                tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests(), false, null);
            } else {
                tradeInCustomQuoteManager.validCustomQuoteCompType(request.getCompRequests(), validateSuspension, required);
            }
        } else if (!request.getSave()) {
            List<TradeInCompDetail> compDetails = tradeInCompDetailRepository.findByTradeIn(tradeIn.getId());
            if (compDetails.size() < required.size()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MISSING_REQUIRED_COMPONENTS,
                        HttpStatus.BAD_REQUEST
                );
            }
        }


        if (request.getUpgradeCompIds() != null && request.getUpgradeCompIds().size() > 0) {
            CommonUtils.stream(request.getUpgradeCompIds());
            int countUps = tradeInUpgradeCompRepository.getTotalNumber(request.getUpgradeCompIds());
            if (countUps != request.getUpgradeCompIds().size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, UPGRADE_COMPONENT_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        if (request.getClean() != null) {
            tradeIn.setClean(request.getClean());
        } else if (!request.getSave() && tradeIn.getClean() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, CLEAN_MUST_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }

        //saving
        if (request.getUpgradeCompIds() != null && request.getUpgradeCompIds().size() > 0) {
            tradeInUpgradeCompDetailRepository.deleteByTradeInId(tradeIn.getId());
            tradeInCustomQuoteManager.insertUpgradeComponent(request.getUpgradeCompIds(), tradeIn.getId());
        }
        if (request.getCompRequests() != null && request.getCompRequests().size() > 0) {
            insertCompDetail(tradeIn.getId(), request.getCompRequests());
        }
        if (tradeIn.getStatusId() == StatusTradeIn.INCOMPLETE_DETAIL.getValue()) {
            if (request.getSave() != null) {
                if (!request.getSave()) {
                    tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_SUMMARY.getValue());
                }
                tradeIn.setSave(request.getSave());
            }
        }
        tradeIn = tradeInRepository.save(tradeIn);

        return tradeIn;
    }

    public Object updateStepSummary(long tradeInId, TradeInSummaryRequest request) throws ExceptionResponse {
        TradeIn tradeIn = findTradeInCheckExist(tradeInId);

        validateRoleForTradeIn(tradeIn);

        checkNotComplete(tradeIn);
        checkNotDecline(tradeIn);

        if (tradeIn.getStatusId() == StatusTradeIn.INCOMPLETE_SUMMARY.getValue()) {
            return createStepSummary(tradeIn, request);
        } else {
            if (request.isSave()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, SCORE_CARD_INVALID),
                        HttpStatus.NOT_FOUND
                );
            }

            TradeInOwner owner = request.getOwner();
            if (owner != null) {
                String address;
                String city;
                String state;
                String zipCode;

                if (owner.getName() != null) {
                    tradeIn.setOwnerName(owner.getName());
                }
                if (owner.getAddress() != null) {
                    tradeIn.setOwnerAddress(owner.getAddress());
                }
                address = tradeIn.getOwnerAddress();

                if (owner.getCity() != null) {
                    tradeIn.setOwnerCity(owner.getCity());
                }
                city = owner.getCity();

                if (owner.getState() != null) {
                    tradeIn.setOwnerState(owner.getState());
                }
                state = tradeIn.getOwnerState();

                if (owner.getZipCode() != null) {
                    tradeIn.setOwnerZipCode(owner.getZipCode());
                }
                zipCode = tradeIn.getOwnerZipCode();

                if (owner.getEmail() != null) {
                    tradeIn.setOwnerEmail(owner.getEmail());
                }
                if (owner.getPhone() != null) {
                    tradeIn.setOwnerPhone(owner.getPhone());
                }
                if (owner.getLicenseOrPassport() != null) {
                    tradeIn.setOwnerLicensePassport(owner.getLicenseOrPassport());
                }
                if (owner.getSerial() != null) {
                    tradeIn.setSerialNumber(owner.getSerial());
                }

                myListingManager.validateShippingLocation(
                        address, city, state, zipCode,
                        Constants.DEFAULT_PARTNER_BICYCLE_COUNTRY_CODE
                );
            }
            TradeInProof tradeInProof = request.getProof();
            if (tradeInProof != null) {
                if (tradeInProof.getDate() != null) {
                    tradeIn.setProofDate(tradeInProof.getDate());
                }
                if (tradeInProof.getName() != null) {
                    tradeIn.setProofName(tradeInProof.getName());
                }
            }
            return tradeInRepository.save(tradeIn);
        }

    }

    private TradeIn createStepSummary(TradeIn tradeIn, TradeInSummaryRequest request) throws ExceptionResponse {
        if (request.getOwner() == null) {
            request.setOwner(new TradeInOwner());
        }
        if (request.getProof() == null) {
            request.setProof(new TradeInProof());
        }
        if (!request.isSave() && (!request.getOwner().checkValideRequest() || !request.getProof().checkValid())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, INVALID_PARAM),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (!request.isSave()) {
            myListingManager.validateShippingLocation(
                    request.getOwner().getAddress(),
                    request.getOwner().getCity(),
                    request.getOwner().getState(),
                    request.getOwner().getZipCode(),
                    Constants.DEFAULT_PARTNER_BICYCLE_COUNTRY_CODE
            );
        }
        if (tradeIn.getStatusId() != StatusTradeIn.INCOMPLETE_SUMMARY.getValue()) {
//            TradeInStatus tradeInStatus = tradeInStatusRepository.findOnePreferActive(tradeIn.getStatusId());
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, SCORE_CARD_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
//        if (!request.isSave() && tradeIn.isSave()) {
//            throw new ExceptionResponse(
//                    new ObjectError(ObjectError.ERROR_NOT_FOUND, SCORE_CARD_INVALID),
//                    HttpStatus.NOT_FOUND
//            );
//        }

        tradeIn.setOwnerName(request.getOwner().getName());
        tradeIn.setOwnerAddress(request.getOwner().getAddress());
        tradeIn.setOwnerCity(request.getOwner().getCity());
        tradeIn.setOwnerState(request.getOwner().getState());
        tradeIn.setOwnerZipCode(request.getOwner().getZipCode());
        tradeIn.setOwnerEmail(request.getOwner().getEmail());
        tradeIn.setOwnerPhone(request.getOwner().getPhone());
        tradeIn.setOwnerLicensePassport(request.getOwner().getLicenseOrPassport());
        tradeIn.setSerialNumber(request.getOwner().getSerial());
        tradeIn.setProofName(request.getProof().getName());
        tradeIn.setProofDate(request.getProof().getDate());

        if (request.isSave()) {
            tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_SUMMARY.getValue());
        } else {
            tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue());
        }
        tradeIn.setSave(request.isSave());

        tradeIn = tradeInRepository.save(tradeIn);
        //only update after step 4
//        inventoryManager.updateInventoryStepTwoAsync(tradeIn);
        return tradeIn;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Object getTradeInDetail(long tradeInId) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeInId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        validateRoleForTradeIn(tradeIn);
        TradeInDetailResponse detail = new TradeInDetailResponse();
        detail.setTradeInDetail(tradeInId);
        detail.setValue(tradeIn.getValue());
        detail.setCondition(tradeIn.getCondition());
        detail.setClean(tradeIn.getClean());
        detail.seteBikeMileage(tradeIn.geteBikeMileage());
        detail.seteBikeHours(tradeIn.geteBikeHours());
        detail.setBaseInfo(traBicBaseInfoResponseRepository.findOne(tradeIn.getBicycleId()));

        if (!StringUtils.isBlank(tradeIn.getOwnerName())) {
            TradeInOwner owner = new TradeInOwner();
            owner.setName(tradeIn.getOwnerName());
            owner.setAddress(tradeIn.getOwnerAddress());
            owner.setCity(tradeIn.getOwnerCity());
            owner.setState(tradeIn.getOwnerState());
            owner.setZipCode(tradeIn.getOwnerZipCode());
            owner.setEmail(tradeIn.getOwnerEmail());
            owner.setPhone(tradeIn.getOwnerPhone());
            owner.setLicenseOrPassport(tradeIn.getOwnerLicensePassport());
            detail.setOwner(owner);
        }
        if (!StringUtils.isBlank(tradeIn.getProofName())) {
            TradeInProof proof = new TradeInProof();
            proof.setName(tradeIn.getProofName());
            proof.setDate(tradeIn.getProofDate());
            detail.setProof(proof);
        }
        detail.setAllUpgradeComponent(tradeInUpgradeCompRepository.findAll());


        return detail;

    }


    public Object getAllStatusTradeIn() {
        return tradeInStatusRepository.findAll();
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getTradeIns(TradeInsRequest request) throws ExceptionResponse {
        ListObjResponse<TradeInResponse> response = new ListObjResponse<>();
//        String userId = CommonUtils.getUserLogin();
        String partnerId = CommonUtils.getPartnerId();

        String content = request.getContent();
        if (content != null) {
            content = "%" + content + "%";
        }
        if (CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            partnerId = request.getPartnerId();
        } else {
            if (request.getPartnerId() != null &&
                    (partnerId == null || !request.getPartnerId().equals(partnerId))
            ) {
                throw new ExceptionResponse(
                        ObjectError.NO_PERMISSION,
                        YOU_DONT_HAVE_PERMISSION,
                        HttpStatus.FORBIDDEN
                );
            }
            if (partnerId == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, YOUR_MUST_BE_PARTNER),
                        HttpStatus.OK
                );
            }
        }

        if (request.getTypeSearch() == null) {
            request.setTypeSearch(TypeSearchTradeInRequest.DATE);
        }
        if (request.getSort() == null) {
            request.setSort(Sort.DESC);
        }
        response.setData(tradeInResponseRepository.findAllSorted(
                request.getStatus() == null ? null : (long) request.getStatus().getValue(),
                request.getArchived(),
                content,
                partnerId,
                request.getIncomplete(),
                request.getTypeSearch(),
                request.getSort(),
                request.getPage().getOffset(),
                request.getPage().getPageSize()
        ));
//        switch (request.getTypeSearch()) {
//            case DATE:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByCreatedTimeDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByCreatedTimeAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//            case BICYCLE:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByBicycleDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByBicycleAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//            case LOCATION:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByLocationDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByLocationAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//            case ID:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByIdDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByIdAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//
//            case STATUS:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderStatusDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderStatusAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//
//            case CANCEL:
//                response.setData(
//                        tradeInResponseRepository.findAllOrderCancel(
//                                request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                request.getArchived(),
//                                content,
//                                partnerId,
//                                request.getSort() == null ? Sort.ASC.name() : request.getSort().name(),
//                                request.getPage().getOffset(),
//                                request.getPage().getPageSize()
//                        )
//                );
//                break;
//
//            default:
//                if (request.getSort() == Sort.DESC) {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByCustomerDesc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                } else {
//                    response.setData(
//                            tradeInResponseRepository.findAllOrderByCustomerAsc(
//                                    request.getStatus() == null ? null : (long) request.getStatus().getValue(),
//                                    request.getArchived(),
//                                    content,
//                                    partnerId,
//                                    request.getPage().getOffset(),
//                                    request.getPage().getPageSize()
//                            )
//                    );
//                }
//                break;
//        }


        response.setTotalItem(
                tradeInResponseRepository.getCount(
                        request.getStatus() == null ? null : (long) request.getStatus().getValue(),
                        request.getArchived(),
                        content,
                        partnerId,
                        request.getIncomplete()
                )
        );
        String cancelName = null;
        boolean isSelectCancel = false;
        for (TradeInResponse data : response.getData()) {
            if (data.isCancel()) {
                data.setStatusId((long) StatusTradeIn.CANCEL.getValue());
                if (!isSelectCancel) {
                    isSelectCancel = true;
                    TradeInStatus tradeInStatus = tradeInStatusRepository.findOne((long) StatusTradeIn.CANCEL.getValue());
                    if (tradeInStatus != null) {
                        cancelName = tradeInStatus.getName();
                    }
                }
                data.setStatusName(cancelName);
            }
        }
        response.setPageSize(request.getPage().getPageSize());
        response.setPage(request.getPage().getPageNumber());
        response.updateTotalPage();
        response.updateTotalPage();
        return response;
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getDetailTradeIn(long tradeId, int indexStep) throws ExceptionResponse {
        if (indexStep < 0 || indexStep > 3) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, STEP_TRADE_IN_INVALID),
                    HttpStatus.BAD_REQUEST
            );
        }

        TradeIn tradeIn = tradeInRepository.findOne(tradeId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        validateRoleForTradeIn(tradeIn);

        StatusTradeIn statusTradeIn = tradeIn.getStatusId() == null ? null : StatusTradeIn.findByValue(tradeIn.getStatusId().intValue());
        if (indexStep == 0 || indexStep == 1) {
            TradeInStepOneTwoResponse response = new TradeInStepOneTwoResponse();
            response.setTradeInId(tradeId);
            response.setTradeValue(tradeIn.getValue());
            response.setClean(tradeIn.getClean());
            response.seteBikeMileage(tradeIn.geteBikeMileage());
            response.seteBikeHours(tradeIn.geteBikeHours());
            response.setBicycleTypeId(tradeIn.getBicycleTypeId());
            if (tradeIn.getBicycleId() != null) {
                BicycleBaseInfo bicycleBaseInfo = inventoryBaseInfoRepository.findOneFromBicycleId(tradeIn.getBicycleId());
                response.setBicycleBaseInfo(bicycleBaseInfo);
                response.setSpec(
                        bicycleSpecRepository.findAllByBicycleId(tradeIn.getBicycleId())
                                .stream()
                                .map(CommonUtils::convertToSpecResponse).collect(Collectors.toList())
                );
                response.setBicycleComponents(
                        bicycleComponentManager.findAllByBicycleId(tradeIn.getBicycleId())
                );
            } else {
                if (tradeIn.getCustomQuoteId() != null) {
                    TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeIn.getCustomQuoteId());
                    if (tradeInCustomQuote != null) {
                        BicycleBaseInfo bicycleBaseInfo = new BicycleBaseInfo();

                        bicycleBaseInfo.setBicycleBrandName(tradeInCustomQuote.getBrandName());
                        bicycleBaseInfo.setBicycleBrandId(tradeInCustomQuote.getBrandId());
                        bicycleBaseInfo.setBicycleModelName(tradeInCustomQuote.getModelName());
                        bicycleBaseInfo.setBicycleModelId(tradeInCustomQuote.getModelId());
                        bicycleBaseInfo.setBicycleYearId(tradeInCustomQuote.getYearId());
                        bicycleBaseInfo.setBicycleYearName(tradeInCustomQuote.getYearName());
//                        bicycleBaseInfo.setBicycleTypeId(tradeInCustomQuote.getTypeId());
                        response.setBicycleTypeId(tradeInCustomQuote.getTypeId());
//                        if (tradeInCustomQuote.getTypeId() != null) {
//                            BicycleType bicycleType = bicycleTypeRepository.findOne(tradeInCustomQuote.getTypeId());
//                            if (bicycleType != null) {
//                                bicycleBaseInfo.setBicycleTypeName(bicycleType.getName());
//                            }
//                        }
                        response.setBicycleBaseInfo(bicycleBaseInfo);
                        response.seteBikeMileage(tradeInCustomQuote.geteBikeMileage());
                        response.seteBikeHours(tradeInCustomQuote.geteBikeHours());
                        response.setClean(tradeInCustomQuote.getClean());
                    }

                }

            }

//            response.setUpgradeComps(
//                    contentCommonRepository.findAllTradeInUpgradeComponent(tradeId)
//                            .stream().map(ContentCommon::getId).collect(Collectors.toList())
//            );
            response.setUpgradeComps(
                    tradeInUpgradeCompRepository.findAllTradeInUpgradeComponent(tradeId)
                            .stream().map(upgrade -> {
                        TradeInUpgradeCompResponse res = new TradeInUpgradeCompResponse();
                        res.setId(upgrade.getId());
                        res.setPercentValue(upgrade.getPercentValue());
                        res.setName(upgrade.getName());
                        res.setUp(upgrade.isUp());
                        res.setValue(upgrade.getName());

                        return res;
                    }).collect(Collectors.toList())
            );

            if (tradeIn.getCustomQuoteId() != null) {
                response.setTradeInComponents(tradeInCompDetailRepository.mapCustomQuoteCompsToTradeIn(tradeIn.getId()));
            } else {
                response.setTradeInComponents(tradeInCompDetailRepository.findByTradeIn(tradeIn.getId()));
            }

            TradeInOwner owner = new TradeInOwner();
            owner.setName(tradeIn.getOwnerName());
            owner.setAddress(tradeIn.getOwnerAddress());
            owner.setCity(tradeIn.getOwnerCity());
            owner.setState(tradeIn.getOwnerState());
            owner.setZipCode(tradeIn.getOwnerZipCode());
            owner.setEmail(tradeIn.getOwnerEmail());
            owner.setPhone(tradeIn.getOwnerPhone());
            owner.setLicenseOrPassport(tradeIn.getOwnerLicensePassport());
            owner.setSerial(tradeIn.getSerialNumber());
            if (!owner.isAllEmpty()) {
                response.setOwner(owner);
            }
            TradeInProof proof = new TradeInProof();
            proof.setName(tradeIn.getProofName());
            proof.setDate(tradeIn.getProofDate());
            if (!proof.isEmptyAll()) {
                response.setProof(proof);
            }
            response.setCondition(tradeIn.getCondition());
            if (tradeIn.getStatusId() == StatusTradeIn.DECLINED_DETAIL.getValue()) {
                response.setDecline(
                        declineTradeInRepository.findOne(tradeId)
                );
            }

            return response;
        }

        if (indexStep == 2) {
            if (statusTradeIn == null || (statusTradeIn != StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES &&
                    statusTradeIn != StatusTradeIn.SHIPPING &&
                    statusTradeIn != StatusTradeIn.COMPLETED &&
                    statusTradeIn != StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED &&
                    statusTradeIn != StatusTradeIn.CUSTOM_QUOTE_PENDING_REVIEW &&
                    statusTradeIn != StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE
            )) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, STEP_TRADE_IN_INVALID),
                        HttpStatus.NOT_FOUND
                );
            }
            List<TradeInImageResponse> tradeInImageResponses = tradeInImageResponseRepository.findAll(tradeId);
            Map<Long, List<TradeInImageResponse>> mapTraImages =
                    tradeInImageResponses.stream().collect(Collectors.groupingBy(TradeInImageResponse::getImageTypeId));
            List<TradeInImageGroupResponse> responses = new ArrayList<>();
            mapTraImages.forEach((tradeInImageTypeId, tradeInImages) -> {
                TradeInImageGroupResponse response = new TradeInImageGroupResponse();
                response.setTradeInId(tradeId);
                response.setImageTypeId(tradeInImageTypeId);
                response.setImageTypeName(tradeInImages.get(0).getImageTypeName());
                response.setFullLinks(
                        tradeInImages.stream().map(img -> {
                            ContentCommonId contentCommonId = new ContentCommonId();
                            contentCommonId.setId(img.getImageId());
                            contentCommonId.setName(img.getFullLink());
                            return contentCommonId;
                        }).collect(Collectors.toList())
                );
                responses.add(response);
            });
            List<TradeInImageType> tradeInImageTypes = tradeInImageTypeRepository.findAll();
            for (TradeInImageType tradeInImageType : tradeInImageTypes) {
                TradeInImageGroupResponse group = CommonUtils.findObject(responses, res -> res.getImageTypeId() == tradeInImageType.getId());
                if (group == null) {
                    group = new TradeInImageGroupResponse();
                    group.setTradeInId(tradeId);
                    group.setFullLinks(new ArrayList<>());
                    group.setImageTypeName(tradeInImageType.getName());
                    group.setImageTypeId(tradeInImageType.getId());
                    responses.add(group);
                }
                group.setImageDefault(tradeInImageType.getDefaultImage());
                group.setRequire(tradeInImageType.isRequire());
            }
            return responses;
        }
        TradeInStepThreeResponse response = new TradeInStepThreeResponse();
        response.setTradeInId(tradeId);
        TradeInShipping tradeInShipping = tradeInShippingRepository.findOneByTradeInId(tradeId);
        if (tradeInShipping != null) {
            response.setShippingType(tradeInShipping.getShippingType());
//            if (tradeInShipping.getShippingType() == ShippingType.BICYCLE_BLUE_BOOK_TYPE) {
            ShippingBicycleBlueBookResponse book = new ShippingBicycleBlueBookResponse();
            book.setLength(tradeInShipping.getLength());
            book.setWidth(tradeInShipping.getWidth());
            book.setHeight(tradeInShipping.getHeight());
            book.setWeight(tradeInShipping.getWeight());
            book.setFromAddress(tradeInShipping.getFromAddress());
            book.setFromCity(tradeInShipping.getFromCity());
            book.setFromState(tradeInShipping.getFromState());
            book.setFromZipCode(tradeInShipping.getFromZipCode());
            book.setCarrierType(tradeInShipping.getCarrierType());
            book.setScheduled(tradeInShipping.getScheduled());
            book.setScheduleDate(tradeInShipping.getScheduleDate());

            List<ShipmentDetailResponse> shipmentDetails = null;
            try {
                if (tradeInShipping.getShippingType() == InboundShippingType.BICYCLE_BLUE_BOOK_TYPE) {
                    shipmentDetails = shipmentManager.getTradeInShipmentDetails(Collections.singletonList(tradeInShipping.getTradeInId()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (shipmentDetails != null && shipmentDetails.size() > 0) {
                ShipmentDetailResponse shipmentDetail = shipmentDetails.get(0);
                book.setFullLinkLabel(shipmentDetail.getFullLinkLabel());
            }

            response.setBlueBook(book);
//            } else {
//                if (tradeInShipping.getShippingType() == ShippingType.MY_ACCOUNT_TYPE) {
            ShippingMyAccountResponse myAccount = new ShippingMyAccountResponse();
            myAccount.setCarrier(tradeInShipping.getCarrier());
            myAccount.setCarrierTrackingNumber(tradeInShipping.getCarrierTrackingNumber());
            response.setMyAccount(myAccount);
        }
//            }
//    }

        return response;
    }

    public Object archiveTradeIn(long tradeInId, boolean isArchive) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeInId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        validateRoleForTradeIn(tradeIn);
        if (CommonUtils.getPartnerId() != null) {
            tradeInRepository.updateUserArchive(tradeInId, isArchive);
            tradeIn.setArchived(isArchive);
        } else {
            tradeInRepository.updateAdminArchive(tradeInId, isArchive);
            tradeIn.setAdminArchived(isArchive);
        }

        return tradeIn;
    }

    public Object deleteImageTradeIn(long tradeId, List<Long> imageImageTypeIds, List<Long> imageIds) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        validateRoleForTradeIn(tradeIn);
        if (imageImageTypeIds != null && imageImageTypeIds.size() == 0) {
            imageImageTypeIds = null;
        }
        if (imageIds != null && imageIds.size() == 0) {
            imageIds = null;
        }
        if (imageImageTypeIds == null && imageIds == null) {
            return SUCCESS;
        }
        boolean isImageTypeNull = imageImageTypeIds == null;
        boolean isImageIdNull = imageIds == null;
        List<Long> temp = new ArrayList<>();
        temp.add(1L);
        List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInIdAndImageTypeIdsAndImageIds(
                tradeId,
                isImageTypeNull, isImageTypeNull ? temp : imageImageTypeIds,
                isImageIdNull, isImageIdNull ? temp : imageIds
        );
        if (tradeInImages.size() > 0) {
            List<String> fullKeys = tradeInImages.stream().map(TradeInImage::getFullLink).collect(Collectors.toList());
            tradeInImageRepository.deleteAll(tradeInImages);
            deleteImageTradeIn(fullKeys);

            if (tradeIn.getStatusId() == (long) StatusTradeIn.SHIPPING.getValue()) {
                List<TradeInImage> tradeInImageIdRequireUploaded = tradeInImageRepository.findAllByTradeInId(tradeId);
                List<Long> imageRequireIds = tradeInImageTypeRepository.findAllImageRequire().stream().map(TradeInImageType::getId).collect(Collectors.toList());
                if (tradeInImageIdRequireUploaded.size() < imageRequireIds.size()) {
                    tradeIn.setSave(true);
                    tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue());
                    tradeInRepository.save(tradeIn);
                }
            }

        }

        return SUCCESS;
    }

    public Object getTradeInCostCalculator(float privateParty) throws ExceptionResponse {
        if (privateParty < 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, PRIVATE_PARTY_VALUE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND
            );
        }
        return TradeInUtil.getPriceConfig(tradeInPriceConfigRepository, 0, privateParty);
    }

    public boolean cancelShipping(long tradeInId) throws ExceptionResponse {
        TradeInShipping tradeInShipping = tradeInShippingRepository.findOneByTradeInId(tradeInId);
        if (tradeInShipping == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_SHIPPING_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        TradeIn tradeIn = tradeInRepository.findOne(tradeInShipping.getTradeInId());
        validateRoleForTradeIn(tradeIn);
        if (!tradeInShipping.getValidTrackingNumber()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, NO_VALID_TRACKING_NUMBDER),
                    HttpStatus.NOT_FOUND
            );
        }
        if (tradeInShipping.getCarrierType() == CarrierType.FEDEX) {
            if (StringUtils.isBlank(tradeInShipping.getFedexTrackingIdType())) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, FEDEX_TRACKING_NO_ID),
                        HttpStatus.NOT_FOUND
                );
            }
            FedexDeleteShipResponse fedexResponse = fedexManager.cancelShip(tradeInShipping);

            if (fedexResponse.isSuccess()) {
                //
                return true;
            } else {
                if (fedexResponse.getNotifications() != null && fedexResponse.getNotifications().size() > 0) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, fedexResponse.getNotifications().get(0).getMessage()),
                            HttpStatus.NOT_FOUND
                    );
                } else {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, FEDEX_ERROR_UNKNOWN_REASON),
                            HttpStatus.NOT_FOUND
                    );
                }

            }
        } else if (tradeInShipping.getCarrierType() == CarrierType.UPS) {
            //Not yet tested
            UPSVoidShipResponse upsResponse = upsManager.cancelShip(tradeInShipping);
            //TODO can not check cancel condition
            //...
        }


        return false;
    }

    public Object cancelTradeIn(long tradeInId, boolean isCancel) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeInId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

        String userId = CommonUtils.getUserLogin();
        validateRoleForTradeIn(tradeIn);

        TradeInCancel tradeInCancel = validateCancelTradeIn(tradeIn, isCancel);

        List<TradeInCancel> tradeInCancels = tradeInCancelRepository.findAllByInActive(
                tradeInId,
                false
        );
        if (tradeInCancels.size() > 0) {
            tradeInCancels = tradeInCancels.stream().peek(cancel -> cancel.setInActive(true)).collect(Collectors.toList());
            tradeInCancelRepository.saveAll(tradeInCancels);
        }

        tradeInCancelRepository.save(tradeInCancel);

        tradeIn.setCancel(isCancel);
        return tradeInRepository.save(tradeIn);
    }

    public Object getTradeInCommon(long tradeInId) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeInId);
        if (tradeIn == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_NOT_EXIST), HttpStatus.BAD_REQUEST);
        }
        TradeInCustomQuoteCommonRes res = new TradeInCustomQuoteCommonRes();
        res.setTradeInId(tradeIn.getId());
        res.setStatus(StatusTradeIn.findByValue(tradeIn.getStatusId().intValue()));
        switch (res.getStatus()) {
            case DECLINED_DETAIL:
                res.setIndexStep(-1);
                break;
            case CUSTOM_QUOTE_VALUE_PROVIDED:
                res.setIndexStep(0);
                break;
            case INCOMPLETE_SUMMARY:
                res.setIndexStep(1);
                break;
            case INCOMPLETE_UPLOAD_IMAGES:
            case CUSTOM_QUOTE_INCOMPLETE:
            case CUSTOM_QUOTE_PENDING_REVIEW:
                res.setIndexStep(2);
                break;
            case SHIPPING:
            case COMPLETED:
                res.setIndexStep(3);
                break;
            default:
                break;

        }
        return res;
    }

    public Object cancelTradeIns(List<Long> tradeInIds, boolean isCancel) throws ExceptionResponse {
        if (tradeInIds.size() < 1) {
            return null;
        }

        tradeInIds = tradeInIds.stream().distinct().collect(Collectors.toList());
        List<TradeIn> tradeIns = tradeInRepository.findAllById(tradeInIds);
        if (tradeIns.size() < tradeInIds.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }

        List<TradeInCancel> newTradeInCancels = new ArrayList<>();

        for (TradeIn tradeIn : tradeIns) {
            CommonUtils.validRolePartner(tradeIn);
            newTradeInCancels.add(validateCancelTradeIn(tradeIn, isCancel));
            tradeIn.setCancel(isCancel);
        }

        List<TradeInCancel> oldTradeInCancels = tradeInCancelRepository.findAllByInActive(
                tradeInIds,
                false
        );
        if (oldTradeInCancels.size() > 0) {
            oldTradeInCancels = oldTradeInCancels.stream().peek(cancel -> cancel.setInActive(true)).collect(Collectors.toList());
            tradeInCancelRepository.saveAll(oldTradeInCancels);
        }
        tradeInCancelRepository.saveAll(newTradeInCancels);

        return tradeInRepository.saveAll(tradeIns);
    }

    private void validateRoleForTradeIn(TradeIn tradeIn) throws ExceptionResponse {
//        String userId = CommonUtils.getUserLogin();
//        UserDetailFullResponse user = authManager.getUserFullInfo(userId);
        CommonUtils.validRolePartner(tradeIn);
    }

//    private void validateRoleForTradeIn(TradeIn tradeIn, UserDetailFullResponse user) throws ExceptionResponse {
//        CommonUtils.validRolePartner(tradeIn, user);
//    }

    private TradeInCancel validateCancelTradeIn(TradeIn tradeIn, boolean isCancel) throws ExceptionResponse {
        List<UserRoleType> roles = CommonUtils.getUserRoles();
        String partnerId = CommonUtils.getPartnerId();

        if (tradeIn.isCancel() == isCancel) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND,
                            TRADE_IN_STATUS_INVALID + ", isCancel: " + tradeIn.isCancel()),
                    HttpStatus.NOT_FOUND
            );
        }

        TypeCancelTradeIn typeCancelTradeIn;
        if (isCancel) {
            if (partnerId != null && partnerId.equals(tradeIn.getPartnerId())) {
                typeCancelTradeIn = TypeCancelTradeIn.BY_PARTNER;
            } else {
                if (CommonUtils.checkRoleAccessFromAdmin(roles)) {
                    typeCancelTradeIn = TypeCancelTradeIn.BY_ADMIN;
                } else {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.NO_PERMISSION,
                                    YOU_DONT_HAVE_PERMISSION),
                            HttpStatus.NOT_FOUND
                    );
                }
            }

            //allow admin cancel while any status
            if (typeCancelTradeIn != TypeCancelTradeIn.BY_ADMIN) {
                if (tradeIn.getStatusId() == StatusTradeIn.DECLINED_DETAIL.getValue() ||
                        tradeIn.getStatusId() == StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue() ||
                        tradeIn.getStatusId() == StatusTradeIn.CUSTOM_QUOTE_PENDING_REVIEW.getValue() ||
                        tradeIn.getStatusId() == StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED.getValue() ||
                        tradeIn.getStatusId() == StatusTradeIn.COMPLETED.getValue()
                    //TODO temporary not allow cancel trade after completed,
                    // waiting for logic with shipping after cancel trade in
                ) {
                    TradeInStatus tradeInStatus = tradeInStatusRepository.findOne(tradeIn.getStatusId());
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND,
                                    TRADE_IN_STATUS_INVALID + (tradeInStatus == null ? "" : "(" + tradeInStatus.getName() + ")")),
                            HttpStatus.FORBIDDEN
                    );
                }
            }
        } else {
            TradeInCancel oldCancel = tradeInCancelRepository.findLast(tradeIn.getId(), isCancel);
            if (oldCancel != null) {
                if (oldCancel.getTypeCancel() != TypeCancelTradeIn.BY_PARTNER
                        && !CommonUtils.checkRoleAccessFromAdmin(roles)
                ) {
                    throw new ExceptionResponse(
                            ObjectError.NO_PERMISSION,
                            YOU_DONT_HAVE_PERMISSION,
                            HttpStatus.FORBIDDEN
                    );
                }
            }

            oldCancel.setInActive(true);
            return oldCancel;
        }


        TradeInCancel tradeInCancel = new TradeInCancel();
        tradeInCancel.setInActive(false);
        tradeInCancel.setCancel(isCancel);
        tradeInCancel.setTypeCancel(typeCancelTradeIn);
        tradeInCancel.setCancelId(CommonUtils.getUserLogin());
        tradeInCancel.setTradeInId(tradeIn.getId());
        tradeInCancel.setStatusId(tradeIn.getStatusId());

        return tradeInCancel;
    }

    public Object getCountIncomplete() throws ExceptionResponse {
        String partnerId = CommonUtils.getPartnerId();
        if (partnerId == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST);
        }

        Map<String, Integer> response = new HashMap<>();
        response.put("scorecards", tradeInResponseRepository.getCount(
                null,
                null,
                null,
                partnerId,
                true
        ));
        //TODO
        response.put("customQuote", 0);
        return response;
    }

    private TradeIn findTradeInCheckExist(long tradeInId) throws ExceptionResponse {
        TradeIn tradeIn = tradeInRepository.findOne(tradeInId);
        if (tradeIn == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        return tradeIn;
    }

    private void checkNotCancel(TradeIn tradeIn) throws ExceptionResponse {
        if (tradeIn.isCancel()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CANCELED),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void checkNotComplete(TradeIn tradeIn) throws ExceptionResponse {
        if (tradeIn.getStatusId() == StatusTradeIn.COMPLETED.getValue()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_COMPLETED),
                    HttpStatus.UNPROCESSABLE_ENTITY
            );
        }
    }

    private void checkNotDecline(TradeIn tradeIn) throws ExceptionResponse {
        if (tradeIn.getStatusId() == StatusTradeIn.DECLINED_DETAIL.getValue()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_DECLINED),
                    HttpStatus.NOT_FOUND
            );
        }
    }

    private List<TradeInCompDetail> insertCompDetail(long tradeInId, List<CompRequest> compRequests) {
        List<TradeInCompDetail> tradeInComps = new ArrayList<>();
        for (CompRequest req : compRequests) {
            TradeInCompDetail comp = new TradeInCompDetail();

            TradeInCompDetailId id = new TradeInCompDetailId();
            id.setTradeInId(tradeInId);
            id.setInventoryCompTypeId(req.getCompId());

            comp.setId(id);
            comp.setValue(req.getValue());

            tradeInComps.add(comp);
        }

        return tradeInCompDetailRepository.saveAll(tradeInComps);
    }

    private void validateUpgradeComps(List<Long> upgradeCompIds) throws ExceptionResponse {
        if (upgradeCompIds != null && upgradeCompIds.size() > 0) {
            CommonUtils.stream(upgradeCompIds);
            int countUps = tradeInUpgradeCompRepository.getTotalNumber(upgradeCompIds);
            if (countUps != upgradeCompIds.size()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, UPGRADE_COMPONENT_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }

    private List insertUpgradeComps(List<Long> upgradeCompIds, long tradeInId) {
        if (upgradeCompIds == null) {
            return null;
        }
        if (upgradeCompIds.size() < 1) {
            return new ArrayList();
        }
        return tradeInUpgradeCompDetailRepository.saveAll(
                upgradeCompIds.stream().map(id -> {
                    TradeInUpgradeCompDetail re = new TradeInUpgradeCompDetail();
                    re.setTradeInId(tradeInId);
                    re.setTradeInUpgradeCompId(id);
                    return re;
                }).collect(Collectors.toList())
        );
    }
}
