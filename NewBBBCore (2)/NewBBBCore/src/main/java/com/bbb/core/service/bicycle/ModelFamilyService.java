package com.bbb.core.service.bicycle;

public interface ModelFamilyService {
    Object getModelFamilies(long brandId);
}
