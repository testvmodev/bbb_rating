package com.bbb.core.service.impl.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bid.AuctionManager;
import com.bbb.core.model.request.bid.auction.AuctionCreateRequest;
import com.bbb.core.model.request.bid.auction.AuctionRequest;
import com.bbb.core.model.request.bid.auction.AuctionUpdateRequest;
import com.bbb.core.service.bid.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class AuctionServiceImpl implements AuctionService {
    @Autowired
    private AuctionManager manager;

    @Override
    public Object getAuction(AuctionRequest request, Pageable page) throws ExceptionResponse {
        return manager.getAuction(request, page);
    }

    @Override
    public Object createAuction(AuctionCreateRequest request) throws ExceptionResponse {
        return manager.createAuction(request);
    }

    @Override
    public Object updateAuction(long auctionId, AuctionUpdateRequest request) throws ExceptionResponse {
        return manager.updateAuction(auctionId, request);
    }
}
