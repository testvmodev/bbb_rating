package com.bbb.core.model.request.bid.offer;

import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.sort.UserOfferSort;
import org.springframework.data.domain.Pageable;

public class UserOfferRequest {
    private String userId;
    private UserOfferSort sortField;
    private Sort sortType;
    private Pageable pageable;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserOfferSort getSortField() {
        return sortField;
    }

    public void setSortField(UserOfferSort sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
