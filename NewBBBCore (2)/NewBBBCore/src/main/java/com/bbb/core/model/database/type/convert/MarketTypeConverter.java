package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.MarketType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MarketTypeConverter implements AttributeConverter<MarketType, String> {
    @Override
    public MarketType convertToEntityAttribute(String value) {
        for(MarketType marketType : MarketType.values()) {
            if (marketType.getValue().equals(value)) return marketType;
        }
        return null;
    }

    @Override
    public String convertToDatabaseColumn(MarketType marketType) {
        if (marketType == null) return null;
        return marketType.getValue();
    }
}
