package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class TaxJurisdiction {
    @JacksonXmlProperty(localName = "JurisdictionID")
    private String jurisdictionID;
    @JacksonXmlProperty(localName = "SalesTaxPercent")
    private float salesTaxPercent;
    @JacksonXmlProperty(localName = "ShippingIncludedInTax")
    private boolean shippingIncludedInTax;

    public String getJurisdictionID() {
        return jurisdictionID;
    }

    public void setJurisdictionID(String jurisdictionID) {
        this.jurisdictionID = jurisdictionID;
    }

    public float getSalesTaxPercent() {
        return salesTaxPercent;
    }

    public void setSalesTaxPercent(float salesTaxPercent) {
        this.salesTaxPercent = salesTaxPercent;
    }

    public boolean isShippingIncludedInTax() {
        return shippingIncludedInTax;
    }

    public void setShippingIncludedInTax(boolean shippingIncludedInTax) {
        this.shippingIncludedInTax = shippingIncludedInTax;
    }
}
