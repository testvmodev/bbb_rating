package com.bbb.core.model.otherservice.response.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartStageResponses {
    @JsonProperty("inventory_id")
    private long inventoryId;
    private String stage;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getStage() {
        if (stage != null) {
            return stage.toUpperCase();
        }
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
