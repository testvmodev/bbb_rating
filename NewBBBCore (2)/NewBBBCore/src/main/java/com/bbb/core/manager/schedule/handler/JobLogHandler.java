package com.bbb.core.manager.schedule.handler;

import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.ScheduleJobLog;
import com.bbb.core.repository.schedule.ScheduleJobConfigRepository;
import com.bbb.core.repository.schedule.ScheduleJobLogRepository;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JobLogHandler {
    @Autowired
    private ScheduleJobLogRepository scheduleJobLogRepository;
    @Autowired
    private ScheduleJobConfigRepository scheduleJobConfigRepository;
    @Autowired
    private JobFailHandler jobFailHandler;

    public void logJobError(ScheduleJob scheduleJob) {
        logJobError(null, scheduleJob);
    }

    public void logJobError(Throwable throwable, ScheduleJob scheduleJob) {
        if (throwable != null) {
            throwable.printStackTrace();
        }

        if (scheduleJob == null) return;

        try {
            if (scheduleJob.getConfig().getLogFailEnable() == null ||
                    !scheduleJob.getConfig().getLogFailEnable()) {
                return;
            }

            ScheduleJobLog log = new ScheduleJobLog();
            log.setJobId(scheduleJob.getConfig().getId());
            log.setStartTime(scheduleJob.startTime);
            log.setFailTime(scheduleJob.failTime);
            log.setCustomLogString(scheduleJob.getCustomLogString());
            log.setCustomLogNumber(scheduleJob.getCustomLogNumber());
            if (throwable != null) {
                String exception = ExceptionUtils.getStackTrace(throwable);
                log.setStackTrace(exception);
            }

            scheduleJobLogRepository.save(log);
        } catch (Exception e) {
            jobFailHandler.handleError(e);
        } finally {
            resetJobLog(scheduleJob);
        }
    }

    public void jogJobSuccess(ScheduleJob scheduleJob) {
        if (scheduleJob == null) return;

        try {
            if (scheduleJob.getConfig().getLogSuccessEnable() == null ||
                    !scheduleJob.getConfig().getLogSuccessEnable()) {
                return;
            }

            ScheduleJobLog log = new ScheduleJobLog();
            log.setJobId(scheduleJob.getConfig().getId());
            log.setStartTime(scheduleJob.startTime);
            log.setFinishTime(scheduleJob.finishTime);
            log.setCustomLogString(scheduleJob.getCustomLogString());
            log.setCustomLogNumber(scheduleJob.getCustomLogNumber());

            scheduleJobLogRepository.save(log);
        } catch (Exception e) {
            jobFailHandler.handleError(e);
        } finally {
            resetJobLog(scheduleJob);
        }
    }

    public void logJobCancel(ScheduleJob scheduleJob) {
        if (scheduleJob == null) return;

        try {
            if (scheduleJob.getConfig().getLogCancelEnable() == null ||
                    !scheduleJob.getConfig().getLogCancelEnable()) {
                return;
            }

            ScheduleJobLog log = new ScheduleJobLog();
            log.setJobId(scheduleJob.getConfig().getId());
            log.setStartTime(scheduleJob.startTime);
            log.setCancelTime(scheduleJob.cancelTime);
            log.setCustomLogString(scheduleJob.getCustomLogString());
            log.setCustomLogNumber(scheduleJob.getCustomLogNumber());

            scheduleJobLogRepository.save(log);
        } catch (Exception e) {
            jobFailHandler.handleError(e);
        } finally {
            resetJobLog(scheduleJob);
        }
    }

    private void resetJobLog(ScheduleJob job) {
        if (job != null) {
            job.startTime = null;
            job.finishTime = null;
            job.cancelTime = null;
            job.failTime = null;
        }
    }
}
