package com.bbb.core.model.request.sort;

public enum MyListingSortField {
    POSTING_TIME("posting_time"),
    STATUS("status_market_listing"),
    ID("market_listing_id"),
    TITLE("title"),
    PRICE("current_listed_price"),
    BEST_OFFER("is_best_offer");

    private final String value;

    MyListingSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
