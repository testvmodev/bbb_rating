package com.bbb.core.model.response.bicycle;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BicycleRatingResponse {

    @Id
    private long id;
    private String bicycleImages;
    private String bicycleYear;
    private String bicycleBrand;
    private String bicycleModel;
    private String bicycleType;
    private String bicycleSize;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBicycleImages() {
        return bicycleImages;
    }

    public void setBicycleImages(String bicycleImages) {
        this.bicycleImages = bicycleImages;
    }

    public String getBicycleYear() {
        return bicycleYear;
    }

    public void setBicycleYear(String bicycleYear) {
        this.bicycleYear = bicycleYear;
    }

    public String getBicycleBrand() {
        return bicycleBrand;
    }

    public void setBicycleBrand(String bicycleBrand) {
        this.bicycleBrand = bicycleBrand;
    }

    public String getBicycleModel() {
        return bicycleModel;
    }

    public void setBicycleModel(String bicycleModel) {
        this.bicycleModel = bicycleModel;
    }

    public String getBicycleType() {
        return bicycleType;
    }

    public void setBicycleType(String bicycleType) {
        this.bicycleType = bicycleType;
    }

    public String getBicycleSize() {
        return bicycleSize;
    }

    public void setBicycleSize(String bicycleSize) {
        this.bicycleSize = bicycleSize;
    }
}
