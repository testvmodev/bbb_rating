package com.bbb.core.model.otherservice.request.mail.scorecard.complete;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentScorecardComplete {
    @JsonProperty("tp_name")
    private String tpName;
    @JsonProperty("bike_name")
    private String bikeName;
    @JsonProperty("delivery_method")
    private String deliveryMethod;

    public String getTpName() {
        return tpName;
    }

    public void setTpName(String tpName) {
        this.tpName = tpName;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }
}
