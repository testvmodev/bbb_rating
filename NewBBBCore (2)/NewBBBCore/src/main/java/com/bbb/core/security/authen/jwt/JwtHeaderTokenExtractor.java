package com.bbb.core.security.authen.jwt;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.security.authen.extractor.TokenExtractor;
import com.bbb.core.security.exception.NoAuthorizationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

@Component
public class JwtHeaderTokenExtractor implements TokenExtractor {

    @Override
    public String extract(String header) {
        if (StringUtils.isBlank(header)) {
//            throw new AuthenticationServiceException("Authorization header cannot be blank!");
//            throw new AuthenticationServiceException(MessageResponses.MESSAGE_MUST_LOGIN);
            throw new NoAuthorizationException(MessageResponses.MESSAGE_MUST_LOGIN);
        }

        if (header.length() < Constants.HEADER_PREFIX.length()) {
            throw new AuthenticationServiceException("Invalid authorization header size.");
        }

        return header.substring(Constants.HEADER_PREFIX.length(), header.length());
    }
}
