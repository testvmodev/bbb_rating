package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.Offer;
import com.bbb.core.model.response.inventory.InventoryDetailResponse;

public class SecretOfferDetailResponse extends Offer {
    private InventoryDetailResponse inventory;
    private String paypalEmailSeller;

    public InventoryDetailResponse getInventory() {
        return inventory;
    }

    public void setInventory(InventoryDetailResponse inventory) {
        this.inventory = inventory;
    }

    public String getPaypalEmailSeller() {
        return paypalEmailSeller;
    }

    public void setPaypalEmailSeller(String paypalEmailSeller) {
        this.paypalEmailSeller = paypalEmailSeller;
    }
}
