package com.bbb.core.common.config.http;

import com.bbb.core.common.Constants;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class WebFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            try {
                filterChain.doFilter(new CacheServletRequestWrapper((HttpServletRequest) servletRequest), servletResponse);
            } catch (HttpMessageNotReadableException | JsonMappingException | NumberFormatException e) {
                filterChain.doFilter(servletRequest, servletResponse);
            } catch (Exception e) {
                filterChain.doFilter(servletRequest, servletResponse);
            }

        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    @Override
    public void destroy() {
    }
}
