package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class DiscountPriceInfo {
    @JacksonXmlProperty(localName = "OriginalRetailPrice")
    private Float originalRetailPrice;

    public Float getOriginalRetailPrice() {
        return originalRetailPrice;
    }

    public void setOriginalRetailPrice(Float originalRetailPrice) {
        this.originalRetailPrice = originalRetailPrice;
    }
}
