package com.bbb.core.repository.salesforce;

import com.bbb.core.model.response.salesforce.SaleForceInfoCallSoldResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleForceInfoCallSoldResponseRepository extends JpaRepository<SaleForceInfoCallSoldResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "inventory.id as inventory_id, inventory.salesforce_id as salesforce_id, inventory_location_shipping.shipping_type as shipping_type, " +
            "inventory.current_listed_price as current_listed_price, " +
            "inventory_type.name as inventory_type_name, " +
            "market_listing.id as listing_id, " +
            "inventory_sale.shipping_address_line as buyer_street, " +
            "inventory_sale.shipping_state as buyer_state, " +
            "inventory_sale.buyer_email as buyer_paypal_email " +
            "FROM " +
            "market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN inventory_sale ON inventory_sale.inventory_id = inventory.id " +
            "LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            "WHERE market_listing.id = :marketListingId and market_listing.is_delete = 0 and inventory.is_delete = 0")
    SaleForceInfoCallSoldResponse findInventoryIdAndSalesforceIdFromMarketListingId(
            @Param(value = "marketListingId") long marketListingId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "inventory.id as inventory_id, inventory.salesforce_id as salesforce_id, inventory_location_shipping.shipping_type as shipping_type, " +
            "inventory.current_listed_price as current_listed_price, " +
            "inventory_type.name as inventory_type_name, " +
            "offer.id as listing_id, " +
            "inventory_sale.shipping_address_line as buyer_street, " +
            "inventory_sale.shipping_state as buyer_state, " +
            "inventory_sale.buyer_email as buyer_paypal_email " +
            "FROM " +
            "offer " +
            "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN inventory_sale ON inventory_sale.inventory_id = inventory.id " +
            "LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            "WHERE offer.id = :offerId and offer.is_delete = 0 and inventory.is_delete = 0")
    SaleForceInfoCallSoldResponse findInventoryIdAndSalesforceIdFromOfferId(
            @Param(value = "offerId") long offerId
    );
}
