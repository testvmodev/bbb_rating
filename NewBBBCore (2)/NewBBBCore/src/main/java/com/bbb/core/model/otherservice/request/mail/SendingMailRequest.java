package com.bbb.core.model.otherservice.request.mail;

import com.bbb.core.model.otherservice.request.mail.offer.content.ReceiveSendingMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SendingMailRequest<T> {
    private ReceiveSendingMailRequest receive;
    @JsonProperty("exchange_type")
    private String exchangeType;
    @JsonProperty("template_key")
    private String templateKey;
    private String subject;
    @JsonProperty("mail_to")
    private List<String> mailTo = new ArrayList<>();
    private List<String> cc = new ArrayList<>();
    private List<String> bcc = new ArrayList<>();
    @JsonProperty("content_mail")
    private T contentMail;

    public ReceiveSendingMailRequest getReceive() {
        return receive;
    }

    public void setReceive(ReceiveSendingMailRequest receive) {
        this.receive = receive;
    }

    public String getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    public String getTemplateKey() {
        return templateKey;
    }

    public void setTemplateKey(String templateKey) {
        this.templateKey = templateKey;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getMailTo() {
        return mailTo;
    }

    public void setMailTo(List<String> mailTo) {
        this.mailTo = mailTo;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public T getContentMail() {
        return contentMail;
    }

    public void setContentMail(T contentMail) {
        this.contentMail = contentMail;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }
}
