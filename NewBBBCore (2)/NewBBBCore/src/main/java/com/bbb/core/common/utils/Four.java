package com.bbb.core.common.utils;

public class Four<T1, T2, T3, T4> extends Third<T1, T2, T3> {
    private T4 four;

    public Four() {}

    public Four(T1 first, T2 second, T3 third, T4 four) {
        super(first, second, third);
        this.four = four;
    }

    public T4 getFour() {
        return four;
    }

    public void setFour(T4 four) {
        this.four = four;
    }
}
