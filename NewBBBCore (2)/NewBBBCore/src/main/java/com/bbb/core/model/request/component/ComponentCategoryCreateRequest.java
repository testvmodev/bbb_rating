package com.bbb.core.model.request.component;

import org.apache.commons.lang3.StringUtils;

public class ComponentCategoryCreateRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean validateCate(){
        return !StringUtils.isBlank(name);
    }
}
