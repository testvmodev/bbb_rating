package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.bbb.core.model.request.tradein.ups.detail.RequestConfig;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipmentRequest {
    @JsonProperty("Request")
    private RequestConfig requestConfig;
    @JsonProperty("Shipment")
    private ShipmentDetail shipment;
    //Custom image format return from ups
    @JsonProperty("LabelSpecification")
    private UPSLabelSpecification UPSLabelSpecification;

    public ShipmentRequest() {}

    public ShipmentRequest(ShipmentDetail shipment) {
        this.shipment = shipment;
    }

    public ShipmentRequest(ShipmentDetail shipment, UPSLabelSpecification UPSLabelSpecification) {
        this.shipment = shipment;
        this.UPSLabelSpecification = UPSLabelSpecification;
    }

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    public ShipmentDetail getShipment() {
        return shipment;
    }

    public void setShipment(ShipmentDetail shipment) {
        this.shipment = shipment;
    }

    public UPSLabelSpecification getUPSLabelSpecification() {
        return UPSLabelSpecification;
    }

    public void setUPSLabelSpecification(UPSLabelSpecification UPSLabelSpecification) {
        this.UPSLabelSpecification = UPSLabelSpecification;
    }
}
