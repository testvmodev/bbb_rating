package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BestOfferDetails {
    @JacksonXmlProperty(localName = "BestOfferEnabled")
    private Boolean bestOfferEnabled;

    public Boolean getBestOfferEnabled() {
        return bestOfferEnabled;
    }

    public void setBestOfferEnabled(Boolean bestOfferEnabled) {
        this.bestOfferEnabled = bestOfferEnabled;
    }
}
