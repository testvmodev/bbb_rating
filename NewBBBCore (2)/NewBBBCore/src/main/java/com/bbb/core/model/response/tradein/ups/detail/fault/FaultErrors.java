package com.bbb.core.model.response.tradein.ups.detail.fault;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FaultErrors {
    @JsonProperty(value = "ErrorDetail")
    private FaultErrorDetail errorDetail;

    public FaultErrorDetail getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(FaultErrorDetail errorDetail) {
        this.errorDetail = errorDetail;
    }
}
