package com.bbb.core.common.ebay;

import java.util.Arrays;
import java.util.List;

public interface EbayValue {
    String ERROR_LANGUAGE = "en_US";
    String WARING_LEVEL = "High";
    String COUNTRY_US = "US";
    String SITE_US = "US";
    String CURRENCY_USD = "USD";
    long CONDITION_ID_DEFAULT = 3000;
    int DISPATCH_TIME_MAX = 3;
    String FIXED_PRICE_ITEM = "FixedPriceItem";
    String PAY_PAL = "PayPal";
    String PAY_PAL_EMAIL_DEFAULT = "thanh.le@vmodev.com";
    int POST_CODE_DEFAULT = 95125;
    int CATEGORY_BICYCLE = 177831;
    String RETURN_ACCEPTED_OPTION_DEFAULT = "ReturnsAccepted";
    String REFUND_OPTION = "MoneyBack";
    String RETURN_WITH_IN_OPTION = "Days_30";
    String DESCRIPTION_RETURN = "Text description of return policy details";
    String SHIPPING_COST_PAID_BY_OPTION = "Buyer";
    String POST_CODE_BBB = "95125";
    String MEASUREMENT_UNIT = "English";
    String PAYMENT_INTRODUCTION = "Payment must be received within 7 business days of purchase.";
    String STATE_DEFAULT = "CA";
    String SHIPPING_SERVICE_DEFAULT = "USPSPriority";
    int SHIPPING_SERVICE_PRIORITY = 1;
    String SKU_DEFAULT = "RLauren_Wom_TShirt_Pnk_S";
    int QUALITY_DEFAULT = 10;
    long SHIPPING_PROFILE_ID = 5984775000L;
    long RETURN_PROFILE_ID = 5984777000L;
    long PAYMENT_PROFILE_ID = 5984774000L;
    String MEASUREMENT_SYSTEM = "English";
    String UNIT_LBS = "lbs";
    String UNIT_OZ = "oz";

    String ERROR_EBAY = "Error";
    int ERROR_CODE_CLOSE_ACUTION = 1047;
    long INVALID_TOKEN_CODE = 21916984L;
    long EXPIRE_TOKEN_CODE = 21917053L;;
    long INVALID_EBAY_TOKEN_CODE = 931L;;


    String URL_EBAY_NORMAL = "/ws/api.dll";


    //notification ebay
    String AUCTION_CHECKOUT_COMPLETE = "AuctionCheckoutComplete";
    String ITEM_UNSOLD = "ItemUnsold";
    String END_OF_AUCTION = "EndOfAuction";

    //api
    String ADD_ITEM = "AddItem";
    String ADD_ITEMS = "AddItems";
    String END_ITEM = "EndItem";
    String END_ITEMS = "EndItems";
    String FETCH_TOKEN = "FetchToken";
    int MAX_ITEMS_LIST_EBAY = 0;
    int MAX_ITEMS_RELIST_EBAY = 5;
    int MAX_START_ASYNC = 5;
    int MAX_END_ITEM = 10;

}
