package com.bbb.core.model.request.common;

public enum CommonComponentType {
    CONDITION("condition"),
    BICYCLE_TYPE("bicycle_type"),
    UPGRADE_COMPONENT("upgrade_component"),
    COMPONENT_TYPE_CUSTOM_QUOTE("component_type_custom_quote"),
    BRAND_BICYCLE("brand_bicycle"),
    ALL_BRAND_BICYCLE("all_brand_bicycle"),
    ALL_MODEL_BICYCLE("all_model_bicycle"),
    ALL_YEAR_BICYCLE("all_year_bicycle"),
    ALL_SIZE_INV("all_size_inv"),
    ALL_FRAME_MATERIAL_INV("all_frame_material_inv"),
    ALL_BRAKE_TYPE_INV("all_brake_type_inv"),
    STATUS_TRADE_IN("status_trade_in"),
    CONFIG_MARKET("config market"),
    EBAY_CATEGORY_PRIMARY("ebay_category_primary"),
    FILTER_INVENTORY_ACTIVE("filter_inventory_active"),
    FILTER_INVENTORY_INACTIVE("filter_inventory_in_active"),
    EBAY_LISTING_TYPE("ebay_listing_type"),
    LISTING_EBAY_DURATION("Listing ebay duration"),
    DETAIL_BICYCLE_MY_LISTING("Detail bicycle my listing"),
    ALL_SUSPENSION("Suspension"),
    ALL_GENDER("Gender"),
    ALL_WHEEL_SIZE("All wheel sizes");
    private final String value;

    CommonComponentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
