package com.bbb.core.model.response.ebay.enditem;

import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "EndItemsResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
public class EndItemsResponse extends BaseEbayResponse {
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "EndItemResponseContainer")
    private List<EndItemResponseContainer> endItemResponseContainers;

    @JacksonXmlElementWrapper(useWrapping=false)
    public List<EndItemResponseContainer> getEndItemResponseContainers() {
        return endItemResponseContainers;
    }

    @JacksonXmlElementWrapper(useWrapping=false)
    public void setEndItemResponseContainers(List<EndItemResponseContainer> endItemResponseContainers) {
        this.endItemResponseContainers = endItemResponseContainers;
    }
}
