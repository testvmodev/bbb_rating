package com.bbb.core.model.response.market.home;

import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DealRecommendInvAucRes extends DealRecommendResponse {
    @Id
    private Long inventoryAuctionId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private long duration;

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

}
