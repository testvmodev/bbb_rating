package com.bbb.core.model.response.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillingWeight {
    @JsonProperty(value = "Weight")
    private String weight;
    @JsonProperty(value = "UnitOfMeasurement")
    private UnitOfMeasurement unitOfMeasurement;

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public UnitOfMeasurement getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }
}
