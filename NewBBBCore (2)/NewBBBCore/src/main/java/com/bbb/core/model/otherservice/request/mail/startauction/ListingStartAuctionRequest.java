package com.bbb.core.model.otherservice.request.mail.startauction;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListingStartAuctionRequest {

    /**
     * auction_inv_id : string
     * inv_id : string
     * title : string
     * start_pricing : number
     * imageUrl : string
     * listed_price : number
     * msrc_price : number
     * fullName : string
     */

    @JsonProperty("auction_inv_id")
    private String auctionInvId;
    @JsonProperty("inv_id")
    private String invId;
    private String title;
    @JsonProperty("start_pricing")
    private double startPricing;
    private String imageUrl;
    @JsonProperty("listed_price")
    private Double listedPrice;
    @JsonProperty("msrc_price")
    private Double msrcPrice;
    private String fullName;

    public String getAuctionInvId() {
        return auctionInvId;
    }

    public void setAuctionInvId(String auctionInvId) {
        this.auctionInvId = auctionInvId;
    }

    public String getInvId() {
        return invId;
    }

    public void setInvId(String invId) {
        this.invId = invId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getStartPricing() {
        return startPricing;
    }

    public void setStartPricing(double startPricing) {
        this.startPricing = startPricing;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getListedPrice() {
        return listedPrice;
    }

    public void setListedPrice(Double listedPrice) {
        this.listedPrice = listedPrice;
    }

    public Double getMsrcPrice() {
        return msrcPrice;
    }

    public void setMsrcPrice(Double msrcPrice) {
        this.msrcPrice = msrcPrice;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
