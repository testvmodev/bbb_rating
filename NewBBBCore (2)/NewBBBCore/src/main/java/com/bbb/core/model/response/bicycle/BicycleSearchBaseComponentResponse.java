package com.bbb.core.model.response.bicycle;

import com.bbb.core.model.response.ContentId;
import com.bbb.core.model.response.embedded.ContentCommonId;

import java.util.List;

public class BicycleSearchBaseComponentResponse {
    private List<ContentCommonId> bicycleBrands;
    private List<ContentCommonId> bicycleYears;

    public List<ContentCommonId> getBicycleBrands() {
        return bicycleBrands;
    }

    public void setBicycleBrands(List<ContentCommonId> bicycleBrands) {
        this.bicycleBrands = bicycleBrands;
    }

    public List<ContentCommonId> getBicycleYears() {
        return bicycleYears;
    }

    public void setBicycleYears(List<ContentCommonId> bicycleYears) {
        this.bicycleYears = bicycleYears;
    }
}
