package com.bbb.core.repository.rating;

import com.bbb.core.model.response.bicycle.BicycleRatingResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BicycleRatingResponseRepository extends JpaRepository<BicycleRatingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "inventory_bicycle.inventory_id as id, " +
            "inventory_image.image as bicycle_images, " +
            "bicycle_brand.name as bicycle_brand, " +
            "bicycle_model.name as bicycle_model, " +
            "bicycle_year.name as bicycle_year, " +
            "bicycle_type.name as bicycle_type, " +
            "bicycle_size.name as bicycle_size " +

            "FROM rating  " +

            "LEFT JOIN market_listing  " +
            "ON rating.market_listing_id = market_listing.id  " +
            "AND market_listing.is_delete = 0  " +

            "LEFT JOIN inventory_auction  " +
            "ON rating.inventory_auction_id = inventory_auction.id  " +
            "AND inventory_auction.is_delete = 0  " +

            "LEFT JOIN inventory  " +
            "ON market_listing.inventory_id = inventory.id  " +
            "OR inventory_auction.inventory_id = inventory.id " +
            "AND inventory.is_delete = 0  " +

            "LEFT JOIN inventory_image " +
            "ON inventory.id = inventory_image.inventory_id " +

            "LEFT JOIN inventory_bicycle " +
            "ON inventory.id = inventory_bicycle.inventory_id " +

            "LEFT JOIN bicycle_brand " +
            "ON inventory_bicycle.brand_id = bicycle_brand.id " +

            "LEFT JOIN bicycle_model " +
            "ON inventory_bicycle.model_id = bicycle_model.id " +

            "LEFT JOIN bicycle_year " +
            "ON inventory_bicycle.year_id = bicycle_year.id " +

            "LEFT JOIN bicycle_type " +
            "ON inventory_bicycle.type_id = bicycle_type.id " +

            "LEFT JOIN bicycle_size " +
            "ON inventory_bicycle.size_id = bicycle_size.id " +

            "WHERE " +
            "(:marketListingId IS NULL OR rating.market_listing_id = :marketListingId) " +
            "AND (:inventoryAuctionId IS NULL OR rating.inventory_auction_id = :inventoryAuctionId)"
    )
    BicycleRatingResponse getBicycle(
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId
    );
}
