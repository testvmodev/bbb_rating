package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.response.bicycle.BicycleYearModelResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleYearModelResponseRepository extends JpaRepository<BicycleYearModelResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.id AS bicycle_id, " +
            "bicycle.model_id AS model_id, " +
            "bicycle.brand_id AS brand_id, " +
            "bicycle.year_id AS year_id, " +
            "bicycle_year.name AS year_name, " +
            "bicycle_type.id AS bicycle_type_id, " +
            "bicycle.image_default AS image_default " +
            "FROM bicycle " +

            //unique years
            "JOIN " +
                "(SELECT MIN(bicycle.id) AS bicycle_id " +
                "FROM bicycle " +
                "WHERE model_id = :modelId " +
                "AND brand_id = :brandId " +
                "AND is_delete = 0 " +
                "GROUP BY bicycle.year_id, bicycle.model_id, bicycle.brand_id" +
                ") as uni " +
            "ON bicycle.id = uni.bicycle_id " +

            "JOIN bicycle_year " +
            "ON bicycle.year_id = bicycle_year.id " +
            "AND bicycle_year.is_delete = 0 " +
            "AND bicycle_year.is_approved = 1 " +

            "JOIN bicycle_model " +
            "ON bicycle.model_id = bicycle_model.id " +
            "AND bicycle_model.is_delete = 0 " +
            "AND bicycle_model.is_approved = 1 " +

            "JOIN bicycle_brand " +
            "ON bicycle.brand_id = bicycle_brand.id " +
            "AND bicycle_brand.is_delete = 0 " +
            "AND bicycle_brand.is_approved = 1 " +

            "LEFT JOIN bicycle_type " +
            "ON bicycle.type_id = bicycle_type.id " +
            "AND bicycle_type.is_delete = 0 " +

            "WHERE bicycle.is_delete = 0 " +
            "AND bicycle.active = 1 " +
            "AND (:excludeYearId IS NULL OR bicycle.year_id != :excludeYearId)"
    )
    List<BicycleYearModelResponse> findFromBrandModel(
            @Param("brandId") long brandId,
            @Param("modelId") long modelId,
            @Param("excludeYearId") Long excludeYearId
    );
}
