package com.bbb.core.model.response.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Error {
    @JacksonXmlProperty(localName = "ShortMessage")
    private String shortMessage;

    @JacksonXmlProperty(localName = "LongMessage")
    private String longMessage;

    @JacksonXmlProperty(localName = "ErrorCode")
    private long errorCode;

    @JacksonXmlProperty(localName = "SeverityCode")
    private String severityCode;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "ErrorParameters")
    private List<ErrorParameter> errorParameters;

    @JacksonXmlProperty(localName = "ErrorClassification")
    private String errorClassification;


    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public String getLongMessage() {
        return longMessage;
    }

    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getSeverityCode() {
        return severityCode;
    }

    public void setSeverityCode(String severityCode) {
        this.severityCode = severityCode;
    }

    public List<ErrorParameter> getErrorParameters() {
        return errorParameters;
    }

    public void setErrorParameters(List<ErrorParameter> errorParameters) {
        this.errorParameters = errorParameters;
    }

    public String getErrorClassification() {
        return errorClassification;
    }

    public void setErrorClassification(String errorClassification) {
        this.errorClassification = errorClassification;
    }
}
