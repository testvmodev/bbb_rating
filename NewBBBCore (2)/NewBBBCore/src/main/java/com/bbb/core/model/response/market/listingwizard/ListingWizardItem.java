package com.bbb.core.model.response.market.listingwizard;

import com.bbb.core.model.database.type.ListingTypeEbay;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class ListingWizardItem {
    @Id
    private Long marketListingId;
    private String lastMsgError;
    private StatusMarketListing status;
    private long marketPlaceId;
    private long marketPlaceConfigId;
    @JsonIgnore
    private long inventoryId;
    @Transient
    private String basePath;
    private LocalDateTime timeListed;
    private Long itemId;


    private Boolean isBestOffer;
    private Float minimumOfferAutoAcceptPrice;
    private Float bestOfferAutoAcceptPrice;
    private Long ebayCategoryId;
    private String ebayCategoryName;
    private Long subEbayCategoryId;
    private String subEbayCategoryName;
    private ListingTypeEbay listingTypeEbay;
    private Long ebayListingDurationId;
    private String ebayListingDurationName;


    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getLastMsgError() {
        return lastMsgError;
    }

    public void setLastMsgError(String lastMsgError) {
        this.lastMsgError = lastMsgError;
    }

    public StatusMarketListing getStatus() {
        return status;
    }

    public void setStatus(StatusMarketListing status) {
        this.status = status;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public long getMarketPlaceConfigId() {
        return marketPlaceConfigId;
    }

    public void setMarketPlaceConfigId(long marketPlaceConfigId) {
        this.marketPlaceConfigId = marketPlaceConfigId;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public LocalDateTime getTimeListed() {
        return timeListed;
    }

    public void setTimeListed(LocalDateTime timeListed) {
        this.timeListed = timeListed;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Long getEbayCategoryId() {
        return ebayCategoryId;
    }

    public void setEbayCategoryId(Long ebayCategoryId) {
        this.ebayCategoryId = ebayCategoryId;
    }

    public String getEbayCategoryName() {
        return ebayCategoryName;
    }

    public void setEbayCategoryName(String ebayCategoryName) {
        this.ebayCategoryName = ebayCategoryName;
    }

    public Long getSubEbayCategoryId() {
        return subEbayCategoryId;
    }

    public void setSubEbayCategoryId(Long subEbayCategoryId) {
        this.subEbayCategoryId = subEbayCategoryId;
    }

    public String getSubEbayCategoryName() {
        return subEbayCategoryName;
    }

    public void setSubEbayCategoryName(String subEbayCategoryName) {
        this.subEbayCategoryName = subEbayCategoryName;
    }

    public ListingTypeEbay getListingTypeEbay() {
        return listingTypeEbay;
    }

    public void setListingTypeEbay(ListingTypeEbay listingTypeEbay) {
        this.listingTypeEbay = listingTypeEbay;
    }

    public Long getEbayListingDurationId() {
        return ebayListingDurationId;
    }

    public void setEbayListingDurationId(Long ebayListingDurationId) {
        this.ebayListingDurationId = ebayListingDurationId;
    }

    public String getEbayListingDurationName() {
        return ebayListingDurationName;
    }

    public void setEbayListingDurationName(String ebayListingDurationName) {
        this.ebayListingDurationName = ebayListingDurationName;
    }
}
