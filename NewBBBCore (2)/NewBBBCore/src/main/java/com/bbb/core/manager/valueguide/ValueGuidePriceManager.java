package com.bbb.core.manager.valueguide;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.manager.templaterest.RestValueGuideManager;
import com.bbb.core.model.database.PrivatePartyValueModifier;
import com.bbb.core.model.database.ValueGuidePrice;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.otherservice.response.valueguide.PredictionPriceResponse;
import com.bbb.core.model.otherservice.response.valueguide.VGBaseResponse;
import com.bbb.core.model.otherservice.response.valueguide.ValueGuideResponse;
import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import com.bbb.core.repository.reout.BrandModelYearRepository;
import com.bbb.core.repository.valueguide.PrivatePartyValueModifierRepository;
import com.bbb.core.repository.valueguide.ValueGuidePriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ValueGuidePriceManager {
    @Autowired
    private RestValueGuideManager restValueGuideManager;
    @Autowired
    private ValueGuidePriceRepository valueGuidePriceRepository;
    @Autowired
    private BrandModelYearRepository brandModelYearRepository;
    @Autowired
    private PrivatePartyValueModifierRepository privatePartyValueModifierRepository;

//    public Float getValueGuidePrice(ConditionInventory condition, long brandId, long modelId, long yearId) {
//        List<ValueGuideResponse> responses = getValueGuidePrices(brandId, modelId, yearId);
//
//        if (responses == null || responses.size() < 1) {
//            return null;
//        }
//
//        for (ValueGuideResponse res : responses) {
//            if (res.getConditionInventory().equals(condition.getValue())) {
//                return res.getAverageValue();
//            }
//        }
//        return null;
//    }

    public Float getValueGuidePriceLocal(ConditionInventory condition, long brandId, long modelId, long yearId) {
        ValueGuidePrice valueGuidePrice = valueGuidePriceRepository.findOne(brandId, modelId, yearId + "");
        if (valueGuidePrice == null) {
            return null;
        }
        switch (condition) {
            case EXCELLENT:
                return getValueAverage(valueGuidePrice.getPriceExcellent());
            case VERY_GOOD:
                return getValueAverage(valueGuidePrice.getPriceVeryGood());
            case GOOD:
                return getValueAverage(valueGuidePrice.getPriceGood());
            default:
                return getValueAverage(valueGuidePrice.getPriceFair());
        }
    }

    public List<Pair<ConditionInventory, Float>> getValueGuidePrices(
            long brandId, long modelId, long yearId, Float msrp
    ) {
        VGBaseResponse<PredictionPriceResponse> response = null;
        if (msrp != null) {
            BrandModelYear brandModelYear = brandModelYearRepository.findOne(
                    brandId, modelId, yearId
            );

            try {
                response = restValueGuideManager.getPredictionPrice(
                        brandModelYear.getBrandName(),
                        brandModelYear.getModelName(),
                        brandModelYear.getYearName(),
                        msrp
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return getValueGuidePrices(brandId, modelId, yearId, response);
    }


    public List<Pair<ConditionInventory, Float>> getValueGuidePrices(
            long brandId, long modelId, long yearId, VGBaseResponse<PredictionPriceResponse> response
    ) {
        if (response != null && response.getData() != null && response.getData().getPrediction() != null) {
            Float price = response.getData().getPrediction();
            List<Pair<ConditionInventory, Float>> prices = new ArrayList<>();
            prices.add(new Pair<>(
                    ConditionInventory.EXCELLENT,
                    price * (1 + Constants.EXCELLENT_PERCENT)));
            prices.add(new Pair<>(
                    ConditionInventory.VERY_GOOD,
                    price * (1 + Constants.VERY_GOOD_PERCENT)));
            prices.add(new Pair<>(
                    ConditionInventory.GOOD,
                    price * (1 + Constants.GOOD_PERCENT)));
            prices.add(new Pair<>(
                    ConditionInventory.FAIR,
                    price * (1 + Constants.FAIR_PERCENT)));
            return prices;
        }

        return getValueGuidesPriceLocal(brandId, modelId, yearId);
    }

    public List<Pair<ConditionInventory, Float>> getValueGuidesPriceLocal(long brandId, long modelId, long yearId) {
        ValueGuidePrice valueGuidePrice = valueGuidePriceRepository.findOne(brandId, modelId, yearId + "");
        List<Pair<ConditionInventory, Float>> res= new ArrayList<>();
        if (valueGuidePrice == null) {
            return res;
        }
        res.add(new Pair<>(ConditionInventory.EXCELLENT, getValueAverage(valueGuidePrice.getPriceExcellent())));
        res.add(new Pair<>(ConditionInventory.VERY_GOOD, getValueAverage(valueGuidePrice.getPriceVeryGood())));
        res.add(new Pair<>(ConditionInventory.GOOD, getValueAverage(valueGuidePrice.getPriceGood())));
        res.add(new Pair<>(ConditionInventory.FAIR, getValueAverage(valueGuidePrice.getPriceFair())));
        return res;
    }

    private Float getValueAverage(String valueGuide) {
        String[] parts = valueGuide.split("-");
        if (parts.length < 2) {
            return null;
        }
        List<Float> values = new ArrayList<>();
        for (String part : parts) {
            values.add(Float.parseFloat(part.replaceAll("[^\\d.]", "")));
        }
        return Math.abs((values.get(0) + values.get(1)) / 2);
    }


//    public List<ValueGuideResponse> getValueGuidePrices(long brandId, long modelId, long yearId) {
//        ValueGuidePrice valueGuidePrice = valueGuidePriceRepository.findOne(brandId, modelId, yearId + "");
//        if (valueGuidePrice == null) {
//            return new ArrayList<>();
//        }
//        List<ValueGuideResponse> responses;
//        try {
//            responses = restValueGuideManager.getTradeInValue(brandId, modelId, yearId);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ArrayList<>();
//        }
//
//        if (responses == null || responses.size() < 1) {
//            return new ArrayList<>();
//        }
//
//        for (ValueGuideResponse res : responses) {
//            String[] parts = res.getValueString().split("-");
//            if (parts.length != 2) {
//                return null;
//            }
//            List<Float> values = new ArrayList<>();
//            for (String part : parts) {
//                values.add(Float.parseFloat(part.replaceAll("[^\\d.]", "")));
//            }
//            res.setAverageValue(Math.abs((values.get(0) + values.get(1)) / 2));
//        }
//
//        return responses;
//    }

    public List<Pair<ConditionInventory, Float>> getPrivatePartyValues(
            long brandId, long modelId, long yearId, Float msrp
    ) {
        VGBaseResponse<PredictionPriceResponse> response = null;
        BrandModelYear brandModelYear = brandModelYearRepository.findOne(
                brandId, modelId, yearId
        );
        try {
            response = restValueGuideManager.getPredictionPrice(
                    brandModelYear.getBrandName(),
                    brandModelYear.getModelName(),
                    brandModelYear.getYearName(),
                    msrp
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response != null && response.getData() != null && response.getData().getPrediction() != null) {
            List<PrivatePartyValueModifier> modifiers = privatePartyValueModifierRepository.findAll();
            List<Pair<ConditionInventory, Float>> prices = new ArrayList<>();
            Float predict = response.getData().getPrediction();

            for (PrivatePartyValueModifier mod : modifiers) {
                prices.add(new Pair<>(mod.getName(), predict * (1 + mod.getValueModifier())));
            }
            return prices;
        }
        return new ArrayList<>();
    }
}
