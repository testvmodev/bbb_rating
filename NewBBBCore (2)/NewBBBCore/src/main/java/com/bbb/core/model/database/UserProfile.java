package com.bbb.core.model.database;


public class UserProfile {

  private long id;
  private String firstName;
  private String lastName;
  private String email;
  private String streetAddress;
  private String city;
  private String state;
  private String postalCode;
  private long providerId;
  private java.sql.Timestamp createdTime;
  private String paypalId;
  private String paypalEmail;
  private String paypalStatus;
  private String paypalUrl;
  private String avatarUrl;
  private long status;
  private String password;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }


  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }


  public long getProviderId() {
    return providerId;
  }

  public void setProviderId(long providerId) {
    this.providerId = providerId;
  }


  public java.sql.Timestamp getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(java.sql.Timestamp createdTime) {
    this.createdTime = createdTime;
  }


  public String getPaypalId() {
    return paypalId;
  }

  public void setPaypalId(String paypalId) {
    this.paypalId = paypalId;
  }


  public String getPaypalEmail() {
    return paypalEmail;
  }

  public void setPaypalEmail(String paypalEmail) {
    this.paypalEmail = paypalEmail;
  }


  public String getPaypalStatus() {
    return paypalStatus;
  }

  public void setPaypalStatus(String paypalStatus) {
    this.paypalStatus = paypalStatus;
  }


  public String getPaypalUrl() {
    return paypalUrl;
  }

  public void setPaypalUrl(String paypalUrl) {
    this.paypalUrl = paypalUrl;
  }


  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }


  public long getStatus() {
    return status;
  }

  public void setStatus(long status) {
    this.status = status;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
