package com.bbb.core.model.response.market.mylisting;

import com.bbb.core.model.database.InventoryCompDetail;
import com.bbb.core.model.database.InventoryImage;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.database.type.StatusMarketListing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class MyListingDetailResponse {
    @Id
    @Column(name = "id")
    private long marketListingId;
    private Long inventoryId;
    private Long bicycleId;
    private Long modelId;
    private Long brandId;
    private Long yearId;
    private Long bicycleTypeId;
    private Long bicycleSizeId;
    private Long inventoryTypeId;
    private Long inventorySizeId;

    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    private String bicycleSizeName;
    private String inventorySizeName;

    private Long brakeTypeId;
    private String brakeName;
    private Long frameMaterialId;
    private String frameMaterialName;

    private Long marketPlaceId;
    private MarketType type;

    private String imageDefault;
    private StatusMarketListing statusMarketListing;

    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
    private Float currentListedPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private Float discountedPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;

    //    private String partnerId;
    private String description;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private String title;
    private ConditionInventory condition;
    private String serialNumber;
    private Boolean isBestOffer;
//    @Transient
//    private String location = ValueCommons.LOCATION_DEFAULT;
    @Transient
    private List<InventoryImage> inventoryImages;
    @Transient
    private List<InventoryCompDetail> inventoryComponents;
    private String paypalEmailSeller;
    private Boolean isInsurance;
    private String zipCode;
    private Float flatRate;
    private Boolean isAllowLocalPickup;
    private OutboundShippingType shippingType;
    private String countryName;
    private String countryCode;
    private String stateName;
    private String stateCode;
    private String cityName;
    private String addressLine;

    private Float eBikeMileage;
    private Float eBikeHours;


    private boolean isDelete;
    @Transient
    private String stageCart;


    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public Long getBicycleSizeId() {
        return bicycleSizeId;
    }

    public void setBicycleSizeId(Long bicycleSizeId) {
        this.bicycleSizeId = bicycleSizeId;
    }

    public Long getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(Long inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public Long getInventorySizeId() {
        return inventorySizeId;
    }

    public void setInventorySizeId(Long inventorySizeId) {
        this.inventorySizeId = inventorySizeId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getBicycleSizeName() {
        return bicycleSizeName;
    }

    public void setBicycleSizeName(String bicycleSizeName) {
        this.bicycleSizeName = bicycleSizeName;
    }

    public String getInventorySizeName() {
        return inventorySizeName;
    }

    public void setInventorySizeName(String inventorySizeName) {
        this.inventorySizeName = inventorySizeName;
    }

    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public String getBrakeName() {
        return brakeName;
    }

    public void setBrakeName(String brakeName) {
        this.brakeName = brakeName;
    }

    public Long getFrameMaterialId() {
        return frameMaterialId;
    }

    public void setFrameMaterialId(Long frameMaterialId) {
        this.frameMaterialId = frameMaterialId;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public Long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(Long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public MarketType getType() {
        return type;
    }

    public void setType(MarketType type) {
        this.type = type;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }

    public List<InventoryImage> getInventoryImages() {
        return inventoryImages;
    }

    public void setInventoryImages(List<InventoryImage> inventoryImages) {
        this.inventoryImages = inventoryImages;
    }

    public List<InventoryCompDetail> getInventoryComponents() {
        return inventoryComponents;
    }

    public void setInventoryComponents(List<InventoryCompDetail> inventoryComponents) {
        this.inventoryComponents = inventoryComponents;
    }

    public String getPaypalEmailSeller() {
        return paypalEmailSeller;
    }

    public void setPaypalEmailSeller(String paypalEmailSeller) {
        this.paypalEmailSeller = paypalEmailSeller;
    }

    public Boolean getInsurance() {
        return isInsurance;
    }

    public void setInsurance(Boolean insurance) {
        isInsurance = insurance;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public String getStageCart() {
        return stageCart;
    }

    public void setStageCart(String stageCart) {
        this.stageCart = stageCart;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }
}
