package com.bbb.core.repository.user;

import com.bbb.core.model.database.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, String> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM account " +
            "WHERE user_id = :userId")
    Account findOne(@Param("userId") String userId);
}
