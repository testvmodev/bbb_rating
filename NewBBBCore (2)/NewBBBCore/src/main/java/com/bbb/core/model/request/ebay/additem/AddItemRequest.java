package com.bbb.core.model.request.ebay.additem;

import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.bbb.core.model.request.ebay.additems.AddItemRequestContainer;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "AddItemRequest", namespace = "urn:ebay:apis:eBLBaseComponents")
public class AddItemRequest extends BaseEbayRequest {

    @JacksonXmlProperty(localName = "Item")
    private Item item;



    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }



}
