package com.bbb.core.manager.schedule.jobs;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.manager.schedule.handler.JobLogHandler;
import org.joda.time.LocalDateTime;

public class DelegatingJobLog implements Runnable {
    private JobLogHandler handler;
    private ScheduleJob job;

    public DelegatingJobLog(JobLogHandler handler, ScheduleJob job) {
        if (handler == null || job == null) {
            throw new NullPointerException();
        }
        this.handler = handler;
        this.job = job;
    }

    @Override
    public void run() {
        try {
            if (job.isRunning) {
                throw new IllegalStateException(MessageResponses.JOB_RUNNING_SAME_TIME);
            }
            job.isRunning = true;
            job.startTime = LocalDateTime.now();
            job.run();
            if (job.isFail) {
                job.failTime = LocalDateTime.now();
                handler.logJobError(job);
            } else {
                job.finishTime = LocalDateTime.now();
                handler.jogJobSuccess(job);
            }
        } catch (Throwable throwable) {
            job.failTime = LocalDateTime.now();
            try {
                job.onFail(throwable);
            } catch (Throwable t) { //death of dead
                t.printStackTrace();
            } finally {
                handler.logJobError(throwable, job);
            }
        } finally {
            job.isRunning = false;
            job.isFail = false;
        }
    }
}