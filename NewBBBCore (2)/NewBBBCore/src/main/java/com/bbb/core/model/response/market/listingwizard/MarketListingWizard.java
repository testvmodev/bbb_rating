package com.bbb.core.model.response.market.listingwizard;

import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MarketListingWizard {
    @Id
    private long marketListingId;
    private long inventoryId;
    private MarketType type;
    private StatusMarketListing status;
    private long marketPlaceConfig;
    private long marketPlaceId;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public MarketType getType() {
        return type;
    }

    public void setType(MarketType type) {
        this.type = type;
    }

    public StatusMarketListing getStatus() {
        return status;
    }

    public void setStatus(StatusMarketListing status) {
        this.status = status;
    }

    public long getMarketPlaceConfig() {
        return marketPlaceConfig;
    }

    public void setMarketPlaceConfig(long marketPlaceConfig) {
        this.marketPlaceConfig = marketPlaceConfig;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }
}
