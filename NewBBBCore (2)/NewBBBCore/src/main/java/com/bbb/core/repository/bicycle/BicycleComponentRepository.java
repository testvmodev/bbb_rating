package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.BicycleComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface BicycleComponentRepository extends JpaRepository<BicycleComponent, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_component WHERE bicycle_component.bicycle_id = :bicycleId")
    List<BicycleComponent> findAllByBicycleId(@Param(value = "bicycleId") long bicycleId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(nativeQuery = true, value = "DELETE FROM bicycle_component " +
            "WHERE bicycle_component.bicycle_id IN :bicycleIds"
    )
    int deleteAllByBicycleIds(
            @Param(value = "bicycleIds") List<Long> bicycleIds
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_component WHERE component_type_id = :compId")
    int getCountByCompId(@Param("compId") long compId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_component WHERE bicycle_id = :bicycleId")
    int getCountByBicycleId(@Param("bicycleId") long bicycleId);
}
