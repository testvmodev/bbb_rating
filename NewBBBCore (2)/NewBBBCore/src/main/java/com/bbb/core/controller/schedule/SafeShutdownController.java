package com.bbb.core.controller.schedule;

import com.bbb.core.common.Constants;
import com.bbb.core.service.schedule.SafeShutdownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SafeShutdownController {
    @Autowired
    private SafeShutdownService service;

    @PostMapping(Constants.URL_SAFE_SHUTDOWN)
    public ResponseEntity safeShutdown() throws Exception {
        return ResponseEntity.ok(service.safeShutdown());
    }
}
