package com.bbb.core.model.response.embedded;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ContentValueCommonId implements Serializable {
    private long id;
    private String name;
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
