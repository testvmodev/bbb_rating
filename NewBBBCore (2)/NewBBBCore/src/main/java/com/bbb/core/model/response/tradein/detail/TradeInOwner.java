package com.bbb.core.model.response.tradein.detail;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class TradeInOwner implements MessageResponses {
    private String name;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String email;
    private String phone;
    private String licenseOrPassport;
    private String serial;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLicenseOrPassport() {
        return licenseOrPassport;
    }

    public void setLicenseOrPassport(String licenseOrPassport) {
        this.licenseOrPassport = licenseOrPassport;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean checkValideRequest() throws ExceptionResponse {
        if (StringUtils.isBlank(name)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_NAME_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(address)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_ADDRESS_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(city)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_CITY_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(state)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_STATE_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (zipCode == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_ZIPCODE_MUST_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(email)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_EMAIL_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(phone)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_PHONE_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(licenseOrPassport)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_LICENCES_PASSPORT_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(serial)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OWNER_SERIAL_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        return true;
    }

    @JsonIgnore
    public boolean isAllEmpty(){
        return StringUtils.isBlank(name) && StringUtils.isBlank(address) &&
                StringUtils.isBlank(city) && StringUtils.isBlank(state) &&
                zipCode == null &&
                StringUtils.isBlank(email) && StringUtils.isBlank(phone)
                && StringUtils.isBlank(licenseOrPassport)
                && StringUtils.isBlank(serial);
    }
}
