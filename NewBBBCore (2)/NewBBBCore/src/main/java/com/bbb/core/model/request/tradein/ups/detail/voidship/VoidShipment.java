package com.bbb.core.model.request.tradein.ups.detail.voidship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VoidShipment {
    @JsonProperty("ShipmentIdentificationNumber")
    private String shipmentIdentificationNumber;

    public VoidShipment() {}

    public VoidShipment(String identificationNumber) {
        this.shipmentIdentificationNumber = identificationNumber;
    }

    public String getShipmentIdentificationNumber() {
        return shipmentIdentificationNumber;
    }

    public void setShipmentIdentificationNumber(String shipmentIdentificationNumber) {
        this.shipmentIdentificationNumber = shipmentIdentificationNumber;
    }
}
