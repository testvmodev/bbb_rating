package com.bbb.core.manager.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.model.database.TradeInPriceConfig;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StatusTradeIn;
import com.bbb.core.repository.tradein.TradeInPriceConfigRepository;

import java.util.Arrays;
import java.util.List;

public class TradeInUtil {
    public static List<TradeInPriceConfig> getPriceConfig(TradeInPriceConfigRepository tradeInPriceConfigRepository, float tradeInValue, Float privatePartyValue) {
        List<TradeInPriceConfig> tradeInPriceConfigs = tradeInPriceConfigRepository.findAll();
        return getPriceConfig(tradeInPriceConfigs, tradeInValue, privatePartyValue);
    }
    public static List<TradeInPriceConfig> getPriceConfig(
            List<TradeInPriceConfig> tradeInPriceConfigs,
            float tradeInValue, Float privatePartyValue
    ) {
        TradeInPriceConfig privateParty = null;
        for (TradeInPriceConfig tradeInPriceConfig : tradeInPriceConfigs) {
            if (Constants.PRIVATE_PARTY_NAME.toUpperCase().equals(tradeInPriceConfig.getName().toUpperCase())) {
                privateParty = tradeInPriceConfig;
                if (privatePartyValue != null) {
                    privateParty.setPrice(privatePartyValue);
                } else {
                    if (privateParty.getPercent() != null) {
                        privateParty.setPrice(privateParty.getPercent() * tradeInValue);
                    }
                }
                break;
            }
        }

//        TradeInPriceConfig privateParty = new TradeInPriceConfig();
//        privateParty.setId(Constants.PRIVATE_PARTY_ID);
//        privateParty.setName(Constants.PRIVATE_PARTY_NAME);
//        privateParty.setPrice(Constants.PRIVATE_PRICE_DEFAULT);
//        tradeInPriceConfigs.add(0, privateParty);

        TradeInPriceConfig selling = null;
        for (TradeInPriceConfig tradeInPriceConfig : tradeInPriceConfigs) {
            if (Constants.SELLING_FEES_EBAY_NAME.toUpperCase().equals(tradeInPriceConfig.getName().toUpperCase())) {
                selling = tradeInPriceConfig;
                if (privateParty != null && privateParty.getPrice() != null && selling.getPercent() != null) {
                    selling.setPrice(selling.getPercent() * privateParty.getPrice());
                    if (selling.getPrice() > selling.getMaxValue()) {
                        selling.setPrice(selling.getMaxValue());
                    }
                }
                break;
            }
        }

//        TradeInPriceConfig selling = new TradeInPriceConfig();
//        selling.setId(Constants.SELLING_FEES_EBAY_ID);
//        selling.setName(Constants.SELLING_FEES_EBAY_NAME);
//        selling.setPrice(privateParty.getPrice() * Constants.SELLING_PERCENT);
//        if (selling.getPrice() > Constants.SELLING_PRICE_MAX) {
//            selling.setPrice(Constants.SELLING_PRICE_MAX);
//        }
//        tradeInPriceConfigs.add(selling);


//
//
        for (TradeInPriceConfig tradeInPriceConfig : tradeInPriceConfigs) {
            if (Constants.TRANSACTION_FEES_NAME.toUpperCase().equals(tradeInPriceConfig.getName().toUpperCase())) {
                if (privateParty != null && privateParty.getPrice() != null && tradeInPriceConfig.getPercent() != null) {
                    Float price = privateParty.getPrice() * tradeInPriceConfig.getPercent();
                    if (tradeInPriceConfig.getAdditional() != null) {
                        price += tradeInPriceConfig.getAdditional();
                    }
                    tradeInPriceConfig.setPrice(price);
                }
            }
        }
//        TradeInPriceConfig tran = new TradeInPriceConfig();
//        tran.setId(Constants.TRANSACTION_FEES_ID);
//        tran.setName(Constants.TRANSACTION_FEES_NAME);
//        tran.setPrice(privateParty.getPrice() * Constants.TRANSACTION_FEES_PERCENT + Constants.TRANSACTION_FEES_PLUS);
//        tradeInPriceConfigs.add(tran);

        for (TradeInPriceConfig tradeInPriceConfig : tradeInPriceConfigs) {
            if (tradeInPriceConfig.getPrice() == null) {
                if (tradeInPriceConfig.getPercent() != null) {
                    tradeInPriceConfig.setPrice(tradeInPriceConfig.getPercent() * tradeInValue);
                }
                if (tradeInPriceConfig.getAdditional() != null) {
                    tradeInPriceConfig.setPrice(tradeInPriceConfig.getPrice() + tradeInPriceConfig.getAdditional());
                }
                if (tradeInPriceConfig.getMaxValue() != null && tradeInPriceConfig.getPrice() > tradeInPriceConfig.getMaxValue()) {
                    tradeInPriceConfig.setPrice(tradeInPriceConfig.getMaxValue());
                }
            }
        }
//
        TradeInPriceConfig totalCost = new TradeInPriceConfig();
        totalCost.setId(Constants.TOTAL_COST_TO_SELLL_PRIVATELY_ID);
        totalCost.setName(Constants.TOTAL_COST_TO_SELLL_PRIVATELY_NAME);
        float totalCostPrice = 0.0f;
        float costShippingPrice = 0;
        for (TradeInPriceConfig tradeInPriceConfig : tradeInPriceConfigs) {
            if (tradeInPriceConfig.getName().toUpperCase().equals(Constants.SHIPPING_COST_NAME.toUpperCase()) && tradeInPriceConfig.getPrice() != null) {
                costShippingPrice = tradeInPriceConfig.getPrice();
            }
            if (tradeInPriceConfig.getName().toUpperCase().equals(Constants.PRIVATE_PARTY_NAME.toUpperCase())) {
                continue;
            }
            if (tradeInPriceConfig.getPrice() != null) {
                totalCostPrice = totalCostPrice + tradeInPriceConfig.getPrice();
            }

        }
        totalCost.setPrice(totalCostPrice);
        tradeInPriceConfigs.add(totalCost);

        if (privateParty != null && privateParty.getPrice() != null) {
            TradeInPriceConfig netAmount = new TradeInPriceConfig();
            netAmount.setId(Constants.NET_AMOUNT_TO_SELLER_ID);
            netAmount.setName(Constants.NET_AMOUNT_TO_SELLER_NAME);
            netAmount.setPrice(privateParty.getPrice() - totalCostPrice);
            tradeInPriceConfigs.add(netAmount);
        }

        return tradeInPriceConfigs;
    }

    public static float getPercent(ConditionInventory condition) {
        switch (condition) {
            case EXCELLENT:
                return Constants.EXCELLENT_PERCENT;
            case VERY_GOOD:
                return Constants.VERY_GOOD_PERCENT;
            case GOOD:
                return Constants.GOOD_PERCENT;
            case FAIR:
            case POOR:
                return Constants.FAIR_PERCENT;
            default:
                return 0;
        }
    }

    public static String getMessage(ConditionInventory condition) {
        switch (condition) {
            case EXCELLENT:
                return Constants.EXCELLENT_MSG;
            case VERY_GOOD:
                return Constants.VERY_GOOD_MSG;
            case GOOD:
                return Constants.GOOD_MSG;
            case FAIR:
            case POOR:
                return Constants.FAIR_MSG;
            default:
                return null;
        }
    }

    public static List<String> getRequiredScorecardComponents() {
        return Arrays.asList(
                ValueCommons.FRAME_MATERIAL_NAME_INV_COMP,
                ValueCommons.GENDER_NAME_INV_COMP,
                ValueCommons.BRAKE_TYPE_NAME_INV_COMP,
                ValueCommons.INVENTORY_FRAME_SIZE_NAME,
                ValueCommons.INVENTORY_WHEEL_SIZE_NAME
        );
    }

    public static List<String> getRequiredCustomQuoteComponents() {
        return Arrays.asList(
                ValueCommons.INVENTORY_FRAME_SIZE_NAME,
                ValueCommons.FRAME_MATERIAL_NAME_INV_COMP,
                ValueCommons.INVENTORY_SHIFTERS_NAME,
                ValueCommons.INVENTORY_FRONT_DERAILLEUR_NAME,
                ValueCommons.INVENTORY_REAR_DERAILLEUR_NAME,
                ValueCommons.INV_COMP_HANDLEBAR_NAME,
                ValueCommons.INV_COMP_STEM_NAME,
                ValueCommons.INV_COMP_CASSETTE_NAME,
                ValueCommons.BRAKE_TYPE_NAME_INV_COMP,
                ValueCommons.INVENTORY_FRONT_BRAKE_NAME,
                ValueCommons.INVENTORY_REAR_BRAKE_NAME,
                ValueCommons.INVENTORY_CRANKSET_NAME,
                ValueCommons.INVENTORY_WHEELS_NAME
        );
    }

    public static List<Integer> getIncompleteStatues() {
        return Arrays.asList(
                StatusTradeIn.INCOMPLETE_DETAIL.getValue(),
                StatusTradeIn.INCOMPLETE_SUMMARY.getValue(),
                StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue(),
                StatusTradeIn.SHIPPING.getValue(),
                StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue()
        );
    }
}
