package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class Transaction {
    @JacksonXmlProperty(localName = "AmountPaid")
    private Currency amountPaid;
    @JacksonXmlProperty(localName = "AdjustmentAmount")
    private Currency adjustmentAmount;
    @JacksonXmlProperty(localName = "ConvertedAdjustmentAmount")
    private Currency convertedAdjustmentAmount;
    @JacksonXmlProperty(localName = "Buyer")
    private Buyer buyer;
    @JacksonXmlProperty(localName = "ShippingDetails")
    private ShippingDetailsNotification shippingDetails;
    @JacksonXmlProperty(localName = "ConvertedAmountPaid")
    private Currency convertedAmountPaid;
    @JacksonXmlProperty(localName = "ConvertedTransactionPrice")
    private Currency convertedTransactionPrice;
    @JacksonXmlProperty(localName = "CreatedDate")
    private LocalDateTime createdDate;
    @JacksonXmlProperty(localName = "DepositType")
    private String depositType;
    @JacksonXmlProperty(localName = "QuantityPurchased")
    private int quantityPurchased;
    @JacksonXmlProperty(localName = "Status")
    private Status status;
    @JacksonXmlProperty(localName = "TransactionID")
    private long transactionID;
    @JacksonXmlProperty(localName = "TransactionPrice")
    private Currency transactionPrice;
    @JacksonXmlProperty(localName = "BestOfferSale")
    private boolean bestOfferSale;
    @JacksonXmlProperty(localName = "ExternalTransaction")
    private ExternalTransaction externalTransaction;
    @JacksonXmlProperty(localName = "ShippingServiceSelected")
    private ShippingServiceSelected shippingServiceSelected;
    @JacksonXmlProperty(localName = "BuyerMessage")
    private String buyerMessage;
    @JacksonXmlProperty(localName = "ContainingOrder")
    private ContainingOrder containingOrder;

    @JacksonXmlProperty(localName = "TransactionSiteID")
    private String transactionSiteID;
    @JacksonXmlProperty(localName = "Platform")
    private String platform;
    @JacksonXmlProperty(localName = "PayPalEmailAddress")
    private String payPalEmailAddress;
    @JacksonXmlProperty(localName = "BuyerGuaranteePrice")
    private Currency buyerGuaranteePrice;
    @JacksonXmlProperty(localName = "ActualShippingCost")
    private Currency actualShippingCost;
    @JacksonXmlProperty(localName = "ActualHandlingCost")
    private Currency actualHandlingCost;
    @JacksonXmlProperty(localName = "OrderLineItemID")
    private String orderLineItemID;
    @JacksonXmlProperty(localName = "IsMultiLegShipping")
    private boolean isMultiLegShipping;
    @JacksonXmlProperty(localName = "IntangibleItem")
    private boolean intangibleItem;
    @JacksonXmlProperty(localName = "MonetaryDetails")
    private MonetaryDetails monetaryDetails;
    @JacksonXmlProperty(localName = "ExtendedOrderID")
    private String extendedOrderID;
    @JacksonXmlProperty(localName = "eBayPlusTransaction")
    private boolean eBayPlusTransaction;

    public Currency getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Currency amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Currency getAdjustmentAmount() {
        return adjustmentAmount;
    }

    public void setAdjustmentAmount(Currency adjustmentAmount) {
        this.adjustmentAmount = adjustmentAmount;
    }

    public Currency getConvertedAdjustmentAmount() {
        return convertedAdjustmentAmount;
    }

    public void setConvertedAdjustmentAmount(Currency convertedAdjustmentAmount) {
        this.convertedAdjustmentAmount = convertedAdjustmentAmount;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    public ShippingDetailsNotification getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(ShippingDetailsNotification shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    public Currency getConvertedAmountPaid() {
        return convertedAmountPaid;
    }

    public void setConvertedAmountPaid(Currency convertedAmountPaid) {
        this.convertedAmountPaid = convertedAmountPaid;
    }

    public Currency getConvertedTransactionPrice() {
        return convertedTransactionPrice;
    }

    public void setConvertedTransactionPrice(Currency convertedTransactionPrice) {
        this.convertedTransactionPrice = convertedTransactionPrice;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public int getQuantityPurchased() {
        return quantityPurchased;
    }

    public void setQuantityPurchased(int quantityPurchased) {
        this.quantityPurchased = quantityPurchased;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(long transactionID) {
        this.transactionID = transactionID;
    }

    public Currency getTransactionPrice() {
        return transactionPrice;
    }

    public void setTransactionPrice(Currency transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    public boolean isBestOfferSale() {
        return bestOfferSale;
    }

    public void setBestOfferSale(boolean bestOfferSale) {
        this.bestOfferSale = bestOfferSale;
    }

    public ExternalTransaction getExternalTransaction() {
        return externalTransaction;
    }

    public void setExternalTransaction(ExternalTransaction externalTransaction) {
        this.externalTransaction = externalTransaction;
    }

    public ShippingServiceSelected getShippingServiceSelected() {
        return shippingServiceSelected;
    }

    public void setShippingServiceSelected(ShippingServiceSelected shippingServiceSelected) {
        this.shippingServiceSelected = shippingServiceSelected;
    }

    public String getBuyerMessage() {
        return buyerMessage;
    }

    public void setBuyerMessage(String buyerMessage) {
        this.buyerMessage = buyerMessage;
    }

    public ContainingOrder getContainingOrder() {
        return containingOrder;
    }

    public void setContainingOrder(ContainingOrder containingOrder) {
        this.containingOrder = containingOrder;
    }

    public String getTransactionSiteID() {
        return transactionSiteID;
    }

    public void setTransactionSiteID(String transactionSiteID) {
        this.transactionSiteID = transactionSiteID;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPayPalEmailAddress() {
        return payPalEmailAddress;
    }

    public void setPayPalEmailAddress(String payPalEmailAddress) {
        this.payPalEmailAddress = payPalEmailAddress;
    }

    public Currency getBuyerGuaranteePrice() {
        return buyerGuaranteePrice;
    }

    public void setBuyerGuaranteePrice(Currency buyerGuaranteePrice) {
        this.buyerGuaranteePrice = buyerGuaranteePrice;
    }

    public Currency getActualShippingCost() {
        return actualShippingCost;
    }

    public void setActualShippingCost(Currency actualShippingCost) {
        this.actualShippingCost = actualShippingCost;
    }

    public Currency getActualHandlingCost() {
        return actualHandlingCost;
    }

    public void setActualHandlingCost(Currency actualHandlingCost) {
        this.actualHandlingCost = actualHandlingCost;
    }

    public String getOrderLineItemID() {
        return orderLineItemID;
    }

    public void setOrderLineItemID(String orderLineItemID) {
        this.orderLineItemID = orderLineItemID;
    }

    public boolean isMultiLegShipping() {
        return isMultiLegShipping;
    }

    public void setMultiLegShipping(boolean multiLegShipping) {
        isMultiLegShipping = multiLegShipping;
    }

    public boolean isIntangibleItem() {
        return intangibleItem;
    }

    public void setIntangibleItem(boolean intangibleItem) {
        this.intangibleItem = intangibleItem;
    }

    public MonetaryDetails getMonetaryDetails() {
        return monetaryDetails;
    }

    public void setMonetaryDetails(MonetaryDetails monetaryDetails) {
        this.monetaryDetails = monetaryDetails;
    }

    public String getExtendedOrderID() {
        return extendedOrderID;
    }

    public void setExtendedOrderID(String extendedOrderID) {
        this.extendedOrderID = extendedOrderID;
    }

    public boolean iseBayPlusTransaction() {
        return eBayPlusTransaction;
    }

    public void seteBayPlusTransaction(boolean eBayPlusTransaction) {
        this.eBayPlusTransaction = eBayPlusTransaction;
    }
}
