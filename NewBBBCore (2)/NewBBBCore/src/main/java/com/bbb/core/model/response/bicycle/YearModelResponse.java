package com.bbb.core.model.response.bicycle;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class YearModelResponse {
    @Id
    private YearModelId id;

    private String yearName;

    public YearModelId getId() {
        return id;
    }

    public void setId(YearModelId id) {
        this.id = id;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

}
