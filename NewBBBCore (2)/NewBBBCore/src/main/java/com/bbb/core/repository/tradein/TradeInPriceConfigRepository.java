package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInPriceConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInPriceConfigRepository extends JpaRepository<TradeInPriceConfig, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_price_config")
    List<TradeInPriceConfig> findAll();
}
