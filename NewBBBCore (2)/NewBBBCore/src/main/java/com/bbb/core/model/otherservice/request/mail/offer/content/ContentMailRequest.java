package com.bbb.core.model.otherservice.request.mail.offer.content;

import com.bbb.core.common.IntegratedServices;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentMailRequest {
    private UserMailRequest user;
    private ListingMailRequest listing;
    private OfferMailRequest offer;
    private BuyerMailRequest buyer;
    @JsonProperty("base_path")
    private String basePath = IntegratedServices.BASE_WEB_APP;
    private SellerMailRequest seller;
    private String urlTransactionComplete = IntegratedServices.BASE_WEB_APP;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingMailRequest getListing() {
        return listing;
    }

    public void setListing(ListingMailRequest listing) {
        this.listing = listing;
    }

    public OfferMailRequest getOffer() {
        return offer;
    }

    public void setOffer(OfferMailRequest offer) {
        this.offer = offer;
    }

    public BuyerMailRequest getBuyer() {
        return buyer;
    }

    public void setBuyer(BuyerMailRequest buyer) {
        this.buyer = buyer;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public SellerMailRequest getSeller() {
        return seller;
    }

    public void setSeller(SellerMailRequest seller) {
        this.seller = seller;
    }

    public String getUrlTransactionComplete() {
        return urlTransactionComplete;
    }

    public void setUrlTransactionComplete(String urlTransactionComplete) {
        this.urlTransactionComplete = urlTransactionComplete;
    }
}
