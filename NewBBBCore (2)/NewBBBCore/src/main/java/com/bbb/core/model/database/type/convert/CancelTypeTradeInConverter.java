package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TypeCancelTradeIn;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CancelTypeTradeInConverter implements AttributeConverter<TypeCancelTradeIn, String> {
    @Override
    public String convertToDatabaseColumn(TypeCancelTradeIn typeCancelTradeIn) {
        if (typeCancelTradeIn == null) {
            return null;
        }
        return typeCancelTradeIn.getValue();
    }

    @Override
    public TypeCancelTradeIn convertToEntityAttribute(String s) {
        return TypeCancelTradeIn.findByValue(s);
    }
}
