package com.bbb.core.service.impl.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.s3.S3DownloadImage;
import com.bbb.core.manager.tradein.TradeInManager;
import com.bbb.core.model.request.bicycle.BicycleFromBrandYearModelRequest;
import com.bbb.core.model.request.tradein.tradin.*;
import com.bbb.core.service.tradein.TradeInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class TradeInServiceImpl implements TradeInService {
    @Autowired
    private TradeInManager manager;

//    @Override
//    public Object createTradeIn(TradeInCreateRequest request) throws ExceptionResponse {
//        return manager.createTradeIn(request);
//    }

    @Override
    public Object postTradeIn(long tradeInId, List<MultipartFile> images, boolean isSave) throws ExceptionResponse {
        return manager.postTradeIn(tradeInId, images, isSave);
    }

    @Override
    public S3DownloadImage getTradeInImage(String keyHash) throws ExceptionResponse {
        return manager.getTradeInImage(keyHash);
    }

    @Override
    public Object calcShipping(TradeInCalculateShipRequest request) throws ExceptionResponse {
        return manager.calcShipping(request);
    }

    @Override
    public Object createShipping(TradeInShippingCreateRequest request) throws ExceptionResponse {
        return manager.createShipping(request);
    }

    @Override
    public Object getTradeInBicycleDetail(BicycleFromBrandYearModelRequest request) throws ExceptionResponse {
        return manager.getTradeInBicycleDetail(request);
    }

    @Override
    public Object getAllTradeInReasonDecline() {
        return manager.getAllTradeInReasonDecline();
    }

    @Override
    public Object declineTradeIn(DeclineTradeInRequest request) throws ExceptionResponse {
        return manager.declineTradeIn(request);
    }

    @Override
    public Object createTradeIn(TradeInCreateRequestTwo request) throws ExceptionResponse, MethodArgumentNotValidException {
        return manager.createTradeIn(request);
    }

    @Override
    public Object updateStepDetail(long tradeInId, TradeInUpdateDetailRequest request) throws ExceptionResponse {
        return manager.updateStepDetail(tradeInId, request);
    }

    @Override
    public Object getTradeInDetail(long tradeInId) throws ExceptionResponse {
        return manager.getTradeInDetail(tradeInId);
    }

    @Override
    public Object getAllStatusTradeIn() {
        return manager.getAllStatusTradeIn();
    }

    @Override
    public Object updateStepSummary(long tradeInId, TradeInSummaryRequest request) throws ExceptionResponse {
        return manager.updateStepSummary(tradeInId, request);
    }

    @Override
    public Object getTradeIns(TradeInsRequest request) throws ExceptionResponse {
        return manager.getTradeIns(request);
    }

    @Override
    public Object getDetailTradeIn(long tradeId, int indexStep) throws ExceptionResponse {
        return manager.getDetailTradeIn(tradeId, indexStep);
    }

    @Override
    public Object archiveTradeIn(long tradeInId, boolean isArchive) throws ExceptionResponse {
        return manager.archiveTradeIn(tradeInId, isArchive);
    }

    @Override
    public Object deleteImageTradeIn(long tradeId, List<Long> imageImageTypeIds, List<Long> imageIds) throws ExceptionResponse {
        return manager.deleteImageTradeIn(tradeId, imageImageTypeIds, imageIds);
    }

    @Override
    public Object getTradeInCostCalculator(float privateParty) throws ExceptionResponse {
        return manager.getTradeInCostCalculator(privateParty);
    }

    @Override
    public Object cancelTradeIn(long tradeInId,  boolean isCancel) throws ExceptionResponse {
        return manager.cancelTradeIn(tradeInId, isCancel);
    }

    @Override
    public Object cancelTradeIns(List<Long> tradeInIds, boolean isCancel) throws ExceptionResponse {
        return manager.cancelTradeIns(tradeInIds, isCancel);
    }

    @Override
    public Object getTradeInCommon(long tradeInId) throws ExceptionResponse {
        return manager.getTradeInCommon(tradeInId);
    }

    @Override
    public Object getCountIncomplete() throws ExceptionResponse {
        return manager.getCountIncomplete();
    }
}
