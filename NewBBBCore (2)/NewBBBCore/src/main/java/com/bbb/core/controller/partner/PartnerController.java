package com.bbb.core.controller.partner;

import com.bbb.core.common.Constants;
import com.bbb.core.model.request.partner.PartnerInfoRequest;
import com.bbb.core.service.partner.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PartnerController {
    @Autowired
    private PartnerService service;

    @PostMapping(value = Constants.URL_UPDATE_INFO_PARTNER, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateInfoPartner(
            @PathVariable(value = Constants.ID) String partnerId,
            @RequestBody PartnerInfoRequest request
    ) {
        return new ResponseEntity<>(service.updateInfoPartner(partnerId, request), HttpStatus.OK);
    }
}
