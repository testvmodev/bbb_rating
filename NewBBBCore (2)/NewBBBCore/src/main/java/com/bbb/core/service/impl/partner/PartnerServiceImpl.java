package com.bbb.core.service.impl.partner;

import com.bbb.core.manager.partner.PartnerManager;
import com.bbb.core.model.request.partner.PartnerInfoRequest;
import com.bbb.core.service.partner.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartnerServiceImpl implements PartnerService {
    @Autowired
    private PartnerManager manager;

    @Override
    public Object updateInfoPartner(String partnerId, PartnerInfoRequest request) {
        return manager.updateInfoPartner(partnerId, request);
    }
}
