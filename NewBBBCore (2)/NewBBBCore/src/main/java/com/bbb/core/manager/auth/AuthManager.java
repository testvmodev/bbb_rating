package com.bbb.core.manager.auth;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.templaterest.RestAuthenticationManager;
import com.bbb.core.model.otherservice.request.common.GoogleServiceRequest;
import com.bbb.core.model.otherservice.response.admin.UserAdminInfoResponse;
import com.bbb.core.model.otherservice.response.common.GoogleServiceResponse;
import com.bbb.core.model.otherservice.response.partner.PartnerDetail;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailResponse;
import com.bbb.core.model.otherservice.response.warehouse.WarehouseResponse;
import com.bbb.core.model.response.ListObjResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AuthManager {
    @Autowired
    private RestAuthenticationManager restAuthenticationManager;
    @Autowired
    private IntegratedServices integratedServices;

    public UserDetailResponse getUser(String userId) {
        Map<String, String> pa = new HashMap<>();//RestAuthenticationManager
        pa.put(Constants.ID, userId);
        return restAuthenticationManager.getObjectPath(integratedServices.URL_GET_USER_DETAIL, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), pa, UserDetailResponse.class);
    }

    public UserDetailFullResponse getUserFullInfo(String userId) {
        Map<String, String> pa = new HashMap<>();
        pa.put(Constants.ID, userId);
//        System.out.println("getUserFullInfo HEADER_SECRET_KEY: " + ConfigDataSource.getSecretKey());
        return restAuthenticationManager.getObject(
                integratedServices.URL_GET_USER_DETAIL_FULL_INFO, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), pa, UserDetailFullResponse.class);
    }

    public List<UserDetailFullResponse> getUsersFullInfo(List<String> userIds) {
        if (userIds == null) return null;
        if (userIds.size() < 1) return new ArrayList<>();

        String userIdsParam = String.join(",", userIds);
        Map<String, String> pa = new HashMap<>();
        pa.put("ids", userIdsParam);

        return restAuthenticationManager.getObject(
                integratedServices.URL_GET_USERS_DETAIL_FULL_INFO, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), pa, new ParameterizedTypeReference<List<UserDetailFullResponse>>() {
                });
    }

    public ListObjResponse<UserDetailResponse> getUsers(Map<String, String> param) {
        return restAuthenticationManager.getObjectNotList(integratedServices.URL_GET_USERS,
                Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                param,
                new ParameterizedTypeReference<ListObjResponse<UserDetailResponse>>() {
                }
        );
    }

    public WarehouseResponse getWarehouseDetail(String warehouseId) {
        Map<String, String> pa = new HashMap<>();
        pa.put(Constants.ID, warehouseId);

        return restAuthenticationManager.getObjectPath(
                integratedServices.URL_GET_WAREHOUSE_DETAIL,
                Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                pa, WarehouseResponse.class
        );
    }

    public WarehouseResponse getWarehouseDetail(String warehouseId, String token) {
        Map<String, String> pa = new HashMap<>();
        pa.put(Constants.ID, warehouseId);
        return restAuthenticationManager.getObjectPath(integratedServices.URL_GET_WAREHOUSE_DETAIL, token, pa, WarehouseResponse.class);
    }

    public GoogleServiceResponse getMapGoogleSerive(String category, String data) {
        GoogleServiceRequest request = new GoogleServiceRequest(category, data);

        return restAuthenticationManager.postObject(
                integratedServices.URL_COMMON_GOOGLE_SERVICE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                GoogleServiceResponse.class
        );
    }

    public ListObjResponse<PartnerDetail> getPartnerDetail(List<String> partnerIds) {
        String ids = CommonUtils.convertToListCallApi(partnerIds);
        Map<String, String> param = new HashMap<>();
        param.put("_ids", ids);
        return restAuthenticationManager.getListObject(
                integratedServices.URL_LIST_PARTNER_DETAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                param,
                new ParameterizedTypeReference<ListObjResponse<PartnerDetail>>() {}
        );
    }

    public List<UserAdminInfoResponse> getUserAdminInfo(List<String> userIds){
        String ids = CommonUtils.convertToListCallApi(userIds);
        Map<String, String> param = new HashMap<>();
        param.put("ids", ids);
        return restAuthenticationManager.getObject(
                integratedServices.URL_LIST_ADMIN_DETAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                param,
                new ParameterizedTypeReference<List<UserAdminInfoResponse>>() {}
        );
    }

}
