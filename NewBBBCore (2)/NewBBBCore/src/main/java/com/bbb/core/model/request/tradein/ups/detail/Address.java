package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {
    @JsonProperty(value = "AddressLine")
    private String addressLine;
    @JsonProperty("City")
    private String city;
    @JsonProperty("StateProvinceCode")
    private String stateProvinceCode;
    @JsonProperty("PostalCode")
    private String postalCode;
    @JsonProperty("CountryCode")
    private String countryCode;

    public Address(String addressLine, String city, String stateProvinceCode, String postalCode, String countryCode) {
        this.addressLine = addressLine;
        this.city = city;
        this.stateProvinceCode = stateProvinceCode;
        this.postalCode = postalCode;
        this.countryCode = countryCode;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String AddressLine) {
        this.addressLine = AddressLine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String City) {
        this.city = City;
    }

    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    public void setStateProvinceCode(String StateProvinceCode) {
        this.stateProvinceCode = StateProvinceCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String PostalCode) {
        this.postalCode = PostalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String CountryCode) {
        this.countryCode = CountryCode;
    }
}
