package com.bbb.core.service.impl.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bid.InventoryAuctionManager;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionCreateRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionUpdateRequest;
import com.bbb.core.security.user.UserContext;
import com.bbb.core.service.bid.InventoryAuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InventoryAuctionServiceImpl implements InventoryAuctionService {
    @Autowired
    private InventoryAuctionManager manager;

    @Override
    public Object getInventoryAuctions(InventoryAuctionRequest request, Pageable page) throws ExceptionResponse {
        return manager.getInventoryAuctions(request, page);
    }

    @Override
    public Object createInventoryAuction(InventoryAuctionCreateRequest request) throws ExceptionResponse {
        return manager.createInventoryAuction(request);
    }

    @Override
    public Object updateInventoryAuction(long inventoryAuctionId, InventoryAuctionUpdateRequest request) throws ExceptionResponse {
        return manager.updateInventoryAuction(inventoryAuctionId, request);
    }

    @Override
    public Object getInventoryAuction(long inventoryAuctionId) throws ExceptionResponse {
        return manager.getInventoryAuction(inventoryAuctionId);
    }

    @Override
    public Object getInventoryAuctionAllBid(long inventoryAuctionId) throws ExceptionResponse {
        return manager.getInventoryAuctionAllBid(inventoryAuctionId);
    }
}
