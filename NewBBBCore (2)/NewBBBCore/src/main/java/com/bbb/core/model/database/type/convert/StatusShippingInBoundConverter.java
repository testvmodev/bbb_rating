package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusShippingInBound;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusShippingInBoundConverter implements AttributeConverter<StatusShippingInBound, String> {
    @Override
    public String convertToDatabaseColumn(StatusShippingInBound statusShippingInBound) {
        if ( statusShippingInBound == null ){
            return null;
        }
        return statusShippingInBound.getValue();
    }

    @Override
    public StatusShippingInBound convertToEntityAttribute(String s) {
        return StatusShippingInBound.findByValue(s);
    }
}
