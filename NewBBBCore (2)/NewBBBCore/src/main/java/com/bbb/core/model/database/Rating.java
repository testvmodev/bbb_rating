package com.bbb.core.model.database;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rating")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler" })
public class Rating implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(name = "rate")
  private float rate;
  @Column(name = "comment")
  private String comment;
  @Column(name = "title")
  private String title;
  @Column(name = "rate_time")
  private LocalDateTime rateTime;
  @Column(name = "user_id")
  private String userId;
  @Column(name = "inventory_auction_id")
  private Long inventoryAuctionId;
  @Column(name = "market_listing_id")
  private Long marketListingId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public float getRate() {
    return rate;
  }

  public void setRate(Long rate) {
    this.rate = rate;
  }


  public LocalDateTime getRateTime() {
    return rateTime;
  }

  public void setRateTime(LocalDateTime rateTime) {
    this.rateTime = rateTime;
  }

  public Long getMarketListingId() {
    return marketListingId;
  }

  public void setMarketListingId(Long bicycleId) {
    this.marketListingId = bicycleId;
  }


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getInventoryAuctionId() {
    return inventoryAuctionId;
  }

  public void setInventoryAuctionId(Long inventoryAuctionId) {
    this.inventoryAuctionId = inventoryAuctionId;
  }

}
