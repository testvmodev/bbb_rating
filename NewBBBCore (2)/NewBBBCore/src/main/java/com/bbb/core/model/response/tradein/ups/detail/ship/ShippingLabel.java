package com.bbb.core.model.response.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShippingLabel {
    @JsonProperty("GraphicImage")
    private String graphicImage;

    public String getGraphicImage() {
        return graphicImage;
    }
}
