package com.bbb.core.model.otherservice.request.event;

public class EventMarketListingExpireRequest extends EventMarketListingDeleteRequest {
    public EventMarketListingExpireRequest() {}

    public EventMarketListingExpireRequest(long inventoryId) {
        super(inventoryId);
    }
}
