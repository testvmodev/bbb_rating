package com.bbb.core.manager.notification;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.manager.templaterest.RestSubscriptionManager;
import com.bbb.core.model.database.InventoryAuction;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.otherservice.request.event.EventSubscriptionRequest;
import com.bbb.core.model.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SubscriptionManager {
    @Autowired
    private RestSubscriptionManager restSubscriptionManager;
    @Autowired
    private IntegratedServices integratedServices;

    @Async
    public void triggerPublishAsync(List<MarketListing> bbbListings) {
        for (MarketListing marketListing : bbbListings) {
            triggerPublishSync(marketListing);
        }
    }

    @Async
    public void triggerPublishAsync(MarketListing marketListing) {
        MessageResponse response = triggerPublishSync(marketListing);
    }

    private MessageResponse triggerPublishSync(MarketListing marketListing) {
        EventSubscriptionRequest request = new EventSubscriptionRequest(
                marketListing.getId(),
                integratedServices.SUBSCRIPTION_ACTION_PUBLISH
        );

        return restSubscriptionManager.postObject(
                integratedServices.URL_EVENT_SUBSCRIPTION,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    @Async
    public void triggerRenewAsync(MarketListing marketListing) {
        EventSubscriptionRequest request = new EventSubscriptionRequest(
                marketListing.getId(),
                integratedServices.SUBSCRIPTION_ACTION_RENEW
        );

        MessageResponse response = restSubscriptionManager.postObject(
                integratedServices.URL_EVENT_SUBSCRIPTION,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    @Async
    public void triggerUpdateAsync(MarketListing marketListing) {
        EventSubscriptionRequest request = new EventSubscriptionRequest(
                marketListing.getId(),
                integratedServices.SUBSCRIPTION_ACTION_UPDATE
        );

        MessageResponse response = restSubscriptionManager.postObject(
                integratedServices.URL_EVENT_SUBSCRIPTION,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    public MessageResponse triggerStartedSync(InventoryAuction inventoryAuction) {
        EventSubscriptionRequest request = new EventSubscriptionRequest(
                integratedServices.SUBSCRIPTION_ACTION_START,
                inventoryAuction.getId()
        );
        MessageResponse response = restSubscriptionManager.postObject(
                integratedServices.URL_EVENT_SUBSCRIPTION,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
        return response;
    }
}
