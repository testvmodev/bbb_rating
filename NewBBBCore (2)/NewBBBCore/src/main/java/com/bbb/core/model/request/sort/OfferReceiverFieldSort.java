package com.bbb.core.model.request.sort;

public enum OfferReceiverFieldSort {
    CREATED_DATE,TITLE,CURRENT_LISTED_PRICE,OFFER_PRICE,OFFER_STATUS,LAST_UPDATE
}
