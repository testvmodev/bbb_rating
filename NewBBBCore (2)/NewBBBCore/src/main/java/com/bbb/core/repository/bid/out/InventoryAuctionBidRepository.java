package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionBid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryAuctionBidRepository extends JpaRepository<InventoryAuctionBid, Long> {
//    private long offerId;
//    private float priceOffer;
//    private StatusOffer statusOffer;
//    private LocalDateTime createdDate;
//    @Query(nativeQuery = true, value = "SELECT * " +
//            "FROM offer " +
//            "WHERE offer.inventory_auction_id = :inventoryAuctionId"
//    )

}
