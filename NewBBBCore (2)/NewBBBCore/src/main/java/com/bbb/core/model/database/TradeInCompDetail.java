package com.bbb.core.model.database;

import com.bbb.core.model.database.embedded.TradeInCompDetailId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class TradeInCompDetail {
    @EmbeddedId
    private TradeInCompDetailId id;
    private String value;

    public TradeInCompDetailId getId() {
        return id;
    }

    public void setId(TradeInCompDetailId id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
