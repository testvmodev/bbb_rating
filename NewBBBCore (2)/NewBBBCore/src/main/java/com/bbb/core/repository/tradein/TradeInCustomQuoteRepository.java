package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInCustomQuote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeInCustomQuoteRepository extends JpaRepository<TradeInCustomQuote, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_custom_quote WHERE trade_in_custom_quote.id = :tradeInCustomQuoteId")
    TradeInCustomQuote findOne(
            @Param(value = "tradeInCustomQuoteId") long tradeInCustomQuoteId
    );

}
