package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ClientDetail {
    @JacksonXmlProperty(localName = "AccountNumber")
    private String accountNumber;
    @JacksonXmlProperty(localName = "MeterNumber")
    private String meterNumber;

    public ClientDetail() {}

    public ClientDetail(String accountNumber, String meterNumber) {
        this.accountNumber = accountNumber;
        this.meterNumber = meterNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }
}
