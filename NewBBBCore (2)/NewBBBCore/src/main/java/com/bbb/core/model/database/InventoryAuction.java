package com.bbb.core.model.database;


import com.bbb.core.common.Constants;
import com.bbb.core.model.database.type.StageInventory;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "inventory_auction")
public class InventoryAuction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long inventoryId;
    private long auctionId;
    private float min;
    private boolean isDelete;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private String winnerId;
    private boolean hasBuyItNow;
    private Float buyItNow;
    private boolean isInactive;
    private StageInventory lastStageInventory;
    @Column(columnDefinition = Constants.NUMERIC)
    private float rating;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public boolean isHasBuyItNow() {
        return hasBuyItNow;
    }

    public void setHasBuyItNow(boolean hasBuyItNow) {
        this.hasBuyItNow = hasBuyItNow;
    }

    public Float getBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(Float buyItNow) {
        this.buyItNow = buyItNow;
    }

    public boolean isInactive() {
        return isInactive;
    }

    public void setInactive(boolean inactive) {
        isInactive = inactive;
    }

    public StageInventory getLastStageInventory() {
        return lastStageInventory;
    }

    public void setLastStageInventory(StageInventory lastStageInventory) {
        this.lastStageInventory = lastStageInventory;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
