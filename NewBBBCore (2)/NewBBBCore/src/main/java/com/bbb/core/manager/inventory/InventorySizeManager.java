package com.bbb.core.manager.inventory;

import com.bbb.core.repository.reout.BicycleSizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InventorySizeManager {
    @Autowired
    private BicycleSizeRepository bicycleSizeRepository;

    public Object getAllInventorySize() {
        return bicycleSizeRepository.findAll();
    }
}
