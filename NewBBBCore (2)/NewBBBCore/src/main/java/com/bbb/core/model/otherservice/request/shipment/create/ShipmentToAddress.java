package com.bbb.core.model.otherservice.request.shipment.create;

public class ShipmentToAddress extends ShipmentFromAddress {
    private String warehouseId;

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }
}
