package com.bbb.core.model.database.embedded;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MarketListingTrackingId implements Serializable {
    private long marketListingId;
    private String userId;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
