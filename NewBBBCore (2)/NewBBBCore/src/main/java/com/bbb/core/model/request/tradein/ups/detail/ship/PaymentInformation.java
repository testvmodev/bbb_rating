package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentInformation {
    @JsonProperty("ShipmentCharge")
    private ShipmentCharge shipmentCharge;

    public ShipmentCharge getShipmentCharge() {
        return shipmentCharge;
    }

    public void setShipmentCharge(ShipmentCharge shipmentCharge) {
        this.shipmentCharge = shipmentCharge;
    }
}
