package com.bbb.core.model.response.tradein.bicycle;


import com.bbb.core.model.database.TradeInPriceConfig;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.response.tradein.ConditionDetailResponse;

import java.util.List;

public class ConditionTradeInBicycleResponse extends ConditionDetailResponse{
    private float tradeInValue;
    private List<TradeInPriceConfig> tradeInPriceConfigs;

    public ConditionTradeInBicycleResponse(ConditionInventory condition, float percent, String message, float tradeInValueRoot) {
        super(condition, percent, message, null);
        tradeInValue = tradeInValueRoot; //+ tradeInValueRoot * percent;
    }

    public float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    public List<TradeInPriceConfig> getTradeInPriceConfigs() {
        return tradeInPriceConfigs;
    }

    public void setTradeInPriceConfigs(List<TradeInPriceConfig> tradeInPriceConfigs) {
        this.tradeInPriceConfigs = tradeInPriceConfigs;
    }
}
