package com.bbb.core.model.request.ebay;

import com.bbb.core.common.ebay.EbayValue;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import javax.xml.bind.annotation.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class BestOfferAutoAcceptPrice {
    @JacksonXmlText
    private double value;
    @JacksonXmlProperty(localName = "currencyID", isAttribute = true)
    private String currencyID = EbayValue.CURRENCY_USD;

    public String getValue() {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return formatter.format(value);
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }
}
