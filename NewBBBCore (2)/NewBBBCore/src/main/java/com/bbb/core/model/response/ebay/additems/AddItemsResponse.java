package com.bbb.core.model.response.ebay.additems;

import com.bbb.core.model.response.ebay.additem.AddItemResponse;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "AddItemsResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
public class AddItemsResponse extends AddItemResponse {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "AddItemResponseContainer")
    private List<AddItemResponseContainer> addItemResponseContainers;

    public List<AddItemResponseContainer> getAddItemResponseContainers() {
        return addItemResponseContainers;
    }

    public void setAddItemResponseContainers(List<AddItemResponseContainer> addItemResponseContainers) {
        this.addItemResponseContainers = addItemResponseContainers;
    }
}
