package com.bbb.core.model.response.onlinestore;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.response.market.mylisting.embed.DraftEmbed;
import com.bbb.core.model.response.onlinestore.listing.OnlineStoreListingEmbed;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "market_listing")
public class OnlineStoreListingResponse {
    @Id
    private Long id;
    private DraftEmbed draft;
    private OnlineStoreListingEmbed finished;
    @JoinColumn(insertable = false, updatable = false)
    private LocalDateTime postingTime;
    @JoinColumn(insertable = false, updatable = false)
    private Boolean isBestOffer;
    @JoinColumn(insertable = false, updatable = false)
    private String title;
    @JoinColumn(insertable = false, updatable = false)
    private StatusMarketListing statusMarketListing;
    @JoinColumn(insertable = false, updatable = false)
    private Float currentListedPrice;
    @JoinColumn(insertable = false, updatable = false)
    private String imageDefault;

    public DraftEmbed getDraft() {
        return draft;
    }

    public void setDraft(DraftEmbed draft) {
        this.draft = draft;
    }

    public OnlineStoreListingEmbed getFinished() {
        return finished;
    }

    public void setFinished(OnlineStoreListingEmbed finished) {
        this.finished = finished;
    }

    public LocalDateTime getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(LocalDateTime postingTime) {
        this.postingTime = postingTime;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }
}
