package com.bbb.core.model.schedule;

import com.bbb.core.model.database.type.StatusOffer;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OfferMarketListing {
    @Id
    private long offerId;
    private long marketListingId;
    private long inventoryId;
    private StatusOffer status;
    private String buyerId;
    private String bicycleName;
    private String inventoryName;
    private String imageDefaultInv;
    private String sellerId;
    private String partnerId;

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public StatusOffer getStatus() {
        return status;
    }

    public void setStatus(StatusOffer status) {
        this.status = status;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getImageDefaultInv() {
        return imageDefaultInv;
    }

    public void setImageDefaultInv(String imageDefaultInv) {
        this.imageDefaultInv = imageDefaultInv;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
