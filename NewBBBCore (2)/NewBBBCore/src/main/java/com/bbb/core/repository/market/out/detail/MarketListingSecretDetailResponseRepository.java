package com.bbb.core.repository.market.out.detail;

import com.bbb.core.model.response.market.marketlisting.detail.MarketListingDetailResponse;
import com.bbb.core.model.response.market.marketlisting.detail.MarketListingSecretDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingSecretDetailResponseRepository extends JpaRepository<MarketListingSecretDetailResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.created_time AS created_time, " +
            "market_listing.start_time AS start_time, " +
            "market_listing.end_time AS end_time, " +
            "market_listing.time_listed AS time_listed, " +
            "market_listing.time_sold AS time_sold, " +
            "market_place.type AS market_type, " +
            "market_listing.status AS status, " +
            "market_listing.paypal_email_seller AS paypal_email_seller, " +

            "market_listing.inventory_id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.model_id AS model_id, " +
            "inventory_bicycle.year_id AS year_id, " +
//            "inv_brand.id AS brand_id, " +
//            "inv_model.id AS model_id, " +
//            "inv_year.id AS year_id , " +
            "bicycle.name AS bicycle_name , " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name , " +
            "inventory.bicycle_type_name AS bicycle_type_name, "+
//            "inventory.bicycle_size_name AS bicycle_size_name, "+
            "frame_size.frame_size_name AS frame_size, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.name AS inventory_name, "+
            "inventory.serial_number AS serial_number, "+
            "inventory.title AS title , " +
            "inventory.discounted_price AS discounted_price , " +
            "inventory.current_listed_price AS current_listed_price , " +
            "inventory.msrp_price AS msrp_price , " +
            "inventory.initial_list_price AS initial_list_price , " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price , " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price , " +
            "inventory.image_default AS image_default , " +
            "inventory.description AS inventory_description, " +
            "inventory.status AS status_inventory,  " +
            "inventory.stage AS stage_inventory,  " +
            "inventory.condition AS condition,  " +
            "inventory.serial_number AS serial_number,  " +
            "inventory.seller_id AS seller_id,  " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.seller_is_bbb AS seller_is_bbb, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.county AS county, " +
            "inventory_location_shipping.address_line AS address_line, " +

            "shipping_fee_config.ebay_shipping_profile_id AS ebay_shipping_profile_id, " +
            "CASE inventory_location_shipping.shipping_type " +
            "WHEN :#{T(com.bbb.core.model.database.type.OutboundShippingType).FLAT_RATE_TYPE.getValue()} THEN inventory_location_shipping.flat_rate " +
            "ELSE shipping_fee_config.shipping_fee END AS shipping_fee, " +
            "shipping_fee_config.label AS shipping_profile_label, " +
            "shipping_fee_config.description AS shipping_profile_description " +

            "FROM market_listing " +

            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                "inventory_comp_detail.inventory_id AS inventory_id, " +
                "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN bicycle_type ON inventory.bicycle_type_name = bicycle_type.name " +
            "LEFT JOIN shipping_fee_config ON bicycle_type.id = shipping_fee_config.bicycle_type_id " +

            "WHERE market_listing.id IN :marketListingIds " +
            "AND market_listing.is_delete = 0"
    )
    List<MarketListingSecretDetailResponse> findAllNotDelete(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );
}
