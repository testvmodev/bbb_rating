package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.type.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SecretMarketListingAuctionResponse {
    @Id
    @JsonIgnore
    private long id;
    private Long marketListingId;
    private Long inventoryAuctionId;
    private StatusInventory statusInventory;
    private StageInventory stageInventory;

    private String sellerName;
    private LocalDateTime createdTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private LocalDateTime timeSold;
    private MarketType marketType;
    private StatusMarketListing statusMarketListing;
    private String bicycleSizeName;
    private long inventoryId;
    private long bicycleId;
    private long brandId;
    private String brandName;
    private long modelId;
    private String modelName;
    private long yearId;
    private String yearName;
    private String inventoryTitle;
    private Float discountedPrice;
    private Float currentListedPrice;
    private Float msrpPrice;
    private Float initialListPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private String imageDefault;
    private String inventoryDescription;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public StatusInventory getStatusInventory() {
        return statusInventory;
    }

    public void setStatusInventory(StatusInventory statusInventory) {
        this.statusInventory = statusInventory;
    }

    public StageInventory getStageInventory() {
        return stageInventory;
    }

    public void setStageInventory(StageInventory stageInventory) {
        this.stageInventory = stageInventory;
    }
}
