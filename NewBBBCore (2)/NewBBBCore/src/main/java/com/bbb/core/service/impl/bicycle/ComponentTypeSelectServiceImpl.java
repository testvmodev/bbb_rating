package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.ComponentTypeSelectManager;
import com.bbb.core.model.request.bicycle.CompTypeSelectCreateRequest;
import com.bbb.core.model.request.bicycle.CompTypeSelectUpdateRequest;
import com.bbb.core.model.request.component.ComponentValuesGetRequest;
import com.bbb.core.service.bicycle.ComponentTypeSelectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComponentTypeSelectServiceImpl implements ComponentTypeSelectService {
    @Autowired
    private ComponentTypeSelectManager manager;
    @Override
    public Object createComponentValues(CompTypeSelectCreateRequest request) throws ExceptionResponse {
        return manager.createComponentValues(request);
    }

    @Override
    public Object getDetailComponentValues(ComponentValuesGetRequest request) throws ExceptionResponse {
        return manager.getDetailComponentValues(request);
    }

    @Override
    public Object getOneComponentValue(long componentValueId) throws ExceptionResponse {
        return manager.getOneComponentValue(componentValueId);
    }

    @Override
    public Object updateComponentValue(long id, CompTypeSelectUpdateRequest request) throws ExceptionResponse {
        return manager.updateComponentValues(id, request);
    }

    @Override
    public Object deleteComponentValue(long id) throws ExceptionResponse {
        return manager.deleteComponentValue(id);
    }
}
