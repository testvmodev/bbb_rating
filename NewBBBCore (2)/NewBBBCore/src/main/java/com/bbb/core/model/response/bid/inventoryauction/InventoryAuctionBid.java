package com.bbb.core.model.response.bid.inventoryauction;

import com.bbb.core.model.database.type.StatusOffer;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryAuctionBid {
    @Id
    private long offerId;
    private float priceOffer;
    private StatusOffer statusOffer;
    private LocalDateTime createdDate;

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public float getPriceOffer() {
        return priceOffer;
    }

    public void setPriceOffer(float priceOffer) {
        this.priceOffer = priceOffer;
    }

    public StatusOffer getStatusOffer() {
        return statusOffer;
    }

    public void setStatusOffer(StatusOffer statusOffer) {
        this.statusOffer = statusOffer;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }
}
