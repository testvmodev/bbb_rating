package com.bbb.core.model.request.tradein.tradin;


import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class TradeInCreateRequest {
    private long bicycleId;
    private String serial;
    private long brakeTypeId;
    private String ownerName;
    private String ownerAddress;
    private String ownerCity;
    private String ownerState;
    private String ownerZipCode;
    private String ownerEmail;
    private String ownerPhone;
    private String ownerLicensePassport;
    private String proofName;
    private LocalDateTime proofDate;
    private float tradeInValue;
    private String color;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public String getOwnerCity() {
        return ownerCity;
    }

    public void setOwnerCity(String ownerCity) {
        this.ownerCity = ownerCity;
    }

    public String getOwnerState() {
        return ownerState;
    }

    public void setOwnerState(String ownerState) {
        this.ownerState = ownerState;
    }

    public String getOwnerZipCode() {
        return ownerZipCode;
    }

    public void setOwnerZipCode(String ownerZipCode) {
        this.ownerZipCode = ownerZipCode;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getOwnerLicensePassport() {
        return ownerLicensePassport;
    }

    public void setOwnerLicensePassport(String ownerLicensePassport) {
        this.ownerLicensePassport = ownerLicensePassport;
    }

    public String getProofName() {
        return proofName;
    }

    public void setProofName(String proofName) {
        this.proofName = proofName;
    }

    public LocalDateTime getProofDate() {
        return proofDate;
    }

    public void setProofDate(LocalDateTime proofDate) {
        this.proofDate = proofDate;
    }

    public float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
