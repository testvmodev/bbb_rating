package com.bbb.core.model.response.tradein.detail;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpStatus;

public class TradeInProof implements MessageResponses {
    private String name;
    private LocalDateTime date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean checkValid() throws ExceptionResponse {
        if (StringUtils.isBlank(name)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, PROOF_NAME_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        if (date == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, PROOF_DATE_MUST_IS_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }

        return true;
    }

    @JsonIgnore
    public boolean isEmptyAll() {
        return StringUtils.isBlank(name) && date == null;
    }
}
