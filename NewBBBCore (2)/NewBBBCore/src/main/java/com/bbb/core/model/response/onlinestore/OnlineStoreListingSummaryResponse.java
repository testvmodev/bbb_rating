package com.bbb.core.model.response.onlinestore;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OnlineStoreListingSummaryResponse {
    @Id
    private String storefrontId;
    private int allListing;
    private int forSale;
    private int sold;
    private int expired;
    private int draft;
    private int salePending;

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public int getAllListing() {
        return allListing;
    }

    public void setAllListing(int allListing) {
        this.allListing = allListing;
    }

    public int getForSale() {
        return forSale;
    }

    public void setForSale(int forSale) {
        this.forSale = forSale;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public int getDraft() {
        return draft;
    }

    public void setDraft(int draft) {
        this.draft = draft;
    }

    public int getSalePending() {
        return salePending;
    }

    public void setSalePending(int salePending) {
        this.salePending = salePending;
    }
}
