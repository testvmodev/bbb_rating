package com.bbb.core.repository.market.out;

import com.bbb.core.model.response.market.MarketConfigCommonRes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketConfigCommonResRepository extends JpaRepository<MarketConfigCommonRes, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_place_config.id AS market_config_id," +
            "market_place.id AS market_place_id, " +
            "market_place.type AS type, " +
            "market_place.name AS market_place_name " +
            "FROM " +
            "market_place_config JOIN market_place ON market_place_config.market_place_id = market_place.id " +
            "WHERE market_place_config.is_delete = 0 AND market_place.is_delete = 0"
    )
    List<MarketConfigCommonRes> findAll();
}
