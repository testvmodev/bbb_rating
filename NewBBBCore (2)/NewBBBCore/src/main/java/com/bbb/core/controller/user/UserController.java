package com.bbb.core.controller.user;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Autowired
    private UserService service;

    @PatchMapping(Constants.URL_SECRET_USER_PATH)
    public ResponseEntity updateStatusActive(
            @PathVariable(Constants.ID) String userId,
            @RequestParam("isActive") boolean isActive
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateStatusActive(userId, isActive));
    }

    @DeleteMapping(Constants.URL_SECRET_USER_PATH)
    public ResponseEntity deletedAccount(
            @PathVariable(Constants.ID) String userId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deletedAccount(userId));
    }

    @GetMapping(Constants.URL_SECRET_USER_VERIFY_SAFE)
    public ResponseEntity verifySafeDeactiveDelete(
            @PathVariable(Constants.ID) String userId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.verifySafe(userId));
    }

    @GetMapping(Constants.URL_USER_BASE_STATS)
    public ResponseEntity getUserBasicStats(
            @PathVariable(Constants.ID) String userId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getUserBasicStats(userId));
    }
}
