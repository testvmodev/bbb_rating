package com.bbb.core.model.response.tradein.customquote;

import com.bbb.core.model.database.type.StatusTradeIn;

public class TradeInCustomQuoteCommonRes {
    private Long customQuoteId;
    private long tradeInId;
    private StatusTradeIn status;
    private int indexStep;

    public Long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(Long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public StatusTradeIn getStatus() {
        return status;
    }

    public void setStatus(StatusTradeIn status) {
        this.status = status;
    }

    public int getIndexStep() {
        return indexStep;
    }

    public void setIndexStep(int indexStep) {
        this.indexStep = indexStep;
    }
}
