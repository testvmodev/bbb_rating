package com.bbb.core.repository.rating;

import com.bbb.core.model.response.rating.RatingResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingResponseRepository extends JpaRepository <RatingResponse, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "rating.id as id, " +
            "rating.user_id as name_display, " +
            "rating.rate as rate, " +
            "rating.comment as comment, " +
            "rating.title as title, " +
            "rating.rate_time as rate_time " +

            "FROM rating " +

            "WHERE " +
            "(:marketListingId IS NULL OR rating.market_listing_id = :marketListingId) " +
            "AND (:inventoryAuctionId IS NULL OR rating.inventory_auction_id = :inventoryAuctionId) "+

            "ORDER BY rating.rate_time DESC " +
            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#size} ROWS ONLY "
    )
    List<RatingResponse> findAllRatingResponse(
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId,
            @Param("offset") long offset,
            @Param("size") long size
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1" +
            "rating.id as id, " +
            "rating.user_id as name_display, " +
            "rating.rate as rate, " +
            "rating.comment as comment, " +
            "rating. title as title, " +
            "rating.rate_time as rate_time " +

            "FROM rating " +

            "WHERE " +
            "(:marketListingId IS NULL OR rating.market_listing_id = :marketListingId) " +
            "AND (:inventoryAuctionId IS NULL OR rating.inventory_auction_id = :inventoryAuctionId) "+
            "AND rating.user_id = :userId "
            )
    RatingResponse getOneByUserId(
            @Param("marketListingId") Long marketListingId,
            @Param("inventoryAuctionId") Long inventoryAuctionId,
            @Param("userId") String userId
    );
}
