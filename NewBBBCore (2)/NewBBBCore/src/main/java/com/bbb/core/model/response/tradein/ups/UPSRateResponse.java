package com.bbb.core.model.response.tradein.ups;

import com.bbb.core.model.response.tradein.ups.detail.Money;
import com.bbb.core.model.response.tradein.ups.detail.fault.Fault;
import com.bbb.core.model.response.tradein.ups.detail.rate.RateResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSRateResponse {
    @JsonProperty("Fault")
    private Fault fault;
    @JsonProperty("RateResponse")
    private RateResponse rateResponse;

    public Fault getFault() {
        return fault;
    }

    public void setFault(Fault fault) {
        this.fault = fault;
    }

    public RateResponse getRateResponse() {
        return rateResponse;
    }

    public void setRateResponse(RateResponse rateResponse) {
        this.rateResponse = rateResponse;
    }

    public boolean isSuccess() {
        try {
            return fault == null && rateResponse.getResponseInfo().getResponseStatus().getCode() == 1;
        } catch (Exception e) {
            return false;
        }
    }

    @JsonIgnore
    public String getFaultMessage() {
        return fault.getDetail().getErrors().getErrorDetail().getPrimaryErrorCode().getDescription();
    }

    @JsonIgnore
    public Money getRatedTotalCharge() {
        return rateResponse.getRatedShipment().getTotalCharges();
    }
}
