package com.bbb.core.security.authen.jwt;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.model.database.type.UserRoleType;
import com.bbb.core.security.WebSecurityConfig;
import com.bbb.core.security.exception.JwtExpiredTokenException;
import com.bbb.core.security.user.UserContext;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class JwtTokenFactory {
    @Autowired
    private Environment environment;
    @Autowired
    private WebSecurityConfig webSecurityConfig;

    public UserContext parseToken(String token) {
        Jws<Claims> claimsJws = parseClaims(token);
        Claims body = claimsJws.getBody();
        String username = body.get("email", String.class);
        String id = body.get("_id", String.class);
        //note: primary/top role
        UserRoleType role = UserRoleType.findByValue(body.get("role", String.class));
        String wholesalerId = body.get("wholesaler", String.class);
        String displayName = body.get("display_name", String.class);
        String email = body.get("email", String.class);
        List<String> rawRoles =  body.get("roles", ArrayList.class);
        List<UserRoleType> roles = new ArrayList<>();
        if (rawRoles != null && rawRoles.size() > 0) {
            roles = rawRoles.stream()
                    .map(r -> UserRoleType.findByValue(r))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        roles.add(0, role);
//        Partner partner = null;
//        Object pat = body.get("partner");
//        if (pat != null && pat instanceof LinkedHashMap) {
//            LinkedHashMap<String, String> link = (LinkedHashMap<String, String>) pat;
//            partner = new Partner();
//            partner.setId(link.get("_id"));
//            partner.setName(link.get("name"));
//            partner.setAddress(link.get("address"));
//        }
        String partnerId = body.get("partner", String.class);
        String onlineStoreId = body.get("online_store", String.class);
        String storefrontId = body.get("storefront", String.class);

        UserContext userContext = new UserContext(username, token);
        userContext.setId(id);
        userContext.setUserRoles(roles);
        userContext.setWholesalerId(wholesalerId);
        userContext.setDisplayName(displayName);
        userContext.setEmail(email);
//        userContext.setPartner(partner);
        userContext.setPartnerId(partnerId);
        userContext.setOnlineStoreId(onlineStoreId);
        userContext.setStorefrontId(storefrontId);

        return userContext;
    }

    private Jws<Claims> parseClaims(String token) {
        System.out.println("token: " + token);
        try {
            String jwtSecret = webSecurityConfig.getTokenSecret();
            return Jwts.parser().setSigningKey(
                    new String(Base64.getEncoder().encode(jwtSecret.getBytes()))).parseClaimsJws(
                    token
            );
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {

            ex.printStackTrace();
            throw new BadCredentialsException(MessageResponses.INVALID_TOKEN, ex);
        } catch (ExpiredJwtException expiredEx) {
            throw new JwtExpiredTokenException(expiredEx.getMessage(), token, expiredEx);
        }
    }
}
