package com.bbb.core.model.otherservice.request.mail.auctionexpired;

import com.bbb.core.model.otherservice.request.mail.offer.content.OfferMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentAuctionExpiredRequest {
    private UserMailRequest user;
    private ListingAuctionExpiredRequest listing;
    private OfferMailRequest offer;
    @JsonProperty("base_path")
    private String basePath;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingAuctionExpiredRequest getListing() {
        return listing;
    }

    public void setListing(ListingAuctionExpiredRequest listing) {
        this.listing = listing;
    }

    public OfferMailRequest getOffer() {
        return offer;
    }

    public void setOffer(OfferMailRequest offer) {
        this.offer = offer;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
