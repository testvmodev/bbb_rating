package com.bbb.core.manager.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.Auction;
import com.bbb.core.model.database.type.UserRoleType;
import com.bbb.core.model.request.bid.auction.AuctionCreateRequest;
import com.bbb.core.model.request.bid.auction.AuctionRequest;
import com.bbb.core.model.request.bid.auction.AuctionUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bid.auction.AuctionResponse;
import com.bbb.core.model.response.bid.auction.OfferOfAuctionResponse;
import com.bbb.core.model.response.bid.auction.StatusAuctionResponse;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.bid.out.OfferOfAuctionResponseRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class AuctionManager implements MessageResponses {
    @Autowired
    private AuctionRepository auctionRepository;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferOfAuctionResponseRepository offerOfAuctionResponseRepository;

    public Object getAuction(AuctionRequest request, Pageable pageable) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }
        ListObjResponse<AuctionResponse> response = new ListObjResponse<>();
        List<Auction> auctions = auctionRepository.findAll(
                request.getCreatedById(),
                pageable.getOffset(),
                pageable.getPageSize()
        );


        List<Long> auctionIds = auctions.stream().map(Auction::getId).collect(Collectors.toList());
        if (auctionIds.size() > 0) {
            LocalDateTime now = LocalDateTime.now();
            Map<Long, List<OfferOfAuctionResponse>> mapOfferOfAuction =
                    offerOfAuctionResponseRepository.findAllFromAuction(auctionIds)
                            .stream()
                            .collect(Collectors.groupingBy(OfferOfAuctionResponse::getAuctionId));
            List<AuctionResponse> data =
                    auctions.stream().map(auction -> {
                        AuctionResponse auctionResponse = new AuctionResponse();
                        auctionResponse.setId(auction.getId());
                        auctionResponse.setCreatedById(auction.getCreatedById());
                        auctionResponse.setStartDate(auction.getStartDate());
                        auctionResponse.setEndDate(auction.getEndDate());
                        auctionResponse.setName(auction.getName());
                        auctionResponse.setCreatedTime(auction.getCreatedTime());
                        auctionResponse.setLastUpdate(auction.getLastUpdate());
                        auctionResponse.setStarted(auction.isStarted());
                        auctionResponse.setExpired(auction.isExpired());
                        if (auction.getStartDate().toDate().getTime() > now.toDate().getTime()) {
                            auctionResponse.setStatus(StatusAuctionResponse.PENDING);
                        } else {
                            if (auction.getEndDate().toDate().getTime() < now.toDate().getTime()) {
                                auctionResponse.setStatus(StatusAuctionResponse.CLOSE);
                            } else {
                                auctionResponse.setStatus(StatusAuctionResponse.ACTIVE);
                                auctionResponse.setRemainTimeAuction(auction.getEndDate().toDate().getTime() -
                                        now.toDate().getTime());
                            }
                        }

                        mapOfferOfAuction.forEach((auctionId, inventoryAuctions) -> {
                            if (auctionId == auction.getId()) {
                                auctionResponse.setNumberInventoryAuction(inventoryAuctions.size());
                            }
                        });

                        return auctionResponse;
                    }).collect(Collectors.toList());
            response.setData(data);
            response.setTotalItem(
                    auctionRepository.countNumber(
                            request.getCreatedById()
                    )
            );
        } else {
            response.setData(new ArrayList<>());
            response.setTotalItem(0);
            response.setTotalItem(0);
        }


        response.setPageSize(pageable.getPageSize());
        response.setPage(pageable.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    public Object createAuction(AuctionCreateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getStartDate() == null || request.getEndDate() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TIME_INVALID),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(request.getName())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, NAME_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        LocalDateTime now = LocalDateTime.now();

        if (request.getEndDate().toDate().getTime() < request.getStartDate().toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, END_DATE_AND_START_DATE_INVALID),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getStartDate().toDate().getTime() < now.toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, START_DATE_MUST_MORE_THAN_CURRENT_TIME),
                    HttpStatus.BAD_REQUEST
            );
        }

        Auction auction = new Auction();
        auction.setCreatedById(CommonUtils.getUserLogin());
        auction.setStartDate(request.getStartDate());
        auction.setEndDate(request.getEndDate());
        auction.setName(request.getName());
        if (auction.getStartDate().compareTo(LocalDateTime.now()) <= 0) {
            auction.setStarted(true);
        }

        return auctionRepository.save(auction);
    }

    public Object updateAuction(long auctionId, AuctionUpdateRequest request) throws ExceptionResponse {
        Auction auction = auctionRepository.findOne(auctionId);
        if (auction == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, AUCTION_ID_NOT_FOUND),
                    HttpStatus.NOT_FOUND
            );
        }
        LocalDateTime now = LocalDateTime.now();
        if (auction.getEndDate().toDate().getTime() < now.toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, AUCTION_HAS_END),
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getStartDate() != null) {
            if (request.getStartDate().toDate().getTime() < now.toDate().getTime()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, START_DATE_MUST_MORE_THAN_CURRENT_TIME),
                        HttpStatus.BAD_REQUEST
                );
            }
            auction.setStartDate(request.getStartDate());
        }
        if (request.getEndDate() != null) {
            auction.setEndDate(request.getEndDate());
        }
        if (request.getEndDate() != null && request.getEndDate().toDate().getTime() < auction.getStartDate().toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, AUCTION_WAS_BEGINNING),
                    HttpStatus.NOT_FOUND
            );
        }
        if ( request.getName() != null ) {
            if ( request.getName().trim().equals("")){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, AUTION_NAME_INVALID),
                        HttpStatus.NOT_FOUND
                );
            }
            auction.setName(request.getName());
        }
        auction.setLastUpdate(null);
        return auctionRepository.save(auction);
    }
}
