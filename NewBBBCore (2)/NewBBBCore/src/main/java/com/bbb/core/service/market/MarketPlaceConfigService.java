package com.bbb.core.service.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.MarketPlaceConfigCreateRequest;

public interface MarketPlaceConfigService {
    Object getAllMarketPlaceConfig();

    Object getMarketPlaceConfigDetail(long id) throws ExceptionResponse;

    Object postMarketPlaceConfig(MarketPlaceConfigCreateRequest request) throws ExceptionResponse;

    Object updateMarketPlaceConfig(long id, MarketPlaceConfigCreateRequest request) throws ExceptionResponse;

    Object deleteMarketPlaceConfig(long id) throws ExceptionResponse;
}
