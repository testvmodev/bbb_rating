package com.bbb.core.model.database.type;

public enum ComponentSuspensionInventory {
    RIGID("Rigid"),
    FRONT("Front"),
    FULL("Full");
    private final String value;

    ComponentSuspensionInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ComponentSuspensionInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "Rigid":
                return RIGID;
            case "Front":
                return FRONT;
            case "Full":
                return FULL;
            default:
                return null;
        }
    }
}
