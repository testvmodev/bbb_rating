package com.bbb.core.manager.market;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.MarketPlaceConfig;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.request.market.MarketPlaceCreateRequest;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.market.MarketPlaceDetailResponse;
import com.bbb.core.repository.market.MarketPlaceConfigRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class MarketPlaceManager {
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private MarketPlaceConfigManager marketPlaceConfigManager;

    public Object getAllMarketPlaces() {
        return marketPlaceRepository.findAll();
    }

    public Object getMarketPlaceDetail(long id) throws ExceptionResponse {
        MarketPlace marketPlace = findMarketPlace(id);
        MarketPlaceDetailResponse response = new MarketPlaceDetailResponse();
        response.setMarketPlace(marketPlace);

        MarketPlaceConfig config = marketPlaceConfigRepository.findOneByMarketPlaceId(id, null);
        response.setConfiguration(config);
        return response;
    }

    public Object postMarketPlace(MarketPlaceCreateRequest request) throws ExceptionResponse {
        MarketPlace marketPlace = new MarketPlace();
        request.setName(validateName(request.getName()));

        marketPlace.setName(request.getName());
        marketPlace.setType(request.getMarketType());

        return marketPlaceRepository.save(marketPlace);
    }

    public Object updateMarketPlace(long id, MarketPlaceCreateRequest request) throws ExceptionResponse {
        MarketPlace marketPlace = findMarketPlace(id);

        if (request.getName() != null) {
            marketPlace.setName(validateName(request.getName()));
        }
        if (request.getMarketType() != null) {
            marketPlace.setType(request.getMarketType());
            if (marketPlace.getType() == MarketType.EBAY) {
                MarketPlaceConfig marketPlaceConfig = marketPlaceConfigRepository.findOneByMarketPlaceId(marketPlace.getId(), null);
                if (marketPlaceConfig != null) {
                    marketPlaceConfigManager.validateEbay(marketPlaceConfig);
                }
            }
        }

        return marketPlaceRepository.save(marketPlace);
    }

    public Object deleteMarketPlace(long id) throws ExceptionResponse {
        MarketPlace marketPlace = findMarketPlace(id);
        marketPlace.setDelete(true);
        marketPlaceRepository.save(marketPlace);
        marketPlaceConfigManager.deleteMarketPlaceConfigAsync(marketPlace);
        return new MessageResponse(MessageResponses.MARKET_PLACE_DELETED);
    }

    private MarketPlace findMarketPlace(long id) throws ExceptionResponse {
        MarketPlace marketPlace = marketPlaceRepository.findOne(id);
        if (marketPlace == null || marketPlace.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(
                            ObjectError.ERROR_NOT_FOUND,
                            MessageResponses.MARKET_PLACE_CONFIG_NOT_EXIST
                    ),
                    HttpStatus.NOT_FOUND
            );
        }
        return marketPlace;
    }

    private String validateName(String name) throws ExceptionResponse {
        if (StringUtils.isBlank(name)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.MARKET_NAME_MUST_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        return name.trim();
    }
}
