package com.bbb.core.repository.bid;

import com.bbb.core.model.database.InventoryAuction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryAuctionRepository extends CrudRepository<InventoryAuction, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE id = ?1")
    InventoryAuction findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction WHERE " +
            "inventory_auction.inventory_id = :inventoryId AND " +
            "inventory_auction.auction_id = :auctionId")
    InventoryAuction findOne(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "auctionId") long auctionId
    );

//    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction " +
//            "WHERE " +
//            "inventory_auction.inventory_id = :inventoryId AND " +
//            "inventory_auction.is_delete = 0 " +
//            "AND inventory_auction.is_inactive = 0")
//    List<InventoryAuction> findAllActive(
//            @Param(value = "inventoryId") long inventoryId
//    );

//    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction " +
//            "WHERE " +
//            "inventory_auction.inventory_id = :inventoryId AND " +
//            "inventory_auction.is_delete = 0 " +
//            "AND inventory_auction.is_inactive = 0")
//    List<InventoryAuction> findAllActive(
//            @Param(value = "inventoryId") long inventoryId
//    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE inventory_auction.id IN :inventoryAuctionIds " +
            "AND inventory_auction.is_inactive = 0 " +
            "AND inventory_auction.is_delete = 0"
    )
    List<InventoryAuction> findListActive(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(inventory_auction.id) FROM inventory_auction " +
            "WHERE " +
            "inventory_auction.inventory_id = :inventoryId AND inventory_auction.is_inactive = 0")
    int getNumberTotal(@Param(value = "inventoryId") long inventoryId);


    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction " +
            "WHERE " +
            "inventory_auction.auction_id IN :auctionIds")
    List<InventoryAuction> getInventoryAuctions(
            @Param(value = "auctionIds") List<Long> auctionIds
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE " +
            "inventory_auction.auction_id IN :auctionIds " +
            "AND inventory_auction.is_inactive = 0")
    List<InventoryAuction> getActiveInventoryAuctions(
            @Param(value = "auctionIds") List<Long> auctionIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction WHERE " +
            "inventory_auction.inventory_id = :inventoryId AND " +
            "inventory_auction.is_delete = 0")
    InventoryAuction findOneActive(@Param(value = "inventoryId") long inventoryId);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE inventory_auction.auction_id IN :auctionIds " +
            "AND inventory_auction.is_inactive = 0 " +
            "AND inventory_auction.is_delete = 0"
    )
    List<InventoryAuction> findAllInventoryAuctionFromAuctionId(
            @Param(value = "auctionIds") List<Long> auctionIds
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE inventory_auction.inventory_id IN :inventoryIds " +
            "AND inventory_auction.is_inactive = :isInactive " +
            "AND inventory_auction.is_delete = :isDelete")
    List<InventoryAuction> findAllFromInventoryId(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "isInactive") boolean isInactive,
            @Param(value = "isDelete") boolean isDelete
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_auction " +
            "WHERE inventory_auction.id IN :inventoryAuctionIds " +
            "AND inventory_auction.is_inactive = :isInactive " +
            "AND inventory_auction.is_delete = :isDelete")
    List<InventoryAuction> findAllFromIds(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "isInactive") boolean isInactive,
            @Param(value = "isDelete") boolean isDelete
    );

    @Query(nativeQuery = true, value =
            "SELECT DISTINCT inventory_auction.inventory_id " +
                    "FROM inventory_auction JOIN auction " +
                    "ON inventory_auction.auction_id = auction.id " +
                    "WHERE " +
                    "inventory_auction.inventory_id IN :inventoryIds AND " +
                    "inventory_auction.is_inactive = 0 AND " +
                    "inventory_auction.is_delete = 0 AND " +
                    "DATEDIFF_BIG(MILLISECOND , GETDATE(), auction.end_date) > 0"
    )
    List<Integer> findAllInventoryAuctionAcctive(
            @Param(value = "inventoryIds") List<Long> inventoryIds
    );

    @Query(nativeQuery = true, value = "SELECT inventory_auction.id " +
            "FROM inventory_auction " +

            "JOIN auction " +
            "ON inventory_auction.auction_id = auction.id " +
            "AND auction.is_expired = 0 " +

            "WHERE inventory_auction.id IN :inventoryAuctionIds " +
            "AND inventory_auction.is_delete = 0 " +
            "AND inventory_auction.is_inactive = 1 " +
            "AND DATEDIFF_BIG(MILLISECOND , GETDATE(), auction.end_date) > 0"
    )
    List<Long> findInactiveByIdsCanReactive(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );

    @Query(nativeQuery = true, value = "SELECT inventory_auction.inventory_id FROM inventory_auction WHERE inventory_auction.id = :inventoryAuctionId")
    Long findInventoryById(
            @Param(value = "inventoryAuctionId") long inventoryAuctionId
    );

    @Query(nativeQuery = true, value = "SELECT DISTINCT inventory_auction.inventory_id " +
            "FROM " +
            "inventory_auction WHERE inventory_auction.id IN :inventoryAuctionIds")
    List<Integer> findAllInventoryIdByIds(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_auction WHERE inventory_auction.id IN :inventoryAuctionIds AND inventory_auction.is_delete = 0")
    List<InventoryAuction> findAllByIds(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );
}
