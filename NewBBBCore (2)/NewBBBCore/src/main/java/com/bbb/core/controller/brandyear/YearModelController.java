package com.bbb.core.controller.brandyear;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.brandyear.BrandYearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class YearModelController {
    @Autowired
    private BrandYearService service;

    @GetMapping(value = Constants.URL_GET_MODEL_FROM_BRAND_YEAR)
    public ResponseEntity getModelFromBrandYear(
            @RequestParam(value = "brandId") long brandId,
            @RequestParam(value = "yearId", required = false) Long yearId,
            @RequestParam(value = "familyName", required = false) String familyName
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getModelFromBrandYear(brandId, yearId, familyName), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_MODEL_BRAND_FROM_BICYCLE)
    public ResponseEntity getAllBicycleModelFromBicycle(
            @RequestParam(value = "brandId") long brandId) throws ExceptionResponse {
        return new ResponseEntity<>(service.getAllBicycleModelFromBicycle(brandId), HttpStatus.OK);
    }
}
