package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryBicycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryBicycleRepository extends JpaRepository<InventoryBicycle, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_bicycle " +
            "WHERE inventory_id = ?1")
    InventoryBicycle findOne(long inventoryId);
}
