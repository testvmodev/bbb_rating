package com.bbb.core.repository.market;

import com.bbb.core.model.database.EbayCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EbayCategoryRepository extends JpaRepository<EbayCategory, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM ebay_catetory WHERE ebay_catetory.is_primary = 1")
    List<EbayCategory> findAllPrimary();

    @Query(nativeQuery = true, value = "SELECT * FROM ebay_catetory WHERE ebay_catetory.parent_id IN :parentIds")
    List<EbayCategory> findAllChild(
            @Param(value = "parentIds") List<Long> parentIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(ebay_catetory.id) " +
            "FROM ebay_catetory WHERE ebay_catetory.id IN :categoriesIds AND ebay_catetory.is_primary = 1")
    int countCategoryPrimary(
            @Param(value = "categoriesIds") List<Long> categoriesIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(ebay_catetory.id) " +
            "FROM ebay_catetory WHERE ebay_catetory.id IN :categoriesIds AND ebay_catetory.is_primary = 0")
    int countSubCategory(
            @Param(value = "categoriesIds") List<Long> categoriesIds
    );
}
