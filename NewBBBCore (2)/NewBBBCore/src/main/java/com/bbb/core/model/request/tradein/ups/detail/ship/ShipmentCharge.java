package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.bbb.core.model.request.tradein.ups.detail.ship.BillShipper;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentCharge {
    @JsonProperty("Type")
    private String type;
    @JsonProperty("BillShipper")
    private BillShipper billShipper;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BillShipper getBillShipper() {
        return billShipper;
    }

    public void setBillShipper(BillShipper billShipper) {
        this.billShipper = billShipper;
    }
}
