package com.bbb.core.model.response.tradein.customquote;

import com.bbb.core.model.database.TradeIn;
import com.bbb.core.model.database.TradeInCustomQuote;

public class TradeInCustomQuoteCreateResponse {
    private long tradeInId;
    private long customQuoteId;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }
}
