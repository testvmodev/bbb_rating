package com.bbb.core.controller.schedule;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.schedule.ScheduleJobConfigUpdateRequest;
import com.bbb.core.service.schedule.ScheduleJobConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ScheduleJobConfigController {
    @Autowired
    private ScheduleJobConfigService service;

    @GetMapping(Constants.URL_JOB_CONFIGS)
    public ResponseEntity getJobConfigs(
            @RequestParam(value = "isEnable", required = false) Boolean isEnable
    ) {
        return ResponseEntity.ok(service.getJobConfigs(isEnable));
    }

    @GetMapping(Constants.URL_JOB_RUNNING)
    public ResponseEntity getRunningJobs() {
        return ResponseEntity.ok(service.getRunningJobs());
    }

    @GetMapping(Constants.URL_JOB_INQUEUE)
    public ResponseEntity getInqueueJobs() {
        return ResponseEntity.ok(service.getInqueueJobs());
    }

    @GetMapping(Constants.URL_JOB_CONFIG)
    public ResponseEntity getJobDetail(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getJobDetail(id));
    }

    @PutMapping(Constants.URL_JOB_CONFIG)
    public ResponseEntity updateJobConfig(
            @PathVariable(value = Constants.ID) long id,
            @RequestBody ScheduleJobConfigUpdateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateJobConfig(id, request));
    }
}
