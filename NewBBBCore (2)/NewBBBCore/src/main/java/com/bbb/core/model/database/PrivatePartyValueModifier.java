package com.bbb.core.model.database;

import com.bbb.core.model.database.type.ConditionInventory;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Condition")
public class PrivatePartyValueModifier {
    @Id
    private long id;
    private ConditionInventory name;
    private float valueModifier;
    private float adjustmentRange;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ConditionInventory getName() {
        return name;
    }

    public void setName(ConditionInventory name) {
        this.name = name;
    }

    public float getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(float valueModifier) {
        this.valueModifier = valueModifier;
    }

    public float getAdjustmentRange() {
        return adjustmentRange;
    }

    public void setAdjustmentRange(float adjustmentRange) {
        this.adjustmentRange = adjustmentRange;
    }
}
