package com.bbb.core.service.impl.onlinestore;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.onlinestore.OnlineStoreManager;
import com.bbb.core.model.request.onlinestore.*;
import com.bbb.core.service.onlinestore.OnlineStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnlineStoreServiceImpl implements OnlineStoreService {
    @Autowired
    private OnlineStoreManager manager;

    @Override
    public Object getTrendingOnlineStores(TrendingOnlineStoreRequest request) throws ExceptionResponse {
        return manager.getTrendingOnlineStores(request);
    }

    @Override
    public Object getOnlineStores(OnlineStoreRequest request) throws ExceptionResponse {
        return manager.getOnlineStores(request);
    }

    @Override
    public Object getMyListings(OnlineStoreListingRequest request) throws ExceptionResponse {
        return manager.getMyListings(request);
    }

    @Override
    public Object getMyListingSummary() throws ExceptionResponse {
        return manager.getMyListingSummary();
    }

    @Override
    public Object getStoreSummaryStats() throws ExceptionResponse {
        return manager.getStoreSummaryStats();
    }

    @Override
    public Object getStoreSaleReport(OnlineStoreSaleReportRequest request) throws ExceptionResponse {
        return manager.getStoreSaleReport(request);
    }

    @Override
    public Object getStoreSaleReportDetail(OnlineStoreSaleReportDetailRequest request) throws ExceptionResponse {
        return manager.getStoreSaleReportDetails(request);
    }
}
