package com.bbb.core.model.response.market.mylisting.embed;

import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class DraftEmbed {
    private Long draftId;
    @Column(name = "content")
    private String rawContent;
    @Transient
    private PersonalListingRequest content;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    @JsonIgnore
    public String getRawContent() {
        return rawContent;
    }

    public void setRawContent(String rawContent) {
        this.rawContent = rawContent;
    }

    public PersonalListingRequest getContent() {
        return content;
    }

    public void setContent(PersonalListingRequest content) {
        this.content = content;
    }
}
