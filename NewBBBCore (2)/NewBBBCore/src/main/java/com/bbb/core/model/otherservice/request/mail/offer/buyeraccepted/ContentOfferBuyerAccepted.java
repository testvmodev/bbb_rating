package com.bbb.core.model.otherservice.request.mail.offer.buyeraccepted;

import com.bbb.core.model.otherservice.request.mail.offer.content.BuyerMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.ListingMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.OfferMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentOfferBuyerAccepted {
    private UserMailRequest user;
    private ListingMailRequest listing;
    private OfferMailRequest offer;
    private BuyerMailRequest buyer;
    @JsonProperty(value = "base_path")
    private String basePath;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingMailRequest getListing() {
        return listing;
    }

    public void setListing(ListingMailRequest listing) {
        this.listing = listing;
    }

    public OfferMailRequest getOffer() {
        return offer;
    }

    public void setOffer(OfferMailRequest offer) {
        this.offer = offer;
    }

    public BuyerMailRequest getBuyer() {
        return buyer;
    }

    public void setBuyer(BuyerMailRequest buyer) {
        this.buyer = buyer;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
