package com.bbb.core.model.request.log;

public class RawData {
    private Object body;
    private Object query;

    public RawData(Object body, Object query) {
        this.body = body;
        this.query = query;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Object getQuery() {
        return query;
    }

    public void setQuery(Object query) {
        this.query = query;
    }
}
