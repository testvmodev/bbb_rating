package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.bbb.core.model.response.tradein.fedex.detail.type.Severity;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CompletedTrackDetails {
    @JacksonXmlProperty(localName = "TrackDetails")
    private TrackDetails trackDetails;

    public TrackDetails getTrackDetails() {
        return trackDetails;
    }

    public void setTrackDetails(TrackDetails trackDetails) {
        this.trackDetails = trackDetails;
    }

    public boolean hasTrackDetail() {
        try {
            return trackDetails.getNotification().getSeverity() == Severity.SUCCESS
                    && trackDetails.getStatusDetail() != null;
        } catch (Exception e) {
            return false;
        }
    }
}
