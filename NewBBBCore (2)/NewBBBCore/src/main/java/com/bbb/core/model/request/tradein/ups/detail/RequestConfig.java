package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestConfig {
    @JsonProperty("RequestOption")
    private String requestOption;

    public RequestConfig(String requestOption) {
        this.requestOption = requestOption;
    }

    public String getRequestOption() {
        return requestOption;
    }

    public void setRequestOption(String requestOption) {
        this.requestOption = requestOption;
    }
}
