package com.bbb.core.common.aws.s3;

import org.springframework.web.multipart.MultipartFile;

public class ObjectUpload {
    private MultipartFile file;
    private String bucket;
    private String keyHash;

    public ObjectUpload(MultipartFile multipartFile, String keyHash, String bucket) {
        this.file = multipartFile;
        this.bucket = bucket;
        this.keyHash = keyHash;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getKeyHash() {
        return keyHash;
    }

    public void setKeyHash(String keyHash) {
        this.keyHash = keyHash;
    }
}
