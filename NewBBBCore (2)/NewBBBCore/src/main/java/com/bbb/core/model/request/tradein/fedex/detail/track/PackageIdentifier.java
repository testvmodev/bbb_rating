package com.bbb.core.model.request.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PackageIdentifier {
    @JacksonXmlProperty(localName = "Type")
    private String type;
    @JacksonXmlProperty(localName = "Value")
    private String value;

    public PackageIdentifier() {}

    public PackageIdentifier(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
