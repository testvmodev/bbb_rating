package com.bbb.core.model.request.tradein.customquote;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.response.ObjectError;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TradeInCustomQuoteCreateRequest implements MessageResponses {
    private Long brandId;
    private String brandName;
    private Long modelId;
    private String modelName;
    private Long yearId;
    private String yearName;
    @NotNull
    private Long typeId;
    private Float eBikeMileage;
    private Float eBikeHours;
    @NotNull
    private Boolean isClean;
    @NotNull
    private ConditionInventory condition;
    private String note;
    @NotNull
    private String employeeName;
    @NotNull
    private String employeeEmail;
    @NotNull
    private String employeeLocation;
    @NotNull
    private String ownerName;
    private List<Long> upgradeComponentsId;
    private List<CompRequest> compCustomQuotes;
    private Float tradeInValue;
    private Boolean isDraft;
//    @NotNull
//    private Long sizeId;


    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeLocation() {
        return employeeLocation;
    }

    public void setEmployeeLocation(String employeeLocation) {
        this.employeeLocation = employeeLocation;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public List<Long> getUpgradeComponentsId() {
        return upgradeComponentsId;
    }

    public List<CompRequest> getCompCustomQuotes() {
        return compCustomQuotes;
    }

    public void setCompCustomQuotes(List<CompRequest> compCustomQuotes) {
        this.compCustomQuotes = compCustomQuotes;
    }

    public void setUpgradeComponentsId(List<Long> upgradeComponentsId) {
        this.upgradeComponentsId = upgradeComponentsId;
    }

    public boolean checkHasValidId() {
        return brandId != null && modelId != null && yearId != null;
    }

    public boolean checkHasName() {
        return !StringUtils.isEmpty(brandName) && !StringUtils.isEmpty(modelName) && !StringUtils.isEmpty(yearName);
    }

    public Float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(Float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

//    public Long getSizeId() {
//        return sizeId;
//    }
//
//    public void setSizeId(Long sizeId) {
//        this.sizeId = sizeId;
//    }


    public Boolean getDraft() {
        return isDraft;
    }

    public void setDraft(Boolean draft) {
        isDraft = draft;
    }

    @SuppressWarnings("Duplicates")
    public void validNormal() throws ExceptionResponse {
        if ((brandId != null && brandName != null) ||
                (brandId == null && brandName == null && (isDraft == null || !isDraft))
        ) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BRAND_ONLY_IS_ID_OR_NAME),
                    HttpStatus.BAD_REQUEST);
        }
        if ((yearId != null && yearName != null) ||
                (yearId == null && yearName == null && (isDraft == null || !isDraft))
        ) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_ONLY_IS_ID_OR_NAME),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
