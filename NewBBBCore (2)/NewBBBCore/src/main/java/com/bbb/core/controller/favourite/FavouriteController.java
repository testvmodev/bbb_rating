package com.bbb.core.controller.favourite;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.FavouriteType;
import com.bbb.core.model.request.favourite.FavouriteAddRequest;
import com.bbb.core.model.request.favourite.FavouriteRemoveRequest;
import com.bbb.core.service.favourite.FavouriteService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class FavouriteController {
    @Autowired
    private FavouriteService service;

    @GetMapping(Constants.URL_FAVOURITES)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getFavourites(Pageable pageable) {
        return ResponseEntity.ok(service.getFavourites(pageable));
    }

    @PostMapping(Constants.URL_FAVOURITE_CREATE)
    public ResponseEntity addFavourite(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @RequestParam("favouriteType") FavouriteType favouriteType
            ) throws ExceptionResponse {
        FavouriteAddRequest request = new FavouriteAddRequest();
        request.setMarketListingId(marketListingId);
        request.setInventoryAuctionId(inventoryAuctionId);
        request.setFavouriteType(favouriteType);

        return ResponseEntity.ok(service.addFavourite(request));
    }

    @DeleteMapping(Constants.URL_FAVOURITE_REMOVE)
    public ResponseEntity removeFavourite(
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @RequestParam("favouriteType") FavouriteType favouriteType
    ) throws ExceptionResponse {
        FavouriteRemoveRequest request = new FavouriteRemoveRequest();
        request.setMarketListingId(marketListingId);
        request.setInventoryAuctionId(inventoryAuctionId);
        request.setFavouriteType(favouriteType);

        return ResponseEntity.ok(service.removeFavourite(request));
    }
}
