package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.OutboundShippingType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OutboundShippingTypeConverter implements AttributeConverter<OutboundShippingType, String> {
    @Override
    public String convertToDatabaseColumn(OutboundShippingType outBoundShippingType) {
        if (outBoundShippingType == null) return null;
        return outBoundShippingType.getValue();
    }

    @Override
    public OutboundShippingType convertToEntityAttribute(String value) {
        if (value == null) return null;

        return OutboundShippingType.findByValue(value);
    }
}
