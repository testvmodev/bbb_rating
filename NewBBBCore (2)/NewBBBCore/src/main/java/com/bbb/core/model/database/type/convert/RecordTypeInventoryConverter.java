package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.RecordTypeInventory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RecordTypeInventoryConverter implements AttributeConverter<RecordTypeInventory, String> {
    @Override
    public String convertToDatabaseColumn(RecordTypeInventory recordTypeInventory) {
        if (recordTypeInventory == null) {
            return null;
        }
        return recordTypeInventory.getValue();
    }

    @Override
    public RecordTypeInventory convertToEntityAttribute(String s) {
        return RecordTypeInventory.findByValue(s);
    }
}
