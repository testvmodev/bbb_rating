package com.bbb.core.manager.inventory;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.fedex.FedExManager;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.ups.UPSManager;
import com.bbb.core.common.utils.GoogleServiceUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bicycle.BicycleComponentManager;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.inventory.async.InventoryManagerAsync;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.salesforce.SalesforceAsync;
import com.bbb.core.manager.shipping.ShipmentManager;
import com.bbb.core.manager.valueguide.ValueGuidePriceManager;
import com.bbb.core.model.InitialInventory;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.embedded.InventoryCompDetailId;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.common.GoogleServiceResponse;
import com.bbb.core.model.otherservice.response.common.google.AddressComponent;
import com.bbb.core.model.otherservice.response.common.google.GeometryLocation;
import com.bbb.core.model.otherservice.response.order.OrderDetailResponse;
import com.bbb.core.model.otherservice.response.order.OrderLineItem;
import com.bbb.core.model.otherservice.response.order.OrderShippingAddress;
import com.bbb.core.model.otherservice.response.shipping.ShipmentDetailResponse;
import com.bbb.core.model.request.inventory.create.*;
import com.bbb.core.model.request.inventory.secret.InventoryUpdateSecretRequest;
import com.bbb.core.model.request.inventory.update.*;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingSaleRequest;
import com.bbb.core.model.response.bicycle.BicycleComponentResponse;
import com.bbb.core.model.request.inventory.InventoryRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.inventory.InventoryCompDetailRes;
import com.bbb.core.model.response.inventory.InventoryDetailResponse;
import com.bbb.core.model.response.inventory.InventoryResponse;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.model.response.tradein.fedex.FedexTrackResponse;
import com.bbb.core.model.response.tradein.ups.UPSTrackResponse;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicycle.BicycleSpecRepository;
import com.bbb.core.repository.bicycle.out.BicycleComponentResponseRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.component.BicycleComponentTypeDetailRepository;
import com.bbb.core.repository.inventory.*;
import com.bbb.core.repository.inventory.out.InitialInventoryRepository;
import com.bbb.core.repository.inventory.out.InventoryCompDetailResRepository;
import com.bbb.core.repository.inventory.out.InventoryDetailResRepository;
import com.bbb.core.repository.market.InventoryLocationShippingRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.InventorySaleRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.reout.BicycleSizeRepository;
import com.bbb.core.repository.stocklocation.StockLocationRepository;
import com.bbb.core.repository.tradein.TradeInCompDetailRepository;
import com.bbb.core.repository.tradein.TradeInCustomQuoteCompDetailRepository;
import com.bbb.core.repository.tradein.TradeInUpgradeCompRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InventoryManager implements MessageResponses, DescriptionValue {
    //region Beans
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryResponseRepository inventoryResponseRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private InventoryImageRepository imageRepository;
    @Autowired
    private InventoryCompDetailRepository copDetailRepository;
    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private BicycleSizeRepository bicycleSizeRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private InitialInventoryRepository initialInventoryRepository;
    @Autowired
    private InventoryUpgradeCompRepository inventoryUpgradeCompRepository;
    @Autowired
    private TradeInUpgradeCompRepository tradeInUpgradeCompRepository;
    @Autowired
    private BicycleComponentResponseRepository bicycleComponentResponseRepository;
    @Autowired
    private BicycleSpecRepository bicycleSpecRepository;
    @Autowired
    private ConditionDescriptionRepository conditionDescriptionRepository;
    @Autowired
    private InventoryTradeInOwnerRepository inventoryTradeInOwnerRepository;
    @Autowired
    private InventoryShippingRepository inventoryShippingRepository;
    @Autowired
    private StockLocationRepository stockLocationRepository;
    @Autowired
    private UPSManager upsManager;
    @Autowired
    private FedExManager fedexManager;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InventoryCompDetailRepository inventoryCompDetailRepository;
    @Autowired
    private S3Manager s3Manager;
    @Autowired
    private TradeInCustomQuoteCompDetailRepository tradeInCustomQuoteCompDetailRepository;
    @Autowired
    private InventoryManagerAsync inventoryManagerAsync;
    @Autowired
    private BicycleComponentTypeDetailRepository bicycleComponentTypeDetailRepository;
    @Autowired
    private InventorySpecRepository inventorySpecRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private InventoryCompDetailResRepository inventoryCompDetailResRepository;
    @Autowired
    private ValueGuidePriceManager valueGuidePriceManager;
    @Autowired
    private InventoryDetailResRepository inventoryDetailResRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventorySaleRepository inventorySaleRepository;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private MyListingManager myListingManager;
    @Autowired
    private TradeInCompDetailRepository tradeInCompDetailRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private ShipmentManager shipmentManager;
    @Autowired
    private InventoryBicycleRepository inventoryBicycleRepository;
    @Autowired
    @Qualifier("mvcValidator")
    private Validator validator;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private AwsResourceConfig awsResourceConfig;
    @Autowired
    private BicycleComponentManager bicycleComponentManager;
    //endregion
    @Autowired
    private SalesforceAsync salesforceAsync;


    public Object getInventories(InventoryRequest request, Pageable page) throws ExceptionResponse {
        if (request.getStartPrice() != null && request.getEndPrice() != null) {
            if (request.getStartPrice() > request.getEndPrice()) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, START_PRICE_MUST_LESS_OR_EQUAL_THAN_END_PRICE),
                        HttpStatus.BAD_REQUEST);
            }
        }
        if (request.getStartYearId() != null && request.getEndYearId() != null) {
            if (request.getStartYearId() > request.getEndYearId()) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, START_YEAR_MUST_LESS_OR_EQUAL_THAN_END_YEAR),
                        HttpStatus.BAD_REQUEST);
            }
        }
        ListObjResponse<InventoryResponse> response = new ListObjResponse<>();
        List<Long> temList = new ArrayList<>();
        temList.add(0L);
        List<String> temListString = new ArrayList<>();
        temListString.add("Demo");
        boolean isNullBicycle = request.getBicycleIds() == null || request.getBicycleIds().size() == 0;
        boolean isNullModel = request.getModelIds() == null || request.getModelIds().size() == 0;
        boolean isNullBrand = request.getBrandIds() == null || request.getBrandIds().size() == 0;
        boolean isNullType = request.getTypeIds() == null || request.getTypeIds().size() == 0;
        boolean isNullFrame = request.getFrameMaterialNames() == null || request.getFrameMaterialNames().size() == 0;
        boolean isNullBrake = request.getBrakeTypeNames() == null || request.getBrakeTypeNames().size() == 0;
        boolean isNullSize = request.getSizeNames() == null || request.getSizeNames().size() == 0;
        boolean isNullCondition = request.getConditions() == null || request.getConditions().size() == 0;
        long marketPlaceId = 0;
        if (request.getMarketType() == null) {
            response.setTotalItem(
                    inventoryResponseRepository.countAll(
                            isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                            isNullModel, isNullModel ? temList : request.getModelIds(),
                            isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                            isNullType, isNullType ? temList : request.getTypeIds(),
                            ValueCommons.INVENTORY_FRAME_MATERIAL_ID,
                            isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                            ValueCommons.INVENTORY_BRAKE_TYPE_ID,
                            isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                            ValueCommons.INVENTORY_SIZE_ID,
                            isNullSize, isNullSize ? temListString : request.getSizeNames(),
                            isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),
//
                            request.getStage() == null ? null : request.getStage().getValue(),
                            request.getStatus() == null ? null : request.getStatus().getValue(),
                            request.getName() == null ? null : "%" + request.getName() + "%",
////
                            request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                            request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),
////
                            request.getStartYearId(),
                            request.getEndYearId()

                    )
            );
        } else {
            marketPlaceId = marketPlaceRepository.getMarketPlaceId(request.getMarketType().getValue());
            response.setTotalItem(
                    inventoryResponseRepository.countAllMarketPlace(
                            isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                            isNullModel, isNullModel ? temList : request.getModelIds(),
                            isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                            isNullType, isNullType ? temList : request.getTypeIds(),
                            ValueCommons.INVENTORY_FRAME_MATERIAL_ID,
                            isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                            ValueCommons.INVENTORY_BRAKE_TYPE_ID,
                            isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                            ValueCommons.INVENTORY_SIZE_ID,
                            isNullSize, isNullSize ? temListString : request.getSizeNames(),
                            isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),
//
                            request.getStage() == null ? null : request.getStage().getValue(),
                            request.getStatus() == null ? null : request.getStatus().getValue(),
                            marketPlaceId,
                            request.getName() == null ? null : "%" + request.getName() + "%",
////
                            request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                            request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),
////
                            request.getStartYearId(),
                            request.getEndYearId()

                    )
            );

        }


        if (response.getTotalItem() > 0) {
            if (request.getMarketType() == null) {
                response.setData(inventoryResponseRepository.findAllSorted(
                        isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                        isNullModel, isNullModel ? temList : request.getModelIds(),
                        isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                        isNullType, isNullType ? temList : request.getTypeIds(),
                        ValueCommons.INVENTORY_FRAME_MATERIAL_ID,
                        isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                        ValueCommons.INVENTORY_BRAKE_TYPE_ID,
                        isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                        ValueCommons.INVENTORY_SIZE_ID,
                        isNullSize, isNullSize ? temListString : request.getSizeNames(),
                        isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),

                        request.getStage() == null ? null : request.getStage().getValue(),
                        request.getStatus() == null ? null : request.getStatus().getValue(),
                        request.getName() == null ? null : "%" + request.getName() + "%",

                        request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                        request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),

                        request.getStartYearId(),
                        request.getEndYearId(),

                        request.getSortField().name(), request.getSortType().name(),
                        page.getOffset(), page.getPageSize()
                ));
            } else {
                response.setData(inventoryResponseRepository.findAllMarketPlaceSorted(
                        isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                        isNullModel, isNullModel ? temList : request.getModelIds(),
                        isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                        isNullType, isNullType ? temList : request.getTypeIds(),
                        ValueCommons.INVENTORY_FRAME_MATERIAL_ID,
                        isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                        ValueCommons.INVENTORY_BRAKE_TYPE_ID,
                        isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                        ValueCommons.INVENTORY_SIZE_ID,
                        isNullSize, isNullSize ? temListString : request.getSizeNames(),
                        isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),

                        request.getStage() == null ? null : request.getStage().getValue(),
                        request.getStatus() == null ? null : request.getStatus().getValue(),
//                        marketPlaceId,
                        request.getName() == null ? null : "%" + request.getName() + "%",

                        request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                        request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),

                        request.getStartYearId(),
                        request.getEndYearId(),

                        request.getSortField().name(), request.getSortType().name(),
                        page.getOffset(), page.getPageSize()
                ));
            }

        }
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }


    public Object getInventoryDetail(long id) throws ExceptionResponse {

        InventoryDetailResponse response = inventoryDetailResRepository.findOnById(id);
        if (response == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_AUCTION_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoryId(id);
        if (inventoryImages.size() > 0) {
            response.setImages(
                    inventoryImages.stream().map(InventoryImage::getImage).collect(Collectors.toList())
            );
        }
        return response;
    }


    public Object importInventoryImage() {
        try {
            Workbook workbook = WorkbookFactory.create(new File("/Users/ducnd/Datas/Vmodev/bbb/NewBBBCore/src/main/resources/Inventory_Images_c.xls"));
            Sheet sheet = workbook.getSheetAt(0);
            Row rowTitle = sheet.getRow(0);
            int indexInventorySaleforce = 0;
            int indexPath = 0;
            int index = 0;
            Cell cellTitle = rowTitle.getCell(index);
            while (cellTitle != null) {
                if (cellTitle.getStringCellValue().equals("Inventory__c")) {
                    indexInventorySaleforce = index;
                }
                if (cellTitle.getStringCellValue().equals("Image_Path__c")) {
                    indexPath = index;
                }
                index++;
                cellTitle = rowTitle.getCell(index);
            }
            index = 1;
            List<InventoryImage> inventoryImages = new ArrayList<>();
            while (true) {
                Row row = sheet.getRow(index);
                if (row == null) {
                    break;
                }
                index++;
                Cell cellIdInventoryId = row.getCell(indexInventorySaleforce);
                if (cellIdInventoryId == null || cellIdInventoryId.getCellTypeEnum() != CellType.STRING) {
                    continue;
                }
                String idInventory = cellIdInventoryId.getStringCellValue();
                Cell cellImage = row.getCell(indexPath);
                if (cellImage == null || cellImage.getCellTypeEnum() != CellType.STRING) {
                    continue;
                }
                String image = cellImage.getStringCellValue();
                InventoryImage inventoryImage = new InventoryImage();
//                inventoryImage.setIdInventorySaleForce(idInventory);
                inventoryImage.setImage(image);
                inventoryImages.add(inventoryImage);
            }
            List<Inventory> inventories = inventoryRepository.findAll();
            List<InventoryImage> inventoryImagesDb = new ArrayList<>();
//            for (InventoryImage inventoryImage : inventoryImages) {
//                for (Inventory inventory : inventories) {
//                    if (inventoryImage.getIdInventorySaleForce().equals(inventory.getIdSaleforce())) {
//                        inventoryImage.setInventoryId(inventory.getId());
//                        inventoryImagesDb.add(inventoryImage);
//                        break;
//                    }

//                }
//            }
            imageRepository.saveAll(inventoryImagesDb);

            return "Success";
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        return "exception";
    }

    public Object importIdSaleForceId() {
        List<Inventory> inventories = inventoryRepository.findAll();
        List<InventoryCompDetail> inventoryCompDetails = copDetailRepository.findAllFromCopType(9);
        for (Inventory inventory : inventories) {
            for (InventoryCompDetail inventoryCompDetail : inventoryCompDetails) {
                if (inventory.getId() == inventoryCompDetail.getId().getInventoryId()) {
                    inventory.setCondition(ConditionInventory.findByValue(inventoryCompDetail.getValue()));
                    break;
                }
            }
        }
        inventoryRepository.saveAll(inventories);


        return "exception";
    }

    public Object createInventory(InventoryCreateFullRequest request) throws ExceptionResponse {
        Bicycle bicycle = bicycleRepository.findOneNotDelete(request.getBaseInfo().getBicycleId());
        if (bicycle == null || !bicycle.isActive() || bicycle.isDelete()) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        InventoryBaseCreateRequest baseInfo = request.getBaseInfo();
        verifyInventoryBaseInfo(baseInfo);

        //shipping now moved to shipment service
//        InventoryShippingCreatedRequest shipping = request.getShipping();
//        verifyInventoryShipping(shipping);
        verifyInventoryComps(request.getComps());
        //must verify base info before this
        GoogleServiceResponse googleServiceResponse = verifyInventoryLocation(request.getLocation(), baseInfo);

        //create inventory
        Inventory inventory = createInventoryBaseInfo(bicycle, baseInfo);
//        if (shipping != null) {
//            inventoryManagerAsync.createInventoryShipping(inventory.getId(), shipping);
//        }

        inventoryManagerAsync.createInventoryLocation(inventory.getId(), request.getLocation(), googleServiceResponse);

        if (request.getUpgradeComps() != null && request.getUpgradeComps().size() > 0) {
            inventoryManagerAsync.createUpgradeComponent(inventory.getId(), request.getUpgradeComps());
        }
        if (request.getComps() != null && request.getComps().size() > 0) {
            inventoryManagerAsync.createCompsAsync(inventory.getId(), request.getComps(), (invId) -> {
                String recommendation = createAndUpdateSearchRecommendation(invId);
                inventory.setSearchGuideRecommendation(recommendation);
            });
        }
        return inventory;
    }

    public String createAndUpdateSearchRecommendation(long inventoryId) {
        Inventory inventory = inventoryRepository.findOneNotDelete(inventoryId);
        if (inventory == null) {
            return "";
        }
        List<InventoryCompDetailRes> inventoryCompDetails = inventoryCompDetailResRepository.findAllCompBrakeTypeAndFrameMaterial(
                inventory.getId()
        );
        String yearName = inventory.getBicycleYearName() == null ? "" : inventory.getBicycleYearName();
        String condition = inventory.getCondition() == null ? "" : inventory.getCondition().getValue();
        String typeName = inventory.getBicycleTypeName() == null ? "" : inventory.getBicycleTypeName();
        String sizeName = inventory.getBicycleSizeName() == null ? "" : inventory.getBicycleSizeName();
        String frameMaterialName = "";
        InventoryCompDetailRes compFrameMaterial = CommonUtils.findObject(inventoryCompDetails, comp -> comp.getName().equals(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME));
        if (compFrameMaterial != null) {
            frameMaterialName = compFrameMaterial.getValue();
        }
        String brakeTypeName = "";
        InventoryCompDetailRes compBrakeType = CommonUtils.findObject(inventoryCompDetails, comp -> comp.getName().equals(ValueCommons.INVENTORY_BRAKE_TYPE_NAME));
        if (compBrakeType != null) {
            brakeTypeName = compBrakeType.getValue();
        }
        String brandName = inventory.getBicycleBrandName() == null ? "" : inventory.getBicycleBrandName();
        String recommendation = yearName;
        if (!condition.equals("")) {
            recommendation = recommendation + " " + condition;
        }
        if (!typeName.equals("")) {
            recommendation = recommendation + " " + typeName;
        }
        if (!sizeName.equals("")) {
            recommendation = recommendation + " " + sizeName;
        }
        if (!frameMaterialName.equals("")) {
            recommendation = recommendation + " " + frameMaterialName;
        }
        if (!brakeTypeName.equals("")) {
            recommendation = recommendation + " " + brakeTypeName;
        }
        if (!brandName.equals("")) {
            recommendation = recommendation + " " + brandName;
        }
        inventory.setSearchGuideRecommendation(recommendation);

        inventoryRepository.updateSearchGuideRecommendation(inventoryId, recommendation);
        return recommendation;
    }


    private Inventory createInventoryBaseInfo(Bicycle bicycle, InventoryBaseCreateRequest baseInfo) throws ExceptionResponse {
        Inventory inventory = new Inventory();
        inventory.setBicycleId(baseInfo.getBicycleId());
        InitialInventory initialInventory = initialInventoryRepository.initInventory(
                bicycle.getModelId(), bicycle.getBrandId(), bicycle.getYearId(), bicycle.getTypeId(), bicycle.getSizeId());
        if (initialInventory == null) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, CAN_NOT_CREATE_INVENTORY),
                    HttpStatus.BAD_REQUEST);
        }
        if (baseInfo.getBbbValue() < 0) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, TRADE_IN_VALUE_MUST_POSITIVE),
                    HttpStatus.BAD_REQUEST);
        }
        inventory.setBicycleModelName(initialInventory.getBicycleModelName());
        inventory.setBicycleBrandName(initialInventory.getBicycleBrandName());
        inventory.setBicycleYearName(initialInventory.getBicycleYearName());
        inventory.setBicycleTypeName(initialInventory.getBicycleTypeName());
        inventory.setBicycleYearName(initialInventory.getBicycleYearName());
        inventory.setBicycleSizeName(initialInventory.getBicycleSizeName());
        inventory.setDescription(generateDescription(bicycle, baseInfo.getSerialNumber(), baseInfo.getCondition()));
        if (baseInfo.getImages() != null && baseInfo.getImages().size() > 0) {
            inventory.setImageDefault(baseInfo.getImages().get(0));
        }
        inventory.setStatus(StatusInventory.IN_ACTIVE);

        InventoryType inventoryType = inventoryTypeRepository.findOne(baseInfo.getTypeId());

        inventory.setMsrpPrice(baseInfo.getMsrPrice());
        inventory.setBbbValue(baseInfo.getBbbValue());
        inventory.setOverrideTradeInPrice(baseInfo.getOverrideTradeInPrice());
        inventory.setCogsPrice(baseInfo.getCogsPrice());
        if (baseInfo.getBbbValue() == 0) {
            inventory.setBbbValue(baseInfo.getCogsPrice());
        }
        inventory.setInitialListPrice(baseInfo.getBbbValue() / 0.6f);
        if (inventoryType.getName().equals(Constants.TRADE_IN_TYPE)) {
            if (inventory.getInitialListPrice() < Constants.MIN_INIT_PRICE_TRADE_IN) {
                inventory.setInitialListPrice(Constants.MIN_INIT_PRICE_TRADE_IN);
            }
        }
        inventory.setDiscountedPrice(inventory.getInitialListPrice() / 0.6f);
        inventory.setFlatPriceChange(baseInfo.getFlatPriceChange());
        inventory.setCurrentListedPrice(inventory.getDiscountedPrice() + inventory.getFlatPriceChange());


//        inventory.setPartnerId(CommonUtils.getUserLogin());
//        inventory.setPartnerEmail(CommonUtils.getUserEmail());
        inventory.setStage(StageInventory.INCOMPLETE_BICYCLE_DETAILS);
        inventory.setSerialNumber(baseInfo.getSerialNumber());
        //name


        inventory.setTypeId(baseInfo.getTypeId());
//        if (baseInfo.getBestOffer()) {
//            inventory.setBestOfferAutoAcceptPrice(inventory.getCurrentListedPrice() * 0.95f);
//            inventory.setBestOfferAutoAcceptPrice(inventory.getCurrentListedPrice() * 0.6f);
//        }
        inventory.setTitle(inventory.getBicycleYearName() + " " + inventory.getBicycleBrandName() + " " + inventory.getBicycleModelName());
        inventory.setCustomQuote(baseInfo.isCustomQuote());
        inventory.setCreatePo(baseInfo.isCustomQuote());
        //todo fake seller id
        if (baseInfo.getSellerId() != null) {
            inventory.setSellerId(ValueCommons.SELLER_ID_DEFAULT);
        } else {
            inventory.setSellerId(baseInfo.getSellerId());
        }

        inventory.setCondition(baseInfo.getCondition());

        inventory.setPreList(baseInfo.getPreList());
        inventory.setCheckInOverride(baseInfo.getCheckInOverride());
        inventory.setDayHold(baseInfo.getDayHold());
        inventory.setOverrideCreatePo(baseInfo.getOverrideCreatePo());
        inventory.setNonComplianceNoti(baseInfo.getNonComplianceNoti());
        inventory.setRefundReturnType(baseInfo.getRefundReturnType());
        inventory.setPriceRole(baseInfo.getPriceRole());
        inventory.setMasterMultiListing(baseInfo.getMasterMultiListing());
        inventory.setReviseItem(baseInfo.getReviseItem());
        inventory.setRecordType(baseInfo.getRecordType());
        inventory.setNote(baseInfo.getNote());
        inventory.setSite(baseInfo.getSite());
        inventory.setStockLocationId(baseInfo.getStockLocationId());
        inventory.setDateReceived(baseInfo.getDateReceived());
        inventory.setConditionScore(baseInfo.getConditionScore());
        inventory.setPrivatePartyValue(baseInfo.getPrivatePartyValue());
        inventory.setValueAdditionalComponent(baseInfo.getValueAdditionalComponent());

        inventory.setSearchGuideRecommendation(
                inventory.getBicycleYearName() + " " + inventory.getCondition() + " " + inventory.getBicycleBrandName()
        );

        setValueGuidePrice(inventory, bicycle);

        inventory = inventoryRepository.save(inventory);

        InventoryBicycle inventoryBicycle = new InventoryBicycle();
        inventoryBicycle.setInventoryId(inventory.getId());
        inventoryBicycle.setBrandId(bicycle.getBrandId());
        inventoryBicycle.setModelId(bicycle.getModelId());
        inventoryBicycle.setYearId(bicycle.getYearId());
        inventoryBicycle.setTypeId(bicycle.getTypeId());
        inventoryBicycle.setSizeId(bicycle.getSizeId());

        inventoryBicycleRepository.save(inventoryBicycle);

        return inventory;
    }

    private void verifyInventoryComps(List<InventoryCompDetailCreateRequest> comps) throws ExceptionResponse {
        if (comps == null || comps.size() == 0) {
            return;
        }
        List<Long> compsId = comps.stream().map(InventoryCompDetailCreateRequest::getInventoryCompTypeId).collect(Collectors.toList());

        int indexOle = compsId.size();
        CommonUtils.stream(comps);
        if (indexOle != comps.size()) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        if (comps.size() > 0) {
            int totalCount = inventoryCompTypeRepository.getTotalCount(comps.stream().map(InventoryCompDetailCreateRequest::getInventoryCompTypeId).collect(Collectors.toList()));
            if (totalCount != indexOle) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, COMPONENT_INVALIDATE),
                        HttpStatus.BAD_REQUEST);
            }
        }


    }

    private void verifyInventoryUpgradeComps(List<InventoryUpgradeCompCreateRequest> upgradeComps) throws
            ExceptionResponse {
        if (upgradeComps == null || upgradeComps.size() == 0) {
            return;
        }
        for (InventoryUpgradeCompCreateRequest upgradeComp : upgradeComps) {
            if (upgradeComp.getPercentValue() < 0) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, PERCENT_VALUE_MUST_BE_POSITIVE),
                        HttpStatus.BAD_REQUEST);
            }
        }

    }

    private void verifyInventoryShipping(InventoryShippingCreatedRequest shipping) throws ExceptionResponse {
        if (shipping != null) {
            if (shipping.getCarrier() == null) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, CARRIER_MUST_BE_NOT_NULL),
                        HttpStatus.NOT_FOUND);
            }

            if (shipping.getTrackingInbound() != null) {
                if (shipping.getCarrier().toUpperCase().equals(CarrierType.UPS.name())) {
                    UPSTrackResponse response = upsManager.getTrack(shipping.getTrackingInbound());
                    if (!response.isSuccess()) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, response.getFaultMessage()),
                                HttpStatus.BAD_REQUEST
                        );
                    }
                } else {
                    if (shipping.getCarrier().toUpperCase().equals(CarrierType.FEDEX.name())) {
                        FedexTrackResponse response = fedexManager.getTrack(shipping.getTrackingInbound());
                        if (!response.isSuccess() || !response.getCompletedTrackDetails().hasTrackDetail()) {
                            throw new ExceptionResponse(
                                    new ObjectError(ObjectError.ERROR_PARAM, response.getCompletedTrackDetails().getTrackDetails().getNotification().getMessage()),
                                    HttpStatus.BAD_REQUEST
                            );
                        }
                    }
                }
            }
        }
    }

    private void verifyInventoryBaseInfo(InventoryBaseCreateRequest baseInfo) throws ExceptionResponse {
        baseInfo.validate();
        if (baseInfo.getStockLocationId() != null) {
            if (stockLocationRepository.exist(baseInfo.getStockLocationId()) == 0) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_NOT_FOUND, STOCKE_LOCATION_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
        }
        boolean notValidInventoryType = baseInfo.getTypeId() == null;
        if (!notValidInventoryType) {
            if (!inventoryTypeRepository.existsById(baseInfo.getTypeId())) {
                notValidInventoryType = true;
            }
        }
        if (notValidInventoryType) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_NOT_FOUND, INVENTORY_TYPE_NOT_FOUND),
                    HttpStatus.NOT_FOUND);
        }
    }

    private GoogleServiceResponse verifyInventoryLocation(
            InventoryLocationShippingCreateRequest location,
            InventoryBaseCreateRequest baseInfo
    ) throws ExceptionResponse {
        InventoryType inventoryType = null;
        if (baseInfo.getTypeId() != null) {
            inventoryType = inventoryTypeRepository.findOne(baseInfo.getTypeId());
        }

        if (location.getAllowLocalPickup() != null
                || location.getFlatRate() != null
                || location.getShippingType() != null
        ) {
            if (baseInfo.getTypeId() != null) {
                if (!inventoryType.getName().equals(ValueCommons.PTP)) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            MessageResponses.INVALID_LOCATION_INFO_NON_PTP,
                            HttpStatus.BAD_REQUEST
                    );
                }
            }
        }
        if (!inventoryType.getName().equals(ValueCommons.PTP)) {
            location.setAllowLocalPickup(true);
            location.setFlatRate(null);
            location.setShippingType(OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE);
        }

        if (location.getShippingType() != null &&
                location.getShippingType() == OutboundShippingType.FLAT_RATE_TYPE
        ) {
            if (location.getFlatRate() == null || location.getFlatRate() < 0) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        FLAT_RATE_MUST_POSITIVE,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        GoogleServiceResponse response = myListingManager.validateShippingLocation(
                location.getAddressLine(),
                location.getCityName(),
                location.getState(),
                location.getZipCode(),
                location.getCountry()
        );
        return response;
    }

    public Object updateInventory(
            long inventoryId, InventoryUpdateFullRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        Inventory inventory = inventoryRepository.findOne(inventoryId);
        if (inventory == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (inventory.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, INVENTORY_DELETED),
                    HttpStatus.BAD_REQUEST
            );
        }

        InventoryLocationShipping locationShipping = inventoryLocationShippingRepository.findOne(inventory.getId());
        GoogleServiceResponse googleServiceResponse = verifyUpdateInventoryLocation(locationShipping, request.getLocation());

        verifyInventoryImage(request.getImages());
        verifyInventoryComps(request.getComps());
        verifyInventoryUpgradeComps(request.getUpgradeComps());
        verifyInventoryShipping(request.getShipping());

        //priority update location first (location is required when validate some fields of base info)
        locationShipping = updateLocation(inventory, locationShipping, request.getLocation(), googleServiceResponse);

        verifyInventoryUpdateBaseInfo(request.getBaseInfo(), locationShipping);

        ConditionInventory oldCondition = inventory.getCondition();
        long oldBicycleId = inventory.getBicycleId();

        inventory = updateInventoryBaseInfo(inventory, request.getBaseInfo());

        if (request.getImages() != null && request.getImages().size() > 0) {
            InventoryImage inventoryImageDefault = updateInventoryImage(inventoryId, request.getImages());
            inventory.setImageDefault(inventoryImageDefault.getImage());
            inventory.setImageDefaultId(inventoryImageDefault.getId());
        }

        inventoryManagerAsync.updateInventoryCompsDetail(inventory, request.getComps());
        inventoryManagerAsync.updateInventoryUpgradeCompsAsync(inventoryId, request.getUpgradeComps());
        inventoryManagerAsync.updateInventoryShipping(inventoryId, request.getShipping());
        inventoryManagerAsync.updateInventoryTradeInOwnerAsync(inventoryId, request.getTradeInOwner());

        if (oldCondition != inventory.getCondition() || oldBicycleId != inventory.getBicycleId()) {
            setValueGuidePrice(inventory);
        }

        String recommentdation = createAndUpdateSearchRecommendation(inventoryId);
        inventory.setSearchGuideRecommendation(recommentdation);
        inventory = inventoryRepository.save(inventory);

        billingManager.inventoryUpdateAsync(inventory.getId());

        return inventory;
    }

    private Inventory updateInventoryBaseInfo(Inventory inventory, InventoryBaseUpdateRequest baseInfo) throws ExceptionResponse {
        if (baseInfo == null) return inventory;

        boolean updateDescription = false;
        InitialInventory initialInventory = null;
        InventoryBicycle inventoryBicycle = null;
        if (baseInfo.getBicycleId() != null && inventory.getBicycleId() != baseInfo.getBicycleId()) {
            Bicycle bicycle = bicycleRepository.findOne(baseInfo.getBicycleId());
            if (bicycle == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_EXIST),
                        HttpStatus.BAD_REQUEST
                );
            }
            if (bicycle.isDelete()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_DELETE, BICYCLE_DELETED),
                        HttpStatus.BAD_REQUEST
                );
            }
            if (!bicycle.isActive()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, BICYCLE_NOT_ACTIVE),
                        HttpStatus.BAD_REQUEST
                );
            }

            updateDescription = true;
            inventory.setBicycleId(baseInfo.getBicycleId());

            initialInventory = initialInventoryRepository.initInventory(
                    bicycle.getModelId(),
                    bicycle.getBrandId(),
                    bicycle.getYearId(),
                    bicycle.getTypeId(),
                    bicycle.getSizeId()
            );
            inventory.setBicycleModelName(initialInventory.getBicycleModelName());
            inventory.setBicycleBrandName(initialInventory.getBicycleBrandName());
            inventory.setBicycleTypeName(initialInventory.getBicycleTypeName());
            inventory.setBicycleYearName(initialInventory.getBicycleYearName());
            inventory.setBicycleSizeName(initialInventory.getBicycleSizeName());

            inventoryBicycle = new InventoryBicycle();
            inventoryBicycle.setInventoryId(inventory.getId());
            inventoryBicycle.setBrandId(bicycle.getBrandId());
            inventoryBicycle.setModelId(bicycle.getModelId());
            inventoryBicycle.setYearId(bicycle.getYearId());
            inventoryBicycle.setTypeId(bicycle.getTypeId());
            inventoryBicycle.setSizeId(bicycle.getSizeId());
        }

        if (baseInfo.getMsrpPrice() != null) {
            inventory.setMsrpPrice(baseInfo.getMsrpPrice());
        }
        if (baseInfo.getBbbValue() != null) {
            inventory.setBbbValue(baseInfo.getBbbValue());
        }
        if (baseInfo.getOverrideTradeInPrice() != null) {
            if (baseInfo.getOverrideTradeInPrice() > inventory.getBbbValue()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, OVERRIDE_BBB_VALUE_MUST_LESS_THAN_OVERRIDE_TRADE_IN),
                        HttpStatus.BAD_REQUEST
                );
            }
            inventory.setOverrideTradeInPrice(baseInfo.getOverrideTradeInPrice());
        }
        if (baseInfo.getInitialListPrice() != null) {
            inventory.setInitialListPrice(baseInfo.getInitialListPrice());
        }
        if (baseInfo.getCurrentListPrice() != null) {
            inventory.setCurrentListedPrice(baseInfo.getCurrentListPrice());
        }
        if (baseInfo.getCogsPrice() != null) {
            inventory.setCogsPrice(baseInfo.getCogsPrice());
        }
        if (baseInfo.getFlatPriceChange() != null) {
            inventory.setFlatPriceChange(baseInfo.getFlatPriceChange());
        }
        if (baseInfo.getDiscountedPrice() != null) {
            inventory.setDiscountedPrice(baseInfo.getDiscountedPrice());
        }
//        if (baseInfo.getBestOfferAutoAcceptPrice() != null) {
//            inventory.setBestOfferAutoAcceptPrice(baseInfo.getBestOfferAutoAcceptPrice());
//        }
//        if (baseInfo.getMinimumOfferAutoAcceptPrice() != null) {
//            inventory.setMinimumOfferAutoAcceptPrice(baseInfo.getMinimumOfferAutoAcceptPrice());
//        }

        if (baseInfo.getCondition() != null) {
            inventory.setCondition(baseInfo.getCondition());
        }
        if (baseInfo.getPartnerId() != null) {
            inventory.setPartnerId(baseInfo.getPartnerId());
        }
        if (baseInfo.getSellerId() != null) {
            inventory.setSellerId(baseInfo.getSellerId());
        }
        if (baseInfo.getSerialNumber() != null) {
            inventory.setSerialNumber(baseInfo.getSerialNumber());
            updateDescription = true;
        }
        if (baseInfo.getInventoryTypeId() != null) {
            inventory.setTypeId(baseInfo.getInventoryTypeId());
        }
        if (baseInfo.getTitle() != null) {
            inventory.setTitle(baseInfo.getTitle());
        }
        if (baseInfo.getCreatePo() != null) {
            inventory.setCreatePo(baseInfo.getCreatePo());
        }
        if (baseInfo.getSizeInventoryId() != null) {
            BicycleSize bicycleSize = bicycleSizeRepository.findOne(baseInfo.getSizeInventoryId());
            if (bicycleSize == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_SIZE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
        if (baseInfo.getStage() != null || baseInfo.getStatus() != null) {
            StatusInventory statusInventory = baseInfo.getStatus();
            StageInventory stageInventory = baseInfo.getStage();
            if (stageInventory == null) stageInventory = inventory.getStage();
            if (statusInventory == null) statusInventory = inventory.getStatus();
            if (!StageInventory.checkValid(stageInventory, statusInventory)) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, STAGE_AND_STATUS_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            } else {
                inventory.setStatus(statusInventory);
                inventory.setStage(stageInventory);
            }
        }
        if (baseInfo.getCustomQuote() != null) {
            inventory.setCustomQuote(baseInfo.getCustomQuote());
        }
        if (baseInfo.getAge() != null) {
            inventory.setAge(baseInfo.getAge());
        }
        if (baseInfo.getPreList() != null) {
            inventory.setPreList(baseInfo.getPreList());
        }
        if (baseInfo.getCheckInOverride() != null) {
            inventory.setCheckInOverride(baseInfo.getCheckInOverride());
        }
        if (baseInfo.getDayHold() != null) {
            inventory.setDayHold(baseInfo.getDayHold());
        }
        if (baseInfo.getOverrideCreatePo() != null) {
            inventory.setOverrideCreatePo(baseInfo.getOverrideCreatePo());
        }
        if (baseInfo.getNonComplianceNoti() != null) {
            inventory.setNonComplianceNoti(baseInfo.getNonComplianceNoti());
        }
        if (baseInfo.getRefundReturnType() != null) {
            inventory.setRefundReturnType(baseInfo.getRefundReturnType());
        }
        if (baseInfo.getPriceRole() != null) {
            inventory.setPriceRole(baseInfo.getPriceRole());
        }
        if (baseInfo.getListingAge() != null) {
            inventory.setListingAge(baseInfo.getListingAge());
        }
        if (baseInfo.getFirstListedDate() != null) {
            inventory.setFirstListedDate(baseInfo.getFirstListedDate());
        }
        if (baseInfo.getDateSold() != null) {
            inventory.setDateSold(baseInfo.getDateSold());
        }
        if (baseInfo.getMasterMultiListing() != null) {
            inventory.setMasterMultiListing(baseInfo.getMasterMultiListing());
        }
        if (baseInfo.getReviseItem() != null) {
            inventory.setReviseItem(baseInfo.getReviseItem());
        }
        if (baseInfo.getRecordType() != null) {
            inventory.setRecordType(baseInfo.getRecordType());
        }
        if (baseInfo.getPartnerEmail() != null) {
            inventory.setPartnerEmail(baseInfo.getPartnerEmail());
        }
        if (baseInfo.getNote() != null) {
            inventory.setNote(baseInfo.getNote());
        }
        if (baseInfo.getSite() != null) {
            inventory.setSite(baseInfo.getSite());
        }
        if (baseInfo.getStockLocationId() != null) {
            inventory.setStockLocationId(baseInfo.getStockLocationId());
        }
        if (baseInfo.getDateReceived() != null) {
            inventory.setDateReceived(baseInfo.getDateReceived());
        }
        if (baseInfo.getConditionScore() != null) {
            inventory.setConditionScore(baseInfo.getConditionScore());
        }
        if (baseInfo.getPrivatePartyValue() != null) {
            inventory.setPrivatePartyValue(baseInfo.getPrivatePartyValue());
        }
        if (baseInfo.getCondition() != null) {
            inventory.setCondition(baseInfo.getCondition());
            updateDescription = true;
        }
        if (baseInfo.getValueAdditionalComponent() != null) {
            inventory.setValueAdditionalComponent(baseInfo.getValueAdditionalComponent());
        }

        Float valuePrice = null;
        //update from bbb value
        if (inventory.getBbbValue() != null) {
            valuePrice = inventory.getBbbValue();
        }
        //update private
        if (baseInfo.getOverrideTradeInPrice() != null) {
            if (baseInfo.getOverrideTradeInPrice() < 0) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BBB_VALUE_MUST_BE_POSITIVE), HttpStatus.BAD_REQUEST);
            }
            valuePrice = inventory.getOverrideTradeInPrice();
        }
        if (valuePrice != null) {
            float additionalValue = 0;
            List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByInventoryIdId(inventory.getId());
            if (inventoryCompTypes.size() > 0) {
                for (InventoryCompType inventoryCompType : inventoryCompTypes) {
                    additionalValue += inventoryCompType.getAdditionalValue();
                }
            }
            inventory.setOverrideTradeInPrice(baseInfo.getOverrideTradeInPrice());
            inventory.setInitialListPrice((valuePrice + additionalValue) / 0.6f);
            inventory.setDiscountedPrice((valuePrice + additionalValue) / 0.6f);
            if (baseInfo.getCurrentListPrice() != null) {
                inventory.setDiscountedPrice(inventory.getCurrentListedPrice() - (inventory.getFlatPriceChange() == null ? 0 : inventory.getFlatPriceChange()));
            } else {
                inventory.setCurrentListedPrice(inventory.getDiscountedPrice() + (inventory.getFlatPriceChange() == null ? 0 : inventory.getFlatPriceChange()));
            }

            inventory.setCogsPrice(additionalValue + valuePrice);
        }

        if (updateDescription) {
            if (initialInventory == null) {
                Bicycle bicycle = bicycleRepository.findOne(baseInfo.getBicycleId());
                initialInventory = initialInventoryRepository.initInventory(
                        bicycle.getModelId(),
                        bicycle.getBrandId(),
                        bicycle.getYearId(),
                        bicycle.getTypeId(),
                        bicycle.getSizeId()
                );
            }
            if (initialInventory != null) {
                inventory.setDescription(generateDescription(initialInventory, inventory.getBicycleId(), inventory.getSerialNumber(), inventory.getCondition()));
            }
        }

        if (inventoryBicycle != null) {
            inventoryBicycleRepository.save(inventoryBicycle);
        }

        return inventory;
    }

    private InventoryImage updateInventoryImage(long inventoryId, List<InventoryImageUpdateRequest> images) {
        List<InventoryImage> inventoryImagesDelete = inventoryImageRepository.findAllByInventoryId(inventoryId);
        if (inventoryImagesDelete.size() > 0) {

            List<String> s3ImageKeys = new ArrayList<>();
            for (InventoryImage inventoryImage : inventoryImagesDelete) {
                String link = inventoryImage.getImage();
                if (link.contains(awsResourceConfig.getDefaultS3Bucket())) {
                    s3ImageKeys.add(link.substring(link.lastIndexOf("/") + 1));
                }
            }
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_ORIGINAL, s3ImageKeys);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_LARGE, s3ImageKeys);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_SMALL, s3ImageKeys);

            inventoryManagerAsync.deleteInventoryImageAsync(inventoryImagesDelete);
        }

        List<InventoryImage> inventoryImages = images.stream().map(image -> new InventoryImage(
                inventoryId,
                image.getLink()
        )).collect(Collectors.toList());

        return inventoryImageRepository.saveAll(inventoryImages).get(0);
    }

    private InventoryLocationShipping updateLocation(
            Inventory inventory, InventoryLocationShipping locationShipping,
            InventoryLocationShippingCreateRequest request, GoogleServiceResponse googleResponse
    ) throws ExceptionResponse {
        if (request == null) {
            return locationShipping;
        }
        if (locationShipping == null) {
            locationShipping = new InventoryLocationShipping();
            if (googleResponse == null || googleResponse.getResults().size() < 1) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MessageResponses.SHIPPING_LOCATION_INVALID,
                        HttpStatus.BAD_REQUEST
                );
            }
            locationShipping.setInventoryId(inventory.getId());
        }

        if (request.getInsurance() != null) {
            locationShipping.setInsurance(request.getInsurance());
        }
        if (request.getAllowLocalPickup() != null) {
            locationShipping.setAllowLocalPickup(request.getAllowLocalPickup());
        }
        if (request.getFlatRate() != null) {
            locationShipping.setFlatRate(request.getFlatRate());
        }
        if (request.getShippingType() != null) {
            locationShipping.setShippingType(request.getShippingType());
        }
        if (googleResponse != null) {
            AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
            AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
            AddressComponent city = GoogleServiceUtils.getCity(googleResponse);
            AddressComponent county = GoogleServiceUtils.getAddressLevel2(googleResponse);

            locationShipping.setZipCode(request.getZipCode());
            locationShipping.setAddressLine(request.getAddressLine());
            locationShipping.setCountryName(country.getLongName());
            locationShipping.setCountryCode(country.getShortName());
            locationShipping.setStateName(state.getLongName());
            locationShipping.setStateCode(state.getShortName());
            locationShipping.setCityName(city.getLongName());
            if (county != null) {
                locationShipping.setCounty(county.getLongName());
            }
            try {
                if (googleResponse.getResults().size() > 0) {
                    GeometryLocation geometryLocation = googleResponse.getResults().get(0).getGeometry().getLocation();
                    locationShipping.setLatitude(geometryLocation.getLat());
                    locationShipping.setLongitude(geometryLocation.getLng());
                }
            } catch (NullPointerException e) {
                //no geometry location from google map
            }
        }

        return inventoryLocationShippingRepository.save(locationShipping);
    }

    private void verifyInventoryUpdateBaseInfo(
            InventoryBaseUpdateRequest baseInfo, InventoryLocationShipping locationShipping
    ) throws ExceptionResponse {
        if (baseInfo == null) return;

        baseInfo.validateUpdate();
        if (baseInfo.getInventoryTypeId() != null) {
            InventoryType inventoryType = inventoryTypeRepository.findOne(baseInfo.getInventoryTypeId());
            if (inventoryType == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_TYPE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
        if (baseInfo.getStockLocationId() != null) {
            if (stockLocationRepository.exist(baseInfo.getStockLocationId()) == 0) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_NOT_FOUND, STOCKE_LOCATION_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
        }

        if (baseInfo.getStatus() == StatusInventory.ACTIVE) {
            if (locationShipping == null) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        INVENTORY_LOCATION_SHIPPING_NOT_FOUND,
                        HttpStatus.CONFLICT
                );
            }
        }
    }


    private void verifyInventoryImage(List<InventoryImageUpdateRequest> images) throws ExceptionResponse {
        if (images == null || images.size() < 1) return;

        for (InventoryImageUpdateRequest image : images) {
            if (image.getLink() == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, IMAGE_MUST_NOT_EMPTY),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }

    private GoogleServiceResponse verifyUpdateInventoryLocation(
            InventoryLocationShipping locationShipping,
            InventoryLocationShippingCreateRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        if (request == null) return null;

        boolean validate = false;
        if (locationShipping == null) {
            BeanPropertyBindingResult errors = new BeanPropertyBindingResult(request, "request");
            validator.validate(request, errors);
            Method currentMethod = new Object() {
            }.getClass().getEnclosingMethod();
            if (errors.hasErrors()) {
                throw new MethodArgumentNotValidException(
                        new MethodParameter(currentMethod, 0),
                        errors);
            }
            validate = true;
        } else {
            if (request.getAddressLine() == null) {
                request.setAddressLine(locationShipping.getAddressLine());
            } else {
                validate = true;
            }
            if (request.getCityName() == null) {
                request.setCityName(locationShipping.getCityName());
            } else {
                validate = true;
            }
            if (request.getState() == null) {
                request.setState(locationShipping.getStateCode());
            } else {
                validate = true;
            }
            if (request.getZipCode() == null) {
                request.setZipCode(locationShipping.getZipCode());
            } else {
                validate = true;
            }
            if (request.getCountry() == null) {
                request.setCountry(locationShipping.getCountryName());
            } else {
                validate = true;
            }
        }
        if (validate) {
            GoogleServiceResponse response = myListingManager.validateShippingLocation(
                    request.getAddressLine(),
                    request.getCityName(),
                    request.getState(),
                    request.getZipCode(),
                    request.getCountry()
            );
            return response;
        }
        return null;
    }

    //    @Async
    public void updateInventoryStepOne(TradeIn tradeIn, Bicycle bicycle, List<Long> upgradeCompIds, TradeInCustomQuote cutomQuote) {
        InitialInventory initial = initialInventoryRepository.initInventory(
                bicycle.getModelId(),
                bicycle.getBrandId(),
                bicycle.getYearId(),
                bicycle.getTypeId(),
                bicycle.getSizeId()
        );
        BicycleType bicycleType = bicycleTypeRepository.findOne(tradeIn.getBicycleTypeId());

        String yearName = "";
        String condition = "";
        String typeName = "";
        String sizeName = "";
        String frameMaterialName = "";
        String brakeTypeName = "";
        String brandName = "";
        if (initial != null) {
            Inventory inventory = new Inventory();
            inventory.setBicycleModelName(initial.getBicycleModelName());
            inventory.setBicycleBrandName(initial.getBicycleBrandName());
            inventory.setBicycleYearName(initial.getBicycleYearName());
//            inventory.setBicycleTypeName(initial.getBicycleTypeName());
            inventory.setBicycleTypeName(bicycleType.getName());
            inventory.setBicycleSizeName(initial.getBicycleSizeName());

            inventory.setRecordType(RecordTypeInventory.BIKE);
            inventory.setTitle(inventory.getBicycleYearName() + " " + inventory.getBicycleBrandName() + " " + inventory.getBicycleModelName());
            inventory.setSellerId(ValueCommons.SELLER_ID_DEFAULT);
            //inv from scorecard will be sold by bbb admin
            inventory.setSellerIsBBB(true);
            inventory.setBicycleId(bicycle.getId());
            inventory.setCondition(tradeIn.getCondition());
            inventory.setPartnerId(tradeIn.getPartnerId());
            inventory.setSerialNumber(tradeIn.getSerialNumber());
//            inventory.setSizeId(tradeIn.getSizeId());

            yearName = initial.getBicycleYearName();
            typeName = initial.getBicycleTypeName();
            sizeName = initial.getBicycleSizeName();
            brandName = initial.getBicycleBrandName();
            condition = tradeIn.getCondition().getValue();

            inventory.setScoreCardId(tradeIn.getId());
            if (cutomQuote != null) {
                inventory.setTypeId(cutomQuote.getTypeId());
            }


            inventory.setStatus(StatusInventory.IN_ACTIVE);
            inventory.setStage(StageInventory.INCOMPLETE_BICYCLE_DETAILS);

            String description = generateDescription(bicycle, tradeIn.getSerialNumber(), tradeIn.getCondition());
            inventory.setDescription(description);
            //set typeId
            InventoryType inventoryType = inventoryTypeRepository.findOneByName(Constants.TRADE_IN_TYPE);
            if (inventoryType != null) {
                inventory.setTypeId(inventoryType.getId());
            }

            //price
            //            override_trade_in_price
            inventory.setMsrpPrice(bicycle.getRetailPrice());
            inventory.setBbbValue(tradeIn.getValue());

//            searchGuideRecommendation
            inventory.setSearchGuideRecommendation(
                    initial.getBicycleYearName() + " " + tradeIn.getCondition() + " " + initial.getBicycleBrandName()
            );

            //test
            inventory.setInitialListPrice(tradeIn.getValue());
            inventory.setCurrentListedPrice(tradeIn.getValue());

            inventory = inventoryRepository.save(inventory);

            inventory.setName("INV-" + inventory.getId());

            inventory.seteBikeMileage(tradeIn.geteBikeMileage());
            inventory.seteBikeHours(tradeIn.geteBikeHours());

            if (cutomQuote != null) {
                inventory.setCustomQuote(true);
            } else {
                inventory.setCustomQuote(false);
            }

            setValueGuidePrice(inventory, bicycle);

            inventory = inventoryRepository.save(inventory);
            long inventoryId = inventory.getId();

            if (upgradeCompIds != null && upgradeCompIds.size() > 0) {
                List<TradeInUpgradeComp> tradeInUpgradeComps = tradeInUpgradeCompRepository.findAll(upgradeCompIds);
                inventoryUpgradeCompRepository.saveAll(
                        tradeInUpgradeComps.stream().map(tr -> {
                            InventoryUpgradeComp inventoryUpgradeComp = new InventoryUpgradeComp();
                            inventoryUpgradeComp.setInventoryId(inventoryId);
                            inventoryUpgradeComp.setName(tr.getName());
                            inventoryUpgradeComp.setPercentValue(tr.getPercentValue());

                            inventoryUpgradeComp.setUp(tr.isUp());
                            return inventoryUpgradeComp;
                        }).collect(Collectors.toList())
                );
            }


            List<Long> compIds = new ArrayList<>();

            if (cutomQuote != null) {
                List<TradeInCustomQuoteCompDetail> comps = tradeInCustomQuoteCompDetailRepository.findAllByCustomQuote(cutomQuote.getId());
                if (comps.size() > 0) {
                    List<InventoryCompDetail> invComps = comps.stream().map(o -> {
                        InventoryCompDetail comp = new InventoryCompDetail();
                        InventoryCompDetailId id = new InventoryCompDetailId();
                        id.setInventoryId(inventoryId);
                        id.setInventoryCompTypeId(o.getId().getCompTypeId());
                        comp.setId(id);
                        comp.setValue(o.getValue());
                        return comp;
                    }).collect(Collectors.toList());

                    inventoryCompDetailRepository.saveAll(invComps).forEach(c -> compIds.add(c.getId().getInventoryCompTypeId()));
                }
            } else {
                List<TradeInCompDetail> comps = tradeInCompDetailRepository.findByTradeIn(tradeIn.getId());
                if (comps.size() > 0) {
                    List<InventoryCompDetail> invComps = comps.stream().map(o -> {
                        InventoryCompDetail comp = new InventoryCompDetail();
                        InventoryCompDetailId id = new InventoryCompDetailId();
                        id.setInventoryId(inventoryId);
                        id.setInventoryCompTypeId(o.getId().getInventoryCompTypeId());
                        comp.setId(id);
                        comp.setValue(o.getValue());
                        return comp;
                    }).collect(Collectors.toList());

                    inventoryCompDetailRepository.saveAll(invComps).forEach(c -> compIds.add(c.getId().getInventoryCompTypeId()));
                }
            }

            //Feb 2019: using comps from trade-in comp, not from bicycle

//            List<BicycleComponentTypeDetail> details = bicycleComponentTypeDetailRepository.findAllByBicycleId(bicycle.getId());
//            if (bicycle.getSizeId() != null) {
//                BicycleSize bicycleSize = bicycleSizeRepository.findOne(bicycle.getSizeId());
//                if (bicycleSize != null) {
//                    BicycleComponentTypeDetail detail = new BicycleComponentTypeDetail();
//                    detail.setBicycleId(bicycle.getId());
//                    detail.setComponentName(ValueCommons.INVENTORY_SIZE_NAME);
//                    detail.setComponentValue(bicycleSize.getName());
//                    details.add(detail);
//                }
//
//            }
//            List<Long> compIds = new ArrayList<>();
//            if (details.size() > 0) {
//                List<String> compsName = details.stream().map(BicycleComponentTypeDetail::getComponentName).collect(Collectors.toList());
//
//                List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByName(compsName);
//                List<String> componentsReal = inventoryCompTypes.stream().map(InventoryCompType::getName).collect(Collectors.toList());
//                List<BicycleComponentTypeDetail> listNotHas = details.stream().filter(type -> !componentsReal.contains(type.getComponentName())).collect(Collectors.toList());
//                if (listNotHas.size() > 0) {
//                    List<InventoryCompType> compTypes = listNotHas.stream().map(nameOut -> {
//                        InventoryCompType inventoryCompType = new InventoryCompType();
//                        inventoryCompType.setName(nameOut.getComponentName());
//                        inventoryCompType.setSelect(false);
//                        return inventoryCompType;
//                    }).collect(Collectors.toList());
//                    Iterable<InventoryCompType> inComps = inventoryCompTypeRepository.saveAll(compTypes);
//                    for (InventoryCompType inComp : inComps) {
//                        inventoryCompTypes.add(inComp);
//                    }
//                }
//                compIds.addAll(inventoryCompTypes.stream().map(InventoryCompType::getId).collect(Collectors.toList()));
//                CommonUtils.stream(compIds);
//                Pair<String, String> pairFrameMaterial = new Pair<>();
//                Pair<String, String> pairBrakeType = new Pair<>();
//                List<InventoryCompDetail> inventoryCompDetails = details.stream().map(detail -> {
//                    InventoryCompType inventoryCompType = CommonUtils.findObject(inventoryCompTypes, comp -> comp.getName().toUpperCase().equals(detail.getComponentName().toUpperCase()));
//                    if (inventoryCompType == null) {
//                        return null;
//                    }
//                    if (inventoryCompType.getName().equals(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME)) {
//                        pairFrameMaterial.setFirst(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME);
//                        pairFrameMaterial.setSecond(detail.getComponentValue());
//                    } else {
//                        if (inventoryCompType.getName().equals(ValueCommons.INVENTORY_BRAKE_TYPE_NAME)) {
//                            pairBrakeType.setFirst(ValueCommons.INVENTORY_BRAKE_TYPE_NAME);
//                            pairBrakeType.setSecond(detail.getComponentValue());
//                        }
//                    }
//                    InventoryCompDetail compDetail = new InventoryCompDetail();
//                    InventoryCompDetailId id = new InventoryCompDetailId();
//                    id.setInventoryId(inventoryId);
//                    id.setInventoryCompTypeId(inventoryCompType.getId());
//                    compDetail.setValue(detail.getComponentValue());
//                    compDetail.setId(id);
//                    return compDetail;
//                }).filter(o -> o != null).collect(Collectors.toList());
//                if (pairFrameMaterial.getFirst() != null) {
//                    frameMaterialName = pairFrameMaterial.getSecond();
//                }
//                if (pairBrakeType.getFirst() != null) {
//                    brakeTypeName = pairBrakeType.getSecond();
//                }
//
//                if (inventoryCompDetails.size() > 0) {
//                    inventoryCompDetailRepository.saveAll(inventoryCompDetails);
//                }
//            }
            //end

            //save spec
            if (tradeIn.getBicycleId() != null) {
                List<BicycleSpec> bicycleSpecs = bicycleSpecRepository.findAllByBicycleId(tradeIn.getBicycleId());
                if (bicycleSpecs.size() > 0) {
                    List<InventorySpec> inventorySpecs = bicycleSpecs.stream().map(spe -> {
                        InventorySpec invSpe = new InventorySpec();
                        invSpe.setInventoryId(inventoryId);
                        invSpe.setName(spe.getName());
                        invSpe.setValue(spe.getValue());
                        return invSpe;
                    }).collect(Collectors.toList());
                    inventorySpecRepository.saveAll(inventorySpecs);
                }
            }
            //end


            updatePriceInventory(inventory, compIds, inventory.getBbbValue(), true);


            //save recommendation value guide

            String recommendation = yearName + " " + condition + " " + typeName + (sizeName == null ? "" : " " + sizeName);
            if (!frameMaterialName.equals("")) {
                recommendation = recommendation + " " + frameMaterialName;
            }
            if (!brakeTypeName.equals("")) {
                recommendation = recommendation + " " + brakeTypeName;
            }
            recommendation = recommendation + " " + brandName;
            inventory.setSearchGuideRecommendation(recommendation);


            inventory = inventoryRepository.save(inventory);


            InventoryBicycle inventoryBicycle = new InventoryBicycle();
            inventoryBicycle.setInventoryId(inventory.getId());
            inventoryBicycle.setBrandId(bicycle.getBrandId());
            inventoryBicycle.setModelId(bicycle.getModelId());
            inventoryBicycle.setYearId(bicycle.getYearId());
            inventoryBicycle.setTypeId(bicycle.getTypeId());
            inventoryBicycle.setSizeId(bicycle.getSizeId());

            inventoryBicycleRepository.save(inventoryBicycle);
            //end
        }

    }

    public void updatePriceInventory(Inventory inventory, List<Long> compTypeIds, float bbbValue, boolean isTypeTradeIn) {
        inventory.setCurrentListedPrice(bbbValue);
        float additionValue = 0;
        if (compTypeIds.size() > 0) {
            List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByIds(compTypeIds);
            for (InventoryCompType inventoryCompType : inventoryCompTypes) {
                additionValue += inventoryCompType.getAdditionalValue();
            }
        }
        inventory.setValueAdditionalComponent(additionValue);
        float initialPrice = (bbbValue + additionValue) / 0.6f;
        if (isTypeTradeIn) {
            if (initialPrice < Constants.MIN_INIT_PRICE_TRADE_IN) {
                initialPrice = Constants.MIN_INIT_PRICE_TRADE_IN;
            }
        }
        inventory.setCogsPrice(bbbValue + additionValue);
        inventory.setInitialListPrice(initialPrice);
        inventory.setCogsPrice(bbbValue + additionValue);
        inventory.setFlatPriceChange(0f);
        inventory.setDiscountedPrice(initialPrice);
        inventory.setCurrentListedPrice(initialPrice);
        inventory.setPrivatePartyValue(Constants.PRIVATE_PRICE_DEFAULT);
    }

    private void saveInventoryTradeInOwner(TradeIn tradeIn, long inventoryId) {
        InventoryTradeInOwner inventoryTradeIn = new InventoryTradeInOwner();
        inventoryTradeIn.setInventoryId(inventoryId);
        inventoryTradeIn.setName(tradeIn.getOwnerName());
        inventoryTradeIn.setPhone(tradeIn.getOwnerPhone());
        inventoryTradeIn.setStreet(tradeIn.getOwnerAddress());
        inventoryTradeIn.setCity(tradeIn.getOwnerCity());
        inventoryTradeIn.setState(tradeIn.getOwnerState());
        inventoryTradeIn.setPostCode(tradeIn.getOwnerZipCode());
        inventoryTradeIn.setEmail(tradeIn.getOwnerEmail());
        inventoryTradeIn.setProofDate(tradeIn.getProofDate());
        inventoryTradeIn.setProofName(tradeIn.getProofName());
        inventoryTradeIn.setTradedBikeId(tradeIn.getBicycleId());
//            inventoryTradeIn.setTradedForBike(tradeIn.getBicycleId());

        inventoryTradeInOwnerRepository.save(inventoryTradeIn);
    }

    private String generateDescription(Bicycle bicycle, String serial, ConditionInventory conditionInventory) {
        InitialInventory initial = initialInventoryRepository.initInventory(
                bicycle.getModelId(),
                bicycle.getBrandId(),
                bicycle.getYearId(),
                bicycle.getTypeId(),
                bicycle.getSizeId()
        );
        return generateDescription(initial, bicycle.getId(), serial, conditionInventory);
    }

    private String generateDescription(InitialInventory initialInventory, Long bicycleId, String serial, ConditionInventory conditionInventory) {
        StringBuilder specifications = new StringBuilder();
        if (initialInventory != null) {
            specifications.append("<p><b>Specifications</b></p>");
            specifications.append("<p>Brand: ").append(initialInventory.getBicycleBrandName()).append("</p>");
            specifications.append("<p>Model: ").append(initialInventory.getBicycleModelName()).append("</p>");
            specifications.append("<p>Year: ").append(initialInventory.getBicycleYearName()).append("</p>");
            if (serial != null) {
                specifications.append("<p>Serial: ").append(serial).append("</p>");
            }
            specifications.append("<p>Condition: ").append(conditionInventory.getValue()).append("</p>");
            specifications.append("<p>Type: ").append(initialInventory.getBicycleTypeName()).append("</p>");
        }


        StringBuilder component = new StringBuilder();
        List<BicycleComponentResponse> bicycleComponentResponses =
                bicycleComponentManager.findAllByBicycleId(bicycleId);
        if (bicycleComponentResponses.size() > 0) {
            component.append("<p><b>Components</b></p>");
            for (BicycleComponentResponse bicycleComponentRespons : bicycleComponentResponses) {
                component.append("<p>").append(bicycleComponentRespons.getComponentName()).append(": ").append(bicycleComponentRespons.getComponentValue()).append("</p>");
            }
        }

        StringBuilder driveTrain = new StringBuilder();
        StringBuilder geometry = new StringBuilder();
        StringBuilder reportCondition = new StringBuilder();
        StringBuilder other = new StringBuilder();
        List<BicycleSpec> bicycleSpecs = bicycleSpecRepository.findAllByBicycleId(bicycleId);
        if (bicycleSpecs.size() > 0) {
            for (BicycleSpec bicycleSpec : bicycleSpecs) {
                if (CommonUtils.contain(DRIVE_TRAINS, bicycleSpec.getName(), String::compareTo)) {
                    if (driveTrain.toString().equals("")) {
                        driveTrain.append("<p><b>Drivetrain</b></p>");
                    }
                    driveTrain.append("<p>").append(bicycleSpec.getName()).append(": ").append(bicycleSpec.getValue()).append("</p>");
                } else {
                    if (CommonUtils.contain(SPECIFICATIONS, bicycleSpec.getName(), String::compareTo)) {
                        specifications.append("<p>").append(bicycleSpec.getName()).append(": ").append(bicycleSpec.getValue()).append("</p>");
                    } else {
                        if (CommonUtils.contain(GEOME_TRYS, bicycleSpec.getName(), String::compareTo)) {
                            if (geometry.toString().equals("")) {
                                geometry.append("<p><b>Geometry</b></p>");
                            }
                            geometry.append("<p>").append(bicycleSpec.getName()).append(": ").append(bicycleSpec.getValue()).append("</p>");
                        } else {
                            if (CommonUtils.contain(REPORT_CONDITION, bicycleSpec.getName(), String::compareTo)) {
                                if (reportCondition.toString().equals("")) {
                                    reportCondition.append("<p><b>Report Condition</b></p>");
                                }
                                reportCondition.append("<p>").append(bicycleSpec.getName()).append(": ").append(bicycleSpec.getValue()).append("</p>");
                            } else {
                                if (other.toString().equals("")) {
                                    other.append("<p><b>Others</b></p>");
                                }
                                other.append("<p>").append(bicycleSpec.getName()).append(": ").append(bicycleSpec.getValue()).append("</p>");
                            }
                        }
                    }
                }
            }
        }

        StringBuilder condition = new StringBuilder();
        if (conditionInventory != null) {
            ConditionDescription conditionDescription = conditionDescriptionRepository.findOne(conditionInventory.getValue());
            if (conditionDescription != null) {
                condition.append("<p><b>Description of ").append(conditionInventory.getValue())
                        .append("</b></p>");
                condition.append(conditionDescription.getDescription());

            }
        }
        StringBuilder description = new StringBuilder();
        if (!specifications.toString().equals("")) {
            description.append(specifications);
        }
        if (!component.toString().equals("")) {
            description.append("<br>").append(component);
        }
        if (!driveTrain.toString().equals("")) {
            description.append("<br>").append(driveTrain);
        }
        if (!geometry.toString().equals("")) {
            description.append("<br>").append(geometry);
        }
        if (!reportCondition.toString().equals("")) {
            description.append("<br>").append(reportCondition);
        }
        if (!other.toString().equals("")) {
            description.append("<br>").append(other);
        }
        if (!condition.toString().equals("")) {
            description.append("<br>").append(condition);
        }

        return description.toString();
    }

    //    @Async
    public void updateInventoryStepTwo(TradeIn tradeIn) {
        Inventory inventory = inventoryRepository.findByScoreCardId(tradeIn.getId());

        if (inventory != null) {
            inventory.setSerialNumber(tradeIn.getSerialNumber());
            inventory.setDescription(
                    generateDescription(bicycleRepository.findOne(inventory.getBicycleId()), tradeIn.getSerialNumber(), tradeIn.getCondition())
            );
            inventory.setTitle(inventory.getBicycleYearName() + " " + inventory.getBicycleBrandName() + " " + inventory.getBicycleModelName());
            inventoryRepository.save(inventory);

            saveInventoryTradeInOwner(tradeIn, inventory.getId());
        }
    }

    //    @Async
    public void updateInventoryStepThree(List<TradeInImage> tradeInImages) {
        Inventory inventory = inventoryRepository.findByScoreCardId(tradeInImages.get(0).getTradeInId());

        if (inventory != null) {
            List<InventoryImage> inventoryImages = new ArrayList<>();
            for (TradeInImage tradeInImage : tradeInImages) {
                InventoryImage inventoryImage = new InventoryImage();
                inventoryImage.setInventoryId(inventory.getId());
                inventoryImage.setImage(tradeInImage.getFullLink());
                inventoryImages.add(inventoryImage);
            }
            inventoryImages = inventoryImageRepository.saveAll(inventoryImages);
            InventoryImage firstInventoryImage = inventoryImages.get(0);
            inventory.setImageDefaultId(firstInventoryImage.getId());
            inventory.setImageDefault(firstInventoryImage.getImage());
            inventoryRepository.save(inventory);
        }
    }

    //    @Async
    public void updateInventoryStepFour(TradeIn tradeIn, TradeInShipping tradeInShipping) {
        Inventory inventory = inventoryRepository.findByScoreCardId(tradeIn.getId());

        if (inventory != null) {
            inventory.setStatus(StatusInventory.PENDING);
            inventory.setStage(StageInventory.IN_TRANSIT_TO_BBB);

            inventoryRepository.save(inventory);

            saveInventoryShipping(inventory.getId(), tradeIn, tradeInShipping);
            saveLocation(inventory.getId(), tradeInShipping);
        }
    }

    private void saveInventoryShipping(long inventoryId, TradeIn tradeIn, TradeInShipping tradeInShipping) {
        if (tradeInShipping.getShippingType() == InboundShippingType.DROP_OFF_BICYCLE_BLUE_BOOK_TYPE) {
            //admin will need to update location when active this inv
            return;
        }
        InventoryShipping inventoryShipping = new InventoryShipping();
        inventoryShipping.setInventoryId(inventoryId);
        if (inventoryShipping.getCarrierType() == null) {
            inventoryShipping.setTrackingInBound(tradeInShipping.getCarrierTrackingNumber());
        } else {
            inventoryShipping.setTrackingInBound(tradeInShipping.getTrackingNumber());
        }

        inventoryShipping.setStatusCarrier(StatusCarrier.I);
        inventoryShipping.setShippingLocationInBound(tradeInShipping.getFromAddress());
        inventoryShipping.setTrackingDateInBound(tradeIn.getCreatedTime());
        inventoryShipping.setCarrier(tradeInShipping.getCarrier());
        inventoryShipping.setCarrierType(tradeInShipping.getCarrierType());
        inventoryShipping.setShippingType(tradeInShipping.getShippingType());

        inventoryShippingRepository.save(inventoryShipping);
    }

    private void saveLocation(long inventoryId, TradeInShipping tradeInShipping) {
        if (tradeInShipping.getShippingType() == InboundShippingType.DROP_OFF_BICYCLE_BLUE_BOOK_TYPE) {
            //admin will need to update location when active this inv
            return;
        }
        List<ShipmentDetailResponse> shipmentDetails = null;
        try {
            if (tradeInShipping.getShippingType() == InboundShippingType.BICYCLE_BLUE_BOOK_TYPE) {
                shipmentDetails = shipmentManager.getTradeInShipmentDetails(Collections.singletonList(tradeInShipping.getTradeInId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        InventoryLocationShipping inventoryLocationShipping = new InventoryLocationShipping();
        inventoryLocationShipping.setInventoryId(inventoryId);
        //default for scorecard to calculate insurance when checkout
        inventoryLocationShipping.setAllowLocalPickup(true);
        inventoryLocationShipping.setShippingType(OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE);
        inventoryLocationShipping.setInsurance(true);


        if (shipmentDetails != null && shipmentDetails.size() > 0) {
            ShipmentDetailResponse shipmentDetail = shipmentDetails.get(0);
            inventoryLocationShipping.setZipCode(shipmentDetail.getToZipCode());
            inventoryLocationShipping.setCountryCode(shipmentDetail.getToCountryCode());
            inventoryLocationShipping.setStateCode(shipmentDetail.getToStateCode());
            inventoryLocationShipping.setCityName(shipmentDetail.getToCity());
            inventoryLocationShipping.setAddressLine(shipmentDetail.getToLine());

            String fullLocation = inventoryLocationShipping.getCityName() + " "
                    + inventoryLocationShipping.getStateCode() + " " //state code
                    + inventoryLocationShipping.getCountryCode();
            GoogleServiceResponse response = authManager.getMapGoogleSerive(
                    IntegratedServices.GOOGLE_SERVICE_CATEGORY_INFO, fullLocation);
            AddressComponent country = GoogleServiceUtils.getCountryAddress(response);
            AddressComponent state = GoogleServiceUtils.getAddressLevel1(response);
            AddressComponent county = GoogleServiceUtils.getAddressLevel2(response);
            if (country != null) {
                inventoryLocationShipping.setCountryName(country.getLongName());
                inventoryLocationShipping.setStateName(state.getLongName());
            }
            if (county != null) {
                inventoryLocationShipping.setCounty(county.getLongName());
            }
            try {
                if (response.getResults().size() > 0) {
                    GeometryLocation geometryLocation = response.getResults().get(0).getGeometry().getLocation();
                    inventoryLocationShipping.setLatitude(geometryLocation.getLat());
                    inventoryLocationShipping.setLongitude(geometryLocation.getLng());
                }
            } catch (NullPointerException e) {
                //no geometry location from google map
            }
        }

        inventoryLocationShippingRepository.save(inventoryLocationShipping);
    }

    public void setValueGuidePrice(Inventory inventory) {
        Bicycle bicycle = bicycleRepository.findOne(inventory.getBicycleId());
        if (bicycle != null) {
            setValueGuidePrice(inventory, bicycle);
        }
    }

    public void setValueGuidePrice(Inventory inventory, Bicycle bicycle) {
        Float valueGuidePrice = null;
        List<Pair<ConditionInventory, Float>> prices = valueGuidePriceManager.getValueGuidePrices(
                bicycle.getBrandId(), bicycle.getModelId(), bicycle.getYearId(),
                bicycle.getRetailPrice()
        );
        Pair<ConditionInventory, Float> price = CommonUtils.findObject(prices, p -> p.getFirst() == inventory.getCondition());
        if (price != null) {
            valueGuidePrice = price.getSecond();
        }

        if (valueGuidePrice == null || valueGuidePrice <= 0) {
            //just randomize it
//            Random random = new Random();
//            int percent = 500 + random.nextInt(500);
//            valueGuidePrice = inventory.getCurrentListedPrice() / (percent / 1000f);
        } else {
            inventory.setValueGuidePrice(valueGuidePrice);
            inventory.setDealPercent((inventory.getCurrentListedPrice() / valueGuidePrice) * 100);
        }
    }

    public Object getSecretInventories(List<Long> inventoriesId) {
        List<InventoryDetailResponse> responses =
                inventoryDetailResRepository.findAllByIds(inventoriesId);
        List<InventoryImage> images = inventoryImageRepository.findAllByInventoriesId(inventoriesId);
        Map<Long, List<InventoryImage>> mapImages = images.stream().collect(Collectors.groupingBy(InventoryImage::getInventoryId));
        mapImages.forEach((inventoryId, imgs) -> {
            InventoryDetailResponse res = CommonUtils.findObject(responses, o -> o.getId() == inventoryId);
            if (res != null) {
                res.setImages(
                        imgs.stream().map(InventoryImage::getImage).collect(Collectors.toList())
                );
            }
        });

        return responses;
    }

    public Object soldInventory(MarketListingSaleRequest request) throws ExceptionResponse {
        try {
            System.out.println("InventoryManager content: " + objectMapper.writeValueAsString(request));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (request.getMarketListingId() == null && request.getOfferId() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YOUR_MUST_TRANSMIT_OFFER_ID_OR_MARKET_LISTING_ID),
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getMarketListingId() != null && request.getOfferId() != null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YOUR_MUST_TRANSMIT_OFFER_ID_OR_MARKET_LISTING_ID),
                    HttpStatus.NOT_FOUND
            );
        }
        OrderDetailResponse orderDetailResponse = billingManager.getOrderDetail(request.getOrderId());
        if (request.getMarketListingId() != null) {
            return soldMarketListing(request, orderDetailResponse);
        } else {
            return soldOffer(request, orderDetailResponse);
        }

    }

    private Object soldMarketListing(MarketListingSaleRequest request, OrderDetailResponse orderDetailResponse) throws ExceptionResponse {
        MarketListing marketListing = marketListingRepository.findOne(request.getMarketListingId());
        if (marketListing == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (marketListing.getStatus() != StatusMarketListing.LISTED && marketListing.getStatus() != StatusMarketListing.SALE_PENDING) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, STATUS_MARKET_LISTING_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        List<MarketToList> marketToLists = marketToListRepository.findAllIdActiveEbayBBB(appConfig.getEbay().isSandbox());
        MarketToList marketBBB = CommonUtils.findObject(marketToLists, o -> o.getType() == MarketType.BBB);
        MarketToList marketEbay = CommonUtils.findObject(marketToLists, o -> o.getType() == MarketType.EBAY);
        if (marketBBB == null || marketEbay == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (marketListing.getMarketPlaceId() != marketBBB.getMarketPlaceId()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }



        List<MarketListing> marketListingsEbay = marketListingRepository.findAllMarketListing(marketListing.getInventoryId(), marketEbay.getMarketConfigId());
        //cancel listing on ebay
        if (marketListingsEbay.size() > 0) {
            List<MarketListing> marketListingsListed = marketListingsEbay.stream().filter(o -> o.getStatus() == StatusMarketListing.LISTED).collect(Collectors.toList());
            if (marketListingsListed.size() > 0) {
                marketListingManagerAsync.endAllItemEbayNoAsync(marketListingsListed);
            }
            marketListingRepository.updateStatusMarketListing(
                    marketListingsEbay.stream().map(MarketListing::getId).collect(Collectors.toList()),
                    StatusMarketListing.DE_LISTED.getValue()
            );
        }
        createInventorySale(marketListing.getInventoryId(), request, orderDetailResponse);
        inventoryRepository.updateStatusAndStage(marketListing.getInventoryId(), StatusInventory.SOLD.getValue(), StatusInventory.SOLD.getValue());
        //cancel offer
        offerManager.rejectOtherOffer(null, marketListing.getId(), null);
        //end
        marketListing.setStatus(StatusMarketListing.SOLD);
        marketListing = marketListingRepository.save(marketListing);
        salesforceAsync.updateSaleForceSoldFromMarketListing(marketListing.getId(), orderDetailResponse);
        return marketListing;
    }


    private void createInventorySale(long inventoryId, MarketListingSaleRequest request, OrderDetailResponse order) {
//        OrderDetailResponse order = billingManager.getOrderDetail(request.getOrderId());
        OrderLineItem item = null;
        for (OrderLineItem i : order.getLineItems()) {
            if (i.getMarketListingId().equals(request.getMarketListingId())) {
                item = i;
                break;
            }
        }

        InventorySale inventorySale = new InventorySale();
        inventorySale.setBuyerId(request.getBuyerId());
        inventorySale.setInventoryId(inventoryId);
        inventorySale.setBuyerDisplayName(request.getBuyerDisplayName());
        inventorySale.setBuyerEmail(request.getBuyerEmail());
        inventorySale.setBuyerRole(request.getBuyerRole());
        inventorySale.setOrderId(request.getOrderId());
        if (item != null) {
            inventorySale.setLocalPickup(item.getLocalPickup());
            inventorySale.setShippingType(item.getShippingType());
            inventorySale.setCartType(item.getCartType());
            if (item.getPriceOffer() != null) {
                inventorySale.setPrice(item.getPriceOffer());
            } else {
                inventorySale.setPrice(item.getCurrentListedPrice());
            }
        }
        if (order.getShippingAddress() != null) {
            OrderShippingAddress shipping = order.getShippingAddress();
            inventorySale.setShippingAddressLine(shipping.getLine1());
            inventorySale.setShippingCity(shipping.getCity());
            inventorySale.setShippingState(shipping.getState());
            inventorySale.setShippingPostalCode(shipping.getPostalCode());
            inventorySale.setShippingPhone(shipping.getPhone());
        }
        inventorySaleRepository.save(inventorySale);
    }

    private Object soldOffer(MarketListingSaleRequest request, OrderDetailResponse orderDetailResponse) throws ExceptionResponse {

        Offer offer = offerRepository.findOne(request.getOfferId());
        if (offer == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.NOT_FOUND
            );
        }
        if (offer.getStatus() != StatusOffer.ACCEPTED) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_OFFER_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        if (offer.getMarketListingId() != null) {
            request.setMarketListingId(offer.getMarketListingId());
            soldMarketListing(request, orderDetailResponse);
            offer.setStatus(StatusOffer.COMPLETED);

        } else {
            if (offer.getInventoryAuctionId() != null) {
                soldInventoryAuction(offer, request, orderDetailResponse);
                offer.setStatus(StatusOffer.COMPLETED);
            }
        }
        offer = offerRepository.save(offer);
        salesforceAsync.updateSaleForceSoldFromOffer(offer.getId(), orderDetailResponse);
        return offer;
    }

    private void soldInventoryAuction(Offer offer, MarketListingSaleRequest request, OrderDetailResponse orderDetailResponse) {
        long inventoryId = inventoryAuctionRepository.findInventoryById(offer.getInventoryAuctionId());
        createInventorySale(inventoryId, request, orderDetailResponse);
        inventoryRepository.updateStatusAndStage(inventoryId, StatusInventory.SOLD.getValue(), StageInventory.SOLD.getValue());
    }

    public Object updateInventorySecret(
            long inventoryId, InventoryUpdateSecretRequest request
    ) throws ExceptionResponse {
        Inventory inventory = inventoryRepository.findOne(inventoryId);
        if (inventory == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (inventory.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, INVENTORY_DELETED),
                    HttpStatus.BAD_REQUEST
            );
        }

        InventoryLocationShipping locationShipping = inventoryLocationShippingRepository.findOne(inventory.getId());
        verifyInventoryUpdateBaseInfo(request.getBaseInfo(), locationShipping);

        inventory = updateInventoryBaseInfo(inventory, request.getBaseInfo());

        ConditionInventory oldCondition = inventory.getCondition();
        long oldBicycleId = inventory.getBicycleId();

        inventory = updateInventoryBaseInfo(inventory, request.getBaseInfo());

        if (oldCondition != inventory.getCondition() || oldBicycleId != inventory.getBicycleId()) {
            setValueGuidePrice(inventory);
        }

        //update default image if willing allow update inv images

        String recommentdation = createAndUpdateSearchRecommendation(inventoryId);
        inventory.setSearchGuideRecommendation(recommentdation);
        inventory = inventoryRepository.save(inventory);

        billingManager.inventoryUpdateAsync(inventory.getId());

        return inventory;
    }
}
