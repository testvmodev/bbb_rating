package com.bbb.core.model.database;

import com.bbb.core.model.database.embedded.InventoryCompDetailId;

import javax.persistence.*;

@Entity
public class InventoryCompDetail {
    @EmbeddedId
    private InventoryCompDetailId id;
    private String value;

    public InventoryCompDetailId getId() {
        return id;
    }

    public void setId(InventoryCompDetailId id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
