package com.bbb.core.model.response.tradein.ups.detail.ship;

import com.bbb.core.model.response.tradein.ups.detail.ResponseInfo;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentResponse {
    @JsonProperty("Response")
    private ResponseInfo responseInfo;
    @JsonProperty("ShipmentResults")
    private ShipmentResults shipmentResults;

    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    public ShipmentResults getShipmentResults() {
        return shipmentResults;
    }
}
