package com.bbb.core.model.request.brand;

import org.springframework.web.multipart.MultipartFile;

public class BrandUpdateRequest{
    private String name;
    private MultipartFile brandLogo;
    private Float valueModifier;
    private Boolean isApproved;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(MultipartFile brandLogo) {
        this.brandLogo = brandLogo;
    }

    public Float getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(Float valueModifier) {
        this.valueModifier = valueModifier;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }
}
