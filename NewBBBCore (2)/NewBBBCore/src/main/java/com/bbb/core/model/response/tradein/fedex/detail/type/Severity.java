package com.bbb.core.model.response.tradein.fedex.detail.type;

public enum Severity {
    SUCCESS("SUCCESS"),
    NOTE("NOTE"),
    WARNING("WARNING"),
    ERROR("ERROR"),
    FAILURE("FAILURE");

    private final String value;

    Severity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Severity findByValue(String value) {
        if (value == null) return null;
        switch (value) {
            case "SUCCESS":
                return SUCCESS;
            case "NOTE":
                return NOTE;
            case "WARNING":
                return WARNING;
            case "ERROR":
                return ERROR;
            case "FAILURE":
                return FAILURE;
            default:
                return null;
        }
    }
}
