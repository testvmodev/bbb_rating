package com.bbb.core.manager.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.model.database.BicycleComponent;
import com.bbb.core.model.database.BicycleSpec;
import com.bbb.core.model.database.ComponentCategory;
import com.bbb.core.model.database.ComponentType;
import com.bbb.core.model.response.bicycle.BicycleComponentResponse;
import com.bbb.core.repository.bicycle.BicycleComponentRepository;
import com.bbb.core.repository.bicycle.BicycleSpecRepository;
import com.bbb.core.repository.bicycle.ComponentCategoryRepository;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import com.bbb.core.repository.bicycle.out.BicycleComponentResponseRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BicycleComponentManager {
    @Autowired
    private BicycleComponentRepository bicycleComponentRepository;
    @Autowired
    private ComponentTypeRepository componentTypeRepository;
    @Autowired
    private BicycleSpecRepository bicycleSpecRepository;
    @Autowired
    private ComponentCategoryRepository componentCategoryRepository;
    @Autowired
    private BicycleComponentResponseRepository bicycleComponentResponseRepository;

    public Object importBicycleComponent(long offset, long size) {
        try {
            Class.forName(Constants.DRIVER_SQL);
            Connection conn = DriverManager.getConnection(Constants.PATH_DB_OLD_BICYCLE, Constants.USERNAME_OLD, Constants.PASSWORD_OLD);
            String query = "SELECT [Bicycle].[Detail].Id AS bicycleId, [Import].[SE_Bicycle].XmlData AS xmlData " +
                    "FROM " +
                    "[Import].[SE_Bicycle] JOIN [Bicycle].[Detail] ON [Import].[SE_Bicycle].Sku = [Bicycle].[Detail].SmartEtailing_Id " + "ORDER BY [Import].[SE_Bicycle].Id ASC OFFSET " + offset + " ROWS FETCH NEXT " + size + " ROWS ONLY";

            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            List<Pair<Long, String>> contents = new ArrayList<>();
            while (resultSet.next()) {
                long bicycleId = resultSet.getLong("bicycleId");
//                System.out.println("bicyelId: " + bicycleId);
                String xml = resultSet.getString("xmlData");
                Pair<Long, String> content = new Pair<>(bicycleId, xml);
                contents.add(content);
            }

            Pair<List<BicycleComponent>, List<BicycleSpec>> parsed = parseContents(contents);
            List<BicycleComponent> bicycleComponents = parsed.getFirst();
            List<BicycleSpec> bicycleSpecs = parsed.getSecond();

            resultSet.close();
            stmt.close();
            conn.close();
            boolean result = Observable.zip(saveBicycleComponentType(bicycleComponents), saveBicycleSpec(bicycleSpecs),
                    (o1, o2) -> {
                        return true;
                    }).blockingFirst();
            return "Succuess";

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Pair<List<BicycleComponent>, List<BicycleSpec>> parseContents(List<Pair<Long, String>> contents) {
        List<ComponentType> componentTypes = componentTypeRepository.findAll();

        return parseContents(contents, componentTypes);
    }
    public Pair<List<BicycleComponent>, List<BicycleSpec>> parseContents(
            List<Pair<Long, String>> contents,
            List<ComponentType> componentTypes
    ) {
        List<String> componentNames = componentTypes.stream()
                .map(ComponentType::getName)
                .collect(Collectors.toList());


        List<BicycleComponent> bicycleComponents = new ArrayList<>();
        List<BicycleSpec> bicycleSpecs = new ArrayList<>();

        for (Pair<Long, String> content : contents) {
            SAXBuilder saxBuilder = new SAXBuilder();
            try {
                Document document = saxBuilder.build(new StringReader(content.getSecond()));
                Element element = document.getRootElement();
                List<Element> elements = element.getChild("Specs").getChildren();

                for (Element ele : elements) {
                    String name = ele.getChild("name").getValue();
                    String value = ele.getChild("value").getValue();
                    if (componentNames.contains(name)) {
                        BicycleComponent bicycleComponent = new BicycleComponent();
                        int index = componentNames.indexOf(name);
                        bicycleComponent.setComponentTypeId(componentTypes.get(index).getId());
                        bicycleComponent.setBicycleId(content.getFirst());
                        bicycleComponent.setComponentValue(value);
                        bicycleComponents.add(bicycleComponent);
                    } else {
                        BicycleSpec bicycleSpec = new BicycleSpec();
                        bicycleSpec.setBicycleId(content.getFirst());
                        bicycleSpec.setName(name);
                        bicycleSpec.setValue(value);
                        bicycleSpecs.add(bicycleSpec);
                    }
                }
            } catch (JDOMException | IOException e) {
                e.printStackTrace();
            }
        }

        return new Pair<>(bicycleComponents, bicycleSpecs);
    }

    public Observable<Boolean> saveBicycleComponentType(List<BicycleComponent> bicycleComponents) {
        return Observable.create((ObservableOnSubscribe<Boolean>) nu -> {
            for (BicycleComponent bicycleComponent : bicycleComponents) {
                try {
                    bicycleComponentRepository.save(bicycleComponent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            nu.onNext(true);
            nu.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public Observable<Boolean> saveBicycleSpec(List<BicycleSpec> bicycleSpecs) {
        return Observable.create((ObservableOnSubscribe<Boolean>) nu -> {
            for (BicycleSpec bicycleSpec : bicycleSpecs) {
                try {
                    bicycleSpecRepository.save(bicycleSpec);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            nu.onNext(true);
            nu.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public List<BicycleComponentResponse> findAllByBicycleId(long bicycleId) {
        return remapComponents(bicycleComponentResponseRepository.findAllByBicycleId(bicycleId));
    }

    public List<BicycleComponentResponse> remapComponents(List<BicycleComponentResponse> comps) {
        System.out.println(comps);
        if (comps == null || comps.size() < 1) {
            return comps;
        }

        ComponentCategory suspensionCategory = componentCategoryRepository.findOne(ValueCommons.COMPONENT_CATEGORY_SUSPENSION);
        ComponentCategory framesetCategory = componentCategoryRepository.findOne(ValueCommons.COMPONENT_CATEGORY_FRAMESET);
        String suspensionName = suspensionCategory != null ? suspensionCategory.getName() : null;
        String framesetName = framesetCategory != null ? framesetCategory.getName() : null;
        return comps.stream().filter(c -> {
            if (c.getComponentName().equals(ValueCommons.INVENTORY_REAR_SOCK_NAME)) {
                if (c.getBicycleTypeName() != null
                        && c.getBicycleTypeName().equals(ValueCommons.BICYCLE_TYPE_MOUNTAIN
                )) {
                    c.setCategoryName(suspensionName);
                    return true;
                }
                return false;
            }

            if (c.getComponentName().equals(ValueCommons.INVENTORY_FRONT_SOCK_NAME)) {
                if (c.getBicycleTypeName() != null) {
                    if (c.getBicycleTypeName().equals(ValueCommons.BICYCLE_TYPE_MOUNTAIN)) {
                        c.setCategoryName(suspensionName);
                        return true;
                    }
                    if (c.getBicycleTypeName().equals(ValueCommons.BICYCLE_TYPE_ROAD)) {
                        c.setCategoryName(framesetName);
                        return true;
                    }
                }
                return false;
            }

            if (c.getComponentName().equals(ValueCommons.INV_COMP_FORK_NAME)) {
                if (c.getBicycleTypeName() != null) {
                    if (c.getBicycleTypeName().equals(ValueCommons.BICYCLE_TYPE_MOUNTAIN)) {
                        if (!StringUtils.isBlank(c.getComponentValue())) {
                            if (StringUtils.containsIgnoreCase(c.getComponentValue(), ValueCommons.INV_COMP_VALUE_RIGID)
                                    && !StringUtils.containsIgnoreCase(c.getComponentValue(), ValueCommons.INV_COMP_VALUE_SHOCK)
                            ) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }

            return true;
        }).collect(Collectors.toList());
    }
}