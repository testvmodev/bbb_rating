package com.bbb.core.common.utils;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.database.type.StatusOffer;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class QueryUtils {
    private List<Integer> integers = Collections.singletonList(-1);
    private List<Long> longs = Collections.singletonList(-1l);
    private List<String> strings = Collections.singletonList("-*Hello_._World$#@");

    public List<Integer> fakeIntegers() {
        return integers;
    }

    public List<Long> fakeLongs() {
        return longs;
    }

    public List<String> fakeStrings() {
        return strings;
    }

    public boolean isEmptyList(List<?> list) {
        if (list == null || list.size() < 1) {
            return true;
        }
        return false;
    }

    public List<String> mapListingStatusToString(List<StatusMarketListing> statusMarketListings) {
        if (statusMarketListings == null) {
            return null;
        }
        return statusMarketListings.stream()
                .map(StatusMarketListing::getValue)
                .collect(Collectors.toList());
    }

    public List<String> mapOfferStatusToString(List<StatusOffer> statusOffers) {
        if (statusOffers == null) {
            return null;
        }
        return statusOffers.stream()
                .map(StatusOffer::getValue)
                .collect(Collectors.toList());
    }

    public String joinStrings(List<String> strings) {
        return String.join(
                ",",
                strings.stream()
                .filter(s -> !StringUtils.isBlank(s))
                .collect(Collectors.toList())
        );
    }

    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public List<String> dateToString(List<LocalDate> dates) {
        return dates.stream()
                .map(date -> date.toString())
                .collect(Collectors.toList());
    }
}
