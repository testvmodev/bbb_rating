package com.bbb.core.manager.market.async;

import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.InventoryImage;
import com.bbb.core.repository.inventory.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MyListingManagerAsync {
    @Autowired
    private InventoryRepository inventoryRepository;

    @Async
    public void updateInventoryImage(Inventory inventory, InventoryImage inventoryImage) {
        inventory.setImageDefault(inventoryImage.getImage());
        inventory.setImageDefaultId(inventoryImage.getId());
        inventoryRepository.save(inventory);
    }
}
