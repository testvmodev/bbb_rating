package com.bbb.core.manager.shipping;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.BicycleType;
import com.bbb.core.model.database.ShippingFeeConfig;
import com.bbb.core.model.request.shipping.ShippingFeeConfigCreateRequest;
import com.bbb.core.model.request.shipping.ShippingFeeConfigUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.shipping.ShippingFeeConfigRepository;
import com.bbb.core.repository.shipping.out.InventoryShippingProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShippingProfileManager {
    @Autowired
    private ShippingFeeConfigRepository shippingFeeConfigRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private InventoryShippingProfileRepository inventoryShippingProfileRepository;

    public Object getShippingProfiles(Pageable pageable) {
        Page<ShippingFeeConfig> shippingFeeConfigs = shippingFeeConfigRepository.findAll(pageable);
        ListObjResponse<ShippingFeeConfig> response = new ListObjResponse<>(pageable.getPageNumber(), pageable.getPageSize());
        response.setTotalItem((int)shippingFeeConfigs.getTotalElements());
        response.setData(shippingFeeConfigs.getContent());
        return response;
    }

    public Object addShippingProfile(ShippingFeeConfigCreateRequest request) throws ExceptionResponse {
        validateBicycleType(request.getBicycleTypeId());

        ShippingFeeConfig shippingFeeConfig = shippingFeeConfigRepository.findOne(request.getBicycleTypeId());
        if (shippingFeeConfig != null) {
            if (!shippingFeeConfig.isDelete()) {
                throw new ExceptionResponse(
                        ObjectError.INVALID_ACTION,
                        MessageResponses.SHIPPING_FEE_CONFIG_ALREADY_EXISTED,
                        HttpStatus.CONFLICT
                );
            }
        } else{
            shippingFeeConfig = new ShippingFeeConfig();
        }
        shippingFeeConfig.setBicycleTypeId(request.getBicycleTypeId());
        shippingFeeConfig.setEbayShippingProfileId(request.getEbayShippingProfileId());
        shippingFeeConfig.setShippingFee(request.getShippingFee());
        shippingFeeConfig.setLabel(request.getLabel());
        shippingFeeConfig.setDescription(request.getDescription());

        return shippingFeeConfigRepository.save(shippingFeeConfig);
    }

    public Object updateShippingProfile(
            long bicycleTypeId, ShippingFeeConfigUpdateRequest request
    ) throws ExceptionResponse {
        ShippingFeeConfig shippingFeeConfig = shippingFeeConfigRepository.findOne(bicycleTypeId);
        if (shippingFeeConfig == null || shippingFeeConfig.isDelete()) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.SHIPPING_FEE_CONFIG_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getEbayShippingProfileId() != null) {
            shippingFeeConfig.setEbayShippingProfileId(request.getEbayShippingProfileId());
        }
        if (request.getShippingFee() != null) {
            shippingFeeConfig.setShippingFee(request.getShippingFee());
        }
        return shippingFeeConfigRepository.save(shippingFeeConfig);
    }

    public Object getInventoryShippingProfiles(List<Long> inventoryIds) {
        //TODO fee also based on shipping type, shipping profile does not apply to all shipping types
        return inventoryShippingProfileRepository.findAll(inventoryIds);
    }

    private void validateBicycleType(long bicycleTypeId) throws ExceptionResponse {
        BicycleType bicycleType = bicycleTypeRepository.findOne(bicycleTypeId);
        if (bicycleType == null || bicycleType.isDelete()) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.BICYCLE_TYPE_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
    }
}
