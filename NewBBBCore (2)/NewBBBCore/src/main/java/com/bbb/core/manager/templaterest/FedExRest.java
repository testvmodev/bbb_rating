package com.bbb.core.manager.templaterest;

import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.request.tradein.fedex.FedexDeleteShipRequest;
import com.bbb.core.model.request.tradein.fedex.FedexRateRequest;
import com.bbb.core.model.request.tradein.fedex.FedexProcessShipRequest;
import com.bbb.core.model.request.tradein.fedex.FedexTrackRequest;
import com.bbb.core.model.response.tradein.fedex.FedexDeleteShipResponse;
import com.bbb.core.model.response.tradein.fedex.FedexProcessShipResponse;
import com.bbb.core.model.response.tradein.fedex.FedexRateResponse;
import com.bbb.core.model.response.tradein.fedex.FedexTrackResponse;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class FedExRest extends RestTemplateManager {
    private final Logger LOG = LoggerFactory.getLogger(FedExRest.class);
    private final String urlShipping;
    private final String urlTrack;
    private final String urlRate;
    private XmlMapper xmlMapper;

    @Autowired
    public FedExRest(Environment environment, XmlMapper xmlMapper) {
        super(environment.getProperty("fedex.base_url"));
        urlShipping = environment.getProperty("fedex.shipping_url");
        urlTrack = environment.getProperty("fedex.track_url");
        urlRate = environment.getProperty("fedex.rate_url");

        this.xmlMapper = xmlMapper;
    }

    public FedexProcessShipResponse processShipment(FedexProcessShipRequest request) {
        return postObject(urlShipping, CommonUtils.toXmlString(request, xmlMapper), FedexProcessShipResponse.class, MediaType.APPLICATION_XML);
    }

    public FedexRateResponse rate(FedexRateRequest request) {
        return postObject(urlRate, CommonUtils.toXmlString(request, xmlMapper), FedexRateResponse.class, MediaType.APPLICATION_XML);
    }

    public FedexTrackResponse getTrack(FedexTrackRequest request) {
        return postObject(urlTrack, CommonUtils.toXmlString(request, xmlMapper), FedexTrackResponse.class, MediaType.APPLICATION_XML);
    }

    public FedexDeleteShipResponse deleteShip(FedexDeleteShipRequest request) {
        return postObject(urlShipping, CommonUtils.toXmlString(request, xmlMapper), FedexDeleteShipResponse.class, MediaType.APPLICATION_XML);
    }


}
