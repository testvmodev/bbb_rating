package com.bbb.core.model.response.bid.offer.type;

public enum UserOfferType {
    RECEIVED("Received"),
    MADE("Made");

    private String value;

    UserOfferType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
