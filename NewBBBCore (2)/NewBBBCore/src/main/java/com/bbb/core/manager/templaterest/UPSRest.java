package com.bbb.core.manager.templaterest;

import com.bbb.core.model.request.tradein.ups.UPSRateRequest;
import com.bbb.core.model.request.tradein.ups.UPSShipRequest;
import com.bbb.core.model.request.tradein.ups.UPSTrackRequest;
import com.bbb.core.model.request.tradein.ups.UPSVoidShipRequest;
import com.bbb.core.model.response.tradein.ups.UPSRateResponse;
import com.bbb.core.model.response.tradein.ups.UPSShipResponse;
import com.bbb.core.model.response.tradein.ups.UPSTrackResponse;
import com.bbb.core.model.response.tradein.ups.UPSVoidShipResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class UPSRest extends RestTemplateManager {
    private final String urlShipping;
    private final String urlRate;
    private final String urlTrack;
    private final String urlVoid;

    @Autowired
    public UPSRest(Environment environment) {
        super(environment.getProperty("ups.base_url"));
        this.urlShipping = environment.getProperty("ups.shipping_url");
        this.urlRate = environment.getProperty("ups.rate_url");
        this.urlTrack = environment.getProperty("ups.track_url");
        this.urlVoid = environment.getProperty("ups.void_url");
    }

    public UPSShipResponse createPublishRateShip(UPSShipRequest request) {
        return postObject(urlShipping, request, UPSShipResponse.class, MediaType.APPLICATION_JSON);
    }

    public UPSRateResponse rate(UPSRateRequest request) {
        return postObject(urlRate, request, UPSRateResponse.class, MediaType.APPLICATION_JSON);
    }

    public UPSTrackResponse track(UPSTrackRequest request) {
        return postObject(urlTrack, request, UPSTrackResponse.class, MediaType.APPLICATION_JSON);
    }

    public UPSVoidShipResponse voidShip(UPSVoidShipRequest request) {
        return postObject(urlVoid, request, UPSVoidShipResponse.class, MediaType.APPLICATION_JSON);
    }
}
