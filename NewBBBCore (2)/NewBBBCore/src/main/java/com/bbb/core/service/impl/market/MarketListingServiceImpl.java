package com.bbb.core.service.impl.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.market.MarketListingManager;
import com.bbb.core.model.request.market.MarketListingRequest;
import com.bbb.core.model.request.market.UserMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.Test;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import com.bbb.core.service.market.MarketListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarketListingServiceImpl implements MarketListingService {
    @Autowired
    private MarketListingManager manager;

    @Override
    public Object getMarketListings(MarketListingRequest request, Pageable page) throws ExceptionResponse {
        return manager.getMarketListings(request, page);
    }

    @Override
    public Object getMarketListingDetail(long marketListingId, boolean isView) throws ExceptionResponse {
        return manager.getMarketListingDetail(marketListingId, isView);
    }

    @Override
    public Object notificationEbay(String contentNotification) throws Exception {
        return manager.notificationEbay(contentNotification);
    }

    @Override
    public Object getRecommends() throws ExceptionResponse {
        return manager.getRecommends();
    }

    @Override
    public Object getSecretMarketListing(long marketListingId) throws ExceptionResponse {
        return manager.getSecretMarketListing(marketListingId);
    }

    @Override
    public Object getSecretMarketListings(List<Long> marketListingIds) throws ExceptionResponse {
        return manager.getSecretMarketListings(marketListingIds);
    }

    @Override
    public Object getUserMarketListings(UserMarketListingRequest request) throws ExceptionResponse {
        return manager.getUserMarketListings(request);
    }

    @Override
    public Object getMarketListingsForMessage(List<Long> marketListingIds) throws ExceptionResponse {
        return manager.getMarketListingsForMessage(marketListingIds);
    }
}
