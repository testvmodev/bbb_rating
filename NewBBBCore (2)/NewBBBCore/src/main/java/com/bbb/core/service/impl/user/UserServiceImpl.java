package com.bbb.core.service.impl.user;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.user.UserManager;
import com.bbb.core.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserManager manager;

    @Override
    public Object updateStatusActive(String userId, boolean isActive) throws ExceptionResponse {
        return manager.updateStatusActive(userId, isActive);
    }

    @Override
    public Object deletedAccount(String userId) throws ExceptionResponse {
        return manager.deletedAccount(userId);
    }

    @Override
    public Object verifySafe(String userId) throws ExceptionResponse {
        return manager.verifySafe(userId);
    }

    @Override
    public Object getUserBasicStats(String userId) throws ExceptionResponse {
        return manager.getUserBasicStats(userId);
    }
}
