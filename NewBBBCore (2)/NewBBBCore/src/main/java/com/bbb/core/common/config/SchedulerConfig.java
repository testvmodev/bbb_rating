package com.bbb.core.common.config;

import com.bbb.core.common.Constants;
import com.bbb.core.manager.schedule.handler.JobFailHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@ConfigurationProperties(prefix = Constants.CONFIG_PREFIX + ".scheduler")
public class SchedulerConfig {
    @Autowired
    private JobFailHandler jobFailHandler;
    private ThreadPoolTaskScheduler jobThreadPool;
    private Boolean enable;
    private Boolean cleanUnused;
    private Integer firstRunDelay;
    private Integer threadSize = 5;

    public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
        if (jobThreadPool == null) {
            jobThreadPool = new ThreadPoolTaskScheduler();
            jobThreadPool.setPoolSize(threadSize);
            jobThreadPool.setThreadNamePrefix("JobsThreadPool");
            jobThreadPool.setErrorHandler(jobFailHandler);
            jobThreadPool.initialize();
        }
        return jobThreadPool;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getCleanUnused() {
        return cleanUnused;
    }

    public void setCleanUnused(Boolean cleanUnused) {
        this.cleanUnused = cleanUnused;
    }

    public Integer getFirstRunDelay() {
        return firstRunDelay;
    }

    public void setFirstRunDelay(Integer firstRunDelay) {
        this.firstRunDelay = firstRunDelay;
    }

    public Integer getThreadSize() {
        return threadSize;
    }

    public void setThreadSize(Integer threadSize) {
        this.threadSize = threadSize;
    }
}
