package com.bbb.core.repository.market.out;

import com.bbb.core.model.response.market.MarketPlaceConfigDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MarketPlaceConfigResponseRepository extends JpaRepository<MarketPlaceConfigDetailResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT market_place.name AS market_place_name, market_place_config.* FROM market_place_config " +
            "JOIN market_place ON market_place.id = market_place_config.market_place_id " +
            "WHERE market_place_config.is_delete = 0")
    List<MarketPlaceConfigDetailResponse> findAll();

    @Query(nativeQuery = true, value = "SELECT market_place.name AS market_place_name, market_place_config.* FROM market_place_config " +
            "JOIN market_place ON market_place.id = market_place_config.market_place_id " +
            "WHERE market_place_config.id = ?1 AND market_place_config.is_delete = 0")
    MarketPlaceConfigDetailResponse findOne(Long id);
}
