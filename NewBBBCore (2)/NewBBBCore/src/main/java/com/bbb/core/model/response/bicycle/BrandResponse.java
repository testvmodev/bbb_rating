package com.bbb.core.model.response.bicycle;

import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class BrandResponse {
    @Id
    private long id;
    private String name;
    private String brandLogo;
    private Float valueModifier;
    private LocalDateTime lastUpdate;
    private boolean isApproved;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public Float getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(Float valueModifier) {
        this.valueModifier = valueModifier;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
