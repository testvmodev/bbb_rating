package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.MarketListingFilterTypeTable;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MarketListingFilterTypeTableConverter implements AttributeConverter<MarketListingFilterTypeTable, String> {
    @Override
    public String convertToDatabaseColumn(MarketListingFilterTypeTable marketListingFilterTypeTable) {
        if (marketListingFilterTypeTable == null) {
            return null;
        }
        return marketListingFilterTypeTable.getValue();
    }

    @Override
    public MarketListingFilterTypeTable convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        for (MarketListingFilterTypeTable value : MarketListingFilterTypeTable.values()) {
            if (value.getValue().equals(s)) {
                return value;
            }
        }
        return null;
    }
}
