package com.bbb.core.model.response.user;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class UserBasicStatsResponse {
    @Id
    private String id;
    private Long createdMarketListings;
    private Long boughtMarketListings;
    private Long createdOffers;
    private Long createdBids;
    private Long wonBids;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreatedMarketListings() {
        return createdMarketListings;
    }

    public void setCreatedMarketListings(Long createdMarketListings) {
        this.createdMarketListings = createdMarketListings;
    }

    public Long getBoughtMarketListings() {
        return boughtMarketListings;
    }

    public void setBoughtMarketListings(Long boughtMarketListings) {
        this.boughtMarketListings = boughtMarketListings;
    }

    public Long getCreatedOffers() {
        return createdOffers;
    }

    public void setCreatedOffers(Long createdOffers) {
        this.createdOffers = createdOffers;
    }

    public Long getCreatedBids() {
        return createdBids;
    }

    public void setCreatedBids(Long createdBids) {
        this.createdBids = createdBids;
    }

    public Long getWonBids() {
        return wonBids;
    }

    public void setWonBids(Long wonBids) {
        this.wonBids = wonBids;
    }
}
