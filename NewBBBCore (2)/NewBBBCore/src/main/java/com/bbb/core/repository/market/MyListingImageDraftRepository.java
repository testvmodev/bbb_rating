package com.bbb.core.repository.market;

import com.bbb.core.model.database.MyListingImageDraft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MyListingImageDraftRepository extends JpaRepository<MyListingImageDraft, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM my_listing_image_draft " +
            "WHERE listing_draft_id = :myListingDraftId")
    List<MyListingImageDraft> findImages(
            @Param("myListingDraftId") long myListingDraftId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM my_listing_image_draft " +
            "WHERE id IN :imageDraftIds")
    List<MyListingImageDraft> findAll(
            @Param("imageDraftIds") List<Long> imageDraftIds
    );
}
