package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.bicycle.ModelFamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ModelFamilyController {
    @Autowired
    private ModelFamilyService service;

    @GetMapping(value = Constants.URL_MODEL_FAMILIES)
    public ResponseEntity getModelFamilies(
            @RequestParam(value = "brandId") long brandId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getModelFamilies(brandId), HttpStatus.OK);
    }
}
