package com.bbb.core.service.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingShipmentCreateRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingUpdateRequest;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MyListingService {
//    Object createMyListingPTP(PersonalListingRequest request) throws ExceptionResponse, MethodArgumentNotValidException;

    Object getAllMyListings(@RequestParam("sortField") MyListingSortField sortField,
                            @RequestParam("sortType") Sort sortType,
                            Pageable pageable
    ) throws ExceptionResponse;

    Object getMyListingDetail(long marketListingId) throws ExceptionResponse;

    Object updateMyListing(
            long marketListingId,
            PersonalListingUpdateRequest request
    ) throws ExceptionResponse;

    Object deleteMyListing(long marketListingId) throws ExceptionResponse;

    Object postImageMarketListingPTP(long marketListingId, List<MultipartFile> images) throws ExceptionResponse;

    Object addImageImageMarketListingPTP(
            long marketListingId, List<MultipartFile> images
    ) throws ExceptionResponse;

    Object deleteImageMarketListingPTP(
            long marketListingId, List<Long> inventoryImageIds
    ) throws ExceptionResponse;

    Object getDraftDetail(long myListingDraftId) throws ExceptionResponse;

    Object createDraftMyListing(
            PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException;

    Object updateDraftMyListing(
            long draftId, PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException;

    Object deleteDraftMyListing(long draftId) throws ExceptionResponse;

    Object postImageDraftMyListing(long listingDraftId, List<MultipartFile> images) throws ExceptionResponse;

    Object deleteImageDraftMyListing(List<Long> imageDraftIds) throws ExceptionResponse;

    Object createListingFromDraft(long draftId) throws ExceptionResponse, MethodArgumentNotValidException, NoSuchMethodException;

    Object relistMyListing(long marketListingId) throws ExceptionResponse;

    Object createShipment(PersonalListingShipmentCreateRequest request) throws ExceptionResponse;
}
