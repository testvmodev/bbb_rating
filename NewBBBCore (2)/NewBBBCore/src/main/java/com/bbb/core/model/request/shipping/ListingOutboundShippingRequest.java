package com.bbb.core.model.request.shipping;

import javax.validation.constraints.NotNull;

public class ListingOutboundShippingRequest {
    @NotNull
    Long marketListingId;
    private String manualCarrier;
    private String manualTrackingNumber;
    private Boolean isScheduled;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getManualCarrier() {
        return manualCarrier;
    }

    public void setManualCarrier(String manualCarrier) {
        this.manualCarrier = manualCarrier;
    }

    public String getManualTrackingNumber() {
        return manualTrackingNumber;
    }

    public void setManualTrackingNumber(String manualTrackingNumber) {
        this.manualTrackingNumber = manualTrackingNumber;
    }

    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public boolean isValidManual() {
        return manualCarrier != null && manualTrackingNumber != null && (isScheduled == null || !isScheduled);
    }
}
