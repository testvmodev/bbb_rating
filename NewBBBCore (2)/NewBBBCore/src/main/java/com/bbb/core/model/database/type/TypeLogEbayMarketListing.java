package com.bbb.core.model.database.type;

public enum TypeLogEbayMarketListing {
    SINGLE("Single"),
    MULTI("Multi");
    private final String value;

    TypeLogEbayMarketListing(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
