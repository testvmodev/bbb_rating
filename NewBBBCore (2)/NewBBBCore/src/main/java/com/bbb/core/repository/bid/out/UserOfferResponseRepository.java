package com.bbb.core.repository.bid.out;

import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.sort.UserOfferSort;
import com.bbb.core.model.response.bid.offer.UserOfferResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserOfferResponseRepository extends JpaRepository<UserOfferResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id AS offer_id, " +
            "offer.buyer_id, " +
            "offer.offer_price, " +
            "offer.status AS offer_status, " +
            "offer.created_time, " +

            "market_listing.id AS market_listing_id, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS market_type, " +
            "market_listing.status AS status_market_listing, " +

            "inventory.id AS inventory_id, " +
            "inventory.image_default AS image_default, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.seller_id AS seller_id, " +

            "inventory.bicycle_id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +

            ":userId AS requested_user_id " +

            "FROM offer " +
            "JOIN market_listing ON offer.market_listing_id IS NOT NULL AND offer.market_listing_id = market_listing.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +

            "WHERE offer.is_delete = 0 " +
            "AND (offer.buyer_id = :userId " +
                "OR inventory.seller_id = :userId) " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.UserOfferSort).CREATED_DATE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()})" +
                "THEN offer.created_time END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.UserOfferSort).CREATED_DATE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()})" +
                "THEN offer.created_time END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY "
    )
    List<UserOfferResponse> findAllUserOffers(
            @Param("userId") String userId,
            @Param("sortField") UserOfferSort sortField,
            @Param("sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM offer " +
            "JOIN market_listing ON offer.market_listing_id IS NOT NULL AND offer.market_listing_id = market_listing.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +

            "WHERE offer.is_delete = 0 " +
            "AND (offer.buyer_id = :userId " +
                "OR inventory.seller_id = :userId) "
    )
    int countUserOffers(
            @Param("userId") String userId
    );
}
