package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class TaxTable {
    @JacksonXmlProperty(localName = "TaxJurisdiction")
    private TaxJurisdiction taxJurisdiction;

    public TaxJurisdiction getTaxJurisdiction() {
        return taxJurisdiction;
    }

    public void setTaxJurisdiction(TaxJurisdiction taxJurisdiction) {
        this.taxJurisdiction = taxJurisdiction;
    }
}
