package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.offer.SecretMarketListingAuctionResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SecretMarketListingAuctionResponseRepository extends JpaRepository<SecretMarketListingAuctionResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "ABS(CHECKSUM(NewId())) AS id, " +
            "market_listing.id AS market_listing_id, " +
            "NULL AS inventory_auction_id, " +
            "inventory.status AS status_inventory, " +
            "inventory.stage AS stage_inventory " +
            "FROM " +
            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "market_listing.id = :marketListingIds"
    )
    List<SecretMarketListingAuctionResponse> findAllMarketListing(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );


    @Query(nativeQuery = true, value = "SELECT " +
            "ABS(CHECKSUM(NewId())) AS id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "NULL AS market_listing_id, " +
            "inventory.status AS status_inventory, " +
            "inventory.stage AS stage_inventory " +
            "FROM " +
            "inventory_auction JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "WHERE " +
            "inventory_auction.id = :inventoryAuctionIds"
    )
    List<SecretMarketListingAuctionResponse> findAllInventoryAuction(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );
}
