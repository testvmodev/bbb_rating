package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryCompTypeSelect;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryCompTypeSelectRepository extends CrudRepository<InventoryCompTypeSelect, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type_select")
    List<InventoryCompTypeSelect> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type_select WHERE inventory_comp_type_select.inventory_comp_type_id IN :componentIds")
    List<InventoryCompTypeSelect> findAllByInventoryCompTypeIds(
            @Param(value = "componentIds") List<Long> componentIds
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM " +
            "(SELECT ( CONVERT(nvarchar(255), inventory_comp_type_select.inventory_comp_type_id) + '_' + inventory_comp_type_select.value) AS title FROM inventory_comp_type_select) AS comp " +
            "WHERE comp.title IN :typeAndValues")
    int getCountTypeValue(
            @Param(value = "typeAndValues") List<String> typeAndValues
    );

//    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type JOIN inventory_comp_type_select ")

    @Query(nativeQuery = true, value = "SELECT inventory_comp_type_select.* " +
            "FROM bicycle_size_category " +
            "JOIN bicycle_size_category_value ON bicycle_size_category.id = bicycle_size_category_value.size_category_id " +
            "JOIN inventory_comp_type_select ON bicycle_size_category_value.inventory_comp_size_value_id = inventory_comp_type_select.id " +
            "WHERE bicycle_size_category.display_name IN :#{@queryUtils.isEmptyList(#categoryNames) ? @queryUtils.fakeStrings() : #categoryNames} "
    )
    List<InventoryCompTypeSelect> findAllFromSizeCategory(
            @Param("categoryNames") List<String> categoryNames
    );
}
