package com.bbb.core.model.request.bicycle;

public class BicycleImportRequest {
    private String urlBicycle;
    private String urlImage;
    private String urlCompSpec;

    public String getUrlBicycle() {
        return urlBicycle;
    }

    public void setUrlBicycle(String urlBicycle) {
        this.urlBicycle = urlBicycle;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlCompSpec() {
        return urlCompSpec;
    }

    public void setUrlCompSpec(String urlCompSpec) {
        this.urlCompSpec = urlCompSpec;
    }
}
