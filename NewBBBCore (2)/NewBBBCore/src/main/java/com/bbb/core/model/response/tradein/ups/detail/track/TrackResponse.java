package com.bbb.core.model.response.tradein.ups.detail.track;

import com.bbb.core.model.response.tradein.ups.detail.Address;
import com.bbb.core.model.response.tradein.ups.detail.ResponseInfo;
import com.bbb.core.model.response.tradein.ups.detail.track.ShipmentTrack;
import com.bbb.core.model.response.tradein.ups.detail.type.TrackStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrackResponse {
    @JsonProperty("Response")
    private ResponseInfo responseInfo;
    @JsonProperty("Shipment")
    private ShipmentTrack shipmentTrack;

    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    public void setResponseInfo(ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    public boolean isDelivered() {
        try {
            return shipmentTrack.getPackageTrack().getActivities().get(0).getStatus().getTrackStatus() == TrackStatus.DELIVERED;
        } catch (Exception e) {
            return false;
        }
    }

    @JsonIgnore
    public Address getLastLocation() {
        try {
            return shipmentTrack.getPackageTrack().getActivities().get(0).getActivityLocation().getAddress();
        } catch (Exception e) {
            return null;
        }
    }
}
