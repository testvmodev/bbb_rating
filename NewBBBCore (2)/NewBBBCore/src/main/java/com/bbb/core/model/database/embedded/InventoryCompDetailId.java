package com.bbb.core.model.database.embedded;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class InventoryCompDetailId implements Serializable{
    private long inventoryId;
    private long inventoryCompTypeId;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getInventoryCompTypeId() {
        return inventoryCompTypeId;
    }

    public void setInventoryCompTypeId(long inventoryCompTypeId) {
        this.inventoryCompTypeId = inventoryCompTypeId;
    }

}
