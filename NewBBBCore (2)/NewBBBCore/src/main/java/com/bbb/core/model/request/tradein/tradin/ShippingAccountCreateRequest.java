package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.response.ObjectError;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class ShippingAccountCreateRequest {
    private CarrierType carrierType;
    private String username;
    private String password;
    private String apiKey;
    private String apiPassword;
    private String accountNumber;
    private String meterNumber;
    private String serviceCode;
    private String phone;
    private String warehouseId;
    private boolean isSandbox;

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public boolean isSandbox() {
        return isSandbox;
    }

    public void setSandbox(boolean sandbox) {
        isSandbox = sandbox;
    }

    public boolean isValid() throws ExceptionResponse {
        switch (carrierType) {
            case UPS:
                if (username == null || StringUtils.isBlank(username)) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.USERNAME_MUST_NOT_EMPTY),
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (password == null || StringUtils.isBlank(password)) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.PASSWORD_MUST_NOT_EMPTY),
                            HttpStatus.BAD_REQUEST
                    );
                }
                return true;
            case FEDEX:
                if (apiPassword == null || StringUtils.isBlank(apiPassword)) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.API_PASSWORD_MUST_NOT_EMPTY),
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (meterNumber == null || StringUtils.isBlank(meterNumber)) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.METER_NUMBER_MUST_NOT_EMPTY),
                            HttpStatus.BAD_REQUEST
                    );
                }
                return true;
            default:
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.CARRIER_NOT_SUPPORT),
                        HttpStatus.BAD_REQUEST
                );
        }
    }
}
