package com.bbb.core.controller.inventory;

import com.bbb.core.common.Constants;
import com.bbb.core.service.inventory.InventoryTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryTypeController {
    @Autowired
    private InventoryTypeService service;

    @GetMapping(value = Constants.URL_GET_INVENTORY_TYPE)
    public ResponseEntity getAllInventoryType() {
        return new ResponseEntity<>(service.getAllInventoryType(),
                HttpStatus.OK);
    }
}
