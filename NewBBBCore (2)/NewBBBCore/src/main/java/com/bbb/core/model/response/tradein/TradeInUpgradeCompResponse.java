package com.bbb.core.model.response.tradein;

import com.bbb.core.model.database.TradeInUpgradeComp;

public class TradeInUpgradeCompResponse extends TradeInUpgradeComp {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
