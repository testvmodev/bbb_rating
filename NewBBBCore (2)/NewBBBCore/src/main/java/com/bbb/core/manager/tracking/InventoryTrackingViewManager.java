package com.bbb.core.manager.tracking;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.InventoryTrackingView;
import com.bbb.core.model.response.ContentDoubleNumber;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.reout.ContentDoubleNumberRepository;
import com.bbb.core.repository.tracking.InventoryTrackingViewRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class InventoryTrackingViewManager {
    private ExecutorService exSave;
    @Autowired
    private InventoryTrackingViewRepository inventoryTrackingViewRepository;
    @Autowired
    private ContentDoubleNumberRepository contentDoubleNumberRepository;
    @Autowired
    private InventoryRepository inventoryRepository;

    @PostConstruct
    public void initConstructor() {
        exSave = Executors.newFixedThreadPool(4);
    }

    public InventoryTrackingView saveTracking(String userId, long inventoryId) {
        InventoryTrackingView inventoryTrackingView =
                inventoryTrackingViewRepository.findOne(userId, inventoryId);
        if (inventoryTrackingView != null) {
            inventoryTrackingView.setLastUpdate(LocalDateTime.now());
            inventoryTrackingView.setCountView(inventoryTrackingView.getCountView() + 1);
            return inventoryTrackingViewRepository.save(inventoryTrackingView);
        } else {
            inventoryTrackingView = new InventoryTrackingView();
            inventoryTrackingView.setUserId(userId);
            inventoryTrackingView.setInventoryId(inventoryId);
            inventoryTrackingView.setCountView(1);
            inventoryTrackingView = inventoryTrackingViewRepository.save(inventoryTrackingView);
            List<InventoryTrackingView> inventoryTrackingViews = inventoryTrackingViewRepository.findAllRemove(userId,
                    Constants.LIMIT_SAVE_TRACKING_VIEW_USER + 1, 990);
            if (inventoryTrackingViews.size() > 0) {
                inventoryTrackingViewRepository.deleteAll(inventoryTrackingViews);
            }
            return inventoryTrackingView;
        }

    }

    public void saveTrackingAsy(String userId, long inventoryId) {
        Observable.create((ObservableOnSubscribe<InventoryTrackingView>) ter -> {
            ter.onNext(saveTracking(userId, inventoryId));
            ter.onComplete();
        }).subscribeOn(Schedulers.from(exSave))
                .subscribe();
    }

    public Object getBestViews(Pageable page) throws ExceptionResponse {
        ListObjResponse<ContentDoubleNumber> response = new ListObjResponse<>();
        response.setData(
                contentDoubleNumberRepository.findAllInventoryTrackingView(page.getOffset(), page.getPageSize())
        );
        response.setTotalItem(contentDoubleNumberRepository.getNumberInventoryTrackingView());
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    public Object getRecentTrackingView(Pageable page) {
        String userId = CommonUtils.getUserLogin();
        List<InventoryTrackingView> inventoryTrackingViews =
                inventoryTrackingViewRepository.findAllInventoryTrackingView(userId,
                        page.getOffset(), page.getPageSize());
        List<Long> inventoryIds = inventoryTrackingViews.stream().map(InventoryTrackingView::getInventoryId).collect(Collectors.toList());
        if (inventoryIds.size() == 0) {
            return new ArrayList<>();
        }
        List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIds);
        List<Inventory> newListInventories = new ArrayList<>();
        for (Long inventoryId : inventoryIds) {
            for (Inventory inventory : inventories) {
                if (inventoryId.equals(inventory.getId())) {
                    newListInventories.add(inventory);
                    break;
                }
            }
        }

        ListObjResponse<Inventory> response = new ListObjResponse<>();
        response.setData(newListInventories);
        response.setTotalItem(
                contentDoubleNumberRepository.getNumberRecentView(userId)
        );
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;

    }
}
