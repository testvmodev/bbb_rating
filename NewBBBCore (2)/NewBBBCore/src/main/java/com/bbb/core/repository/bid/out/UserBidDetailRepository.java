package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.offer.offerbid.UserBidDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserBidDetailRepository extends JpaRepository<UserBidDetail, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id AS offer_id, " +
            "offer.buyer_id AS buyer_id, " +
            "offer.offer_price AS offer_price, " +
            "offer.status AS offer_status, " +
            "offer.created_time AS created_time, " +
            "offer.last_update AS last_update, " +
            "offer.inventory_auction_id AS inventory_auction_id " +

            "FROM offer " +
            "WHERE buyer_id = :userId " +
            "AND offer.inventory_auction_id IS NOT NULL " +
            "AND offer.inventory_auction_id IN :inventoryAuctionIds"
    )
    List<UserBidDetail> findAll(
            @Param("userId") String userId,
            @Param("inventoryAuctionIds") List<Long> inventoryAuctionIds
    );
}
