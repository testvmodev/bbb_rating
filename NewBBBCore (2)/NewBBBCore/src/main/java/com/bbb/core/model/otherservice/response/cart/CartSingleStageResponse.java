package com.bbb.core.model.otherservice.response.cart;

public class CartSingleStageResponse {
    private String stage;

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
