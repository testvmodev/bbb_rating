package com.bbb.core.model.request.log;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RawHeaders {
    private String ip;
    @JsonProperty("user_agent")
    private String userAgent;

    public RawHeaders(String ip, String userAgent) {
        this.ip = ip;
        this.userAgent = userAgent;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
