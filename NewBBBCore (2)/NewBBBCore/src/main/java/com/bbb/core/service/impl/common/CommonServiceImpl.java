package com.bbb.core.service.impl.common;

import com.bbb.core.manager.common.CommonManager;
import com.bbb.core.model.request.common.CommonComponentType;
import com.bbb.core.service.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonServiceImpl implements CommonService {
    @Autowired
    private CommonManager manager;
    @Override
    public Object getCommonComponents(List<CommonComponentType> components) {
        return manager.getCommonComponents(components);
    }
}
