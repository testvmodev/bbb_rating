package com.bbb.core.repository.reout;

import com.bbb.core.model.response.ContentDoubleLong;
import com.bbb.core.model.response.ContentDoubleLongId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentDoubleLongRepository extends JpaRepository<ContentDoubleLong, ContentDoubleLongId> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.inventory_id AS first_id, " +
            "market_listing.market_place_config_id AS second_id " +
            "FROM market_listing " +
            "WHERE market_listing.inventory_id IN :inventoriesId " +
            "AND market_listing.is_delete = 0 AND market_listing.status = 'Listed'"
    )
    List<ContentDoubleLong> findAll(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );
}
