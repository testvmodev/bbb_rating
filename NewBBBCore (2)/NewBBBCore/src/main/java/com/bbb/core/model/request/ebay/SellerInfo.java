package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellerInfo {
    @JacksonXmlProperty(localName = "AllowPaymentEdit")
    private Boolean allowPaymentEdit;
    @JacksonXmlProperty(localName = "CheckoutEnabled")
    private Boolean checkoutEnabled;
    @JacksonXmlProperty(localName = "CIPBankAccountStored")
    private Boolean cIPBankAccountStored;
    @JacksonXmlProperty(localName = "GoodStanding")
    private Boolean goodStanding;
    @JacksonXmlProperty(localName = "LiveAuctionAuthorized")
    private Boolean liveAuctionAuthorized;
    @JacksonXmlProperty(localName = "MerchandizingPref")
    private String merchandizingPref;
    @JacksonXmlProperty(localName = "QualifiesForB2BVAT")
    private String qualifiesForB2BVAT;
    @JacksonXmlProperty(localName = "StoreOwner")
    private Boolean storeOwner;
    @JacksonXmlProperty(localName = "SafePaymentExempt")
    private Boolean safePaymentExempt;

    public Boolean getAllowPaymentEdit() {
        return allowPaymentEdit;
    }

    public void setAllowPaymentEdit(Boolean allowPaymentEdit) {
        this.allowPaymentEdit = allowPaymentEdit;
    }

    public Boolean getCheckoutEnabled() {
        return checkoutEnabled;
    }

    public void setCheckoutEnabled(Boolean checkoutEnabled) {
        this.checkoutEnabled = checkoutEnabled;
    }

    public Boolean getcIPBankAccountStored() {
        return cIPBankAccountStored;
    }

    public void setcIPBankAccountStored(Boolean cIPBankAccountStored) {
        this.cIPBankAccountStored = cIPBankAccountStored;
    }

    public Boolean getGoodStanding() {
        return goodStanding;
    }

    public void setGoodStanding(Boolean goodStanding) {
        this.goodStanding = goodStanding;
    }

    public Boolean getLiveAuctionAuthorized() {
        return liveAuctionAuthorized;
    }

    public void setLiveAuctionAuthorized(Boolean liveAuctionAuthorized) {
        this.liveAuctionAuthorized = liveAuctionAuthorized;
    }

    public String getMerchandizingPref() {
        return merchandizingPref;
    }

    public void setMerchandizingPref(String merchandizingPref) {
        this.merchandizingPref = merchandizingPref;
    }

    public String getQualifiesForB2BVAT() {
        return qualifiesForB2BVAT;
    }

    public void setQualifiesForB2BVAT(String qualifiesForB2BVAT) {
        this.qualifiesForB2BVAT = qualifiesForB2BVAT;
    }

    public Boolean getStoreOwner() {
        return storeOwner;
    }

    public void setStoreOwner(Boolean storeOwner) {
        this.storeOwner = storeOwner;
    }

    public Boolean getSafePaymentExempt() {
        return safePaymentExempt;
    }

    public void setSafePaymentExempt(Boolean safePaymentExempt) {
        this.safePaymentExempt = safePaymentExempt;
    }
}
