package com.bbb.core.repository.schedule;

import com.bbb.core.model.database.ScheduleJobLog;
import org.joda.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

public interface ScheduleJobLogRepository extends JpaRepository<ScheduleJobLog, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM schedule_job_log " +
            "WHERE (:jobId IS NULL OR :jobId = job_id)" +
            "ORDER BY id OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY")
    List<ScheduleJobLog> findAll(
            @Param("jobId") Long jobId,
            @Param("offset") long offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM schedule_job_log " +
            "WHERE (:jobId IS NULL OR :jobId = job_id)")
    int countAll(@Param("jobId") Long jobId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(nativeQuery = true, value = "DELETE FROM schedule_job_log " +
            "WHERE (:jobId IS NULL OR :jobId = job_id)" +
            "AND start_time <= :olderThan"
    )
    int purgeLogs(
            @Param("jobId") Long jobId,
            @Param("olderThan") Timestamp olderThan
    );
}
