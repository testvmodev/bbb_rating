package com.bbb.core.model.response.model.family;

import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.model.ModelFromBrandYearResponse;

import java.util.List;

public class ModelFamilyAndYearResponse {
    private List<ModelFromBrandYearResponse> models;
    private List<ContentCommonId> years;

    public List<ModelFromBrandYearResponse> getModels() {
        return models;
    }

    public void setModels(List<ModelFromBrandYearResponse> models) {
        this.models = models;
    }

    public List<ContentCommonId> getYears() {
        return years;
    }

    public void setYears(List<ContentCommonId> years) {
        this.years = years;
    }
}
