package com.bbb.core.model.database;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bicycle")

public class Bicycle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "brand_id")
    private Long brandId;
    @Column(name = "model_id")
    private Long modelId;
    @Column(name = "type_id")
    private Long typeId;
    @Column(name = "year_id")
    private Long yearId;
    @Column(name = "retail_price")
    private Float retailPrice;
    @Column(name = "description")
    private String description;
    @Column(name = "last_update")
    private LocalDateTime lastUpdate;
    @Column(name = "image_default")
    private String imageDefault;
    @Column(name = "image_id")
    private Long imageId;
    @Column(name = "id_saleforce")
    private String idSaleforce;
    @Column(name = "is_delete")
    private boolean isDelete;
    @Column(name = "active")
    private boolean active;
    @Column(name = "name")
    private String name;
    @Column(name = "brake_type_id")
    private Long brakeTypeId;
    @Column(name = "size_id")
    private Long sizeId;
    @Column(name = "tracking_count")
    private long trackingCount;
    private Boolean isOversized;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public Float getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Float retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    @JsonIgnore
    public String getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(String idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    @JsonIgnore
    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public Long getSizeId() {
        return sizeId;
    }

    public void setSizeId(Long sizeId) {
        this.sizeId = sizeId;
    }

    public long getTrackingCount() {
        return trackingCount;
    }

    public void setTrackingCount(long trackingCount) {
        this.trackingCount = trackingCount;
    }

    public Boolean getOversized() {
        return isOversized;
    }

    public void setOversized(Boolean oversized) {
        isOversized = oversized;
    }
}
