package com.bbb.core.model.database.type;

public enum MarketType {
    EBAY("Ebay"),
    POS("Pos"),
    BBB("BBB"),
    WHOLE_SALLER("Wholesaler");

    private final String value;

    MarketType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
