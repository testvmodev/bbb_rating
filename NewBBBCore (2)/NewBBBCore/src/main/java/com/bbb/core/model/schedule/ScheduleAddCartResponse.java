package com.bbb.core.model.schedule;

import com.bbb.core.model.otherservice.response.ErrorRestResponse;
import com.bbb.core.model.otherservice.response.cart.CartResponse;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;

public class ScheduleAddCartResponse {
    private CartResponse cartAddResponse;
    private SendingMailResponse sendingMailResponse;
    private ErrorRestResponse cartErrorResponse;
    private ErrorRestResponse mailErrorResponse;
    private long offerId;

    public CartResponse getCartAddResponse() {
        return cartAddResponse;
    }

    public void setCartAddResponse(CartResponse cartAddResponse) {
        this.cartAddResponse = cartAddResponse;
    }

    public SendingMailResponse getSendingMailResponse() {
        return sendingMailResponse;
    }

    public void setSendingMailResponse(SendingMailResponse sendingMailResponse) {
        this.sendingMailResponse = sendingMailResponse;
    }

    public ErrorRestResponse getCartErrorResponse() {
        return cartErrorResponse;
    }

    public void setCartErrorResponse(ErrorRestResponse cartErrorResponse) {
        this.cartErrorResponse = cartErrorResponse;
    }

    public ErrorRestResponse getMailErrorResponse() {
        return mailErrorResponse;
    }

    public void setMailErrorResponse(ErrorRestResponse mailErrorResponse) {
        this.mailErrorResponse = mailErrorResponse;
    }

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }
}
