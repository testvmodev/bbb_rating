package com.bbb.core.model.database;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "market_place_config")
public class MarketPlaceConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long marketPlaceId;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private boolean isDelete;
    private String name;
    private String ebayAppId;
    private String ebayCertId;
    private String ebayDevId;
    private String ebayToken;
    private boolean isApiSandbox;
    private String sessionEbay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEbayAppId() {
        return ebayAppId;
    }

    public void setEbayAppId(String ebayAppId) {
        this.ebayAppId = ebayAppId;
    }

    public String getEbayCertId() {
        return ebayCertId;
    }

    public void setEbayCertId(String ebayCertId) {
        this.ebayCertId = ebayCertId;
    }

    public String getEbayDevId() {
        return ebayDevId;
    }

    public void setEbayDevId(String ebayDevId) {
        this.ebayDevId = ebayDevId;
    }

    public String getEbayToken() {
        return ebayToken;
    }

    public void setEbayToken(String ebayToken) {
        this.ebayToken = ebayToken;
    }

    public boolean isEbayApiSandbox() {
        return isApiSandbox;
    }

    public void setEbayApiSandbox(boolean ebayApiSandbox) {
        isApiSandbox = ebayApiSandbox;
    }

    public boolean isApiSandbox() {
        return isApiSandbox;
    }

    public void setApiSandbox(boolean apiSandbox) {
        isApiSandbox = apiSandbox;
    }

    public String getSessionEbay() {
        return sessionEbay;
    }

    public void setSessionEbay(String sessionEbay) {
        this.sessionEbay = sessionEbay;
    }
}
