package com.bbb.core.repository.reout;

import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ContentCommon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface ContentCommonRepository extends JpaRepository<ContentCommon, Long> {
    //search model
    @Query(nativeQuery = true, value = "SELECT bicycle_model.id, bicycle_model.name FROM bicycle_model ORDER BY bicycle_model.name ASC OFFSET ?1 ROWS FETCH NEXT ?2 ROWS ONLY")
    List<ContentCommon> findAllBicycleModel(int offset, int limit);

    @Query(nativeQuery = true, value = "SELECT bicycle_model.id, bicycle_model.name FROM bicycle_model WHERE bicycle_model.brand_id = ?1 ORDER BY bicycle_model.name ASC OFFSET ?2 ROWS FETCH NEXT ?3 ROWS ONLY")
    List<ContentCommon> findAllBicycleModelByBrandId(long brandId, int offset, int limit);

    @Query(nativeQuery = true, value = "SELECT bicycle_model.id, bicycle_model.name FROM bicycle_model WHERE bicycle_model.name LIKE ?1 ORDER BY bicycle_model.name ASC OFFSET ?2 ROWS FETCH NEXT ?3 ROWS ONLY")
    List<ContentCommon> findAllBicycleModelByName(String name, int offset, int limit);

    @Query(nativeQuery = true, value = "SELECT bicycle_model.id, bicycle_model.name FROM bicycle_model WHERE bicycle_model.brand_id = ?1 AND bicycle_model.name LIKE ?2 ORDER BY bicycle_model.name ASC OFFSET ?3 ROWS FETCH NEXT ?4 ROWS ONLY")
    List<ContentCommon> findAllBicycleModelByBrandIdAndName(long brandId, String name, int offset, int limit);


    @Query(nativeQuery = true, value = "SELECT bicycle_model.id as id, bicycle_model.name AS name " +
            "FROM bicycle_model " +
            "WHERE " +
            "(:isBrandIdNull = 1 OR bicycle_model.brand_id IN :brandIds) AND " +
            "bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1")
    List<ContentCommon> findAllBicycleModel(
            @Param(value = "isBrandIdNull") boolean isBrandIdNull,
            @Param(value = "brandIds") List<Long> brandIds
    );



    @Query(nativeQuery = true, value = "SELECT bicycle_brand.id AS id, bicycle_brand.name AS name FROM bicycle_brand WHERE bicycle_brand.is_approved = 1 AND bicycle_brand.is_delete = 0")
    List<ContentCommon> findAllBicycleBrand();

    //bicyle model
    @Query(nativeQuery = true, value = "SELECT bicycle_model.id as id, bicycle_model.name AS name " +
            "FROM bicycle_model " +
            "WHERE " +
            "bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1")
    List<ContentCommon> findAllBicycleModel();

    //bicyle model
    @Query(nativeQuery = true, value = "SELECT bicycle_size.id as id, bicycle_size.name AS name " +
            "FROM bicycle_size ORDER BY bicycle_size.sort_order ASC"
    )
    List<ContentCommon> findAllSizeBicycle();

    @Query(nativeQuery = true, value = "SELECT inventory_comp_type_select.id as id, inventory_comp_type_select.value AS name " +
            "FROM inventory_comp_type JOIN inventory_comp_type_select ON inventory_comp_type.id = inventory_comp_type_select.inventory_comp_type_id " +
            "WHERE " +
            "inventory_comp_type.name = :sizeName " +
            "ORDER BY inventory_comp_type_select.order_sort ASC")
    List<ContentCommon> findAllInvComp(
            @Param(value = "sizeName") String sizeName
    );


    //support inventory
    @Query(nativeQuery = true, value = "SELECT bicycle_brand.id AS id, bicycle_brand.name AS name, bicycle_brand.name AS name FROM " +
            "bicycle JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "WHERE bicycle.id = :bicycleId")
    ContentCommon findBicycleBrand(@Param(value = "bicycleId") long bicycleId);

    @Query(nativeQuery = true, value = "SELECT bicycle_model.id AS id, bicycle_model.name AS name FROM " +
            "bicycle JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "WHERE bicycle.id = :bicycleId")
    ContentCommon findBicycleModel(@Param(value = "bicycleId") long bicycleId);

    @Query(nativeQuery = true, value = "SELECT bicycle_year.id AS id, bicycle_year.name AS name FROM " +
            "bicycle JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "WHERE bicycle.id = :bicycleId")
    ContentCommon findBicycleYear(@Param(value = "bicycleId") long bicycleId);

    @Query(nativeQuery = true, value = "SELECT bicycle_type.id AS id, bicycle_type.name AS name FROM " +
            "bicycle JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "WHERE bicycle.id = :bicycleId")
    ContentCommon findBicycleType(@Param(value = "bicycleId") long bicycleId);

    @Query(nativeQuery = true, value = "SELECT inventory_image.id as id, inventory_image.image as name FROM inventory_image where inventory_image.inventory_id = :inventoryId")
    List<ContentCommon> findImagesInventory(@Param(value = "inventoryId") long inventoryId);

    //bicycle type
    @Query(nativeQuery = true, value = "SELECT bicycle_type.id AS id, bicycle_type.name as name FROM bicycle_type WHERE bicycle_type.is_delete = 0")
    List<ContentCommon> findAllBicycleType();

    //inventory size
    @Query(nativeQuery = true, value = "SELECT bicycle_size.id AS id, bicycle_size.name as name FROM bicycle_size")
    List<ContentCommon> findAllInventorySize();

    //inventory year
    @Query(nativeQuery = true, value = "SELECT bicycle_year.id AS id, bicycle_year.name AS name FROM bicycle_year WHERE bicycle_year.is_delete = 0 AND bicycle_year.is_approved = 1 ORDER BY bicycle_year.id ASC")
    List<ContentCommon> findAllBicycleYear();

    //brake type
    @Query(nativeQuery = true, value = "SELECT brake_type.id AS id, brake_type.name AS name FROM brake_type")
    List<ContentCommon> findAllBrakeType();

    //frame material
    @Query(nativeQuery = true, value = "SELECT inventory_frame_material.id AS id, inventory_frame_material.name AS name FROM inventory_frame_material")
    List<ContentCommon> findAllInventoryMaterial();

    //image inventory
    @Query(nativeQuery = true, value = "SELECT inventory_image.id as id, inventory_image.image as name FROM inventory_image WHERE inventory_image.inventory_id = :inventoryId")
    List<ContentCommon> findAllImageInventory(@Param(value = "inventoryId") long inventoryId);


    //upgrade component
    @Query(nativeQuery = true, value = "SELECT trade_in_upgrade_comp.id AS id, trade_in_upgrade_comp.name AS name FROM " +
            "trade_in_upgrade_comp_detail " +
            "JOIN trade_in_upgrade_comp ON trade_in_upgrade_comp_detail.trade_in_upgrade_comp_id = trade_in_upgrade_comp.id " +
            "WHERE trade_in_upgrade_comp_detail.trade_in_id = :tradeInId"
    )
    List<ContentCommon> findAllTradeInUpgradeComponent(
            @Param(value = "tradeInId") long tradeInId
    );

    //model from bicycle
    @Query(nativeQuery = true, value = "SELECT bicycle_model.id AS id, bicycle_model.name AS name FROM " +
            "(SELECT DISTINCT bicycle.model_id AS model_id FROM bicycle " +
            "WHERE bicycle.brand_id = :brandId AND bicycle.active = 1 AND bicycle.is_delete = 0) AS model " +
            "JOIN bicycle_model ON model.model_id = bicycle_model.id " +
            "WHERE bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1 " +
            "ORDER BY bicycle_model.name ASC"
    )
    List<ContentCommon> findAllModeBrandFromBicycle(
            @Param(value = "brandId") long brandId
    );


    //get model from brand and year
    @Query(nativeQuery = true, value = "SELECT bicycle_model.id AS id, bicycle_model.name AS name FROM " +
            "(SELECT DISTINCT bicycle.model_id AS model_id FROM bicycle WHERE " +
            "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
            "(:yearId IS NULL OR bicycle.year_id = :yearId) AND " +
            "bicycle.active = 1 AND bicycle.is_delete = 0) AS model " +
            "JOIN bicycle_model ON model.model_id = bicycle_model.id"
    )
    List<ContentCommon> findAllModelFromBrandYear(
            @Param(value = "brandId") Long brandId,
            @Param(value = "yearId") Long yearId
    );

    //get year
    //get year
    @Query(nativeQuery = true, value = "SELECT bicycle_year.id AS id, bicycle_year.name AS name " +
            "FROM " +
            "(SELECT DISTINCT bicycle.year_id AS year_id " +
            "FROM bicycle WHERE bicycle.is_delete = 0 AND bicycle.active =1 ) AS year " +
            "JOIN bicycle_year ON year.year_id = bicycle_year.id " +
            "ORDER BY bicycle_year.id ASC"
    )
    List<ContentCommon> findAllYearBicycle();

    @Query(nativeQuery = true, value = "SELECT bicycle_year.id AS id, bicycle_year.name AS name FROM " +
            "(SELECT DISTINCT bicycle.year_id AS year_id FROM bicycle WHERE " +
            "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
            "(:modelId IS NULL OR bicycle.model_id = :modelId) AND " +
            "bicycle.active = 1 AND bicycle.is_delete = 0) AS year " +
            "JOIN bicycle_year ON year.year_id = bicycle_year.id"
    )
    List<ContentCommon> findAllYearFromBrandModel(
            @Param(value = "brandId") Long brandId,
            @Param(value = "modelId") Long modelId
    );
    //end

    @Query(nativeQuery = true, value = "SELECT bicycle_brand.id AS id, bicycle_brand.name AS name FROM " +
            "(SELECT DISTINCT bicycle.brand_id AS brand_id FROM bicycle WHERE bicycle.is_delete = 0 AND bicycle.active = 1) AS brand " +
            "JOIN bicycle_brand ON brand.brand_id = bicycle_brand.id " +
            "WHERE bicycle_brand.is_approved = 1 AND bicycle_brand.is_delete = 0 " +
            "ORDER BY bicycle_brand.name ASC"
    )
    List<ContentCommon> findAllBrandBicycle();

    @Query(nativeQuery = true, value = "SELECT ebay_catetory.id AS id, ebay_catetory.name AS name FROM ebay_catetory WHERE ebay_catetory.is_primary = 1 ORDER BY ebay_catetory.sort_primary ASC")
    List<ContentCommon> findAllEbayCategoriesPrimary();

    //bicycle iamge defaul
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_image.bicycle_id AS id, bicycle_image.uri AS name " +
            "FROM " +
            "bicycle_image JOIN " +
            "(SELECT MIN(bicycle_image.id) AS min_id " +
            "FROM bicycle_image " +
            "WHERE " +
            "bicycle_image.bicycle_id IN :bicycleIds " +
            "GROUP BY bicycle_image.bicycle_id) AS bicycle_min " +
            "ON bicycle_image.id = bicycle_min.min_id"
    )
    List<ContentCommon> findAllImageDefaultBicycle(
            @Param(value = "bicycleIds") List<Long> bicycleIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_model.id AS id," +
            "bicycle_model.name AS name " +
            "FROM bicycle_model " +
            "WHERE brand_id = :brandId")
    List<ContentCommon> findAllModelFromBrand(
            @Param(value = "brandId") long brandId
    );

    @Query(nativeQuery = true, value = "SELECT bicycle_type.id as id, bicycle_type.name as name FROM bicycle_type " +
            "WHERE " +
            "bicycle_type.name IN :names  AND " +
            "bicycle_type.is_delete = 0")
    List<ContentCommon> findBicycleTypeByNames(@Param(value = "names") List<String> names);

}

