package com.bbb.core.model.otherservice.response.partner;

import com.bbb.core.model.response.common.ContentDoubleString;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDateTime;

import java.util.List;

public class PartnerDetail {
    @JsonProperty(value = "staffs")
    private List<String> staffs;
    @JsonProperty(value = "partner_childs")
    private List<String> partnerChilds;
    @JsonProperty(value = "smart_tailing")
    private boolean isSmartTailing;
    @JsonProperty(value = "mailing_address")
    private boolean isMailingAddress;
    @JsonProperty(value = "widget_enable")
    private boolean isWidgetEnable;


    @JsonProperty(value = "_id")
    private String id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "address")
    private String address;
    @JsonProperty(value = "mailing_address")
    private String city;
    @JsonProperty(value = "state")
    private String state;
    @JsonProperty(value = "zip_code")
    private String zipCode;

    @JsonProperty(value = "lat")
    private Float lat;
    @JsonProperty(value = "lng")
    private Float lng;
    @JsonProperty(value = "phone")
    private String phone;
    @JsonProperty(value = "referal_code")
    private String referalCode;
    @JsonProperty(value = "website")
    private String website;
    @JsonProperty(value = "tell_about")
    private String tellAbout;
    @JsonProperty(value = "date_created")
    private String dateCreated;
    @JsonProperty(value = "date_updated")
    private String dateUpdated;

    public List<String> getStaffs() {
        return staffs;
    }

    public void setStaffs(List<String> staffs) {
        this.staffs = staffs;
    }

    public List<String> getPartnerChilds() {
        return partnerChilds;
    }

    public void setPartnerChilds(List<String> partnerChilds) {
        this.partnerChilds = partnerChilds;
    }

    public boolean isSmartTailing() {
        return isSmartTailing;
    }

    public void setSmartTailing(boolean smartTailing) {
        isSmartTailing = smartTailing;
    }

    public boolean isMailingAddress() {
        return isMailingAddress;
    }

    public void setMailingAddress(boolean mailingAddress) {
        isMailingAddress = mailingAddress;
    }

    public boolean isWidgetEnable() {
        return isWidgetEnable;
    }

    public void setWidgetEnable(boolean widgetEnable) {
        isWidgetEnable = widgetEnable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTellAbout() {
        return tellAbout;
    }

    public void setTellAbout(String tellAbout) {
        this.tellAbout = tellAbout;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
