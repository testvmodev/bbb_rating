package com.bbb.core.model.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inventory_wh_con")
public class InventoryWhCon {
    @Id
    private long inventoryId;
    private Boolean isIncorrectCondition;
    private Boolean isIncorrectModificaton;
    private Boolean isBoxDamaged;
    private Boolean isIncorrectBrandModeYear;
    private Boolean isBikeDamaged;
    private Boolean isNotClean;
    private Boolean isMissingImage;
    private Boolean isMissingCustomInfo;
    private Boolean isNeedNewPhoto;
    private Boolean isIncorrectBoxSize;
    private Boolean isIncorrectlyPacked;
    private Boolean isBrokenPart;
    private Boolean isNonCompliant;


    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Boolean getIncorrectCondition() {
        return isIncorrectCondition;
    }

    public void setIncorrectCondition(Boolean incorrectCondition) {
        isIncorrectCondition = incorrectCondition;
    }

    public Boolean getIncorrectModificaton() {
        return isIncorrectModificaton;
    }

    public void setIncorrectModificaton(Boolean incorrectModificaton) {
        isIncorrectModificaton = incorrectModificaton;
    }

    public Boolean getBoxDamaged() {
        return isBoxDamaged;
    }

    public void setBoxDamaged(Boolean boxDamaged) {
        isBoxDamaged = boxDamaged;
    }

    public Boolean getIncorrectBrandModeYear() {
        return isIncorrectBrandModeYear;
    }

    public void setIncorrectBrandModeYear(Boolean incorrectBrandModeYear) {
        isIncorrectBrandModeYear = incorrectBrandModeYear;
    }

    public Boolean getBikeDamaged() {
        return isBikeDamaged;
    }

    public void setBikeDamaged(Boolean bikeDamaged) {
        isBikeDamaged = bikeDamaged;
    }

    public Boolean getNotClean() {
        return isNotClean;
    }

    public void setNotClean(Boolean notClean) {
        isNotClean = notClean;
    }

    public Boolean getMissingImage() {
        return isMissingImage;
    }

    public void setMissingImage(Boolean missingImage) {
        isMissingImage = missingImage;
    }

    public Boolean getMissingCustomInfo() {
        return isMissingCustomInfo;
    }

    public void setMissingCustomInfo(Boolean missingCustomInfo) {
        isMissingCustomInfo = missingCustomInfo;
    }

    public Boolean getNeedNewPhoto() {
        return isNeedNewPhoto;
    }

    public void setNeedNewPhoto(Boolean needNewPhoto) {
        isNeedNewPhoto = needNewPhoto;
    }

    public Boolean getIncorrectBoxSize() {
        return isIncorrectBoxSize;
    }

    public void setIncorrectBoxSize(Boolean incorrectBoxSize) {
        isIncorrectBoxSize = incorrectBoxSize;
    }

    public Boolean getIncorrectlyPacked() {
        return isIncorrectlyPacked;
    }

    public void setIncorrectlyPacked(Boolean incorrectlyPacked) {
        isIncorrectlyPacked = incorrectlyPacked;
    }

    public Boolean getBrokenPart() {
        return isBrokenPart;
    }

    public void setBrokenPart(Boolean brokenPart) {
        isBrokenPart = brokenPart;
    }

    public Boolean getNonCompliant() {
        return isNonCompliant;
    }

    public void setNonCompliant(Boolean nonCompliant) {
        isNonCompliant = nonCompliant;
    }
}
