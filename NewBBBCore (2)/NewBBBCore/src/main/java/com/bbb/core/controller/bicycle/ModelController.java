package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.model.ModelUpdateRequest;
import com.bbb.core.model.request.sort.BicycleModelSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ModelService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ModelController {
    private final ModelService service;

    @Autowired
    public ModelController(ModelService service) {
        this.service = service;
    }

    @GetMapping(value = Constants.URL_GET_MODELS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "page"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "size"),
    })
    public ResponseEntity getModels(
            @RequestParam(value = "brandIds", required = false) List<Long> brandIds,
            @RequestParam(value = "nameSearch", required = false) String nameSearch,
            @RequestParam(value = "sortField", defaultValue = "NAME") BicycleModelSortField sortField,
            @RequestParam(value = "sortType", defaultValue = "ASC") Sort sortType,
            Pageable pageable) throws ExceptionResponse{
        return new ResponseEntity<>(service.getModels(brandIds, nameSearch,sortField, sortType, pageable),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_MODEL)
    public ResponseEntity getModelDetail(
            @PathVariable(value = Constants.ID) long modelId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getModelDetail(modelId),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_GET_MODEL_CREATE)
    public ResponseEntity createModel(
            @RequestBody ModelCreateRequest model
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.createModel(model),
                HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_MODEL)
    public ResponseEntity deleteModel(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteModel(id),
                HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_MODEL)
    public ResponseEntity updateBrand(
            @PathVariable(value = Constants.ID) long modelId,
            @RequestBody ModelUpdateRequest model
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateBrand(modelId,model),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_MODELS_FROM_BRAND_YEAR)
    public ResponseEntity getModelsFromBrandYear(
            @RequestParam(value = "brandId", required = false) Long brandId,
            @RequestParam(value = "yearId", required = false) Long yearId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getModelsFromBrandYear(brandId,yearId),
                HttpStatus.OK);
    }

    @GetMapping(Constants.URL_GET_MODELS_FROM_BRAND)
    public ResponseEntity getAllModelsFromBrand(
            @PathVariable(Constants.ID) long brandId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getAllModelsFromBrand(brandId));
    }
}
