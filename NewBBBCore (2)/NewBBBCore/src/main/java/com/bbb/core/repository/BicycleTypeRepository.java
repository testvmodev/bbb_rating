package com.bbb.core.repository;

import com.bbb.core.model.database.BicycleType;
import com.bbb.core.model.request.sort.BicycleTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BicycleTypeRepository extends CrudRepository<BicycleType, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM bicycle_type " +
            "WHERE id = ?1 AND is_delete = 0 ")
    BicycleType findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_type WHERE bicycle_type.is_delete = 0 ORDER BY bicycle_type.sort_order ASC ")
    List<BicycleType> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_type " +
            "WHERE " +
            "(:name IS NULL OR bicycle_type.name LIKE :name ) AND " +
            "bicycle_type.is_delete = 0 " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN bicycle_type.name END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN bicycle_type.name END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<BicycleType> findAllByName(
            @Param(value = "name") String name,
            @Param(value = "sortField") BicycleTypeSortField sortField,
            @Param(value = "sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM bicycle_type " +
            "WHERE " +
            "(:name IS NULL OR bicycle_type.name LIKE :name ) AND " +
            "bicycle_type.is_delete = 0 "
    )
    int countAll(@Param(value = "name") String name);

    @Query(nativeQuery = true,
            value = "SELECT COUNT(id) FROM bicycle_type WHERE bicycle_type.id = :typeId ")
    int getCount(
            @Param(value = "typeId") long typeId
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(bicycle_type.id) FROM bicycle_type WHERE bicycle_type.id IN :bicycleTypeIds AND bicycle_type.is_delete = 0")
    int countTypeBicycle(
            @Param(value = "bicycleTypeIds") List<Long> bicycleTypeIds
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle_type " +
            "WHERE " +
            "(:name IS NULL OR bicycle_type.name LIKE :name ) AND " +
            "bicycle_type.is_delete = 0 "
    )
    BicycleType findOneByName(@Param(value = "name") String name);

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM bicycle_type " +
            "WHERE name IN :names " +
            "AND bicycle_type.is_delete = 0"
    )
    BicycleType findOneByNames(
            @Param("names") List<String> names
    );
}
