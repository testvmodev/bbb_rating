package com.bbb.core.repository.inventory;

import com.bbb.core.model.response.inventory.InventoryResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryResponseRepository extends CrudRepository<InventoryResponse, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory.id AS id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.model_id  AS model_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.year_id AS year_id, " +
            "inventory_bicycle.type_id AS type_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, inventory.stage AS stage, " +
            "inventory.name AS name,  inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory_size.inventory_id AS size_id, " +
            "inventory_size.size_name AS size_name " +

            "FROM inventory " +


            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) " +
                "AS inventory_size " +
                "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) " +
                "AS brake_type " +
                "ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) " +
                "AS frame_material " +
                "ON inventory.id = frame_material.inventory_id " +

            //for sorting only
            "LEFT JOIN " +
            "(SELECT MIN(market_listing.start_time) AS time_start_listing, " +
            "MIN(market_listing.end_time) AS time_end_listing, " +
            "market_listing.inventory_id AS inventory_id " +
            "FROM market_listing " +
            "GROUP BY market_listing.inventory_id ) " +
            "AS inventory_market_listing " +
            "ON inventory.id = inventory_market_listing.inventory_id " +

            //main conditions
            "WHERE " +
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR bicycle_model.id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR bicycle_brand.id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory_type.id IN :typeIds) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR inventory_size.size_name IN :sizeNames) AND " +

            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +

            "(:stage IS NULL OR inventory.stage = :stage) AND " +
            "(:status IS NULL OR inventory.status = :status) AND " +
//            "(market_listing.market_place_id = :marketPlaceId) AND " +
            "(:name IS NULL OR inventory.name LIKE :name) AND " +

            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR bicycle_year.id >= :startYearId) AND " +
            "(:endYearId IS NULL OR bicycle_year.id <= :endYearId) " +

            "ORDER BY " +
            "CASE :sortField WHEN 'TIME_START_LISTING_NEWEST' THEN inventory_market_listing.time_start_listing END DESC, " +
            "CASE :sortField WHEN 'TIME_END_LISTING_SOONEST' THEN inventory_market_listing.time_end_listing END, " +
            "CASE WHEN :sortField='LISTED_PRICE' AND :sortType='DESC' THEN inventory.current_listed_price END DESC, " +//(CASE :sortType " +
            "CASE WHEN :sortField='LISTED_PRICE' AND :sortType!='DESC' THEN inventory.current_listed_price END " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<InventoryResponse> findAllMarketPlaceSorted(
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeIds") List<Long> typeIds,

            @Param("frameMaterialId") long frameMaterialId,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("brakeTypeId") long brakeTypeId,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("sizeId") long sizeId,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,

            @Param("stage") String stage,
            @Param("status") String status,
//            @Param("marketPlaceId") long marketPlaceId,
            @Param("name") String name,

            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("sortField") String sortField,
            @Param("sortType") String sortType,
            @Param("offset") long offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory.id AS id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.model_id  AS model_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.year_id AS year_id, " +
            "inventory_bicycle.type_id AS type_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
//            "inventory_type.id AS type_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, inventory.stage AS stage, " +
            "inventory.name AS name,  inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory_size.inventory_id AS size_id, " +
            "inventory_size.size_name AS size_name " +
            "FROM inventory " +


            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN " +
            "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.value AS size_name " +
            "FROM inventory_comp_detail " +
            "WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) " +
            "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) " +
                "AS brake_type " +
                "ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail " +
                "WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) " +
                "AS frame_material " +
                "ON inventory.id = frame_material.inventory_id " +

            //for sorting only
            "LEFT JOIN " +
            "(SELECT MIN(market_listing.start_time) AS time_start_listing, " +
            "MIN(market_listing.end_time) AS time_end_listing, " +
            "market_listing.inventory_id AS inventory_id " +
            "FROM market_listing " +
            "GROUP BY market_listing.inventory_id ) " +
            "AS inventory_market_listing " +
            "ON inventory.id = inventory_market_listing.inventory_id " +

            //main conditions
            "WHERE " +
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR bicycle_model.id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR bicycle_brand.id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory_type.id IN :typeIds) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR inventory_size.size_name IN :sizeNames) AND " +

            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +

            "(:stage IS NULL OR inventory.stage = :stage) AND " +
            "(:status IS NULL OR inventory.status = :status) AND " +
            "(:name IS NULL OR inventory.name LIKE :name) AND " +

            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR bicycle_year.id >= :startYearId) AND " +
            "(:endYearId IS NULL OR bicycle_year.id <= :endYearId) " +

            "ORDER BY " +
            "CASE :sortField WHEN 'TIME_START_LISTING_NEWEST' THEN inventory_market_listing.time_start_listing END DESC, " +
            "CASE :sortField WHEN 'TIME_END_LISTING_SOONEST' THEN inventory_market_listing.time_end_listing END, " +
            "CASE WHEN :sortField='LISTED_PRICE' AND :sortType='DESC' THEN inventory.current_listed_price END DESC, " +//(CASE :sortType " +
            "CASE WHEN :sortField='LISTED_PRICE' AND :sortType!='DESC' THEN inventory.current_listed_price END " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<InventoryResponse> findAllSorted(
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeIds") List<Long> typeIds,

            @Param("frameMaterialId") long frameMaterialId,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("brakeTypeId") long brakeTypeId,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("sizeId") long sizeId,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,

            @Param("stage") String stage,
            @Param("status") String status,
            @Param("name") String name,

            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("sortField") String sortField,
            @Param("sortType") String sortType,
            @Param("offset") long offset,
            @Param("size") int size
    );


    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM inventory " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS size_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) AS inventory_size ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS brake_type_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) AS brake_type ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS fragment_material_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) AS frame_material ON inventory.id = frame_material.inventory_id " +
            "WHERE " +
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR bicycle_model.id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR bicycle_brand.id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory_type.id IN :typeIds) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR inventory_size.size_name IN :sizeNames) AND " +


            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +
////
            "(:stage IS NULL OR inventory.stage = :stage) AND " +
            "(:status IS NULL OR inventory.status = :status) AND " +
            "(:name IS NULL OR inventory.name LIKE :name) AND " +
////
////
            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR bicycle_year.id >= :startYearId) AND " +
            "(:endYearId IS NULL OR bicycle_year.id <= :endYearId)"

    )
    int countAll(
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeIds") List<Long> typeIds,

            @Param("frameMaterialId") long frameMaterialId,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("brakeTypeId") long brakeTypeId,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("sizeId") long sizeId,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,
//
            @Param("stage") String stage,
            @Param("status") String status,
            @Param("name") String name,
////
            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM inventory " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN market_listing ON inventory.id = market_listing.inventory_id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS size_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) AS inventory_size ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS brake_type_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) AS brake_type ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN (SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS fragment_material_name FROM inventory_comp_detail WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) AS frame_material ON inventory.id = frame_material.inventory_id " +
            "WHERE " +
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR bicycle_model.id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR bicycle_brand.id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory_type.id IN :typeIds) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR inventory_size.size_name IN :sizeNames) AND " +


            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +
////
            "(:stage IS NULL OR inventory.stage = :stage) AND " +
            "(:status IS NULL OR inventory.status = :status) AND " +
            "( market_listing.market_place_id = :marketPlaceId) AND " +
            "(:name IS NULL OR inventory.name LIKE :name) AND " +
////
////
            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR bicycle_year.id >= :startYearId) AND " +
            "(:endYearId IS NULL OR bicycle_year.id <= :endYearId)"

    )
    int countAllMarketPlace(
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeIds") List<Long> typeIds,

            @Param("frameMaterialId") long frameMaterialId,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("brakeTypeId") long brakeTypeId,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("sizeId") long sizeId,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,
//
            @Param("stage") String stage,
            @Param("status") String status,
            @Param("marketPlaceId") long marketPlaceId,
            @Param("name") String name,
////
            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId
    );
}
