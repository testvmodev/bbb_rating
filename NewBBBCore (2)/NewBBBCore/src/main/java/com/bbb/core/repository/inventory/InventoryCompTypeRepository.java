package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryCompType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryCompTypeRepository extends CrudRepository<InventoryCompType, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type")
    List<InventoryCompType> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(inventory_comp_type.id) FROM inventory_comp_type WHERE inventory_comp_type.id IN :ids")
    int getTotalCount(@Param(value = "ids") List<Long> ids);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type WHERE inventory_comp_type.id IN :ids")
    List<InventoryCompType> findAllByIds(@Param(value = "ids") List<Long> ids);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_type WHERE inventory_comp_type.name IN :compTypeNames")
    List<InventoryCompType> findAllByName(
            @Param(value = "compTypeNames") List<String> compTypeNames
    );


    @Query(nativeQuery = true, value = "SELECT DISTINCT " +
            "inventory_comp_type.id, inventory_comp_type.name, " +
            "inventory_comp_type.is_select, inventory_comp_type.additional_value " +
            "FROM " +
            "trade_in_custom_quote_comp_detail JOIN inventory_comp_type ON " +
            "trade_in_custom_quote_comp_detail.comp_type_id = inventory_comp_type.id " +
            "WHERE trade_in_custom_quote_comp_detail.trade_in_custom_quote_id = :tradeInCustomQuoteId"
    )
    List<InventoryCompType> findAllByCustomquoteId(
            @Param(value = "tradeInCustomQuoteId") long tradeInCustomQuoteId
    );


    @Query(nativeQuery = true, value = "SELECT DISTINCT " +
            "inventory_comp_type.id, inventory_comp_type.name, " +
            "inventory_comp_type.is_select, inventory_comp_type.additional_value " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON " +
            "inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId"
    )
    List<InventoryCompType> findAllByInventoryIdId(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_comp_type " +
            "WHERE name = :compTypeName")
    InventoryCompType findByName(
            @Param("compTypeName") String compTypeName
    );
}
