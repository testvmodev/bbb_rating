package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CompletedShipmentDetail {
    @JacksonXmlProperty(localName = "UsDomestic")
    private boolean usDomestic;
    @JacksonXmlProperty(localName = "CarrierCode")
    private String carrierCode;
    @JacksonXmlProperty(localName = "MasterTrackingId")
    private TrackingId masterTrackingId;
    @JacksonXmlProperty(localName = "ServiceTypeDescription")
    private String serviceTypeDescription;
    @JacksonXmlProperty(localName = "OperationalDetail")
    private OperationalDetail operationalDetail;
    @JacksonXmlProperty(localName = "ShipmentRating")
    private ShipmentRating shipmentRating;
    @JacksonXmlProperty(localName = "CompletedPackageDetails")
    private CompletedPackageDetails completedPackageDetails;

    public boolean isUsDomestic() {
        return usDomestic;
    }

    public void setUsDomestic(boolean usDomestic) {
        this.usDomestic = usDomestic;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public TrackingId getMasterTrackingId() {
        return masterTrackingId;
    }

    public void setMasterTrackingId(TrackingId masterTrackingId) {
        this.masterTrackingId = masterTrackingId;
    }

    public String getServiceTypeDescription() {
        return serviceTypeDescription;
    }

    public void setServiceTypeDescription(String serviceTypeDescription) {
        this.serviceTypeDescription = serviceTypeDescription;
    }

    public OperationalDetail getOperationalDetail() {
        return operationalDetail;
    }

    public void setOperationalDetail(OperationalDetail operationalDetail) {
        this.operationalDetail = operationalDetail;
    }

    public ShipmentRating getShipmentRating() {
        return shipmentRating;
    }

    public void setShipmentRating(ShipmentRating shipmentRating) {
        this.shipmentRating = shipmentRating;
    }

    public CompletedPackageDetails getCompletedPackageDetails() {
        return completedPackageDetails;
    }

    public void setCompletedPackageDetails(CompletedPackageDetails completedPackageDetails) {
        this.completedPackageDetails = completedPackageDetails;
    }
}
