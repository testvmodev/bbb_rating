package com.bbb.core.model.request.tradein.ups;

import com.bbb.core.model.request.tradein.ups.detail.track.TrackRequest;
import com.bbb.core.model.request.tradein.ups.detail.account.UPSSecurity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSTrackRequest {
    @JsonProperty("UPSSecurity")
    private UPSSecurity upsSecurity;
    @JsonProperty("TrackRequest")
    private TrackRequest trackRequest;

    public UPSSecurity getUpsSecurity() {
        return upsSecurity;
    }

    public void setUpsSecurity(UPSSecurity upsSecurity) {
        this.upsSecurity = upsSecurity;
    }

    public TrackRequest getTrackRequest() {
        return trackRequest;
    }

    public void setTrackRequest(TrackRequest trackRequest) {
        this.trackRequest = trackRequest;
    }
}
