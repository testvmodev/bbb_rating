package com.bbb.core.model.database;


public class BicycleComponentUpgrade {

  private long id;
  private String name;
  private long typeId;
  private long bicycleId;
  private String description;
  private String cost;
  private long ageInMonthRange;
  private String premium;
  private java.sql.Timestamp upgradeDate;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public long getTypeId() {
    return typeId;
  }

  public void setTypeId(long typeId) {
    this.typeId = typeId;
  }


  public long getBicycleId() {
    return bicycleId;
  }

  public void setBicycleId(long bicycleId) {
    this.bicycleId = bicycleId;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public String getCost() {
    return cost;
  }

  public void setCost(String cost) {
    this.cost = cost;
  }


  public long getAgeInMonthRange() {
    return ageInMonthRange;
  }

  public void setAgeInMonthRange(long ageInMonthRange) {
    this.ageInMonthRange = ageInMonthRange;
  }


  public String getPremium() {
    return premium;
  }

  public void setPremium(String premium) {
    this.premium = premium;
  }


  public java.sql.Timestamp getUpgradeDate() {
    return upgradeDate;
  }

  public void setUpgradeDate(java.sql.Timestamp upgradeDate) {
    this.upgradeDate = upgradeDate;
  }

}
