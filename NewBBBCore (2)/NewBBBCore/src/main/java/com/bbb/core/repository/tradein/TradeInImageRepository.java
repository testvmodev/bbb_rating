package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInImageRepository extends JpaRepository<TradeInImage, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image WHERE trade_in_image.key_hash = :keyHash")
    TradeInImage findByKeyHash(@Param("keyHash") String keyHash);


    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image WHERE trade_in_image.trade_in_id = :tradeInId")
    List<TradeInImage> findAllByTradeInId(
            @Param(value = "tradeInId") long tradeInId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image WHERE trade_in_image.custom_quote_id = :customQuoteId")
    List<TradeInImage> findAllByTradeInCustomQuoteId(
            @Param(value = "customQuoteId") long customQuoteId
    );


    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image " +
            "WHERE " +
            "trade_in_image.trade_in_id = :tradeInId AND " +
            "(:isImageTypeNull = 1 OR trade_in_image.image_type_id IN :imageTypeIds) AND " +
            "(:imageIdsNull = 1 OR trade_in_image.id IN :imageIds)"
    )
    List<TradeInImage> findAllByTradeInIdAndImageTypeIdsAndImageIds(
            @Param(value = "tradeInId") long tradeInId,
            @Param(value = "isImageTypeNull") boolean isImageTypeNull,
            @Param(value = "imageTypeIds") List<Long> imageTypeIds,
            @Param(value = "imageIdsNull") boolean imageIdsNull,
            @Param(value = "imageIds") List<Long> imageIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_image WHERE trade_in_image.custom_quote_id = :customQuoteId AND trade_in_image.image_type_id = :imageTypeId")
    List<TradeInImage> findAllByCustomQuoteIdAndImageTypeId(
            @Param(value = "customQuoteId") long customQuoteId,
            @Param(value = "imageTypeId") long imageTypeId
    );



    @Query(nativeQuery = true, value = "SELECT trade_in_image.id " +
            "FROM trade_in_image " +
            "WHERE trade_in_image.trade_in_id = :tradeInId "
    )
    List<Long> getAllTradeImageImageUpload(
            @Param(value = "tradeInId") long tradeInId
    );

}
