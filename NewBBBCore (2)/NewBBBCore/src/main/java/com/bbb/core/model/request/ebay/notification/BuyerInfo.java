package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class BuyerInfo {
    @JacksonXmlProperty(localName = "ShippingAddress")
    private ShippingAddress shippingAddress;

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
