package com.bbb.core.model.response.tradein;

import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TradeInResponse {
    @Id
    private long tradeInId;
    private String partnerAddress;
    private String partnerName;
    private LocalDateTime createdTime;
    private String customerName;
    private String titleBicycle;
    private Long statusId;
    private String statusName;
    private boolean isCancel;
    private boolean isSave;
    private InboundShippingType shippingType;
    private Long customQuoteId;
    private Long inventoryId;
    private StatusInventory inventoryStatus;
    private StageInventory inventoryStage;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public String getPartnerAddress() {
        return partnerAddress;
    }

    public void setPartnerAddress(String partnerAddress) {
        this.partnerAddress = partnerAddress;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTitleBicycle() {
        return titleBicycle;
    }

    public void setTitleBicycle(String titleBicycle) {
        this.titleBicycle = titleBicycle;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }

    public InboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(InboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public Long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(Long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public StatusInventory getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(StatusInventory inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public StageInventory getInventoryStage() {
        return inventoryStage;
    }

    public void setInventoryStage(StageInventory inventoryStage) {
        this.inventoryStage = inventoryStage;
    }
}
