package com.bbb.core.controller.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.s3.S3DownloadImage;
import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.StatusTradeIn;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.model.request.tradein.tradin.*;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.request.bicycle.BicycleFromBrandYearModelRequest;
import com.bbb.core.service.tradein.TradeInService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiParam;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class TradeInController {
    @Autowired
    private TradeInService service;

    //tradeInImageTypeId_name
    //step 3
    @PostMapping(value = Constants.URL_TRADE_IN_UPLOAD_IMAGE)
    public ResponseEntity postTradeIn(
            @RequestParam(value = "tradeInId") long tradeInId,
            @RequestParam(value = "isSave", required = false, defaultValue = "false") boolean isSave,
//            @RequestParam(value = "isAddImage", required = false, defaultValue = "false") boolean isAddImage,
            @RequestParam(value = "images") List<MultipartFile> images
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.postTradeIn(tradeInId, images, isSave), HttpStatus.OK);
    }

    @GetMapping(Constants.URL_TRADE_IN_LOAD_IMAGE)
    public void getTradeInImage(
            HttpServletResponse response,
            @RequestParam("keyHash") String keyHash
    ) throws ExceptionResponse {
        S3DownloadImage image = service.getTradeInImage(keyHash);
        response.setContentType(image.getMediaType().toString());
        try {
            response.getOutputStream().write(image.getData());
        } catch (IOException e) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, "Internal Error"),
                    HttpStatus.NOT_FOUND
            );
        }
    }

    @GetMapping(Constants.URL_TRADE_IN_CALCULATE_SHIPPING)
    public ResponseEntity calcShipping(
            @RequestParam(value = "length") int length,
            @RequestParam(value = "width") int width,
            @RequestParam(value = "height") int height,
            @RequestParam(value = "weight") float weight,
            @RequestParam(value = "fromAddress") String fromAddress,
            @RequestParam(value = "fromCity") String fromCity,
            @RequestParam(value = "fromState") String fromState,
            @RequestParam(value = "fromZipCode") String fromZipCode
    ) throws ExceptionResponse {
        TradeInCalculateShipRequest request = new TradeInCalculateShipRequest();
        request.setLength(length);
        request.setWidth(width);
        request.setHeight(height);
        request.setWeight(weight);
        request.setFromAddress(fromAddress);
        request.setFromCity(fromCity);
        request.setFromState(fromState);
        request.setFromZipCode(fromZipCode);
        return new ResponseEntity<>(service.calcShipping(request), HttpStatus.OK);
    }

    //step 4
    @PostMapping(value = Constants.URL_TRADE_IN_SHIPPING)
    public ResponseEntity createShipping(
            @RequestParam(value = "tradeInId") long tradeInId,
            @RequestParam(value = "length", required = false) Integer length,
            @RequestParam(value = "width", required = false) Integer width,
            @RequestParam(value = "height", required = false) Integer height,
            @RequestParam(value = "weight", required = false) Float weight,
            @RequestParam(value = "fromAddress", required = false) String fromAddress,
            @RequestParam(value = "fromCity", required = false) String fromCity,
            @ApiParam(value = "State code")
            @RequestParam(value = "fromState", required = false) String fromState,
            @RequestParam(value = "fromZipCode", required = false) String fromZipCode,
//carrier type is in shipping account
//            @RequestParam(value = "carrierType", required = false) CarrierType carrierType,
            @RequestParam(value = "carrier", required = false) String carrier,
            @RequestParam(value = "carrierTrackingNumber", required = false) String carrierTrackingNumber,
            @RequestParam(value = "shippingType", required = false) InboundShippingType shippingType,
            @RequestParam(value = "isScheduled", required = false, defaultValue = "false") boolean isScheduled,
            @RequestParam(value = "scheduleDate", required = false) LocalDate scheduleDate,
            @RequestParam(value = "isSave", required = false, defaultValue = "false") boolean isSave
    ) throws ExceptionResponse {
        TradeInShippingCreateRequest request = new TradeInShippingCreateRequest();
        request.setTradeInId(tradeInId);
        request.setShippingType(shippingType);

        ShippingBicycleBlueBookRequest blueBook = new ShippingBicycleBlueBookRequest();
        blueBook.setLength(length);
        blueBook.setWidth(width);
        blueBook.setHeight(height);
        blueBook.setWeight(weight);
        blueBook.setFromAddress(fromAddress);
        blueBook.setFromCity(fromCity);
        blueBook.setFromState(fromState);
        blueBook.setFromZipCode(fromZipCode);
//        blueBook.setCarrierType(carrierType);
        blueBook.setScheduled(isScheduled);
        blueBook.setScheduleDate(scheduleDate);
        request.setBlueBook(blueBook);

        ShippingMyAccountRequest myAccount = new ShippingMyAccountRequest();
        myAccount.setCarrier(carrier);
        myAccount.setCarrierTrackingNumber(carrierTrackingNumber);
        request.setMyAccount(myAccount);
        request.setSave(isSave);
        return new ResponseEntity<>(service.createShipping(request), HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_TRADE_IN_BICYCLE)
    public ResponseEntity getTradeInBicycleDetail(
            @RequestParam(value = "brandId") long brandId,
            @RequestParam(value = "modelId") long modelId,
            @RequestParam(value = "yearId") long yearId
    ) throws ExceptionResponse {
        BicycleFromBrandYearModelRequest request = new BicycleFromBrandYearModelRequest();
        request.setBrandId(brandId);
        request.setModelId(modelId);
        request.setYearId(yearId);

        return new ResponseEntity<>(service.getTradeInBicycleDetail(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_TRADE_IN_REASON_DECLINE)
    public ResponseEntity getAllTradeInReasonDecline() {
        return new ResponseEntity<>(service.getAllTradeInReasonDecline(), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_TRADE_IN_DECLINE_DETAIL)
    public ResponseEntity declineTradeIn(
            @RequestBody DeclineTradeInRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.declineTradeIn(request),
                HttpStatus.OK);
    }

    //step 1
    @PostMapping(value = Constants.URL_TRADE_IN)
    public ResponseEntity createTradeIn(
//            @RequestParam(value = "bicycleId", required = false) Long bicycleId,
//            @RequestParam(value = "condition", required = false) ConditionInventory condition,
//            @RequestParam(value = "value", required = false) Float value,
//            @RequestParam(value = "upgradeCompIds", required = false) List<Long> upgradeCompIds,
//            @RequestParam(value = "bicycleTypeId", required = false) Long bicycleTypeId,
//            @RequestParam(value = "compRequests", required = false) List<CompRequest> compRequests,
//            @RequestParam(value = "customQuoteId", required = false) Long customQuoteId,
//            @RequestParam(value = "eBikeMileage", required = false) Float eBikeMileage,
//            @RequestParam(value = "eBikeHours", required = false) Float eBikeHours,
//            @RequestParam(value = "isClean", required = false) Boolean isClean
            @RequestBody TradeInCreateRequestTwo request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
//        TradeInCreateRequestTwo request = new TradeInCreateRequestTwo(bicycleId, condition, value, upgradeCompIds, customQuoteId);
//        request.setBicycleId(bicycleTypeId);
//        request.setCompRequests(compRequests);
//        request.seteBikeMileage(eBikeMileage);
//        request.seteBikeHours(eBikeHours);
//        request.setClean(isClean);
        return new ResponseEntity<>(
                service.createTradeIn(request),
                HttpStatus.OK);
    }

//    @GetMapping(value = Constants.URL_TRADE_IN_PATH)
//    public ResponseEntity getTradeInDetail(
//            @PathVariable(value = Constants.ID) long tradeInId
//    ) throws ExceptionResponse {
//        return new ResponseEntity<>(service.getTradeInDetail(tradeInId),
//                HttpStatus.OK);
//    }

    @PutMapping(Constants.URL_TRADE_IN_PATH)
    public ResponseEntity updateStepDetail(
            @PathVariable(Constants.ID) long tradeInId,
            @RequestBody TradeInUpdateDetailRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateStepDetail(tradeInId, request));
    }

    @PostMapping(value = Constants.URL_TRADE_IN_SUMMARY_PATH)
    public ResponseEntity updateStepSummary(
            @PathVariable(value = Constants.ID) long id,
            @RequestBody TradeInSummaryRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateStepSummary(id, request),
                HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_TRADE_IN_STATUS)
    public ResponseEntity getAllStatusTradeIn() {
        return new ResponseEntity<>(service.getAllStatusTradeIn(),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_TRADE_INS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getTradeIns(
            @RequestParam(value = "isArchived", required = false) Boolean isArchived,
            @RequestParam(value = "status", required = false) StatusTradeIn status,
            @RequestParam(value = "typeSort", required = false, defaultValue = "DATE") TypeSearchTradeInRequest typeSort,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "partnerId", required = false) String partnerId,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") Sort sort,
            @RequestParam(value = "isIncomplete", required = false) Boolean isIncomplete,
            Pageable page
    ) throws ExceptionResponse {
        TradeInsRequest request = new TradeInsRequest(isArchived, status, typeSort, content, partnerId, sort, page);
        request.setIncomplete(isIncomplete);
        return new ResponseEntity<>(service.getTradeIns(request),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_TRADE_IN_PATH)
    public ResponseEntity getDetailTradeIn(
            @PathVariable(value = Constants.ID) long tradeId,
            @RequestParam(value = "indexStep") int indexStep
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getDetailTradeIn(tradeId, indexStep),
                HttpStatus.OK);
    }

    @PatchMapping(value = Constants.URL_TRADE_IN_ARCHIVED)
    public ResponseEntity archiveTradeIn(
            @RequestParam(value = "tradeInId") long tradeInId,
            @RequestParam(value = "isArchive") boolean isArchive
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.archiveTradeIn(tradeInId, isArchive),
                HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_TRADE_IN_IMAGE + "/{tradeId}")
    public ResponseEntity deleteImageTradeIn(
            @PathVariable(value = "tradeId") long tradeId,
            @RequestParam(value = "imageTypeId", required = false) List<Long> imageImageTypeIds,
            @RequestParam(value = "imageIds", required = false) List<Long> imageIds
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteImageTradeIn(tradeId, imageImageTypeIds, imageIds),
                HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_GET_TRADE_IN_COST_CALCULATOR)
    public ResponseEntity getTradeInCostCalculator(
            @RequestParam(value = "privateParty") float privateParty
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getTradeInCostCalculator(privateParty),
                HttpStatus.OK);
    }

    @PatchMapping(value = Constants.URL_TRADE_IN_CANCEL)
    public ResponseEntity cancelOrReActiveTradeIn(
            @PathVariable(value = Constants.ID) long tradeInId,
            @RequestParam(value = "isCancel") boolean isCancel
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.cancelTradeIn(tradeInId, isCancel),
                HttpStatus.OK);
    }

    @PatchMapping(value = Constants.URL_TRADE_INS_CANCEL)
    public ResponseEntity cancelOrReActiveTradeIns(
            @RequestParam(value = "tradeInIds") List<Long> tradeInIds,
            @RequestParam(value = "isCancel") boolean isCancel
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.cancelTradeIns(tradeInIds, isCancel));
    }

    @GetMapping(value = Constants.URL_TRADE_IN_COMMON)
    public ResponseEntity getTradeInCommon(
            @PathVariable(value = Constants.ID) long tradeInId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getTradeInCommon(tradeInId),
                HttpStatus.OK);
    }

    @GetMapping(Constants.URL_COUNT_TRADE_INCOMPLETE)
    public ResponseEntity getCountIncomplete() throws ExceptionResponse {
        return ResponseEntity.ok(service.getCountIncomplete());
    }
}
