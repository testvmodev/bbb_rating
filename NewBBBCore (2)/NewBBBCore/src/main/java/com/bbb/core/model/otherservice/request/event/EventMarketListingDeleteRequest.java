package com.bbb.core.model.otherservice.request.event;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventMarketListingDeleteRequest {
    @JsonProperty("inventory_id")
    private long inventoryId;

    public EventMarketListingDeleteRequest() {}

    public EventMarketListingDeleteRequest(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }
}
