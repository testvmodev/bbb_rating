package com.bbb.core.manager.salesforce;

import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.GoogleServiceUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bicycle.util.BrandModelYearManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.inventory.InventoryManager;
import com.bbb.core.manager.inventory.async.InventoryManagerAsync;
import com.bbb.core.manager.market.MarketListingManager;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.manager.notification.SubscriptionManager;
import com.bbb.core.manager.shipping.ShipmentManager;
import com.bbb.core.manager.tradein.TradeInCustomQuoteManager;
import com.bbb.core.model.InitialInventory;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.common.GoogleServiceResponse;
import com.bbb.core.model.otherservice.response.common.google.AddressComponent;
import com.bbb.core.model.otherservice.response.common.google.GeometryLocation;
import com.bbb.core.model.otherservice.response.shipping.ShippingConfigResponse;
import com.bbb.core.model.otherservice.response.warehouse.WarehouseResponse;
import com.bbb.core.model.request.inventory.create.InventoryCompDetailCreateRequest;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import com.bbb.core.model.request.saleforce.ListingFullUpdateRequest;
import com.bbb.core.model.request.saleforce.create.ListingInventoryBaseInfo;
import com.bbb.core.model.request.saleforce.create.ListingInventoryLocation;
import com.bbb.core.model.request.saleforce.create.SaleforceInvCompRequest;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import com.bbb.core.model.response.salesforce.BaseSaleForceResponse;
import com.bbb.core.model.response.salesforce.CreateListingSalesforceResponse;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.inventory.*;
import com.bbb.core.repository.inventory.out.InitialInventoryRepository;
import com.bbb.core.repository.market.InventoryLocationShippingRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.MarketPlaceConfigRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SalesforceManager implements MessageResponses {
    //region Beans
    //managers
    @Autowired
    private TradeInCustomQuoteManager tradeInCustomQuoteManager;
    @Autowired
    private MyListingManager myListingManager;
    @Autowired
    private BrandModelYearManager brandModelYearManager;
    @Autowired
    private InventoryManager inventoryManager;
    @Autowired
    private InventoryManagerAsync inventoryManagerAsync;
    @Autowired
    private ShipmentManager shipmentManager;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private SubscriptionManager subscriptionManager;
    @Autowired
    private MarketListingManager marketListingManager;
    @Autowired
    private BillingManager billingManager;
    //repos
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private InventoryBicycleRepository inventoryBicycleRepository;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InitialInventoryRepository initialInventoryRepository;
    @Autowired
    private InventoryCompDetailRepository inventoryCompDetailRepository;
    //
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AppConfig appConfig;
    //endregion

    //region API
    public Object createListingSalesforce(
            ListingFullCreateRequest request
    ) throws ExceptionResponse {
        Inventory existedInventory = inventoryRepository.findLastSalesforceInventory(
                request.getAddBikeRequest().getInventoryItem().getSalesforceId(), null);
        if (existedInventory != null) {
            //
        }

        //validate
        ListingInventoryBaseInfo inventoryBase = request.getAddBikeRequest().getBaseInfo();
        CommonUtils.checkYearValid(inventoryBase.getYearName());

        BicycleType bicycleType = bicycleTypeRepository.findOneByName(inventoryBase.getBicycleType());
        if (bicycleType == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    BICYCLE_TYPE_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        List<CompRequest> compRequests = tradeInCustomQuoteManager.validateSaleforceCompType(
                request.getAddBikeRequest().getComponents(),
                true
        );
        CompRequest frameSize = mapInvSizeToFrameSize(
                request.getAddBikeRequest().getInventoryItem().getSize(),
                request.getAddBikeRequest().getComponents()
        );
        if (frameSize != null) {
            if (compRequests == null) {
                compRequests = new ArrayList<>();
            }
            compRequests.add(frameSize);
        }

        InventoryType inventoryType = inventoryTypeRepository.findOneByName(inventoryBase.getInventoryTypeName());
        if (inventoryType == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    INVENTORY_TYPE_NOT_FOUND,
                    HttpStatus.NOT_FOUND
            );
        }
        if (request.getAddBikeRequest().getBestOffer()) {
            myListingManager.validatePrices(
                    inventoryBase.getCurrentListedPrice(),
                    request.getAddBikeRequest().getBestOffer(),
                    request.getAddBikeRequest().getBestOfferAutoAcceptPrice(),
                    request.getAddBikeRequest().getMinimumOfferAutoAcceptPrice()
            );
        }

        //creating bicycle
        BrandModelYear brandModelYear = brandModelYearManager.findOneCreateIfNotExist(
                inventoryBase.getBrandName(),
                inventoryBase.getModelName(),
                inventoryBase.getYearName()
        );
        Bicycle bicycle = null;
        if (brandModelYear.isBicycleMightExist()) {
            bicycle = bicycleRepository.findOneByBrandModelYear(
                    brandModelYear.getBrandId(),
                    brandModelYear.getModelId(),
                    brandModelYear.getYearId(),
                    null
            );
        }
        if (bicycle == null) {
            bicycle = new Bicycle();
            bicycle.setBrandId(brandModelYear.getBrandId());
            bicycle.setModelId(brandModelYear.getModelId());
            bicycle.setTypeId(bicycleType.getId());
            bicycle.setYearId(brandModelYear.getYearId());
            bicycle.setRetailPrice(inventoryBase.getMsrpPrice());
            bicycle.setName(brandModelYear.getYearName() + " " + brandModelYear.getBrandName() + " " + brandModelYear.getModelName());
            if (inventoryBase.getImages() != null && inventoryBase.getImages().size() > 0) {
                bicycle.setImageDefault(inventoryBase.getImages().get(0));
            }
            bicycle = bicycleRepository.save(bicycle);
        }

        //init objects
        Inventory inventory = new Inventory();
        MarketListing marketListing = new MarketListing();
        if (existedInventory != null) {
            inventory = existedInventory;
            inventory.setDelete(false);
            MarketListing existedListing = marketListingRepository.findLastByInventoryId(inventory.getId(), null);
            if (existedListing != null) {
                marketListing = existedListing;
                marketListing.setDelete(false);
            }
        }

        //creating inventory
        inventory.setBicycleId(bicycle.getId());
        inventory.setBicycleBrandName(brandModelYear.getBrandName());
        inventory.setBicycleModelName(brandModelYear.getModelName());
        inventory.setBicycleYearName(brandModelYear.getYearName());
        inventory.setBicycleTypeName(bicycleType.getName());
//        inventory.setStatus(inventoryBase.getStatusInventory());
//        inventory.setStage(inventoryBase.getStageInventory());
        inventory.setStatus(StatusInventory.ACTIVE);
        inventory.setStage(StageInventory.LISTED);
        inventory.setCondition(inventoryBase.getCondition());
        inventory.setTypeId(inventoryType.getId());
        inventory.setTitle(inventory.getBicycleYearName() + " " + inventory.getBicycleBrandName() + " " + inventory.getBicycleModelName());
        inventory.setDescription(inventoryBase.getDescription());
        inventory.setCurrentListedPrice(inventoryBase.getCurrentListedPrice());
        inventory.setBbbValue(inventoryBase.getCurrentListedPrice());
        inventory.setDiscountedPrice(inventoryBase.getCurrentListedPrice());
        inventory.setInitialListPrice(inventoryBase.getCurrentListedPrice());
        inventory.setCogsPrice(inventoryBase.getCurrentListedPrice());
        inventory.setMsrpPrice(inventoryBase.getMsrpPrice());
        inventory.setRecordType(inventoryBase.getRecordType());
        inventory.setImageDefault(inventoryBase.getImages().get(0));
        inventory.setSerialNumber(inventoryBase.getSerialNumber());
        inventory.setNote(inventoryBase.getNote());
        //store the id of inv on salesforce
        inventory.setSalesforceId(request.getAddBikeRequest().getInventoryItem().getSalesforceId());
        inventory.setOversized(inventoryBase.getOversized());
        inventory.setSellerId(ValueCommons.SELLER_ID_DEFAULT);
        inventory.setSellerIsBBB(true);
        inventory.setSearchGuideRecommendation("");
        if (!StringUtils.isBlank(request.getAddBikeRequest().getInventoryItem().getInventoryName())) {
            inventory.setName(request.getAddBikeRequest().getInventoryItem().getInventoryName());
        }
        inventory = inventoryRepository.save(inventory);

        Inventory invFinal = inventory;

        List<InventoryCompDetailCreateRequest> invCompRequests = CollectionUtils.isNotEmpty(compRequests)
                ? compRequests.stream().map(req -> {
            InventoryCompDetailCreateRequest compReq = new InventoryCompDetailCreateRequest();
            compReq.setInventoryCompTypeId(req.getCompId());
            compReq.setValue(req.getValue());
            return compReq;
        }).collect(Collectors.toList())
                : null;
        inventoryManagerAsync.createCompsAsync(
                inventory.getId(),
                invCompRequests,
                invId -> {
                    invFinal.setSearchGuideRecommendation(inventoryManager.createAndUpdateSearchRecommendation(invId));
                });
        inventoryManagerAsync.createUpgradeComponent(inventory.getId(), request.getAddBikeRequest().getUpgradeComps());

        long inventoryId = inventory.getId();
        List<InventoryImage> images = inventoryBase.getImages().stream()
                .map(url -> {
                    InventoryImage inventoryImage = new InventoryImage();
                    inventoryImage.setInventoryId(inventoryId);
                    inventoryImage.setImage(url);

                    return inventoryImage;
                }).collect(Collectors.toList());
        images = inventoryImageRepository.saveAll(images);

        InventoryBicycle inventoryBicycle = new InventoryBicycle();
        inventoryBicycle.setInventoryId(inventory.getId());
        inventoryBicycle.setBrandId(brandModelYear.getBrandId());
        inventoryBicycle.setModelId(brandModelYear.getModelId());
        inventoryBicycle.setYearId(brandModelYear.getYearId());
        inventoryBicycle.setTypeId(bicycleType.getId());
        inventoryBicycleRepository.save(inventoryBicycle);

        InventoryLocationShipping locationShipping = new InventoryLocationShipping();
        locationShipping.setInventoryId(inventoryId);


        if (inventoryBase.getLocalPickupOnly() != null && inventoryBase.getLocalPickupOnly()) {
            //
        } else {
            locationShipping.setShippingType(OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE);
            locationShipping.setInsurance(true);
        }

        if (request.getAddBikeRequest().getLocationShipping() == null) {
            //default true if no info
            locationShipping.setAllowLocalPickup(true);
            ShippingConfigResponse shippingConfig = shipmentManager.getActiveShippingConfig();
            if (shippingConfig.getWarehouseId() != null) {
                WarehouseResponse warehouseResponse = authManager.getWarehouseDetail(shippingConfig.getWarehouseId());
                locationShipping.setAddressLine(warehouseResponse.getShippingAddress().getAddress());

                updateLocation(
                        locationShipping,
                        warehouseResponse.getShippingAddress().getCity(),
                        warehouseResponse.getShippingAddress().getState(),
                        warehouseResponse.getShippingAddress().getZipCode(),
                        warehouseResponse.getShippingAddress().getCountryCode()
                );
            }
        } else {
            ListingInventoryLocation locationShippingRequest = request.getAddBikeRequest().getLocationShipping();
            if (locationShippingRequest.getAllowLocalPickup() != null) {
                locationShipping.setAllowLocalPickup(locationShippingRequest.getAllowLocalPickup());
            } else {
                locationShipping.setAllowLocalPickup(true);
            }
            locationShipping.setAddressLine(locationShippingRequest.getAddressLine());
            updateLocation(
                    locationShipping,
                    locationShippingRequest.getCityName(),
                    locationShippingRequest.getState(),
                    locationShippingRequest.getZipCode(),
                    locationShippingRequest.getCountry()
            );
        }
        locationShipping = inventoryLocationShippingRepository.save(locationShipping);


        //create listing
        Long marketPlaceId = marketPlaceRepository.getMarketPlaceId(MarketType.BBB.getValue());
        Long marketPlaceConfigId = marketPlaceConfigRepository.findOneByMarketPlaceId(
                marketPlaceId, appConfig.getEbay().isSandbox()).getId();
        marketListing.setInventoryId(inventory.getId());
        marketListing.setMarketPlaceId(marketPlaceId);
        marketListing.setMarketPlaceConfigId(marketPlaceConfigId);
        if (request.getAddBikeRequest().getStatusMarketListing() != null) {
            marketListing.setStatus(request.getAddBikeRequest().getStatusMarketListing());
        } else {
            //TODO update saleforce then make this param required
            marketListing.setStatus(StatusMarketListing.LISTED);
        }
        marketListing.setBestOffer(request.getAddBikeRequest().getBestOffer());
        marketListing.setBestOfferAutoAcceptPrice(request.getAddBikeRequest().getBestOfferAutoAcceptPrice());
        marketListing.setMinimumOfferAutoAcceptPrice(request.getAddBikeRequest().getMinimumOfferAutoAcceptPrice());
        marketListing.setTimeListed(LocalDateTime.now());

        marketListing = marketListingRepository.save(marketListing);

        CreateListingSalesforceResponse response = mapResponse(marketListing);

        subscriptionManager.triggerPublishAsync(marketListing);

        return response;
    }

    public Object updateListingSalesforce(
            String salesforceId, ListingFullUpdateRequest request
    ) throws ExceptionResponse {
        Inventory inventory = findInventorySalesforce(salesforceId);
        MarketListing marketListing = marketListingRepository.findLastByInventoryId(inventory.getId(), null);
        Bicycle bicycle = null;
        InventoryBicycle inventoryBicycle = null;
        InventoryLocationShipping locationShipping = null;
        List<InventoryImage> insertInventoryImages = new ArrayList<>();
        List<InventoryImage> deleteInventoryImages = new ArrayList<>();


        boolean updateBicycle = false;
        boolean updateInventory = false;
        boolean updateInventoryBicycle = false;
        boolean updateImage = false;
        boolean updateLocation = false;
        boolean updateListing = false;



        ListingInventoryLocation locationRequest = request.getUpdateBikeRequest().getLocationShipping();
        if (locationRequest != null) {
            locationShipping = inventoryLocationShippingRepository.findOne(inventory.getId());

            if (!StringUtils.isBlank(locationRequest.getZipCode())
                    && !locationRequest.getZipCode().equals(locationShipping.getZipCode())
            ) {
                updateLocation = true;
            } else {
                locationRequest.setZipCode(locationShipping.getZipCode());
            }
            if (!StringUtils.isBlank(locationRequest.getCountry())
                    && !locationRequest.getCountry().equals(locationShipping.getCountryName())
                    && !locationRequest.getCountry().equals(locationShipping.getCountryName())
            ) {
                updateLocation = true;
            } else {
                locationRequest.setCountry(locationShipping.getCountryName());
            }
            if (!StringUtils.isBlank(locationRequest.getState())
                    && !locationRequest.getState().equals(locationShipping.getStateName())
                    && !locationRequest.getState().equals(locationShipping.getStateCode())
            ) {
                updateLocation = true;
            } else {
                locationRequest.setState(locationShipping.getStateName());
            }
            if (!StringUtils.isBlank(locationRequest.getCityName())
                    && !locationRequest.getCityName().equals(locationShipping.getCityName())
            ) {
                updateLocation = true;
            } else {
                locationRequest.setCityName(locationShipping.getCityName());
            }
            if (!StringUtils.isBlank(locationRequest.getAddressLine())
                    && !locationRequest.getAddressLine().equals(locationShipping.getAddressLine())
            ) {
                updateLocation = true;
            } else {
                locationRequest.setAddressLine(locationShipping.getAddressLine());
            }

            //update other location detail (dont update location later than this, later are shipping info)
            if (updateLocation) {
                updateLocation(
                        locationShipping,
                        locationRequest.getCityName(),
                        locationRequest.getState(),
                        locationRequest.getZipCode(),
                        locationRequest.getCountry()
                );
            }

            if (locationRequest.getAllowLocalPickup() != null
                    && locationRequest.getAllowLocalPickup() != locationShipping.isAllowLocalPickup()
            ) {
                locationShipping.setAllowLocalPickup(locationRequest.getAllowLocalPickup());
                updateLocation = true;
            }
        }

        ListingInventoryBaseInfo inventoryBase = request.getUpdateBikeRequest().getBaseInfo();
        if (inventoryBase != null) {
            inventoryBicycle = inventoryBicycleRepository.findOne(inventory.getId());

            if (StringUtils.isBlank(inventoryBase.getBicycleType())) {
                BicycleType bicycleType = bicycleTypeRepository.findOneByName(inventoryBase.getBicycleType());
                if (bicycleType == null) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_NOT_FOUND,
                            BICYCLE_TYPE_NOT_EXIST,
                            HttpStatus.NOT_FOUND
                    );
                } else if (bicycleType.getId() != inventoryBicycle.getTypeId()) {
                    inventoryBicycle.setTypeId(bicycleType.getId());
                    updateInventoryBicycle = true;
                }
            }

            //current base info of inventory
            InitialInventory initialInventory = initialInventoryRepository.initInventory(
                    inventoryBicycle.getModelId(), inventoryBicycle.getBrandId(),
                    inventoryBicycle.getYearId(), inventoryBicycle.getTypeId(),
                    null
            );

            //bicycle
            if (StringUtils.isBlank(inventoryBase.getBrandName())
                && !inventoryBase.getBrandName().equals(initialInventory.getBicycleBrandName())
            ) {
                updateBicycle = true;
            } else {
                inventoryBase.setBrandName(initialInventory.getBicycleBrandName());
            }
            if (StringUtils.isBlank(inventoryBase.getModelName())
                    && !inventoryBase.getModelName().equals(initialInventory.getBicycleModelName())
            ) {
                updateBicycle = true;
            } else {
                inventoryBase.setModelName(initialInventory.getBicycleModelName());
            }
            if (StringUtils.isBlank(inventoryBase.getYearName())
                    && !inventoryBase.getYearName().equals(initialInventory.getBicycleYearName())
            ) {
                updateBicycle = true;
            } else {
                inventoryBase.setYearName(initialInventory.getBicycleYearName());
            }

            if (updateBicycle) {
                BrandModelYear brandModelYear = brandModelYearManager.findOneCreateIfNotExist(
                        inventoryBase.getBrandName(),
                        inventoryBase.getModelName(),
                        inventoryBase.getYearName()
                );
                if (brandModelYear.isBicycleMightExist()) {
                    bicycle = bicycleRepository.findOneByBrandModelYear(
                            brandModelYear.getBrandId(),
                            brandModelYear.getModelId(),
                            brandModelYear.getYearId(),
                            null
                    );
                }
                if (bicycle == null) {
                    bicycle = new Bicycle();
                    bicycle.setBrandId(brandModelYear.getBrandId());
                    bicycle.setModelId(brandModelYear.getModelId());
                    bicycle.setTypeId(inventoryBicycle.getTypeId());
                    bicycle.setYearId(brandModelYear.getYearId());
                    bicycle.setRetailPrice(inventoryBase.getMsrpPrice());
                    bicycle.setName(brandModelYear.getYearName() + " " + brandModelYear.getBrandName() + " " + brandModelYear.getModelName());
                    if (inventoryBase.getImages() != null && inventoryBase.getImages().size() > 0) {
                        bicycle.setImageDefault(inventoryBase.getImages().get(0));
                    }
                }

                inventoryBicycle.setBrandId(brandModelYear.getBrandId());
                inventoryBicycle.setModelId(brandModelYear.getModelId());
                inventoryBicycle.setYearId(brandModelYear.getYearId());
                updateInventoryBicycle = true;
            }

            //inventory
            if (!StringUtils.isBlank(inventoryBase.getInventoryTypeName())) {
                InventoryType inventoryType = inventoryTypeRepository.findOneByName(
                        inventoryBase.getInventoryTypeName());
                if (inventoryType == null) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_NOT_FOUND,
                            INVENTORY_TYPE_NOT_FOUND,
                            HttpStatus.NOT_FOUND
                    );
                }
                inventory.setTypeId(inventoryType.getId());
                updateInventory = true;
            }
            if (inventoryBase.getStatusInventory() != null
                    && inventoryBase.getStatusInventory() != inventory.getStatus()
            ) {
                inventory.setStatus(inventoryBase.getStatusInventory());
                updateInventory = true;
            }
            if (inventoryBase.getStageInventory() != null
                    && inventoryBase.getStageInventory() != inventory.getStage()
            ) {
                inventory.setStage(inventoryBase.getStageInventory());
                updateInventory = true;
            }
            if (!StringUtils.isBlank(inventoryBase.getDescription())
                    && !inventoryBase.getDescription().equals(inventory.getDescription())
            ) {
                inventory.setDescription(inventoryBase.getDescription());
                updateInventory = true;
            }
            if (inventoryBase.getMsrpPrice() != null
                    && inventoryBase.getMsrpPrice() > 0
                    && inventoryBase.getMsrpPrice() != inventory.getMsrpPrice()
            ) {
                inventory.setMsrpPrice(inventoryBase.getMsrpPrice());
                updateInventory = true;
            }
            if (inventoryBase.getCurrentListedPrice() != null
                    && inventoryBase.getCurrentListedPrice() != inventory.getCurrentListedPrice()
            ) {
                inventory.setCurrentListedPrice(inventoryBase.getCurrentListedPrice());
                updateInventory = true;
            }
            if (!StringUtils.isBlank(inventoryBase.getSerialNumber())
                    && !inventoryBase.getSerialNumber().equals(inventory.getSerialNumber())
            ) {
                inventory.setSerialNumber(inventoryBase.getSerialNumber());
                updateInventory = true;
            }
            if (inventoryBase.getCondition() != null
                    && inventoryBase.getCondition() != inventory.getCondition()
            ) {
                inventory.setCondition(inventoryBase.getCondition());
                updateInventory = true;
            }
            if (inventoryBase.getRecordType() != null
                    && inventoryBase.getRecordType() != inventory.getRecordType()
            ) {
                inventory.setRecordType(inventoryBase.getRecordType());
                updateInventory = true;
            }
            if (!StringUtils.isBlank(inventoryBase.getNote())
                    && !inventoryBase.getNote().equals(inventory.getNote())
            ) {
                inventory.setNote(inventoryBase.getNote());
                updateInventory = true;
            }
            if (inventoryBase.getOversized() != null
                    && inventoryBase.getOversized() != inventory.getOversized()
            ) {
                inventory.setOversized(inventoryBase.getOversized());
                updateInventory = true;
            }

            //shipping
            if (inventoryBase.getLocalPickupOnly() != null) {
                if (locationShipping == null) {
                    locationShipping = inventoryLocationShippingRepository.findOne(inventory.getId());
                }
                if (inventoryBase.getLocalPickupOnly()
                        && locationShipping.getShippingType() != null
                ) {
                    locationShipping.setShippingType(null);
                    updateLocation = true;
                } else if (locationShipping.getShippingType() == null) {
                    locationShipping.setShippingType(OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE);
                    locationShipping.setInsurance(true);
                    updateLocation = true;
                }
            }

            //image
            if (inventoryBase.getImages() != null && inventoryBase.getImages().size() > 0) {
                List<InventoryImage> currentInventoryImages = inventoryImageRepository.findAllByInventoryId(
                        inventory.getId());
                for (InventoryImage inventoryImage : currentInventoryImages) {
                    //deleted
                    if (!inventoryBase.getImages().contains(inventoryImage.getImage())) {
                        deleteInventoryImages.add(inventoryImage);
                        if (inventory.getImageDefaultId() != null) {
                            if (inventory.getImageDefaultId() == inventoryImage.getId()) {
                                inventory.setImageDefaultId(null);
                                inventory.setImageDefault(null);
                            }
                        } else {
                            if (inventory.getImageDefault() != null
                                    && inventory.getImageDefault().equals(inventoryImage.getImage())
                            ) {
                                inventory.setImageDefault(null);
                            }
                        }
                    } else {
                        inventoryBase.getImages().remove(inventoryImage.getImage());
                    }
                }
            }
        }

        if (request.getUpdateBikeRequest().getBestOffer() != null) {
            marketListing.setBestOffer(request.getUpdateBikeRequest().getBestOffer());
            updateListing = true;
        }
        if (request.getUpdateBikeRequest().getBestOfferAutoAcceptPrice() != marketListing.getBestOfferAutoAcceptPrice()) {
            marketListing.setBestOfferAutoAcceptPrice(request.getUpdateBikeRequest().getBestOfferAutoAcceptPrice());
            updateListing = true;
        }
        if (request.getUpdateBikeRequest().getMinimumOfferAutoAcceptPrice() != marketListing.getMinimumOfferAutoAcceptPrice()) {
            marketListing.setMinimumOfferAutoAcceptPrice(request.getUpdateBikeRequest().getMinimumOfferAutoAcceptPrice());
            updateListing = true;
        }
        if (request.getUpdateBikeRequest().getStatusMarketListing() != null
                && request.getUpdateBikeRequest().getStatusMarketListing() != marketListing.getStatus()
        ) {
            marketListing.setStatus(request.getUpdateBikeRequest().getStatusMarketListing());
            updateListing = true;
        }
        myListingManager.validatePrices(
                inventory.getCurrentListedPrice(),
                marketListing.isBestOffer(),
                marketListing.getBestOfferAutoAcceptPrice(),
                marketListing.getMinimumOfferAutoAcceptPrice()
        );


        //saving

        List<SaleforceInvCompRequest> salesforceComponents = request.getUpdateBikeRequest().getComponents();
        if (salesforceComponents != null && salesforceComponents.size() > 0) {
            List<CompRequest> compRequests = tradeInCustomQuoteManager.validateSaleforceCompType(salesforceComponents, true);

            if (request.getUpdateBikeRequest().getInventoryItem() != null) {
                CompRequest frameSize = mapInvSizeToFrameSize(
                        request.getUpdateBikeRequest().getInventoryItem().getSize(),
                        salesforceComponents
                );
                if (frameSize != null) {
                    if (compRequests == null) {
                        compRequests = Collections.singletonList(frameSize);
                    }  else {
                        compRequests.add(frameSize);
                    }
                }
            }

            inventoryCompDetailRepository.deleteAllByInventoryId(inventory.getId());
            inventoryManagerAsync.createCompsSync(
                    inventory.getId(),
                    compRequests.stream().map(req -> {
                        InventoryCompDetailCreateRequest createRequest = new InventoryCompDetailCreateRequest();
                        createRequest.setInventoryCompTypeId(req.getCompId());
                        createRequest.setValue(req.getValue());
                        return createRequest;
                    }).collect(Collectors.toList())
            );
        }

        if (request.getUpdateBikeRequest().getUpgradeComps() != null
                && request.getUpdateBikeRequest().getUpgradeComps().size() > 0
        ) {
            inventoryManagerAsync.createUpgradeComponent(
                    inventory.getId(),
                    request.getUpdateBikeRequest().getUpgradeComps()
            );
        }

        if (updateBicycle && bicycle != null) {
            bicycle = bicycleRepository.save(bicycle);

            inventory.setBicycleId(bicycle.getId());
            updateInventory = true;
        }

        if (updateImage) {
            if (deleteInventoryImages.size() > 0) {
                //seem like deleted old default image
                //save now to prevent conflict foreign key image_default_id
                if (inventory.getImageDefault() == null && updateInventory) {
                    inventory = inventoryRepository.save(inventory);
                }

                inventoryImageRepository.deleteAll(deleteInventoryImages);
            }
            if (insertInventoryImages.size() > 0) {
                if (inventory.getImageDefault() == null) {
                    List<InventoryImage> images = inventoryImageRepository.saveAll(insertInventoryImages);
                    inventory.setImageDefault(images.get(0).getImage());
                    inventory.setImageDefaultId(images.get(0).getId());
                    updateInventory = true;
                } else {
                    inventoryImageRepository.saveAll(insertInventoryImages);
                }
            }
        }

        if (updateInventory) {
            inventoryRepository.save(inventory);
        }

        if (updateInventoryBicycle && inventoryBicycle != null) {
            inventoryBicycleRepository.save(inventoryBicycle);
        }

        if (locationShipping != null && updateLocation) {
            inventoryLocationShippingRepository.save(locationShipping);
        }

        if (updateListing) {
            marketListingRepository.save(marketListing);
        }

        CreateListingSalesforceResponse response = mapResponse(marketListing);

        billingManager.marketListingUpdateAsync(marketListing.getId());
        subscriptionManager.triggerUpdateAsync(marketListing);

        return response;
    }

    public Object deListListingSalesforce(String salesforceId) throws ExceptionResponse {
        Inventory inventory = findInventorySalesforce(salesforceId);
        MarketListing marketListing = marketListingRepository.findLastByInventoryId(
                inventory.getId(), null);
        //just de-list, not delete
        if (marketListing != null) {
            marketListing.setStatus(StatusMarketListing.DE_LISTED);
//            marketListing.setDelete(true);
            marketListingRepository.save(marketListing);
        }
//        inventory.setDelete(true);
        inventory.setStatus(StatusInventory.IN_ACTIVE);
        inventoryRepository.save(inventory);

        BaseSaleForceResponse response = new BaseSaleForceResponse("Ok", "Ok");
        return response;
    }

    public Object soldListingSalesforce(String salesforceId) throws ExceptionResponse {
        Inventory inventory = findInventorySalesforce(salesforceId);
        MarketListing marketListing = marketListingRepository.findLastByInventoryId(
                inventory.getId(), false);
        if (marketListing != null) {
            marketListing.setStatus(StatusMarketListing.DE_LISTED);
            marketListingRepository.save(marketListing);
        }
        inventory.setStatus(StatusInventory.SOLD);
        inventory.setStage(StageInventory.SOLD);
        inventoryRepository.save(inventory);

        marketListingManager.cancelOffer(Arrays.asList(marketListing.getId()));

        return new BaseSaleForceResponse("Ok", "Ok");
    }
    //endregion

    //region Shared
    private Inventory findInventorySalesforce(String salesforceId) throws ExceptionResponse {
        Inventory inventory = inventoryRepository.findLastSalesforceInventory(salesforceId, null);
        if (inventory == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    INVENTORY_OF_SALESFORCE_NOT_FOUND,
                    HttpStatus.NOT_FOUND
            );
        }

        return inventory;
    }

    private InventoryLocationShipping updateLocation(
            InventoryLocationShipping locationShipping,
            String city, String stateRaw, String zipCode, String countryRaw
    ) {
        String fullLocation = city + " "
                + stateRaw + " "
                + zipCode + " "
                + countryRaw;
        GoogleServiceResponse googleResponse = authManager.getMapGoogleSerive(
                IntegratedServices.GOOGLE_SERVICE_CATEGORY_INFO, fullLocation);
        AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
        AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
        AddressComponent county = GoogleServiceUtils.getAddressLevel2(googleResponse);
        if (country != null) {
            locationShipping.setCountryName(country.getLongName());
            locationShipping.setCountryCode(country.getShortName());
        }
        if (state != null) {
            locationShipping.setStateName(state.getLongName());
            locationShipping.setStateCode(state.getShortName());
        }
        if (county != null) {
            locationShipping.setCounty(county.getLongName());
        }
        try {
            if (googleResponse.getResults().size() > 0) {
                GeometryLocation geometryLocation = googleResponse.getResults().get(0).getGeometry().getLocation();
                locationShipping.setLatitude(geometryLocation.getLat());
                locationShipping.setLongitude(geometryLocation.getLng());
            }
        } catch (NullPointerException e) {
            //no geometry location from google map
        }
        locationShipping.setCityName(city);
        locationShipping.setZipCode(zipCode);

        return locationShipping;
    }

    private CompRequest mapInvSizeToFrameSize(String inventorySize, List<SaleforceInvCompRequest> sfCompRequest) {
        // map inv size to frame size component, skip validating the value
        if (!StringUtils.isBlank(inventorySize)) {
            boolean existedFrameSize = false;
            if (sfCompRequest != null) {
                SaleforceInvCompRequest sfComp = CommonUtils.findObject(
                        sfCompRequest,
                        c -> c.getCompTypeName().equalsIgnoreCase(ValueCommons.INVENTORY_FRAME_SIZE_NAME)
                );
                if (sfComp != null) {
                    existedFrameSize = true;
                }
            }
            if (!existedFrameSize) {
                InventoryCompType compType = inventoryCompTypeRepository.findByName(ValueCommons.INVENTORY_FRAME_SIZE_NAME);
                if (compType != null) {
                    CompRequest comp = new CompRequest();
                    comp.setCompId(compType.getId());
                    comp.setValue(inventorySize);

                    return comp;
                }
            }
        }
        return null;
    }

    private CreateListingSalesforceResponse mapResponse(MarketListing marketListing) {
        CreateListingSalesforceResponse response = modelMapper.map(marketListing, CreateListingSalesforceResponse.class);
        response.setWebUrl(IntegratedServices.BASE_WEB_APP + IntegratedServices.MARKET_PLACE +
                IntegratedServices.BUY_NOW + "/" + marketListing.getId());

        return response;
    }
    //endregion
}
