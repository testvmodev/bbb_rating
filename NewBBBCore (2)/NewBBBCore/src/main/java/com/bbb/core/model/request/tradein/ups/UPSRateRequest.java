package com.bbb.core.model.request.tradein.ups;

import com.bbb.core.model.request.tradein.ups.detail.account.UPSSecurity;
import com.bbb.core.model.request.tradein.ups.detail.rate.RateRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSRateRequest {
    @JsonProperty("UPSSecurity")
    private UPSSecurity upsSecurity;
    @JsonProperty("RateRequest")
    private RateRequest rateRequest;

    public UPSSecurity getUpsSecurity() {
        return upsSecurity;
    }

    public void setUpsSecurity(UPSSecurity upsSecurity) {
        this.upsSecurity = upsSecurity;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }
}
