package com.bbb.core.model.response.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Money {
    @JsonProperty("CurrencyCode")
    private String currencyCode;
    @JsonProperty("MonetaryValue")
    private String monetaryValue;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getMonetaryValue() {
        return monetaryValue;
    }
}
