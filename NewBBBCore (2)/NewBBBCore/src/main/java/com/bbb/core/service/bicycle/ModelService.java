package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.model.ModelUpdateRequest;
import com.bbb.core.model.request.sort.BicycleModelSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ModelService {
    Object getModels(List<Long> brandIds, String nameSearch, BicycleModelSortField sortField, Sort sortType , Pageable pageable)
            throws ExceptionResponse;

    Object getModelDetail(long modelId) throws ExceptionResponse;

    Object createModel(ModelCreateRequest model) throws ExceptionResponse;

    Object deleteModel(long id) throws ExceptionResponse;

    Object updateBrand(long modelId, ModelUpdateRequest model) throws ExceptionResponse;

    Object getModelsFromBrandYear(Long brandId, Long yearId) throws ExceptionResponse;

    Object getAllModelsFromBrand(long brandId) throws ExceptionResponse;
}
