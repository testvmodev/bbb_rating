package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.offer.UserBidResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserBidResponseRepository extends JpaRepository<UserBidResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            ":userId AS requested_user_id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "auction.id AS auction_id, " +
            "inventory_auction.min AS min_price, " +
            "inventory_auction.created_time AS created_time, " +
            "auction.name AS auction_name, " +
            "max_offer.highest_price AS current_highest_bid, " +
            "DATEDIFF_BIG(MILLISECOND, GETDATE(), auction.end_date) AS duration, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "inventory_auction.winner_id AS winner_id, " +

            "inventory.id inventory_id, " +
            "inventory.status AS status, " +
            "inventory.stage AS stage, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.name AS inventory_name, " +
            "inventory_type.id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.condition AS condition " +

            "FROM inventory_auction " +
            "JOIN (" +
                "SELECT inventory_auction_id " +
                "FROM offer " +
                "WHERE offer.buyer_id = :userId " +
                "GROUP BY offer.inventory_auction_id" +
            ") AS bid_inv " +
            "ON bid_inv.inventory_auction_id = inventory_auction.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN (" +
                "SELECT MAX(offer.offer_price) AS highest_price, " +
                "inventory_auction_id " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id" +
            ") AS max_offer " +
            "ON inventory_auction.id = max_offer.inventory_auction_id " +

            "WHERE inventory_auction.is_delete = 0 " +
            "ORDER BY inventory_auction.id " +
            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY "
    )
    List<UserBidResponse> findAllByUser(
            @Param("userId") String userId,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(inventory_auction.id) " +
            "FROM inventory_auction " +
            "JOIN (" +
                "SELECT inventory_auction_id " +
                "FROM offer " +
                "WHERE offer.buyer_id = :userId " +
                "GROUP BY offer.inventory_auction_id" +
            ") AS bid_inv " +
            "ON bid_inv.inventory_auction_id = inventory_auction.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "LEFT JOIN (" +
                "SELECT MAX(offer.offer_price) AS highest_price," +
                "inventory_auction_id " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id" +
            ") AS max_offer " +
            "ON inventory_auction.id = max_offer.inventory_auction_id " +

            "WHERE inventory_auction.is_delete = 0 "
    )
    int countAllByUser(
            @Param("userId") String userId
    );
}
