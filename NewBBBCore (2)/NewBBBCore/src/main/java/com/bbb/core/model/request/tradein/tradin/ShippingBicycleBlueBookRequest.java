package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.http.HttpStatus;

public class ShippingBicycleBlueBookRequest implements MessageResponses {
    private Integer length;
    private Integer width;
    private Integer height;
    private Float weight;
    private String fromAddress;
    private String fromCity;
    private String fromState;
    private String fromZipCode;
    private Boolean isScheduled;
    private LocalDate scheduleDate;


    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getFromZipCode() {
        return fromZipCode;
    }

    public void setFromZipCode(String fromZipCode) {
        this.fromZipCode = fromZipCode;
    }

    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public boolean checkValid() throws ExceptionResponse {
        if (length == null || length <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, LENGTH_MUST_IS_NOT_EMPTY_AND_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (width == null || width <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, WIDTH_MUST_IS_NOT_EMPTY_AND_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (height == null || height <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, HEIGHT_MUST_IS_NOT_EMPTY_AND_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (weight == null || weight <= 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, WEIGHT_MUST_IS_NOT_EMPTY_AND_POSITIVE),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(fromAddress)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, FROM_ADDRESS_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(fromCity)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, FROM_CITY_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(fromState)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, FROM_STATE_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(fromZipCode)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, FROM_ZIP_CODE_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        fromZipCode = fromZipCode.trim();

        if (isScheduled != null && isScheduled) {
            if (scheduleDate == null) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        SCHEDULE_DATE_MUST_NOT_EMPTY,
                        HttpStatus.BAD_REQUEST
                );
            }
        }

//        if (carrierType == null) {
//            throw new ExceptionResponse(
//                    new ObjectError(ObjectError.ERROR_NOT_FOUND, CARRIER_TYPE_MUST_IS_NOT_EMPTY_AND_POSITIVE),
//                    HttpStatus.BAD_REQUEST
//            );
//        }
        return true;
    }
}
