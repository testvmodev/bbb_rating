package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInReasonDecline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInReasonDeclineRepository extends JpaRepository<TradeInReasonDecline, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_reason_decline " +
            "WHERE id = ?1")
    TradeInReasonDecline findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_reason_decline")
    List<TradeInReasonDecline> findAll();
}
