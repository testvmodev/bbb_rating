package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Dimensions {
    @JacksonXmlProperty(localName = "Length")
    private int length;
    @JacksonXmlProperty(localName = "Width")
    private int width;
    @JacksonXmlProperty(localName = "Height")
    private int height;
    @JacksonXmlProperty(localName = "Units")
    private String units;

    public Dimensions() {}

    public Dimensions(int length, int width, int height, String units) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.units = units;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
