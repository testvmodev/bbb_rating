package com.bbb.core.common.utils.s3;

import java.net.URL;

public class S3UploadFromUrl<T> {
    private String pre;
    private String folder;
    private URL url;
    private T otherData;

    public S3UploadFromUrl(String pre, String folder, URL url) {
        this(pre, folder, url, null);
    }

    public S3UploadFromUrl(String pre, String folder, URL url, T otherData) {
        this.pre = pre;
        this.folder = folder;
        this.url = url;
        this.otherData = otherData;
    }

    public String getPre() {
        return pre;
    }

    public void setPre(String pre) {
        this.pre = pre;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public T getOtherData() {
        return otherData;
    }

    public void setOtherData(T otherData) {
        this.otherData = otherData;
    }
}
