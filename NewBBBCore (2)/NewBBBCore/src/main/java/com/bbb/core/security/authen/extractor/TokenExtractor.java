package com.bbb.core.security.authen.extractor;

public interface TokenExtractor {
    String extract(String payload);
}
