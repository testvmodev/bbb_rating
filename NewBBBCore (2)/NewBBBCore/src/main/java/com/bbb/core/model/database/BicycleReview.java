package com.bbb.core.model.database;


public class BicycleReview {

  private long id;
  private String review;
  private java.sql.Timestamp reviewTime;
  private String reviewUrl;
  private long sourceId;
  private long column;
  private long bicycleId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }


  public java.sql.Timestamp getReviewTime() {
    return reviewTime;
  }

  public void setReviewTime(java.sql.Timestamp reviewTime) {
    this.reviewTime = reviewTime;
  }


  public String getReviewUrl() {
    return reviewUrl;
  }

  public void setReviewUrl(String reviewUrl) {
    this.reviewUrl = reviewUrl;
  }


  public long getSourceId() {
    return sourceId;
  }

  public void setSourceId(long sourceId) {
    this.sourceId = sourceId;
  }


  public long getColumn() {
    return column;
  }

  public void setColumn(long column) {
    this.column = column;
  }


  public long getBicycleId() {
    return bicycleId;
  }

  public void setBicycleId(long bicycleId) {
    this.bicycleId = bicycleId;
  }

}
