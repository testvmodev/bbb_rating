package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.brand.BrandUpdateRequest;
import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface BrandService {
    Object getBrands(String nameSearch, Sort sortType, BicycleBrandSortField sortField, Pageable pageable) throws ExceptionResponse;

    Object createBrand(BrandCreateRequest brand) throws ExceptionResponse;

    Object deleteBrand(long id) throws ExceptionResponse;

    Object getBrandDetail(long brandId) throws ExceptionResponse;

    Object updateBrand(long brandId, BrandUpdateRequest brand) throws ExceptionResponse;

    Object deleteLogoBrand(long id) throws ExceptionResponse;

    Object getAllBrands() throws ExceptionResponse;
}
