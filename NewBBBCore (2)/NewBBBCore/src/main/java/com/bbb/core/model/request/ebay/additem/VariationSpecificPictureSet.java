package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class VariationSpecificPictureSet {
    @JacksonXmlProperty(localName = "VariationSpecificValue")
    private String variationSpecificValue;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "PictureURL")
    private List<String> pictureURL;

    public String getVariationSpecificValue() {
        return variationSpecificValue;
    }

    public void setVariationSpecificValue(String variationSpecificValue) {
        this.variationSpecificValue = variationSpecificValue;
    }

    public List<String> getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(List<String> pictureURL) {
        this.pictureURL = pictureURL;
    }
}
