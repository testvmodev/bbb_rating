package com.bbb.core.repository.bicyclemodel.out;

import com.bbb.core.model.response.ContentDoubleLongId;
import com.bbb.core.model.response.model.family.ModelFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface ModelFamilyRepository extends JpaRepository<ModelFamily, ContentDoubleLongId> {
    @Query(nativeQuery = true, value = "SELECT " +
            "model_as_id.model_id AS first_id,model_as_id.year_id AS second_id, " +
            "bicycle_model.name AS model_name, bicycle_year.name AS year_name " +
            "FROM " +
            "(SELECT DISTINCT bicycle.model_id, bicycle.year_id FROM bicycle WHERE " +
            "bicycle.brand_id = :brandId AND bicycle.is_delete = 0 AND bicycle.active = 1) AS model_as_id " +
            "JOIN bicycle_model ON model_as_id.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON model_as_id.year_id = bicycle_year.name " +
            "WHERE bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1"
    )
    List<ModelFamily> findAllModelFamilyId(
            @Param(value = "brandId") long brandId
    );
}
