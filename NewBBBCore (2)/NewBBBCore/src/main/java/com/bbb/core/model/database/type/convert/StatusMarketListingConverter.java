package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusMarketListing;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusMarketListingConverter implements AttributeConverter<StatusMarketListing, String> {
    @Override
    public String convertToDatabaseColumn(StatusMarketListing statusMarketListing) {
        if (statusMarketListing == null) {
            return null;
        }
        return statusMarketListing.getValue();
    }

    @Override
    public StatusMarketListing convertToEntityAttribute(String s) {
        for (StatusMarketListing value : StatusMarketListing.values()) {
            if (value.getValue().equals(s)) {
                return value;
            }
        }
        return null;
    }
}
