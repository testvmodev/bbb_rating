package com.bbb.core.controller.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.MarketPlaceConfigCreateRequest;
import com.bbb.core.service.market.MarketPlaceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MarketPlaceConfigController {
    @Autowired
    private MarketPlaceConfigService service;

    @GetMapping(Constants.URL_MARKET_PLACE_CONFIGS)
    public ResponseEntity getAllMarketPlaceConfig() {
        return ResponseEntity.ok(service.getAllMarketPlaceConfig());
    }

    @GetMapping(Constants.URL_MARKET_PLACE_CONFIG)
    public ResponseEntity getMarketPlaceConfigDetail(@PathVariable(value = Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.getMarketPlaceConfigDetail(id));
    }

    @PostMapping(Constants.URL_MARKET_PLACE_CONFIGS)
    public ResponseEntity postMarketPlaceConfig(
            @RequestParam(value = "marketPlaceId") long marketPlaceId,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "ebayAppId", required = false) String ebayAppId,
            @RequestParam(value = "ebayCertId", required = false) String ebayCertId,
            @RequestParam(value = "ebayDevId", required = false) String ebayDevId,
            @RequestParam(value = "ebayToken", required = false) String ebayToken,
            @RequestParam(value = "isEbayApiSandbox", required = false) Boolean isEbayApiSandbox
    ) throws ExceptionResponse {
        MarketPlaceConfigCreateRequest request = new MarketPlaceConfigCreateRequest();
        request.setName(name);
        request.setMarketPlaceId(marketPlaceId);
        request.setEbayAppId(ebayAppId);
        request.setEbayCertId(ebayCertId);
        request.setEbayDevId(ebayDevId);
        request.setEbayToken(ebayToken);
        request.setEbayApiSandbox(isEbayApiSandbox);

        return ResponseEntity.ok(service.postMarketPlaceConfig(request));
    }

    @PutMapping(Constants.URL_MARKET_PLACE_CONFIG)
    public ResponseEntity updateMarketPlaceConfig(
            @PathVariable(value = Constants.ID) long id,
            @RequestParam(value = "marketPlaceId", required = false) Long marketPlaceId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "ebayAppId", required = false) String ebayAppId,
            @RequestParam(value = "ebayCertId", required = false) String ebayCertId,
            @RequestParam(value = "ebayDevId", required = false) String ebayDevId,
            @RequestParam(value = "ebayToken", required = false) String ebayToken,
            @RequestParam(value = "isEbayApiSandbox", required = false) Boolean isEbayApiSandbox
    ) throws ExceptionResponse {
        MarketPlaceConfigCreateRequest request = new MarketPlaceConfigCreateRequest();
        request.setMarketPlaceId(marketPlaceId);
        request.setName(name);
        request.setEbayAppId(ebayAppId);
        request.setEbayCertId(ebayCertId);
        request.setEbayDevId(ebayDevId);
        request.setEbayToken(ebayToken);
        request.setEbayApiSandbox(isEbayApiSandbox);

        return ResponseEntity.ok(service.updateMarketPlaceConfig(id, request));
    }

    @DeleteMapping(Constants.URL_MARKET_PLACE_CONFIG)
    public ResponseEntity deleteMarketPlaceConfig(@PathVariable(value = Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteMarketPlaceConfig(id));
    }
}
