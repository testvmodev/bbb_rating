package com.bbb.core.model.response.market.home.recentview;

import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RecentViewInventoryAuctionResponse extends RecentViewResponse {
    @Id
    private long inventoryAuctionId;
    private Float minBid;
    private Long numberBids;
    private Float currentHighestBid;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private long duration;

    public long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public Float getMinBid() {
        return minBid;
    }

    public void setMinBid(Float minBid) {
        this.minBid = minBid;
    }

    public Long getNumberBids() {
        return numberBids;
    }

    public void setNumberBids(Long numberBids) {
        this.numberBids = numberBids;
    }

    public Float getCurrentHighestBid() {
        return currentHighestBid;
    }

    public void setCurrentHighestBid(Float currentHighestBid) {
        this.currentHighestBid = currentHighestBid;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
