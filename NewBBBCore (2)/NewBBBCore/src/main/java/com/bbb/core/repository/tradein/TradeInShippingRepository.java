package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInShipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface TradeInShippingRepository extends JpaRepository<TradeInShipping, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "* FROM trade_in_shipping WHERE trade_in_shipping.trade_in_id = :tradeInId")
    TradeInShipping findOneByTradeInId(
            @Param(value = "tradeInId") long tradeInId
    );
}
