package com.bbb.core.model.response.tradein.ups.detail.track;

import com.bbb.core.model.response.tradein.ups.detail.Status;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Activity {
    @JsonProperty("ActivityLocation")
    private ActivityLocation activityLocation;
    @JsonProperty("Status")
    private Status status;
    @JsonProperty("Date")
    private String date;
    @JsonProperty("Time")
    private String time;

    public ActivityLocation getActivityLocation() {
        return activityLocation;
    }

    public void setActivityLocation(ActivityLocation activityLocation) {
        this.activityLocation = activityLocation;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
