package com.bbb.core.repository.market;

import com.bbb.core.model.database.MarketPlaceConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketPlaceConfigRepository extends JpaRepository<MarketPlaceConfig, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM market_place_config " +
            "WHERE id = ?1")
    MarketPlaceConfig findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM market_place_config " +
            "WHERE is_delete = 0")
    List<MarketPlaceConfig> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM market_place_config " +
            "WHERE market_place_config.is_delete = 0 AND " +
            "market_place_config.market_place_id = :marketPlaceId AND " +
            "market_place_config.is_api_sandbox = :isSandbox")
    List<MarketPlaceConfig> findAllByMarketPlaceId(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "isSandbox") Boolean isSandbox
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM market_place_config " +
            "WHERE is_delete = 0 AND market_place_id = :marketPlaceId AND " +
            "market_place_config.is_api_sandbox = :isSandbox")
    MarketPlaceConfig findOneByMarketPlaceId(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "isSandbox") Boolean isSandbox);

    @Query(nativeQuery = true, value = "SELECT * FROM market_place_config " +
            "WHERE market_place_config.id IN :marketPlaceConfigId AND " +
            "market_place_config.is_delete = 0 AND " +
            "market_place_config.is_api_sandbox = :isSandbox")
    List<MarketPlaceConfig> findAllByIds(
            @Param(value = "marketPlaceConfigId") List<Long> marketPlaceConfigId,
            @Param(value = "isSandbox") boolean isSandbox
    );



}
