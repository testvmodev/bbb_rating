package com.bbb.core.model.response.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ModelFromBrandYear {

    private long id;
    private String name;
    @Id
    private long bicycleId;
    private String bicycleName;
    private String bicycleImageDefault;
    private long yearId;
    private String yearName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleImageDefault() {
        return bicycleImageDefault;
    }

    public void setBicycleImageDefault(String bicycleImageDefault) {
        this.bicycleImageDefault = bicycleImageDefault;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }
}
