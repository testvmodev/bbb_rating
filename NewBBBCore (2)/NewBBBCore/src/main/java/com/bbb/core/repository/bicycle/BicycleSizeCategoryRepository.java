package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.BicycleSizeCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleSizeCategoryRepository extends JpaRepository<BicycleSizeCategory, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM bicycle_size_category"
    )
    List<BicycleSizeCategory> findAllCategories();
}
