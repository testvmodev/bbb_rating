package com.bbb.core.model.otherservice.request.mail.offer.rejectedaccepted;

import com.bbb.core.model.otherservice.request.mail.offer.content.ListingMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentOfferRejectAccepted {
    private String reason;
    private UserMailRequest user;
    private ListingMailRequest listing;
    @JsonProperty(value = "base_path")
    private String basePath;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingMailRequest getListing() {
        return listing;
    }

    public void setListing(ListingMailRequest listing) {
        this.listing = listing;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
