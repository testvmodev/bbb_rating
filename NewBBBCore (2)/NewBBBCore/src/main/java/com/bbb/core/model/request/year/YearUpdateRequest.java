package com.bbb.core.model.request.year;

public class YearUpdateRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
