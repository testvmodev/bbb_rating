package com.bbb.core.model.request.sort;

public enum  OfferBidFieldSort {
    CREATED_TIME,BID_PRICE, STATUS_BID, INVENTORY_ID
}
