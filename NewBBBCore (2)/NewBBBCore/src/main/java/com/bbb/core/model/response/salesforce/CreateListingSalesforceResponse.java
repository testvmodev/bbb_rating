package com.bbb.core.model.response.salesforce;

import com.bbb.core.model.database.MarketListing;

public class CreateListingSalesforceResponse extends MarketListing {
    private String webUrl;

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }
}
