package com.bbb.core.model.request.market.marketlisting.tosale;

import com.bbb.core.model.database.type.MarketListingFilterTypeTable;
import com.bbb.core.model.response.common.OperatorMarketListingFilter;

import javax.validation.constraints.NotNull;
import java.util.List;

public class MarketListingInvToSalRequest {
    @NotNull
    private Long id;
    private List<String> values;
    private Float start;
    private Float end;
    @NotNull
    private OperatorMarketListingFilter operator;
    @NotNull
    private MarketListingFilterTypeTable typeTable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public Float getStart() {
        return start;
    }

    public void setStart(Float start) {
        this.start = start;
    }

    public Float getEnd() {
        return end;
    }

    public void setEnd(Float end) {
        this.end = end;
    }

    public OperatorMarketListingFilter getOperator() {
        return operator;
    }

    public void setOperator(OperatorMarketListingFilter operator) {
        this.operator = operator;
    }

    public MarketListingFilterTypeTable getTypeTable() {
        return typeTable;
    }

    public void setTypeTable(MarketListingFilterTypeTable typeTable) {
        this.typeTable = typeTable;
    }
}
