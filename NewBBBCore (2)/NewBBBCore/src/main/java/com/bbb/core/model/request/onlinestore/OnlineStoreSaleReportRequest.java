package com.bbb.core.model.request.onlinestore;

import org.joda.time.YearMonth;

public class OnlineStoreSaleReportRequest {
    private YearMonth month;

    public YearMonth getMonth() {
        return month;
    }

    public void setMonth(YearMonth month) {
        this.month = month;
    }
}
