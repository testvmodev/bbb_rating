package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class Seller {
    @JacksonXmlProperty(localName = "AboutMePage")
    private Boolean aboutMePage;
    @JacksonXmlProperty(localName = "Email")
    private String email;
    @JacksonXmlProperty(localName = "FeedbackScore")
    private Integer feedbackScore;
    @JacksonXmlProperty(localName = "PositiveFeedbackPercent")
    private Float positiveFeedbackPercent;
    @JacksonXmlProperty(localName = "FeedbackPrivate")
    private Boolean feedbackPrivate;
    @JacksonXmlProperty(localName = "FeedbackRatingStar")
    private String feedbackRatingStar;
    @JacksonXmlProperty(localName = "IDVerified")
    private Boolean iDVerified;
    @JacksonXmlProperty(localName = "eBayGoodStanding")
    private Boolean eBayGoodStanding;
    @JacksonXmlProperty(localName = "NewUser")
    private String newUser;
    @JacksonXmlProperty(localName = "RegistrationDate")
    private LocalDateTime registrationDate;
    @JacksonXmlProperty(localName = "Site")
    private String site;
    @JacksonXmlProperty(localName = "Status")
    private String Status;
    @JacksonXmlProperty(localName = "UserID")
    private String userID;
    @JacksonXmlProperty(localName = "UserIDChanged")
    private Boolean userIDChanged;
    @JacksonXmlProperty(localName = "UserIDLastChanged")
    private LocalDateTime userIDLastChanged;
    @JacksonXmlProperty(localName = "VATStatus")
    private String vATStatus;
    @JacksonXmlProperty(localName = "SellerInfo")
    private SellerInfo sellerInfo;
    @JacksonXmlProperty(localName = "MotorsDealer")
    private Boolean motorsDealer;
//    @JacksonXmlProperty(localName = "SellingStatus")


    public Boolean getAboutMePage() {
        return aboutMePage;
    }

    public void setAboutMePage(Boolean aboutMePage) {
        this.aboutMePage = aboutMePage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFeedbackScore() {
        return feedbackScore;
    }

    public void setFeedbackScore(Integer feedbackScore) {
        this.feedbackScore = feedbackScore;
    }

    public Float getPositiveFeedbackPercent() {
        return positiveFeedbackPercent;
    }

    public void setPositiveFeedbackPercent(Float positiveFeedbackPercent) {
        this.positiveFeedbackPercent = positiveFeedbackPercent;
    }

    public Boolean getFeedbackPrivate() {
        return feedbackPrivate;
    }

    public void setFeedbackPrivate(Boolean feedbackPrivate) {
        this.feedbackPrivate = feedbackPrivate;
    }

    public String getFeedbackRatingStar() {
        return feedbackRatingStar;
    }

    public void setFeedbackRatingStar(String feedbackRatingStar) {
        this.feedbackRatingStar = feedbackRatingStar;
    }

    public Boolean getiDVerified() {
        return iDVerified;
    }

    public void setiDVerified(Boolean iDVerified) {
        this.iDVerified = iDVerified;
    }

    public Boolean geteBayGoodStanding() {
        return eBayGoodStanding;
    }

    public void seteBayGoodStanding(Boolean eBayGoodStanding) {
        this.eBayGoodStanding = eBayGoodStanding;
    }

    public String getNewUser() {
        return newUser;
    }

    public void setNewUser(String newUser) {
        this.newUser = newUser;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Boolean getUserIDChanged() {
        return userIDChanged;
    }

    public void setUserIDChanged(Boolean userIDChanged) {
        this.userIDChanged = userIDChanged;
    }

    public LocalDateTime getUserIDLastChanged() {
        return userIDLastChanged;
    }

    public void setUserIDLastChanged(LocalDateTime userIDLastChanged) {
        this.userIDLastChanged = userIDLastChanged;
    }

    public String getvATStatus() {
        return vATStatus;
    }

    public void setvATStatus(String vATStatus) {
        this.vATStatus = vATStatus;
    }

    public SellerInfo getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(SellerInfo sellerInfo) {
        this.sellerInfo = sellerInfo;
    }

    public Boolean getMotorsDealer() {
        return motorsDealer;
    }

    public void setMotorsDealer(Boolean motorsDealer) {
        this.motorsDealer = motorsDealer;
    }
}
