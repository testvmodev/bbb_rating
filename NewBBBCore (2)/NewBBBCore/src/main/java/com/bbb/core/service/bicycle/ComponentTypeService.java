package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.component.ComponentTypeCreateRequest;
import com.bbb.core.model.request.component.ComponentTypeGetRequest;
import com.bbb.core.model.request.component.ComponentTypeUpdateRequest;
import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;

public interface ComponentTypeService {
//    Object importComponentType();

    Object getComponentTypes(ComponentTypeGetRequest request) throws ExceptionResponse;

    Object getComponentType(long typeId) throws ExceptionResponse;

    Object createComponentType(ComponentTypeCreateRequest request) throws ExceptionResponse;

    Object updateComponentType(long typeId, ComponentTypeUpdateRequest request) throws ExceptionResponse;

    Object deleteComponentType(long typeId)throws ExceptionResponse;
}
