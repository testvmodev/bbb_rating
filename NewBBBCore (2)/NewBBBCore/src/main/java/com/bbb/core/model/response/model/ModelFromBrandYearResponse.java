package com.bbb.core.model.response.model;

import java.util.List;

public class ModelFromBrandYearResponse {
    private long id;
    private String name;
    private List<BicycleModelResponse> bicycle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BicycleModelResponse> getBicycle() {
        return bicycle;
    }

    public void setBicycle(List<BicycleModelResponse> bicycle) {
        this.bicycle = bicycle;
    }

}
