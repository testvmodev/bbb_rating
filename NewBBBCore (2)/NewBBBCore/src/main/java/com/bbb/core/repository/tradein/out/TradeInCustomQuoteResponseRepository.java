package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import com.bbb.core.model.request.sort.CustomQuoteSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.tradein.customquote.TradeInCustomQuoteResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInCustomQuoteResponseRepository extends JpaRepository<TradeInCustomQuoteResponse, Long> {
//    @Id
//    private long customQuoteId;
//    private long tradeInId;
//    private String employeeLocation;
//    private LocalDateTime createdTime;
//    private String employeeName;
//    @Transient
//    private String titleBicycle;
//    private String brandName;
//    private String modelName;
//    private String yearName;
//    private StatusTradeInCustomQuote status;
    @Query(nativeQuery = true, value = "SELECT " +
            "trade_in_custom_quote.id AS custom_quote_id, " +
            "trade_in.id AS trade_in_id, " +
            "trade_in_custom_quote.employee_location AS employee_location, " +
            "trade_in_custom_quote.created_time AS created_time, " +
            "trade_in_custom_quote.employee_name AS employee_name, " +
            "trade_in_custom_quote.brand_name AS brand_name, " +
            "trade_in_custom_quote.model_name AS model_name, " +
            "trade_in_custom_quote.year_name AS year_name, " +
            "trade_in_custom_quote.status AS status, " +
            "trade_in_custom_quote.e_bike_mileage AS e_bike_mileage, " +
            "trade_in_custom_quote.e_bike_hours AS e_bike_hours, " +
            "trade_in_custom_quote.is_clean AS is_clean, " +
            "title.title_bicycle AS title_bicycle, " +
            "trade_in.partner_id AS partner_id, " +
            "trade_in_custom_quote.user_created_id AS user_created_id, " +
            "trade_in.last_update AS last_update " +

            "FROM trade_in_custom_quote " +
            "JOIN trade_in ON trade_in_custom_quote.id = trade_in.custom_quote_id " +
            "CROSS APPLY (" +
            "SELECT ISNULL(trade_in_custom_quote.year_name + ' ', '') + ISNULL(trade_in_custom_quote.brand_name + ' ', '') + ISNULL(trade_in_custom_quote.model_name, '') AS title_bicycle" +
            ") AS title " +

            "WHERE " +
            "(:partnerId IS NULL OR trade_in.partner_id = :partnerId) " +
            "AND (:isArchived IS NULL OR trade_in.is_archived = :isArchived) " +
            "AND (:isAdminArchived IS NULL OR trade_in.is_admin_archived = :isAdminArchived) " +
            "AND (:isIncomplete IS NULL " +
                "OR (:isIncomplete = 1 AND trade_in_custom_quote.status = :#{T(com.bbb.core.model.database.type.StatusTradeInCustomQuote).INCOMPLETE_DETAILS.getValue()})" +
                "OR (:isIncomplete = 0 AND trade_in_custom_quote.status != :#{T(com.bbb.core.model.database.type.StatusTradeInCustomQuote).INCOMPLETE_DETAILS.getValue()})) " +
            "AND (:status IS NULL OR trade_in_custom_quote.status = :status) " +
            "AND (:content IS NULL OR " +
            "title.title_bicycle LIKE :#{'%' + #content + '%'}) " +

            "ORDER BY " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).CREATED_TIME} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.created_time END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).CREATED_TIME} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.created_time END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).LAST_UPDATE} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.last_update END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).LAST_UPDATE} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.last_update END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).TITLE} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN title.title_bicycle END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).TITLE} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN title.title_bicycle END ASC, " +

            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).STATUS} " +
            "AND :#{#sortType} = :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.status END DESC, " +
            "CASE WHEN :#{#sortField} = :#{T(com.bbb.core.model.request.sort.CustomQuoteSortField).STATUS} " +
            "AND :#{#sortType} != :#{T(com.bbb.core.model.request.sort.Sort).DESC} THEN trade_in_custom_quote.status END ASC " +

            "OFFSET :offset  ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<TradeInCustomQuoteResponse> findAllSorted(
            @Param(value = "partnerId") String partnerId,
            @Param(value = "isArchived") Boolean isArchived,
            @Param(value = "isAdminArchived") Boolean isAdminArchived,
            @Param("isIncomplete") Boolean isIncomplete,
            @Param(value = "status") String status,
            @Param(value = "content") String content,
            @Param("sortField") CustomQuoteSortField sortField,
            @Param("sortType") Sort sortType,
            @Param(value = "offset") long offset,
            @Param(value = "size") int pageSize
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(trade_in_custom_quote.id) " +
            "FROM " +
            "trade_in JOIN trade_in_custom_quote ON trade_in.custom_quote_id = trade_in_custom_quote.id " +
            "WHERE " +
            "(:partnerId IS NULL OR trade_in.partner_id = :partnerId) " +
            "AND (:isArchived IS NULL OR trade_in.is_archived = :isArchived) " +
            "AND (:isAdminArchived IS NULL OR trade_in.is_admin_archived = :isAdminArchived) " +
            "AND (:isIncomplete IS NULL " +
            "OR (:isIncomplete = 1 AND trade_in_custom_quote.status = :#{T(com.bbb.core.model.database.type.StatusTradeInCustomQuote).INCOMPLETE_DETAILS.getValue()})" +
            "OR (:isIncomplete = 0 AND trade_in_custom_quote.status != :#{T(com.bbb.core.model.database.type.StatusTradeInCustomQuote).INCOMPLETE_DETAILS.getValue()})) " +
            "AND (:status IS NULL OR trade_in_custom_quote.status = :status)" +
            "AND (:content IS NULL OR " +
            "ISNULL(trade_in_custom_quote.year_name + ' ', '') + ISNULL(trade_in_custom_quote.brand_name + ' ', '') + ISNULL(trade_in_custom_quote.model_name, '') LIKE :#{'%' + #content + '%'}) "
    )
    int getTotalNumber(
            @Param(value = "partnerId") String partnerId,
            @Param(value = "isArchived") Boolean isArchived,
            @Param(value = "isAdminArchived") Boolean isAdminArchived,
            @Param("isIncomplete") Boolean isIncomplete,
            @Param(value = "content") String content,
            @Param(value = "status") String status
    );
}
