package com.bbb.core.model.response.tradein.ups.detail.fault;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FaultErrorDetail {
    @JsonProperty(value = "Severity")
    private String severity;
    @JsonProperty(value = "PrimaryErrorCode")
    private FaultPrimaryErrorCode primaryErrorCode;

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public FaultPrimaryErrorCode getPrimaryErrorCode() {
        return primaryErrorCode;
    }

    public void setPrimaryErrorCode(FaultPrimaryErrorCode primaryErrorCode) {
        this.primaryErrorCode = primaryErrorCode;
    }
}
