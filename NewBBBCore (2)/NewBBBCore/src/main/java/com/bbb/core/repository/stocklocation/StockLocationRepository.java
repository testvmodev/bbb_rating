package com.bbb.core.repository.stocklocation;

import com.bbb.core.model.database.StockLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StockLocationRepository extends JpaRepository<StockLocation, Long> {
    @Query(nativeQuery = true, value = "SELECT COUNT(stock_location.id) FROM stock_location WHERE stock_location.id = :stockLocationId")
    int exist(
            @Param(value = "stockLocationId") long stockLocationId
    );
}
