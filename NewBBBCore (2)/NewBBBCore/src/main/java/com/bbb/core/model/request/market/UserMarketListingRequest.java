package com.bbb.core.model.request.market;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class UserMarketListingRequest {
    private String userId;
    private List<StatusMarketListing> marketListingStatuses;
    private MyListingSortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<StatusMarketListing> getMarketListingStatuses() {
        return marketListingStatuses;
    }

    public void setMarketListingStatuses(List<StatusMarketListing> marketListingStatuses) {
        this.marketListingStatuses = marketListingStatuses;
    }

    public MyListingSortField getSortField() {
        return sortField;
    }

    public void setSortField(MyListingSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
