package com.bbb.core.manager.market.async;

import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ListingWizardManagerAsync {
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;

//    @Async
//    public void updateWhenDelistedEbayFinish(List<MarketListing> ebays) {
//        MarketToList marketToList = marketToListRepository.findAllIdActiveBBB(isSandbox);
//        if ( marketToList == null ){
//            return;
//        }
//        List<Long> marketListingIds = ebays.stream().map(MarketListing::getId).collect(Collectors.toList());
//        marketListingRepository.updateStatusMarketListing(marketListingIds, StatusMarketListing.DE_LISTED.getValue());
//        List<Long> inventoryIds = ebays.stream().map(MarketListing::getInventoryId).collect(Collectors.toList());
//        List<Integer> inventoryIdsListedBBB =  marketListingRepository.findAllInventoryAtBBBNotListed(inventoryIds, marketToList.getMarketPlaceId());
//        List<Long> inventoryReadyTobeListed = new ArrayList<>();
//        for (Long inventoryId : inventoryIds) {
//            if (CommonUtils.findObject(inventoryIdsListedBBB, o->(o.longValue() == inventoryId)) == null ){
//                inventoryReadyTobeListed.add(inventoryId);
//            }
//        }
//        if ( inventoryReadyTobeListed.size() > 0 ) {
//            inventoryRepository.updateStatusAndStage(inventoryReadyTobeListed, StatusInventory.ACTIVE.getValue(), StageInventory.READY_LISTED.getValue());
//        }
//    }

    @Async
    public void updateWhenInventoryWhenDeListedAsync(List<MarketListing> bbbs, List<MarketListing> ebays) {

        List<Long> inventoryIds = bbbs.stream().map(MarketListing::getInventoryId).collect(Collectors.toList());
        inventoryIds.addAll(
                ebays.stream().map(MarketListing::getInventoryId).collect(Collectors.toList())
        );
        CommonUtils.stream(inventoryIds);
        if (inventoryIds.size() > 0) {
            inventoryRepository.updateStatusAndStageFromInventoryIdMarketListingEnd(
                    inventoryIds,
                    StatusInventory.ACTIVE.getValue(),
                    StageInventory.READY_LISTED.getValue());
        }
    }
}
