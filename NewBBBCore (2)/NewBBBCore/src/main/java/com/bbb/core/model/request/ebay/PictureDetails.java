package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class PictureDetails {
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "PictureURL")
    private List<String> pictureURL;

    public List<String> getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(List<String> pictureURL) {
        this.pictureURL = pictureURL;
    }
}
