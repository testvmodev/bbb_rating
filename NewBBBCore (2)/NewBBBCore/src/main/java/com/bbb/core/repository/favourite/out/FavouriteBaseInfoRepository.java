package com.bbb.core.repository.favourite.out;

import com.bbb.core.model.response.favourite.FavouriteBaseInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavouriteBaseInfoRepository extends JpaRepository<FavouriteBaseInfo, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "favourite.id AS favourite_id, " +
            "favourite.market_listing_id AS market_listing_id, " +
            "favourite.inventory_auction_id AS inventory_auction_id " +
            "FROM favourite " +
            "LEFT JOIN market_listing ON favourite.market_listing_id = market_listing.id " +
            "LEFT JOIN inventory_auction ON favourite.inventory_auction_id = inventory_auction.id " +
            "LEFT JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "(" +
            "market_listing.is_delete = 0 OR " +
            "(DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND "+
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 AND " +
            "inventory_auction.is_inactive = 0) " +
            ") "+
            "ORDER BY favourite.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<FavouriteBaseInfo> findAllFavourite(
            @Param(value = "userId") String userId,
            @Param(value = "now") String now,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(favourite.id) " +
            "FROM favourite " +
            "LEFT JOIN market_listing ON favourite.market_listing_id = market_listing.id " +
            "LEFT JOIN inventory_auction ON favourite.inventory_auction_id = inventory_auction.id " +
            "LEFT JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "(" +
            "market_listing.is_delete = 0 OR " +
            "(DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND "+
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 AND " +
            "inventory_auction.is_inactive = 0) " +
            ") "
    )
    int countAll(
            @Param(value = "userId") String userId,
            @Param(value = "now") String now
    );
}
