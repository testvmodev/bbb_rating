package com.bbb.core.repository.inventory.out;

import com.bbb.core.model.response.inventory.InventoryCompDetailRes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryCompDetailResRepository extends JpaRepository<InventoryCompDetailRes, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_id AS inventory_id," +
            "inventory_comp_detail.inventory_comp_type_id AS  inventory_comp_type_id, " +
            "inventory_comp_type.name AS name, " +
            "inventory_comp_detail.value AS value " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON  inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId")
    List<InventoryCompDetailRes> findAllComp(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_id AS inventory_id," +
            "inventory_comp_detail.inventory_comp_type_id AS  inventory_comp_type_id, " +
            "inventory_comp_type.name AS name, " +
            "inventory_comp_detail.value AS value " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON  inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId AND " +
            "(" +
            "inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME} OR "+
            "inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME} " +
            ")"
    )
    List<InventoryCompDetailRes> findAllCompBrakeTypeAndFrameMaterial(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_comp_detail.inventory_id AS inventory_id," +
            "inventory_comp_detail.inventory_comp_type_id AS  inventory_comp_type_id, " +
            "inventory_comp_type.name AS name, " +
            "inventory_comp_detail.value AS value " +
            "FROM " +
            "inventory_comp_detail JOIN inventory_comp_type ON  inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_detail.inventory_id IN :inventoriesId")
    List<InventoryCompDetailRes> findAllComp(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );
}
