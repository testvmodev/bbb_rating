package com.bbb.core.manager.schedule.jobs.auction;

import com.bbb.core.common.Constants;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.admin.AdminManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.notification.SubscriptionManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.Auction;
import com.bbb.core.model.database.InventoryAuction;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailResponse;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionResponse;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.out.InventoryAuctionResponseRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, forceDisable = false, defaultTimeType = JobTimeType.FIX_DELAY, defaultTimeSeconds = 240)
public class AuctionStartJob extends ScheduleJob {
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private AdminManager adminManager;
    @Autowired
    private InventoryAuctionResponseRepository inventoryAuctionResponseRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private SubscriptionManager subscriptionManager;


    @Override
    public void run() {
        List<Auction> auctions = auctionRepository.findAllAuctionStart();
        if (auctions.size() > 0) {
            List<Long> auctionIds = auctions.stream().map(Auction::getId).collect(Collectors.toList());
            auctions = auctions.stream().peek(au -> {
                au.setStarted(true);
            }).collect(Collectors.toList());
            auctionRepository.saveAll(auctions);

            List<InventoryAuction> inventoryAuctions = inventoryAuctionRepository.findAllInventoryAuctionFromAuctionId(auctionIds);
            if (inventoryAuctions.size() > 0) {
                ListObjResponse<UserDetailResponse> users = adminManager.getActiveWholesaler();
                if (users.getData() != null && users.getData().size() > 0) {
                    List<String> emails = users.getData().stream().map(UserDetailResponse::getEmail)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList());
                    ExecutorService executor = Executors.newFixedThreadPool(4);
                    List<Observable<SendingMailResponse>> obSendMail = new ArrayList<>();
                    for (Auction auction : auctions) {
                        obSendMail.add(
                                createObserverSendMail(auction, inventoryAuctions, emails, executor)
                        );
                    }
                    Observable.zip(obSendMail, result -> {
                        List<SendingMailResponse> sendingMailResponses = new ArrayList<>();
                        for (Object o : result) {
                            if (o != null) {
                                sendingMailResponses.add((SendingMailResponse) o);
                            }
                        }
                        return sendingMailResponses;
                    }).observeOn(
                            Schedulers.newThread()
                    ).subscribe();
                    List<Observable<MessageResponse>> obSubscription = new ArrayList<>();
                    for (InventoryAuction inventoryAuction : inventoryAuctions) {
                        obSubscription.add(createObserverSubscriptionTrigger(
                                inventoryAuction, executor
                        ));
                    }
                    Observable.zip(obSubscription, result -> {
                        List<MessageResponse> subscriptionResponses = new ArrayList<>();
                        for (Object o : result) {
                            if (o != null) {
                                subscriptionResponses.add((MessageResponse) o);
                            }
                        }
                        return subscriptionResponses;
                    }).observeOn(
                            Schedulers.newThread()
                    ).subscribe();
                }
            }
        }
    }

    @Override
    protected void onFail(Throwable throwable) {
        System.out.println("Error received by job");
//        setCustomLogString("Can have custom log of each job");
    }

    private Observable<SendingMailResponse> createObserverSendMail(Auction auction, List<InventoryAuction> inventoryAuctions, List<String> emails, ExecutorService executor) {
        List<Long> inventoryIds = new ArrayList<>();
        for (InventoryAuction inventoryAuction : inventoryAuctions) {
            if (inventoryAuction.getAuctionId() == auction.getId()) {
                inventoryIds.add(inventoryAuction.getInventoryId());
            }
        }
        CommonUtils.stream(inventoryIds);
        List<InventoryAuctionResponse> inventoryAuctionResponses = inventoryAuctionResponseRepository.findAll(
                auction.getId(),
                inventoryIds,
                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate())
        );

        return Observable.create((ObservableOnSubscribe<SendingMailResponse>) su -> {
            try {
                emails.add("duc.nguyen@vmodev.com");
                SendingMailResponse sendingMailResponse = notificationManager.sendMailStartAuction(
                        auction,
                        inventoryAuctionResponses,
                        emails
                );
                su.onNext(
                        sendingMailResponse
                );
            } catch (HttpClientErrorException e) {
                e.printStackTrace();
            }
            su.onComplete();
        }).subscribeOn(Schedulers.from(executor));

    }

    private Observable<MessageResponse> createObserverSubscriptionTrigger(
            InventoryAuction inventoryAuction, ExecutorService executor
    ) {
        return Observable.create((ObservableOnSubscribe<MessageResponse>) su -> {
            try {
                MessageResponse response = subscriptionManager.triggerStartedSync(inventoryAuction);
                su.onNext(response);
            } catch (HttpClientErrorException e) {
                e.printStackTrace();
            }
            su.onComplete();
        }).subscribeOn(Schedulers.from(executor));
    }
}
