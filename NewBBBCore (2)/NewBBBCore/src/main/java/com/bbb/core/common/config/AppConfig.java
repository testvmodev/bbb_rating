package com.bbb.core.common.config;

import com.bbb.core.common.Constants;
import com.bbb.core.common.config.env.Environments;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = Constants.CONFIG_PREFIX)
public class AppConfig {
    private Environments env;
    private Boolean enableServiceLog;
    private Boolean logApiException;
    private EndpointConfig endpoint;
    private EbayConfig ebay;

    public Environments getEnv() {
        return env;
    }

    public void setEnv(Environments env) {
        this.env = env;
    }

    public Boolean getEnableServiceLog() {
        return enableServiceLog;
    }

    public void setEnableServiceLog(Boolean enableServiceLog) {
        this.enableServiceLog = enableServiceLog;
    }

    public Boolean getLogApiException() {
        return logApiException;
    }

    public void setLogApiException(Boolean logApiException) {
        this.logApiException = logApiException;
    }

    public EndpointConfig getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(EndpointConfig endpoint) {
        this.endpoint = endpoint;
    }

    public EbayConfig getEbay() {
        return ebay;
    }

    public void setEbay(EbayConfig ebay) {
        this.ebay = ebay;
    }

    public static class EndpointConfig {
        private String baseApiUrl;
        private String baseCoreApiUrl;
        private String baseValueGuideApiUrl;
        private String coreApiPrefix;
        private String authApiPrefix;
        private String billingApiPrefix;
        private String notifyApiPrefix;
        private String subscriptionApiPrefix;
        private String shipmentApiPrefix;
        private String valueGuideApiPrefix;
        private String baseWebappUrl;

        public String getBaseApiUrl() {
            return baseApiUrl;
        }

        public void setBaseApiUrl(String baseApiUrl) {
            this.baseApiUrl = baseApiUrl;
        }

        public String getBaseCoreApiUrl() {
            return baseCoreApiUrl;
        }

        public void setBaseCoreApiUrl(String baseCoreApiUrl) {
            this.baseCoreApiUrl = baseCoreApiUrl;
        }

        public String getBaseValueGuideApiUrl() {
            return baseValueGuideApiUrl;
        }

        public void setBaseValueGuideApiUrl(String baseValueGuideApiUrl) {
            this.baseValueGuideApiUrl = baseValueGuideApiUrl;
        }

        public String getCoreApiPrefix() {
            return coreApiPrefix;
        }

        public void setCoreApiPrefix(String coreApiPrefix) {
            this.coreApiPrefix = coreApiPrefix;
        }

        public String getAuthApiPrefix() {
            return authApiPrefix;
        }

        public void setAuthApiPrefix(String authApiPrefix) {
            this.authApiPrefix = authApiPrefix;
        }

        public String getBillingApiPrefix() {
            return billingApiPrefix;
        }

        public void setBillingApiPrefix(String billingApiPrefix) {
            this.billingApiPrefix = billingApiPrefix;
        }

        public String getNotifyApiPrefix() {
            return notifyApiPrefix;
        }

        public void setNotifyApiPrefix(String notifyApiPrefix) {
            this.notifyApiPrefix = notifyApiPrefix;
        }

        public String getSubscriptionApiPrefix() {
            return subscriptionApiPrefix;
        }

        public void setSubscriptionApiPrefix(String subscriptionApiPrefix) {
            this.subscriptionApiPrefix = subscriptionApiPrefix;
        }

        public String getShipmentApiPrefix() {
            return shipmentApiPrefix;
        }

        public void setShipmentApiPrefix(String shipmentApiPrefix) {
            this.shipmentApiPrefix = shipmentApiPrefix;
        }

        public String getValueGuideApiPrefix() {
            return valueGuideApiPrefix;
        }

        public void setValueGuideApiPrefix(String valueGuideApiPrefix) {
            this.valueGuideApiPrefix = valueGuideApiPrefix;
        }

        public String getBaseWebappUrl() {
            return baseWebappUrl;
        }

        public void setBaseWebappUrl(String baseWebappUrl) {
            this.baseWebappUrl = baseWebappUrl;
        }
    }

    public static class EbayConfig {
        private String endpoint;
        private boolean isSandbox;

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public boolean isSandbox() {
            return isSandbox;
        }

        public void setSandbox(boolean sandbox) {
            isSandbox = sandbox;
        }
    }
}
