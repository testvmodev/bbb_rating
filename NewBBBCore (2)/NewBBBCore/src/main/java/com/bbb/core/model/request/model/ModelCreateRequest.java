package com.bbb.core.model.request.model;

import org.apache.commons.lang3.StringUtils;

public class ModelCreateRequest {
    private Long brandId;
    private String name;

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean validateBrand() {
        return !StringUtils.isBlank(name);
    }

}
