package com.bbb.core.service.tracking;

import com.bbb.core.common.exception.ExceptionResponse;
import org.springframework.data.domain.Pageable;

public interface InventoryTrackingViewService {
    Object getBestViews(Pageable page) throws ExceptionResponse;

    Object getRecentTrackingView(Pageable page) throws ExceptionResponse;
}
