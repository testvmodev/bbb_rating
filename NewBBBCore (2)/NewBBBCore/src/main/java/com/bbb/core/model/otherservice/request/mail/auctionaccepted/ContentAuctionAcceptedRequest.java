package com.bbb.core.model.otherservice.request.mail.auctionaccepted;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentAuctionAcceptedRequest {
    @JsonProperty("link_card")
    private String linkCard;
    private ListingAuctionAcceptedRequest listing;

    public String getLinkCard() {
        return linkCard;
    }

    public void setLinkCard(String linkCard) {
        this.linkCard = linkCard;
    }

    public ListingAuctionAcceptedRequest getListing() {
        return listing;
    }

    public void setListing(ListingAuctionAcceptedRequest listing) {
        this.listing = listing;
    }
}
