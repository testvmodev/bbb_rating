package com.bbb.core.manager.templaterest;

import com.bbb.core.common.IntegratedServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestAuthenticationManager extends RestTemplateManager {
    @Autowired
    public RestAuthenticationManager(IntegratedServices integratedServices) {
        super(integratedServices.getBaseUrlAuthentication());
    }
}
