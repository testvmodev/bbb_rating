package com.bbb.core.model.otherservice.request.mail.listingshipped;

import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentListingShipped {
    private UserMailRequest user;
    private ListingInfoShipped listing;
    @JsonProperty("base_path")
    private String basePath;
    private OrderItemShipped orderItem;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingInfoShipped getListing() {
        return listing;
    }

    public void setListing(ListingInfoShipped listing) {
        this.listing = listing;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public OrderItemShipped getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItemShipped orderItem) {
        this.orderItem = orderItem;
    }
}
