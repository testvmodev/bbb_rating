package com.bbb.core.common.utils;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class UnitUtils {
    public static final String XS = "XS";
    public static final String S = "S";
    public static final String M = "M";
    public static final String L = "L";
    public static final String XL = "XL";
    public static final String XXL = "XXL";

    //TODO remove bicycle_size_name from inventory switch to use component frame size then stop using this converter
    public static String convertToSizeBicycle(String value) throws ExceptionResponse{
        if (value == null) return value;
        if (!StringUtils.containsIgnoreCase(value, "cm")
                && !StringUtils.containsIgnoreCase(value, "\"")
        ) {
            return value;
        }

        float valueCm = convertToCm(value);
        return convertCmToSize(valueCm);
    }

    public static float convertToCm(String value) throws ExceptionResponse {
        Pair<Float, String> pai = detectToValueAndUnit(value);
        String unit = pai.getSecond().toUpperCase();
        switch (unit){
            case "CM":
                return pai.getFirst();
            case "\"":
                return pai.getFirst()*2.54f;
            default:
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        MessageResponses.VALUE_INVALID), HttpStatus.BAD_REQUEST);
        }
    }

    private static Pair<Float, String> detectToValueAndUnit(String value) throws ExceptionResponse{
        if (value.contains(" ")){
            String values[] = value.split(" ");
            return new Pair<>(Float.valueOf(values[0]), values[1]);
        }else {
            int index = value.length()-1;
            for (int i = value.length()-2; i >=0; i++){
                char c = value.charAt(i);
                if( c >= '0' &&  c <= '9'){
                    index = i+1;
                    break;
                }
            }
            return new Pair<>(Float.valueOf(value.substring(0, index)), value.substring(index));
        }
    }

    public static String convertCmToSize(float value){
        if ( value <= 51){
            return XS;
        }
        if ( value <= 53){
            return S;
        }
        if ( value <= 55){
            return M;
        }
        if ( value <= 57){
            return L;
        }
        if ( value <= 60){
            return XL;
        }
       return XXL;
    }
}
