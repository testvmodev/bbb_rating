package com.bbb.core.model.database.type;

public enum TypeListingDuration {
//    CUSTOM_CODE("CustomCode"),
//    DAYS_1("Days_1"),
    DAYS_10("Days_10"),
    DAYS_120("Days_120"),
    DAYS_14("Days_14"),
    DAYS_21("Days_21"),
    DAYS_3("Days_3"),
    DAYS_30("Days_30"),
    DAYS_5("Days_5"),
    DAYS_60("Days_60"),
    DAYS_7("Days_7"),
    DAYS_90("Days_90");
//    GTC("GTC");
    private final String value;

    TypeListingDuration(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TypeListingDuration findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (TypeListingDuration typeListingDuration : values()) {
            if (value.equals(typeListingDuration.value)) {
                return typeListingDuration;
            }
        }
        return null;
    }
}
