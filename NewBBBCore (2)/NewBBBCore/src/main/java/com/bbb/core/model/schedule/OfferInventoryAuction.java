package com.bbb.core.model.schedule;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OfferInventoryAuction {
    @Id
    private long offerId;
    private float offerPrice;
    private String buyerId;
    private long inventoryId;
    private Long auctionId;
    private Long inventoryAuctionId;
    private String imageDefaultInv;
    private String inventoryName;
    private StatusInventory statusNameInv;
    private StageInventory stageNameInv;
    private ConditionInventory conditionInv;
    private String listingPrice;
    private String nameAuction;
    private String bicycleName;
    private String modelName;
    private String brandName;
    private String sizeName;
    private String yearName;
    private String typeName;

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public float getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(float offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public String getImageDefaultInv() {
        return imageDefaultInv;
    }

    public void setImageDefaultInv(String imageDefaultInv) {
        this.imageDefaultInv = imageDefaultInv;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public StatusInventory getStatusNameInv() {
        return statusNameInv;
    }

    public void setStatusNameInv(StatusInventory statusNameInv) {
        this.statusNameInv = statusNameInv;
    }

    public StageInventory getStageNameInv() {
        return stageNameInv;
    }

    public void setStageNameInv(StageInventory stageNameInv) {
        this.stageNameInv = stageNameInv;
    }

    public ConditionInventory getConditionInv() {
        return conditionInv;
    }

    public void setConditionInv(ConditionInventory conditionInv) {
        this.conditionInv = conditionInv;
    }

    public String getListingPrice() {
        return listingPrice;
    }

    public void setListingPrice(String listingPrice) {
        this.listingPrice = listingPrice;
    }

    public String getNameAuction() {
        return nameAuction;
    }

    public void setNameAuction(String nameAuction) {
        this.nameAuction = nameAuction;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }
}
