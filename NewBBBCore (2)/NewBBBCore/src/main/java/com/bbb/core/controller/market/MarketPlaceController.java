package com.bbb.core.controller.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.request.market.MarketPlaceCreateRequest;
import com.bbb.core.service.market.MarketPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MarketPlaceController {
    @Autowired
    private MarketPlaceService service;

    @GetMapping(Constants.URL_MARKET_PLACES)
    public ResponseEntity getAllMarketPlaces() {
        return ResponseEntity.ok(service.getAllMarketPlaces());
    }

    @GetMapping(Constants.URL_MARKET_PLACE)
    public ResponseEntity getMarketPlaceDetail(@PathVariable(value = Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.getMarketPlaceDetail(id));
    }

    @PostMapping(Constants.URL_MARKET_PLACES)
    public ResponseEntity postMarketPlace(
            @RequestParam("marketName") String marketPlaceName,
            @RequestParam("marketType") MarketType marketType
    ) throws ExceptionResponse {
        MarketPlaceCreateRequest request = new MarketPlaceCreateRequest();
        request.setName(marketPlaceName);
        request.setMarketType(marketType);

        return ResponseEntity.ok(service.postMarketPlace(request));
    }

    @PutMapping(Constants.URL_MARKET_PLACE)
    public ResponseEntity updateMarketPlace(
            @PathVariable(value = Constants.ID) long id,
            @RequestParam(value = "marketName", required = false) String marketPlaceName,
            @RequestParam(value = "marketType", required = false) MarketType marketType
    ) throws ExceptionResponse {
        MarketPlaceCreateRequest request = new MarketPlaceCreateRequest();
        request.setName(marketPlaceName);
        request.setMarketType(marketType);

        return ResponseEntity.ok(service.updateMarketPlace(id, request));
    }

    @DeleteMapping(Constants.URL_MARKET_PLACE)
    public ResponseEntity deleteMarketPlace(@PathVariable(value = Constants.ID) long id) throws ExceptionResponse {
        return ResponseEntity.ok(service.deleteMarketPlace(id));
    }
}
