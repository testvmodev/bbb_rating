package com.bbb.core.model.otherservice.response.shipping;

import com.bbb.core.model.database.type.CarrierType;

public class ShippingConfigResponse {
    private long id;
    private String user;
    private String password;
    private String accountNumber;
    private String apiKey;
    private String apiPassword;
    private String meterNumber;
    private CarrierType carrierType;
    private String shipServiceCode;
    private String pickupServiceCode;
    private boolean isSandbox;
    private boolean isActive;
    private String warehouseId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getShipServiceCode() {
        return shipServiceCode;
    }

    public void setShipServiceCode(String shipServiceCode) {
        this.shipServiceCode = shipServiceCode;
    }

    public String getPickupServiceCode() {
        return pickupServiceCode;
    }

    public void setPickupServiceCode(String pickupServiceCode) {
        this.pickupServiceCode = pickupServiceCode;
    }

    public boolean isSandbox() {
        return isSandbox;
    }

    public void setSandbox(boolean sandbox) {
        isSandbox = sandbox;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }
}
