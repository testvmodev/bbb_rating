package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusOffer;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OfferStatusConverter implements AttributeConverter<StatusOffer, String> {
    @Override
    public String convertToDatabaseColumn(StatusOffer statusOffer) {
        if (statusOffer == null) {
            return null;
        }
        return statusOffer.getValue();
    }

    @Override
    public StatusOffer convertToEntityAttribute(String s) {
        return StatusOffer.findByValue(s);
    }
}
