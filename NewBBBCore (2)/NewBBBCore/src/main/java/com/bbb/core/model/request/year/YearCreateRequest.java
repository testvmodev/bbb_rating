package com.bbb.core.model.request.year;

public class YearCreateRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
