package com.bbb.core.controller.tracking;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.tracking.RecentMarketListingRequest;
import com.bbb.core.model.request.tracking.RecentRequest;
import com.bbb.core.security.authen.usercontext.UserContextManager;
import com.bbb.core.service.tracking.TrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class TrackingController {
    @Autowired
    private TrackingService service;
    @Autowired
    private UserContextManager userContextManager;

    @GetMapping(value = Constants.URL_MARKET_LISTING_DEALS)
    public ResponseEntity getDealMarketListings(
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getDeals(), HttpStatus.OK);
    }
    @GetMapping(value = Constants.URL_DEALS)
    public ResponseEntity getDeals(
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getDeals(), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_RECENT_TRACKINGS)
    public ResponseEntity getRecentTrackings(
            @RequestBody @Valid List<RecentMarketListingRequest> recent
    ) throws ExceptionResponse {

        return new ResponseEntity<>(
                service.getRecentTrackings(
                        recent),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_BICYCLE_RECENT_TRACKINGS)
    public ResponseEntity getBicycleRecentTrackings(
            @RequestBody @Valid List<RecentRequest> recent
    ) throws ExceptionResponse {

        return new ResponseEntity<>(
                service.getBicycleRecentTrackings(
                        recent),
                HttpStatus.OK);
    }

}
