package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.LengthUnit;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LengthUnitConverter implements AttributeConverter<LengthUnit, String> {
    @Override
    public String convertToDatabaseColumn(LengthUnit lengthUnit) {
        if (lengthUnit == null) {
            return null;
        }
        return lengthUnit.getValue();
    }

    @Override
    public LengthUnit convertToEntityAttribute(String value) {
        return LengthUnit.findByValue(value);
    }
}
