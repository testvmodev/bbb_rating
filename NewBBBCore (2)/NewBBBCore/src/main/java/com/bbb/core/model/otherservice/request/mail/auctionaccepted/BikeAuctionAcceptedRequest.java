package com.bbb.core.model.otherservice.request.mail.auctionaccepted;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BikeAuctionAcceptedRequest {
    @JsonProperty("brand_name")
    private String brandName;
    @JsonProperty("model_name")
    private String modelName;
    @JsonProperty("year_name")
    private String yearName;
    @JsonProperty("type_name")
    private String typeName;
    @JsonProperty("size_name")
    private String sizeName;
    private String status;
    private String condition;
    @JsonProperty("inventory_name")
    private String inventoryName;
    private float price;
    private String fullName;
    private String image;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
