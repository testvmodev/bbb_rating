package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusTradeInCustomQuote;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusTradeInCustomQuoteConverter implements AttributeConverter<StatusTradeInCustomQuote, String> {
    @Override
    public String convertToDatabaseColumn(StatusTradeInCustomQuote customQuote) {
        if (customQuote == null ){
            return null;
        }
        return customQuote.getValue();
    }

    @Override
    public StatusTradeInCustomQuote convertToEntityAttribute(String s) {
        return StatusTradeInCustomQuote.findByValue(s);
    }
}
