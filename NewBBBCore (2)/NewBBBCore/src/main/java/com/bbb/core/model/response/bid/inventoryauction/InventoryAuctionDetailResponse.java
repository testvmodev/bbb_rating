package com.bbb.core.model.response.bid.inventoryauction;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.database.type.convert.StageInventoryConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventoryAuctionDetailResponse {
    @Id
    private int inventoryAuctionId;
    private long auctionId;
    private long inventoryId;
    private String auctionName;
    private float min;
    private LocalDateTime createdTime;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private long duration;
    @JsonIgnore
    private boolean isExpire;
//    @Transient
    private long numberBids;
    private long numberBidders;
    private Float currentHighestBid;


    private long bicycleId;
    private long modelId;
    private long brandId;
    private long yearId;
//    private long typeId;
//    private Long sizeId;

    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
//    private String bicycleSizeName;
    private String frameSize;

    private String serialNumber;

    private Long maxOfferId;

    private boolean isDelete;

    @Transient
    private String stageCart;


    @Transient
    private String brakeName;
    @Transient
    private Long frameMaterialId;
    @Transient
    private String frameMaterialName;
    @Transient
    private String sizeName;
    @Transient
    private String colorName;
    @Transient
    private String suspensionName;
    @Transient
    private String bottomBracketName;
    @Transient
    private String genderName;
    @Transient
    private String headsetName;
    @Transient
    private String wheelSizeName;
//    @Transient
//    private String location = ValueCommons.LOCATION_DEFAULT;
    @Transient
    private StatusInventoryAuctionResponse statusInventoryAuctionOffer;



    private String imageDefault;
    @Transient
    private List<String> images;
    private StatusInventory status;

    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
    private Float currentListedPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private float discountedPrice;

    private String partnerId;
    private String sellerId;
    private String storefrontId;
    @Column(name = "seller_is_bbb")
    private Boolean sellerIsBBB;

    @Convert(converter = StageInventoryConverter.class)
    private StageInventory stage;
    private String inventoryName;
//    private Long typeInventoryId;
//    private String typeInventoryName;
    private String title;
    private String inventoryDescription;

    private ConditionInventory condition;

    private Boolean hasBuyItNow;
    private Float buyItNow;

    private Boolean isAllowLocalPickup;
    private Boolean isInsurance;
    private Float flatRate;
    private OutboundShippingType shippingType;
    private String zipCode;
    private String countryName;
    private String countryCode;
    private String stateName;
    private String stateCode;
    private String cityName;
    private String addressLine;


    private Long ebayShippingProfileId;
    private Float shippingFee;
    private String shippingProfileLabel;
    private String shippingProfileDescription;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Transient
    private Boolean isFavourite;
    @Transient
    private LocalDateTime timeCall;

    public int getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(int inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getAuctionName() {
        return auctionName;
    }

    public void setAuctionName(String auctionName) {
        this.auctionName = auctionName;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isExpire() {
        return isExpire;
    }

    public void setExpire(boolean expire) {
        isExpire = expire;
    }

    public long getNumberBids() {
        return numberBids;
    }

    public void setNumberBids(long numberBids) {
        this.numberBids = numberBids;
    }

    public long getNumberBidders() {
        return numberBidders;
    }

    public void setNumberBidders(long numberBidders) {
        this.numberBidders = numberBidders;
    }

    public Float getCurrentHighestBid() {
        return currentHighestBid;
    }

    public void setCurrentHighestBid(Float currentHighestBid) {
        this.currentHighestBid = currentHighestBid;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getModelId() {
        return modelId;
    }

    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

//    public long getTypeId() {
//        return typeId;
//    }
//
//    public void setTypeId(long typeId) {
//        this.typeId = typeId;
//    }
//
//    public Long getSizeId() {
//        return sizeId;
//    }
//
//    public void setSizeId(Long sizeId) {
//        this.sizeId = sizeId;
//    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    //support old response model
    @Deprecated
    public String getBicycleSizeName() {
        return frameSize;
    }

//    public void setBicycleSizeName(String bicycleSizeName) {
//        this.bicycleSizeName = bicycleSizeName;
//    }

    public String getBrakeName() {
        return brakeName;
    }

    public void setBrakeName(String brakeName) {
        this.brakeName = brakeName;
    }

    public Long getFrameMaterialId() {
        return frameMaterialId;
    }

    public void setFrameMaterialId(Long frameMaterialId) {
        this.frameMaterialId = frameMaterialId;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }


    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

//    public Long getTypeInventoryId() {
//        return typeInventoryId;
//    }
//
//    public void setTypeInventoryId(Long typeInventoryId) {
//        this.typeInventoryId = typeInventoryId;
//    }
//
//    public String getTypeInventoryName() {
//        return typeInventoryName;
//    }
//
//    public void setTypeInventoryName(String typeInventoryName) {
//        this.typeInventoryName = typeInventoryName;
//    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Boolean getHasBuyItNow() {
        return hasBuyItNow;
    }

    public void setHasBuyItNow(Boolean hasBuyItNow) {
        this.hasBuyItNow = hasBuyItNow;
    }

    public Float getBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(Float buyItNow) {
        this.buyItNow = buyItNow;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSuspensionName() {
        return suspensionName;
    }

    public void setSuspensionName(String suspensionName) {
        this.suspensionName = suspensionName;
    }

    public String getBottomBracketName() {
        return bottomBracketName;
    }

    public void setBottomBracketName(String bottomBracketName) {
        this.bottomBracketName = bottomBracketName;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getHeadsetName() {
        return headsetName;
    }

    public void setHeadsetName(String headsetName) {
        this.headsetName = headsetName;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public LocalDateTime getTimeCall() {
        return timeCall;
    }

    public void setTimeCall(LocalDateTime timeCall) {
        this.timeCall = timeCall;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    public String getInventoryDescription() {
        return inventoryDescription;
    }

    public void setInventoryDescription(String inventoryDescription) {
        this.inventoryDescription = inventoryDescription;
    }

    public String getStageCart() {
        return stageCart;
    }

    public void setStageCart(String stageCart) {
        this.stageCart = stageCart;
    }

    public StatusInventoryAuctionResponse getStatusInventoryAuctionOffer() {
        return statusInventoryAuctionOffer;
    }

    public void setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse statusInventoryAuctionOffer) {
        this.statusInventoryAuctionOffer = statusInventoryAuctionOffer;
    }

    public Long getMaxOfferId() {
        return maxOfferId;
    }

    public void setMaxOfferId(Long maxOfferId) {
        this.maxOfferId = maxOfferId;
    }

    public String getWheelSizeName() {
        return wheelSizeName;
    }

    public void setWheelSizeName(String wheelSizeName) {
        this.wheelSizeName = wheelSizeName;
    }
    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }

    public Boolean getInsurance() {
        return isInsurance;
    }

    public void setInsurance(Boolean insurance) {
        isInsurance = insurance;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLocation() {
        return cityName + ", " + stateCode +", " + zipCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(Long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public Float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(Float shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getShippingProfileLabel() {
        return shippingProfileLabel;
    }

    public void setShippingProfileLabel(String shippingProfileLabel) {
        this.shippingProfileLabel = shippingProfileLabel;
    }

    public String getShippingProfileDescription() {
        return shippingProfileDescription;
    }

    public void setShippingProfileDescription(String shippingProfileDescription) {
        this.shippingProfileDescription = shippingProfileDescription;
    }


    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(String frameSize) {
        this.frameSize = frameSize;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public Boolean getSellerIsBBB() {
        return sellerIsBBB;
    }

    public void setSellerIsBBB(Boolean sellerIsBBB) {
        this.sellerIsBBB = sellerIsBBB;
    }
}
