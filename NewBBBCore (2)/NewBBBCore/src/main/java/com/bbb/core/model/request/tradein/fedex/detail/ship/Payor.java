package com.bbb.core.model.request.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Payor {
    @JacksonXmlProperty(localName = "ResponsibleParty")
    private ResponsibleParty responsibleParty;

    public Payor() {}

    public Payor(ResponsibleParty responsibleParty) {
        this.responsibleParty = responsibleParty;
    }

    public ResponsibleParty getResponsibleParty() {
        return responsibleParty;
    }

    public void setResponsibleParty(ResponsibleParty responsibleParty) {
        this.responsibleParty = responsibleParty;
    }
}
