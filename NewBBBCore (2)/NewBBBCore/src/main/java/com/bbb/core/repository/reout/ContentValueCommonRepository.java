package com.bbb.core.repository.reout;

import com.bbb.core.model.response.ContentValueCommon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContentValueCommonRepository extends CrudRepository<ContentValueCommon, Long> {

    /**
     *
     * @param inventoryId inventory id to find componentId and componentName
     * @return ContentValueCommon id:inventory_comp_type_id,  name: name of inventory_comp_type, value: value of inventory_comp_type
     */
    @Query(nativeQuery = true, value = "SELECT  inventory_comp_type.id AS id,  inventory_comp_type.name AS name, inventory_comp_detail.value AS value " +
            "FROM " +
            "inventory_comp_type JOIN inventory_comp_detail ON inventory_comp_type.id = inventory_comp_detail.inventory_comp_type_id " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId")
    List<ContentValueCommon> findInventoryCopDetail(@Param(value = "inventoryId") long inventoryId);

    /**
     *
     * @param inventoryId id of inventory
     * @return ContentValueCommon id: inventoryId, name: partnerId, value: sellerId
     */
    @Query(nativeQuery = true, value = "SELECT TOP 1 inventory.id AS id, inventory.partner_id AS name, inventory.seller_id AS value FROM inventory WHERE inventory.id = :inventoryId")
    ContentValueCommon findPartnerIdAndSellerIdInventoryByInventoryId(
            @Param(value = "inventoryId") long inventoryId
    );
}
