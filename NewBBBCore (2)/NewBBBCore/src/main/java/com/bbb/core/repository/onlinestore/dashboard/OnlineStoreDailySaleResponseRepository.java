package com.bbb.core.repository.onlinestore.dashboard;

import com.bbb.core.model.response.onlinestore.dashboard.OnlineStoreDailySaleResponse;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OnlineStoreDailySaleResponseRepository extends JpaRepository<OnlineStoreDailySaleResponse, LocalDate> {
    @Query(nativeQuery = true, value = "SELECT " +
            "days.value AS date, " +
            "ISNULL(SUM(inventory_sale.price), 0) AS sale " +
            "FROM " +
            "STRING_SPLIT(:#{@queryUtils.joinStrings(@queryUtils.dateToString(#dates))}, ',') AS days " +

            "LEFT JOIN inventory ON inventory.storefront_id IS NOT NULL " +
            "AND inventory.storefront_id = :storefrontId " +

            "LEFT JOIN inventory_sale ON inventory.id = inventory_sale.inventory_id " +
            "AND CONVERT(CHAR(10), inventory_sale.created_time, 120) = days.value " +

            "GROUP BY days.value"
    )
    List<OnlineStoreDailySaleResponse> getSaleDetails(
            @Param("storefrontId") String storefrontId,
            @Param("dates") List<LocalDate> dates
    );
}
