package com.bbb.core.model.request.saleforce.create;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.inventory.create.InventoryUpgradeCompCreateRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SalesforceAddBikeRequest {
    //inventory
    @NotNull
    private ListingInventoryBaseInfo baseInfo;
    private ListingInventoryLocation locationShipping;
    private List<SaleforceInvCompRequest> components;
    private List<InventoryUpgradeCompCreateRequest> upgradeComps;
    //listing
    @NotNull
    private Boolean isBestOffer;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private StatusMarketListing statusMarketListing;
    //saleforce
    @JsonProperty("InventoryItem")
    @NotNull
    private SaleForceInventoryItem inventoryItem;

    public ListingInventoryBaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(ListingInventoryBaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

    public ListingInventoryLocation getLocationShipping() {
        return locationShipping;
    }

    public void setLocationShipping(ListingInventoryLocation locationShipping) {
        this.locationShipping = locationShipping;
    }

    public List<SaleforceInvCompRequest> getComponents() {
        return components;
    }

    public void setComponents(List<SaleforceInvCompRequest> components) {
        this.components = components;
    }

    public List<InventoryUpgradeCompCreateRequest> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<InventoryUpgradeCompCreateRequest> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public SaleForceInventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(SaleForceInventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }
}
