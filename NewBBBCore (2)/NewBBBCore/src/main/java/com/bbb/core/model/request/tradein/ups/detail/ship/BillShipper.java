package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BillShipper {
    @JsonProperty("AccountNumber")
    private String accountNumber;

    public BillShipper(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
