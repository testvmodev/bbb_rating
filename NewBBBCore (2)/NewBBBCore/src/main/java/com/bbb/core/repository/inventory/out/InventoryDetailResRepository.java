package com.bbb.core.repository.inventory.out;

import com.bbb.core.model.response.inventory.InventoryDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryDetailResRepository extends JpaRepository<InventoryDetailResponse, Long> {
//    private long id;
//    private Boolean isAllowLocalPickup;
//    private Boolean isInsurance;
//    private Float flatRate;

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory.id AS id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.model_id AS model_id, " +
            "inventory_bicycle.year_id AS year_id , " +
            "inventory_bicycle.type_id AS type_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "inventory.image_default AS image_default, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.stage AS stage, " +
            "inventory.name AS name, " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.seller_is_bbb AS seller_is_bbb, " +
            "inventory_size.size_name AS size_name, " +

            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.county AS county " +


            "FROM inventory " +


            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
//            "LEFT JOIN market_listing ON inventory.id = market_listing.inventory_id " +
            "LEFT JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                    "inventory_comp_detail.inventory_id AS inventory_id, " +
                    "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                    "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
            "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                    "inventory_comp_detail.inventory_id AS inventory_id, " +
                    "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                    "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                    "inventory_comp_detail.inventory_id AS inventory_id, " +
                    "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                    "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            //for sorting only
            "LEFT JOIN " +
            "(SELECT MIN(market_listing.start_time) AS time_start_listing, " +
            "MIN(market_listing.end_time) AS time_end_listing, " +
            "market_listing.inventory_id AS inventory_id " +
            "FROM market_listing " +
            "GROUP BY market_listing.inventory_id ) " +
            "AS inventory_market_listing " +
            "ON inventory.id = inventory_market_listing.inventory_id " +

            //main conditions
            "WHERE " +
            "inventory.id = :inventoryId AND " +
            "inventory.is_delete = 0"

    )
    InventoryDetailResponse findOnById(
           @Param(value = "inventoryId")long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory.id AS id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.model_id AS model_id, " +
            "inventory_bicycle.year_id AS year_id , " +
            "inventory_bicycle.type_id AS type_id, " +
//            "inv_brand.id AS brand_id, " +
//            "inv_model.id AS model_id, " +
//            "inv_year.id AS year_id , " +
//            "inventory_type.id AS type_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.stage AS stage, " +
            "inventory.name AS name, " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.seller_is_bbb AS seller_is_bbb, " +
            "inventory_size.inventory_id AS size_id, " +
            "inventory_size.size_name AS size_name, " +

            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.county AS county " +
            "FROM inventory " +


            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
//            "LEFT JOIN market_listing ON inventory.id = market_listing.inventory_id " +
            "LEFT JOIN inventory_location_shipping on inventory.id = inventory_location_shipping.inventory_id " +
            "LEFT JOIN " +
            "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.value AS size_name " +
            "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
            "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
            "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.value AS brake_type_name " +
            "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
            "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +
            "LEFT JOIN " +
            "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
            "inventory_comp_detail.inventory_id AS inventory_id, " +
            "inventory_comp_detail.value AS fragment_material_name " +
            "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
            "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            //for sorting only
            "LEFT JOIN " +
            "(SELECT MIN(market_listing.start_time) AS time_start_listing, " +
            "MIN(market_listing.end_time) AS time_end_listing, " +
            "market_listing.inventory_id AS inventory_id " +
            "FROM market_listing " +
            "GROUP BY market_listing.inventory_id ) " +
            "AS inventory_market_listing " +
            "ON inventory.id = inventory_market_listing.inventory_id " +

            //main conditions
            "WHERE " +
            "inventory.id IN :inventoriesId AND " +
            "inventory.is_delete = 0"

    )
    List<InventoryDetailResponse> findAllByIds(
            @Param(value = "inventoriesId")List<Long> inventoriesId
    );
}
