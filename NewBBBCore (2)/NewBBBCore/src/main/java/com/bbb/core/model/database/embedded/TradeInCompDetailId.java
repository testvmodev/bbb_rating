package com.bbb.core.model.database.embedded;

import com.bbb.core.model.database.TradeInCompDetail;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TradeInCompDetailId implements Serializable {
    private long tradeInId;
    private long inventoryCompTypeId;

    public TradeInCompDetailId() {}

    public TradeInCompDetailId(long tradeInId, long inventoryCompTypeId) {
        this.tradeInId = tradeInId;
        this.inventoryCompTypeId = inventoryCompTypeId;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public long getInventoryCompTypeId() {
        return inventoryCompTypeId;
    }

    public void setInventoryCompTypeId(long inventoryCompTypeId) {
        this.inventoryCompTypeId = inventoryCompTypeId;
    }
}
