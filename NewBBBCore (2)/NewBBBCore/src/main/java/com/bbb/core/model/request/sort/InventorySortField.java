package com.bbb.core.model.request.sort;

public enum InventorySortField {
    TIME_START_LISTING_NEWEST("time_start_listing"),
    TIME_END_LISTING_SOONEST("time_end_listing"),
    LISTED_PRICE("current_listed_price"),
    BEST_DEAL("deal_percent");

    private final String value;

    InventorySortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static InventorySortField findByValue(String value) {
        for (InventorySortField inventorySortField : InventorySortField.values()) {
            if (inventorySortField.getValue().equals(value)) {
                return inventorySortField;
            }
        }
        return null;
    }
}
