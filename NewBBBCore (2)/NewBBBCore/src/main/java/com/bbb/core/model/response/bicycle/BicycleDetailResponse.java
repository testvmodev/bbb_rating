package com.bbb.core.model.response.bicycle;

import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.tradein.bicycle.ConditionTradeInBicycleResponse;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

public class BicycleDetailResponse extends Bicycle {
    private List<String> images = new ArrayList<>();
    private String brandName;
    private String yearName;
    private String modelName;
    private String typeName;
    private String sizeName;
//    private List<ContentCommonId> yearModels;
    private List<BicycleComponentResponse> components;
    @Deprecated
    private List<BicycleSpecResponse> spec;
    private Float msrp;
    private List<ConditionValueGuideBicycleResponse> conditions;
    private List<BicycleYearModelResponse> additionalYears;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

//    public List<ContentCommonId> getYearModels() {
//        return yearModels;
//    }
//
//    public void setYearModels(List<ContentCommonId> yearModels) {
//        this.yearModels = yearModels;
//    }

    public List<BicycleComponentResponse> getComponents() {
        return components;
    }

    public void setComponents(List<BicycleComponentResponse> components) {
        this.components = components;
    }

    public List<BicycleSpecResponse> getSpec() {
        return spec;
    }

    public void setSpec(List<BicycleSpecResponse> spec) {
        this.spec = spec;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Float getMsrp() {
        return getRetailPrice();
    }

//    public void setMsrp(Float msrp) {
//        this.msrp = msrp;
//    }

    public List<ConditionValueGuideBicycleResponse> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionValueGuideBicycleResponse> conditions) {
        this.conditions = conditions;
    }

    public List<BicycleYearModelResponse> getAdditionalYears() {
        return additionalYears;
    }

    public void setAdditionalYears(List<BicycleYearModelResponse> additionalYears) {
        this.additionalYears = additionalYears;
    }
}
