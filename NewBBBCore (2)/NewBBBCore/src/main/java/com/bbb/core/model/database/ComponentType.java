package com.bbb.core.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "component_type")
public class ComponentType {
  @Id
  @GeneratedValue(strategy =  GenerationType.IDENTITY)
  private long id;
  private String name;
  private long sortOrder;
  private long componentCategoryId;
  private boolean isDelete;
  private Integer forOnlyBicycleTypeId;
  private boolean isSelect;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getComponentCategoryId() {
    return componentCategoryId;
  }

  public void setComponentCatsegoryId(long componentCategoryId) {
    this.componentCategoryId = componentCategoryId;
  }

  @JsonIgnore
  public boolean isDelete() {
    return isDelete;
  }

  public void setDelete(boolean delete) {
    isDelete = delete;
  }

  public Integer getForOnlyBicycleTypeId() {
    return forOnlyBicycleTypeId;
  }

  public void setForOnlyBicycleTypeId(Integer forOnlyBicycleTypeId) {
    this.forOnlyBicycleTypeId = forOnlyBicycleTypeId;
  }

  public long getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(long sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setComponentCategoryId(long componentCategoryId) {
    this.componentCategoryId = componentCategoryId;
  }

  public boolean isSelect() {
    return isSelect;
  }

  public void setSelect(boolean select) {
    isSelect = select;
  }
}
