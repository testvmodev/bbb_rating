package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LabelImageFormat {
    @JsonProperty("Code")
    private String code;

    public LabelImageFormat(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
