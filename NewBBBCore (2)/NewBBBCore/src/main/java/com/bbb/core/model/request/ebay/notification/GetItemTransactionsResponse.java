package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class GetItemTransactionsResponse {
    @JacksonXmlProperty(localName = "Timestamp")
    private LocalDateTime timestamp;
    @JacksonXmlProperty(localName = "Ack")
    private String ack;
    @JacksonXmlProperty(localName = "CorrelationID")
    private long correlationID;
    @JacksonXmlProperty(localName = "Version")
    private int version;
    @JacksonXmlProperty(localName = "Build")
    private String build;
    @JacksonXmlProperty(localName = "NotificationEventName")
    private String notificationEventName;
    @JacksonXmlProperty(localName = "RecipientUserID")
    private String recipientUserID;
    @JacksonXmlProperty(localName = "EIASToken")
    private String eIASToken;
    @JacksonXmlProperty(localName = "PaginationResult")
    private PaginationResult paginationResult;
    @JacksonXmlProperty(localName = "HasMoreTransactions")
    private boolean hasMoreTransactions;
    @JacksonXmlProperty(localName = "TransactionsPerPage")
    private int transactionsPerPage;
    @JacksonXmlProperty(localName = "PageNumber")
    private int pageNumber;
    @JacksonXmlProperty(localName = "ReturnedTransactionCountActual")
    private int returnedTransactionCountActual;
    @JacksonXmlProperty(localName = "Item")
    private Item item;
    @JacksonXmlProperty(localName = "TransactionArray")
    private TransactionArray transactionArray;
    @JacksonXmlProperty(localName = "PayPalPreferred")
    private boolean payPalPreferred;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public long getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(long correlationID) {
        this.correlationID = correlationID;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getNotificationEventName() {
        return notificationEventName;
    }

    public void setNotificationEventName(String notificationEventName) {
        this.notificationEventName = notificationEventName;
    }

    public String getRecipientUserID() {
        return recipientUserID;
    }

    public void setRecipientUserID(String recipientUserID) {
        this.recipientUserID = recipientUserID;
    }

    public String geteIASToken() {
        return eIASToken;
    }

    public void seteIASToken(String eIASToken) {
        this.eIASToken = eIASToken;
    }

    public PaginationResult getPaginationResult() {
        return paginationResult;
    }

    public void setPaginationResult(PaginationResult paginationResult) {
        this.paginationResult = paginationResult;
    }

    public boolean isHasMoreTransactions() {
        return hasMoreTransactions;
    }

    public void setHasMoreTransactions(boolean hasMoreTransactions) {
        this.hasMoreTransactions = hasMoreTransactions;
    }

    public int getTransactionsPerPage() {
        return transactionsPerPage;
    }

    public void setTransactionsPerPage(int transactionsPerPage) {
        this.transactionsPerPage = transactionsPerPage;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getReturnedTransactionCountActual() {
        return returnedTransactionCountActual;
    }

    public void setReturnedTransactionCountActual(int returnedTransactionCountActual) {
        this.returnedTransactionCountActual = returnedTransactionCountActual;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public TransactionArray getTransactionArray() {
        return transactionArray;
    }

    public void setTransactionArray(TransactionArray transactionArray) {
        this.transactionArray = transactionArray;
    }

    public boolean isPayPalPreferred() {
        return payPalPreferred;
    }

    public void setPayPalPreferred(boolean payPalPreferred) {
        this.payPalPreferred = payPalPreferred;
    }
}
