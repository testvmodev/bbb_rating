package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TypeExpirationTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TypeExpirationTimeConverter implements AttributeConverter<TypeExpirationTime, String> {
    @Override
    public String convertToDatabaseColumn(TypeExpirationTime typeExpirationTime) {
        if (typeExpirationTime == null) return null;
        return typeExpirationTime.getValue();
    }

    @Override
    public TypeExpirationTime convertToEntityAttribute(String s) {
        return TypeExpirationTime.findByValue(s);
    }
}
