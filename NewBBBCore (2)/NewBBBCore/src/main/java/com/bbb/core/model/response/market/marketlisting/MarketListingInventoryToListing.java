package com.bbb.core.model.response.market.marketlisting;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class MarketListingInventoryToListing {
    @Id
    private long inventoryId;
    private String inventoryName;
    private String inventoryTitle;
    private Float listPrice;
    private Float initialPrice;
//    private Float minPriceBestOffer;
//    private Float autoAcceptedPrice;
    private String imageDefault;
    private String partnerId;
    @Transient
    private List<MarketToList> marketToLists;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Float getListPrice() {
        return listPrice;
    }

    public void setListPrice(Float listPrice) {
        this.listPrice = listPrice;
    }

//    public Float getMinPriceBestOffer() {
//        return minPriceBestOffer;
//    }
//
//    public void setMinPriceBestOffer(Float minPriceBestOffer) {
//        this.minPriceBestOffer = minPriceBestOffer;
//    }
//
//    public Float getAutoAcceptedPrice() {
//        return autoAcceptedPrice;
//    }
//
//    public void setAutoAcceptedPrice(Float autoAcceptedPrice) {
//        this.autoAcceptedPrice = autoAcceptedPrice;
//    }

    public List<MarketToList> getMarketToLists() {
        return marketToLists;
    }

    public void setMarketToLists(List<MarketToList> marketToLists) {
        this.marketToLists = marketToLists;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public Float getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(Float initialPrice) {
        this.initialPrice = initialPrice;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
