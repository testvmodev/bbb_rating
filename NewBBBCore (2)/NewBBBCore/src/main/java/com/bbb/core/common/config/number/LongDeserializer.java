package com.bbb.core.common.config.number;

import com.bbb.core.common.exception.ExceptionNumberFormatRequest;
import com.bbb.core.model.response.ObjectError;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;

public class LongDeserializer extends JsonDeserializer<Long> {
    @Override
    public Long deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String value = jsonParser.getValueAsString();
        if (StringUtils.isBlank(value)) {
            return null;
        }
        try {
            return Long.valueOf(value);
        }catch (NumberFormatException e) {
            throw new ExceptionNumberFormatRequest(new ObjectError(
                    ObjectError.ERROR_PARAM, "Hello"),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
