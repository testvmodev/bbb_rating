package com.bbb.core.repository.onlinestore;

import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.onlinestore.OnlineStoreResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OnlineStoreResponseRepository extends JpaRepository<OnlineStoreResponse, String> {
    @Query(nativeQuery = true, value = "SELECT trending.* " +
            "FROM (" +
            "SELECT " +
            "storefront.value AS storefront_id, " +
            "COUNT(sold_listing.id) AS sold_market_listings, " +
            "COUNT(listed_listing.id) AS listed_market_listings " +
            "FROM " +
            "STRING_SPLIT(:#{@queryUtils.joinStrings(#storefrontIds)}, ',') " +
            "AS storefront " +

            "LEFT JOIN inventory ON inventory.storefront_id = storefront.value AND inventory.is_delete = 0 " +
            "LEFT JOIN inventory_type ON inventory_type.id = inventory.type_id " +
            "LEFT JOIN market_listing AS listed_listing " +
            "ON inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "AND listed_listing.inventory_id = inventory.id " +
            "AND listed_listing.is_delete = 0 " +
            "AND listed_listing.market_place_id = :marketPlaceId " +
            "AND listed_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} " +
            "LEFT JOIN market_listing AS sold_listing " +
            "ON inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "AND sold_listing.inventory_id = inventory.id " +
            "AND sold_listing.is_delete = 0 " +
            "AND sold_listing.market_place_id = :marketPlaceId " +
            "AND sold_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +

            "GROUP BY storefront.value " +
            ") AS trending " +

            "ORDER BY trending.sold_market_listings DESC, trending.listed_market_listings DESC " +
            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<OnlineStoreResponse> findTrendingSorted(
            @Param("storefrontIds") List<String> storefrontIds,
            @Param("marketPlaceId") long marketPlaceId,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT trending.* " +
            "FROM (" +
            "SELECT " +
            "inventory.storefront_id, " +
            "COUNT(sold_listing.id) AS sold_market_listings, " +
            "COUNT(listed_listing.id) AS listed_market_listings " +
            "FROM inventory " +

            "JOIN inventory_type ON inventory_type.id = inventory.type_id " +
            "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "LEFT JOIN market_listing AS listed_listing ON listed_listing.inventory_id = inventory.id " +
            "AND listed_listing.is_delete = 0 " +
            "AND listed_listing.market_place_id = :marketPlaceId " +
            "AND listed_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} " +
            "LEFT JOIN market_listing AS sold_listing ON sold_listing.inventory_id = inventory.id " +
            "AND sold_listing.is_delete = 0 " +
            "AND sold_listing.market_place_id = :marketPlaceId " +
            "AND sold_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +

            "WHERE inventory.storefront_id IS NOT NULL " +
            "AND inventory.is_delete = 0 " +

            "GROUP BY inventory.storefront_id " +
            ") AS trending " +

            "ORDER BY trending.sold_market_listings DESC, trending.listed_market_listings DESC " +
            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<OnlineStoreResponse> findTrendingSorted(
            @Param("marketPlaceId") long marketPlaceId,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT onlinestore.* " +
            "FROM (" +
            "SELECT " +
            "storefront.value AS storefront_id, " +
            "COUNT(sold_listing.id) AS sold_market_listings, " +
            "COUNT(listed_listing.id) AS listed_market_listings " +
            "FROM " +
            "STRING_SPLIT(:#{@queryUtils.joinStrings(#storefrontIds)}, ',') " +
            "AS storefront " +

            "LEFT JOIN inventory ON inventory.storefront_id = storefront.value AND inventory.is_delete = 0 " +
            "LEFT JOIN inventory_type ON inventory_type.id = inventory.type_id " +
            "LEFT JOIN market_listing AS listed_listing " +
                "ON inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                "AND listed_listing.inventory_id = inventory.id " +
                "AND listed_listing.is_delete = 0 " +
                "AND listed_listing.market_place_id = :marketPlaceId " +
                "AND listed_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} " +
            "LEFT JOIN market_listing AS sold_listing " +
                "ON inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                "AND sold_listing.inventory_id = inventory.id " +
                "AND sold_listing.is_delete = 0 " +
                "AND sold_listing.market_place_id = :marketPlaceId " +
                "AND sold_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +

            "GROUP BY storefront.value " +
            ") AS onlinestore " +

            "ORDER BY " +
            "CASE WHEN :#{#sortForSale.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN onlinestore.listed_market_listings END ASC, " +
            "CASE WHEN :#{#sortForSale.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN onlinestore.listed_market_listings END DESC, " +
            "onlinestore.sold_market_listings DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<OnlineStoreResponse> findAll(
            @Param("storefrontIds") List<String> storefrontIds,
            @Param("marketPlaceId") long marketPlaceId,
            @Param("sortForSale") Sort sortForSale,
            @Param("pageable") Pageable pageable
    );
}
