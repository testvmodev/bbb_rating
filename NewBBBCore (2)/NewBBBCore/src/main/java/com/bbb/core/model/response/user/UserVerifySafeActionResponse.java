package com.bbb.core.model.response.user;

public class UserVerifySafeActionResponse {
    private Boolean marketListing;
    private Boolean offer;

    public Boolean getMarketListing() {
        return marketListing;
    }

    public void setMarketListing(Boolean marketListing) {
        this.marketListing = marketListing;
    }

    public Boolean getOffer() {
        return offer;
    }

    public void setOffer(Boolean offer) {
        this.offer = offer;
    }

    public boolean isSafe() {
        return (marketListing != null && !marketListing) &&
                (offer != null && !offer);
    }
}
