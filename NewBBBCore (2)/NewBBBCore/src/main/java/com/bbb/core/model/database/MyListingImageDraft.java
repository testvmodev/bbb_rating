package com.bbb.core.model.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MyListingImageDraft {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long listingDraftId;
    private String image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getListingDraftId() {
        return listingDraftId;
    }

    public void setListingDraftId(long listingDraftId) {
        this.listingDraftId = listingDraftId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
