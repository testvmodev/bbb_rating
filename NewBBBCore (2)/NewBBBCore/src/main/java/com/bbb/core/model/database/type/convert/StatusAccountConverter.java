package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusAccount;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusAccountConverter implements AttributeConverter<StatusAccount, String> {
    @Override
    public String convertToDatabaseColumn(StatusAccount statusAccount) {
        if (statusAccount == null) return null;
        return statusAccount.getValue();
    }

    @Override
    public StatusAccount convertToEntityAttribute(String value) {
        return StatusAccount.findByValue(value);
    }
}
