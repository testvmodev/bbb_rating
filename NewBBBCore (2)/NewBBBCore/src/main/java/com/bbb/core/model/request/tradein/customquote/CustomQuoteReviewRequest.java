package com.bbb.core.model.request.tradein.customquote;

public class CustomQuoteReviewRequest {
    private long tradeInCustomQuoteId;
    private Float tradeInValue;
    private String reviewNote;

    public long getTradeInCustomQuoteId() {
        return tradeInCustomQuoteId;
    }

    public void setTradeInCustomQuoteId(long tradeInCustomQuoteId) {
        this.tradeInCustomQuoteId = tradeInCustomQuoteId;
    }

    public Float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(Float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    public String getReviewNote() {
        return reviewNote;
    }

    public void setReviewNote(String reviewNote) {
        this.reviewNote = reviewNote;
    }
}
