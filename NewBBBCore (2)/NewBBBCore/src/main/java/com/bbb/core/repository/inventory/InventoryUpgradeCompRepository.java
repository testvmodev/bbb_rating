package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryUpgradeComp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface InventoryUpgradeCompRepository extends JpaRepository<InventoryUpgradeComp, Long> {
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "DELETE from inventory_upgrade_comp " +
            "WHERE inventory_upgrade_comp.inventory_id = :inventoryId")
    void deleteAllByInventoryId(@Param("inventoryId") long inventoryId);
}
