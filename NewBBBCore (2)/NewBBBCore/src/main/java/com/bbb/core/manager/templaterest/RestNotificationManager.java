package com.bbb.core.manager.templaterest;

import com.bbb.core.common.IntegratedServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestNotificationManager extends RestTemplateManager {
    @Autowired
    public RestNotificationManager(IntegratedServices integratedServices) {
        super(integratedServices.getBaseUrlNotify());
    }
}
