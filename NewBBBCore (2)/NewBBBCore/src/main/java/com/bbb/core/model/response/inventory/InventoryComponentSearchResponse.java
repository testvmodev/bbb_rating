package com.bbb.core.model.response.inventory;

import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.database.type.ConditionInventory;

import java.util.List;

public class InventoryComponentSearchResponse {
    private List<ContentCommonId> bicycleTypes;
    private List<ContentCommonId> bicycleBrands;
    private List<ContentCommonId> bicycleModels;
    private List<ContentCommonId> inventorySizes;
    private List<ContentCommonId> bicycleYears;
    private List<ContentCommonId> brakeTypes;
    private List<ContentCommonId> frameMaterials;
    private List<ConditionInventory> conditions;

    public List<ContentCommonId> getBicycleTypes() {
        return bicycleTypes;
    }

    public void setBicycleTypes(List<ContentCommonId> bicycleTypes) {
        this.bicycleTypes = bicycleTypes;
    }

    public List<ContentCommonId> getBicycleBrands() {
        return bicycleBrands;
    }

    public void setBicycleBrands(List<ContentCommonId> bicycleBrands) {
        this.bicycleBrands = bicycleBrands;
    }

    public List<ContentCommonId> getBicycleModels() {
        return bicycleModels;
    }

    public void setBicycleModels(List<ContentCommonId> bicycleModels) {
        this.bicycleModels = bicycleModels;
    }

    public List<ContentCommonId> getInventorySizes() {
        return inventorySizes;
    }

    public void setInventorySizes(List<ContentCommonId> inventorySizes) {
        this.inventorySizes = inventorySizes;
    }

    public List<ContentCommonId> getBicycleYears() {
        return bicycleYears;
    }

    public void setBicycleYears(List<ContentCommonId> bicycleYears) {
        this.bicycleYears = bicycleYears;
    }

    public List<ContentCommonId> getBrakeTypes() {
        return brakeTypes;
    }

    public void setBrakeTypes(List<ContentCommonId> brakeTypes) {
        this.brakeTypes = brakeTypes;
    }

    public List<ConditionInventory> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionInventory> conditions) {
        this.conditions = conditions;
    }

    public List<ContentCommonId> getFrameMaterials() {
        return frameMaterials;
    }

    public void setFrameMaterials(List<ContentCommonId> frameMaterials) {
        this.frameMaterials = frameMaterials;
    }
}
