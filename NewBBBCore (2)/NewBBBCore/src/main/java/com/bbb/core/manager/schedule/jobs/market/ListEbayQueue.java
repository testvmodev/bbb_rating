package com.bbb.core.manager.schedule.jobs.market;

import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.FIX_RATE, defaultTimeSeconds = 30 * 60)
public class ListEbayQueue extends ScheduleJob {
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private AppConfig appConfig;

    @Override
    public void run() {
        MarketToList marketToList = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToList == null) {
            return;
        }
        List<MarketListing> marketListings = marketListingRepository.findAllMarketListingQueueEbay(
                marketToList.getMarketPlaceId(),
                0,
                EbayValue.MAX_START_ASYNC
        );
        if (marketListings.size() > 0) {
            for (MarketListing marketListing : marketListings) {
                marketListing.setStatus(StatusMarketListing.LISTING);
                marketListing.setCountListingError(0);
            }
            marketListingRepository.saveAll(marketListings);
            marketListingManagerAsync.listEbay(marketListings, null);
        }
    }

    public void onFail(Throwable throwable) {}
}
