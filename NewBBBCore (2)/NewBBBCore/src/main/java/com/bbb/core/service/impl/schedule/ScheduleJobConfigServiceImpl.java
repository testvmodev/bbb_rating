package com.bbb.core.service.impl.schedule;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.schedule.ScheduleJobConfigManager;
import com.bbb.core.model.request.schedule.ScheduleJobConfigUpdateRequest;
import com.bbb.core.service.schedule.ScheduleJobConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleJobConfigServiceImpl implements ScheduleJobConfigService {
    @Autowired
    private ScheduleJobConfigManager manager;

    @Override
    public Object getJobConfigs(Boolean isEnable) {
        return manager.getJobConfigs(isEnable);
    }

    @Override
    public Object getRunningJobs() {
        return manager.getRunningJobs();
    }

    @Override
    public Object getInqueueJobs() {
        return manager.getInqueueJobs();
    }

    @Override
    public Object getJobDetail(long id) throws ExceptionResponse {
        return manager.getJobDetail(id);
    }

    @Override
    public Object updateJobConfig(long id, ScheduleJobConfigUpdateRequest request) throws ExceptionResponse {
        return manager.updateJobConfig(id, request);
    }
}
