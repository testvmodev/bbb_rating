package com.bbb.core.model.request.ebay.additems;

import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "AddItemsRequest", namespace = "urn:ebay:apis:eBLBaseComponents")
public class AddItemsRequest extends BaseEbayRequest {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "AddItemRequestContainer")
    private List<AddItemRequestContainer> addItemRequestContainers;

    public List<AddItemRequestContainer> getAddItemRequestContainers() {
        return addItemRequestContainers;
    }

    public void setAddItemRequestContainers(List<AddItemRequestContainer> addItemRequestContainers) {
        this.addItemRequestContainers = addItemRequestContainers;
    }

}
