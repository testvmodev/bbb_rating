package com.bbb.core.model.database.type;

public enum StatusTradeInCustomQuote {
    INCOMPLETE_DETAILS("Pending"),
    PENDING_REVIEW("InReview"),
    PROVIDED_VALUE("Reviewed"),
    COMPLETED("Completed");
    private final String value;

    StatusTradeInCustomQuote(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StatusTradeInCustomQuote findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (StatusTradeInCustomQuote status : StatusTradeInCustomQuote.values()) {
            if (status.getValue().equals(value)) {
                return status;
            }
        }
        return null;
    }
}
