package com.bbb.core.model.database;

import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_custom_quote")
public class TradeInCustomQuote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Long brandId;
    private String brandName;
    private Long modelId;
    private String modelName;
    private Long yearId;
    private String yearName;
    private boolean isCreateFromBicycle;
    private StatusTradeInCustomQuote status;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private Float tradeInValue;
    private Long typeId;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Boolean isClean;
    private String userCreatedId;
    private String note;
    private String employeeName;
    private String employeeEmail;
    private String employeeLocation;
    private String ownerName;
    private String userReviewId;
//    private long sizeId;
    private String adminReviewNote;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }


    public StatusTradeInCustomQuote getStatus() {
        return status;
    }

    public void setStatus(StatusTradeInCustomQuote status) {
        this.status = status;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isCreateFromBicycle() {
        return isCreateFromBicycle;
    }

    public void setCreateFromBicycle(boolean createFromBicycle) {
        isCreateFromBicycle = createFromBicycle;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Float getTradeInValue() {
        return tradeInValue;
    }

    public void setTradeInValue(Float tradeInValue) {
        this.tradeInValue = tradeInValue;
    }

    public String getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(String userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeLocation() {
        return employeeLocation;
    }

    public void setEmployeeLocation(String employeeLocation) {
        this.employeeLocation = employeeLocation;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getUserReviewId() {
        return userReviewId;
    }

    public void setUserReviewId(String userReviewId) {
        this.userReviewId = userReviewId;
    }

//    public long getSizeId() {
//        return sizeId;
//    }
//
//    public void setSizeId(long sizeId) {
//        this.sizeId = sizeId;
//    }


    public String getAdminReviewNote() {
        return adminReviewNote;
    }

    public void setAdminReviewNote(String adminReviewNote) {
        this.adminReviewNote = adminReviewNote;
    }
}
