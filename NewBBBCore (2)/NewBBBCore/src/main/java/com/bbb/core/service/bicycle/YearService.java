package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.year.YearCreateRequest;
import com.bbb.core.model.request.year.YearUpdateRequest;

public interface YearService {
    Object getYears(String yearName);

    Object createYear(YearCreateRequest request) throws ExceptionResponse;

    Object updateYear(long yearId, YearUpdateRequest request) throws ExceptionResponse;

    Object deleteType(long yearId) throws ExceptionResponse;

    Object getYearFromBrandModel(Long brandId, Long modelId) throws ExceptionResponse;
}
