package com.bbb.core.model;

import javax.persistence.*;

@Entity
@Table(name = "bicycle_model")
public class ModelBrand {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "brandId", referencedColumnName = "id")
    private BrandModels brandModel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BrandModels getBrandModel() {
        return brandModel;
    }

    public void setBrandModel(BrandModels brandModel) {
        this.brandModel = brandModel;
    }


}
