package com.bbb.core.model.otherservice.request.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class CartAddRequest {
    @JsonProperty(value = "local_pickup")
    private boolean isLocalPickup;
    @JsonProperty(value = "buyer_id")
    private String buyerId;
    @JsonProperty(value = "cart_type")
    private String cartType;
    @JsonProperty(value = "offer_id")
    private long offerId;

    public boolean isLocalPickup() {
        return isLocalPickup;
    }

    public void setLocalPickup(boolean localPickup) {
        isLocalPickup = localPickup;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }
}
