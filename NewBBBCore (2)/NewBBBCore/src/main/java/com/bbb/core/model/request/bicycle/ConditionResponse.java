package com.bbb.core.model.request.bicycle;

import com.bbb.core.model.database.type.ConditionInventory;

public class ConditionResponse {
    private ConditionInventory key;
    private String value;

    public ConditionResponse(ConditionInventory key, String value) {
        this.key = key;
        this.value = value;
    }

    public ConditionInventory getKey() {
        return key;
    }

    public void setKey(ConditionInventory key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
