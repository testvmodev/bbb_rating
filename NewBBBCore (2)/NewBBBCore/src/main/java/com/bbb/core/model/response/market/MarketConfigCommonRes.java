package com.bbb.core.model.response.market;

import com.bbb.core.model.database.type.MarketType;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MarketConfigCommonRes {
    @Id
    private long marketConfigId;
    private long marketPlaceId;
    private MarketType type;
    private String marketName;

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public long getMarketConfigId() {
        return marketConfigId;
    }

    public void setMarketConfigId(long marketConfigId) {
        this.marketConfigId = marketConfigId;
    }

    public MarketType getType() {
        return type;
    }

    public void setType(MarketType type) {
        this.type = type;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }
}
