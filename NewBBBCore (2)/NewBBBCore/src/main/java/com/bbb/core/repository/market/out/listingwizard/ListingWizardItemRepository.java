package com.bbb.core.repository.market.out.listingwizard;

import com.bbb.core.model.response.market.listingwizard.ListingWizardItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingWizardItemRepository extends JpaRepository<ListingWizardItem, Long> {
//    private Boolean isBestOffer;
//    private Float minimumOfferAutoAcceptPrice;
//    private Float bestOfferAutoAcceptPrice;
//    private Long ebayCategoryId;
//    private String ebayCategoryName;
//    private Long subEbayCategoryId;
//    private String subEbayCategoryName;
//    private ListingTypeEbay listingTypeEbay;
//    private Long ebayListingDurationId;
//    private String ebayListingDurationName;


    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.last_msg_error AS last_msg_error, " +
            "market_listing.status AS status, " +
            "market_listing.inventory_id AS inventory_id, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_listing.market_place_config_id AS market_place_config_id, " +
            "market_listing.time_listed AS time_listed, " +
            "market_listing.item_id AS item_id, " +
            "market_listing.is_best_offer AS is_best_offer, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.ebay_category_id AS ebay_category_id, " +
            "primary_category.name AS ebay_category_name, " +
            "market_listing.ebay_sub_category_id AS sub_ebay_category_id, " +
            "sub_category.name AS sub_ebay_category_name, " +
            "market_listing.listing_type_ebay AS listing_type_ebay, " +
            "market_listing.ebay_listing_duration_id AS ebay_listing_duration_id, " +
            "ebay_listing_duration.value AS ebay_listing_duration_name " +
            "FROM market_listing " +
            "LEFT JOIN ebay_catetory AS primary_category ON market_listing.ebay_category_id = primary_category.id " +
            "LEFT JOIN ebay_catetory AS sub_category ON market_listing.ebay_category_id = sub_category.id " +
            "LEFT JOIN ebay_listing_duration ON market_listing.ebay_listing_duration_id = ebay_listing_duration.id " +
            "WHERE market_listing.inventory_id IN :inventoriesId AND market_listing.is_delete = 0")
    List<ListingWizardItem> findAllByInventoryId(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );
}
