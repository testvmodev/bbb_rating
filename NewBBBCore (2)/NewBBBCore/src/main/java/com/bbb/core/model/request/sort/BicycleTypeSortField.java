package com.bbb.core.model.request.sort;

public enum  BicycleTypeSortField {
    NAME("name");
    private final String value;

    BicycleTypeSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
