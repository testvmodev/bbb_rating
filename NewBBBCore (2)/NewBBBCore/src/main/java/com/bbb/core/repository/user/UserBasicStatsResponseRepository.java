package com.bbb.core.repository.user;

import com.bbb.core.model.response.user.UserBasicStatsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserBasicStatsResponseRepository extends JpaRepository<UserBasicStatsResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT *, :userId AS id " +
            "FROM (" +
            "SELECT COUNT(market_listing.id) AS created_market_listings " +
            "FROM market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE market_listing.is_delete = 0 " +
            "AND inventory.seller_id = :userId " +
            ") AS listing_created " +
            "JOIN (" +
            "SELECT COUNT(market_listing_sale.market_listing_id) AS bought_market_listings " +
            "FROM market_listing_sale " +
            "WHERE buyer_id = :userId " +
            ") AS listing_bought ON 1 = 1 " +
            "JOIN (" +
            "SELECT COUNT(offer.id) AS created_offers " +
            "FROM offer " +
            "WHERE offer.buyer_id = :userId " +
            "AND offer.market_listing_id IS NOT NULL " +
            "AND offer.is_delete = 0 " +
            ") AS offer_created ON 1 = 1 " +
            "JOIN (" +
            "SELECT COUNT(offer.id) AS created_bids " +
            "FROM offer " +
            "WHERE offer.is_delete = 0 " +
            "AND offer.buyer_id = :userId " +
            "AND offer.inventory_auction_id IS NOT NULL " +
            ") AS bid_created ON 1 = 1 " +
            "JOIN (" +
            "SELECT COUNT(DISTINCT inventory_auction.id) AS won_bids " +
            "FROM inventory_auction " +
            "LEFT JOIN offer ON (inventory_auction.id = offer.inventory_auction_id " +
                "AND offer.is_delete = 0 " +
                "AND offer.buyer_id = :userId " +
                "AND inventory_auction.winner_id = :userId) " +
            "WHERE offer.inventory_auction_id IS NOT NULL " +
            ") AS bid_win ON 1 = 1"
    )
    UserBasicStatsResponse findOne(
            @Param("userId") String userId
    );
}
