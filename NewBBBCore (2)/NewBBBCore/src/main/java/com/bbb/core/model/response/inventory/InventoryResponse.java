package com.bbb.core.model.response.inventory;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.database.type.convert.StageInventoryConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryResponse extends InventoryBaseInfo{
    @Id
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
