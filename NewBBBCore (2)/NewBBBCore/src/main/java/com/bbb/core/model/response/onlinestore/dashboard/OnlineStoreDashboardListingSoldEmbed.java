package com.bbb.core.model.response.onlinestore.dashboard;

import javax.persistence.Embeddable;

@Embeddable
public class OnlineStoreDashboardListingSoldEmbed {
    private int allSold;
    private int awaitingShipment;
    private int shipped;

    public int getAllSold() {
        return allSold;
    }

    public void setAllSold(int allSold) {
        this.allSold = allSold;
    }

    public int getAwaitingShipment() {
        return awaitingShipment;
    }

    public void setAwaitingShipment(int awaitingShipment) {
        this.awaitingShipment = awaitingShipment;
    }

    public int getShipped() {
        return shipped;
    }

    public void setShipped(int shipped) {
        this.shipped = shipped;
    }
}
