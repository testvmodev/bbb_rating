package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.database.type.ConditionInventory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DeclineTradeIn {
    @Id
    private Long bicycleId;
    private ConditionInventory condition;

    private long reasonDeclineId;
    @ApiModelProperty(hidden = true) //only response, not include in request
    private String reasonName;
    private String comment;
    private String priceExpected;
    private float priceSuggest;

    public DeclineTradeIn(Long bicycleId, long reasonDeclineId, String comment, String priceExpected, float priceSuggest, ConditionInventory condition) {
        this.bicycleId = bicycleId;
        this.reasonDeclineId = reasonDeclineId;
        this.comment = comment;
        this.priceExpected = priceExpected;
        this.priceSuggest = priceSuggest;
        this.condition = condition;
    }

    public DeclineTradeIn() {
    }

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getReasonDeclineId() {
        return reasonDeclineId;
    }

    public void setReasonDeclineId(long reasonDeclineId) {
        this.reasonDeclineId = reasonDeclineId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPriceExpected() {
        return priceExpected;
    }

    public void setPriceExpected(String priceExpected) {
        this.priceExpected = priceExpected;
    }

    public float getPriceSuggest() {
        return priceSuggest;
    }

    public void setPriceSuggest(float priceSuggest) {
        this.priceSuggest = priceSuggest;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getReasonName() {
        return reasonName;
    }

    @JsonIgnore //only response, not include in request
    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

}
