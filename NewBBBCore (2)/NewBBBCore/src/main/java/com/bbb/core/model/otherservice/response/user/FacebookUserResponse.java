package com.bbb.core.model.otherservice.response.user;


import com.fasterxml.jackson.annotation.JsonProperty;

public class FacebookUserResponse {
    private String id;
    @JsonProperty("facebook_token")
    private String facebookToken;
    @JsonProperty("facebook_avatar")
    private String facebookAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getFacebookAvatar() {
        return facebookAvatar;
    }

    public void setFacebookAvatar(String facebookAvatar) {
        this.facebookAvatar = facebookAvatar;
    }
}
