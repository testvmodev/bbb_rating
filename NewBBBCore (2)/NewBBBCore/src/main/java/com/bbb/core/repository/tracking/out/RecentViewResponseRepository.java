//package com.bbb.core.repository.tracking.out;
//
//import com.bbb.core.model.response.market.home.recentview.RecentViewResponse;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface RecentViewResponseRepository extends JpaRepository<RecentViewResponse, Long> {
//
//
//
//
//
//    @Query(nativeQuery = true, value = "SELECT " +
//            "tracking.id AS id, " +
//            "tracking.market_listing_id AS market_listing_id," +
//            "tracking.inventory_auction_id AS inventory_auction_id, " +
//            "inventory.id AS inventory_id, " +
//            "inventory.title AS inventory_title, " +
//            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.current_listed_price AS current_listed_price, " +
//            "inventory.discounted_price AS discounted_price, " +
//            "inventory.image_default AS image_default, " +
//            "tracking.type AS type, " +
//            "(CASE WHEN tracking.inventory_auction_id IS NOT NULL THEN " +
//                "inventory_auction.min " +
//            "ELSE NULL END) AS min_bid, " +
//            "max_price_offer.count_bid AS number_bids, "+
//            "tracking.last_viewed AS last_view_time, "+
//            "inventory.bicycle_size_name AS size_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name " +
//
//            "FROM tracking " +
//            "LEFT JOIN market_listing ON tracking.market_listing_id = market_listing.id " +
//            "LEFT JOIN inventory_auction ON tracking.inventory_auction_id = inventory_auction.id " +
//            "JOIN inventory ON " +
//            "(" +
//                "(tracking.market_listing_id IS NOT NULL AND market_listing.inventory_id = inventory.id) OR " +
//                "(tracking.inventory_auction_id IS NOT NULL AND inventory_auction.inventory_id = inventory.id)" +
//            ") " +
//
//            "LEFT JOIN " +
//                "(SELECT DISTINCT " +
//                    "inventory_auction.inventory_id AS max_inventory_id, " +
//                    "inventory_auction.auction_id AS max_auction_id,  " +
//                    "MAX(offer.offer_price) AS max_offer_price, " +
//                    "COUNT(offer.id) AS count_bid " +
//                    "FROM offer " +
//                    "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id  " +
//                    "WHERE offer.is_delete = 0 AND inventory_auction.auction_id IS NOT NULL " +
//                    "GROUP BY inventory_auction.inventory_id, inventory_auction.auction_id ) AS max_price_offer " +
//            "ON inventory_auction.inventory_id = max_price_offer.max_inventory_id AND inventory_auction.auction_id = max_price_offer.max_auction_id " +
//
//
//
//            "WHERE " +
//            "tracking.user_id = :userId  AND " +
//            "(:marketType IS NULL OR tracking.type = :marketType)  " +
//            "ORDER BY tracking.last_viewed DESC " +
//            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY")
//    List<RecentViewResponse> findAllTracking(
//            @Param(value = "userId") String userId,
//            @Param(value = "marketType") String marketType,
//            @Param(value = "offset") int offset,
//            @Param(value = "size") int size
//    );
//}
