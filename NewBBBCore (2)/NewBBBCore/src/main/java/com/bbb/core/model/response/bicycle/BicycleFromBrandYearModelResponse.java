package com.bbb.core.model.response.bicycle;

import com.bbb.core.model.response.embedded.ContentCommonId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class BicycleFromBrandYearModelResponse {
    @Id
    private long bicycleId;
    private String description;
    private String name;
    private long brandId;
    private String brandName;
    private long modelId;
    private String modelName;
    private long yearId;
    private String yearName;
    private long typeId;
    private String typeName;
    private Long sizeId;
    private String sizeName;
    private Long brakeTypeId;
    private String brakeTypeName;
    private String imageDefault;
    @Transient
    private List<ContentCommonId> yearModels;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public long getModelId() {
        return modelId;
    }

    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getSizeId() {
        return sizeId;
    }

    public void setSizeId(Long sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public String getBrakeTypeName() {
        return brakeTypeName;
    }

    public void setBrakeTypeName(String brakeTypeName) {
        this.brakeTypeName = brakeTypeName;
    }

    public List<ContentCommonId> getYearModels() {
        return yearModels;
    }

    public void setYearModels(List<ContentCommonId> yearModels) {
        this.yearModels = yearModels;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }
}
