package com.bbb.core.model.otherservice.request.mail.customquote.complete;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentCustomQuoteComplete {
    @JsonProperty("tp_name")
    private String tpName;
    @JsonProperty("bike_name")
    private String bikeName;
    @JsonProperty("employee_emails") //emails but only one email??
    private String employeeEmails;

    public String getTpName() {
        return tpName;
    }

    public void setTpName(String tpName) {
        this.tpName = tpName;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public String getEmployeeEmails() {
        return employeeEmails;
    }

    public void setEmployeeEmails(String employeeEmails) {
        this.employeeEmails = employeeEmails;
    }
}
