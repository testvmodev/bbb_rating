package com.bbb.core.model.request.inventory.update;

import com.bbb.core.model.request.inventory.create.InventoryCompDetailCreateRequest;
import com.bbb.core.model.request.inventory.create.InventoryLocationShippingCreateRequest;
import com.bbb.core.model.request.inventory.create.InventoryShippingCreatedRequest;
import com.bbb.core.model.request.inventory.create.InventoryUpgradeCompCreateRequest;

import java.util.List;

public class InventoryUpdateFullRequest {
    private InventoryBaseUpdateRequest baseInfo;
    private InventoryShippingCreatedRequest shipping;
    private List<InventoryImageUpdateRequest> images;
    private List<InventoryCompDetailCreateRequest> comps;
    private List<InventoryUpgradeCompCreateRequest> upgradeComps;
    private InventoryTradeInOwnerUpdateRequest tradeInOwner;
    private InventoryLocationShippingCreateRequest location;

    public InventoryBaseUpdateRequest getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(InventoryBaseUpdateRequest baseInfo) {
        this.baseInfo = baseInfo;
    }

    public InventoryShippingCreatedRequest getShipping() {
        return shipping;
    }

    public void setShipping(InventoryShippingCreatedRequest shipping) {
        this.shipping = shipping;
    }

    public List<InventoryImageUpdateRequest> getImages() {
        return images;
    }

    public void setImages(List<InventoryImageUpdateRequest> images) {
        this.images = images;
    }

    public List<InventoryCompDetailCreateRequest> getComps() {
        return comps;
    }

    public void setComps(List<InventoryCompDetailCreateRequest> comps) {
        this.comps = comps;
    }

    public List<InventoryUpgradeCompCreateRequest> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<InventoryUpgradeCompCreateRequest> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public InventoryTradeInOwnerUpdateRequest getTradeInOwner() {
        return tradeInOwner;
    }

    public void setTradeInOwner(InventoryTradeInOwnerUpdateRequest tradeInOwner) {
        this.tradeInOwner = tradeInOwner;
    }

    public InventoryLocationShippingCreateRequest getLocation() {
        return location;
    }

    public void setLocation(InventoryLocationShippingCreateRequest location) {
        this.location = location;
    }
}
