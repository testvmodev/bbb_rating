package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.auction.OfferOfAuctionResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OfferOfAuctionResponseRepository extends CrudRepository<OfferOfAuctionResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id AS id, " +
            "offer.status AS status, " +
//            "offer.auction_id AS auction_id, " +
//            "offer.inventory_id AS inventory_id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.auction_id AS auction_id " +
            "FROM offer " +

            "JOIN inventory_auction " +
            "ON inventory_auction.id = offer.inventory_auction_id " +
            "AND inventory_auction.auction_id IN :auctionIds " +

            "WHERE offer.inventory_auction_id IS NOT NULL"
    )
    List<OfferOfAuctionResponse> findAllFromAuction(
            @Param(value = "auctionIds") List<Long> auctionIds
    );

    //disabled due to no-use and going to be outdated
//    @Query(nativeQuery = true, value = "SELECT " +
//            "offer.id AS id, offer.status AS status, offer.auction_id AS auction_id, offer.inventory_id AS inventory_id " +
//            "FROM " +
//            "offer " +
//            "WHERE " +
//            "offer.auction_id IN :auctionIds AND " +
//            "offer.inventory_id IN :inventoryIds"
//    )
//    List<OfferOfAuctionResponse> findAll(
//            @Param(value = "auctionIds") List<Long> auctionIds,
//            @Param(value = "inventoryIds") List<Long> inventoryIds
//    );
}
