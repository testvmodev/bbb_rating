package com.bbb.core.model.database;

import com.bbb.core.model.database.type.JobTimeType;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ScheduleJobConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String jobType;
    private boolean isEnable;
    private JobTimeType jobTimeType;
    private Long jobTimeSeconds;
    private String jobTimeCron;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private boolean isDelete;
    private Boolean logFailEnable;
    private Boolean logCancelEnable;
    private Boolean logSuccessEnable;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }

    public JobTimeType getJobTimeType() {
        return jobTimeType;
    }

    public void setJobTimeType(JobTimeType jobTimeType) {
        this.jobTimeType = jobTimeType;
    }

    public Long getJobTimeSeconds() {
        return jobTimeSeconds;
    }

    public void setJobTimeSeconds(Long jobTimeSeconds) {
        this.jobTimeSeconds = jobTimeSeconds;
    }

    public String getJobTimeCron() {
        return jobTimeCron;
    }

    public void setJobTimeCron(String jobTimeCron) {
        this.jobTimeCron = jobTimeCron;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Boolean getLogFailEnable() {
        return logFailEnable;
    }

    public void setLogFailEnable(Boolean logFailEnable) {
        this.logFailEnable = logFailEnable;
    }

    public Boolean getLogCancelEnable() {
        return logCancelEnable;
    }

    public void setLogCancelEnable(Boolean logCancelEnable) {
        this.logCancelEnable = logCancelEnable;
    }

    public Boolean getLogSuccessEnable() {
        return logSuccessEnable;
    }

    public void setLogSuccessEnable(Boolean logSuccessEnable) {
        this.logSuccessEnable = logSuccessEnable;
    }
}
