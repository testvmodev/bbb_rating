package com.bbb.core.model.request.shipping;

import javax.validation.constraints.NotNull;

public class ShippingFeeConfigCreateRequest {
    @NotNull
    private Long bicycleTypeId;
    @NotNull
    private Long ebayShippingProfileId;
    @NotNull
    private Float shippingFee;
    @NotNull
    private String label;
    @NotNull
    private String description;

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public Long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(Long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public Float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(Float shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
