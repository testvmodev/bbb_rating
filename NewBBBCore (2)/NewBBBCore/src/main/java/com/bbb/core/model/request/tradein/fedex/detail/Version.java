package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Version {
    @JacksonXmlProperty(localName = "ServiceId")
    private String serviceId;
    @JacksonXmlProperty(localName = "Major")
    private int major;
    @JacksonXmlProperty(localName = "Intermediate")
    private int intermediate;
    @JacksonXmlProperty(localName = "Minor")
    private int minor;

    public Version() {}

    public Version(String serviceId, int majorVersion) {
        this.serviceId = serviceId;
        this.major = majorVersion;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getIntermediate() {
        return intermediate;
    }

    public void setIntermediate(int intermediate) {
        this.intermediate = intermediate;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }
}
