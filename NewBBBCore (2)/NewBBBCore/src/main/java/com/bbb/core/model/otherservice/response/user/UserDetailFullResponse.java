package com.bbb.core.model.otherservice.response.user;

import com.bbb.core.model.otherservice.response.user.full.NormalUserResponse;
import com.bbb.core.model.otherservice.response.user.full.OnlineStoreUserResponse;
import com.bbb.core.model.otherservice.response.user.full.PartnerUserResponse;
import com.bbb.core.model.otherservice.response.user.full.StorefrontResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

public class UserDetailFullResponse {
    private String provider;
    @JsonProperty("os_type")
    private String osType;
    @JsonProperty("_id")
    private String id;
    private String email;
    private String role;
    @JsonProperty("display_name")
    private String displayName;
    @Nullable
    private NormalUserResponse normal;
    @Nullable
    private PartnerUserResponse partner;
    @Nullable
    @JsonProperty("online_store")
    private OnlineStoreUserResponse onlineStore;
    @Nullable
    private StorefrontResponse storefront;
    private String gravatar;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public NormalUserResponse getNormal() {
        return normal;
    }

    public void setNormal(@Nullable NormalUserResponse normal) {
        this.normal = normal;
    }

    public PartnerUserResponse getPartner() {
        return partner;
    }

    public void setPartner(PartnerUserResponse partner) {
        this.partner = partner;
    }

    @Nullable
    public OnlineStoreUserResponse getOnlineStore() {
        return onlineStore;
    }

    public void setOnlineStore(@Nullable OnlineStoreUserResponse onlineStore) {
        this.onlineStore = onlineStore;
    }

    @Nullable
    public StorefrontResponse getStorefront() {
        return storefront;
    }

    public void setStorefront(@Nullable StorefrontResponse storefront) {
        this.storefront = storefront;
    }

    public String getGravatar() {
        return gravatar;
    }

    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }
}
