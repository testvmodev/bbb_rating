package com.bbb.core.service.inventory;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.inventory.InventoryCreateRequest;
import com.bbb.core.model.request.inventory.InventoryRequest;
import com.bbb.core.model.request.inventory.create.InventoryCreateFullRequest;
import com.bbb.core.model.request.inventory.secret.InventoryUpdateSecretRequest;
import com.bbb.core.model.request.inventory.update.InventoryUpdateFullRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingSaleRequest;
import com.bbb.core.security.user.UserContext;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

public interface InventoryService {
    Object getInventories(InventoryRequest request, Pageable page) throws ExceptionResponse;

    Object importIdSaleForceId();

    Object getInventoryDetail(long id) throws ExceptionResponse;

    Object importInventoryImage();

//    Object createInventory(InventoryCreateRequest request) throws ExceptionResponse;
    Object createInventory(InventoryCreateFullRequest request) throws ExceptionResponse;

    Object updateInventory(long inventoryId, InventoryUpdateFullRequest request) throws ExceptionResponse, MethodArgumentNotValidException;

    Object getSecretInventories(List<Long> inventoriesId) throws ExceptionResponse;

    Object soldInventory(MarketListingSaleRequest request) throws ExceptionResponse;

    Object updateInventorySecret(
            long inventoryId, InventoryUpdateSecretRequest request
    ) throws ExceptionResponse;
}
