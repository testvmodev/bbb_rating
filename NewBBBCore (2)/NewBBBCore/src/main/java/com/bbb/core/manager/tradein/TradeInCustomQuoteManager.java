package com.bbb.core.manager.tradein;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.s3.S3UploadImage;
import com.bbb.core.common.utils.s3.S3UploadResponse;
import com.bbb.core.common.utils.s3.S3UploadUtils;
import com.bbb.core.manager.bicycle.BrandManager;
import com.bbb.core.manager.bicycle.ModelManager;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bicycle.YearManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.templaterest.RestSendingMailManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.embedded.TradeInCustomQuoteCompDetailId;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StatusTradeIn;
import com.bbb.core.model.database.type.StatusTradeInCustomQuote;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.mail.SendingMailRequest;
import com.bbb.core.model.request.mail.customquote.ChangeValueCustomQuoteContentMailRequest;
import com.bbb.core.model.request.mail.customquote.ReviewedCustomQuoteContentMailRequest;
import com.bbb.core.model.request.mail.customquote.NewCustomQuoteContentMailRequest;
import com.bbb.core.model.request.mail.offeraccept.content.ReceiveSendingMailRequest;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.saleforce.create.SaleforceInvCompRequest;
import com.bbb.core.model.request.tradein.customquote.*;
import com.bbb.core.model.request.year.YearCreateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import com.bbb.core.model.response.mail.BaseMailResponse;
import com.bbb.core.model.response.tradein.TradeInUpgradeCompResponse;
import com.bbb.core.model.response.tradein.customquote.*;
import com.bbb.core.repository.BicycleBrandRepository;
import com.bbb.core.repository.BicycleImageRepository;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.BicycleYearRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicyclemodel.BicycleModelRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeSelectRepository;
import com.bbb.core.repository.reout.BicycleSizeRepository;
import com.bbb.core.repository.reout.BrandModelYearRepository;
import com.bbb.core.repository.tradein.*;
import com.bbb.core.repository.tradein.out.TradeInCustomQuoteResponseRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TradeInCustomQuoteManager extends TradeInBaseManager implements MessageResponses {
    //region Beans
    @Autowired
    private TradeInCustomQuoteRepository tradeInCustomQuoteRepository;
    @Autowired
    private BrandModelYearRepository brandModelYearRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private BrandManager brandManager;
    @Autowired
    private ModelManager modelManager;
    @Autowired
    private BicycleYearRepository bicycleYearRepository;
    @Autowired
    private TradeInRepository tradeInRepository;
    @Autowired
    private BicycleImageRepository bicycleImageRepository;
    @Autowired
    private TradeInUpgradeCompDetailRepository tradeInUpgradeCompDetailRepository;
    @Autowired
    private TradeInUpgradeCompRepository tradeInUpgradeCompRepository;
    @Autowired
    private TradeInCustomQuoteResponseRepository tradeInCustomQuoteResponseRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private BicycleBrandRepository bicycleBrandRepository;
    @Autowired
    private BicycleModelRepository bicycleModelRepository;
    @Autowired
    private TradeInCustomQuoteCompDetailRepository tradeInCustomQuoteCompDetailRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InventoryCompTypeSelectRepository inventoryCompTypeSelectRepository;
    @Autowired
    private RestSendingMailManager restSendingMailManager;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private TradeInPriceConfigRepository tradeInPriceConfigRepository;
    @Autowired
    private BicycleSizeRepository bicycleSizeRepository;
    @Autowired
    @Qualifier("mvcValidator")
    private Validator validator;
    @Autowired
    private AwsResourceConfig awsResourceConfig;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private YearManager yearManager;
    //endregion

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getTradeInCustomQuotes(
            TradeInCustomQuoteRequest request
    ) throws ExceptionResponse {
        String partnerId = CommonUtils.getPartnerId();
        if (CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            partnerId = request.getPartnerId();
        } else {
            if (partnerId == null ||
                    (request.getPartnerId() != null && !partnerId.equals(request.getPartnerId()))
            ) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION), HttpStatus.OK);
            }
        }


        ListObjResponse<TradeInCustomQuoteResponse> response = new ListObjResponse<>();
        response.setData(
                tradeInCustomQuoteResponseRepository.findAllSorted(
                        partnerId,
                        partnerId == null ? null : request.getArchived(),
                        partnerId == null ? request.getArchived() : null,
                        request.getIncomplete(),
                        request.getCustomQuoteStatus() == null ? null : request.getCustomQuoteStatus().getValue(),
                        request.getContent(),
                        request.getSortField(),
                        request.getSortType(),
                        request.getPageable().getOffset(),
                        request.getPageable().getPageSize())
        );
//        response.setData(
//                response.getData().stream().peek(o -> {
//                    o.setTitleBicycle(o.getYearName() + " " + o.getBrandName() + " " + o.getModelName());
//                }).collect(Collectors.toList())
//        );
        response.setTotalItem(tradeInCustomQuoteResponseRepository.getTotalNumber(
                partnerId,
                partnerId == null ? null : request.getArchived(),
                partnerId == null ? request.getArchived() : null,
                request.getIncomplete(),
                request.getContent(),
                request.getCustomQuoteStatus() == null ? null : request.getCustomQuoteStatus().getValue())
        );
        response.setPageSize(request.getPageable().getPageSize());
        response.updateTotalPage();
        return response;

    }

    public Object getDetailCustomQuote(long customQuoteId) throws ExceptionResponse {
        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(customQuoteId);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, NOT_FOUND_CUSTOM_QUOTE), HttpStatus.NOT_FOUND);
        }
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuote.getId());

        CommonUtils.validRolePartner(tradeIn);

        TradeInCustomQuoteDetailResponse response = modelMapper.map(tradeInCustomQuote, TradeInCustomQuoteDetailResponse.class);
        //only admin can see review note
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            response.setAdminReviewNote(null);
        }
        response.setTradeIn(tradeInRepository.findOneByCustomQuoteId(response.getId()));
        if (response.getTypeId() != null) {
            BicycleType bicycleType = bicycleTypeRepository.findOne(response.getTypeId());
            if (bicycleType != null) {
                response.setTypeName(bicycleType.getName());
            }
        }
        float tradeInValue = 0;
        if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PROVIDED_VALUE) {
            tradeInValue = tradeInCustomQuote.getTradeInValue();
        }
        List<TradeInCustomQuoteConditionResponse> conditions = new ArrayList<>();
        conditions.add(new TradeInCustomQuoteConditionResponse(ConditionInventory.EXCELLENT, Constants.EXCELLENT_PERCENT, Constants.EXCELLENT_MSG, tradeInValue * Constants.EXCELLENT_PERCENT));
        conditions.add(new TradeInCustomQuoteConditionResponse(ConditionInventory.VERY_GOOD, Constants.VERY_GOOD_PERCENT, Constants.VERY_GOOD_MSG, tradeInValue * Constants.VERY_GOOD_PERCENT));
        conditions.add(new TradeInCustomQuoteConditionResponse(ConditionInventory.GOOD, Constants.GOOD_PERCENT, Constants.GOOD_MSG, tradeInValue * Constants.GOOD_PERCENT));
        conditions.add(new TradeInCustomQuoteConditionResponse(ConditionInventory.FAIR, Constants.FAIR_PERCENT, Constants.FAIR_MSG, tradeInValue * Constants.FAIR_PERCENT));

        for (TradeInCustomQuoteConditionResponse condition : conditions) {
            if (condition.getCondition() == response.getTradeIn().getCondition()) {
                condition.setSelect(true);
                break;
            }
        }
        response.setConditions(conditions);
        response.setTradeInImages(tradeInImageRepository.findAllByTradeInCustomQuoteId(response.getId()));

        List<Long> upgradeCompIds = tradeInUpgradeCompDetailRepository.findAllByTradeInId(response.getTradeIn().getId())
                .stream().map(TradeInUpgradeCompDetail::getTradeInUpgradeCompId).collect(Collectors.toList());
        if (upgradeCompIds.size() > 0) {
            response.setUpgradeComps(
                    tradeInUpgradeCompRepository.findAll(upgradeCompIds)
                            .stream().map(o -> {
                        TradeInUpgradeCompResponse re = new TradeInUpgradeCompResponse();
                        re.setId(o.getId());
                        re.setName(o.getName());
                        re.setPercentValue(o.getPercentValue());
                        re.setUp(o.isUp());
                        return re;
                    }).collect(Collectors.toList())
            );
        }
        if (tradeInCustomQuote.getTradeInValue() != null) {
            response.setTradeInPriceConfigs(
                    TradeInUtil.getPriceConfig(tradeInPriceConfigRepository, tradeInCustomQuote.getTradeInValue(), null)
            );
        }

        List<TradeInCustomQuoteCompDetail> compDetails = tradeInCustomQuoteCompDetailRepository.findAllByCustomQuote(customQuoteId);
        List<Long> compIds = compDetails.stream().map(o -> o.getId().getCompTypeId()).collect(Collectors.toList());
        List<InventoryCompType> comTypes;
        if (compIds.size() > 0) {
            comTypes = inventoryCompTypeRepository.findAllByIds(compIds);
        } else {
            comTypes = new ArrayList<>();
        }
        response.setComps(
                compDetails.stream().map(o -> {
                    TradeInCustomQuoteCompResponse re = new TradeInCustomQuoteCompResponse();
                    re.setComponentId(o.getId().getCompTypeId());
                    re.setValue(o.getValue());
                    InventoryCompType coo = CommonUtils.findObject(comTypes, co -> co.getId() == o.getId().getCompTypeId());
                    if (coo != null) {
                        re.setName(coo.getName());
                    }
                    return re;
                }).collect(Collectors.toList())
        );


//        BicycleSize bicycleSize = bicycleSizeRepository.findOne(tradeInCustomQuote.getSizeId());
//        if (bicycleSize != null) {
//            response.setSizeName(bicycleSize.getName());
//        }
        if (response.getComps() != null && response.getComps().size() > 0) {
            TradeInCustomQuoteCompResponse comp = CommonUtils.findObject(response.getComps(), c -> c.getName().equals(ValueCommons.INVENTORY_FRAME_SIZE_NAME));
            if  (comp != null) {
                response.setSizeName(comp.getValue());
            }
        }

        return response;
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Object createCustomQuote(TradeInCustomQuoteCreateRequest request) throws ExceptionResponse, MethodArgumentNotValidException {
        if (request.getDraft() == null) {
            request.setDraft(false);
        }

        //manual trigger validating
        if (!request.getDraft()) {
            BeanPropertyBindingResult errors = new BeanPropertyBindingResult(request, "request");
            validator.validate(request, errors);
            Method currentMethod = new Object() {
            }.getClass().getEnclosingMethod();
            if (errors.hasErrors()) {
                throw new MethodArgumentNotValidException(
                        new MethodParameter(currentMethod, 0),
                        errors);
            }
        }
        //end

        CommonUtils.checkYearValid(request.getYearName());

        //valid email
        if (request.getEmployeeEmail() != null || !request.getDraft()) {
            CommonUtils.checkEmailFormatValid(request.getEmployeeEmail());
        }

        //check partner
        String userId = CommonUtils.getUserLogin();
        UserDetailFullResponse user = authManager.getUserFullInfo(userId);
        String partnerId = CommonUtils.getPartnerId();
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            if (partnerId == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, PARTNER_ID_INVALID_OR_MUST_BE_NOT_EMPTY_PARTNER),
                        HttpStatus.FORBIDDEN);
            }
        }


        //size now is from component frame size
        //check size (old)
//        if (bicycleSizeRepository.getCount(request.getSizeId()) == 0) {
//            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BICYCLE_SIZE_NOT_EXIST),
//                    HttpStatus.BAD_REQUEST);
//        }

        //valid upgrade component component
        validUpgradeComponents(request.getUpgradeComponentsId());

//        if (bicycleTypeRepository.getCount(request.getTypeId()) == 0) {
//            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
//                    TYPE_ID_NOT_EXIST),
//                    HttpStatus.BAD_REQUEST);
//        }

        BicycleType bicycleType = null;
        if (request.getTypeId() != null) {
            bicycleType = bicycleTypeRepository.findOne(request.getTypeId());
            if (bicycleType == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        TYPE_ID_NOT_EXIST),
                        HttpStatus.BAD_REQUEST);
            }

            if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
            //not required anymore even with type ebike
//                if (!request.getDraft()) {
//                    if (request.geteBikeMileage() == null) {
//                        throw new ExceptionResponse(
//                                ObjectError.ERROR_PARAM,
//                                EBIKE_MILEAGE_MUST_NOT_EMPTY,
//                                HttpStatus.BAD_REQUEST
//                        );
//                    }
//                    if (request.geteBikeHours() == null) {
//                        throw new ExceptionResponse(
//                                ObjectError.ERROR_PARAM,
//                                EBIKE_HOUR_MUST_NOT_EMPTY,
//                                HttpStatus.BAD_REQUEST
//                        );
//                    }
//                }
                if (request.geteBikeMileage() != null && request.geteBikeMileage() < 0) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            EBIKE_MILEAGE_MUST_BE_POSITIVE,
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (request.geteBikeHours() != null && request.geteBikeHours() < 0) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            EBIKE_HOUR_MUST_BE_POSITIVE,
                            HttpStatus.BAD_REQUEST
                    );
                }
            }
        } else if (!request.getDraft()) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    TYPE_ID_NOT_EXIST,
                    HttpStatus.BAD_REQUEST
            );
        }


        //check component type
        List<String> required = request.getDraft() ? null : TradeInUtil.getRequiredCustomQuoteComponents();
        validCustomQuoteCompType(request.getCompCustomQuotes(), false, required);
        //end valid


        request.validNormal();
        if (request.getYearId() != null) {
            request.setYearName(request.getYearId() + "");
        }
        BrandModelYear brandModelYear =
                brandModelYearRepository.findOne(
                        request.getBrandId(),
                        request.getBrandName(),
                        request.getModelId(),
                        request.getModelName(),
                        request.getYearName(),
                        System.currentTimeMillis());
        if (brandModelYear == null) {
            if (request.getBrandId() != null || request.getModelId() != null || request.getYearId() != null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, NOT_FOUND_BRAND_MODEL_YEAR),
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            if (brandModelYear.getBrandId() != null) {
                request.setBrandId(brandModelYear.getBrandId());
                request.setBrandName(brandModelYear.getBrandName());
            } else {
                if (request.getBrandId() != null) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                            HttpStatus.BAD_REQUEST);
                }
                //create new brand
                if (request.getBrandName() != null) {
                    BrandCreateRequest brandRequest = new BrandCreateRequest();
                    brandRequest.setName(request.getBrandName());
                    brandRequest.setValueModifier(0.0f);
                    BicycleBrand bicycleBrand = brandManager.createBrandIgnoreRole(brandRequest);
                    request.setBrandId(bicycleBrand.getId());
                }
            }

            if (brandModelYear.getModelId() != null) {
                request.setModelId(brandModelYear.getModelId());
                request.setModelName(brandModelYear.getModelName());
            } else {
                if (request.getModelId() != null) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MODEL_ID_NOT_EXIST),
                            HttpStatus.BAD_REQUEST);
                }
                //create new model
                if (request.getModelName() != null) {
                    ModelCreateRequest modelRequest = new ModelCreateRequest();
                    modelRequest.setBrandId(request.getBrandId());
                    modelRequest.setName(request.getModelName());
                    BicycleModel bicycleModel = modelManager.createModelIgnoreRole(modelRequest);
                    request.setModelId(bicycleModel.getId());
                }
            }

            if (brandModelYear.getYearId() != null) {
                request.setYearId(brandModelYear.getYearId());
                request.setYearName(brandModelYear.getYearName());
            } else {
                if (request.getYearId() != null) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_NOT_EXIST),
                            HttpStatus.BAD_REQUEST);
                }
                //create new year
                if (request.getYearName() != null) {
                    YearCreateRequest yearRequest = new YearCreateRequest();
                    yearRequest.setName(request.getYearName());
                    BicycleYear bicycleYear = yearManager.createYearIgnoreRole(yearRequest);
                    request.setYearId(bicycleYear.getId());
                }
            }

        }


        TradeInCustomQuote tradeInCustomQuote = new TradeInCustomQuote();
        tradeInCustomQuote.setBrandId(request.getBrandId());
        tradeInCustomQuote.setBrandName(request.getBrandName());
        tradeInCustomQuote.setModelId(request.getModelId());
        tradeInCustomQuote.setModelName(request.getModelName());
        tradeInCustomQuote.setYearId(request.getYearId());
        tradeInCustomQuote.setYearName(request.getYearName());
        if (request.getBrandId() != null && request.getModelId() != null && request.getYearId() != null) {
            int countBicycle = bicycleRepository.getNumber(request.getBrandId(), request.getModelId(), request.getYearId());
            if (countBicycle > 0) {
                tradeInCustomQuote.setCreateFromBicycle(true);
            } else {
                tradeInCustomQuote.setCreateFromBicycle(false);
            }
        } else {
            tradeInCustomQuote.setCreateFromBicycle(false);
        }
        tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.INCOMPLETE_DETAILS);
        tradeInCustomQuote.setTypeId(request.getTypeId());
        tradeInCustomQuote.setUserCreatedId(CommonUtils.getUserLogin());
        tradeInCustomQuote.setNote(request.getNote());
        tradeInCustomQuote.setEmployeeName(request.getEmployeeName());
        tradeInCustomQuote.setEmployeeEmail(request.getEmployeeEmail());
        tradeInCustomQuote.setEmployeeLocation(request.getEmployeeLocation());
        tradeInCustomQuote.setOwnerName(request.getOwnerName());
//        tradeInCustomQuote.setSizeId(request.getSizeId());
        if (bicycleType != null && bicycleType.getName().equals(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
            tradeInCustomQuote.seteBikeMileage(request.geteBikeMileage());
            tradeInCustomQuote.seteBikeHours(request.geteBikeHours());
        }
        tradeInCustomQuote.setClean(request.getClean());

        tradeInCustomQuote = tradeInCustomQuoteRepository.save(tradeInCustomQuote);
        long tradeInCustomQuoteId = tradeInCustomQuote.getId();

        //save comp type
        insertTradeInCustomCompDetail(request.getCompCustomQuotes(), tradeInCustomQuoteId);
        //end


        TradeIn tradeIn = new TradeIn();
        tradeIn.setCustomQuoteId(tradeInCustomQuote.getId());
        tradeIn.setCondition(request.getCondition());
        tradeIn.setOwnerName(request.getOwnerName());
//        dfdf
        tradeIn.setPartnerId(partnerId);
        tradeIn.setPartnerAddress(CommonUtils.getPartnerAddress(user));
        tradeIn.setPartnerName(CommonUtils.getPartnerName(user));
        tradeIn.setUserCreatedId(CommonUtils.getUserLogin());
        tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue());
        tradeIn.setTitle(request.getYearName() + " " + request.getBrandName() + " " + request.getModelName());
        tradeIn.setSave(request.getDraft());
        tradeIn = tradeInRepository.save(tradeIn);

        //save comps
        insertUpgradeComponent(request.getUpgradeComponentsId(), tradeIn.getId());
        //end

        TradeInCustomQuoteCreateResponse response = new TradeInCustomQuoteCreateResponse();
        response.setCustomQuoteId(tradeInCustomQuote.getId());
        response.setTradeInId(tradeIn.getId());
        return response;
    }

    private void validUpgradeComponents(List<Long> upgradeComponentIds) throws ExceptionResponse {
        if (upgradeComponentIds != null && upgradeComponentIds.size() > 0) {
            int count = tradeInUpgradeCompRepository.getCountUpgradeComponent(upgradeComponentIds);
            if (count != upgradeComponentIds.size()) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_UPGRADE_COMP_INVALID),
                        HttpStatus.BAD_REQUEST);
            }
        }
    }

    public void insertUpgradeComponent(List<Long> upgradeCompIds, long tradeInId) {
        if (upgradeCompIds != null && upgradeCompIds.size() > 0) {
            tradeInUpgradeCompDetailRepository.saveAll(
                    upgradeCompIds.stream().map(id -> {
                        TradeInUpgradeCompDetail detail = new TradeInUpgradeCompDetail();
                        detail.setTradeInId(tradeInId);
                        detail.setTradeInUpgradeCompId(id);
                        return detail;
                    }).collect(Collectors.toList())
            );
        }
    }

    private void insertTradeInCustomCompDetail(List<CompRequest> cus, long tradeInCustomQuoteId) {
        if (cus != null && cus.size() > 0) {
            List<TradeInCustomQuoteCompDetail> tradeInCustomQuoteCompDetails =
                    cus.stream().map(o -> {
                        TradeInCustomQuoteCompDetail re = new TradeInCustomQuoteCompDetail();
                        TradeInCustomQuoteCompDetailId id = new TradeInCustomQuoteCompDetailId();
                        id.setCompTypeId(o.getCompId());
                        id.setTradeInCustomQuoteId(tradeInCustomQuoteId);
                        re.setId(id);
                        re.setValue(o.getValue());
                        return re;
                    }).collect(Collectors.toList());
            tradeInCustomQuoteCompDetailRepository.saveAll(tradeInCustomQuoteCompDetails);
        }
    }

    public void validCustomQuoteCompType(List<CompRequest> comps) throws ExceptionResponse {
        validCustomQuoteCompType(comps, false, null);
    }

    public void validCustomQuoteCompType(List<CompRequest> comps, boolean validateSuspension, List<String> requiredComps) throws ExceptionResponse {
        if (comps != null && comps.size() > 0) {
            validateComps(comps);
            List<Long> compIds = comps.stream()
                    .map(CompRequest::getCompId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            if (compIds.size() < comps.size()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        COMPONENT_ID_MUST_NOT_NULL,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            CommonUtils.stream(compIds);
            if (compIds.size() < comps.size()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        COMPONENT_ID_MUST_NOT_DUPLICATE,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByIds(compIds);
            if (inventoryCompTypes.size() != compIds.size()) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        COMPONENT_TYPE_INVALID),
                        HttpStatus.BAD_REQUEST);
            }
            if (requiredComps != null && requiredComps.size() > 0) {
                for (String compName : requiredComps) {
                    InventoryCompType type = CommonUtils.findObject(inventoryCompTypes, t -> t.getName().equals(compName));
                    if (type == null) {
                        throw new ExceptionResponse(
                                ObjectError.ERROR_PARAM,
                                MISSING_REQUIRED_COMPONENTS,
                                HttpStatus.BAD_REQUEST
                        );
                    }
                }
            }
            validCustomQuoteCompValue(comps, inventoryCompTypes, validateSuspension, true);
        } else if (requiredComps != null && requiredComps.size() > 0) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    MISSING_REQUIRED_COMPONENTS,
                    HttpStatus.BAD_REQUEST
            );
        }
    }

//    public List<CompRequest> validateSaleforceCompType(List<SaleforceInvCompRequest> comps) throws ExceptionResponse {
//        return validateSaleforceCompType(comps, false);
//    }

    public List<CompRequest> validateSaleforceCompType(
            List<SaleforceInvCompRequest> comps,
            boolean ignoredUnknownType
    ) throws ExceptionResponse {
        if (comps == null) {
            return null;
        }
        if (comps.size() < 1) {
            return new ArrayList<>();
        }
//        if (comps != null && comps.size() > 0) {
            List<String> compNames = comps.stream()
                    .map(c -> c.getCompTypeName())
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            if (compNames.size() < comps.size()) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        COMPONENT_ID_MUST_NOT_NULL,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            int oldsize = compNames.size();
            compNames = compNames.stream()
                    .distinct()
                    .collect(Collectors.toList());
            if (compNames.size() < oldsize) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        COMPONENT_ID_MUST_NOT_DUPLICATE,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByName(compNames);
            if (inventoryCompTypes.size() < comps.size() && !ignoredUnknownType) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        COMPONENT_TYPE_INVALID),
                        HttpStatus.BAD_REQUEST);
            }

            List<String> inventoryCompNames = inventoryCompTypes.stream()
                .map(i -> i.getName())
                .collect(Collectors.toList());
            List<CompRequest> compRequests = comps.stream()
                    .map(c -> {
                        if (ignoredUnknownType) {
                            if (!inventoryCompNames.stream().anyMatch(c.getCompTypeName()::equalsIgnoreCase)) {
                                return null;
                            }
                        }

                        CompRequest request = new CompRequest();
                        request.setValue(c.getValue());
                        InventoryCompType type = CommonUtils.findObject(inventoryCompTypes, t -> t.getName().equalsIgnoreCase(c.getCompTypeName()));
                        request.setCompId(type.getId());

                        return request;
                    }).filter(Objects::nonNull)
                    .collect(Collectors.toList());
            validCustomQuoteCompValue(compRequests, inventoryCompTypes, false, false);
            return compRequests;
//        }
    }

    private void validCustomQuoteCompValue(
            List<CompRequest> comps, List<InventoryCompType> inventoryCompTypes,
            boolean validateSuspension, boolean validateValueOfCompSelect
    ) throws ExceptionResponse {
        //validate blank value
        validateComps(comps);

        //
        if (validateValueOfCompSelect){
            List<InventoryCompType> typeSelects = inventoryCompTypes.stream().filter(InventoryCompType::isSelect).collect(Collectors.toList());
            if (typeSelects.size() > 0) {
                List<String> typeIdValues = new ArrayList<>();
                for (CompRequest compCustomQuoteRequest : comps) {
                    for (InventoryCompType typeSelect : typeSelects) {
                        if (compCustomQuoteRequest.getCompId() == typeSelect.getId()) {
                            typeIdValues.add(compCustomQuoteRequest.getCompId() + "_" + compCustomQuoteRequest.getValue());
                            break;
                        }
                    }
                }
                if (typeIdValues.size() > 0) {
                    int countTypeValue = inventoryCompTypeSelectRepository.getCountTypeValue(typeIdValues);
                    if (countTypeValue != typeIdValues.size()) {
                        throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                                VALUE_COMPONENT_TYPE_INVALID),
                                HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }

        if (validateSuspension) {
            boolean hasSuspension = false;
            for (InventoryCompType type : inventoryCompTypes) {
                if (type.getName().equalsIgnoreCase(ValueCommons.SUSPENSION_NAME_INV_COMP)) {
                    hasSuspension = true;
                    break;
                }
            }
            if (!hasSuspension) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        SUSPENSION_MUST_NOT_EMPTY,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }

    public Object updateTradeInCustomQuote(long customQuoteId, TradeInCustomQuoteUpdateRequest request) throws ExceptionResponse {
        boolean isAdmin = false;
        if (CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            isAdmin = true;
        }

        if (request.getSubmit() == null) {
            request.setSubmit(true);
        }

        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(customQuoteId);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_CUSTOM_QUOTE_NOT_FOUND),
                    HttpStatus.NOT_FOUND);
        }

        if (!isAdmin) {
            if (!tradeInCustomQuote.getUserCreatedId().equals(CommonUtils.getUserLogin())) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                        HttpStatus.NOT_FOUND);
            }
        }

        if (tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.INCOMPLETE_DETAILS) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS,
                    HttpStatus.BAD_REQUEST
            );
        }

        //value will be set by admin
//        if (request.getTradeInValue() != null) {
//            if (request.getTradeInValue() <= 0) {
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_VALUE_MUST_POSITIVE),
//                        HttpStatus.NOT_FOUND);
//            }
//        } else {
//            if (request.getSubmit() && tradeInCustomQuote.getTradeInValue() == null) {
//                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_VALUE_NOT_EMPTY),
//                        HttpStatus.NOT_FOUND);
//            }
//        }

        if (request.getSubmit()) {
            validateCustomQuoteEnoughImages(tradeInCustomQuote.getId());
        }

        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(customQuoteId);
        if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PROVIDED_VALUE) {
            if (isAdmin) {
                if (request.getTradeInValue() != null) {
                    tradeInCustomQuote.setTradeInValue(request.getTradeInValue());
                    tradeInCustomQuote = tradeInCustomQuoteRepository.save(tradeInCustomQuote);
                    tradeIn.setValue(request.getTradeInValue());
                    tradeInRepository.save(tradeIn);
                }
                return tradeInCustomQuote;
            } else {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                        HttpStatus.BAD_REQUEST);
            }
        }


        if (tradeIn.getStatusId() == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                    HttpStatus.BAD_REQUEST);
        }

        if (request.getYearName() != null && request.getYearId() != null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_MUST_CHOOSE_YEAR_ID_OR_YEAR_NAME),
                    HttpStatus.BAD_REQUEST);
        } else if (request.getSubmit()
                && (request.getYearId() == null && request.getYearName() == null)
                && (tradeInCustomQuote.getYearId() == null && tradeInCustomQuote.getYearName() == null)
        ) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_ONLY_IS_ID_OR_NAME),
                    HttpStatus.BAD_REQUEST);
        }
        if (request.getBrandName() != null && request.getBrandId() != null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_MUST_CHOOSE_BRAND_ID_OR_BRAND_NAME),
                    HttpStatus.BAD_REQUEST);
        } else if (request.getSubmit()
                && (request.getBrandId() == null && request.getBrandName() == null)
                && (tradeInCustomQuote.getBrandId() == null && tradeInCustomQuote.getBrandName() == null)
        ) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BRAND_ONLY_IS_ID_OR_NAME),
                    HttpStatus.BAD_REQUEST);
        }
        if (request.getModelName() != null && request.getModelId() != null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_MUST_CHOOSE_BRAND_ID_OR_BRAND_NAME),
                    HttpStatus.BAD_REQUEST);
        } else if (request.getSubmit()
                && (request.getModelId() == null && request.getModelName() == null)
                && (tradeInCustomQuote.getModelId() == null && tradeInCustomQuote.getModelName() == null)
        ) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MODEL_ONLY_IS_ID_OR_NAME),
                    HttpStatus.BAD_REQUEST);
        }

        validateComps(request.getCompCustomQuotes());
        //valid upgrade component component
        validUpgradeComponents(request.getUpgradeComponentsId());

        //valid comp update
        if (request.getCompCustomQuotes() != null && request.getCompCustomQuotes().size() > 0) {
            List<String> required = TradeInUtil.getRequiredCustomQuoteComponents();
            if (request.getSubmit()) {
                validCustomQuoteCompType(request.getCompCustomQuotes(), false, required);
            } else {
                validCustomQuoteCompType(request.getCompCustomQuotes());
            }
        } else if (request.getSubmit()) {
            List<TradeInCustomQuoteCompDetail> comps = tradeInCustomQuoteCompDetailRepository.findAllByCustomQuote(tradeInCustomQuote.getId());
            List<String> required = TradeInUtil.getRequiredCustomQuoteComponents();
            validateCustomQuoteComps(comps, required);
        }

        if (request.getBrandId() != null) {
            BicycleBrand bicycleBrand = bicycleBrandRepository.findOneActive(request.getBrandId());
            if (bicycleBrand == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            tradeInCustomQuote.setBrandId(request.getBrandId());
            tradeInCustomQuote.setBrandName(bicycleBrand.getName());
        }
        //value will be set by admin
//        if (request.getTradeInValue() != null) {
//            tradeInCustomQuote.setTradeInValue(request.getTradeInValue());
//        }
        if (request.getBrandName() != null) {
            tradeInCustomQuote.setBrandName(request.getBrandName());
            BicycleBrand bicycleBrand = bicycleBrandRepository.findOneByNameActive(request.getBrandName());
            if (bicycleBrand != null) {
                tradeInCustomQuote.setBrandId(bicycleBrand.getId());
            } else {
                tradeInCustomQuote.setBrandId(null);
            }
        }
        if (request.getModelId() != null) {
            BicycleModel bicycleModel = bicycleModelRepository.findOneActive(request.getModelId());
            if (bicycleModel == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MODEL_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            tradeInCustomQuote.setModelId(bicycleModel.getId());
            tradeInCustomQuote.setModelName(bicycleModel.getName());
        }
        if (request.getModelName() != null) {
            tradeInCustomQuote.setModelName(request.getModelName());
            BicycleModel bicycleModel = bicycleModelRepository.findOneByNameActive(request.getModelName());
            if (bicycleModel != null) {
                tradeInCustomQuote.setModelId(bicycleModel.getId());
            } else {
                tradeInCustomQuote.setModelId(null);
            }
        }
        if (request.getYearId() != null) {
            BicycleYear bicycleYear = bicycleYearRepository.findOne(request.getYearId());
            if (bicycleYear == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            tradeInCustomQuote.setYearId(bicycleYear.getId());
            tradeInCustomQuote.setYearName(bicycleYear.getName());
        }
        if (request.getYearName() != null) {
            try {
                long yearId = Long.parseLong(request.getYearName());
                tradeInCustomQuote.setYearName(request.getYearName());
                BicycleYear bicycleYear = bicycleYearRepository.findOne(yearId);
                if (bicycleYear != null) {
                    tradeInCustomQuote.setYearId(yearId);
                } else {
                    tradeInCustomQuote.setYearId(null);
                }
            } catch (NumberFormatException e) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_NAME_INVALID),
                        HttpStatus.NOT_FOUND);
            }
        }
        if (tradeInCustomQuote.getBrandId() != null && tradeInCustomQuote.getModelId() != null && tradeInCustomQuote.getYearId() != null) {
            int countBicycle = bicycleRepository.getCountByBrandModelYearIgnoreActive(tradeInCustomQuote.getBrandId(), tradeInCustomQuote.getModelId(), tradeInCustomQuote.getYearId());
            if (countBicycle > 0) {
                tradeInCustomQuote.setCreateFromBicycle(true);
            } else {
                tradeInCustomQuote.setCreateFromBicycle(false);
            }

        } else {
            tradeInCustomQuote.setCreateFromBicycle(false);
        }

        if (request.getTypeId() != null) {
            if (bicycleTypeRepository.getCount(request.getTypeId()) == 0) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TYPE_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            tradeInCustomQuote.setTypeId(request.getTypeId());
        }

        //note is nullable
        if (request.getNote() != null) {
            tradeInCustomQuote.setNote(request.getNote());
        }

        if (request.getEmployeeName() != null) {
            tradeInCustomQuote.setEmployeeName(request.getEmployeeName());
        } else if (request.getSubmit() && tradeInCustomQuote.getEmployeeName() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_NAME_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getEmployeeEmail() != null) {
            if (request.getSubmit() || tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.INCOMPLETE_DETAILS) {
                CommonUtils.checkEmailFormatValid(request.getEmployeeEmail());
            }
            tradeInCustomQuote.setEmployeeEmail(request.getEmployeeEmail());
        } else if (request.getSubmit() && tradeInCustomQuote.getEmployeeEmail() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_EMAIL_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getEmployeeLocation() != null) {
            tradeInCustomQuote.setEmployeeLocation(request.getEmployeeLocation());
        } else if (request.getSubmit() && tradeInCustomQuote.getEmployeeLocation() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_LOCATION_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getOwnerName() != null) {
            tradeInCustomQuote.setOwnerName(request.getOwnerName());
        } else if (request.getSubmit() && tradeInCustomQuote.getOwnerName() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    OWNER_NAME_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (request.getClean() != null) {
            tradeInCustomQuote.setClean(request.getClean());
        } else if (request.getSubmit() && tradeInCustomQuote.getClean() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    CLEAN_MUST_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.geteBikeHours() != null || request.geteBikeMileage() != null) {
            if (tradeInCustomQuote.getTypeId() != null) {
                BicycleType bicycleType = bicycleTypeRepository.findOne(tradeInCustomQuote.getTypeId());
                if (bicycleType.getName().equalsIgnoreCase(ValueCommons.BICYCLE_TYPE_E_BIKE)) {
                    tradeInCustomQuote.seteBikeHours(request.geteBikeHours());
                    tradeInCustomQuote.seteBikeMileage(request.geteBikeMileage());
                }
            }
        }

        if (request.getUpgradeComponentsId() != null && request.getUpgradeComponentsId().size() > 0) {
            //delete old upgrade component
            tradeInUpgradeCompDetailRepository.deleteByTradeInId(tradeIn.getId());
            //update
            insertUpgradeComponent(request.getUpgradeComponentsId(), tradeIn.getId());
        }

        if (request.getCompCustomQuotes() != null && request.getCompCustomQuotes().size() > 0) {
            //delete old custom quote
            tradeInCustomQuoteCompDetailRepository.deleteByCustomQuoteId(customQuoteId);
            //update component
            insertTradeInCustomCompDetail(request.getCompCustomQuotes(), tradeInCustomQuote.getId());
        }
        //end

        tradeIn.setSave(!request.getSubmit());

        if (request.getSubmit() && tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.INCOMPLETE_DETAILS) {
            tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.PENDING_REVIEW);

            if (tradeIn.getStatusId() == StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue()) {
                tradeIn.setStatusId((long)StatusTradeIn.CUSTOM_QUOTE_PENDING_REVIEW.getValue());
            }

            sendEmailInReviewCustomQuoteAsync(tradeInCustomQuote, tradeIn);
        }
        tradeIn = tradeInRepository.save(tradeIn);


        return tradeInCustomQuoteRepository.save(tradeInCustomQuote);

    }

    private void validateComps(List<CompRequest> comps) throws ExceptionResponse {
        if (comps == null) return;
        for (CompRequest comp : comps) {
            if (comp.getValue() == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        COMPONENT_VALUE_MUST_BE_NOT_BLANK),
                        HttpStatus.BAD_REQUEST);
            }
            comp.setValue(comp.getValue().trim());
            if (StringUtils.isBlank(comp.getValue())) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                        COMPONENT_VALUE_MUST_BE_NOT_BLANK),
                        HttpStatus.BAD_REQUEST);
            }
        }
    }

    private void validateCustomQuoteComps(List<TradeInCustomQuoteCompDetail> comps, List<String> required) throws ExceptionResponse {
        if (required != null && required.size() > 0) {
            if (comps != null && comps.size() > 0) {
                List<Long> compTypeIds = comps.stream()
                        .map(TradeInCustomQuoteCompDetail::getId)
                        .map(TradeInCustomQuoteCompDetailId::getCompTypeId)
                        .collect(Collectors.toList());
                List<InventoryCompType> compTypes = inventoryCompTypeRepository.findAllByIds(compTypeIds);

                for (String compName : required) {
                    InventoryCompType type = CommonUtils.findObject(compTypes, t -> t.getName().equals(compName));
                    if (type == null) {
                        throw new ExceptionResponse(
                                ObjectError.ERROR_PARAM,
                                MISSING_REQUIRED_COMPONENTS,
                                HttpStatus.BAD_REQUEST
                        );
                    }
                }
            } else {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MISSING_REQUIRED_COMPONENTS,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }

    public Object postTradeInCustomQuote(long tradeInIdCustomQuote, List<MultipartFile> images, final boolean isAddImage, final boolean isSave) throws ExceptionResponse {
        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeInIdCustomQuote);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, NOT_FOUND_CUSTOM_QUOTE), HttpStatus.NOT_FOUND);
        }
        if (tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.INCOMPLETE_DETAILS &&
                tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.PENDING_REVIEW) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_TRADE_IN_INVALID), HttpStatus.NOT_FOUND);
        }

        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuote.getId());

        //check permission
        CommonUtils.validRolePartner(tradeIn);

        if (!isSave) {
            validateCustomQuoteEnoughDetails(tradeInCustomQuote, tradeIn);
        }

        List<TradeInImageType> tradeInImageTypesRequireFull = tradeInImageTypeRepository.findAllImageRequire();
        List<TradeInImageType> tradeInImageTypesNotRequireFull = tradeInImageTypeRepository.findAllImageNotRequire();

        if (images == null || images.size() == 0) {
            if (!isSave) {
                List<Long> tradeInImageIdsUpload = tradeInImageRepository.getAllTradeImageImageUpload(tradeIn.getId());
                if (tradeInImageTypesRequireFull.size() > tradeInImageIdsUpload.size()) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_MUST_UPLOAD_IMAGE_REQUIRE),
                            HttpStatus.BAD_REQUEST
                    );
                }
                tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_PENDING_REVIEW.getValue());
                tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.PENDING_REVIEW);
                tradeIn = tradeInRepository.save(tradeIn);
                return tradeInCustomQuoteRepository.save(tradeInCustomQuote);
            }
            tradeIn.setSave(true);
            tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.INCOMPLETE_DETAILS);
            tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue());
            tradeIn = tradeInRepository.save(tradeIn);
            tradeInCustomQuoteRepository.save(tradeInCustomQuote);
            return tradeIn;
        } else {
            List<Long> tradeInImagesUploaded = tradeInImageRepository.getAllTradeImageImageUpload(tradeIn.getId());
            if (tradeInImagesUploaded.size() + images.size() > Constants.MAX_TRADE_IN_IMAGE) {
                throw new ExceptionResponse(
                        ObjectError.INVALID_ACTION,
                        MAX_IMAGE_UPLOAD_EXCEED,
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        List<TradeInImageType> imageTypes = new ArrayList<>();
        imageTypes.addAll(tradeInImageTypesRequireFull);
        imageTypes.addAll(tradeInImageTypesNotRequireFull);
        Random rd = new Random();

        List<S3UploadImage<Long>> uploads = new ArrayList<>();
        for (MultipartFile multipartFile : images) {
            int index = rd.nextInt(imageTypes.size());
            S3UploadImage<Long> uploadImage = new S3UploadImage<>(Constants.TRADE_IN_FOLDER + "_" + tradeIn.getId() + "_" + imageTypes.get(index).getId(),
                    S3Value.FOLDER_TRADEIN_ORIGINAL, multipartFile, imageTypes.get(index).getId());
            uploads.add(uploadImage);
        }

        List<S3UploadResponse<Long>> s3UploadResponses = S3UploadUtils.uploads(s3Manager, uploads);
        List<TradeInImage> newTradeInImages = new ArrayList<>();
        for (S3UploadResponse<Long> s3UploadRespons : s3UploadResponses) {
            TradeInImage tradeInImage = new TradeInImage();
            tradeInImage.setImageTypeId(s3UploadRespons.getOtherData());
            tradeInImage.setFullLink(S3UploadUtils.getFullLinkS3(awsResourceConfig, s3UploadRespons.getFullKey()));
            tradeInImage.setKeyHash(s3UploadRespons.getFullKey());
            tradeInImage.setCustomQuoteId(tradeInCustomQuote.getId());
            tradeInImage.setTradeInId(tradeIn.getId());
            newTradeInImages.add(tradeInImage);
        }


        tradeInImageRepository.saveAll(newTradeInImages);
        if (isSave) {
            tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.INCOMPLETE_DETAILS);
            tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue());
        } else {
            List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInCustomQuoteId(tradeInIdCustomQuote);

            if (tradeInImages.size() < tradeInImageTypesRequireFull.size()) {
                tradeIn.setStatusId((long) StatusTradeIn.INCOMPLETE_UPLOAD_IMAGES.getValue());
            } else {
                tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.PENDING_REVIEW);
                tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_PENDING_REVIEW.getValue());
            }

        }
        tradeIn.setSave(isSave);

        if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PENDING_REVIEW) {
            sendEmailInReviewCustomQuoteAsync(tradeInCustomQuote, tradeIn);
        }

        tradeInRepository.save(tradeIn);
        return tradeInCustomQuoteRepository.save(tradeInCustomQuote);
    }

    public Object deleteImageTradeInCustomQuote(long tradeInCustomQuoteId, long imageTypeId) throws ExceptionResponse {
        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeInCustomQuoteId);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PROVIDED_VALUE) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                    HttpStatus.NOT_FOUND
            );
        }
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuoteId);
        CommonUtils.validRolePartner(tradeIn);

        List<TradeInImage> tradeInImages = tradeInImageRepository
                .findAllByCustomQuoteIdAndImageTypeId(tradeInCustomQuoteId, imageTypeId);
        if (tradeInImages.size() > 0) {
            tradeInImageRepository.deleteAll(tradeInImages);
            List<Long> imageTypeIds = new ArrayList<>();
            imageTypeIds.add(imageTypeId);
            deleteImageTradeIn(tradeInCustomQuoteId, imageTypeIds);


            List<TradeInImage> tradeInImagesAll = tradeInImageRepository.findAllByTradeInCustomQuoteId(tradeInCustomQuoteId);
            List<TradeInImageType> tradeInImageTypes = tradeInImageTypeRepository.findAllImageRequire();
            if (tradeInImagesAll.size() < tradeInImageTypes.size()) {
                tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.INCOMPLETE_DETAILS);
                tradeInCustomQuoteRepository.save(tradeInCustomQuote);
                tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_INCOMPLETE.getValue());
                tradeInRepository.save(tradeIn);
            }
        }

        return SUCCESS;
    }

    public Object updateTradeInCustomQuoteReviewed(CustomQuoteReviewRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, ROLE_INVALID),
                    HttpStatus.FORBIDDEN
            );
        }


        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(request.getTradeInCustomQuoteId());
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if (tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.PENDING_REVIEW
                && tradeInCustomQuote.getStatus() != StatusTradeInCustomQuote.PROVIDED_VALUE
        ) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS),
                    HttpStatus.UNPROCESSABLE_ENTITY
            );
        }

        if (request.getTradeInValue() != null) {
            if (request.getTradeInValue() <= 0) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, TRADE_IN_VALUE__MUST_POSITIVE),
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        } else {
            if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PENDING_REVIEW) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        TRADE_IN_VALUE_NOT_EMPTY,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        }

        if (request.getReviewNote() != null) {
            tradeInCustomQuote.setAdminReviewNote(request.getReviewNote());
        }

        if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PENDING_REVIEW) {
            tradeInCustomQuote.setStatus(StatusTradeInCustomQuote.PROVIDED_VALUE);
            tradeInCustomQuote.setTradeInValue(request.getTradeInValue());
            tradeInCustomQuote.setUserReviewId(CommonUtils.getUserLogin());
            tradeInCustomQuote = tradeInCustomQuoteRepository.save(tradeInCustomQuote);
            createTradeInFromCustomQuote(tradeInCustomQuote);


            TradeInCustomQuote tradeInFinal = tradeInCustomQuote;
            Observable.create((ObservableOnSubscribe<Boolean>) et -> {
                sendEmailReviewedCustomQuote(tradeInFinal.getEmployeeEmail(),
                        request.getTradeInCustomQuoteId(), tradeInFinal.getUserCreatedId());
            }).subscribeOn(Schedulers.newThread())
                    .subscribe(su -> {
                    }, e -> {
                        e.printStackTrace();
                    });
        } else if (tradeInCustomQuote.getStatus() == StatusTradeInCustomQuote.PROVIDED_VALUE) {
            float oldValue = tradeInCustomQuote.getTradeInValue();
            tradeInCustomQuote.setTradeInValue(request.getTradeInValue());
            tradeInCustomQuote = tradeInCustomQuoteRepository.save(tradeInCustomQuote);
            TradeInCustomQuote tradeInFinal = tradeInCustomQuote;
            Observable.create((ObservableOnSubscribe<Boolean>) et -> {
                sendEmailChangeValueCustomQuote(
                        tradeInFinal.getUserCreatedId(),
                        request.getTradeInCustomQuoteId(),
                        oldValue, request.getTradeInValue());
            }).subscribeOn(Schedulers.newThread())
                    .subscribe(su -> {
                    }, e -> {
                        e.printStackTrace();
                    });
        }

        return tradeInCustomQuote;
    }


    protected void sendEmailReviewedCustomQuote(String emailEmployee, Long tradeInCustomQuoteId, String userPartnerCreated) {
        UserDetailFullResponse userDetail = authManager.getUserFullInfo(userPartnerCreated);

        SendingMailRequest<ReviewedCustomQuoteContentMailRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(Constants.BBB_ID_GROUP, Constants.BICYCLE_BLUE_BOOK));
        request.setExchange_type("default");
        request.setTemplate_key(IntegratedServices.CUSTOM_QUOTE_REVIEWED_TEMPLATE);
        request.setMail_to(Arrays.asList(userDetail.getEmail()));

        ReviewedCustomQuoteContentMailRequest content = new ReviewedCustomQuoteContentMailRequest();
        content.setEmployee_email(emailEmployee);
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuoteId);
        content.setTrade_in_id(tradeIn.getId() + "");
        content.setTrade_in_link(appConfig.getEndpoint().getBaseWebappUrl());
        request.setContent_mail(content);
        restSendingMailManager.postObject(IntegratedServices.URL_SEND_MAIL, Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                request, BaseMailResponse.class);

    }

    protected void sendEmailChangeValueCustomQuote(
            String userPartnerCreated,
            Long tradeInCustomQuoteId,
            float oldValue, float newValue
    ) {
        UserDetailFullResponse userDetail = authManager.getUserFullInfo(userPartnerCreated);

        SendingMailRequest<ChangeValueCustomQuoteContentMailRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(Constants.BBB_ID_GROUP, Constants.BICYCLE_BLUE_BOOK));
        request.setExchange_type("default");
        request.setTemplate_key(IntegratedServices.CUSTOM_QUOTE_CHANGE_VALUE_TEMPLATE);
        request.setMail_to(Arrays.asList(userDetail.getEmail()));

        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuoteId);
        ChangeValueCustomQuoteContentMailRequest content = new ChangeValueCustomQuoteContentMailRequest();
        String message = IntegratedServices.CUSTOM_QUOTE_CHANGE_VALUE_MESSAGE
                .replace(IntegratedServices.TRADE_IN_ID, String.valueOf(tradeIn.getId()))
                .replace(IntegratedServices.BICYCLE_TITLE, tradeIn.getTitle())
                .replace(IntegratedServices.CUSTOM_QUOTE_REVIEWED_OLD_VALUE, String.valueOf(oldValue))
                .replace(IntegratedServices.CUSTOM_QUOTE_REVIEWED_NEW_VALUE, String.valueOf(newValue));
        content.setMessage(message);
        request.setContent_mail(content);
        restSendingMailManager.postObject(IntegratedServices.URL_SEND_MAIL, Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                request, BaseMailResponse.class);

    }

    private void sendEmailInReviewCustomQuoteAsync(TradeInCustomQuote tradeInCustomQuote, TradeIn tradeIn) {
        notificationManager.sendMailCustomQuoteCompleteAsync(tradeInCustomQuote, tradeIn, ValueCommons.EMAIL_ADMINS);
//        Observable.create((ObservableOnSubscribe<Boolean>) et -> {
//            sendEmailInReviewCustomQuote(
//                    tradeInCustomQuote.getYearName() + " " + tradeInCustomQuote.getBrandName() + " " + tradeInCustomQuote.getModelName(),
//                    tradeInCustomQuote.getEmployeeEmail(), tradeInCustomQuote.getUserCreatedId());
//            et.onNext(true);
//            et.onComplete();
//        }).subscribeOn(Schedulers.newThread())
//                .subscribe(su -> {
//
//                }, er -> {
//                    er.printStackTrace();
//                });;
    }

    protected void sendEmailInReviewCustomQuote(String nameBicycle, String emailEmployee, String userPartnerId) {
        UserDetailFullResponse userDetail = authManager.getUserFullInfo(userPartnerId);

        SendingMailRequest<NewCustomQuoteContentMailRequest> request = new SendingMailRequest<>();
        request.setReceive(new ReceiveSendingMailRequest(Constants.BBB_ID_GROUP, Constants.BICYCLE_BLUE_BOOK));
        request.setExchange_type("default");
        request.setTemplate_key(IntegratedServices.TEMPLATE_CUSTOM_QUOTE_AVAILABLE);
        request.setBcc(ValueCommons.EMAIL_ADMINS);
        String shopName = "default";
        if (userDetail.getPartner() != null) {
            shopName = userDetail.getPartner().getName();
        }
        NewCustomQuoteContentMailRequest content = new NewCustomQuoteContentMailRequest(
                shopName,
                nameBicycle,
                emailEmployee
        );
        request.setContent_mail(content);
        restSendingMailManager.postObject(IntegratedServices.URL_SEND_MAIL, Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                request, BaseMailResponse.class);

    }


    private void createTradeInFromCustomQuote(TradeInCustomQuote customQuote) throws ExceptionResponse {
        long bicycleId = updateInfoCustomQuote(customQuote);
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(customQuote.getId());
        tradeIn.setBicycleId(bicycleId);
        tradeIn.setValue(customQuote.getTradeInValue());
        tradeIn.setStatusId((long) StatusTradeIn.CUSTOM_QUOTE_VALUE_PROVIDED.getValue());
        tradeInRepository.save(tradeIn);

    }

    private long updateInfoCustomQuote(TradeInCustomQuote customQuote) throws ExceptionResponse {
        Long bicycleId = null;
        if (customQuote.isCreateFromBicycle()) {
            Bicycle bicycle = bicycleRepository.findOneByBrandModelYear(
                    customQuote.getBrandId(), customQuote.getModelId(), customQuote.getYearId(),
                    true
            );
            if (bicycle != null) {
                bicycleId = bicycle.getId();
            }
        } else {
            BrandModelYear brandModelYear = brandModelYearRepository.findOne(
                    customQuote.getBrandName(),
                    customQuote.getModelName(),
                    customQuote.getYearName(),
                    System.currentTimeMillis());
            if (brandModelYear != null && brandModelYear.getBrandId() == null && brandModelYear.getModelId() == null && brandModelYear.getYearId() == null) {
                brandModelYear = null;
            }
            boolean isCreateBrand = true;
            boolean isCreateModel = true;
            boolean isCreateyear = true;
            if (brandModelYear != null) {
                if (brandModelYear.getBrandId() != null) {
                    isCreateBrand = false;
                    customQuote.setBrandId(brandModelYear.getBrandId());
                }
                if (brandModelYear.getModelId() != null) {
                    isCreateModel = false;
                    customQuote.setModelId(brandModelYear.getModelId());
                }
                if (brandModelYear.getYearId() != null) {
                    isCreateyear = false;
                    customQuote.setYearId(brandModelYear.getYearId());
                }

            }
            if (isCreateBrand) {
                BrandCreateRequest brandRequest = new BrandCreateRequest();
                brandRequest.setName(customQuote.getBrandName());
                brandRequest.setValueModifier(0.0f);
                BicycleBrand bicycleBrand = brandManager.createBrandIgnoreRole(brandRequest);
                customQuote.setBrandId(bicycleBrand.getId());
            }
            if (isCreateModel) {
                ModelCreateRequest modelRequest = new ModelCreateRequest();
                modelRequest.setBrandId(customQuote.getBrandId());
                modelRequest.setName(customQuote.getModelName());
                BicycleModel bicycleModel = modelManager.createModelIgnoreRole(modelRequest);
                customQuote.setModelId(bicycleModel.getId());
            }
            if (isCreateyear) {
                BicycleYear bicycleYear = new BicycleYear();
                bicycleYear.setName(customQuote.getYearName());
                bicycleYear.setId(Long.valueOf(customQuote.getYearName()));
                bicycleYear.setApproved(true);
                bicycleYear.setDelete(false);
                bicycleYear = bicycleYearRepository.save(bicycleYear);
                customQuote.setYearId(bicycleYear.getId());
            }
        }
        if (bicycleId == null) {
            Bicycle bicycle = createBicycleFromCustomQuote(customQuote);
            bicycleId = bicycle.getId();
        }
        return bicycleId;
    }

    private Bicycle createBicycleFromCustomQuote(TradeInCustomQuote customQuote) {
        Bicycle bicycle = new Bicycle();
        bicycle.setBrandId(customQuote.getBrandId());
        bicycle.setModelId(customQuote.getModelId());
        bicycle.setTypeId(customQuote.getTypeId());
        bicycle.setYearId(customQuote.getYearId());
        bicycle.setRetailPrice(customQuote.getTradeInValue());
//        bicycle.setSizeId(customQuote.getSizeId());
        bicycle.setActive(false);

        bicycle.setDelete(false);

        bicycle.setName(customQuote.getYearName() + " " + customQuote.getBrandName() + " " + customQuote.getModelName());
        bicycle = bicycleRepository.save(bicycle);

        List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInCustomQuoteId(customQuote.getId());
        if (tradeInImages.size() > 0) {
            List<BicycleImage> bicycleImages = new ArrayList<>();
            for (TradeInImage tradeInImage : tradeInImages) {
                BicycleImage bicycleImage = new BicycleImage();
                bicycleImage.setBicycleId(bicycle.getId());
                bicycleImage.setUri(tradeInImage.getFullLink());
                bicycleImages.add(bicycleImageRepository.save(bicycleImage));
            }
            bicycle.setImageDefault(tradeInImages.get(0).getFullLink());
            bicycle.setImageId(bicycleImages.get(0).getId());
            bicycle = bicycleRepository.save(bicycle);
        }

        return bicycle;
    }


    public Object getCustomQuoteCommon(long tradeInCustomQuoteId) throws ExceptionResponse {
        TradeInCustomQuote tradeInCustomQuote = tradeInCustomQuoteRepository.findOne(tradeInCustomQuoteId);
        if (tradeInCustomQuote == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_CUSTOM_QUOTE_NOT_FOUND), HttpStatus.BAD_REQUEST);
        }
        TradeIn tradeIn = tradeInRepository.findOneByCustomQuoteId(tradeInCustomQuoteId);
        if (tradeIn == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, TRADE_IN_NOT_EXIST), HttpStatus.BAD_REQUEST);
        }
        TradeInCustomQuoteCommonRes res = new TradeInCustomQuoteCommonRes();
        res.setCustomQuoteId(tradeInCustomQuoteId);
        res.setTradeInId(tradeIn.getId());
        res.setStatus(StatusTradeIn.findByValue(tradeIn.getStatusId().intValue()));
        switch (res.getStatus()) {
            case DECLINED_DETAIL:
                res.setIndexStep(-1);
                break;
            case CUSTOM_QUOTE_VALUE_PROVIDED:
                res.setIndexStep(0);
                break;
            case INCOMPLETE_SUMMARY:
                res.setIndexStep(1);
                break;
            case INCOMPLETE_UPLOAD_IMAGES:
            case CUSTOM_QUOTE_INCOMPLETE:
            case CUSTOM_QUOTE_PENDING_REVIEW:
                res.setIndexStep(2);
                break;
            case SHIPPING:
            case COMPLETED:
                res.setIndexStep(3);
                break;
            default:
                break;

        }
        return res;
    }

    private void validateCustomQuoteEnoughImages(long tradeInCustomQuoteId) throws ExceptionResponse {
        List<TradeInImageType> tradeInImageTypesRequired = tradeInImageTypeRepository.findAllImageRequire();
        validateCustomQuoteEnoughImages(tradeInCustomQuoteId, tradeInImageTypesRequired);
    }

    private void validateCustomQuoteEnoughImages(
            long tradeInCustomQuoteId, List<TradeInImageType> tradeInImageTypesRequired
    ) throws ExceptionResponse {
        List<TradeInImage> tradeInImages = tradeInImageRepository.findAllByTradeInCustomQuoteId(tradeInCustomQuoteId);

        if (tradeInImages.size() < tradeInImageTypesRequired.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, UPLOAD_IMAGE_REQUIRED_BEFORE_SUBMIT),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void validateCustomQuoteEnoughDetails(TradeInCustomQuote tradeInCustomQuote, TradeIn tradeIn) throws ExceptionResponse {
        if (tradeInCustomQuote.getBrandId() == null && tradeInCustomQuote.getBrandName() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    BRAND_ONLY_IS_ID_OR_NAME,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeInCustomQuote.getModelId() == null && tradeInCustomQuote.getModelName() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    MODEL_ONLY_IS_ID_OR_NAME,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeInCustomQuote.getYearId() == null && tradeInCustomQuote.getYearName() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    YEAR_ONLY_IS_ID_OR_NAME,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeInCustomQuote.getTypeId() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    TYPE_ID_NOT_EXIST,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeInCustomQuote.getClean() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    CLEAN_MUST_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        //not required
        if (tradeInCustomQuote.getNote() == null) {}

        if (StringUtils.isBlank(tradeInCustomQuote.getEmployeeName())) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_NAME_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(tradeInCustomQuote.getEmployeeEmail())) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_EMAIL_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        } else {
            CommonUtils.checkEmailFormatValid(tradeInCustomQuote.getEmployeeEmail());
        }

        if (StringUtils.isBlank(tradeInCustomQuote.getEmployeeLocation())) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    EMPLOYEE_LOCATION_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (StringUtils.isBlank(tradeInCustomQuote.getOwnerName())) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    OWNER_NAME_NOT_EMPTY,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (tradeIn.getCondition() == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    CONDITION_MUST_BE_NOT_NULL,
                    HttpStatus.BAD_REQUEST
            );
        }

        List<TradeInCustomQuoteCompDetail> comps = tradeInCustomQuoteCompDetailRepository.findAllByCustomQuote(tradeInCustomQuote.getId());
        List<String> required = TradeInUtil.getRequiredCustomQuoteComponents();

        validateCustomQuoteComps(comps, required);
    }
}
