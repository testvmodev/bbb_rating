package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Pictures {
    @JacksonXmlProperty(localName = "VariationSpecificName")
    private String variationSpecificName;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "VariationSpecificPictureSet")
    private List<VariationSpecificPictureSet> variationSpecificPictureSets;

    public String getVariationSpecificName() {
        return variationSpecificName;
    }

    public void setVariationSpecificName(String variationSpecificName) {
        this.variationSpecificName = variationSpecificName;
    }

    public List<VariationSpecificPictureSet> getVariationSpecificPictureSets() {
        return variationSpecificPictureSets;
    }

    public void setVariationSpecificPictureSets(List<VariationSpecificPictureSet> variationSpecificPictureSets) {
        this.variationSpecificPictureSets = variationSpecificPictureSets;
    }
}
