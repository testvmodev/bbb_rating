package com.bbb.core.repository.tracking.out.recentview;

import com.bbb.core.model.response.market.home.recentview.RecentViewMarketListingResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecentViewMarketListingResRepository extends JpaRepository<RecentViewMarketListingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id," +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory_type.name AS inventory_type_name, " +
            ":marketType AS market_type , "+
            "'last_view_time' = CONVERT(DATETIME, NULL), " +
            "inventory.bicycle_size_name AS size_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory_type.id = inventory.type_id " +

            "WHERE "+
            "market_listing.is_delete = 0 AND "+
            "market_listing.status = :status AND "+
            "market_listing.id IN :marketListingIds AND "+
            "market_listing.market_place_id = :marketPlaceId"
    )
    List<RecentViewMarketListingResponse> findAllMarketListing(
            @Param(value = "status") String status,
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "marketType") String marketType,
            @Param(value = "marketPlaceId") long marketPlaceId
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "tracking.market_listing_id AS market_listing_id," +
            "inventory.id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory_type.name AS inventory_type_name, " +
            "tracking.type AS market_type, " + //listing tracking type same value as market type, but not all tracking type
            "tracking.last_viewed AS last_view_time, "+
            "inventory.bicycle_size_name AS size_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +

            "FROM tracking " +
            "LEFT JOIN market_listing ON tracking.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory_type.id = inventory.type_id " +

            "WHERE " +
            "tracking.user_id = :userId  AND " +
            "(:marketType IS NULL OR tracking.type = :marketType)  " +
            "ORDER BY tracking.last_viewed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY")
    List<RecentViewMarketListingResponse> findAllTracking(
            @Param(value = "userId") String userId,
            @Param(value = "marketType") String marketType,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );
}
