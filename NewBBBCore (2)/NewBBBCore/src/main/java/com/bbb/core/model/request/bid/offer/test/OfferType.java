package com.bbb.core.model.request.bid.offer.test;

public enum OfferType {
    MARKET_LISTING,
    INVENTORY_AUCTION
}
