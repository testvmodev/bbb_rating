package com.bbb.core.model.request.tradein.ups.detail.account;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceAccessToken {
    @JsonProperty("AccessLicenseNumber")
    private String accessLicenseNumber;

    public ServiceAccessToken(String accessLicenseNumber) {
        this.accessLicenseNumber = accessLicenseNumber;
    }

    public String getAccessLicenseNumber() {
        return accessLicenseNumber;
    }

    public void setAccessLicenseNumber(String accessLicenseNumber) {
        this.accessLicenseNumber = accessLicenseNumber;
    }
}
