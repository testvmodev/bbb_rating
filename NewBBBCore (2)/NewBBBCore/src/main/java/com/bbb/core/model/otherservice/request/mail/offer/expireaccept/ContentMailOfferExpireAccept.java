package com.bbb.core.model.otherservice.request.mail.offer.expireaccept;

import com.bbb.core.model.otherservice.request.mail.offer.content.OfferMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentMailOfferExpireAccept {
    private UserMailRequest user;
    private ListingExpireRequest listing;
    private OfferMailRequest offer;
    @JsonProperty("base_path")
    private String basePath;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingExpireRequest getListing() {
        return listing;
    }

    public void setListing(ListingExpireRequest listing) {
        this.listing = listing;
    }

    public OfferMailRequest getOffer() {
        return offer;
    }

    public void setOffer(OfferMailRequest offer) {
        this.offer = offer;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
