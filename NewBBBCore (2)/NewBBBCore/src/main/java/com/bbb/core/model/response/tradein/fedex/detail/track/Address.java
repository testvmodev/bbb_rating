package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Address {
    @JacksonXmlProperty(localName = "City")
    private String city;
    @JacksonXmlProperty(localName = "StateOrProvinceCode")
    private String stateProvinceCode;
    @JacksonXmlProperty(localName = "PostalCode")
    private int postalCode;
    @JacksonXmlProperty(localName = "CountryCode")
    private String countryCode;
    @JacksonXmlProperty(localName = "CountryName")
    private String countryName;
    @JacksonXmlProperty(localName = "Residential")
    private boolean residential;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    public void setStateProvinceCode(String stateProvinceCode) {
        this.stateProvinceCode = stateProvinceCode;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public boolean isResidential() {
        return residential;
    }

    public void setResidential(boolean residential) {
        this.residential = residential;
    }
}
