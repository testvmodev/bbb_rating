package com.bbb.core.repository.bicyclemodel;

import com.bbb.core.model.database.BicycleModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleModelRepository extends CrudRepository<BicycleModel, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_model " +
            "WHERE id = ?1 AND is_delete = 0")
    BicycleModel findOne(long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_model")
    int getTotalCount();

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM bicycle_model " +
            "WHERE bicycle_model.name LIKE ?1 " +
            "AND is_delete = 0"
    )
    int getTotalCountByName(String name);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM bicycle_model " +
            "WHERE bicycle_model.brand_id = ?1 " +
            "AND is_delete = 0"
    )
    int getTotalCountByBrandId(long brandId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM bicycle_model " +
            "WHERE bicycle_model.brand_id = :brandId " +
            "AND bicycle_model.name = :name " +
            "AND bicycle_model.is_delete = 0"
    )
    int getTotalCountByBrandIdName(
            @Param("brandId") long brandId,
            @Param("name") String name
    );


    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_model WHERE bicycle_model.id = ?1 AND bicycle_model.is_delete = 0")
    BicycleModel findOneNotDelete(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_model")
    List<BicycleModel> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_model " +
            "WHERE bicycle_model.brand_id = :brandId and bicycle_model.is_delete = 0")
    int getCount(@Param(value = "brandId") long brandId);


    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_model WHERE bicycle_model.id = :modelId AND bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1")
    BicycleModel findOneActive(
            @Param(value = "modelId") long modelId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle_model WHERE bicycle_model.name = :modelName AND bicycle_model.is_delete = 0 AND bicycle_model.is_approved = 1")
    BicycleModel findOneByNameActive(
            @Param(value = "modelName") String modelName
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM bicycle_model " +
            "WHERE " +
            "(:id IS NULL OR bicycle_model.id = :id) " +
            "AND (:name IS NULL OR bicycle_model.name LIKE :#{'%' + #name + '%'}) " +
            "AND bicycle_model.is_delete = 0 " +
            "ORDER BY is_approved DESC"
    )
    BicycleModel findOneNotDelete(
            @Param("id") Long id,
            @Param("name") String name
    );
}
