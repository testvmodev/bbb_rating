package com.bbb.core.repository.bid.out;

import com.bbb.core.model.database.Offer;
import com.bbb.core.model.request.sort.OfferBidFieldSort;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.bid.offer.offerbid.BidOfferResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BidOfferResponseRepository extends JpaRepository<BidOfferResponse, Long> {
    @Query(nativeQuery = true, value =
            "SELECT " +
                    "offer.id AS offer_id, " +
                    "inventory_auction.inventory_id AS inventory_id, " +
                    "inventory_auction.id AS inventory_auction_id, " +
                    "inventory.name AS inventory_name, " +
                    "inventory.image_default AS image_default, " +
                    "offer.status AS status, " +
                    "offer.created_time AS created_time, " +
                    "offer.offer_price AS bid_price, " +
                    "inventory.title AS title, " +
                    "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
                    "DATEDIFF_BIG(MILLISECOND, auction.start_date, auction.end_date) AS total_time " +
                    "FROM " +
                    "offer " +
                    "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                    "JOIN auction ON inventory_auction.auction_id = auction.id " +
                    "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
                    "WHERE " +
                    "offer.inventory_auction_id = :inventoryAuctionId AND " +
                    "(:userId IS NULL OR offer.buyer_id = :userId) AND " +
                    "offer.is_delete = 0 " +
                    "ORDER BY " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).CREATED_TIME.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.created_time END DESC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).CREATED_TIME.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.created_time END ASC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).BID_PRICE.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.offer_price END DESC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).BID_PRICE.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.offer_price END ASC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).STATUS_BID.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN offer.status END DESC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).STATUS_BID.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN offer.status END ASC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).INVENTORY_ID.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory_auction.inventory_id END DESC, " +
                    "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.OfferBidFieldSort).INVENTORY_ID.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory_auction.inventory_id END ASC "
    )
    List<BidOfferResponse> findAll(
            @Param(value = "inventoryAuctionId") long inventoryAuctionId,
            @Param(value = "userId") String userId,
            @Param(value = "now") String now,
            @Param(value = "fieldSort") String fieldSort,
            @Param(value = "sortType") String sortType
    );

    @Query(nativeQuery = true, value =
            "SELECT " +
                    "offer.id AS offer_id, " +
                    "inventory_auction.inventory_id AS inventory_id, " +
                    "inventory_auction.id AS inventory_auction_id, " +
                    "inventory.name AS inventory_name, " +
                    "inventory.image_default AS image_default, " +
                    "offer.status AS status, " +
                    "offer.created_time AS created_time, " +
                    "offer.offer_price AS bid_price, " +
                    "inventory.title AS title, " +
                    "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
                    "DATEDIFF_BIG(MILLISECOND, auction.start_date, auction.end_date) AS total_time " +
                    "FROM " +
                    "offer " +
                    "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                    "JOIN auction ON inventory_auction.auction_id = auction.id " +
                    "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
                    "WHERE " +
                    "offer.inventory_auction_id = :inventoryAuctionId AND " +
                    "offer.is_delete = 0 " +
                    "ORDER BY offer.created_time DESC"
    )
    List<BidOfferResponse> findAllBid(
            @Param(value = "inventoryAuctionId") long inventoryAuctionId,
            @Param(value = "now") String now
    );
}
