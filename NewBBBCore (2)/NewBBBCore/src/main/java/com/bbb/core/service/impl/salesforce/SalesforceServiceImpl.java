package com.bbb.core.service.impl.salesforce;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.salesforce.SalesforceManager;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import com.bbb.core.model.request.saleforce.ListingFullUpdateRequest;
import com.bbb.core.service.salesforce.SalesforceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesforceServiceImpl implements SalesforceService {
    @Autowired
    private SalesforceManager manager;

    @Override
    public Object createListingSalesforce(ListingFullCreateRequest request) throws ExceptionResponse {
        return manager.createListingSalesforce(request);
    }

    @Override
    public Object updateListingSalesforce(String salesforceId, ListingFullUpdateRequest request) throws ExceptionResponse {
        return manager.updateListingSalesforce(salesforceId, request);
    }

    @Override
    public Object deListListingSalesforce(String salesforceId) throws ExceptionResponse {
        return manager.deListListingSalesforce(salesforceId);
    }

    @Override
    public Object soldListingSalesforce(String salesforceId) throws ExceptionResponse {
        return manager.soldListingSalesforce(salesforceId);
    }
}
