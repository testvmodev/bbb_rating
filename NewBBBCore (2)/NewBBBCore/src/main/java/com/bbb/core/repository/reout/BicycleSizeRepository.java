package com.bbb.core.repository.reout;

import com.bbb.core.model.database.BicycleSize;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BicycleSizeRepository extends CrudRepository<BicycleSize, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM bicycle_size " +
            "WHERE id = ?1")
    BicycleSize findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_size")
    List<BicycleSize> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM bicycle_size WHERE bicycle_size.id = :sizeId")
    int getCount(
            @Param(value = "sizeId") long sizeId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle_size WHERE bicycle_size.name = :sizeName")
    BicycleSize findOneByName(
            @Param(value = "sizeName") String sizeName
    );

}
