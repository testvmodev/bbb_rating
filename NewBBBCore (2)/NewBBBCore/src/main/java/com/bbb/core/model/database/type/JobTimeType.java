package com.bbb.core.model.database.type;

public enum JobTimeType {
    FIX_RATE("Fixed rate"),
    FIX_DELAY("Fixed delay"),
    CRON("Cron");

    private final String value;

    JobTimeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static JobTimeType findByValue(String value) {
        for (JobTimeType type : JobTimeType.values()) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        return null;
    }
}
