package com.bbb.core.repository.tracking.out;

import com.bbb.core.model.response.tracking.RecentViewBicycleResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecentViewBicycleResponseRepository extends JpaRepository<RecentViewBicycleResponse, Long> {


    @Query(nativeQuery = true, value = "SELECT " +
            "ABS(CHECKSUM(NewId())) AS id, " +
            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS title, " +
            "bicycle_size.name AS size_name, " +
            "bicycle_brand.name AS brand_name, " +
            "bicycle_model.name AS model_name, " +
            "bicycle_year.name AS year_name, " +
            "bicycle_type.name AS type_name, " +
            "'last_view_time' = CONVERT(DATETIME, NULL), " +
            "bicycle.image_default AS image_default " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "LEFT JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "WHERE " +
            "bicycle.id IN :bicycleIds AND " +
            "bicycle.is_delete = 0 AND " +
            "bicycle.active = 1 "

    )
    List<RecentViewBicycleResponse> findAllBicycle(
            @Param(value = "bicycleIds") List<Long> bicycleIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "tracking.id AS id, " +
            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS title, " +
            "bicycle_size.name AS size_name, " +
            "bicycle_brand.name AS brand_name, " +
            "bicycle_model.name AS model_name, " +
            "bicycle_year.name AS year_name, " +
            "bicycle_type.name AS type_name, " +
            "tracking.last_viewed AS last_view_time, " +
            "bicycle.image_default AS image_default " +
            "FROM " +
            "tracking " +
            "JOIN bicycle ON tracking.bicycle_id = bicycle.id "+
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "LEFT JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "WHERE " +
            "tracking.user_id = :userId AND " +
            "bicycle.is_delete = 0 AND "+
            "bicycle.active = 1 AND " +
            "tracking.type = :#{T(com.bbb.core.model.database.type.TrackingType).BICYCLE.getValue()} " +
            "ORDER BY tracking.last_viewed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<RecentViewBicycleResponse> findAllTracking(
            @Param(value = "userId") String userId,
            @Param(value = "offset") int offset,
            @Param(value = "size") int maxSize
    );
}
