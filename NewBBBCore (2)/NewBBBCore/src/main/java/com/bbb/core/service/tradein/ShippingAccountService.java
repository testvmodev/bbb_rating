package com.bbb.core.service.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.tradein.tradin.ShippingAccountCreateRequest;

public interface ShippingAccountService {
    Object getAccounts();

    Object addAccount(ShippingAccountCreateRequest request) throws ExceptionResponse;

    Object deleteAccount(long id) throws ExceptionResponse;

    Object activate(long id) throws ExceptionResponse;
}
