package com.bbb.core.service.user;

import com.bbb.core.common.exception.ExceptionResponse;

public interface UserService {
    Object updateStatusActive(String userId, boolean isActive) throws ExceptionResponse;

    Object deletedAccount(String userId) throws ExceptionResponse;

    Object verifySafe(String userId) throws ExceptionResponse;

    Object getUserBasicStats(String userId) throws ExceptionResponse;
}
