package com.bbb.core.repository.market.out;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.filter.ListingSellerType;
import com.bbb.core.model.response.market.marketlisting.MarketListingResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingResponseRepository extends JpaRepository<MarketListingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "market_listing.id AS market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
            "inventory_type.id AS type_id, " +
            "inventory.serial_number AS serial_number, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS type, " +

            "inventory.image_default AS image_default, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +
            "gender.gender_name AS gender, " +
            "suspension.suspension_type AS suspension, " +
            "wheel_size.wheel_size_name AS wheel_size, " +

            "market_listing.status AS status_market_listing, " +
            "inventory.status AS status_inventory,  " +
            "inventory.stage AS stage_inventory,  " +
            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "inventory.condition AS condition, " +
//            "inventory_size. AS size_id, " +
            "inventory_size.size_name AS size_name, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type " +

            "FROM market_listing " +


            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            "CROSS APPLY (SELECT " +
            //calc distance with ms sql server api
            "GEOGRAPHY\\:\\:Point(ISNULL(inventory_location_shipping.latitude, 0), ISNULL(inventory_location_shipping.longitude, 0), 4326).STDistance(GEOGRAPHY\\:\\:Point(:#{#latitude == null ? 0 : #latitude}, :#{#longitude == null ? 0 : #longitude}, 4326)) AS distance " +
            ") AS geography " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Size') " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Brake Type') " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Material') " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "WHERE inventory_comp_type.name = 'Gender') " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "WHERE inventory_comp_type.name = 'Suspension') " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Wheel Size') " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Size') " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +


            //main conditions
            "WHERE " +
            "(1 = :#{@queryUtils.isEmptyList(#bicycleIds) ? 1 : 0} OR " +
                "inventory.bicycle_id IN :#{@queryUtils.isEmptyList(#bicycleIds) ? @queryUtils.fakeIntegers() : #bicycleIds}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#modelIds) ? 1 : 0} OR " +
                "inventory_bicycle.model_id IN :#{@queryUtils.isEmptyList(#modelIds) ? @queryUtils.fakeIntegers() : #modelIds}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#brandIds) ? 1 : 0} OR " +
                "inventory_bicycle.brand_id IN :#{@queryUtils.isEmptyList(#brandIds) ? @queryUtils.fakeIntegers() : #brandIds}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#typeIds) ? 1 : 0} OR " +
                "inventory_type.id IN :#{@queryUtils.isEmptyList(#typeIds) ? @queryUtils.fakeIntegers() : #typeIds}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#typeBicycleNames) ? 1 : 0} OR " +
                "inventory.bicycle_type_name IN :#{@queryUtils.isEmptyList(#typeBicycleNames) ? @queryUtils.fakeStrings() : #typeBicycleNames}) " +

            "AND (1 = :#{@queryUtils.isEmptyList(#frameMaterialNames) ? 1 : 0} OR " +
                "frame_material.fragment_material_name IN :#{@queryUtils.isEmptyList(#frameMaterialNames) ? @queryUtils.fakeStrings() : #frameMaterialNames}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#brakeTypeNames) ? 1 : 0} OR " +
                "brake_type.brake_type_name IN :#{@queryUtils.isEmptyList(#brakeTypeNames) ? @queryUtils.fakeStrings() : #brakeTypeNames}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#sizeNames) ? 1 : 0} OR " +
                "frame_size.frame_size_name IN :#{@queryUtils.isEmptyList(#sizeNames) ? @queryUtils.fakeStrings() : #sizeNames}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#genders) ? 1 : 0} OR " +
                "gender.gender_name IN :#{@queryUtils.isEmptyList(#genders) ? @queryUtils.fakeStrings() : #genders}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#suspensions) ? 1 : 0} OR " +
                "suspension.suspension_type IN :#{@queryUtils.isEmptyList(#suspensions) ? @queryUtils.fakeStrings() : #suspensions}) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#wheelSizes) ? 1 : 0} OR " +
                "wheel_size.wheel_size_name IN :#{@queryUtils.isEmptyList(#wheelSizes) ? @queryUtils.fakeStrings() : #wheelSizes}) " +

            "AND (1 = :#{@queryUtils.isEmptyList(#conditions) ? 1 : 0} OR " +
                "inventory.condition IN :#{@queryUtils.isEmptyList(#conditions) ? @queryUtils.fakeStrings() : #conditions}) " +

            "AND (:#{#statusMarketListing} IS NULL OR " +
                "market_listing.status = :#{#statusMarketListing == null ? '!@#$%' : #statusMarketListing.getValue()}) " +
            "AND (:#{#marketPlaceId} IS NULL OR market_listing.market_place_id = :#{#marketPlaceId}) " +
            "AND (:#{#name} IS NULL OR inventory.title LIKE :#{'%' + #name + '%'}) " +

            "AND (:#{#startPriceValue} IS NULL OR inventory.current_listed_price >= :#{#startPriceValue}) " +
            "AND (:#{#endPriceValue} IS NULL OR inventory.current_listed_price <= :#{#endPriceValue}) " +
            "AND (:#{#startYearId} IS NULL OR inventory_bicycle.year_id >= :#{#startYearId}) " +
            "AND (:#{#endYearId} IS NULL OR inventory_bicycle.year_id <= :#{#endYearId}) " +

            "AND (:#{#sortField} != 'BEST_DEAL' OR (inventory.deal_percent IS NOT NULL AND inventory.deal_percent <= 100)) " +
            "AND (:#{#sellerType} = :#{T(com.bbb.core.model.request.filter.ListingSellerType).ALL} " +
                "OR (:#{#sellerType} = :#{T(com.bbb.core.model.request.filter.ListingSellerType).PERSONAL} " +
                    "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                    "AND inventory.storefront_id IS NULL) " +
                "OR (:#{#sellerType} = :#{T(com.bbb.core.model.request.filter.ListingSellerType).ONLINE_STORE} " +
                    "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                    "AND inventory.storefront_id IS NOT NULL)" +
            ") " +

            "AND (:#{#zipCode} IS NULL " +
                "OR inventory_location_shipping.zip_code = :#{#zipCode}) " +
            "AND (:#{#cityName} IS NULL " +
                "OR inventory_location_shipping.city_name LIKE :#{'%' + #cityName + '%'}) " +
            "AND (:#{#state} IS NULL " +
                "OR inventory_location_shipping.state_name LIKE :#{'%' + #state + '%'} " +
                "OR inventory_location_shipping.state_code LIKE :#{'%' + #state + '%'}) " +
            //support blank or just null?
            "AND (1 = :#{T(org.apache.commons.lang3.StringUtils).isBlank(#country) ? 1 : 0} " +
                "OR inventory_location_shipping.country_name LIKE :#{'%' + #country + '%'} " +
                "OR inventory_location_shipping.country_code LIKE :#{'%' + #country + '%'}) " +
            "AND (1 = :#{(#latitude == null || #longitude == null || #radius == null) ? 1 : 0} " +
                "OR (distance <= :#{#radius == null ? 0 : #radius} " +
                "AND inventory_location_shipping.latitude IS NOT NULL " +
                "AND inventory_location_shipping.longitude IS NOT NULL)) " +

            "AND (:content IS NULL OR (" +
                "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} " +
                "OR CAST(inventory.description AS nvarchar(MAX)) collate SQL_Latin1_General_CP1_CI_AS  LIKE :#{'%' + #content + '%'} " +
                ")) " +

            "AND (:storefrontId IS NULL OR inventory.storefront_id = :storefrontId) " +

            "AND (1 = :#{@queryUtils.isEmptyList(#marketListingIds) ? 1 : 0} OR " +
                "market_listing.id IN :#{@queryUtils.isEmptyList(#marketListingIds) ? @queryUtils.fakeLongs() : #marketListingIds}) " +

            "AND inventory.is_delete = 0 " +
            "AND market_listing.is_delete = 0 " +

            //fake usages for all params with legacy style
            //why? to deal with stupid/buggy validating logic of hibernate treats ::Point as prefix of param, though :: is ms sql function calling
            //update spring boot to latest 2.x for newer spring data and hibernate version might fix this problem
//            "AND (1 = 1 " +
//            "OR :bicycleIds = :bicycleIds " +
//            "OR :modelIds = :modelIds " +
//            "OR :brandIds = :brandIds " +
//            "OR 0 IN :typeIds " +
//            "OR 1 IN :typeBicycleNames " +
//            "OR :frameMaterialNames = :frameMaterialNames " +
//            "OR :brakeTypeNames = :brakeTypeNames " +
//            "OR :sizeNames = :sizeNames " +
//            "OR :genders = :genders " +
//            "OR :suspensions = :suspensions " +
//            "OR :wheelSizes = :wheelSizes " +
//            "OR :conditions = :conditions " +
//            "OR :statusMarketListing = :statusMarketListing " +
//            "OR :marketPlaceId = :marketPlaceId " +
//            "OR :name = :name " +
//            "OR :startPriceValue = :startPriceValue " +
//            "OR :endPriceValue = :endPriceValue " +
//            "OR :startYearId = :startYearId " +
//            "OR :endYearId = :endYearId " +
//            "OR :sellerType = :sellerType " +
//            "OR :zipCode = :zipCode " +
//            "OR :cityName = :cityName " +
//            "OR :state = :state " +
//            "OR :country = :country " +
//            "OR :latitude = :latitude " +
//            "OR :longitude = :longitude " +
//            "OR :radius = :radius " +
//            "OR :sortField = :sortField " +
//            "OR :sortType = :sortType " +
//            "OR :offset = :offset " +
//            "OR :size = :size) " +

            //sorting
            "ORDER BY " +
                "CASE WHEN :#{#sortField} = 'BEST_DEAL' THEN inventory.deal_percent ELSE 0 END DESC, " +
                "CASE WHEN :#{#sortField} = 'TIME_START_LISTING_NEWEST' THEN market_listing.time_listed END DESC, " +
                "CASE WHEN :#{#sortField} = 'TIME_END_LISTING_SOONEST' THEN market_listing.time_listed END ASC, " +
                "CASE WHEN :#{#sortField} = 'LISTED_PRICE' AND :#{#sortType} = 'DESC' THEN inventory.current_listed_price END DESC, " +
                "CASE WHEN :#{#sortField} = 'LISTED_PRICE' AND :#{#sortType} != 'DESC' THEN inventory.current_listed_price END ASC " +
            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#size} ROWS ONLY"
    )
    List<MarketListingResponse> findAllMarketPlaceSorted(
            @Param("bicycleIds") List<Long> bicycleIds,
            @Param("modelIds") List<Long> modelIds,
            @Param("brandIds") List<Long> brandIds,
            @Param("typeIds") List<Long> typeIds,
            @Param("typeBicycleNames") List<String> typeBicycleNames,

            @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("sizeNames") List<String> sizeNames,
            @Param("genders") List<String> genders,
            @Param("suspensions") List<String> suspensions,
            @Param("wheelSizes") List<String> wheelSizes,

            @Param("conditions") List<String> conditions,

            @Param("statusMarketListing") StatusMarketListing statusMarketListing,
            @Param("marketPlaceId") Long marketPlaceId,
            @Param("name") String name,

            @Param("startPriceValue") Float startPriceValue,
            @Param("endPriceValue") Float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("sellerType") ListingSellerType sellerType,
            @Param("zipCode") String zipCode,
            @Param("cityName") String cityName,
            @Param("state") String state,
            @Param("country") String country,
            @Param("latitude") Float latitude,
            @Param("longitude") Float longitude,
            @Param("radius") Float radius,

            @Param("content") String content,

            @Param("storefrontId") String storefrontId,

            @Param("marketListingIds") List<Long> marketListingIds,

            @Param("sortField") String sortField,
            @Param("sortType") String sortType,
            @Param("offset") long offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(market_listing.id) " +
            "FROM market_listing " +


            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +


            //main conditions
            "WHERE " +
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR inventory_bicycle.model_id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR inventory_bicycle.brand_id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory_type.id IN :typeIds) AND " +
            "(:isTypeBicycleNull = 1 OR inventory.bicycle_type_name IN :typeBicycleNames) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR frame_size.frame_size_name IN :sizeNames) AND " +
            "(:isGenderNull = 1 OR gender.gender_name IN :genders) AND " +
            "(:isSuspensionNull = 1 OR suspension.suspension_type IN :suspensions) AND " +
            "(:isWheelSizeNull = 1 OR wheel_size.wheel_size_name IN :wheelSizes) AND " +

            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +

            "(:statusMarketListing IS NULL OR market_listing.status = :statusMarketListing) AND " +
            "(:marketPlaceId IS NULL OR market_listing.market_place_id = :marketPlaceId) AND " +
            "(:name IS NULL OR inventory.name LIKE :#{'%' + #name + '%'}) AND " +

            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR inventory_bicycle.year_id >= :startYearId) AND " +
            "(:endYearId IS NULL OR inventory_bicycle.year_id <= :endYearId) AND " +
            "(:sortField <> 'BEST_DEAL' OR (inventory.deal_percent IS NOT NULL AND inventory.deal_percent <= 100)) AND "+

            "(:sellerType = :#{T(com.bbb.core.model.request.filter.ListingSellerType).ALL} OR " +
                "(:#{#sellerType} = :#{T(com.bbb.core.model.request.filter.ListingSellerType).PERSONAL} " +
                    "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                    "AND inventory.storefront_id IS NULL) OR " +
                "(:#{#sellerType} = :#{T(com.bbb.core.model.request.filter.ListingSellerType).ONLINE_STORE} " +
                    "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
                    "AND inventory.storefront_id IS NOT NULL)" +
            ") AND "+
            "(:zipCode IS NULL OR inventory_location_shipping.zip_code = :zipCode) AND " +
            "(:cityName IS NULL OR " +
            "inventory_location_shipping.city_name LIKE :#{'%' + #cityName + '%'}) AND " +
            "(:state IS NULL OR " +
                "inventory_location_shipping.state_name LIKE :#{'%' + #state + '%'} OR " +
                "inventory_location_shipping.state_code LIKE :#{'%' + #state + '%'}) AND " +
            "(:country IS NULL " +
            "OR inventory_location_shipping.country_name LIKE :#{'%' + #country + '%'} " +
            "OR inventory_location_shipping.country_code LIKE :#{'%' + #country + '%'}) AND " +
            "(:isLocationRadiusNull = 1 OR " +
                "(GEOGRAPHY\\:\\:Point(ISNULL(inventory_location_shipping.latitude, 0), ISNULL(inventory_location_shipping.longitude, 0), 4326).STDistance(GEOGRAPHY\\:\\:Point(:latitude, :longitude, 4326)) <= :radius AND " +
                "inventory_location_shipping.latitude IS NOT NULL AND inventory_location_shipping.longitude IS NOT NULL)) AND " +

            "(:content IS NULL OR (" +
            "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} " +
            "OR CAST(inventory.description AS nvarchar(MAX)) collate SQL_Latin1_General_CP1_CI_AS  LIKE :#{'%' + #content + '%'} " +
            ")) AND " +

            "(:storefrontId IS NULL OR inventory.storefront_id = :storefrontId) AND " +

            "(:isListingIdNull = 1 OR market_listing.id IN :marketListingIds) AND " +

            "inventory.is_delete = 0 AND " +
            "market_listing.is_delete = 0"

    )
    int getTotalCountMarketPlace(
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeIds") List<Long> typeIds,
            @Param("isTypeBicycleNull") boolean isTypeBicycleNull, @Param("typeBicycleNames") List<String> typeBicycleNames,

            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,
            @Param("isGenderNull") boolean isGenderNull, @Param("genders") List<String> genders,
            @Param("isSuspensionNull") boolean isSuspensionNull, @Param("suspensions") List<String> suspensions,
            @Param("isWheelSizeNull") boolean isWheelSizeNull, @Param("wheelSizes") List<String> wheelSizes,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,

            @Param("statusMarketListing") String statusMarketListing,
            @Param("marketPlaceId") Long marketPlaceId,
            @Param("name") String name,

            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("sellerType") ListingSellerType sellerType,
            @Param("zipCode") String zipCode,
            @Param("cityName") String cityName,
            @Param("state") String state,
            @Param("country") String country,
            @Param("isLocationRadiusNull") boolean isLocationRadiusNull,
            @Param("latitude") Float latitude,
            @Param("longitude") Float longitude,
            @Param("radius") Float radius,

            @Param("content") String content,

            @Param("storefrontId") String storefrontId,

            @Param("isListingIdNull") boolean isListingIdNull, @Param("marketListingIds") List<Long> marketListingIds,

            @Param("sortField") String sortField
    );


    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "market_listing.id AS market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
//            "inventory_type.id AS type_id, " +
            "inventory.serial_number AS serial_number, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS type, " +

            "inventory.image_default AS image_default, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +
            "gender.gender_name AS gender, " +
            "suspension.suspension_type AS suspension, " +
            "wheel_size.wheel_size_name AS wheel_size, " +

            "market_listing.status AS status_market_listing, " +
            "inventory.status AS status_inventory,  " +
            "inventory.stage AS stage_inventory,  " +
            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "inventory.condition AS condition, " +
//            "inventory_size.inventory_id AS size_id, " +
            "inventory_size.size_name AS size_name, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type " +

            "FROM market_listing " +


            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +



            //main conditions
            "WHERE " +
            "market_listing.id IN :marketListingIds"

    )
    List<MarketListingResponse> findAllByIds(
          @Param(value = "marketListingIds")List<Long> marketListingIds
    );
}
