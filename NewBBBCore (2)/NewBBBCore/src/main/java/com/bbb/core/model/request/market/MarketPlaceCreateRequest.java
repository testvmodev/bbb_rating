package com.bbb.core.model.request.market;

import com.bbb.core.model.database.type.MarketType;

public class MarketPlaceCreateRequest {
    private String name;
    private MarketType marketType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }
}
