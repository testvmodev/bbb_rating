package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.response.bid.offer.offerbid.UserBidDetail;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Immutable
public class UserBidResponse {
    @Id
    private long inventoryAuctionId;
    private long auctionId;
    private float minPrice;
    private LocalDateTime createdTime;
    private String auctionName;
    private Float currentHighestBid;
    private long duration;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String winnerId;

    private long inventoryId;
    private StatusInventory status;
    private StageInventory stage;
    private Float currentListedPrice;
    private Float msrpPrice;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private String inventoryTitle;
    private ConditionInventory condition;

    @Transient
    private List<UserBidDetail> bids;
    @JsonIgnore
    private String requestedUserId;

    public Boolean isWinner() {
        if (requestedUserId != null) {
            if (winnerId != null && winnerId.equals(requestedUserId)) {
                return true;
            }
            return false;
        }
        return null;
    }

    public long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }

    public float getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(float minPrice) {
        this.minPrice = minPrice;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getAuctionName() {
        return auctionName;
    }

    public void setAuctionName(String auctionName) {
        this.auctionName = auctionName;
    }

    public Float getCurrentHighestBid() {
        return currentHighestBid;
    }

    public void setCurrentHighestBid(Float currentHighestBid) {
        this.currentHighestBid = currentHighestBid;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public List<UserBidDetail> getBids() {
        return bids;
    }

    public void setBids(List<UserBidDetail> bids) {
        this.bids = bids;
    }

    public String getRequestedUserId() {
        return requestedUserId;
    }

    public void setRequestedUserId(String requestedUserId) {
        this.requestedUserId = requestedUserId;
    }
}
