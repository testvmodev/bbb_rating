package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycletype.BicycleTypeGetRequest;
import com.bbb.core.model.request.bicycletype.TypeCreateRequest;
import com.bbb.core.model.request.bicycletype.TypeUpdateRequest;
import com.bbb.core.model.request.sort.BicycleTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.TypeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TypeController {
    @Autowired
    private TypeService service;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    @GetMapping(value = Constants.URL_GET_BICYCLE_TYPE)
    public ResponseEntity getBicycleType(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sortField", defaultValue = "NAME") BicycleTypeSortField sortField,
            @RequestParam(value = "sortType" , defaultValue = "ASC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        BicycleTypeGetRequest request = new BicycleTypeGetRequest();
        request.setName(name);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);
        return new ResponseEntity<>(service.getBicycleTypes(request), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_DETAIL_BICYCLE_TYPE)
    public ResponseEntity<Object> getDetailBicycleType(
            @PathVariable(value = Constants.ID) long typeId) throws ExceptionResponse{
        return new ResponseEntity<>(service.getDetailBicycleType(typeId), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_BICYCLE_TYPE_CREATE)
    public ResponseEntity createBicycleType(
            @RequestBody TypeCreateRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.createType(request), HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_BICYCLE_TYPE)
    public ResponseEntity updateBicycleType(
            @PathVariable(value = Constants.ID) long typeId,
            @RequestBody TypeUpdateRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateType(typeId, request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_BICYCLE_TYPE)
    public ResponseEntity deleteBicycleType(
            @PathVariable(value = Constants.ID) long typeId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteType(typeId), HttpStatus.OK);
    }
}
