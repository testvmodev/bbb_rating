package com.bbb.core.repository.market.out.honme;

import com.bbb.core.model.response.market.home.DealRecommendInvAucRes;
import com.bbb.core.model.response.market.home.DealRecommendMarketListingRes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRecommendInvAucResRepository extends JpaRepository<DealRecommendInvAucRes, Long> {
//    private long inventoryId;
//    private String inventoryTitle;
//    private String bicycleTypeName;
//    private Float currentListedPrice;
//    private Float discountedPrice;
//    private String imageDefault;
//    @Transient
//    private Boolean isFavourite;
//    private String sizeName;
//    "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    @Query(nativeQuery = true, value =
            "SELECT " +
                    "max_offer.inventory_auction_id, " +
                    "inventory_auction.inventory_id, " +
                    "inventory.title AS inventory_title, " +
                    "inventory.bicycle_type_name AS bicycle_type_name, " +
                    "inventory.current_listed_price, " +
                    "inventory.discounted_price, " +
                    "inventory.image_default, " +
                    "inventory.bicycle_size_name, " +
                    "auction.start_date AS start_date, " +
                    "auction.end_date AS end_date, " +
                    "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
                    "FROM "+
            "(SELECT offer.inventory_auction_id, " +
                    "COUNT(offer.id) AS count_offer, " +
                    "MAX(offer.offer_price) AS max_price, " +
                    "MAX(offer.created_time) AS max_created_time " +
                    "FROM offer " +
                    "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                    "JOIN auction ON inventory_auction.auction_id = auction.id " +
                    "WHERE " +
                    "offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} AND " +
                    "offer.inventory_auction_id IS NOT NULL AND " +
                    "auction.end_date < GETDATE() AND " +
                    "auction.start_date >= GETDATE() " +
                    "GROUP BY offer.inventory_auction_id " +
                    "ORDER BY COUNT(offer.id) DESC, MAX(offer.offer_price) DESC, MAX(offer.created_time) DESC " +
                    "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY " +
                    ") AS max_offer " +
             "JOIN inventory_auction ON max_offer.inventory_auction_id = inventory_auction.id " +
             "JOIN inventory ON inventory_auction.inventory_id = inventory.id "+
             "JOIN auction ON inventory_auction.auction_id = auction.id "
    )
    List<DealRecommendInvAucRes> findAllDealsFromOffer(
            @Param(value = "now") String now,
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price, " +
            "inventory.discounted_price, " +
            "inventory.image_default, " +
            "inventory.bicycle_size_name, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM " +
            "inventory_auction JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "auction.end_date < GETDATE() AND auction.start_date >= GETDATE() AND " +
            "(:isNullInventoryAuctionIds = 1 OR inventory_auction.id NOT IN :inventoryAuctionIds) " +
            "ORDER BY inventory_auction.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY "
    )
    List<DealRecommendInvAucRes> findAllDealsNotFromOffer(
            @Param(value = "now") String now,
            @Param(value = "isNullInventoryAuctionIds") boolean isNullInventoryAuctionIds,
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name,  " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM " +
            "inventory_auction JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "inventory_auction.is_delete = 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND " +
            "inventory_auction.is_inactive = 0 AND " +
            "(:isHasInventoryAuctionIds = 0 OR inventory_auction.id NOT IN :inventoryAuctionIdsNotIn)" +
            "ORDER BY " +
            "inventory_auction.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendInvAucRes> findAllInventoryAuctionRecommendCommons(
            @Param(value = "isHasInventoryAuctionIds") boolean isHasInventoryAuctionIds,
            @Param(value = "inventoryAuctionIdsNotIn") List<Long> inventoryAuctionIdsNotIn,
            @Param(value = "now") String now,
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name,  " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM " +
            "favourite JOIN inventory_auction ON favourite.inventory_auction_id = inventory_auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "favourite.user_id = :userId AND " +
            "favourite.is_delete = 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND " +
            "inventory_auction.is_inactive = 0 AND " +
            "(:isHasInventoryAuctionIds = 0 OR inventory_auction.id NOT IN :inventoryAuctionIdsNotIn)" +
            "ORDER BY " +
            "favourite.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendInvAucRes> findAllFavourites(
            @Param(value = "isHasInventoryAuctionIds") boolean isHasInventoryAuctionIds,
            @Param(value = "inventoryAuctionIdsNotIn") List<Long> inventoryAuctionIdsNotIn,
            @Param(value = "userId") String userId,
            @Param(value = "now") String now,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM " +
            "tracking JOIN inventory_auction ON tracking.inventory_auction_id = inventory_auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "WHERE " +
            "tracking.user_id = :userId AND " +
            "inventory_auction.is_delete = 0 AND " +
            "inventory_auction.is_inactive = 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND " +
            "(:hasInventoryAuctionIds = 0 OR tracking.inventory_auction_id NOT IN :inventoryAuctionIds) " +
            "ORDER BY " +
            "tracking.last_viewed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendInvAucRes> findAllRecents(
            @Param(value = "hasInventoryAuctionIds") boolean hasInventoryAuctionIds,
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "userId") String userId,
            @Param(value = "now") String now,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );
}
