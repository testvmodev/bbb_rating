package com.bbb.core.model.response.ebay.additem;

import com.bbb.core.model.response.embedded.ContentCommonId;

import java.util.List;

public class CategoryEbayResponse {
    private long primaryId;
    private String primaryName;
    private List<ContentCommonId> childs;

    public long getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(long primaryId) {
        this.primaryId = primaryId;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public List<ContentCommonId> getChilds() {
        return childs;
    }

    public void setChilds(List<ContentCommonId> childs) {
        this.childs = childs;
    }
}
