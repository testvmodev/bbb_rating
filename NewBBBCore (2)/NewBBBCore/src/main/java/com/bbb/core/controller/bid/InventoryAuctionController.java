package com.bbb.core.controller.bid;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionCreateRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionUpdateRequest;
import com.bbb.core.model.request.sort.InventoryAuctionSort;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.security.authen.usercontext.UserContextManager;
import com.bbb.core.service.bid.InventoryAuctionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class InventoryAuctionController {
    @Autowired
    private InventoryAuctionService service;
    @Autowired
    private UserContextManager userContextManager;

    @GetMapping(value = Constants.URL_GET_INVENTORY_AUCTIONS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getInventoryAuctions(
            @RequestParam(value = "auctionId", required = false) Long auctionId,
            @RequestParam(value = "inventoryId", required = false) Long inventoryId,
            @RequestParam(value = "auctionName", required = false) String auctionName,


            @RequestParam(value = "bicycleIds", required = false) List<Long> bicycleIds,
            @RequestParam(value = "modelIds", required = false) List<Long> modelIds,
            @RequestParam(value = "brandIds", required = false) List<Long> brandIds,
            @RequestParam(value = "typeBicycleNames", required = false) List<String> typeBicycleNames,
            @RequestParam(value = "frameMaterialNames", required = false) List<String> frameMaterialNames,
            @RequestParam(value = "brakeTypeNames", required = false) List<String> brakeTypeNames,
            @RequestParam(value = "sizeNames", required = false) List<String> sizeNames,
            @RequestParam(value = "conditions", required = false) List<ConditionInventory> conditions,
            @RequestParam(value = "startYearId", required = false) Long startYearId,
            @RequestParam(value = "endYearId", required = false) Long endYearId,
            @RequestParam(value = "startPrice", required = false) Float startPrice,
            @RequestParam(value = "endPrice", required = false) Float endPrice,
            @RequestParam(value = "searchContent", required = false) String searchContent,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sortType", required = true, defaultValue = "ASC") Sort sortType,
            @RequestParam(value = "sortField") InventoryAuctionSort sortField,
            @RequestParam(value = "genders", required = false) List<String> genders,
            @RequestParam(value = "suspensions", required = false) List<String> suspensions,
            @RequestParam(value = "wheelSizes", required = false) List<String> wheelSizes,
            @RequestParam(value = "zipCode", required = false) String zipCode,
            @RequestParam(value = "cityName", required = false) String cityName,
            @ApiParam(value = "Can be either stateCode or sateName")
            @RequestParam(value = "state", required = false) String state,
            @ApiParam(value = "Can be either country name or country code")
            @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "latitude", required = false) Float latitude,
            @RequestParam(value = "longitude", required = false) Float longitude,
            @ApiParam(value = "radius (meter)")
            @RequestParam(value = "radius", required = false) Float radius,
            @RequestParam(value = "content", required = false) String content,
            Pageable page
    ) throws ExceptionResponse {
        InventoryAuctionRequest request = new InventoryAuctionRequest();
        request.setAuctionId(auctionId);
        request.setInventoryId(inventoryId);
        request.setAuctionName(auctionName);
        request.setBicycleIds(bicycleIds);
        request.setModelIds(modelIds);
        request.setBrandIds(brandIds);
        request.setFrameMaterialNames(frameMaterialNames);
        request.setBrakeTypeNames(brakeTypeNames);
        request.setSizeNames(sizeNames);
        request.setConditions(conditions);
        request.setStartYearId(startYearId);
        request.setEndYearId(endYearId);
        request.setStartPrice(startPrice);
        request.setEndPrice(endPrice);
        request.setSearchContent(searchContent);
        request.setTypeBicycleNames(typeBicycleNames);
        request.setName(name);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setGenders(genders);
        request.setSuspensions(suspensions);
        request.setWheelSizes(wheelSizes);
        request.setZipCode(zipCode);
        request.setCityName(cityName);
        request.setState(state);
        request.setCountry(country);
        request.setLatitude(latitude);
        request.setLongitude(longitude);
        request.setRadius(radius);
        request.setContent(content);

        return new ResponseEntity(service.getInventoryAuctions(request, page), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_INVENTORY_AUCTION_CREATE)
    public ResponseEntity createInventoryAuction(
            @RequestParam(value = "inventoryId") long inventoryId,
            @RequestParam(value = "auctionId") long auctionId,
            @RequestParam(value = "min") float min,
            @RequestParam(value = "buyItNow", required = false) Float buyItNow,
            @RequestParam(value = "hasBuyItNow", defaultValue = "false") boolean hasBuyItNow
    ) throws ExceptionResponse {
        InventoryAuctionCreateRequest request = new InventoryAuctionCreateRequest();
        request.setInventoryId(inventoryId);
        request.setAuctionId(auctionId);
        request.setMin(min);
        request.setBuyItNow(buyItNow);
        request.setHasBuyItNow(hasBuyItNow);
        return new ResponseEntity<>(service.createInventoryAuction(request), HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_INVENTORY_AUCTION)
    public ResponseEntity updateInventoryAuction(
            @RequestBody InventoryAuctionUpdateRequest request,
            @PathVariable(value = Constants.ID) long inventoryAuctionId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateInventoryAuction(inventoryAuctionId, request), HttpStatus.OK);
    }

    @GetMapping(Constants.URL_INVENTORY_AUCTION)
    public ResponseEntity getInventoryAuctionDetail(
            @PathVariable(value = Constants.ID) long inventoryAuctionId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getInventoryAuction(inventoryAuctionId));
    }

    @GetMapping(Constants.URL_INVENTORY_AUCTION_ALL_BID)
    public ResponseEntity getInventoryAuctionAllBid(
            @PathVariable(value = Constants.ID) long inventoryAuctionId
    ) throws ExceptionResponse{
        return ResponseEntity.ok(service.getInventoryAuctionAllBid(inventoryAuctionId));
    }
}
