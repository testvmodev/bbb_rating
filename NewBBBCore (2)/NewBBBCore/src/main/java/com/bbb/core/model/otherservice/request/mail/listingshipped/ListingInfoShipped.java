package com.bbb.core.model.otherservice.request.mail.listingshipped;

public class ListingInfoShipped {
    private long id;
    private BikeListingShipped bike;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BikeListingShipped getBike() {
        return bike;
    }

    public void setBike(BikeListingShipped bike) {
        this.bike = bike;
    }
}
