package com.bbb.core.model.response.market.marketlisting.detail;

import com.bbb.core.model.database.InventorySale;
import com.bbb.core.model.database.type.*;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.List;

@Entity
public class MarketListingSecretDetailResponse {
    @Id
    private long marketListingId;
    private LocalDateTime createdTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private LocalDateTime timeSold;
    private LocalDateTime timeListed;
    private MarketType marketType;
    private StatusMarketListing status;
    private long inventoryId;
    private long bicycleId;
    private long brandId;
    private long modelId;
    private long yearId;

    //change
    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    //    private String bicycleSizeName;
    private String typeInventoryName;
    private String frameSize;

    private String inventoryName;
    private String serialNumber;

    private String title;
    private Float discountedPrice;
    private Float currentListedPrice;
    private Float msrpPrice;
    private Float initialListPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private String imageDefault;
    private String inventoryDescription;
    private StatusInventory statusInventory;
    private StageInventory stageInventory;
    private ConditionInventory condition;
    private String sellerId;
    private String partnerId;
    private String storefrontId;

    private Boolean isAllowLocalPickup;
    private Boolean isInsurance;
    private Float flatRate;
    private OutboundShippingType shippingType;
    private String zipCode;
    private String countryName;
    private String countryCode;
    private String stateName;
    private String stateCode;
    private String cityName;
    private String county;
    private String addressLine;


    private Long ebayShippingProfileId;
    private Float shippingFee;
    private String shippingProfileLabel;
    private String shippingProfileDescription;
//    private String rating;
    private boolean isDelete;


    @Transient
    private String stageCart;


    @Transient
    private List<String> images;


    //    @Transient
//    private String sizeName;
    @Transient
    private String suspensionName;
    @Transient
    private String frameMaterialName;
    @Transient
    private String brakeName;
    @Transient
    private String bottomBracketName;
    @Transient
    private String genderName;
    @Transient
    private String headsetName;
    @Transient
    private String colorName;
    @Transient
    private String wheelSizeName;


    //    @Transient
//    private String location = ValueCommons.LOCATION_DEFAULT;
    @Transient
    private LocalDateTime timeCall;

    @Transient
    private MarketListingInfoBid auctionInfo;

    @Transient
    private InventorySale sale;

    @Transient
    private Integer offerCount;
    private String paypalEmailSeller;
    @Column(name = "seller_is_bbb")
    private Boolean sellerIsBBB;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getTimeSold() {
        return timeSold;
    }

    public void setTimeSold(LocalDateTime timeSold) {
        this.timeSold = timeSold;
    }

    public LocalDateTime getTimeListed() {
        return timeListed;
    }

    public void setTimeListed(LocalDateTime timeListed) {
        this.timeListed = timeListed;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }

    public StatusMarketListing getStatus() {
        return status;
    }

    public void setStatus(StatusMarketListing status) {
        this.status = status;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public long getModelId() {
        return modelId;
    }

    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(String frameSize) {
        this.frameSize = frameSize;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public String getInventoryDescription() {
        return inventoryDescription;
    }

    public void setInventoryDescription(String inventoryDescription) {
        this.inventoryDescription = inventoryDescription;
    }

    public StatusInventory getStatusInventory() {
        return statusInventory;
    }

    public void setStatusInventory(StatusInventory statusInventory) {
        this.statusInventory = statusInventory;
    }

    public StageInventory getStageInventory() {
        return stageInventory;
    }

    public void setStageInventory(StageInventory stageInventory) {
        this.stageInventory = stageInventory;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }

    public Boolean getInsurance() {
        return isInsurance;
    }

    public void setInsurance(Boolean insurance) {
        isInsurance = insurance;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(Long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public Float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(Float shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getShippingProfileLabel() {
        return shippingProfileLabel;
    }

    public void setShippingProfileLabel(String shippingProfileLabel) {
        this.shippingProfileLabel = shippingProfileLabel;
    }

    public String getShippingProfileDescription() {
        return shippingProfileDescription;
    }

    public void setShippingProfileDescription(String shippingProfileDescription) {
        this.shippingProfileDescription = shippingProfileDescription;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getStageCart() {
        return stageCart;
    }

    public void setStageCart(String stageCart) {
        this.stageCart = stageCart;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getSuspensionName() {
        return suspensionName;
    }

    public void setSuspensionName(String suspensionName) {
        this.suspensionName = suspensionName;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public String getBrakeName() {
        return brakeName;
    }

    public void setBrakeName(String brakeName) {
        this.brakeName = brakeName;
    }

    public String getBottomBracketName() {
        return bottomBracketName;
    }

    public void setBottomBracketName(String bottomBracketName) {
        this.bottomBracketName = bottomBracketName;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getHeadsetName() {
        return headsetName;
    }

    public void setHeadsetName(String headsetName) {
        this.headsetName = headsetName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getWheelSizeName() {
        return wheelSizeName;
    }

    public void setWheelSizeName(String wheelSizeName) {
        this.wheelSizeName = wheelSizeName;
    }

    public LocalDateTime getTimeCall() {
        return timeCall;
    }

    public void setTimeCall(LocalDateTime timeCall) {
        this.timeCall = timeCall;
    }

    public MarketListingInfoBid getAuctionInfo() {
        return auctionInfo;
    }

    public void setAuctionInfo(MarketListingInfoBid auctionInfo) {
        this.auctionInfo = auctionInfo;
    }

    public InventorySale getSale() {
        return sale;
    }

    public void setSale(InventorySale sale) {
        this.sale = sale;
    }

    public Integer getOfferCount() {
        return offerCount;
    }

    public void setOfferCount(Integer offerCount) {
        this.offerCount = offerCount;
    }

    public String getPaypalEmailSeller() {
        return paypalEmailSeller;
    }

    public void setPaypalEmailSeller(String paypalEmailSeller) {
        this.paypalEmailSeller = paypalEmailSeller;
    }

    public Boolean getSellerIsBBB() {
        return sellerIsBBB;
    }

    public void setSellerIsBBB(Boolean sellerIsBBB) {
        this.sellerIsBBB = sellerIsBBB;
    }
}
