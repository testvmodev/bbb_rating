package com.bbb.core.model.response.bid.auction;

public enum StatusAuctionResponse {
    PENDING,
    ACTIVE,
    CLOSE
}
