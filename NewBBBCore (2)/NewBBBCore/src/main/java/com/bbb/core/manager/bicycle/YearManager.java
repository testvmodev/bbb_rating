package com.bbb.core.manager.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.BicycleYear;
import com.bbb.core.model.request.year.YearCreateRequest;
import com.bbb.core.model.request.year.YearUpdateRequest;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.BicycleYearRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Component
public class YearManager implements MessageResponses {
    @Autowired
    private BicycleYearRepository bicycleYearRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;

    public Object getYears(String name) {
        return bicycleYearRepository.findAllName(name == null ? null : "%" + name + "%");
    }

    public Object createYear(YearCreateRequest request) throws ExceptionResponse {
        return createYearIgnoreRole(request);
    }

    public BicycleYear createYearIgnoreRole(YearCreateRequest request) throws ExceptionResponse {
        if (request == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if (request.getName() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_NAME_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST);
        } else {
            request.setName(request.getName().trim());
        }

        if (bicycleYearRepository.findFirstByName(request.getName()) != null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_NAME_EXIST),
                    HttpStatus.BAD_REQUEST);
        }

        CommonUtils.checkYearValid(request.getName());
        long year;
        try {
            year = Long.parseLong(request.getName());
        } catch (Exception e) {
//            e.printStackTrace();
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_INVALID),
                    HttpStatus.BAD_REQUEST);
        }


        BicycleYear bicycleYear = new BicycleYear();
        bicycleYear.setId(year);
        bicycleYear.setName(request.getName());
        bicycleYear.setApproved(false);
        bicycleYear = bicycleYearRepository.save(bicycleYear);
        return bicycleYear;
    }

    public Object updateYear(long yearId, YearUpdateRequest request) throws ExceptionResponse {
        BicycleYear bicycleYear = bicycleYearRepository.findOne(yearId);
        if (bicycleYear == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }

        if (request == null || request.getName() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, YEAR_NOT_NULL),
                    HttpStatus.BAD_REQUEST);
        }
        bicycleYear.setName(request.getName());
        return bicycleYearRepository.save(bicycleYear);
    }


    public Object deleteType(long typeId) throws ExceptionResponse {
        BicycleYear bicycleYear = bicycleYearRepository.findOne(typeId);
        if (bicycleYear == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BRAND_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        int countYear = bicycleRepository.getCountYear(bicycleYear.getId());
        if ( countYear > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, (countYear==1?THERE_IS:THERE_ARE+countYear)+BICYCLE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }

        bicycleYear.setDelete(true);
        bicycleYearRepository.save(bicycleYear);
        return new com.bbb.core.model.response.MessageResponse(SUCCESS);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getYearFromBrandModel(Long brandId, Long modelId) throws ExceptionResponse{
        return contentCommonRepository.findAllYearFromBrandModel(brandId, modelId).stream().map(ContentCommon::getId).collect(Collectors.toList());
    }
}
