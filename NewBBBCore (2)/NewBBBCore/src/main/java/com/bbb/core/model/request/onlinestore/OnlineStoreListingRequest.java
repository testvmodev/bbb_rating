package com.bbb.core.model.request.onlinestore;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.sort.OnlineStoreMyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ObjectError;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import java.util.List;

public class OnlineStoreListingRequest {
    private String content;
    private List<StatusMarketListing> statuses;
    private LocalDate fromDay;
    private LocalDate toDay;
    private OnlineStoreMyListingSortField sortField;
    private Sort sort;
    private Pageable pageable;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        if (content != null) {
            content = content.trim();
        }
        this.content = content;
    }

    public List<StatusMarketListing> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StatusMarketListing> statuses) {
        this.statuses = statuses;
    }

    public LocalDate getFromDay() {
        return fromDay;
    }

    public void setFromDay(LocalDate fromDay) {
        this.fromDay = fromDay;
    }

    public LocalDate getToDay() {
        return toDay;
    }

    public void setToDay(LocalDate toDay) {
        this.toDay = toDay;
    }

    public OnlineStoreMyListingSortField getSortField() {
        return sortField;
    }

    public void setSortField(OnlineStoreMyListingSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public void validate() throws ExceptionResponse {
        if (fromDay != null && toDay != null) {
            if (fromDay.compareTo(toDay) > 0) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MessageResponses.FROM_DAY_CANT_HIGHER_THAN_TO_DAY,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }
}
