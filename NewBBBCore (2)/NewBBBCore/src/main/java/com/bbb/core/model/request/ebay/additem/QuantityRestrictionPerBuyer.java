package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class QuantityRestrictionPerBuyer {
    @JacksonXmlProperty(localName = "MaximumQuantity")
    private int maximumQuantity;

    public int getMaximumQuantity() {
        return maximumQuantity;
    }

    public void setMaximumQuantity(int maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }
}
