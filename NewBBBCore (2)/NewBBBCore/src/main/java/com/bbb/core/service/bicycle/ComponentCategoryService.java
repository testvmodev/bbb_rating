package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.component.ComponentCategoryCreateRequest;
import com.bbb.core.model.request.component.ComponentCategoryGetRequest;
import com.bbb.core.model.request.component.ComponentCategoryUpdateRequest;

public interface ComponentCategoryService {
    Object getComponentCategories(ComponentCategoryGetRequest request) throws ExceptionResponse;

    Object getDetailComponentCategory(long cateId) throws ExceptionResponse;

    Object createComponentCategory(ComponentCategoryCreateRequest request) throws ExceptionResponse;

    Object updateComponentCategory(long cateId, ComponentCategoryUpdateRequest request) throws ExceptionResponse;

    Object deleteComponentCategory(long cateId) throws ExceptionResponse;

}
