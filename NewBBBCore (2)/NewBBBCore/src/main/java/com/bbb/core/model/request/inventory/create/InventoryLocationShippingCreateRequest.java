package com.bbb.core.model.request.inventory.create;

import com.bbb.core.model.database.type.OutboundShippingType;

import javax.validation.constraints.NotNull;

public class InventoryLocationShippingCreateRequest {
    @NotNull
    private OutboundShippingType shippingType;
    @NotNull
    private Boolean isAllowLocalPickup;
//    @NotNull
    private Boolean isInsurance;
//    @NotNull
    private Float flatRate;
    @NotNull
    private String zipCode;
    @NotNull
    private String country;
    @NotNull
    private String state;
    @NotNull
    private String cityName;
//    @NotNull
//    private String county;
    @NotNull
    private String addressLine;

    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public Boolean getInsurance() {
        return isInsurance;
    }

    public void setInsurance(Boolean insurance) {
        isInsurance = insurance;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

//    public String getCounty() {
//        return county;
//    }

//    public void setCounty(String county) {
//        this.county = county;
//    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }
}
