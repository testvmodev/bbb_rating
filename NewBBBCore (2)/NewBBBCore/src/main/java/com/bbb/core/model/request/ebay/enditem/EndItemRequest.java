package com.bbb.core.model.request.ebay.enditem;

import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.bbb.core.model.request.ebay.enditems.EndItemRequestContainer;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "EndItemRequest", namespace = "urn:ebay:apis:eBLBaseComponents")
public class EndItemRequest extends BaseEbayRequest {
    @JacksonXmlProperty(localName = "ItemID")
    private long itemID;
    @JacksonXmlProperty(localName = "EndingReason")
    private String endingReason;


    public long getItemID() {
        return itemID;
    }

    public void setItemID(long itemID) {
        this.itemID = itemID;
    }

    public String getEndingReason() {
        return endingReason;
    }

    public void setEndingReason(String endingReason) {
        this.endingReason = endingReason;
    }
}
