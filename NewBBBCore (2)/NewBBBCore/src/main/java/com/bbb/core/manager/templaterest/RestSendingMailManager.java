package com.bbb.core.manager.templaterest;

import com.bbb.core.common.IntegratedServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestSendingMailManager extends RestTemplateManager {
    @Autowired
    public RestSendingMailManager(IntegratedServices integratedServices) {
        super(integratedServices.getBaseUrlNotify());
    }
}
