package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RequesterCredentials {
    @JacksonXmlProperty(localName = "eBayAuthToken")
    private String eBayAuthToken;

    public String geteBayAuthToken() {
        return eBayAuthToken;
    }

    public void seteBayAuthToken(String eBayAuthToken) {
        this.eBayAuthToken = eBayAuthToken;
    }
}
