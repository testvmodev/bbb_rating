package com.bbb.core.model.request.tradein.tradin;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class TradeInImageGroupRequest {
    private long imageTypeId;
    private List<MultipartFile> image;
    private boolean isRequire;

    public TradeInImageGroupRequest(long imageTypeId, List<MultipartFile> image) {
        this.imageTypeId = imageTypeId;
        this.image = image;
    }

    public long getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(long imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    public List<MultipartFile> getImage() {
        return image;
    }

    public void setImage(List<MultipartFile> image) {
        this.image = image;
    }

    public boolean isRequire() {
        return isRequire;
    }

    public void setRequire(boolean require) {
        isRequire = require;
    }
}
