package com.bbb.core.model.request.inventory.update;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.response.ObjectError;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpStatus;

import java.util.List;

public class InventoryBaseUpdateRequest implements MessageResponses {
    private Long bicycleId;
    private List<String> images;

    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
    private Float currentListPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private Float discountedPrice;

    private ConditionInventory condition;
    private String partnerId;
    private String sellerId;
    private String serialNumber;
    private Long inventoryTypeId;
    private String title;
    private Boolean createPo;
    private Long sizeInventoryId;
    private StatusInventory status;
    private StageInventory stage;
    private Boolean isCustomQuote;
    private Long age;
    private Boolean isPreList;
    private Boolean isCheckInOverride;
    private LocalDate dayHold;
    private Boolean isOverrideCreatePo;
    private Boolean isNonComplianceNoti;
    private RefundReturnTypeInventory refundReturnType;
    private Float priceRole;
    private Float listingAge;
    private LocalDate firstListedDate;
    private LocalDate dateSold;
    private String masterMultiListing;
    private Boolean isReviseItem;
    private RecordTypeInventory recordType;
    private String partnerEmail;
    private String note;
    private String site;
    private Long stockLocationId;
    private LocalDateTime dateReceived;
    private Float conditionScore;
    private Float privatePartyValue;
    private Float valueAdditionalComponent;

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCurrentListPrice() {
        return currentListPrice;
    }

    public void setCurrentListPrice(Float currentListPrice) {
        this.currentListPrice = currentListPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }


    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Long getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(Long inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCreatePo() {
        return createPo;
    }

    public void setCreatePo(Boolean createPo) {
        this.createPo = createPo;
    }

    public Long getSizeInventoryId() {
        return sizeInventoryId;
    }

    public void setSizeInventoryId(Long sizeInventoryId) {
        this.sizeInventoryId = sizeInventoryId;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public Boolean getCustomQuote() {
        return isCustomQuote;
    }

    public void setCustomQuote(Boolean customQuote) {
        isCustomQuote = customQuote;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Boolean getPreList() {
        return isPreList;
    }

    public void setPreList(Boolean preList) {
        isPreList = preList;
    }

    public Boolean getCheckInOverride() {
        return isCheckInOverride;
    }

    public void setCheckInOverride(Boolean checkInOverride) {
        isCheckInOverride = checkInOverride;
    }

    public LocalDate getDayHold() {
        return dayHold;
    }

    public void setDayHold(LocalDate dayHold) {
        this.dayHold = dayHold;
    }

    public Boolean getOverrideCreatePo() {
        return isOverrideCreatePo;
    }

    public void setOverrideCreatePo(Boolean overrideCreatePo) {
        isOverrideCreatePo = overrideCreatePo;
    }

    public Boolean getNonComplianceNoti() {
        return isNonComplianceNoti;
    }

    public void setNonComplianceNoti(Boolean nonComplianceNoti) {
        isNonComplianceNoti = nonComplianceNoti;
    }

    public RefundReturnTypeInventory getRefundReturnType() {
        return refundReturnType;
    }

    public void setRefundReturnType(RefundReturnTypeInventory refundReturnType) {
        this.refundReturnType = refundReturnType;
    }

    public Float getPriceRole() {
        return priceRole;
    }

    public void setPriceRole(Float priceRole) {
        this.priceRole = priceRole;
    }

    public Float getListingAge() {
        return listingAge;
    }

    public void setListingAge(Float listingAge) {
        this.listingAge = listingAge;
    }

    public LocalDate getFirstListedDate() {
        return firstListedDate;
    }

    public void setFirstListedDate(LocalDate firstListedDate) {
        this.firstListedDate = firstListedDate;
    }

    public LocalDate getDateSold() {
        return dateSold;
    }

    public void setDateSold(LocalDate dateSold) {
        this.dateSold = dateSold;
    }

    public String getMasterMultiListing() {
        return masterMultiListing;
    }

    public void setMasterMultiListing(String masterMultiListing) {
        this.masterMultiListing = masterMultiListing;
    }

    public Boolean getReviseItem() {
        return isReviseItem;
    }

    public void setReviseItem(Boolean reviseItem) {
        isReviseItem = reviseItem;
    }

    public RecordTypeInventory getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordTypeInventory recordType) {
        this.recordType = recordType;
    }

    public String getPartnerEmail() {
        return partnerEmail;
    }

    public void setPartnerEmail(String partnerEmail) {
        this.partnerEmail = partnerEmail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getStockLocationId() {
        return stockLocationId;
    }

    public void setStockLocationId(Long stockLocationId) {
        this.stockLocationId = stockLocationId;
    }

    public LocalDateTime getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Float getConditionScore() {
        return conditionScore;
    }

    public void setConditionScore(Float conditionScore) {
        this.conditionScore = conditionScore;
    }

    public Float getPrivatePartyValue() {
        return privatePartyValue;
    }

    public void setPrivatePartyValue(Float privatePartyValue) {
        this.privatePartyValue = privatePartyValue;
    }

    public Float getValueAdditionalComponent() {
        return valueAdditionalComponent;
    }

    public void setValueAdditionalComponent(Float valueAdditionalComponent) {
        this.valueAdditionalComponent = valueAdditionalComponent;
    }

    public void validate() throws ExceptionResponse {
        if (msrpPrice < 0 ) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, MSR_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( bbbValue < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, TRADE_IN_VALUE_MUST_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( cogsPrice < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, COGS_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( flatPriceChange < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, FLAT_PRICE_CHANGE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( priceRole != null && priceRole < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, PRICE_ROLE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }

    }

    public void validateUpdate() throws ExceptionResponse {
        if (msrpPrice != null && msrpPrice < 0 ) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, MSR_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if (bbbValue != null &&  bbbValue < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, TRADE_IN_VALUE_MUST_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( cogsPrice!= null && cogsPrice < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, COGS_PRICE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( flatPriceChange != null && flatPriceChange < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, FLAT_PRICE_CHANGE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }
        if ( priceRole != null && priceRole < 0){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, PRICE_ROLE_MUST_BE_POSITIVE),
                    HttpStatus.NOT_FOUND);
        }

    }

}
