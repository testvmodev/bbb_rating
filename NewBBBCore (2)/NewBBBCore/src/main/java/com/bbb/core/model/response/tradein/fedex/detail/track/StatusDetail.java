package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class StatusDetail {
//    @JacksonXmlProperty(localName = "CreationTime")
//    private DateTime creationTime;
    @JacksonXmlProperty(localName = "Code")
    private String code;
    @JacksonXmlProperty(localName = "Description")
    private String description;
    @JacksonXmlProperty(localName = "Location")
    private Location location;

//    public DateTime getCreationTime() {
//        return creationTime;
//    }
//
//    public void setCreationTime(DateTime creationTime) {
//        this.creationTime = creationTime;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
