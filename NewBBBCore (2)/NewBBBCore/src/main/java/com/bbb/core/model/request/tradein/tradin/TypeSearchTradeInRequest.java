package com.bbb.core.model.request.tradein.tradin;

public enum TypeSearchTradeInRequest {
    DATE("date"),
    ID("id"),
    BICYCLE("bicycle"),
    LOCATION("location"),
    CUSTOMER("customer"),
    STATUS("status"),
    CANCEL("cancel");

    private final String value;

    TypeSearchTradeInRequest(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
