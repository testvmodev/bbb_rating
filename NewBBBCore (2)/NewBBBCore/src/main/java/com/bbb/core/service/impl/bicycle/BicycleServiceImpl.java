package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycle.*;
import com.bbb.core.service.bicycle.BicycleService;
import com.bbb.core.manager.bicycle.BicycleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BicycleServiceImpl implements BicycleService {
    private final BicycleManager manager;

    @Autowired
    public BicycleServiceImpl(BicycleManager manager) {
        this.manager = manager;
    }

    @Override
    public Object getBicycles(BicycleGetRequest request) throws ExceptionResponse {
        return manager.findAllBicycle(request);
    }

    @Override
    public Object createBicycle(BicycleCreateRequest request) throws ExceptionResponse {
        return manager.createBicycle(request);
    }

    @Override
    public Object updateBicycle(long bicycleId, BicycleUpdateRequest request) throws ExceptionResponse {
        return manager.updateBicycle(bicycleId, request);
    }

    @Override
    public Object getBicycleDetail(long bicycleId) throws ExceptionResponse {
        return manager.getBicycleDetail(bicycleId, true);
    }

    @Override
    public Object deleteBicycle(long id) throws ExceptionResponse {
        return manager.deleteBicycle(id);
    }

    @Override
    public Object getBicyclesFromBrandYearModel(BicycleFromBrandYearModelRequest request, Pageable page) throws ExceptionResponse {
        return manager.getBicyclesFromBrandYearModel(request, page);
    }

    @Override
    public Object getBicyclesFromContent(String content, Pageable page) {
        return manager.getBicyclesFromContent(content, page);
    }

    @Override
    public Object getAllCondition() throws ExceptionResponse {
        return manager.getAllCondition();
    }

    @Override
    public void importBicycleFromOlbDb(long offset, long size) {
        manager.importBicycleFromOlbDb(offset, size);
    }

    @Override
    public Object removeImageIncorrect() {
        return manager.removeImageIncorrect();
    }

    @Override
    public Object getBicycleSearchBaseComponent() throws ExceptionResponse {
        return manager.getBicycleSearchBaseComponent();
    }

    @Override
    public Object getBicycleRecommends(long bicycleId) throws ExceptionResponse {
        return manager.getBicycleRecommends(bicycleId);
    }

    @Override
    public Object getBicycleYearFromModel(long brandId, long modelId) throws ExceptionResponse {
        return manager.getBicycleYearFromModel(brandId, modelId);
    }

    @Override
    public Object getValueGuidePrices(long brandId, long modelId, long yearId) throws ExceptionResponse {
        return manager.getValueGuidePrices(brandId, modelId, yearId);
    }
}
