package com.bbb.core.controller.tracking;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.tracking.InventoryTrackingViewService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryTrackingViewController {
    @Autowired
    private InventoryTrackingViewService service;

    @GetMapping(value = Constants.URL_GET_BEST_VIEW_TRACKING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getBestViewTrackingInventory(
            Pageable page
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getBestViews(page), HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_GET_RECENT_VIEW_TRACKING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getRecentTrackingView(
            Pageable page
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getRecentTrackingView(page), HttpStatus.OK);
    }

}
