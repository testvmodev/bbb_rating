package com.bbb.core.model.request.bid.inventoryauction;

public class InventoryAuctionUpdateRequest {
    private Float min;
//    private Boolean isDelete;
    private Float buyItNow;
    private Boolean hasBuyItNow;

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }

//    public Boolean getDelete() {
//        return isDelete;
//    }
//
//    public void setDelete(Boolean delete) {
//        isDelete = delete;
//    }


    public Float getBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(Float buyItNow) {
        this.buyItNow = buyItNow;
    }

    public Boolean getHasBuyItNow() {
        return hasBuyItNow;
    }

    public void setHasBuyItNow(Boolean hasBuyItNow) {
        this.hasBuyItNow = hasBuyItNow;
    }
}
