package com.bbb.core.model.response;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContentDoubleNumber {
    @Id
    private long id;
    private int value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
