package com.bbb.core.model.request.log;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogServiceRequest {
    private String env;
    private String facility;
    private String host;
    @JsonProperty("short_message")
    private String shortMessage;
    private int level;
    private String path;
    private int status;
    @JsonProperty("raw_data")
    private RawData rawData;
    @JsonProperty("raw_headers")
    private RawHeaders rawHeaders;
    @JsonProperty("raw_response")
    private Object rawResponse;

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RawData getRawData() {
        return rawData;
    }

    public void setRawData(RawData rawData) {
        this.rawData = rawData;
    }

    public RawHeaders getRawHeaders() {
        return rawHeaders;
    }

    public void setRawHeaders(RawHeaders rawHeaders) {
        this.rawHeaders = rawHeaders;
    }

    public Object getRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(Object rawResponse) {
        this.rawResponse = rawResponse;
    }
}
