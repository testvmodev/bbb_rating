package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class NameValueList {
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "Value")
    private List<String> values;

    public NameValueList(String name, List<String> values) {
        this.name = name;
        this.values = values;
    }

    public NameValueList() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
