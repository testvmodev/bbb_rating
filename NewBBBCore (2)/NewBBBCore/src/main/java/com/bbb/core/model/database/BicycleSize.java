package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "bicycle_size")
public class BicycleSize {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  private String name;
  private long sortOrder;
  private boolean isStandard;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public long getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(long sortOrder) {
    this.sortOrder = sortOrder;
  }


  public boolean getIsStandard() {
    return isStandard;
  }

  public void setIsStandard(boolean isStandard) {
    this.isStandard = isStandard;
  }

}
