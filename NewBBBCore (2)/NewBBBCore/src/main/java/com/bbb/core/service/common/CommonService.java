package com.bbb.core.service.common;

import com.bbb.core.model.request.common.CommonComponentType;

import java.util.List;

public interface CommonService {
    Object getCommonComponents(List<CommonComponentType> components);
}
