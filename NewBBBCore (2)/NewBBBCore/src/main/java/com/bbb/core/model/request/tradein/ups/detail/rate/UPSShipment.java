package com.bbb.core.model.request.tradein.ups.detail.rate;

import com.bbb.core.model.request.tradein.ups.detail.ShipAddress;
import com.bbb.core.model.request.tradein.ups.detail.ShipService;
import com.bbb.core.model.request.tradein.ups.detail.Shipper;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSShipment {
    @JsonProperty("Shipper")
    private Shipper shipper;
    @JsonProperty("ShipTo")
    private ShipAddress shipTo;
    @JsonProperty("ShipFrom")
    private ShipAddress shipFrom;
    @JsonProperty("Service")
    private ShipService service;
    @JsonProperty("Package")
    private RatePackageDetail ratePackageDetail;

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public ShipAddress getShipTo() {
        return shipTo;
    }

    public void setShipTo(ShipAddress shipTo) {
        this.shipTo = shipTo;
    }

    public ShipAddress getShipFrom() {
        return shipFrom;
    }

    public void setShipFrom(ShipAddress shipFrom) {
        this.shipFrom = shipFrom;
    }

    public ShipService getService() {
        return service;
    }

    public void setService(ShipService service) {
        this.service = service;
    }

    public RatePackageDetail getRatePackageDetail() {
        return ratePackageDetail;
    }

    public void setRatePackageDetail(RatePackageDetail ratePackageDetail) {
        this.ratePackageDetail = ratePackageDetail;
    }
}
