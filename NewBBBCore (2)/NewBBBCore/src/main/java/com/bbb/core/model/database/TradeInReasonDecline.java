package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_reason_decline")
public class TradeInReasonDecline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String reason;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
