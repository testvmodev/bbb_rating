package com.bbb.core.service.schedule;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.schedule.ScheduleJobConfigUpdateRequest;

public interface ScheduleJobConfigService {
    Object getJobConfigs(Boolean isEnable);

    Object getRunningJobs();

    Object getInqueueJobs();

    Object getJobDetail(long id) throws ExceptionResponse;

    Object updateJobConfig(
            long id,
            ScheduleJobConfigUpdateRequest request
    ) throws ExceptionResponse;
}
