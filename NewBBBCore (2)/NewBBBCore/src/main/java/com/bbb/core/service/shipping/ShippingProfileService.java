package com.bbb.core.service.shipping;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.shipping.ShippingFeeConfigCreateRequest;
import com.bbb.core.model.request.shipping.ShippingFeeConfigUpdateRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ShippingProfileService {
    Object getShippingProfiles(Pageable pageable);

    Object addShippingProfile(ShippingFeeConfigCreateRequest request) throws ExceptionResponse;

    Object updateShippingProfile(long bicycleTypeId, ShippingFeeConfigUpdateRequest request) throws ExceptionResponse;

    Object getInventoryShippingProfiles(List<Long> inventoryIds);
}
