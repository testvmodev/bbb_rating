package com.bbb.core.model.request.inventory.create;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class InventoryCompDetailCreateRequest {
    @NotNull
    private Long inventoryCompTypeId;
    @NotNull
    @NotBlank
    private String value;

    public Long getInventoryCompTypeId() {
        return inventoryCompTypeId;
    }

    public void setInventoryCompTypeId(Long inventoryCompTypeId) {
        this.inventoryCompTypeId = inventoryCompTypeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
