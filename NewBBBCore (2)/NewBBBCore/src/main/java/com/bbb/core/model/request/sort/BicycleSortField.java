package com.bbb.core.model.request.sort;

public enum  BicycleSortField {
    ID("id"),
    BRAND("brand_id"),
    MODEL("model_id"),
    YEAR("year_id");

    private final String value;

    BicycleSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
