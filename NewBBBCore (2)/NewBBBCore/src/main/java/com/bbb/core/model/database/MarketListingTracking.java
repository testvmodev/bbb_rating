//package com.bbb.core.model.database;
//
//import com.bbb.core.model.database.embedded.MarketListingTrackingId;
//import org.hibernate.annotations.Generated;
//import org.hibernate.annotations.GenerationTime;
//import org.joda.time.LocalDateTime;
//import org.springframework.data.annotation.CreatedDate;
//import org.springframework.data.annotation.LastModifiedDate;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "market_listing_tracking")
//public class MarketListingTracking {
//    @EmbeddedId
//    private MarketListingTrackingId id;
//    @CreatedDate
//    @Generated(GenerationTime.INSERT)
//    private LocalDateTime createdTime;
//    @LastModifiedDate
//    @Generated(GenerationTime.ALWAYS)
//    private LocalDateTime lastUpdate;
//
//    public MarketListingTrackingId getId() {
//        return id;
//    }
//
//    public void setId(MarketListingTrackingId id) {
//        this.id = id;
//    }
//
//    public LocalDateTime getCreatedTime() {
//        return createdTime;
//    }
//
//    @Generated(GenerationTime.INSERT)
//    public void setCreatedTime(LocalDateTime createdTime) {
//        this.createdTime = createdTime;
//    }
//
//    public LocalDateTime getLastUpdate() {
//        return lastUpdate;
//    }
//
//    @Generated(GenerationTime.ALWAYS)
//    public void setLastUpdate(LocalDateTime lastUpdate) {
//        this.lastUpdate = lastUpdate;
//    }
//
//}
