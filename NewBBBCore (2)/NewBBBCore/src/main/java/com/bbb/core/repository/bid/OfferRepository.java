package com.bbb.core.repository.bid;

import com.bbb.core.model.database.Offer;
import com.bbb.core.model.database.type.StatusOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OfferRepository extends JpaRepository<Offer, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM offer " +
            "WHERE id = ?1")
    Offer findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM offer")
    List<Offer> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id)  " +
            "FROM offer " +
            "WHERE " +
            "offer.market_listing_id = :marketListingId " +
            "AND (offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()} OR offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).COMPLETED.getValue()}) " +
            "AND offer.is_delete = 0"
    )
    int getNumberAcceptedOrCompleted(
            @Param(value = "marketListingId") long marketListingId
    );


    @Query(nativeQuery = true, value = "SELECT * FROM offer WHERE offer.id IN :offerIds")
    List<Offer> findAllByIds(
            @Param(value = "offerIds") List<Long> offerIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM offer " +
            "WHERE " +
            "offer.id NOT IN :offerIds AND " +
            "offer.inventory_auction_id IN :inventoryAuctionIds AND " +
            "(offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} OR offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).COMPLETED.getValue()})"
    )
    List<Offer> findAllByNotIdsAndPendingOrCounted(
            @Param(value = "offerIds") List<Long> offerIds,
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );


    @Query(nativeQuery = true, value = "SELECT * FROM offer " +
            "WHERE " +
            "(:invAucNull = 1 OR offer.inventory_auction_id IN :inventoryAuctionIds) AND " +
            "(:marListNull = 1 OR offer.market_listing_id IN :marketListingIds) AND " +
            "offer.is_delete = 0 AND " +
            "(offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} OR " +
            "offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).COUNTERED.getValue()}) AND " +
            "(:excludeOfferId IS NULL OR offer.id <> :excludeOfferId)"
    )
    List<Offer> findPendingCounterByAuctionOrListing(
            @Param(value = "invAucNull") boolean invAucNull,
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "marListNull") boolean marListNull,
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "excludeOfferId") Long excludeOfferId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM offer " +
            "WHERE " +
            "1 = :#{(!@queryUtils.isEmptyList(#inventoryAuctionIds) || !@queryUtils.isEmptyList(#marketListingIds)) ? 1 : 0} " +
            "AND is_delete = 0 " +
            "AND (status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).COUNTERED.getValue()}" +
            ")" +
            "AND (1 = :#{@queryUtils.isEmptyList(#inventoryAuctionIds) ? 1 : 0} " +
                "OR offer.inventory_auction_id IN :#{@queryUtils.isEmptyList(#inventoryAuctionIds) ? @queryUtils.fakeLongs() : #inventoryAuctionIds}" +
            ")" +
            "AND (1 = :#{@queryUtils.isEmptyList(#marketListingIds) ? 1 : 0} " +
                "OR offer.market_listing_id IN :#{@queryUtils.isEmptyList(#marketListingIds) ? @queryUtils.fakeLongs() : #marketListingIds}" +
            ")"
    )
    List<Offer> findActiveByAuctionOrListing(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id) " +
            "FROM offer " +
            "WHERE " +
            "offer.buyer_id = :userId AND " +
            "offer.is_delete = 0 AND " +
            "offer.is_paid = 0 AND " +
//            "offer.inventory_auction_id IS NOT NULL AND " +
            "(offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} OR offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()})")
    int getTotalCount(
            @Param(value = "userId") String userId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM offer " +
            "WHERE is_delete = 0 " +
            "AND market_listing_id = :marketListingId " +
            "AND (status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).COUNTERED.getValue()}" +
            ")"
    )
    List<Offer> findAllActive(
            @Param(value = "marketListingId") Long marketListingId
    );

    @Query(nativeQuery = true, value = "SELECT MAX(offer.offer_price) " +
            "FROM offer " +
            "WHERE offer.market_listing_id = :marketListingId AND " +
            "(offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} OR " +
            "offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).name()})"
    )
    Double findMaxPriceOfferMarketListing(
            @Param(value = "marketListingId") long marketListingId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM offer " +

            "WHERE buyer_id = :userId " +
            "AND is_delete = 0 " +
            "AND (status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()} " +
                "OR status = :#{T(com.bbb.core.model.database.type.StatusOffer).COUNTERED.getValue()}" +
            ")"
    )
    List<Offer> findAllActiveOfUser(
            @Param("userId") String userId
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE offer set offer.status = :status WHERE offer.id = :offerId")
    void updateStatusOffer(
            @Param(value = "offerId") long offerId,
            @Param(value = "status") String status
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM offer " +
            "WHERE buyer_id = :userId " +
            "AND (:status IS NULL " +
            "OR status = :#{#status == null ? ' ' : #status.getValue()}) " +
            "AND is_paid = :isPaid " +
            "AND is_delete = 0")
    int countUserOffers(
            @Param(value = "userId") String userId,
            @Param("status") StatusOffer status,
            @Param("isPaid") boolean isPaid
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(offer.id)  " +
            "FROM offer " +
            "WHERE " +
            "offer.market_listing_id = :marketListingId " +
            "AND offer.is_delete = 0"
    )
    int countListingOffers(
            @Param(value = "marketListingId") long marketListingId
    );
}
