package com.bbb.core.security.authen.jwt;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.security.authen.extractor.TokenExtractor;
import com.bbb.core.security.exception.NoAuthorizationException;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ducnd on 6/25/17.
 */
public class JwtTokenAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
    private static final Logger LOG = LoggerFactory.getLogger(JwtTokenAuthenticationProcessingFilter.class);
    private final AuthenticationFailureHandler failureHandler;
    private final TokenExtractor tokenExtractor;
    private final JwtTokenFactory jwtTokenFactory;
    private final OrRequestMatcher skipMatchers;
    private final OrRequestMatcher authenMatchers;

    public JwtTokenAuthenticationProcessingFilter(List<Pair<HttpMethod, String>> pathsToSkip,
                                                  List<Pair<HttpMethod, String>> pathsMustAuthen,
                                                  String processingPath,
                                                  AuthenticationFailureHandler failureHandler,
                                                  JwtTokenFactory jwtTokenFactory,
                                                  TokenExtractor tokenExtractor) {
        super(new AntPathRequestMatcher(processingPath));

        //if doesnt contain in both list, still process by default
        //only skip when contain in skipList and not in authenList
        List<RequestMatcher> skipMatchers = pathsToSkip.stream()
                .map(p -> new AntPathRequestMatcher(p.getSecond(), p.getFirst().name()))
                .collect(Collectors.toList());
        this.skipMatchers = new OrRequestMatcher(skipMatchers);

        List<RequestMatcher> processMatchers = pathsMustAuthen.stream()
                .map(p -> new AntPathRequestMatcher(p.getSecond(), p.getFirst().name()))
                .collect(Collectors.toList());
        this.authenMatchers = new OrRequestMatcher(processMatchers);


        this.failureHandler = failureHandler;
        this.jwtTokenFactory = jwtTokenFactory;
        this.tokenExtractor = tokenExtractor;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String tokenPayload = httpServletRequest.getHeader(Constants.HEADER_NAME_AUTH);
        LOG.info("attemptAuthentication tokenPayload: " + tokenPayload);
        Authentication authentication;
        try {
            JwtAuthenticationToken authenticationToken = new JwtAuthenticationToken(
                    jwtTokenFactory.parseToken(tokenExtractor.extract(tokenPayload)));
            authentication = getAuthenticationManager().authenticate(authenticationToken);
        } catch (NoAuthorizationException e) {
            if (!authenMatchers.matches(httpServletRequest) && skipMatchers.matches(httpServletRequest)) {
                return new GuestAuthentication();
            } else {
                throw e;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadCredentialsException(e.getMessage() != null ? e.getMessage() : MessageResponses.INVALID_TOKEN, e);
        }
        return authentication;

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authResult);
        SecurityContextHolder.setContext(context);
        chain.doFilter(request, response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        failureHandler.onAuthenticationFailure(request, response, failed);
    }

}
