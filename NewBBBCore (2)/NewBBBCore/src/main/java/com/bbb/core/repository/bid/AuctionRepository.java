package com.bbb.core.repository.bid;

import com.bbb.core.model.database.Auction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AuctionRepository extends JpaRepository<Auction, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM auction " +
            "WHERE id = ?1")
    Auction findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM auction")
    List<Auction> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM auction " +
            "WHERE " +
            "(:createdById IS NULL OR auction.created_by_id = :createdById) " +
            "ORDER BY id OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<Auction> findAll(
            @Param(value = "createdById") String createdById,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM auction " +
            "WHERE " +
            "(:createdById IS NULL OR auction.created_by_id = :createdById)"
    )
    int countNumber(
            @Param(value = "createdById") String createdById
    );


    @Query(nativeQuery = true, value = "SELECT * FROM auction " +
            "WHERE " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) < 0 " +
            "AND auction.is_expired = 0 " +
            "AND auction.is_started = 1 " +
            "AND :auctionId IS NULL OR auction.id = :auctionId"
    )
    List<Auction> getAuctionExpireNoYetUpdate(
            @Param(value = "now") String now,
            @Param(value = "auctionId") Long auctionId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM auction " +
            "WHERE auction.is_started = 0 " +
            "AND auction.is_expired = 0 " +
            "AND DATEDIFF_BIG(MILLISECOND,  auction.start_date, GETDATE()) > 0"
    )
    List<Auction> findAllAuctionStart();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM auction " +
            "WHERE auction.id IN :auctionIds")
    List<Auction> findAllById(
            @Param(value = "auctionIds") List<Long> auctionIds
    );
}
