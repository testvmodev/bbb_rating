package com.bbb.core.repository.expirationtime;

import com.bbb.core.model.database.ExpirationTimeConfig;
import com.bbb.core.model.database.type.TypeExpirationTime;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpirationTimeConfigRepository extends CrudRepository<ExpirationTimeConfig, Long> {
//    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
//            "FROM expiration_time_config " +
//            "WHERE expire_type = :typeExpiration")
    ExpirationTimeConfig findFirstByType (
            @Param(value = "typeExpiration") TypeExpirationTime typeExpiration
    );

    @Query(nativeQuery = true, value = "SELECT * FROM expiration_time_config WHERE " +
            "expiration_time_config.expire_type = :#{T(com.bbb.core.model.database.type.TypeExpirationTime).OFFER_AUCTION.getValue()} OR " +
            "expiration_time_config.expire_type = :#{T(com.bbb.core.model.database.type.TypeExpirationTime).OFFER_MARKET_LISTING.getValue()} "
    )
    List<ExpirationTimeConfig> findFirstByOfferAuctionOrOfferMarketListing ();
}
