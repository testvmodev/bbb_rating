package com.bbb.core.model.database.type;

import java.util.ArrayList;
import java.util.List;

public enum StageInventory {
    ACCEPTED("Accepted"),
    WAITING_ON_PARTS("Waiting on Parts"),
    PART_OUT("Part Out"),
    THIRTY_DAY_HOLD("30 Day Hold"),
    PHOTOGRAPHY("Photography"),
    READY_LISTED("Ready to be Listed"),
    LISTED("Listed"),
    BOXED("Boxed"),
    SOLD("Sold"),
    SALE_PENDING("Sale Pending"),
    UPS_STAGING("UPS Staging"),
    BBB_UPS_CLAIM("BBB UPS Claim"),
    LISTED_ACTIVE_BIDS("List-Active Bids"),
    ARCHIVED("Archived"),
    INCOMPLETE_BICYCLE_DETAILS("Incomplete - Bicycle Details"),
    INCOMPLETE_CUSTOM_QUOTE("Incomplete - Custome Quote"),
    INCOMPLETE_SUMMARY("Incomplete - Summary"),
    INCOMPLETE_UPDATE_IMAGES("Incomplete - Update Images"),
    INCOMPLETE_TRANSACTION_DETAILS("Incomplete - Transaction Details"),
    CUSTOM_QUOTE_PENDING_REVIEW("Custom Quote - Pending Review"),
    COMPLETE("Complete"),
    CUSTOM_QUOTE_REVIEWED("Custom Quote Reviewed"),
    DECLINED("Declined"),
    EXPIRED("Expired"),
    STAFF_USER("Staff User"),
    SHIPPED_TO_CUSTOMER("Shipped to Customer"),
    RECEIVED_BY_CUSTOMER("Received by Customer"),
    SCORECARD_DECLINED_DEALER("Scorecard Declined - Dealer"),
    DOES_NOT_QUALIFY("Does Not Qualify"),
    RETURN_TO_DEALER_NON_COMPLIANT("Return to Dealer - Non-Compliant"),
    IN_TRANSIT_TO_BBB("In Transit to BBB"),
    RECEIVED_AT_WAREHOUSE("Received at Warehouse"),
    INSPECTION("Inspection"),
    NON_COMPLIANT("Non-Compliant"),
    CUSTOMER_RETURN_DAMAGE("Customer Return - Damage"),
    CUSTOMER_RETURN("Customer Return"),
    RESERVED("Reserved"),
    DEALER_UPS_CLAIM("Dealer UPS Claim"),
    LAYAWAY_HOLD("Layaway Hold");

    private final String value;

    StageInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StageInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (StageInventory stageInventory : StageInventory.values()) {
            if (stageInventory.getValue().equals(value)){
                return stageInventory;
            }
        }
        return null;
    }

    public static List<StageInventory> findListStageInventory(StatusInventory status) {
//        IN_ACTIVE("In-Active"),
//                ACTIVE("Active"),
//                SOLD("Sold"),
//                DECLINED("Declined"),
//                PENDING("Pending"),
//                NONE_COMPLIANT("Non-Compliant");
        List<StageInventory> stageInventories = new ArrayList<>();
        switch (status) {
            case IN_ACTIVE:
                stageInventories.add(PART_OUT);
                stageInventories.add(DEALER_UPS_CLAIM);
                stageInventories.add(ARCHIVED);
                stageInventories.add(INCOMPLETE_BICYCLE_DETAILS);
                stageInventories.add(INCOMPLETE_CUSTOM_QUOTE);
                stageInventories.add(INCOMPLETE_SUMMARY);
                stageInventories.add(INCOMPLETE_UPDATE_IMAGES);
                stageInventories.add(INCOMPLETE_TRANSACTION_DETAILS);
                stageInventories.add(CUSTOM_QUOTE_PENDING_REVIEW);
                stageInventories.add(COMPLETE);
                stageInventories.add(CUSTOM_QUOTE_REVIEWED);
                stageInventories.add(DECLINED);
                stageInventories.add(STAFF_USER);
                break;
            case ACTIVE:
                stageInventories.add(ACCEPTED);
                stageInventories.add(WAITING_ON_PARTS);
                stageInventories.add(PART_OUT);
                stageInventories.add(THIRTY_DAY_HOLD);
                stageInventories.add(PHOTOGRAPHY);
                stageInventories.add(READY_LISTED);
                stageInventories.add(BOXED);
                stageInventories.add(SOLD);
                stageInventories.add(UPS_STAGING);
                stageInventories.add(BBB_UPS_CLAIM);
                stageInventories.add(LISTED_ACTIVE_BIDS);
                stageInventories.add(LAYAWAY_HOLD);
                break;
            case SOLD:
                stageInventories.add(SOLD);
                stageInventories.add(SHIPPED_TO_CUSTOMER);
                stageInventories.add(RECEIVED_BY_CUSTOMER);
                break;
            case DECLINED:
                stageInventories.add(SCORECARD_DECLINED_DEALER);
                stageInventories.add(DOES_NOT_QUALIFY);
                stageInventories.add(RETURN_TO_DEALER_NON_COMPLIANT);
                break;
            case PENDING:
                stageInventories.add(IN_TRANSIT_TO_BBB);
                stageInventories.add(RECEIVED_AT_WAREHOUSE);
                stageInventories.add(INSPECTION);
                break;
            case NONE_COMPLIANT:
                stageInventories.add(NON_COMPLIANT);
                break;
            case RETURNED:
                stageInventories.add(CUSTOMER_RETURN_DAMAGE);
                stageInventories.add(CUSTOMER_RETURN);
                break;
            case RESERVED:
                stageInventories.add(RESERVED);
                break;
            default:
                break;
        }
        return stageInventories;
    }


    public static boolean checkValid(StageInventory stage, StatusInventory statusInventory) {
        return findListStageInventory(statusInventory).contains(stage);
    }
}
