package com.bbb.core.model.request.market.listingwizard;

public enum  ListingWizardType {
    ACTIVE,LISTED,QUEUED,ERROR
}
