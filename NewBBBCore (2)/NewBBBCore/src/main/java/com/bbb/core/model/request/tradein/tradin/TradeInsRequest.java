package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.database.type.StatusTradeIn;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;


public class TradeInsRequest {
    private Boolean isArchived;
    private StatusTradeIn status;
    private TypeSearchTradeInRequest typeSearch;
    private String content;
    private String partnerId;
    private Sort sort;
    private Pageable page;
    private Boolean isIncomplete;

    public TradeInsRequest(
            Boolean isArchived, StatusTradeIn status,
            TypeSearchTradeInRequest typeSearch, String content,
            String partnerId,
            Sort sort, Pageable page) {
        this.isArchived = isArchived;
        this.status = status;
        this.typeSearch = typeSearch;
        this.content = content;
        this.partnerId = partnerId;
        this.sort = sort;
        this.page = page;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getArchived() {
        return isArchived;
    }

    public void setArchived(Boolean archived) {
        isArchived = archived;
    }

    public StatusTradeIn getStatus() {
        return status;
    }

    public void setStatus(StatusTradeIn status) {
        this.status = status;
    }

    public TypeSearchTradeInRequest getTypeSearch() {
        return typeSearch;
    }

    public void setTypeSearch(TypeSearchTradeInRequest typeSearch) {
        this.typeSearch = typeSearch;
    }


    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public Pageable getPage() {
        return page;
    }

    public void setPage(Pageable page) {
        this.page = page;
    }

    public Boolean getIncomplete() {
        return isIncomplete;
    }

    public void setIncomplete(Boolean incomplete) {
        isIncomplete = incomplete;
    }
}
