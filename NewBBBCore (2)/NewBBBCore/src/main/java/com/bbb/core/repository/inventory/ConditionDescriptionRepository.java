package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.ConditionDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ConditionDescriptionRepository extends JpaRepository<ConditionDescription, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM condition_description " +
            "WHERE condition_description.condition = :condition")
    ConditionDescription findOne(@Param(value = "condition") String condition);
}
