package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface InventoryAuctionResponseRepository extends CrudRepository<InventoryAuctionResponse, Long> {


    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.auction_id AS auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.created_time AS created_time, " +
            "inventory_auction.min AS min, " +
            "inventory_auction.has_buy_it_now AS has_buy_it_now, " +
            "inventory_auction.buy_it_now AS buy_it_now, " +
            "auction.name AS auction_name, " +

            "(CASE WHEN max_offer.number_bid IS NULL THEN 0 ELSE max_offer.number_bid END) AS number_bids, " +
            "max_offer.max_price AS current_highest_bid, " +

            "inventory.title AS inventory_title, " +
            "inventory.image_default AS image_default, " +
            "auction_duration.duration AS duration, " +

            //start
            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_model.name as bicycle_model_name, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_brand.name AS bicycle_brand_name, " +
//            "bicycle_year.id AS year_id, " +
//            "bicycle_year.name AS bicycle_year_name," +
//            "bicycle_type.id AS type_id, " +
//            "bicycle_type.name AS bicycle_type_name, " +
//            "bicycle_size.id AS size_id, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +

//            "inventory_bicycle.model_id AS model_id, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
//            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inventory_bicycle.year_id AS year_id , " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +

            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.frame_material_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +
            "gender.gender_name AS gender, " +
            "suspension.suspension_type AS suspension, " +
            "wheel_size.wheel_size_name AS wheel_size, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.stage AS stage, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.value_additional_component AS value_additional_component, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name " +
            //end

            "FROM inventory_auction " +

            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN " +
                "(SELECT auction.id AS auction_id, DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
                "FROM auction) AS auction_duration ON inventory_auction.auction_id = auction_duration.auction_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Size') " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT " +
                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "offer.inventory_id AS inventory_id, " +
//                "offer.auction_id AS auction_id, " +
                "COUNT(*) AS number_bid, " +
                "MAX(offer.offer_price) AS max_price  " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id" +
                ") AS max_offer " +
//                "GROUP BY offer.inventory_id, offer.auction_id ) AS max_offer " +
            "ON inventory_auction.id = max_offer.inventory_auction_id " +
//            "ON inventory_auction.inventory_id = max_offer.inventory_id AND inventory_auction.auction_id = max_offer.auction_id "+

//            "LEFT JOIN " +
//                "(SELECT " +
//                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "MAX(offer.offer_price) AS max_price  " +
//                "FROM offer " +
//                "WHERE " +
//                "offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL AND " +
//                "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                "GROUP BY offer.inventory_auction_id" +
//                ") AS max_offer_price " +
//             "ON inventory_auction.id = max_offer_price.inventory_auction_id " +

            //start
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
//            "JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            "CROSS APPLY (SELECT " +
                "GEOGRAPHY\\:\\:Point(ISNULL(inventory_location_shipping.latitude, 0), ISNULL(inventory_location_shipping.longitude, 0), 4326).STDistance(GEOGRAPHY\\:\\:Point(:latitude, :longitude, 4326)) AS distance " +
            ") AS geography " +

            //end
            "WHERE " +
            "(:auctionId IS NULL OR inventory_auction.auction_id = :auctionId) AND " +
            "(:inventoryId IS NULL OR inventory_auction.inventory_id = :inventoryId) AND " +
            "(:auctionName IS NULL OR auction.name LIKE :auctionName) AND " +
            "inventory_auction.is_delete = 0 AND " +
            "inventory_auction.is_inactive = 0 AND " +

            //start
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR inventory_bicycle.model_id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR inventory_bicycle.brand_id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory.bicycle_type_name IN :typeBicycleNames) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR frame_size.frame_size_name IN :sizeNames) AND " +
            "(:isGenderNull = 1 OR gender.gender_name IN :genders) AND " +
            "(:isSuspensionNull = 1 OR suspension.suspension_type IN :suspensions) AND " +
            "(:isWheelSizeNull = 1 OR wheel_size.wheel_size_name IN :wheelSizes) AND " +

            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +
            "(:name IS NULL OR inventory.title LIKE :name) AND " +

            "(:zipCode IS NULL OR inventory_location_shipping.zip_code = :zipCode) AND " +
            "(:cityName IS NULL OR " +
                "inventory_location_shipping.city_name LIKE :#{'%' + #cityName + '%'}) AND " +
            "(:state IS NULL OR " +
                "inventory_location_shipping.state_name LIKE :#{'%' + #state + '%'} OR " +
                "inventory_location_shipping.state_code LIKE :#{'%' + #state + '%'}) AND " +
            "(:country IS NULL " +
                "OR inventory_location_shipping.country_name LIKE :#{'%' + #country + '%'} " +
                "OR inventory_location_shipping.country_code LIKE :#{'%' + #country + '%'}) AND " +
            "(:isLocationRadiusNull = 1 OR " +
                "(distance <= :radius AND " +
                "inventory_location_shipping.latitude IS NOT NULL AND " +
                "inventory_location_shipping.longitude IS NOT NULL)) AND " +

            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR inventory_bicycle.year_id >= :startYearId) AND " +
            "(:endYearId IS NULL OR inventory_bicycle.year_id <= :endYearId) AND " +

            "(:content IS NULL OR (" +
            "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} " +
            "OR CAST(inventory.description AS nvarchar(MAX)) collate SQL_Latin1_General_CP1_CI_AS  LIKE :#{'%' + #content + '%'} " +
            ")) AND " +

            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND "+
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 "+
            //end


            //sort
            "ORDER BY  " +
                "CASE WHEN :sortField = :#{T(com.bbb.core.model.request.sort.InventoryAuctionSort).DURATION_AUCTION.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN auction_duration.duration END DESC, " +
                "CASE WHEN :sortField = :#{T(com.bbb.core.model.request.sort.InventoryAuctionSort).DURATION_AUCTION.name()} AND :sortType != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN auction_duration.duration END ASC, " +
                "CASE WHEN :sortField = :#{T(com.bbb.core.model.request.sort.InventoryAuctionSort).MAX_PRICE_OFFER.name()} AND :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN max_offer.max_price END DESC, " +
                "CASE WHEN :sortField = :#{T(com.bbb.core.model.request.sort.InventoryAuctionSort).MAX_PRICE_OFFER.name()} AND :sortType != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN max_offer.max_price END ASC," +
                "CASE WHEN :sortType = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory_auction.created_time END DESC, " +
                "CASE WHEN :sortType = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory_auction.created_time END ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<InventoryAuctionResponse> findAllSorted(
            @Param(value = "auctionId") Long auctionId,
            @Param(value = "inventoryId") Long inventoryId,
            @Param(value = "auctionName") String auctionName,


            //start
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeBicycleNames") List<String> typeBicycleNames,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,
            @Param("isGenderNull") boolean isGenderNull, @Param("genders") List<String> genders,
            @Param("isSuspensionNull") boolean isSuspensionNull, @Param("suspensions") List<String> suspensions,
            @Param("isWheelSizeNull") boolean isWheelSizeNull, @Param("wheelSizes") List<String> wheelSizes,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,
            @Param("name") String name,

            @Param("zipCode") String zipCode,
            @Param("cityName") String cityName,
            @Param("state") String state,
            @Param("country") String country,
            @Param("isLocationRadiusNull") boolean isLocationRadiusNull,
            @Param("latitude") Float latitude,
            @Param("longitude") Float longitude,
            @Param("radius") Float radius,

            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("content") String content,

            @Param("now") String now,

            //end

            @Param("sortField") String sortField,
            @Param("sortType") String sortType,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );


    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM inventory_auction " +

            //start
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Size') " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "WHERE " +
            "(:auctionId IS NULL OR inventory_auction.auction_id = :auctionId) AND " +
            "(:inventoryId IS NULL OR inventory_auction.inventory_id = :inventoryId) AND " +
            "(:auctionName IS NULL OR auction.name LIKE :auctionName) AND " +
            "inventory_auction.is_delete = 0 AND " +
            "inventory_auction.is_inactive = 0 AND " +

            //start
            "(:isBicycledNull = 1 OR inventory.bicycle_id IN :bicycleIds) AND " +
            "(:isModelNull = 1 OR inventory_bicycle.model_id IN :modelIds) AND " +
            "(:isBrandNull = 1 OR inventory_bicycle.brand_id IN :brandIds) AND " +
            "(:isTypeNull = 1 OR inventory.bicycle_type_name IN :typeBicycleNames) AND " +

            "(:isFrameNull = 1 OR frame_material.fragment_material_name IN :frameMaterialNames) AND " +
            "(:isBrakeNull = 1 OR brake_type.brake_type_name IN :brakeTypeNames) AND " +
            "(:isSizeNull = 1 OR frame_size.frame_size_name IN :sizeNames) AND " +
            "(:isGenderNull = 1 OR gender.gender_name IN :genders) AND " +
            "(:isSuspensionNull = 1 OR suspension.suspension_type IN :suspensions) AND " +
            "(:isWheelSizeNull = 1 OR wheel_size.wheel_size_name IN :wheelSizes) AND " +

            "(:isConditionNull = 1 OR inventory.condition IN :conditions) AND " +
            "(:name IS NULL OR inventory.title LIKE :name) AND " +

            "(:zipCode IS NULL OR inventory_location_shipping.zip_code = :zipCode) AND " +
            "(:cityName IS NULL OR " +
                "inventory_location_shipping.city_name LIKE :#{'%' + #cityName + '%'}) AND " +
            "(:state IS NULL OR " +
                "inventory_location_shipping.state_name LIKE :#{'%' + #state + '%'} OR " +
                "inventory_location_shipping.state_code LIKE :#{'%' + #state + '%'}) AND " +
            "(:country IS NULL " +
                "OR inventory_location_shipping.country_name LIKE :#{'%' + #country + '%'} " +
                "OR inventory_location_shipping.country_code LIKE :#{'%' + #country + '%'}) AND " +
            "(:isLocationRadiusNull = 1 OR " +
                "(GEOGRAPHY\\:\\:Point(ISNULL(inventory_location_shipping.latitude, 0), ISNULL(inventory_location_shipping.longitude, 0), 4326).STDistance(GEOGRAPHY\\:\\:Point(:latitude, :longitude, 4326)) <= :radius AND " +
                "inventory_location_shipping.latitude IS NOT NULL AND inventory_location_shipping.longitude IS NOT NULL)) AND " +

            "(:isStartPriceNull = 1 OR inventory.current_listed_price >= :startPriceValue) AND " +
            "(:isEndPriceNull = 1 OR inventory.current_listed_price <= :endPriceValue) AND " +
            "(:startYearId IS NULL OR inventory_bicycle.year_id >= :startYearId) AND " +
            "(:endYearId IS NULL OR inventory_bicycle.year_id <= :endYearId) AND " +

            "(:content IS NULL OR (" +
            "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} " +
            "OR CAST(inventory.description AS nvarchar(MAX)) collate SQL_Latin1_General_CP1_CI_AS  LIKE :#{'%' + #content + '%'} " +
            ")) AND " +

            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) <= 0 AND "+
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) >= 0 "
            //end
    )
    int getNumberTotal(
            @Param(value = "auctionId") Long auctionId,
            @Param(value = "inventoryId") Long inventoryId,
            @Param(value = "auctionName") String auctionName,


            //start
            @Param("isBicycledNull") boolean isBicycledNull, @Param("bicycleIds") List<Long> bicycleIds,
            @Param("isModelNull") boolean isModelNull, @Param("modelIds") List<Long> modelIds,
            @Param("isBrandNull") boolean isBrandNull, @Param("brandIds") List<Long> brandIds,
            @Param("isTypeNull") boolean isTypeNull, @Param("typeBicycleNames") List<String> typeBicycleNames,
            @Param("isFrameNull") boolean isFrameNull, @Param("frameMaterialNames") List<String> frameMaterialNames,
            @Param("isBrakeNull") boolean isBrakeNull, @Param("brakeTypeNames") List<String> brakeTypeNames,
            @Param("isSizeNull") boolean isSizeNull, @Param("sizeNames") List<String> sizeNames,
            @Param("isGenderNull") boolean isGenderNull, @Param("genders") List<String> genders,
            @Param("isSuspensionNull") boolean isSuspensionNull, @Param("suspensions") List<String> suspensions,
            @Param("isWheelSizeNull") boolean isWheelSizeNull, @Param("wheelSizes") List<String> wheelSizes,

            @Param("isConditionNull") boolean isConditionNull, @Param("conditions") List<String> conditions,
            @Param("name") String name,

            @Param("zipCode") String zipCode,
            @Param("cityName") String cityName,
            @Param("state") String state,
            @Param("country") String country,
            @Param("isLocationRadiusNull") boolean isLocationRadiusNull,
            @Param("latitude") Float latitude,
            @Param("longitude") Float longitude,
            @Param("radius") Float radius,

            @Param("isStartPriceNull") boolean isStartPriceNull, @Param("startPriceValue") float startPriceValue,
            @Param("isEndPriceNull") boolean isEndPriceNull, @Param("endPriceValue") float endPriceValue,
            @Param("startYearId") Long startYearId,
            @Param("endYearId") Long endYearId,

            @Param("content") String content,

            @Param("now") String now

    );

    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.auction_id AS auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.created_time AS created_time, " +
            "inventory_auction.min AS min, " +
            "inventory_auction.has_buy_it_now AS has_buy_it_now, " +
            "inventory_auction.buy_it_now AS buy_it_now, " +
            "auction.name AS auction_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.image_default AS image_default, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
            "auction.end_date AS end_date, "+
            "auction.start_date AS start_date, "+
            //start
            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_model.name as bicycle_model_name, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_brand.name AS bicycle_brand_name, " +
//            "bicycle_year.id AS year_id, " +
//            "bicycle_year.name AS bicycle_year_name," +
//            "bicycle_type.id AS type_id, " +
//            "bicycle_type.name AS bicycle_type_name, " +
//            "bicycle_size.id AS size_id, " +

//            "inventory_bicycle.model_id AS model_id, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
//            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inventory_bicycle.year_id AS year_id , " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +

            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.frame_material_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +
            "gender.gender_name AS gender, " +
            "suspension.suspension_type AS suspension, " +
            "wheel_size.wheel_size_name AS wheel_size, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.stage AS stage, " +
            "inventory.name AS inventory_name, " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.value_additional_component AS value_additional_component, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +

            "(CASE WHEN max_offer.number_bid IS NULL THEN 0 ELSE max_offer.number_bid END) AS number_bids, " +
            "max_offer.max_price AS current_highest_bid " +
            //end

            "FROM inventory_auction " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Size') " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT " +
                    "offer.inventory_auction_id AS inventory_auction_id, " +
                    "COUNT(*) AS number_bid, " +
                    "MAX(offer.offer_price) AS max_price  " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id" +
                ") AS max_offer " +
            "ON inventory_auction.id = max_offer.inventory_auction_id " +

//            "LEFT JOIN " +
//                "(SELECT " +
//                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "MAX(offer.offer_price) AS max_price  " +
//                "FROM offer " +
//                "WHERE " +
//                "offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL AND " +
//                "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                "GROUP BY offer.inventory_auction_id" +
//                ") AS max_offer_price " +
//            "ON inventory_auction.id = max_offer_price.inventory_auction_id " +

            //start
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
//            "JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            //end

            "WHERE inventory_auction.auction_id = :auctionId " +
            "AND inventory_auction.inventory_id IN :inventoryIds"
    )
    List<InventoryAuctionResponse> findAll(
            @Param("auctionId") Long auctionId,
            @Param("inventoryIds") List<Long> inventoryIds,
            @Param("now") String now
    );



    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS id, " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.auction_id AS auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.created_time AS created_time, " +
            "inventory_auction.min AS min, " +
            "inventory_auction.has_buy_it_now AS has_buy_it_now, " +
            "inventory_auction.buy_it_now AS buy_it_now, " +
            "auction.name AS auction_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.image_default AS image_default, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
            "auction.end_date AS end_date, "+
            "auction.start_date AS start_date, "+
            //start
            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_model.name as bicycle_model_name, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_brand.name AS bicycle_brand_name, " +
//            "bicycle_year.id AS year_id, " +
//            "bicycle_year.name AS bicycle_year_name," +
//            "bicycle_type.id AS type_id, " +
//            "bicycle_type.name AS bicycle_type_name, " +
//            "bicycle_size.id AS size_id, " +

//            "inventory_bicycle.model_id AS model_id, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
//            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inventory_bicycle.year_id AS year_id , " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +

            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.frame_material_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +
            "gender.gender_name AS gender, " +
            "suspension.suspension_type AS suspension, " +
            "wheel_size.wheel_size_name AS wheel_size, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.stage AS stage, " +
            "inventory.name AS inventory_name, " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.value_additional_component AS value_additional_component, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +
            "inventory.is_delete AS is_delete, " +

            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +

            "(CASE WHEN max_offer.number_bid IS NULL THEN 0 ELSE max_offer.number_bid END) AS number_bids, " +
            "max_offer.max_price AS current_highest_bid " +
            //end

            "FROM inventory_auction " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS gender_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).GENDER_NAME_INV_COMP}) " +
                "AS gender " +
            "ON inventory.id = gender.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS suspension_type " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).SUSPENSION_NAME_INV_COMP}) " +
                "AS suspension " +
            "ON inventory.id = suspension.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS wheel_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_WHEEL_SIZE_NAME}) " +
                "AS wheel_size " +
            "ON inventory.id = wheel_size.inventory_id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                //TODO spring-data bug
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "WHERE inventory_comp_type.name = 'Frame Size') " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT " +
                "offer.inventory_auction_id AS inventory_auction_id, " +
                "COUNT(*) AS number_bid, " +
                "MAX(offer.offer_price) AS max_price  " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id" +
                ") AS max_offer " +
            "ON inventory_auction.id = max_offer.inventory_auction_id " +

//            "LEFT JOIN " +
//                "(SELECT " +
//                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "MAX(offer.offer_price) AS max_price  " +
//                "FROM offer " +
//                "WHERE " +
//                "offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL AND " +
//                "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                "GROUP BY offer.inventory_auction_id" +
//                ") AS max_offer_price " +
//            "ON inventory_auction.id = max_offer_price.inventory_auction_id " +

            //start
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
//            "JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +
            //end

            "WHERE "+
            "inventory_auction.id IN :inventoryAuctionIds"
    )
    List<InventoryAuctionResponse> findAll(
            @Param("inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param("now") String now
    );


}
