package com.bbb.core.service.impl.brandyear;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.brandyear.YearModelManager;
import com.bbb.core.service.brandyear.BrandYearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandYearServiceImpl implements BrandYearService {
    @Autowired
    private YearModelManager manager;


    @Override
    public Object getModelFromBrandYear(long brandId, Long yearId, String familyName) throws ExceptionResponse {
        return manager.getModelFromBrandYear(brandId, yearId, familyName);
    }

    @Override
    public Object getAllBicycleModelFromBicycle(long brandId) throws ExceptionResponse {
        return manager.getAllBicycleModelFromBicycle(brandId);
    }
}
