package com.bbb.core.model.request.inventory;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.request.sort.Sort;

import java.util.List;

public class InventoryRequest {
    private List<Long> bicycleIds;
    private List<Long> modelIds;
    private List<Long> brandIds;
    private List<String> sizeNames;
    private Long startYearId;
    private Long endYearId;
    private List<Long> typeIds;
    private Float startPrice;
    private Float endPrice;
    private List<String> frameMaterialNames;
    private List<String> brakeTypeNames;
    private StatusInventory status;
    private StageInventory stage;
    private MarketType marketType;
    private String name;
    private List<ConditionInventory> conditions;
    private String searchContent;
    private Sort sortType;
    private InventorySortField sortField;

    public List<Long> getBicycleIds() {
        return bicycleIds;
    }

    public void setBicycleIds(List<Long> bicycleIds) {
        this.bicycleIds = bicycleIds;
    }

    public List<Long> getModelIds() {
        return modelIds;
    }

    public void setModelIds(List<Long> modelIds) {
        this.modelIds = modelIds;
    }

    public List<Long> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Long> brandIds) {
        this.brandIds = brandIds;
    }

    public Long getStartYearId() {
        return startYearId;
    }

    public void setStartYearId(Long startYearId) {
        this.startYearId = startYearId;
    }

    public Long getEndYearId() {
        return endYearId;
    }

    public void setEndYearId(Long endYearId) {
        this.endYearId = endYearId;
    }

    public List<Long> getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(List<Long> typeIds) {
        this.typeIds = typeIds;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }


    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ConditionInventory> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionInventory> conditions) {
        this.conditions = conditions;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public List<String> getSizeNames() {
        return sizeNames;
    }

    public void setSizeNames(List<String> sizeNames) {
        this.sizeNames = sizeNames;
    }

    public List<String> getFrameMaterialNames() {
        return frameMaterialNames;
    }

    public void setFrameMaterialNames(List<String> frameMaterialNames) {
        this.frameMaterialNames = frameMaterialNames;
    }

    public List<String> getBrakeTypeNames() {
        return brakeTypeNames;
    }

    public void setBrakeTypeNames(List<String> brakeTypeNames) {
        this.brakeTypeNames = brakeTypeNames;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public InventorySortField getSortField() {
        return sortField;
    }

    public void setSortField(InventorySortField sortField) {
        this.sortField = sortField;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }
}
