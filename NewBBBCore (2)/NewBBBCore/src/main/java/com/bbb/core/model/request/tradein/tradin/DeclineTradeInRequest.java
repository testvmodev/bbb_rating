package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.request.tradein.customquote.CompRequest;

import java.util.List;

public class DeclineTradeInRequest extends DeclineTradeIn {
    private List<CompRequest> compRequests;
    private List<Long> upgradeCompIds;
    private Long customQuoteId;
    private Long bicycleTypeId;

    public DeclineTradeInRequest() {}

    public DeclineTradeInRequest(Long bicycleId, long reasonDeclineId, String comment, String priceExpected, float priceSuggest, ConditionInventory condition, Long customQuoteId) {
        super(bicycleId, reasonDeclineId, comment, priceExpected, priceSuggest, condition);
        this.customQuoteId = customQuoteId;
    }

    public DeclineTradeInRequest(Long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public List<CompRequest> getCompRequests() {
        return compRequests;
    }

    public void setCompRequests(List<CompRequest> compRequests) {
        this.compRequests = compRequests;
    }

    public Long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(Long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public List<Long> getUpgradeCompIds() {
        return upgradeCompIds;
    }

    public void setUpgradeCompIds(List<Long> upgradeCompIds) {
        this.upgradeCompIds = upgradeCompIds;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }
}
