package com.bbb.core.common;

import com.bbb.core.common.utils.Pair;
import org.springframework.http.HttpMethod;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface Constants {
    String END_POINT_MATCH_API = "/api/**";
    String END_POINT_MATCH_API_SECRET_KEY = "/secret/api/**";
    String API_SECRET_KEY = "/secret/api";
    String HEADER_PREFIX = "Bearer ";
    String ENPOINT_LOGIN = "/auth/login";
    String ENPOINT_REGISTER = "/auth/register";
    String HEADER_NAME_AUTH = "Authorization";
    String HEADER_SECRET_KEY = "x-bbb-client-secret";
    String ROLE_PREFIX = "ROLE_";
    String SPACE_KEY_REDIS = "_@#_";

    String SECRET_KEY_SERVICE_LOCAL = "EiuJlvMVNaYT65V6FI72Vv8QpCXP0ODj8CKGgXVlISjnzMnUdpUfzE0xouRQfCHQd220518";

    String CONFIG_PREFIX = "bbb.core";

    String BBB_ID_GROUP = "BBB id group";
    String BICYCLE_BLUE_BOOK = "Bicycle blue book";
    String CUSTOM_QUOTE_SUB_MAIL_NAME = "Custom Quote";
    String TRADE_IN_TYPE = "Trade-in";
    String NUMERIC = "NUMERIC(36,2)";
    String NUMERIC_3 = "NUMERIC(36,3)";


    //time
    long SECOND = 1000L;
    long MINUTE = SECOND * 60;
    long HOUR = MINUTE * 60;
    long DAY = HOUR * 24;
    long WEEK = DAY * 7;

    String API = "/api";
    String BICYCLE = "/bicycle";
    String BICYCLES = "/bicycles";
    String BRAND = "/brand";
    String MODEL = "/model";
    String MODEL_FROM_BRAND_YEAR = "/modelFromBrandYear";
    String YEAR_FROM_BRAND_MODEL = "/yearFromBrandModel";
    String ID = "id";
    String ID_PATH = "/{" + ID + "}";
    String TYPE  = "/type";
    String LOGO = "/logo";
    String DELETE = "/delete";
    String CREATE = "/create";
    String INVENTORY = "/inventory";
    String INVENTORIES = "/inventories";
    String INVENTORY_CREATE = "/inventoryCreate";
    String BICYCLE_YEAR = "/bicycleYear";
    String BICYCLE_TYPE = "/bicycleType";
    String INVENTORY_TYPE = "/inventoryType";
    String INVENTORY_SIZE = "/inventorySize";
    String COMPONENT = "/component";
    String COMMON = "/common";
    String SEARCH = "/search";
    String OFFER = "/offer";
    String OFFERS = "/offers";
    String BID = "/bid";
    String BIDS = "/bids";
    String AUCTION = "/auction";
    String INVENTORY_AUCTION = "/inventoryAuction";
    String INVENTORY_AUCTIONS = "/inventoryAuctions";
    String YEAR = "/year";
    String CONTENT = "/content";
    String TRACKING_VIEW = "/trackingView";
    String RECENT = "/recent";
    String TRACKINGS = "/trackings";
    String TRADE_IN = "/tradeIn";
    String CANCEL = "/cancel";
    String TRADE_INS = "/tradeIns";
    String IMAGE_TYPE = "/imageType";
    String IMAGE = "/image";
    String SHIPPING = "/shipping";
    String POST_IMAGE = "/postImage";
    String LOAD_IMAGE = "/loadImage";
    String CONDITION = "/condition";
    String REASON_DECLINES = "/reasonDeclines";
    String DETAIL_DECLINE = "/detailDecline";
    String SUMMARY = "/summary";
    String CALCULATE_SHIPPING = "/calculateShipping";
    String SHIPPING_ACCOUNT = "/shippingAccount";
    String ACTIVATE = "/activate";
    String STATUS = "/status";
    String ARCHIVED = "/archived";
    String FAMILY = "/family";
    String FAMILIES = "/families";
    String BICYCLE_SEARCH = "/bicycleSearch";
    String BASE_COMPONENT = "/baseComponent";
    String CUSTOM_QUOTE = "/customQuote";
    String CUSTOM_QUOTES = "/customQuotes";
    String REVIEWED = "/reviewed";
    String COST_CALCULATOR = "/costCalculator";
    String MARKET_PLACE = "/marketPlace";
    String MARKET_LISTING = "/marketListing";
    String SALE = "/sale";
    String MARKET_LISTINGS = "/marketListings";
    String CONFIGURATION = "/configuration";
    String PARTNER = "/Partner";
    String PAY = "/pay";
    String EBAY = "/ebay";
    String NOTIFICATION = "/notification";
    String TO_LISTING = "/toListing";
    String CATEGORY = "/category";
    String CHILD = "/child";
    String FAVOURITE = "/favourite";
    String DEALS = "/deals";
    String RECOMMENDS = "/recommends";
    String LISTING_WIZARD = "/listingWizard";
    String LISTING_WIZARDS = "/listingWizards";
    String SHUTDOWN = "/shutdown";
    String JOB = "/job";
    String RUNNING = "/running";
    String INQUEUE = "/inqueue";
    String LOG = "/log";
    String PURGE = "/purge";
    String PTP = "/PTP";
    String DRAFT = "/draft";
    String FINISH = "/finish";
    String USER = "/user";
    String MY_LISTING = "/myListing";
    String SOLD = "/sold";
    String BUYER = "/buyer";
    String RECEIVER = "/receiver";
    String VALUE = "/value";
    String DE_LISTED = "/deListed";
    String START_ASYNC = "/startAsync";
    String REMOVE_QUEUE = "/removeQueueMarketListing";
    String INTEGRATION_TEST = "/integrationTest";
    String CART = "/cart";
    String VERIFY_SAFE = "/verify-safe";
    String SHIPPING_PROFILE = "/shipping-profile";
    String ALL_BID = "/allBid";
    String STATS = "/stats";
    String INCOMPLETE = "/incomplete";
    String RELIST = "/relist";
    String ONLINE_STORE = "/onlineStore";
    String TRENDING = "/trending";
    String MESSAGE = "/message";
    String DETAIL = "/detail";
    String SALESFORCE = "/salesforce";
    String BRANDS = "/brands";

    String TRADE_IN_FOLDER = "tradein";
    String TRADE_IN_QUOTE_FOLDER = "tradeinQuote";
    String MARKET_LISTING_PTP_FOLDER = "marketListingPTP";
    String BRAND_LOGO_FOLDER = "brand";
    String BICYCLE_FOLDER = "bicycle";

    String FORMAT_DATE = "yyyy-MM-dd";
    String FORMAT_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    String FORMAT_DATE_TIME_EBAY = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    String FORMAT_MONTH = "yyyy-MM";


    String URL_GET_BICYCLE = API + BICYCLE;
    String URL_BICYCLE = API + BICYCLE + ID_PATH;
    String URL_BICYCLE_CREATE = API + BICYCLE;

    String URL_BICYCLE_SEARCH_BASE_COMPONENT = API + BICYCLE_SEARCH + BASE_COMPONENT;

    //bicycle model
    String URL_GET_BRAND = API + BRAND;
    String URL_BRAND = API + BRAND + ID_PATH;
    String URL_BRAND_CREATE = API + BRAND;
    String URL_BRAND_LOGO = API + BRAND + ID_PATH + LOGO;
    String URL_BRANDS = API + BRANDS;


    //model
    String URL_GET_MODELS = API + MODEL;
    String URL_GET_MODELS_FROM_BRAND_YEAR = API + MODEL_FROM_BRAND_YEAR;
    String URL_GET_YEARS_FROM_BRAND_MODEL = API + YEAR_FROM_BRAND_MODEL;
    String URL_MODEL = API + MODEL + ID_PATH;
    String URL_GET_MODEL_CREATE = API + MODEL;
    String URL_GET_MODELS_FROM_BRAND = API + BRAND + ID_PATH + MODEL;

    //inventory
    String URL_GET_INVENTORY = API + INVENTORY;
    String URL_INVENTORY = API + INVENTORY + ID_PATH;
    String URL_INVENTORY_CREATE = API + INVENTORY_CREATE;
    String URL_SECRET_INVENTORIES= API_SECRET_KEY + INVENTORIES;

    //year
    String URL_GET_BICYCLE_YEAR = API + BICYCLE_YEAR;
    String URL_BICYCLE_YEAR = API + BICYCLE_YEAR + ID_PATH;
    String URL_BICYCLE_YEAR_CREATE = API + BICYCLE_YEAR;


    //type
    //year
    String URL_GET_BICYCLE_TYPE = API + BICYCLE_TYPE;
    String URL_GET_DETAIL_BICYCLE_TYPE = API + BICYCLE_TYPE + ID_PATH;
    String URL_BICYCLE_TYPE = API + BICYCLE_TYPE + ID_PATH;
    String URL_BICYCLE_TYPE_CREATE = API + BICYCLE_TYPE;

    //component category
    String URL_GET_COMPONENT_CATEGORY = API + COMPONENT + CATEGORY;
    String URL_GET_DETAIL_COMPONENT_CATEGORY =  API + COMPONENT + CATEGORY +ID_PATH;
    String URL_CREATE_COMPONENT_CATEGORY = API + COMPONENT + CATEGORY;
    String URL_UPDATE_COMPONENT_CATEGORY = API + COMPONENT + CATEGORY +ID_PATH;

    //component type
    String URL_GET_COMPONENT_TYPE = API + COMPONENT + TYPE;
    String URL_GET_DETAIL_COMPONENT_TYPE = API + COMPONENT + TYPE + ID_PATH;
    String URL_CREATE_COMPONENT_TYPE = API + COMPONENT + TYPE;
    String URL_UPDATE_COMPONENT_TYPE = API + COMPONENT + TYPE + ID_PATH;

    //component type select
    String URL_CREATE_COMPONENT_TYPE_VALUE = API + COMPONENT + TYPE + VALUE;
    String URL_GET_DETAIL_COMPONENT_TYPE_VALUE = API + COMPONENT + TYPE + ID_PATH +VALUE;
    String URL_GET_ONE_COMPONENT_VALUE = API + COMPONENT + TYPE + VALUE + ID_PATH;
    String URL_UPDATE_COMPONENT_TYPE_VALUE = API + COMPONENT + TYPE + VALUE + ID_PATH;
    String URL_DELETE_COMPONENT_TYPE_VALUE = API + COMPONENT + TYPE + VALUE + ID_PATH;

    //component inventory
    String URL_COMPONENT_INVENTORY_SEARCH = API + INVENTORY + SEARCH;

    ///inventory type
    String URL_GET_INVENTORY_TYPE = API + INVENTORY_TYPE;

    String URL_INVENTORY_SIZE = API + INVENTORY_SIZE;


    String URL_MODEL_BRAND = API + MODEL + BRAND;
    String URL_MODEL_BRAND_FROM_BICYCLE = API + MODEL + BICYCLE;

    //model family
    String URL_MODEL_FAMILIES = API + MODEL + FAMILIES;

    //offer
    String URL_GET_OFFERS = API + OFFERS;
    String URL_GET_OFFERS_RECEIVER_MY_LISTING = API + OFFERS + RECEIVER + MY_LISTING;
    String URL_GET_OFFERS_ONLINE_STORE_RECEIVED = API + ONLINE_STORE + OFFERS;
    String URL_GET_OFFERS_MARKET_LISTING = API + MARKET_LISTING + ID_PATH + OFFERS;
    String URL_OFFER_CREATE = API + OFFER;
    String URL_OFFER = API + OFFER + ID_PATH;
    String URL_OFFER_UPDATE_PAID = API_SECRET_KEY + OFFER + ID_PATH + PAY;
    String URL_OFFER_BIDS = API + OFFERS + BID;
    String URL_OFFER_BID = API + USER + INVENTORY_AUCTION + BID+ ID_PATH;
    String URL_OFFER_CART = API_SECRET_KEY + OFFER + CART + ID_PATH;
    String URL_OFFERS_OF_INVENTORY_AUCTION = API + INVENTORY_AUCTION + ID_PATH + OFFERS;
    String URL_USER_OFFERS = API + USER + ID_PATH  + OFFERS;
    String URL_USER_BIDS = API + USER + ID_PATH + BIDS;
    String URL_GET_OFFER_SECRET = API_SECRET_KEY + OFFER + ID_PATH;

    //auction
    String URL_GET_AUCTION = API + AUCTION;
    String URL_AUCTION_CREATE = API + AUCTION;
    String URL_AUCTION = API + AUCTION + ID_PATH;

    //inventory_auction
    String URL_GET_INVENTORY_AUCTION = API + INVENTORY_AUCTION;
    String URL_GET_INVENTORY_AUCTIONS = API + INVENTORY_AUCTIONS;
    String URL_INVENTORY_AUCTION_CREATE = API + INVENTORY_AUCTION;
    String URL_INVENTORY_AUCTION = API + INVENTORY_AUCTION + ID_PATH;
    String URL_INVENTORY_AUCTION_ALL_BID = API + INVENTORY_AUCTION + ID_PATH + ALL_BID;


    //year model
    String URL_GET_MODEL_FROM_BRAND_YEAR = API + BRAND + YEAR + MODEL;

    //bicycle
    String URL_GET_BICYCLE_BRAND_YEAR_MODEL = API + BICYCLES + BRAND + YEAR + MODEL;
    String URL_GET_BICYCLE_CONTENT = API + BICYCLES + CONTENT;
    String URL_GET_BICYCLE_CONDITION = API + BICYCLE + CONDITION;
    String URL_BICYCLE_RECENT_TRACKINGS = API + BICYCLE + RECENT + TRACKINGS;
    String URL_BICYCLE_RECOMMENDS = API + BICYCLE + RECOMMENDS;
    String URL_BICYCLE_YEARS = API + BICYCLE + YEAR;
    String URL_BICYCLE_VALUE = API + BICYCLE + VALUE;

    //tracking
    String URL_GET_BEST_VIEW_TRACKING = API + INVENTORY + TRACKING_VIEW;
    String URL_GET_RECENT_VIEW_TRACKING = API + INVENTORY + TRACKING_VIEW + RECENT;
    String URL_GET_COMMON_COMPONENT = API + COMMON + COMPONENT;
    String URL_GET_TRADE_IN_COST_CALCULATOR = API + TRADE_IN + TRADE_IN + COST_CALCULATOR;

    //trade in
    String URL_TRADE_IN = API + TRADE_IN;
    String URL_TRADE_IN_STATUS = API + TRADE_IN + STATUS;
    String URL_TRADE_INS = API + TRADE_INS;
    String URL_TRADE_IN_PATH = API + TRADE_IN + ID_PATH;
    String URL_TRADE_IN_ARCHIVED = API + TRADE_IN + ARCHIVED;
    String URL_TRADE_IN_UPLOAD_IMAGE = API + TRADE_IN + POST_IMAGE;
    String URL_TRADE_IN_LOAD_IMAGE = API + TRADE_IN + LOAD_IMAGE;
    String URL_GET_TRADE_IN_IMAGE_TYPE = API + TRADE_IN + IMAGE_TYPE;
    String URL_TRADE_IN_SHIPPING = API + TRADE_IN + SHIPPING;
    String URL_TRADE_IN_BICYCLE = API + TRADE_IN + BICYCLE;
    String URL_TRADE_IN_REASON_DECLINE = API + TRADE_IN + REASON_DECLINES;
    String URL_TRADE_IN_DECLINE_DETAIL = API + TRADE_IN + DETAIL_DECLINE;
    String URL_TRADE_IN_SUMMARY_PATH = API + TRADE_IN + SUMMARY + ID_PATH;
    String URL_TRADE_IN_CALCULATE_SHIPPING = API + TRADE_IN + CALCULATE_SHIPPING;
    String URL_SHIPPING_ACCOUNT = API + TRADE_IN + SHIPPING_ACCOUNT;
    String URL_SHIPPING_ACCOUNT_PATH = API + TRADE_IN + SHIPPING_ACCOUNT + ID_PATH;
    String URL_SHIPPING_ACCOUNT_ACTIVATE = API + TRADE_IN + SHIPPING_ACCOUNT + ID_PATH + ACTIVATE;
    String URL_TRADE_IN_IMAGE = API + TRADE_IN + IMAGE;
    String URL_TRADE_IN_CANCEL = API + TRADE_IN + ID_PATH + CANCEL;
    String URL_TRADE_INS_CANCEL = API + TRADE_IN + CANCEL;
    String URL_TRADE_IN_CUSTOM_QUOTE = API + TRADE_IN + CUSTOM_QUOTE;
    String URL_TRADE_IN_CUSTOM_QUOTE_PATH = API + TRADE_IN + CUSTOM_QUOTE + ID_PATH;
    String URL_TRADE_IN_CUSTOM_QUOTE_COMMON = API + TRADE_IN + CUSTOM_QUOTE + COMMON + ID_PATH;
    String URL_TRADE_IN_COMMON = API + TRADE_IN + COMMON + ID_PATH;
    String URL_TRADE_IN_CUSTOM_QUOTES = API + TRADE_IN + CUSTOM_QUOTES;
    String URL_TRADE_IN_CUSTOM_QUOTE_UPLOAD_IMAGE = API + TRADE_IN + CUSTOM_QUOTE + POST_IMAGE;
    String URL_TRADE_IN_CUSTOM_QUOTE_REVIEWED = API + TRADE_IN + CUSTOM_QUOTE + REVIEWED;
    String URL_COUNT_TRADE_INCOMPLETE = API + TRADE_IN + INCOMPLETE;
    String URL_INVENTORY_SALESFORCE = "https://uat-bicyclebluebook.cs9.force.com/services/apexrest";
    String MARKET_LISTING_BBBV2 = "/MarketListingBBBV2";

    //ebay


    Float MIN_INIT_PRICE_TRADE_IN = 75f;

    String URL_LOG_SERVICE_BASE = "http://logger.bicyclebluebook.com:12201";
    String URL_LOG = "/gelf";
    int LOG_400 = 6;
    int LOG_409 = 2;
    int LOG_500 = 0;

    //offer
    String URL_SECRET_MARKET_LISTING_INVENTORY_AUCTIONS = API_SECRET_KEY + MARKET_LISTING + INVENTORY_AUCTIONS;


    //market
    String URL_MARKET_PLACES = API + MARKET_PLACE;
    String URL_MARKET_PLACE = API + MARKET_PLACE + ID_PATH;
    String URL_MARKET_PLACE_CONFIGS = API + MARKET_PLACE + CONFIGURATION;
    String URL_MARKET_PLACE_CONFIG = API + MARKET_PLACE + CONFIGURATION + ID_PATH;

    //market listing
    String URL_MARKET_LISTING = API + MARKET_LISTING;
    String URL_MARKET_LISTING_SALE = API + MARKET_LISTING + SALE;
    String URL_SECRET_INVENTORY_SALE = API_SECRET_KEY + INVENTORY + SOLD;
    String URL_MARKET_LISTING_PATH = API + MARKET_LISTING + ID_PATH;
    String URL_MARKET_LISTING_SECRET_PATH = API_SECRET_KEY + MARKET_LISTING + ID_PATH;
    String URL_MARKET_LISTINGS = API + MARKET_LISTINGS;
    String URL_LISTING_WIZARD_INVENTORIES = API + LISTING_WIZARD + INVENTORIES;
    String URL_EBAY_NOTIFICATION = API + EBAY + NOTIFICATION;
    String URL_MARKET_LISTING_DEALS = API + MARKET_LISTING + DEALS;
    String URL_MARKET_LISTING_RECOMMENDS = API + MARKET_LISTING + RECOMMENDS;
    String URL_RECENT_TRACKINGS = API + RECENT + TRACKINGS;
    String URL_SECRET_MARKET_LISTINGS = API_SECRET_KEY + MARKET_LISTINGS;
    String URL_USER_MARKET_LISTINGS = API + USER + ID_PATH + MARKET_LISTINGS;
    String URL_MESSAGE_MARKET_LISTINGS = API + MESSAGE + MARKET_LISTINGS;

    //tracking
    String URL_DEALS = API + DEALS;

    //p2p
    String URL_PTP_MY_LISTING = API + MARKET_LISTING + MY_LISTING;
    String URL_MARKET_LISTING_PTP = API + MARKET_LISTING + PTP;
    String URL_MARKET_LISTING_PTP_PATH = API + MARKET_LISTING + PTP + ID_PATH;
    String URL_MARKET_LISTING_PTP_PATH_IMAGE = API + MARKET_LISTING + PTP + ID_PATH + IMAGE;
    String URL_MARKET_LISTING_PTP_DRAFT = API + MARKET_LISTING + PTP + DRAFT;
    String URL_MARKET_LISTING_PTP_DRAFT_POST_IMAGE_PATH = API + MARKET_LISTING + PTP + DRAFT + POST_IMAGE + ID_PATH;
    String URL_MARKET_LISTING_PTP_DRAFT_PATH = API + MARKET_LISTING + PTP + DRAFT + ID_PATH;
    String URL_MARKET_LISTING_PTP_DRAFT_FINISH = API + MARKET_LISTING + PTP + DRAFT + ID_PATH + FINISH;
    String URL_MARKET_LISTING_PTP_DRAFT_IMAGE = API + MARKET_LISTING + PTP + DRAFT + IMAGE;
    String URL_MARKET_LISTING_PTP_RELIST = API + MARKET_LISTING + PTP + ID_PATH + RELIST;
    String URL_MARKET_LISTING_PTP_SHIPMENT = API + MARKET_LISTING + PTP + ID_PATH + SHIPPING;

    //online store
    String URL_GET_ONLINE_STORE = API_SECRET_KEY + ONLINE_STORE;
    String URL_GET_ONLINE_STORE_SUMMARY = API + ONLINE_STORE + SUMMARY;
    String URL_GET_ONLINE_STORE_TRENDING = API_SECRET_KEY + ONLINE_STORE + TRENDING;
    String URL_GET_ONLINE_STORE_MY_LISTING = API + ONLINE_STORE + MY_LISTING;
    String URL_GET_ONLINE_STORE_MY_LISTING_SUMMARY = API + ONLINE_STORE + MY_LISTING + SUMMARY;
    String URL_GET_ONLINE_STORE_SALE_REPORT = API + ONLINE_STORE + SALE;
    String URL_GET_ONLINE_STORE_SALE_REPORT_DETAIL = API + ONLINE_STORE + SALE + DETAIL;

    //listing wizard
    String URL_LISTING_WIZARD = API + LISTING_WIZARDS;
    String URL_LISTING_WIZARD_DE_LISTED = API + LISTING_WIZARDS + DE_LISTED;
    String URL_LISTING_WIZARD_START_ASYNC = API + LISTING_WIZARDS + START_ASYNC;
    String URL_LISTING_WIZARD_REMOVE_QUEUE = API + LISTING_WIZARDS + REMOVE_QUEUE;

    //salesforce
    String URL_MARKET_LISTING_SALESFORCE = API_SECRET_KEY + MARKET_LISTING + SALESFORCE;
    String URL_MARKET_LISTING_SALESFORCE_PATH_ID = API_SECRET_KEY + MARKET_LISTING + SALESFORCE + ID_PATH;
    String URL_MARKET_LISTING_SALESFORCE_SOLD = API_SECRET_KEY + MARKET_LISTING + SALESFORCE + ID_PATH + SOLD;


    //favourite
    String URL_FAVOURITES = API + FAVOURITE;
    String URL_FAVOURITE_CREATE = API + FAVOURITE;
    String URL_FAVOURITE_REMOVE = API + FAVOURITE;

    String URL_UPDATE_INFO_PARTNER = API_SECRET_KEY + PARTNER + ID_PATH;


    //job queue
    String URL_SAFE_SHUTDOWN = API_SECRET_KEY + SHUTDOWN;
    String URL_JOB_CONFIGS = API + JOB + CONFIGURATION;
    String URL_JOB_RUNNING = API + JOB + RUNNING;
    String URL_JOB_INQUEUE = API + JOB + INQUEUE;
    String URL_JOB_CONFIG = API + JOB + CONFIGURATION + ID_PATH;
    String URL_UPDATE_JOB_CONFIG = API_SECRET_KEY + CONFIGURATION + ID_PATH;
    String URL_JOB_LOGS = API + JOB + LOG;
    String URL_JOB_LOGS_PURGE = API + JOB + LOG + PURGE;


    String URL_SECRET_USER_PATH = API_SECRET_KEY + USER + ID_PATH;
    String URL_SECRET_USER_VERIFY_SAFE = API_SECRET_KEY + USER + ID_PATH + VERIFY_SAFE;
    String URL_USER_BASE_STATS = API + USER + ID_PATH + STATS;


    String URL_SHIPPING_PROFILES = API + SHIPPING_PROFILE;
    String URL_SHIPPING_PROFILE = API + SHIPPING_PROFILE + ID_PATH;
    String URL_SECRET_SHIPPING_PROFILES = API_SECRET_KEY + SHIPPING_PROFILE;


    //support integration test
    String URL_SECRET_OFFER_INTEGRATION_TEST = API_SECRET_KEY + OFFER + INTEGRATION_TEST;

    //only apply to HttpMethod.GET
    List<Pair<HttpMethod, String>> API_GET_SKIP_AUTHEN = Arrays.asList(URL_GET_BICYCLE,
            URL_BICYCLE,
//            URL_BICYCLE_CRATE,
//            URL_BICYCLE_CRATE,
            URL_GET_BRAND,
            URL_BRAND,
            URL_GET_MODELS,
//            URL_GET_MODEL_CREATE,
            URL_GET_INVENTORY,
            URL_INVENTORY,
            URL_GET_BICYCLE_YEAR,
            URL_BICYCLE_YEAR,
//            URL_BICYCLE_YEAR_CRATE,
            URL_GET_BICYCLE_TYPE,
//            URL_BICYCLE_TYPE_CRATE,
            URL_COMPONENT_INVENTORY_SEARCH,
            URL_GET_INVENTORY_TYPE,
            URL_INVENTORY_SIZE,
            URL_MODEL_BRAND,
//            URL_GET_OFFERS,
//            URL_OFFER_CREATE,
            URL_OFFER,
            URL_GET_AUCTION,
//            URL_AUCTION_CREATE,
            URL_AUCTION,
            URL_GET_INVENTORY_AUCTION,
//            URL_INVENTORY_AUCTION_CREATE,
            URL_INVENTORY_AUCTION,
            URL_GET_MODEL_FROM_BRAND_YEAR,
            URL_GET_BICYCLE_BRAND_YEAR_MODEL,
            URL_GET_BICYCLE_CONTENT,
            URL_GET_BEST_VIEW_TRACKING,
            URL_GET_TRADE_IN_IMAGE_TYPE,
            URL_TRADE_IN_LOAD_IMAGE,
            URL_GET_BICYCLE_CONDITION,
            URL_TRADE_IN_BICYCLE,
            URL_TRADE_IN_STATUS,
            URL_MODEL_BRAND_FROM_BICYCLE,
            URL_MODEL_FAMILIES,
            URL_GET_MODELS_FROM_BRAND_YEAR,
            URL_GET_YEARS_FROM_BRAND_MODEL,
            URL_BICYCLE_SEARCH_BASE_COMPONENT,
            URL_GET_COMMON_COMPONENT,
            URL_GET_TRADE_IN_COST_CALCULATOR,
            URL_TRADE_IN_REASON_DECLINE,
            URL_MARKET_LISTINGS,
            URL_MARKET_LISTING_DEALS,
            URL_MARKET_LISTING_PATH,
            URL_MARKET_LISTING_RECOMMENDS,
            URL_BICYCLE_RECOMMENDS,
            URL_GET_MODELS_FROM_BRAND
    ).stream().map(s -> new Pair<>(HttpMethod.GET, s)).collect(Collectors.toList());

    List<Pair<HttpMethod, String>> API_POST_SKIP_AUTHEN =
            Arrays.asList(
                    URL_RECENT_TRACKINGS,
                    URL_EBAY_NOTIFICATION,
                    URL_BICYCLE_RECENT_TRACKINGS
            ).stream().map(s -> new Pair<>(HttpMethod.POST, s)).collect(Collectors.toList());

    //fix AntPathRequestMatcher misunderstand some similar endpoints, eg: .../abc/{id} and .../abc/def
    List<Pair<HttpMethod, String>> API_GET_MUST_AUTHEN =
            Arrays.asList(
                    URL_PTP_MY_LISTING
            ).stream().map(u -> new Pair<>(HttpMethod.GET, u))
            .collect(Collectors.toList());


    int PAGE_SIZE_DEFAULT = 20;


    String DRIVER_SQL = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    String PATH_DB_OLD_BICYCLE = "jdbc:sqlserver://104.40.52.31:1433;databaseName=BBB_Prod";
    String USERNAME_OLD = "publishadmin";
    String PASSWORD_OLD = "Snql2c1!";

    int LIMIT_SAVE_TRACKING_VIEW_USER = 14;
    String MD5_SALT = "MD5_SALT_HASH_KEY";
    String ORIGIN_FOLDER_S3 = "original";
    String SHIPPING_FOLDER_S3 = "shipping";
    String LARGE_FOLDER_S3 = "large";
    String SMALL_FOLDER_S3 = "small";


    //price config
    String PRIVATE_PARTY_NAME = "BicycleBlueBook.com Private Party Value";
    long PRIVATE_PARTY_ID = -100;
    String SELLING_FEES_EBAY_NAME = "Selling fees (eBay)";
    String SHIPPING_COST_NAME = "Shipping cost";
    long SELLING_FEES_EBAY_ID = -99;
    float SELLING_PERCENT = 0.1f;
    float SELLING_PRICE_MAX = 250f;

    String TRANSACTION_FEES_NAME = "Transaction fees (PayPal)";
    float TRANSACTION_FEES_PERCENT = 0.029f;
    float TRANSACTION_FEES_PLUS = 0.3f;
    long TRANSACTION_FEES_ID = -98;

    String TOTAL_COST_TO_SELLL_PRIVATELY_NAME = "TOTAL COST TO SELL PRIVATELY";
    long TOTAL_COST_TO_SELLL_PRIVATELY_ID = -97;

    String NET_AMOUNT_TO_SELLER_NAME = "NET AMOUNT TO SELLER";
    long NET_AMOUNT_TO_SELLER_ID = -96;

    long COST_SHIPPING_ID = 5;

    long INVENTORY_TRADE_IN = 3; //note: update this if change inventory type trade-in in database
    float PRIVATE_PRICE_DEFAULT = 100;

    float EXCELLENT_PERCENT = 0.03f;
    String EXCELLENT_MSG = "Looks new and is in excellent mechanical condition";
    float VERY_GOOD_PERCENT = 0.23f;
    String VERY_GOOD_MSG = "Has minor cosmetic defects and is in excellent mechanical condition";
    float GOOD_PERCENT = 0.54f;
    String GOOD_MSG = "Has some repairable cosmetic defects and is free is major mechanical problems";
    float FAIR_PERCENT = 0.18f;
    String FAIR_MSG = "Has some cosmetic defects that require repairing and/or replacing";
    String ADMIN = "admin";

    String TEXT_XML_TYPE = "text/xml;charset=utf-8";

    long VALUE_GUIDE_PRICE_BASE_DEFAULT = 1000;

    int MAX_TRY_SEND_MAIL = 10;


    int MAX_TRADE_IN_IMAGE = 12;



    //default package info for outbound shipping
    int DEFAULT_OUTBOUND_PACKAGE_LENGTH = 54;
    int DEFAULT_OUTBOUND_PACKAGE_WIDTH = 8;
    int DEFAULT_OUTBOUND_PACKAGE_HEIGHT = 30;
    float DEFAULT_OUTBOUND_PACKAGE_WEIGHT = 35;


    String DEFAULT_PARTNER_BICYCLE_COUNTRY_CODE = "US";

    //test
    long TEST_LISTING_OFFER_ID = 1092;
    long TEST_AUCTION_OFFER_ID = 1091;
    String RATING = "/rating";
    String URL_RATING_CREATE = API + RATING;
    String URL_RATING = API + RATING;
    long MAX_RATE = 5;
    long MIN_RATE = 1;
    String URL_RATING_PATH = API + RATING + USER + ID_PATH;
}
