package com.bbb.core.common.config;

import com.bbb.core.common.Constants;
import com.bbb.core.model.database.Bicycle;
import com.bbb.core.security.WebSecurityConfig;
import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Configuration
public class ConfigDataSource {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigDataSource.class);
    private static String secretKey;
//    @Value("${server.path.secretkey}")
//    private String pathSecretKey;
    private DataSource dataSource;
    private AppConfig appConfig;
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    ConfigDataSource(DataSource dataSource, AppConfig appConfig, WebSecurityConfig webSecurityConfig) {
        this.dataSource = dataSource;
        this.appConfig = appConfig;
        this.webSecurityConfig = webSecurityConfig;
    }

    @PostConstruct
    private void init(){
//        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = (LocalContainerEntityManagerFactoryBean)entityManagerFactory;
//        localContainerEntityManagerFactoryBean.setBeanClassLoader(new ClassLoader(localContainerEntityManagerFactoryBean.getBeanClassLoader()) {
//            private volatile Class<?> cached = null;
//
//            @Override
//            public Class<?> loadClass(String name) throws ClassNotFoundException {
//                if (javax.persistence.TypedQuery.class.getName().equals(name)) {
//                    if (cached == null)
//                        cached = super.loadClass(name);
//                    return cached;
//                }
//                return super.loadClass(name);
//            }
//        });
        if (appConfig.getEndpoint() == null
                || appConfig.getEndpoint().getBaseCoreApiUrl() == null
                || appConfig.getEndpoint().getBaseCoreApiUrl().contains("localhost")
        ) {
            secretKey = Constants.SECRET_KEY_SERVICE_LOCAL;
        } else {
            readSecret();
        }
    }

//    @Bean(name = "dataSource")
//    @Primary
//    public DataSource dataSource() {
//
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClass(environment.getProperty("spring.datasource.driver-class-name"));
//        dataSource.setJdbcUrl(environment.getProperty("spring.datasource.url"));
//        dataSource.setUser(environment.getProperty("spring.datasource.username"));
//        dataSource.setPassword(environment.getProperty("spring.datasource.password"));
//
//        if (environment.getProperty("base.url").contains("localhost")) {
//            secretKey = Constants.SECRET_KEY_SERVICE_LOCAL;
//        } else {
//            readSecret();
//        }
//        return dataSource;
//    }

    public static String getSecretKey() {
        return secretKey;
    }


    private void readSecret() {
        try {
            FileInputStream in = new FileInputStream(webSecurityConfig.getFileSecretKey());
            StringBuilder secret = new StringBuilder();
            byte[] b = new byte[1024];
            int le = in.read(b);
            while (le > -1) {
                secret.append(new String(b, 0, le));
                le = in.read(b);
            }
            in.close();
            secretKey = secret.toString().trim();
            LOG.info("readSecret secretKey" + secretKey);

        } catch (IOException e) {
            e.printStackTrace();
            secretKey = Constants.SECRET_KEY_SERVICE_LOCAL;
            LOG.info("readSecret exception: " + e.getMessage());
        }
        if (secretKey == null) {
            LOG.info("readSecret null secretKey");
        }
    }

//    @Bean(name = "entityManagerFactory")
//    @Primary
//    public EntityManagerFactory entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
//        emf.setDataSource(dataSource);
//        emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
//        emf.setPackagesToScan("com.bbb.core.model");
//        emf.setPersistenceUnitName("default");
//
//        Properties properties = new Properties();
//        properties.setProperty("hibernate.connection.driver_class", environment.getProperty("spring.datasource.driver-class-name"));
//        properties.setProperty("datasource.url", environment.getProperty("spring.datasource.url"));
//        properties.setProperty("datasource.username", environment.getProperty("spring.datasource.username"));
//        properties.setProperty("datasource.password", environment.getProperty("spring.datasource.password"));
//        properties.setProperty("hibernate.show_sql", "true");
//        properties.setProperty("hibernate.dialect", environment.getProperty("spring.jpa.hibernate.dialect"));
//        if (environment.getProperty("spring.jpa.hibernate.naming.implicit-strategy") != null) {
//            properties.setProperty("hibernate.implicit_naming_strategy", environment.getProperty("spring.jpa.hibernate.naming.implicit-strategy"));
//        }
//        if (environment.getProperty("spring.jpa.hibernate.naming.physical-strategy") != null) {
//            properties.setProperty("hibernate.physical_naming_strategy", environment.getProperty("spring.jpa.hibernate.naming.physical-strategy"));
//        }
//
//        emf.setJpaProperties(properties);
//
//        emf.afterPropertiesSet();
//        return emf.getObject();
//    }

//    @Bean(name = "sessionFactory")
//    @Primary
//    public SessionFactory sessionFactory(EntityManagerFactory entityManagerFactory) {
//        if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
//            throw new NullPointerException("factory is not a hibernate factory");
//        }
//        return entityManagerFactory.unwrap(SessionFactory.class);
//    }


//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
//        LocalContainerEntityManagerFactoryBean entityManagerFactory =
//                new LocalContainerEntityManagerFactoryBean();
//
//        entityManagerFactory.setDataSource(dataSource);
//
////        // Classpath scanning of @Component, @Service, etc annotated class
////        entityManagerFactory.setPackagesToScan(
////                "com.bbb.core.model");
////
////        // Vendor adapter
////        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
////        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
////
////        // Hibernate properties
////        Properties additionalProperties = new Properties();
////        additionalProperties.put(
////                "hibernate.dialect",
////                environment.getProperty("spring.jpa.hibernate.dialect"));
////        additionalProperties.put(
////                "hibernate.show_sql",
////                environment.getProperty("spring.jpa.show-sql"));
////        additionalProperties.put(
////                "hibernate.hbm2ddl.auto",
////                environment.getProperty("hibernate.hbm2ddl.auto"));
////        entityManagerFactory.setJpaProperties(additionalProperties);
//
//
//
//        entityManagerFactory.setEntityManagerInterface(javax.persistence.EntityManager.class);
//        return entityManagerFactory;
//    }

//    @Bean
//    @Primary
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        vendorAdapter.setDatabase(Database.SQL_SERVER);
//        vendorAdapter.setGenerateDdl(true);
//        vendorAdapter.setShowSql(true);
//
//        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
//
//        factory.setJpaVendorAdapter(vendorAdapter);
//
//        factory.setPackagesToScan("com.bbb.core");
//
//        factory.setDataSource(dataSource());
//
//        factory.afterPropertiesSet();
//
//        return factory;
//    }
//
//    @Bean
//    @Primary
//    public PlatformTransactionManager transactionManager() {
//        JpaTransactionManager txManager = new JpaTransactionManager();
//        txManager.setEntityManagerFactory(entityManagerFactory().getNativeEntityManagerFactory());
//        return txManager;
//    }

    /**
     * Declare the transaction manager.
     */
//    @Bean
//    public JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
//        JpaTransactionManager transactionManager =
//                new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(
//                entityManagerFactory.getObject());
//        return transactionManager;
//    }

    /**
     * PersistenceExceptionTranslationPostProcessor is a bean post processor
     * which adds an advisor to any bean annotated with Repository so that any
     * platform-specific exceptions are caught and then rethrown as one
     * Spring's unchecked data access exceptions (i.e. a subclass of
     * DataAccessException).
     */
//    @Bean
//    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
//        return new PersistenceExceptionTranslationPostProcessor();
//    }

//    @Bean(name = "entityManagerFactory")
//    @Primary
//    public SessionFactory sessionFactory() {
//        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
//        sessionFactoryBean.setDataSource(dataSource);
//        sessionFactoryBean.setPackagesToScan("com.bbb.core");
//
//        Properties properties = new Properties();
//        properties.setProperty("hibernate.connection.driver_class", environment.getProperty("spring.datasource.driver-class-name"));
//        properties.setProperty("datasource.url", environment.getProperty("spring.datasource.url"));
//        properties.setProperty("datasource.username", environment.getProperty("spring.datasource.username"));
//        properties.setProperty("datasource.password", environment.getProperty("spring.datasource.password"));
//        properties.setProperty("hibernate.show_sql", "true");
//        properties.setProperty("hibernate.dialect", environment.getProperty("spring.jpa.hibernate.dialect"));
//        if (environment.getProperty("spring.jpa.hibernate.naming.implicit-strategy") != null) {
//            properties.setProperty("hibernate.implicit_naming_strategy", environment.getProperty("spring.jpa.hibernate.naming.implicit-strategy"));
//        }
//        if (environment.getProperty("spring.jpa.hibernate.naming.physical-strategy") != null) {
//            properties.setProperty("hibernate.physical_naming_strategy", environment.getProperty("spring.jpa.hibernate.naming.physical-strategy"));
//        }

//        ServiceRegistry serviceRegistry
//                = new StandardServiceRegistryBuilder()
//                .applySettings(properties).build();

        // builds a session factory from the service registry
//        org.hibernate.cfg.Configuration config = new org.hibernate.cfg.Configuration()
//                .addAnnotatedClass(Bicycle.class);
//        return config.buildSessionFactory(serviceRegistry);

//        sessionFactoryBean.setHibernateProperties(properties);
//
//        try {
//            sessionFactoryBean.afterPropertiesSet();
//        } catch (IOException e) {
//            return null;
//        }
//
//        return sessionFactoryBean.getObject();
//    }

//    @Bean
//    public MultipartConfigElement multipartConfigElement() {
//
//        MultipartConfigFactory factory = new MultipartConfigFactory();
//
//        factory.setMaxFileSize("200MB");
//
//        factory.setMaxRequestSize("200MB");
//
//        return factory.createMultipartConfig();
//
//    }

    public static SessionFactory getSessionFactory(EntityManagerFactory entityManagerFactory) {
        if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        return entityManagerFactory.unwrap(SessionFactory.class);
    }
}
