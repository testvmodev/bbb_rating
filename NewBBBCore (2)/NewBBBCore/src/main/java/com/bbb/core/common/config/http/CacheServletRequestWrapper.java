package com.bbb.core.common.config.http;

import com.bbb.core.common.Constants;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.MediaType;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CacheServletRequestWrapper extends HttpServletRequestWrapper {
    private CacheServletInputStream cacheIn = null;

    public CacheServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }


    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (cacheIn != null) {
            return cacheIn;
        }
        ServletInputStream in = super.getInputStream();
        String contentType = getContentType();
        if (MediaType.APPLICATION_JSON_VALUE.equals(contentType) || Constants.TEXT_XML_TYPE.equals(contentType)) {
            ByteArrayOutputStream ar = new ByteArrayOutputStream();
            IOUtils.copy(in, ar);
            byte[] b = ar.toByteArray();
            String content = new String(b, 0, b.length);
            if (  MediaType.APPLICATION_JSON_VALUE.equals(contentType)){
                try {
                    content = new JSONObject(content).toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            ByteArrayInputStream intem = new ByteArrayInputStream(ar.toByteArray());
            cacheIn = new CacheServletInputStream(intem, b.length, content);

            System.out.println("content: " + content);
            return cacheIn;
        }
        return in;
    }
}
