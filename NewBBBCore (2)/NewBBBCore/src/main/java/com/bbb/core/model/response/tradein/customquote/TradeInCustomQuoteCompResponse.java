package com.bbb.core.model.response.tradein.customquote;

public class TradeInCustomQuoteCompResponse {
    private long componentId;
    private String name;
    private String value;

    public long getComponentId() {
        return componentId;
    }

    public void setComponentId(long componentId) {
        this.componentId = componentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
