package com.bbb.core.model.response.bid.auction;

import com.bbb.core.model.database.Auction;

public class AuctionResponse extends Auction {
    private long numberInventoryAuction;
    private StatusAuctionResponse status;
    private Long remainTimeAuction;

    public long getNumberInventoryAuction() {
        return numberInventoryAuction;
    }

    public void setNumberInventoryAuction(long numberInventoryAuction) {
        this.numberInventoryAuction = numberInventoryAuction;
    }

    public StatusAuctionResponse getStatus() {
        return status;
    }

    public void setStatus(StatusAuctionResponse status) {
        this.status = status;
    }

    public Long getRemainTimeAuction() {
        return remainTimeAuction;
    }

    public void setRemainTimeAuction(Long remainTimeAuction) {
        this.remainTimeAuction = remainTimeAuction;
    }
}
