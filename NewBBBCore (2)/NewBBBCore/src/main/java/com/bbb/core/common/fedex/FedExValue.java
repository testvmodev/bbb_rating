package com.bbb.core.common.fedex;

public interface FedExValue {
    String USERNAME = "bicyclebluebook";
    String PASSWORD = "EuG-QHy-ybq-3kp";

    String TEST_KEY = "clzMHLoDpJWjwx7K";
    String TEST_PASSWORD = "qUwr1HWvjHWz02IplxxeqRjsC";
    String TEST_ACCOUNT_NUMBER = "510087780";
    String TEST_METER_NUMBER = "119053853";

    String SERVICE_ID_SHIP = "ship";
    String SERVICE_ID_RATE = "crs";
    String SERVICE_ID_TRACK = "trck";
    int SHIP_VERSION_MAJOR = 21;
    int RATE_VERSION_MAJOR = 20;
    int TRACK_VERSION_MAJOR = 14;

    String BBB_NAME_DEFAULT = "Bicycle blue book";
    String COUNTRY_CODE_DEFAULT = "US";
    int PACKAGE_PER_SHIP_DEFAULT = 1;
    String DROP_OFF_TYPE_DEFAULT = "REGULAR_PICKUP";
    String SERVICE_TYPE_DEFAULT = FedExServices.GROUND.getValue();
    String PACKAGING_TYPE_DEFAULT = "YOUR_PACKAGING";
    String PAYMENT_TYPE_DEFAULT = "SENDER";
    String LABEL_FORMAT_TYPE_DEFAULT = "COMMON2D";
    String LABEL_IMAGE_TYPE_DEFAULT = "PNG";
    String LABEL_STOCK_TYPE_DEFAULT = "PAPER_LETTER";
    String TRACK_IDENTIFIER_TYPE_DEFAULT = "TRACKING_NUMBER_OR_DOORTAG";
    String TRACK_DELIVERED_STATUS_CODE = "DL";
    String TRACK_OPTION_FULL_EVENTS = "INCLUDE_DETAILED_SCANS";
    String WEIGHT_UNIT = "LB";
    String LENGTH_UNIT = "IN";
    String DELETION_CONTROL_DEFAULT = "DELETE_ALL_PACKAGES";

    String SHIP = "/xml/ship";
    String RATE = "/xml/rate";
    String TRACK = "/xml/track";

    String BASE_TEST = "https://wsbeta.fedex.com";

    String BASE_ENDPOINT = "https://ws.fedex.com";

    String SHIP_ROOT_NAMESPACE = "http://fedex.com/ws/ship/v21";
    String RATE_ROOT_NAMESPACE = "http://fedex.com/ws/rate/v20";
    String TRACK_ROOT_NAMESPACE = "http://fedex.com/ws/track/v14";
}
