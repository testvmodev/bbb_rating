package com.bbb.core.model.response.tradein.fedex.detail;

import com.bbb.core.model.response.tradein.fedex.detail.type.Severity;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Notification implements Serializable {
    @JacksonXmlProperty(localName = "Severity")
    private Severity severity;
    @JacksonXmlProperty(localName = "Source")
    private String source;
    @JacksonXmlProperty(localName = "Code")
    private int code;
    @JacksonXmlProperty(localName = "LocalizedMessage")
    private String localizedMessage;
    @JacksonXmlProperty(localName = "Message")
    private String message;


    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(String localizedMessage) {
        this.localizedMessage = localizedMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
