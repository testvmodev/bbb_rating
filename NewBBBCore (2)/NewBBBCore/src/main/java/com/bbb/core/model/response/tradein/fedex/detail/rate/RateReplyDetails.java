package com.bbb.core.model.response.tradein.fedex.detail.rate;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RateReplyDetails {
    @JacksonXmlProperty(localName = "ServiceType")
    private String serviceType;
    @JacksonXmlProperty(localName = "PackagingType")
    private String packagingType;
    @JacksonXmlProperty(localName = "SignatureOption")
    private String signatureOption;
    @JacksonXmlProperty(localName = "RatedShipmentDetails")
    private RatedShipmentDetails ratedShipmentDetails;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getSignatureOption() {
        return signatureOption;
    }

    public void setSignatureOption(String signatureOption) {
        this.signatureOption = signatureOption;
    }

    public RatedShipmentDetails getRatedShipmentDetails() {
        return ratedShipmentDetails;
    }

    public void setRatedShipmentDetails(RatedShipmentDetails ratedShipmentDetails) {
        this.ratedShipmentDetails = ratedShipmentDetails;
    }
}
