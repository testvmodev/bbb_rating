package com.bbb.core.model.otherservice.request.event;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventMarketListingUpdateRequest {
    @JsonProperty("market_listing_id")
    private long marketListingId;

    public EventMarketListingUpdateRequest() {}

    public EventMarketListingUpdateRequest(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }
}
