package com.bbb.core.repository.onlinestore.dashboard;

import com.bbb.core.model.response.onlinestore.OnlineStoreSummaryStatsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OnlineStoreSummaryStatsResponseRepository extends JpaRepository<OnlineStoreSummaryStatsResponse, String> {
    @Query(nativeQuery = true, value = "SELECT " +
            ":storefrontId AS storefront_id, " +
            "listing_summary.*, " +
            "offer_summary.* " +
            "FROM (" +
            "SELECT " +
            "COUNT(my_listing.status) AS all_listing, " +
            "ISNULL(" +
            "SUM(CASE WHEN (my_listing.status IS NOT NULL AND my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()}) " +
            "THEN 1 ELSE 0 END), " +
            "0) AS for_sale, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS all_sold, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +
            "AND my_listing.is_local_pickup = 0 " +
            "AND my_listing.inventory_stage = :#{T(com.bbb.core.model.database.type.StageInventory).SOLD.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS awaiting_shipment, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +
            "AND my_listing.is_local_pickup = 0 " +
            "AND my_listing.inventory_stage != :#{T(com.bbb.core.model.database.type.StageInventory).SOLD.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS shipped, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).EXPIRED.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS expired, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS draft, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SALE_PENDING.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS sale_pending " +

            "FROM " +

            "(SELECT " +
            "market_listing.status, " +
            "inventory.stage AS inventory_stage, " +
            "inventory_sale.is_local_pickup " +

            "FROM market_listing " +

            "JOIN inventory " +
            "ON market_listing.inventory_id = inventory.id " +
            "AND inventory.storefront_id = :#{#storefrontId} " +
            "AND inventory.is_delete = 0 " +

            "JOIN inventory_type " +
            "ON inventory_type.id = inventory.type_id " +
            "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +

            "OUTER APPLY (" +
            "SELECT TOP 1 * " +
            "FROM inventory_sale " +
            "WHERE inventory_sale.inventory_id = inventory.id " +
            "ORDER BY inventory_sale.created_time DESC " +
            ") AS inventory_sale " +

            "WHERE market_listing.is_delete = 0 " +


            "UNION ALL " +


            "SELECT " +
            ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} AS status, " +
            "NULL AS inventory_stage, " +
            "NULL AS is_local_pickup " +

            "FROM my_listing_draft " +
            "WHERE my_listing_draft.storefront_id = :storefrontId " +
            "AND my_listing_draft.is_delete = 0 " +

            ") AS my_listing " +

            ") AS listing_summary " +


            "JOIN (" +
            "SELECT COUNT(offer.id) AS open_offers " +
            "FROM market_listing " +

            "JOIN inventory " +
            "ON market_listing.inventory_id = inventory.id " +
            "AND inventory.storefront_id = :#{#storefrontId} " +
            "AND inventory.is_delete = 0 " +

            "JOIN inventory_type " +
            "ON inventory_type.id = inventory.type_id " +
            "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +

            "LEFT JOIN offer ON market_listing.id = offer.market_listing_id " +

            "WHERE market_listing.is_delete = 0 " +
            "AND offer.is_delete = 0 " +
            "AND offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).PENDING.getValue()} " +
            ") AS offer_summary ON 1 = 1 "
    )
    OnlineStoreSummaryStatsResponse getSummary(@Param("storefrontId") String storefrontId);
}
