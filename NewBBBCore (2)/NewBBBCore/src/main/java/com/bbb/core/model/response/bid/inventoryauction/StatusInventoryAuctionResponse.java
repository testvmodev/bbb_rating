package com.bbb.core.model.response.bid.inventoryauction;

public enum  StatusInventoryAuctionResponse {
    PENDING, LISTED, CLOSE, SALE_PENDING, SOLD, CLOSED_CART_EXPIRED
}
