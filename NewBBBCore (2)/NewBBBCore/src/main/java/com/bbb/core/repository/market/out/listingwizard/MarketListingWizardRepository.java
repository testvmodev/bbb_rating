package com.bbb.core.repository.market.out.listingwizard;

import com.bbb.core.model.response.market.listingwizard.MarketListingWizard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingWizardRepository extends JpaRepository<MarketListingWizard, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "market_place.type AS type, " +
            "market_listing.status AS status, " +
            "market_listing.market_place_config_id AS market_place_config, " +
            "market_listing.market_place_id AS market_place_id " +
            "FROM market_listing " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "WHERE " +
            "market_listing.inventory_id IN :inventoriesId AND " +
            "market_listing.is_delete = 0  "
    )
    List<MarketListingWizard> findAll(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );
}
