package com.bbb.core.controller.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.request.ebay.notification.EbayNotificationRequest;
import com.bbb.core.model.request.filter.ListingSellerType;
import com.bbb.core.model.request.market.MarketListingRequest;
import com.bbb.core.model.request.market.UserMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.Test;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.security.authen.usercontext.UserContextManager;
import com.bbb.core.service.market.MarketListingService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
public class MarketListingController {
    private static final Logger LOG = LoggerFactory.getLogger(MarketListingController.class);
    @Autowired
    private MarketListingService service;
    @Autowired
    private XmlMapper xmlMapper;
    @Autowired
    private UserContextManager userContextManager;

    @GetMapping(value = Constants.URL_MARKET_LISTINGS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getMarketListings(
            @RequestParam(value = "bicycleIds", required = false) List<Long> bicycleIds,
            @RequestParam(value = "modelIds", required = false) List<Long> modelIds,
            @RequestParam(value = "brandIds", required = false) List<Long> brandIds,
            @RequestParam(value = "typeIds", required = false) List<Long> typeIds,
            @RequestParam(value = "sizeNames", required = false) List<String> sizeNames,
            @RequestParam(value = "typeBicycleNames", required = false) List<String> typeBicycleNames,
            @RequestParam(value = "frameMaterialNames", required = false) List<String> frameMaterialNames,
            @RequestParam(value = "brakeTypeNames", required = false) List<String> brakeTypeNames,
            @RequestParam(value = "conditions", required = false) List<ConditionInventory> conditions,
            @RequestParam(value = "startYearId", required = false) Long startYearId,
            @RequestParam(value = "endYearId", required = false) Long endYearId,
            @RequestParam(value = "startPrice", required = false) Float startPrice,
            @RequestParam(value = "endPrice", required = false) Float endPrice,
            @RequestParam(value = "searchContent", required = false) String searchContent,
            @RequestParam(value = "statusMarketListing", required = false) StatusMarketListing statusMarketListing,
            @RequestParam(value = "marketType", required = false) MarketType marketType,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sortType", defaultValue = "ASC") Sort sortType,
            @RequestParam(value = "sortField", defaultValue = "TIME_START_LISTING_NEWEST") InventorySortField sortField,
            @RequestParam(value = "genders", required = false) List<String> genders,
            @RequestParam(value = "suspensions", required = false) List<String> suspensions,
            @RequestParam(value = "wheelSizes", required = false) List<String> wheelSizes,
            @RequestParam(value = "sellerType", required = false, defaultValue = "ALL") ListingSellerType sellerType,
            @RequestParam(value = "zipCode", required = false) String zipCode,
            @RequestParam(value = "cityName", required = false) String cityName,
            @ApiParam(value = "Can be either stateCode or sateName")
            @RequestParam(value = "state", required = false) String state,
            @ApiParam(value = "Can be either country name or country code")
            @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "latitude", required = false) Float latitude,
            @RequestParam(value = "longitude", required = false) Float longitude,
            @ApiParam(value = "radius (meter)")
            @RequestParam(value = "radius", required = false) Float radius,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "storefrontId", required = false) String storefrontId,
            @RequestParam(value = "marketListingIds", required = false) List<Long> marketListingIds,
            Pageable page,
            HttpServletRequest httpServletRequest
    ) throws ExceptionResponse {
        MarketListingRequest request = new MarketListingRequest();
        request.setBicycleIds(bicycleIds);
        request.setModelIds(modelIds);
        request.setBrandIds(brandIds);
        request.setSizeNames(sizeNames);
        request.setTypeIds(typeIds);
        request.setTypeBicycleNames(typeBicycleNames);
        request.setFrameMaterialNames(frameMaterialNames);
        request.setBrakeTypeNames(brakeTypeNames);
        request.setConditions(conditions);
        request.setStartYearId(startYearId);
        request.setEndYearId(endYearId);
        request.setStartPrice(startPrice);
        request.setEndPrice(endPrice);
        request.setSearchContent(searchContent);
        request.setStatusMarketListing(statusMarketListing);
        request.setMarketType(marketType);
        request.setName(name);
        request.setSortType(sortType);
        request.setSortField(sortField);
        request.setGenders(genders);
        request.setSuspensions(suspensions);
        request.setWheelSizes(wheelSizes);
        request.setSellerType(sellerType);
        request.setZipCode(zipCode);
        request.setCityName(cityName);
        request.setState(state);
        request.setCountry(country);
        request.setLatitude(latitude);
        request.setLongitude(longitude);
        request.setRadius(radius);
        request.setContent(content);
        request.setStorefrontId(storefrontId);
        request.setMarketListingIds(marketListingIds);

        return new ResponseEntity<>(service.getMarketListings(request, page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_MARKET_LISTING_PATH)
    public ResponseEntity getMarketListingDetail(
            @PathVariable(value = Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getMarketListingDetail(marketListingId, true), HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_MARKET_LISTING_SECRET_PATH)
    public ResponseEntity getMarketListingDetailSecret(
            @PathVariable(value = Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getSecretMarketListing(marketListingId), HttpStatus.OK);
    }


    @PostMapping(value = Constants.URL_EBAY_NOTIFICATION)
    public String notificationEbay(
            HttpServletRequest request
    ) throws Exception {
        LOG.info("notificationEbay.....................................................");
        String contentNotification = CommonUtils.readString(request.getInputStream());
        service.notificationEbay(contentNotification);
//        System.out.println("content: " + contentNotification);

        return "Success";
    }


//    @PostMapping(value = Constants.URL_MARKET_LISTING_SALE)
//    public ResponseEntity soldInventory(
//            @RequestParam(value = "marketListingId") long marketListingId
//    ) throws ExceptionResponse {
//        return new ResponseEntity<>(service.soldInventory(marketListingId), HttpStatus.OK);
//    }


//    @PostMapping(value = Constants.URL_MARKET_LISTING_SALE)
//    public ResponseEntity soldMarketListing(
//            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
//            @RequestParam(value = "offerId", required = false) Long offerId,
//            @RequestParam(value = "buyerId") String buyerId,
//            @RequestParam(value = "buyerDisplayName") String buyerDisplayName,
//            @RequestParam(value = "buyerEmail") String buyerEmail,
//            @RequestParam(value = "buyerRole") UserRoleType buyerRole
//    ) throws ExceptionResponse {
//        MarketListingSaleRequest request = new MarketListingSaleRequest();
//        request.setMarketListingId(marketListingId);
//        request.setOfferId(offerId);
//        request.setBuyerId(buyerId);
//        request.setBuyerDisplayName(buyerDisplayName);
//        request.setBuyerEmail(buyerEmail);
//        request.setBuyerRole(buyerRole);
//        return new ResponseEntity<>(service.soldInventory(request), HttpStatus.OK);
//    }


    @PostMapping(value = "/testNotification", consumes = MediaType.APPLICATION_XML_VALUE)
    public String testNotification(
            ServletRequest request
    ) {
        String content = null;
        try {
            content = CommonUtils.readString(request.getInputStream());
            content = content.replaceAll("\n", "");
            EbayNotificationRequest ebayNotificationRequest = xmlMapper.readValue(content, EbayNotificationRequest.class);
            System.out.println("log");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "success";
    }


    @GetMapping(value = Constants.URL_MARKET_LISTING_RECOMMENDS)
    public ResponseEntity getRecommends(
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getRecommends(), HttpStatus.OK);
    }

    @GetMapping(Constants.URL_SECRET_MARKET_LISTINGS)
    public ResponseEntity getSecretMarketListingDetails(
            @RequestParam(value = "marketListingIds") List<Long> marketListingIds
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getSecretMarketListings(marketListingIds));
    }

    @GetMapping(Constants.URL_USER_MARKET_LISTINGS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getUserMarketListings(
            @ApiParam(value = "user id")
            @PathVariable(Constants.ID) String userId,
            @RequestParam(value = "marketListingStatuses", required = false) List<StatusMarketListing> marketListingStatuses,
            @RequestParam(value = "sortField", defaultValue = "POSTING_TIME") MyListingSortField sortField,
            @RequestParam(value = "sortType", defaultValue = "DESC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        UserMarketListingRequest request = new UserMarketListingRequest();
        request.setUserId(userId);
        request.setMarketListingStatuses(marketListingStatuses);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);

        return ResponseEntity.ok(service.getUserMarketListings(request));
    }

    @GetMapping(Constants.URL_MESSAGE_MARKET_LISTINGS)
    public ResponseEntity getMarketListingsForMessage(
            @RequestParam(value = "marketListingIds") List<Long> marketListingIds
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getMarketListingsForMessage(marketListingIds));
    }
}
