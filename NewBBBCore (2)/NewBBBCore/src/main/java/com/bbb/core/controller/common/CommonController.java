package com.bbb.core.controller.common;

import com.bbb.core.common.Constants;
import com.bbb.core.model.request.common.CommonComponentType;
import com.bbb.core.service.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommonController {
    @Autowired
    private CommonService service;

    @GetMapping(value = Constants.URL_GET_COMMON_COMPONENT)
    public ResponseEntity getCommonComponent(
            @RequestParam(value = "components") List<CommonComponentType> components
    ) {
        return new ResponseEntity<>(service.getCommonComponents(components), HttpStatus.OK);
    }

}
