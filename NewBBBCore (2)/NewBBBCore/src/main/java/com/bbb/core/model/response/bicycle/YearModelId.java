package com.bbb.core.model.response.bicycle;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class YearModelId implements Serializable {
    private long yearId;
    private long modelId;
    private long brandId;

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public long getModelId() {
        return modelId;
    }

    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }
}
