package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Variations {
    @JacksonXmlProperty(localName = "VariationSpecificsSet")
    private VariationSpecificsSet variationSpecificsSet;
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "Variation")
    private List<Variation> variations;
    @JacksonXmlProperty(localName = "Pictures")
    private Pictures pictures;

    public VariationSpecificsSet getVariationSpecificsSet() {
        return variationSpecificsSet;
    }

    public void setVariationSpecificsSet(VariationSpecificsSet variationSpecificsSet) {
        this.variationSpecificsSet = variationSpecificsSet;
    }

    public List<Variation> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    public Pictures getPictures() {
        return pictures;
    }

    public void setPictures(Pictures pictures) {
        this.pictures = pictures;
    }
}
