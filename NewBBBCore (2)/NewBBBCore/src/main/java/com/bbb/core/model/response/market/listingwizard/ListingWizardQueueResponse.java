package com.bbb.core.model.response.market.listingwizard;

import com.bbb.core.model.database.type.StatusMarketListing;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class ListingWizardQueueResponse {
    @Id
    private long marketListingId;
    private long inventoryId;
    private String inventoryName;
    private String inventoryTitle;
    private float currentListedPrice;
    private float initialListPrice;
    private String imageDefault;
    private StatusMarketListing statusMarketListing;
    private String partnerId;
    private String sellerId;
    @Transient
    private String ownerName;
    private String ownerId;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
