package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellerProfiles {
    @JacksonXmlProperty(localName = "SellerShippingProfile")
    private SellerShippingProfile sellerShippingProfile;
    @JacksonXmlProperty(localName = "SellerReturnProfile")
    private SellerReturnProfile sellerReturnProfile;
    @JacksonXmlProperty(localName = "SellerPaymentProfile")
    private SellerPaymentProfile sellerPaymentProfile;

    public SellerShippingProfile getSellerShippingProfile() {
        return sellerShippingProfile;
    }

    public void setSellerShippingProfile(SellerShippingProfile sellerShippingProfile) {
        this.sellerShippingProfile = sellerShippingProfile;
    }

    public SellerReturnProfile getSellerReturnProfile() {
        return sellerReturnProfile;
    }

    public void setSellerReturnProfile(SellerReturnProfile sellerReturnProfile) {
        this.sellerReturnProfile = sellerReturnProfile;
    }

    public SellerPaymentProfile getSellerPaymentProfile() {
        return sellerPaymentProfile;
    }

    public void setSellerPaymentProfile(SellerPaymentProfile sellerPaymentProfile) {
        this.sellerPaymentProfile = sellerPaymentProfile;
    }
}
