package com.bbb.core.model.response.tradein.fedex.detail;

import com.bbb.core.model.request.tradein.fedex.detail.Weight;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShipmentRateDetail {
    @JacksonXmlProperty(localName = "RateType")
    private String rateType;
    @JacksonXmlProperty(localName = "RateZone")
    private int rateZone;
    @JacksonXmlProperty(localName = "RatedWeightMethod")
    private String ratedWeightMethod;
    @JacksonXmlProperty(localName = "DimDivisor")
    private float dimDivisor;
    @JacksonXmlProperty(localName = "FuelSurchargePercent")
    private float fuelSurchargePercent;
    @JacksonXmlProperty(localName = "TotalBillingWeight")
    private Weight totalBillingWeight;
    @JacksonXmlProperty(localName = "TotalSurcharges")
    private Money totalSurcharges;
    @JacksonXmlProperty(localName = "TotalNetChargeWithDutiesAndTaxes")
    private Money totalCharges;

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public int getRateZone() {
        return rateZone;
    }

    public void setRateZone(int rateZone) {
        this.rateZone = rateZone;
    }

    public String getRatedWeightMethod() {
        return ratedWeightMethod;
    }

    public void setRatedWeightMethod(String ratedWeightMethod) {
        this.ratedWeightMethod = ratedWeightMethod;
    }

    public float getDimDivisor() {
        return dimDivisor;
    }

    public void setDimDivisor(float dimDivisor) {
        this.dimDivisor = dimDivisor;
    }

    public float getFuelSurchargePercent() {
        return fuelSurchargePercent;
    }

    public void setFuelSurchargePercent(float fuelSurchargePercent) {
        this.fuelSurchargePercent = fuelSurchargePercent;
    }

    public Weight getTotalBillingWeight() {
        return totalBillingWeight;
    }

    public void setTotalBillingWeight(Weight totalBillingWeight) {
        this.totalBillingWeight = totalBillingWeight;
    }

    public Money getTotalSurcharges() {
        return totalSurcharges;
    }

    public void setTotalSurcharges(Money totalSurcharges) {
        this.totalSurcharges = totalSurcharges;
    }

    public Money getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(Money totalCharges) {
        this.totalCharges = totalCharges;
    }
}
