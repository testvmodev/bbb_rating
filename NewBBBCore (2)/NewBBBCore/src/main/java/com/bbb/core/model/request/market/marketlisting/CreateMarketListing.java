package com.bbb.core.model.request.market.marketlisting;

import com.bbb.core.model.database.EbayListingDuration;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.TypeListingDuration;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;

public class CreateMarketListing {
    private long inventoryId;
    private Long ebayCategoryId;
    private Long ebaySubCategoryId;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
//    private TypeListingDuration durationEbay;
    private Long ebayListingDurationId;
    @NotNull
    private Boolean isBestOffer;
    @NotNull
    private Long marketPlaceConfigId;

    //result after validation
    private EbayListingDuration ebayListingDuration;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getEbayCategoryId() {
        return ebayCategoryId;
    }

    public void setEbayCategoryId(Long ebayCategoryId) {
        this.ebayCategoryId = ebayCategoryId;
    }

    public Long getEbaySubCategoryId() {
        return ebaySubCategoryId;
    }

    public void setEbaySubCategoryId(Long ebaySubCategoryId) {
        this.ebaySubCategoryId = ebaySubCategoryId;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

//    public TypeListingDuration getDurationEbay() {
//        return durationEbay;
//    }
//
//    public void setDurationEbay(TypeListingDuration durationEbay) {
//        this.durationEbay = durationEbay;
//    }

    public Long getEbayListingDurationId() {
        return ebayListingDurationId;
    }

    public void setEbayListingDurationId(Long ebayListingDurationId) {
        this.ebayListingDurationId = ebayListingDurationId;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public Long getMarketPlaceConfigId() {
        return marketPlaceConfigId;
    }

    public void setMarketPlaceConfigId(Long marketPlaceConfigId) {
        this.marketPlaceConfigId = marketPlaceConfigId;
    }

    @JsonIgnore
    public EbayListingDuration getEbayListingDuration() {
        return ebayListingDuration;
    }

    @JsonIgnore
    public void setEbayListingDuration(EbayListingDuration ebayListingDuration) {
        this.ebayListingDuration = ebayListingDuration;
    }
}
