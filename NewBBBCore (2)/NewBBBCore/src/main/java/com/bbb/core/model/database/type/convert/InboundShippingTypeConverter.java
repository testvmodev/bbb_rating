package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.InboundShippingType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InboundShippingTypeConverter implements AttributeConverter<InboundShippingType, String> {
    @Override
    public String convertToDatabaseColumn(InboundShippingType shippingType) {
        if ( shippingType == null ) {
            return null;
        }
        return shippingType.getValue();
    }

    @Override
    public InboundShippingType convertToEntityAttribute(String s) {
        return InboundShippingType.findByValue(s);
    }
}
