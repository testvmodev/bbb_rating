package com.bbb.core.manager.bicycle.util;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.BrandManager;
import com.bbb.core.manager.bicycle.ModelManager;
import com.bbb.core.model.database.BicycleBrand;
import com.bbb.core.model.database.BicycleModel;
import com.bbb.core.model.database.BicycleYear;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import com.bbb.core.repository.BicycleYearRepository;
import com.bbb.core.repository.reout.BrandModelYearRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrandModelYearManager {
    @Autowired
    private BrandManager brandManager;
    @Autowired
    private ModelManager modelManager;
    @Autowired
    private BicycleYearRepository bicycleYearRepository;
    @Autowired
    private BrandModelYearRepository brandModelYearRepository;

    /**
     * Find brand + year + model. if doesnt exist and provide name, create new one
     * @return existed brand year model or newly created
     * @throws ExceptionResponse
     */
    public BrandModelYear findOneCreateIfNotExist(
            String brandName, String modelName, String yearName
    ) throws ExceptionResponse {
        Long brandId;
        Long modelId;
        Long yearId;
        BrandModelYear brandModelYear = brandModelYearRepository.findOne(
                brandName, modelName, yearName,
                System.currentTimeMillis());
        boolean bicycleMightExist = true;
//        if (brandModelYear != null) {
//            bicycleMightExist = true;
//        }
        if (brandModelYear == null || brandModelYear.getBrandId() == null) {
            BrandCreateRequest brandRequest = new BrandCreateRequest();
            brandRequest.setName(brandName);
            brandRequest.setValueModifier(0.0f);
            BicycleBrand bicycleBrand = brandManager.createBrandIgnoreRole(brandRequest);

            brandId = bicycleBrand.getId();
            bicycleMightExist = false;
        } else {
            brandId = brandModelYear.getBrandId();
        }
        if (brandModelYear == null || brandModelYear.getModelId() == null) {
            ModelCreateRequest modelRequest = new ModelCreateRequest();
            modelRequest.setBrandId(brandId);
            modelRequest.setName(modelName);
            BicycleModel bicycleModel = modelManager.createModelIgnoreRole(modelRequest);

            modelId = bicycleModel.getId();
            bicycleMightExist = false;
        } else {
            modelId = brandModelYear.getModelId();
        }

        if (brandModelYear == null || brandModelYear.getYearId() == null) {
            BicycleYear bicycleYear = new BicycleYear();
            bicycleYear.setName(yearName);
            bicycleYear.setId(Long.valueOf(yearName));
            bicycleYear.setApproved(false);
            bicycleYear.setDelete(false);
            bicycleYear = bicycleYearRepository.save(bicycleYear);

            yearId = bicycleYear.getId();
            bicycleMightExist = false;
        } else{
            yearId = brandModelYear.getYearId();
        }

        if (brandModelYear == null) {
            brandModelYear = new BrandModelYear();
            brandModelYear.setBrandName(brandName);
            brandModelYear.setModelName(modelName);
            brandModelYear.setYearName(yearName);
        }
        brandModelYear.setBrandId(brandId);
        brandModelYear.setModelId(modelId);
        brandModelYear.setYearId(yearId);
        brandModelYear.setBicycleMightExist(bicycleMightExist);

        return brandModelYear;
    }
}
