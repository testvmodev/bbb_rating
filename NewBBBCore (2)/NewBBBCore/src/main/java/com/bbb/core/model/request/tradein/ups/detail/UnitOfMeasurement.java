package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnitOfMeasurement {
    @JsonProperty("Code")
    private String code;

    public UnitOfMeasurement(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
