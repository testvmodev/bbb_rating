package com.bbb.core.model.database.embedded;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TradeInCustomQuoteCompDetailId implements Serializable {
    private long tradeInCustomQuoteId;
    private long compTypeId;


    public long getTradeInCustomQuoteId() {
        return tradeInCustomQuoteId;
    }

    public void setTradeInCustomQuoteId(long tradeInCustomQuoteId) {
        this.tradeInCustomQuoteId = tradeInCustomQuoteId;
    }

    public long getCompTypeId() {
        return compTypeId;
    }

    public void setCompTypeId(long compTypeId) {
        this.compTypeId = compTypeId;
    }

}
