package com.bbb.core.model.otherservice.request.event;

import com.bbb.core.common.IntegratedServices;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventSubscriptionRequest {
    private String type;
    @JsonProperty("market_listing_id")
    private Long marketListingId;
    @JsonProperty("inventory_auction_id")
    private Long inventoryAuctionId;
    private String action;

    public EventSubscriptionRequest() {}

    public EventSubscriptionRequest(long marketListingId, String action) {
        this.marketListingId = marketListingId;
        this.action = action;
        this.type = IntegratedServices.SUBSCRIPTION_TYPE_MARKET;
    }

    public EventSubscriptionRequest(String action, long inventoryAuctionId) {
        this.action = action;
        this.inventoryAuctionId = inventoryAuctionId;
        this.type = IntegratedServices.SUBSCRIPTION_TYPE_AUCTION;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
