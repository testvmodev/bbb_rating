package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PackageType {
    @JsonProperty("Code")
    private String code;

    public PackageType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
