package com.bbb.core.model.request.bid.auction;

public class AuctionRequest {
    private String createdById;


    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }
}
