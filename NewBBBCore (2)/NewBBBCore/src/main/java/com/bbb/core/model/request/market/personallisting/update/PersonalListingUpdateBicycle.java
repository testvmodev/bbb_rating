package com.bbb.core.model.request.market.personallisting.update;

public class PersonalListingUpdateBicycle {
    private String brandName;
    private String modelName;
    private String yearName;
    private Long bicycleTypeId;
//    private Long bicycleSizeId;
    private Float eBikeMileage;
    private Float eBikeHours;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

//    public Long getBicycleSizeId() {
//        return bicycleSizeId;
//    }
//
//    public void setBicycleSizeId(Long bicycleSizeId) {
//        this.bicycleSizeId = bicycleSizeId;
//    }


    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }
}
