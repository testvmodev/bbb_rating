package com.bbb.core.model.response.common;

import com.bbb.core.model.database.InventoryCompTypeSelect;

import java.util.List;

public class BicycleDetailMyListingResponse {
    private List<InventoryCompTypeSelect> types;
    private List<InventoryCompResponse> comps;

    public List<InventoryCompTypeSelect> getTypes() {
        return types;
    }

    public void setTypes(List<InventoryCompTypeSelect> types) {
        this.types = types;
    }

    public List<InventoryCompResponse> getComps() {
        return comps;
    }

    public void setComps(List<InventoryCompResponse> comps) {
        this.comps = comps;
    }
}
