package com.bbb.core.manager.brandyear;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.model.BicycleModelResponse;
import com.bbb.core.model.response.model.ModelFromBrandYear;
import com.bbb.core.model.response.model.ModelFromBrandYearResponse;
import com.bbb.core.model.response.model.family.ModelFamilyAndYearResponse;
import com.bbb.core.repository.bicyclemodel.out.ModelFromBrandYearRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class YearModelManager implements MessageResponses {
    @Autowired
    private ModelFromBrandYearRepository modelFromBrandYearRepository;
    @Autowired
    private ContentCommonRepository commonRepository;

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getModelFromBrandYear(long brandId, final Long yearId, String familyName) throws ExceptionResponse {
        if (StringUtils.isBlank(familyName)) {
            if (yearId == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YEAR_ID_MUST_BE_NOT_NULL), HttpStatus.BAD_REQUEST);
            }
            List<ModelFromBrandYear> res = modelFromBrandYearRepository.findAllByBrandYear(brandId, yearId);
            return convertModelFromBrandYearToModelFromBrandYearResponse(res);
        } else {
            List<ModelFromBrandYear> modelFromBrandYears = modelFromBrandYearRepository.findAllByBrandYear(brandId, yearId, "%" + familyName + "%");
            final String upFam = familyName.toUpperCase();
            modelFromBrandYears = modelFromBrandYears.stream().peek(mo -> {
                while (mo.getName().contains("  ")) {
                    mo.setName(mo.getName().replaceAll(" {2}", " "));
                }
                mo.setName(mo.getName().trim());
                String nameUp = mo.getName().toUpperCase();

                if (!nameUp.startsWith(upFam)) {
                    String strRemove = null;
                    if (nameUp.contains(ValueCommons.S_WORKS)) {
                        strRemove = ValueCommons.S_WORKS;
                    } else {
                        if (nameUp.contains(ValueCommons.WOMEN_S)) {
                            strRemove = ValueCommons.WOMEN_S;
                        } else {
                            if (nameUp.contains(ValueCommons.MEN_S)) {
                                strRemove = ValueCommons.MEN_S;
                            } else {
                                if (nameUp.contains(ValueCommons.BOY_S)) {
                                    strRemove = ValueCommons.BOY_S;
                                } else {
                                    if (nameUp.contains(ValueCommons.GIRL_S)) {
                                        strRemove = ValueCommons.GIRL_S;
                                    }
                                }
                            }
                        }
                    }
                    if (strRemove == null) {
                        mo.setName(null);
                    } else {
                        if (strRemove.equals(ValueCommons.S_WORKS) || nameUp.indexOf(strRemove) == 0) {
                            int indexWork = nameUp.indexOf(strRemove);
                            String modelName = nameUp.substring(indexWork + strRemove.length()).trim();
                            if (modelName.contains("+")) {
                                modelName = modelName.replaceAll("\\+", "").trim();
                            }
                            if (modelName.contains(" ")) {
                                int indexSpace = modelName.indexOf(' ');
                                modelName = modelName.substring(0, indexSpace).trim();
                            }
                            if (!modelName.equals(upFam)) {
                                mo.setName(null);
                            }
                        } else {
                            mo.setName(null);
                        }
                    }
                }
            }).filter(mo -> !StringUtils.isBlank(mo.getName()))
                    .collect(Collectors.toList());
            List<ContentCommonId> years = null;
            if (yearId == null) {
                years = modelFromBrandYears.stream().map(mo -> {
                    ContentCommonId year = new ContentCommonId();
                    year.setId(mo.getYearId());
                    year.setName(mo.getYearName());
                    return year;
                }).collect(Collectors.toList());
                CommonUtils.stream(years, (o1, o2) -> Long.compare(o1.getId(), o2.getId()));
                if (years.size() == 0) {
                    return new ArrayList<>();
                }
                if (years.size() > 1) {
                    years.sort((o1, o2) -> Long.compare(o2.getId(), o1.getId()));
                }
                long newYearId = years.get(0).getId();
                modelFromBrandYears = modelFromBrandYears.stream().filter(o -> o.getYearId() == newYearId).collect(Collectors.toList());
            }

            List<ModelFromBrandYearResponse> modelResponse = convertModelFromBrandYearToModelFromBrandYearResponse(modelFromBrandYears);
            if ( yearId != null ){
                return modelResponse;
            }else {
                ModelFamilyAndYearResponse response = new ModelFamilyAndYearResponse();
                response.setModels(modelResponse);
                response.setYears(years);
                return response;
            }

        }

    }

    private List<ModelFromBrandYearResponse> convertModelFromBrandYearToModelFromBrandYearResponse(List<ModelFromBrandYear> res) {
        Map<Long, List<ModelFromBrandYear>> mapRes = res.stream().collect(Collectors.groupingBy(ModelFromBrandYear::getId));
        List<ModelFromBrandYearResponse> responses = new ArrayList<>();
        mapRes.forEach((modelId, mos) -> {
            ModelFromBrandYearResponse resModel = new ModelFromBrandYearResponse();
            resModel.setId(modelId);
            resModel.setName(mos.get(0).getName());
            resModel.setBicycle(
                    mos.stream().map(mo -> {
                        BicycleModelResponse bi = new BicycleModelResponse();
                        bi.setBicycleId(mo.getBicycleId());
                        bi.setBicycleName(mo.getBicycleName());
                        bi.setBicycleImageDefault(mo.getBicycleImageDefault());
                        return bi;
                    }).collect(Collectors.toList())
            );
//            List<ContentCommonId> years = mos.stream().map(mo -> {
//                ContentCommonId year = new ContentCommonId();
//                year.setId(mo.getYearId());
//                year.setName(mo.getYearName());
//                return year;
//            }).collect(Collectors.toList());
//            CommonUtils.stream(years, (o1, o2)->Long.compare(o2.getId(), o2.getId()));
//            resModel.setYears(years);
            responses.add(resModel);
        });
        return responses;
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getAllBicycleModelFromBicycle(long brandId) throws ExceptionResponse {
        return commonRepository.findAllModeBrandFromBicycle(brandId).stream().map(ContentCommon::getId).collect(Collectors.toList());
    }
}
