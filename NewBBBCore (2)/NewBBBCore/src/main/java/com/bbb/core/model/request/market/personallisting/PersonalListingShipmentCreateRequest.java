package com.bbb.core.model.request.market.personallisting;

public class PersonalListingShipmentCreateRequest {
    private Long marketListingId;
    private String carrier;
    private String trackingNumber;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        if (carrier != null) {
            carrier = carrier.trim();
        }
        this.carrier = carrier;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
