package com.bbb.core.common.config.time;

import com.bbb.core.common.Constants;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.LocalDate;
import org.joda.time.YearMonth;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class YearMonthSerializer extends JsonSerializer<YearMonth> {
	@Override
	public void serialize(YearMonth yearMonth, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		jsonGenerator.writeString(yearMonth.toString(Constants.FORMAT_MONTH));
	}
}
