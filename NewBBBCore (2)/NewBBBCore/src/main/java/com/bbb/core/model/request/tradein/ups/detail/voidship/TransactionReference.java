package com.bbb.core.model.request.tradein.ups.detail.voidship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionReference {
    @JsonProperty("CustomerContext")
    private String customerContext;

    public TransactionReference() {}

    public TransactionReference(String customerContext) {
        this.customerContext = customerContext;
    }

    public String getCustomerContext() {
        return customerContext;
    }

    public void setCustomerContext(String customerContext) {
        this.customerContext = customerContext;
    }
}
