package com.bbb.core.service.impl.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.tradein.TradeInCustomQuoteManager;
import com.bbb.core.model.request.tradein.customquote.CustomQuoteReviewRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteCreateRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteRequest;
import com.bbb.core.model.request.tradein.customquote.TradeInCustomQuoteUpdateRequest;
import com.bbb.core.service.tradein.TradeInCustomQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class TradeInCustomQuoteServiceImpl implements TradeInCustomQuoteService {
    @Autowired
    private TradeInCustomQuoteManager manager;

    @Override
    public Object getTradeInCustomQuotes(TradeInCustomQuoteRequest request) throws ExceptionResponse {
        return manager.getTradeInCustomQuotes(request);
    }

    @Override
    public Object getDetailCustomQuote(long customQuoteId) throws ExceptionResponse {
        return manager.getDetailCustomQuote(customQuoteId);
    }

    @Override
    public Object createCustomQuote(TradeInCustomQuoteCreateRequest request) throws ExceptionResponse, MethodArgumentNotValidException {
        return manager.createCustomQuote(request);
    }



    @Override
    public Object updateTradeInCustomQuote(long customQuoteId, TradeInCustomQuoteUpdateRequest request) throws ExceptionResponse {
        return manager.updateTradeInCustomQuote(customQuoteId, request);
    }

    @Override
    public Object getCustomQuoteCommon(long tradeInCustomQuoteId) throws ExceptionResponse {
        return manager.getCustomQuoteCommon(tradeInCustomQuoteId);
    }

    @Override
    public Object postTradeInCustomQuote(long tradeInIdCustomQuote, List<MultipartFile> images, boolean isAddImage, boolean isSave) throws ExceptionResponse {
        return manager.postTradeInCustomQuote(tradeInIdCustomQuote, images, isAddImage, isSave);
    }

    @Override
    public Object deleteImageTradeInCustomQuote(long tradeInCustomQuoteId, long imageImageTypeId) throws ExceptionResponse {
        return manager.deleteImageTradeInCustomQuote(tradeInCustomQuoteId, imageImageTypeId);
    }

    @Override
    public Object updateTradeInCustomQuoteReviewed(CustomQuoteReviewRequest request) throws ExceptionResponse {
        return manager.updateTradeInCustomQuoteReviewed(request);
    }
}
