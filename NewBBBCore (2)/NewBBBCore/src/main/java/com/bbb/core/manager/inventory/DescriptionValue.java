package com.bbb.core.manager.inventory;


public interface DescriptionValue {
    String[] SPECIFICATIONS = new String[]{"Gender", "Frame Material", "Suspension", "Fork", "Rear Shock", "Bottom Bracket", "WeightCalculateShip"};
    String[] DRIVE_TRAINS = new String[]{"Front Derailleur", "Rear Derailleur", "Shifters", "Crankset", "Cassette"};
    String[] GEOME_TRYS = new String[]{"Steerer Tube Length", "Effective Top Tube", "Seat Tube", "Seat Tube Angle", "Head Tube Angle",
            "Head Tube", "Stack", "Reach", "Standover Height", "Rear Spacing"};

    String [] REPORT_CONDITION = new String[]{"CWheels", "Frame", "Drive Train", "Rubber", "Touch Points", "Is 26\" wheeled mountain bike?", "Is Bicycle Clean?"};
}
