package com.bbb.core.manager.favourite;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.Favourite;
import com.bbb.core.model.database.InventoryAuction;
import com.bbb.core.model.database.InventoryCompType;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.FavouriteType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.favourite.FavouriteAddRequest;
import com.bbb.core.model.request.favourite.FavouriteRemoveRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionResponse;
import com.bbb.core.model.response.favourite.FavouriteBaseInfo;
import com.bbb.core.model.response.favourite.FavouriteResponse;
import com.bbb.core.model.response.market.marketlisting.MarketListingResponse;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.out.InventoryAuctionResponseRepository;
import com.bbb.core.repository.favourite.FavouriteRepository;
import com.bbb.core.repository.favourite.out.FavouriteBaseInfoRepository;
import com.bbb.core.repository.favourite.out.FavouriteResponseRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.MarketListingResponseRepository;
import com.bbb.core.repository.reout.ContentIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FavouriteManager {
    @Autowired
    private FavouriteRepository favouriteRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private ContentIdRepository contentIdRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private FavouriteResponseRepository favouriteResponseRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private FavouriteBaseInfoRepository favouriteBaseInfoRepository;
    @Autowired
    private InventoryAuctionResponseRepository inventoryAuctionResponseRepository;
    @Autowired
    private MarketListingResponseRepository marketListingResponseRepository;


    public Object getFavourites(Pageable page) {
        String userId = CommonUtils.getUserLogin();
        ListObjResponse<Object> response = new ListObjResponse();

        String now =  new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date());
        response.setTotalItem(favouriteBaseInfoRepository.countAll(userId, now));
        response.setData(new ArrayList<>());
        if (response.getTotalItem() > 0){
            List<FavouriteBaseInfo> favourites = favouriteBaseInfoRepository.findAllFavourite(
                    userId,
                    new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                    page.getOffset(),
                    page.getPageSize()
            );

            List<Long> marketListingIds = favourites.stream().filter(o->o.getMarketListingId() != null).map(FavouriteBaseInfo::getMarketListingId).collect(Collectors.toList());
            List<MarketListingResponse> marketListingResponses;
            if (marketListingIds.size() > 0 ){
                marketListingResponses = marketListingResponseRepository.findAllByIds(marketListingIds);
            }else {
                marketListingResponses = null;
            }
            List<Long> inventoryAuctionIds = favourites.stream().filter(o->o.getInventoryAuctionId() != null).map(FavouriteBaseInfo::getInventoryAuctionId).collect(Collectors.toList());
            List<InventoryAuctionResponse> inventoryAuctions;
            if (inventoryAuctionIds.size() > 0 ){
                inventoryAuctions = inventoryAuctionResponseRepository.findAll(inventoryAuctionIds, now);
            }else {
                inventoryAuctions = null;
            }
            for (FavouriteBaseInfo favourite : favourites) {
                if (favourite.getMarketListingId() != null ){
                    if ( marketListingResponses != null){
                        for (MarketListingResponse marketListingRespons : marketListingResponses) {
                            if (marketListingRespons.getMarketListingId() == favourite.getMarketListingId().intValue()){
                                response.getData().add(marketListingRespons);
                                marketListingResponses.remove(marketListingRespons);
                                break;
                            }
                        }
                    }
                }else {
                    if(favourite.getInventoryAuctionId() != null ){
                        if (inventoryAuctions != null ){
                            for (InventoryAuctionResponse inventoryAuction : inventoryAuctions) {
                                if (inventoryAuction.getInventoryAuctionId() == favourite.getInventoryAuctionId().intValue()){
                                    response.getData().add(inventoryAuction);
                                    inventoryAuctions.remove(inventoryAuction);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();

        return response;
    }

    public Object addFavourite(FavouriteAddRequest request) throws ExceptionResponse {
        request.validate();

        String errorString = null;
        Integer errorCode = null;
        if (request.getFavouriteType() == FavouriteType.MARKET_LIST) {
            MarketListing marketListing = marketListingRepository.findOne(request.getMarketListingId());
            if (marketListing == null) {
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_NOT_FOUND, MessageResponses.MARKET_LISTING_NOT_EXIST),
//                        HttpStatus.BAD_REQUEST
//                );
                errorString = MessageResponses.MARKET_LISTING_NOT_EXIST;
                errorCode = ObjectError.ERROR_NOT_FOUND;
            }
//            else if (marketListing.getStatus() != StatusMarketListing.LISTED) {
//                errorString = MessageResponses.STATUS_MARKET_LISTING_INVALID;
//                errorCode = ObjectError.INVALID_ACTION;
//            }
        } else if (request.getFavouriteType() == FavouriteType.AUCTION) {
            InventoryAuction inventoryAuction = inventoryAuctionRepository.findOne(request.getInventoryAuctionId());
            if (inventoryAuction == null
//                    || inventoryAuction.isDelete() || inventoryAuction.isInactive()
            ) {
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_NOT_FOUND, MessageResponses.INVENTORY_AUCTION_NOT_EXIST),
//                        HttpStatus.BAD_REQUEST
//                );
                errorString = MessageResponses.INVENTORY_AUCTION_NOT_EXIST;
                errorCode = ObjectError.ERROR_NOT_FOUND;
            }
        }
        if (errorString != null) {
            throw new ExceptionResponse(errorCode, errorString, HttpStatus.BAD_REQUEST);
        }

        String userId = CommonUtils.getUserLogin();
        boolean isFavourite = favouriteRepository.isFavourite(
                userId,
                request.getFavouriteType() == FavouriteType.MARKET_LIST ? request.getMarketListingId() : null,
                request.getFavouriteType() == FavouriteType.AUCTION ? request.getInventoryAuctionId() : null
        );
        if (isFavourite) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.INVALID_ACTION, MessageResponses.ALREADY_FAVOURITE),
                    HttpStatus.BAD_REQUEST
            );
        }

        Favourite favourite = new Favourite();
        favourite.setType(request.getFavouriteType());
        favourite.setUserId(userId);
        favourite.setMarketListingId(request.getMarketListingId());
        favourite.setInventoryAuctionId(request.getInventoryAuctionId());

        return favouriteRepository.save(favourite);
    }

    public Object removeFavourite(FavouriteRemoveRequest request) throws ExceptionResponse {
        request.validate();
        Favourite favourite = favouriteRepository.findOne(
                CommonUtils.getUserLogin(),
                request.getFavouriteType() == FavouriteType.MARKET_LIST ? request.getMarketListingId() : null,
                request.getFavouriteType() == FavouriteType.AUCTION ? request.getInventoryAuctionId() : null
        );
        if (favourite == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.INVALID_ACTION, MessageResponses.NOT_FAVOURITE),
                    HttpStatus.BAD_REQUEST
            );
        }
        favourite.setDelete(true);
        favouriteRepository.save(favourite);

        return new MessageResponse("Success");
    }

//    public List<Long> checkFavourite(String userId, List<Long> marketListingIds) {
//        return contentIdRepository.findAllListingFavourite(userId, marketListingIds)
//                .stream().map(content -> content.getId())
//                .collect(Collectors.toList());
//    }
}
