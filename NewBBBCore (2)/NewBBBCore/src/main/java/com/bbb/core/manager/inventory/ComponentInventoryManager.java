package com.bbb.core.manager.inventory;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.inventory.InventoryComponentSearchResponse;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ComponentInventoryManager implements MessageResponses {
    @Autowired
    private ContentCommonRepository commonRepository;


    public Object getComponentInventoriesSearch() {
        InventoryComponentSearchResponse response = new InventoryComponentSearchResponse();
        response.setBicycleBrands(
                commonRepository.findAllBicycleBrand().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );

        response.setBicycleYears(
                commonRepository.findAllBicycleYear().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );
        response.setBicycleTypes(
                commonRepository.findAllBicycleType().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );
        response.setBrakeTypes(
                commonRepository.findAllBrakeType().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );
        response.setInventorySizes(
                commonRepository.findAllInventorySize().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );
        response.setFrameMaterials(
                commonRepository.findAllInventoryMaterial().stream().map(ContentCommon::getId).collect(Collectors.toList())
        );
        response.setConditions(new ArrayList<>());
        response.getConditions().add(ConditionInventory.EXCELLENT);
        response.getConditions().add(ConditionInventory.VERY_GOOD);
        response.getConditions().add(ConditionInventory.GOOD);
        response.getConditions().add(ConditionInventory.FAIR);
        response.getConditions().add(ConditionInventory.POOR);
        response.getConditions().add(ConditionInventory.USED);
        return response;
    }

    public Object getAllBicycleModel(List<Long> brandIds) throws ExceptionResponse {
//        if (bicycleBrandRepository.getNumberBrand(brandId) == 0) {
//            throw new ExceptionResponse(
//                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BRAND_ID_NOT_EXIST),
//                    HttpStatus.NOT_FOUND
//            );
//        }
//        return commonRepository.findAllBicycleModel(brandId).stream().map(ContentCommon::getId).collect(Collectors.toList());
        boolean isNullBrand = brandIds == null || brandIds.size() == 0;
        List<Long> tem = new ArrayList<>();
        tem.add(0L);
        return commonRepository.findAllBicycleModel(
                isNullBrand,
                isNullBrand ? tem : brandIds
        ).stream().map(ContentCommon::getId).collect(Collectors.toList());
    }

}
