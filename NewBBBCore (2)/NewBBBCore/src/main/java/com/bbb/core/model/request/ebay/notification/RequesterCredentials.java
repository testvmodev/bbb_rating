package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class RequesterCredentials {
    @JacksonXmlProperty(localName = "ebl:NotificationSignature")
    private String notificationSignature;

    public String getNotificationSignature() {
        return notificationSignature;
    }

    public void setNotificationSignature(String notificationSignature) {
        this.notificationSignature = notificationSignature;
    }
}
