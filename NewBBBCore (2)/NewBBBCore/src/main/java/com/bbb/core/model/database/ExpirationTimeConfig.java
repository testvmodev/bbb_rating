package com.bbb.core.model.database;

import com.bbb.core.model.database.type.TypeExpirationTime;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

@Entity
@Table(name = "expiration_time_config")
public class ExpirationTimeConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "expire_type")
    private TypeExpirationTime type;
    private float expireAfterHours;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeExpirationTime getType() {
        return type;
    }

    public void setType(TypeExpirationTime typeExpirationTime) {
        this.type = typeExpirationTime;
    }

    public float getExpireAfterHours() {
        return expireAfterHours;
    }

    public void setExpireAfterHours(float expireAfterHours) {
        this.expireAfterHours = expireAfterHours;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }
}
