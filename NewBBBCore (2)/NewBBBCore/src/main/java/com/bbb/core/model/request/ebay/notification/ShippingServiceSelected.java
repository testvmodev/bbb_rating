package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingServiceSelected {
    @JacksonXmlProperty(localName = "ShippingService")
    private String shippingService;
    @JacksonXmlProperty(localName = "ShippingServiceCost")
    private Currency shippingServiceCost;

    public String getShippingService() {
        return shippingService;
    }

    public void setShippingService(String shippingService) {
        this.shippingService = shippingService;
    }

    public Currency getShippingServiceCost() {
        return shippingServiceCost;
    }

    public void setShippingServiceCost(Currency shippingServiceCost) {
        this.shippingServiceCost = shippingServiceCost;
    }
}
