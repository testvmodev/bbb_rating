package com.bbb.core.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ListObjResponse<T> {
    private List<T> data;
    @JsonProperty("total_item")
    private int totalItem;
    private int page;
    @JsonProperty("page_size")
    private int pageSize;
    @JsonProperty("total_page")
    private int totalPage;

    public ListObjResponse() {}

    public ListObjResponse(int page, int pageSize) {
        this.page = page+1;
        this.pageSize = pageSize;
        this.data = new ArrayList<>();//default empty list data
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
        if (pageSize > 0 && totalItem >= 0) {
            totalPage = (int) Math.ceil((float) totalItem / pageSize);
        }
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page+1;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        if (pageSize > 0 && totalItem >= 0) {
            totalPage = (int) Math.ceil((float) totalItem / pageSize);
        }
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public void updateTotalPage(){
        totalPage =  (int)Math.ceil((float) totalItem / pageSize);
    }
}
