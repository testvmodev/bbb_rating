package com.bbb.core.security.user;


import com.bbb.core.model.database.type.UserRoleType;

import java.util.List;

public class UserContext {
    private String username;
    private String id;
    private List<UserRoleType> userRoles;
//    private String firstName;
//    private String lastName;
    private String wholesalerId;
    private String email;
    private String displayName;
//    private Partner partner;
    private String partnerId;
    private String token;
    private String onlineStoreId;
    private String storefrontId;

    public UserContext(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<UserRoleType> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoleType> userRoles) {
        this.userRoles = userRoles;
    }

    public String getWholesalerId() {
        return wholesalerId;
    }

    public void setWholesalerId(String wholesalerId) {
        this.wholesalerId = wholesalerId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

//    public Partner getPartner() {
//        return partner;
//    }

//    public void setPartner(Partner partner) {
//        this.partner = partner;
//    }


    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getOnlineStoreId() {
        return onlineStoreId;
    }

    public void setOnlineStoreId(String onlineStoreId) {
        this.onlineStoreId = onlineStoreId;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }
}
