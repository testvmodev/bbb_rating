package com.bbb.core.model.response.tradein.ups.detail.track;

import com.bbb.core.model.request.tradein.ups.detail.ShipService;
import com.bbb.core.model.response.tradein.ups.detail.track.PackageTrack;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentTrack {
    @JsonProperty("Service")
    private ShipService service;
    @JsonProperty("PickupDate")
    private String PickupDate;
    @JsonProperty("Package")
    private PackageTrack packageTrack;

    public ShipService getService() {
        return service;
    }

    public void setService(ShipService service) {
        this.service = service;
    }

    @JsonIgnore
    public String getPickupDate() {
        return PickupDate;
    }

    public void setPickupDate(String pickupDate) {
        PickupDate = pickupDate;
    }

    public PackageTrack getPackageTrack() {
        return packageTrack;
    }

    public void setPackageTrack(PackageTrack packageTrack) {
        this.packageTrack = packageTrack;
    }
}
