package com.bbb.core.model.response.market.listingwizard;

import com.bbb.core.model.database.type.ListingTypeEbay;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.bbb.core.model.database.type.TypeListingDuration;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class ListingWizardResponse {
    @Id
    private long inventoryId;
    private String inventoryName;
    private String inventoryTitle;
    private float currentListedPrice;

    private String imageDefault;
    private String partnerId;
    private String sellerId;
    private Float initialPrice;
    private StatusInventory status;
    private StageInventory stage;
    @Transient
    private ListingWizardItem ebay;
    @Transient
    private ListingWizardItem bbb;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public ListingWizardItem getEbay() {
        return ebay;
    }

    public void setEbay(ListingWizardItem ebay) {
        this.ebay = ebay;
    }

    public ListingWizardItem getBbb() {
        return bbb;
    }

    public void setBbb(ListingWizardItem bbb) {
        this.bbb = bbb;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Float getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(Float initialPrice) {
        this.initialPrice = initialPrice;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }


}
