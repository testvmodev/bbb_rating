package com.bbb.core.service.impl.inventory;

import com.bbb.core.manager.inventory.InventoryTypeManager;
import com.bbb.core.service.inventory.InventoryTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryTypeServiceImpl implements InventoryTypeService {
    @Autowired
    private InventoryTypeManager manager;

    @Override
    public Object getAllInventoryType() {
        return manager.getAllInventoryType();
    }
}
