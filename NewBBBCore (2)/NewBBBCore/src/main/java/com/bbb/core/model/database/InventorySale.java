package com.bbb.core.model.database;

import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.database.type.UserRoleType;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

@Entity
@Table(name = "inventory_sale")
public class InventorySale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long inventoryId;
    private String buyerId;
    private String buyerDisplayName;
    private String buyerEmail;
    private UserRoleType buyerRole;
    private String orderId;
    private Boolean isLocalPickup;
    private OutboundShippingType shippingType;
    private String shippingAddressLine;
    private String shippingCity;
    private String shippingState;
    private String shippingPostalCode;
    private String shippingPhone;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    private boolean isDelete;
    private Float price;
    private String cartType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerDisplayName() {
        return buyerDisplayName;
    }

    public void setBuyerDisplayName(String buyerDisplayName) {
        this.buyerDisplayName = buyerDisplayName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public UserRoleType getBuyerRole() {
        return buyerRole;
    }

    public void setBuyerRole(UserRoleType buyerRole) {
        this.buyerRole = buyerRole;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Boolean isLocalPickup() {
        return isLocalPickup;
    }

    public void setLocalPickup(Boolean localPickup) {
        isLocalPickup = localPickup;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingAddressLine() {
        return shippingAddressLine;
    }

    public void setShippingAddressLine(String shippingAddressLine) {
        this.shippingAddressLine = shippingAddressLine;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingPostalCode() {
        return shippingPostalCode;
    }

    public void setShippingPostalCode(String shippingPostalCode) {
        this.shippingPostalCode = shippingPostalCode;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }


    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }
}
