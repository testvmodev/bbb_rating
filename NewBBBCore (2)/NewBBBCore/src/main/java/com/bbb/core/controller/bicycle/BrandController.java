package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.brand.BrandUpdateRequest;
import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.BrandService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class BrandController {
    private BrandService service;

    @Autowired
    public BrandController(BrandService service) {
        this.service = service;
    }

    @GetMapping(value = Constants.URL_GET_BRAND)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "page"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "size"),
    })
    public ResponseEntity getBrands(
            @RequestParam(value = "nameSearch", required = false) String nameSearch,
            @RequestParam(value = "sortType", defaultValue = "ASC") Sort sortType,
            @RequestParam(value = "sortField", defaultValue = "NAME")BicycleBrandSortField sortField,
            Pageable pageable) throws ExceptionResponse {
        return new ResponseEntity<>(service.getBrands(nameSearch,sortType, sortField, pageable),
                HttpStatus.OK);

    }

    @PostMapping(value = Constants.URL_BRAND_CREATE)
    public ResponseEntity createBrand(
            @RequestParam(value = "brandLogo", required = false ) MultipartFile brandLogo,
            @RequestParam String name,
            @RequestParam(value = "valueModifier", required = false ) Float valueModifier
            ) throws ExceptionResponse {
        BrandCreateRequest request = new BrandCreateRequest();
        request.setBrandLogo(brandLogo);
        request.setName(name);
        request.setValueModifier(valueModifier);
        return new ResponseEntity<>(service.createBrand(request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_BRAND)
    public ResponseEntity deleteBrand(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteBrand(id),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_BRAND)
    public ResponseEntity getBrandDetail(
            @PathVariable(value = Constants.ID) long brandId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getBrandDetail(brandId),
                HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_BRAND)
    public ResponseEntity updateBrand(
            @PathVariable(value = Constants.ID) long brandId,
            @RequestParam (value = "brandLogo" , required = false) MultipartFile brandLogo,
            @RequestParam (value = "name" , required = false) String name,
            @RequestParam (value = "valueModifier" , required = false) Float valueModifier,
            @RequestParam (value = "isApproved" , required = false) Boolean isApproved
    ) throws ExceptionResponse {
        BrandUpdateRequest request = new BrandUpdateRequest();
        request.setBrandLogo(brandLogo);
        request.setName(name);
        request.setValueModifier(valueModifier);
        request.setApproved(isApproved);
        return new ResponseEntity<>(service.updateBrand(brandId, request), HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_BRAND_LOGO)
    public ResponseEntity deleteLogoBrand(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteLogoBrand(id),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_BRANDS)
    public ResponseEntity getAllBrands() throws ExceptionResponse{
        return new ResponseEntity<>(service.getAllBrands(), HttpStatus.OK);
    }
}
