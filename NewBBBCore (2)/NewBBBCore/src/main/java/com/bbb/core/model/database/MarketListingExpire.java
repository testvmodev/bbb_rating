package com.bbb.core.model.database;

import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MarketListingExpire {
    @Id
    private long id;
    private LocalDateTime lastMailExpireSoon;
    private LocalDateTime lastMailExpired;
    private Integer expireSoonFailCount = 0;
    private Integer expiredFailCount = 0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getLastMailExpireSoon() {
        return lastMailExpireSoon;
    }

    public void setLastMailExpireSoon(LocalDateTime lastMailExpireSoon) {
        this.lastMailExpireSoon = lastMailExpireSoon;
    }

    public LocalDateTime getLastMailExpired() {
        return lastMailExpired;
    }

    public void setLastMailExpired(LocalDateTime lastMailExpired) {
        this.lastMailExpired = lastMailExpired;
    }

    public Integer getExpireSoonFailCount() {
        return expireSoonFailCount;
    }

    public void setExpireSoonFailCount(Integer expireSoonFailCount) {
        this.expireSoonFailCount = expireSoonFailCount;
    }

    public Integer getExpiredFailCount() {
        return expiredFailCount;
    }

    public void setExpiredFailCount(Integer expiredFailCount) {
        this.expiredFailCount = expiredFailCount;
    }
}
