package com.bbb.core.model.other;

import com.fasterxml.jackson.annotation.JsonProperty;
//import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategorySpecifyRecommendation {
    @JsonProperty(value = "Name")
//    @SerializedName(value = "Name")
    private String name;

    @JsonProperty(value = "ValidationRules")
//    @SerializedName(value = "ValidationRules")
    private ValidationRules validationRules;

    @JsonProperty(value = "ValueRecommendation")
//    @SerializedName(value = "ValueRecommendation")
    private List<ValueRecommendation> valueRecommendations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValidationRules getValidationRules() {
        return validationRules;
    }

    public void setValidationRules(ValidationRules validationRules) {
        this.validationRules = validationRules;
    }

    public List<ValueRecommendation> getValueRecommendations() {
        return valueRecommendations;
    }

    public void setValueRecommendations(List<ValueRecommendation> valueRecommendations) {
        this.valueRecommendations = valueRecommendations;
    }
}
