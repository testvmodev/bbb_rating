package com.bbb.core.manager.bid.async;

import com.bbb.core.model.database.Tracking;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.TrackingType;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.tracking.TrackingRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class InventoryAuctionManagerAsync {
    @Autowired
    private TrackingRepository trackingRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;

    @Async
    public void createTracking(String userId, long inventoryAuctionId) {
        Tracking tracking = trackingRepository.findOneAuction(userId, inventoryAuctionId);
        if (tracking == null) {
            tracking = new Tracking();
        }
        tracking.setLastViewed(LocalDateTime.now());
        tracking.setUserId(userId);
        tracking.setInventoryAuctionId(inventoryAuctionId);
        tracking.setCountTracking(tracking.getCountTracking() + 1);
        tracking.setType(TrackingType.WHOLE_SALLER);

        trackingRepository.save(tracking);
    }

    @Async
    public void delistMarketListingQueue(long inventoryId){
        marketListingRepository.updateStatusMarketListingToDeListFromQueue(inventoryId);
    }
}
