package com.bbb.core.repository.market;

import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface MarketListingRepository extends JpaRepository<MarketListing, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM market_listing " +
            "WHERE id = ?1"
    )
    MarketListing findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.id IN :marketListings AND " +
            "market_listing.is_delete = 0"
    )
    List<MarketListing> findAllByListingId(
            @Param(value = "marketListings") List<Long> marketListings
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.inventory_id IN :inventories AND " +
            "market_listing.is_delete = 0"
    )
    List<MarketListing> findAllByInventoryId(
            @Param(value = "inventories") List<Long> inventories
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.inventory_id IN :inventories"
    )
    List<MarketListing> findAllByInventoryIdPassDelete(
            @Param(value = "inventories") List<Long> inventories
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.inventory_id = :inventoryId AND " +
            "(market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} OR " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTING.getValue()} OR " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTING_ERROR.getValue()}) " +
            "AND " +
            "market_listing.is_delete = 0"
    )
    List<MarketListing> findAllByInventoryIdActive(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM market_listing WHERE market_listing.item_id = :itemId AND market_listing.is_delete = 0")
    MarketListing findOneByItemId(
            @Param(value = "itemId") long itemId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id = :inventoryId AND " +
            "(market_listing.status = 'Listed' OR market_listing.status = 'Pending') AND " +
            "market_listing.is_delete = 0")
    List<MarketListing> findAllByInventoryListedPending(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id = :inventoryId AND " +
            "(market_listing.status = 'Listed' OR market_listing.status = 'Pending') AND " +
            "market_listing.id <> :marketListingIdSold AND " +
            "market_listing.is_delete = 0")
    List<MarketListing> findAllByInventoryListedPending(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "marketListingIdSold") long marketListingIdSold
    );

//    @Query(nativeQuery = true, value = "SELECT * FROM " +
//            "market_listing WHERE " +
//            "market_listing.inventory_id = :inventoryId AND " +
//            "market_listing.market_place_id <> :placeId AND " +
//            "market_listing.status = 'Listed'"
//    )
//    List<MarketListing> findAllToDelist(
//            @Param(value = "inventoryId") long inventoryId,
//            @Param(value = "placeId") long placeId
//    );

    //    @Override
//    @Query(nativeQuery = true, value = "SELECT COUNT(id) " +
//            "FROM market_listing " +
//            "WHERE id = :id AND is_delete = 0"
//    )
//    int countListing(@Param("id") long id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.market_place_config_id = :marketListingConfigId AND " +
            "market_listing.status = :statusErrorListing AND " +
//            "(market_listing.status = :statusListing AND " +
            "DATEDIFF_BIG(MILLISECOND, market_listing.last_update, CONVERT(DATETIME, :now)) > :timeOutListing AND " +
            "market_listing.count_listing_error <= :maxCountListingError " +
            "ORDER BY market_listing.count_listing_error DESC, market_listing.last_update ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<MarketListing> findAllToReListEbay(
            @Param(value = "marketListingConfigId") long marketListingConfigId,
            @Param(value = "statusErrorListing") String statusErrorListing,
//            @Param(value = "statusListing") String statusListing,
            @Param(value = "now") String now,
            @Param(value = "timeOutListing") long timeOutListing,
            @Param(value = "maxCountListingError") long maxCountListingError,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE market_listing " +
            "SET market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).ERROR.getValue()} " +
            "WHERE " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.count_listing_error > :maxCount AND " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTING_ERROR.getValue()}"
    )
    void updateDeleteMarketListingEbayMaxError(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "maxCount") long maxCount
    );

//    @Query(nativeQuery = true, value = "SELECT CASE EXISTS(SELECT * FROM market_listing WHERE market_listing.id = :marketListingId)")


    @Query(nativeQuery = true, value = "SELECT CASE WHEN " +
            "EXISTS(" +
            "SELECT * FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id = :inventoryId AND " +
            "(market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.name()} OR " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTING.name()} OR " +
            "(market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTING_ERROR.name()} AND " +
            "market_listing.count_listing_error <= :#{T(com.bbb.core.manager.schedule.jobs.market.RelistEbay).MAX_COUNT_LISTING_EBAY})" +
            ")" +
            ")" +
            "THEN " +
            "CAST(1 AS BIT) " +
            "ELSE CAST(0 AS BIT) END"
    )
    boolean checkExistInventoryListing(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id = :inventoryId AND " +
            "market_listing.market_place_config_id = :marketPlaceConfigId AND " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.status <> :#{T(com.bbb.core.model.database.type.StatusMarketListing).ERROR.name()}"
    )
    List<MarketListing> findAllMarketListing(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "marketPlaceConfigId") long marketPlaceConfigId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id IN :inventoryIds AND " +
            "market_listing.market_place_config_id = :marketPlaceConfigId AND " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.status <> :#{T(com.bbb.core.model.database.type.StatusMarketListing).ERROR.name()}"
    )
    List<MarketListing> findAllMarketListing(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "marketPlaceConfigId") long marketPlaceConfigId
    );

    @Query(nativeQuery = true, value = "SELECT DISTINCT market_listing.inventory_id " +
            "FROM " +
            "market_listing WHERE market_listing.id IN :marketListingIds")
    List<Integer> findAllInventoryIdByIds(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).QUEUE.getValue()} AND " +
            "market_listing.is_delete = 0 " +
            "ORDER BY market_listing.start_queue_time ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<MarketListing> findAllMarketListingQueueEbay(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT * FROM market_listing WHERE market_listing.id IN :marketListingIds")
    List<MarketListing> findAllByIds(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT market_listing.* " +
            "FROM market_listing " +

            "JOIN inventory " +
            "ON market_listing.inventory_id = inventory.id " +
            "AND inventory.is_delete = 0 " +
            "AND inventory.seller_id = :userId " +

            "JOIN inventory_type " +
            "ON inventory.type_id = inventory_type.id " +
            "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +

            "WHERE market_listing.is_delete = 0 " +
            "AND (:statusMarketListing IS NULL " +
            "OR market_listing.status = :#{#statusMarketListing == null ? ' ' : #statusMarketListing.getValue()})"
    )
    List<MarketListing> findAllPersonalListing(
            @Param("userId") String userId,
            @Param("statusMarketListing") StatusMarketListing statusMarketListing
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(market_listing.id) " +
            "FROM market_listing " +

            "JOIN inventory " +
            "ON market_listing.inventory_id = inventory.id " +
            "AND inventory.is_delete = 0 " +
            "AND inventory.seller_id = :userId " +

            "JOIN inventory_type " +
            "ON inventory.type_id = inventory_type.id " +
            "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +

            "WHERE market_listing.is_delete = 0 " +
            "AND (:statusMarketListing IS NULL " +
            "OR market_listing.status = :#{#statusMarketListing == null ? ' ' : #statusMarketListing.getValue()})"
    )
    int countAllPersonalListing(
            @Param("userId") String userId,
            @Param("statusMarketListing") StatusMarketListing statusMarketListing
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(market_listing.id) " +
            "FROM market_listing " +

            "JOIN inventory " +
            "ON market_listing.inventory_id = inventory.id " +
            "AND inventory.is_delete = 0 " +
            "AND inventory.seller_id = :userId " +

            "WHERE market_listing.is_delete = 0 " +
            "AND (:statusMarketListing IS NULL " +
            "OR market_listing.status = :#{#statusMarketListing == null ? ' ' : #statusMarketListing.getValue()})"
    )
    int countAllUserListing(
            @Param("userId") String userId,
            @Param("statusMarketListing") StatusMarketListing statusMarketListing
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE market_listing SET market_listing.status = :status WHERE market_listing.id = :marketListingId")
    void updateStatusMarketListing(
            @Param(value = "marketListingId") long marketListingId,
            @Param(value = "status") String status
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE market_listing SET " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).DE_LISTED.getValue()}," +
            "market_listing.is_delete = 1 " +
            "WHERE market_listing.inventory_id = :inventoryId AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).QUEUE.getValue()} ")
    void updateStatusMarketListingToDeListFromQueue(
            @Param(value = "inventoryId") long inventoryId
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE market_listing SET market_listing.status = :status WHERE market_listing.id IN :marketListingIds")
    void updateStatusMarketListing(
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "status") String status
    );

    @Query(nativeQuery = true, value = "SELECT market_listing.inventory_id " +
            "FROM market_listing " +
            "WHERE " +
            "market_listing.inventory_id IN :inventoryIds AND " +
            "market_listing.market_place_id = :marketPlaceIdBBB AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()}")
    List<Integer> findAllInventoryAtBBBNotListed(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "marketPlaceIdBBB") long marketPlaceIdBBB
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(market_listing.id) " +
            "FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.id IN :marketListingIds AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).QUEUE.getValue()} AND " +
            "market_listing.market_place_id = :marketPlaceId")
    int countMarketListingQueue(
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "marketPlaceId") Long marketPlaceId
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value =
            "UPDATE market_listing SET market_listing.is_delete = 1 WHERE market_listing.id IN :marketListingIds")
    void deleteMarketListings(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE market_listing SET market_listing.last_update = GETDATE() WHERE market_listing.id IN :marketListingIds")
    void updateLastUpdate(
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 market_listing.id FROM market_listing " +
            "WHERE " +
            "market_listing.item_id = :itemId AND " +
            "market_listing.is_delete = 0")
    Integer findMarketListingIdByItemId(
            @Param(value = "itemId") long itemId
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value =
            "UPDATE market_listing SET " +
                    "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).ERROR.getValue()}, " +
                    "market_listing.last_msg_error = :lastMsgError " +
                    "WHERE market_listing.id IN :marketListingIds")
    void updateStatusErrorAndLastMsgError(
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "lastMsgError") String lastMsgError
    );

    @Query(nativeQuery = true, value =
            "SELECT * " +
                    "FROM market_listing " +
                    "WHERE " +
                    "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND "+
                    "market_listing.market_place_id = :marketPlaceId AND "+
                    "DATEDIFF_BIG(MILLISECOND, market_listing.end_time, CONVERT(DATETIME, :now)) > 0 AND " +
                    "market_listing.is_delete = 0"
    )
    List<MarketListing> findAllMarketListingEbayExpire(
            @Param(value = "now") String now,
            @Param(value = "marketPlaceId") long marketPlaceId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM market_listing " +
            "WHERE market_listing.inventory_id = :inventoryId " +
            "AND (:isDelete IS NULL OR market_listing.is_delete = :isDelete)" +
            "ORDER BY market_listing.id DESC"
    )
    MarketListing findLastByInventoryId(
            @Param("inventoryId") long inventoryId,
            @Param("isDelete") Boolean isDelete
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM market_listing " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "WHERE market_listing.id = :marketListingId " +
            "AND market_place.type = :#{#marketType.getValue()}"
    )
    MarketListing findOneInMarketPlace(
            @Param("marketListingId") long marketListingId,
            @Param("marketType") MarketType marketType
    );
}
