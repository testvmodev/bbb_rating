package com.bbb.core.model.request.bid.offer.offerbid;

import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.request.sort.InventoryOfferFieldSort;
import com.bbb.core.model.request.sort.Sort;

import java.util.List;

public class OfferBidRequest {
    private List<StatusOffer> statusBids;
    private List<Long> inventoryIds;
    private Sort sortType;
    private Boolean isWin;
    private InventoryOfferFieldSort sortField;

    public List<StatusOffer> getStatusBids() {
        return statusBids;
    }

    public void setStatusBids(List<StatusOffer> statusBids) {
        this.statusBids = statusBids;
    }

    public List<Long> getInventoryIds() {
        return inventoryIds;
    }

    public void setInventoryIds(List<Long> inventoryIds) {
        this.inventoryIds = inventoryIds;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public InventoryOfferFieldSort getSortField() {
        return sortField;
    }

    public void setSortField(InventoryOfferFieldSort sortField) {
        this.sortField = sortField;
    }

    public Boolean getWin() {
        return isWin;
    }

    public void setWin(Boolean win) {
        isWin = win;
    }
}
