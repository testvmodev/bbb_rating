package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInUpgradeComp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public interface TradeInUpgradeCompRepository extends JpaRepository<TradeInUpgradeComp, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(trade_in_upgrade_comp.id) " +
            "FROM trade_in_upgrade_comp WHERE trade_in_upgrade_comp.id IN :upgradeCompIds")
    int getTotalNumber(@Param(value = "upgradeCompIds") List<Long> upgradeCompIds);

    @Query(nativeQuery = true, value = "SELECT " +
            "* " +
            "FROM trade_in_upgrade_comp WHERE trade_in_upgrade_comp.id IN :upgradeCompIds")
    List<TradeInUpgradeComp> findAll(@Param(value = "upgradeCompIds") List<Long> upgradeCompIds);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_upgrade_comp " +
            "WHERE trade_in_upgrade_comp.id IN " +
            "(SELECT trade_in_upgrade_comp_detail.trade_in_upgrade_comp_id FROM trade_in_upgrade_comp_detail " +
            "WHERE trade_in_upgrade_comp_detail.trade_in_id = :tradeInId)")
    List<TradeInUpgradeComp> findAllTradeInUpgradeComponent(
            @Param(value = "tradeInId") long tradeInId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_upgrade_comp")
    List<TradeInUpgradeComp> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM trade_in_upgrade_comp WHERE id IN :upgradeCompIds")
    int getCountUpgradeComponent(
            @Param(value = "upgradeCompIds") List<Long> upgradeCompIds
    );
}
