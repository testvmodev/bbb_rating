package com.bbb.core.model.database.type;

public enum FavouriteType {
    MARKET_LIST("Market listing"),
    AUCTION("Auction");

    private final String value;

    FavouriteType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static FavouriteType findByValue(String value) {
        for (FavouriteType type : FavouriteType.values()) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        return null;
    }
}
