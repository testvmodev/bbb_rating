package com.bbb.core.repository.market.out;

import com.bbb.core.model.response.market.marketlisting.MarketToList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketToListRepository extends JpaRepository<MarketToList, Long> {
//    private long marketConfigId;
//    private MarketType type;
//    private String martKetPlaceName;

    @Query(nativeQuery = true, value = "SELECT " +
            "market_place_config.id AS market_config_id, " +
            "market_place.id AS market_place_id, " +
            "market_place.type AS type, " +
            "market_place.name AS market_place_name " +
            "FROM market_place_config JOIN market_place ON market_place_config.market_place_id = market_place.id " +
            "WHERE " +
            "market_place_config.is_delete = 0 AND market_place.is_delete = 0 " +
            "AND market_place_config.is_api_sandbox = :isSandbox " +
            "AND (market_place.type = :#{T(com.bbb.core.model.database.type.MarketType).BBB.getValue()} OR " +
            "market_place.type = :#{T(com.bbb.core.model.database.type.MarketType).EBAY.getValue()})")
    List<MarketToList> findAllIdActiveEbayBBB(
            @Param(value = "isSandbox") boolean isSandbox
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "market_place_config.id AS market_config_id, " +
            "market_place.id AS market_place_id, " +
            "market_place.type AS type, " +
            "market_place.name AS market_place_name " +
            "FROM market_place_config JOIN market_place ON market_place_config.market_place_id = market_place.id " +
            "WHERE " +
            "market_place_config.is_delete = 0 AND market_place.is_delete = 0 " +
            "AND market_place_config.is_api_sandbox = :isSandbox AND " +
            "market_place.type = :#{T(com.bbb.core.model.database.type.MarketType).EBAY.getValue()}")
    MarketToList findAllIdActiveEbay(
            @Param(value = "isSandbox") boolean isSandbox
    );
    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "market_place_config.id AS market_config_id, " +
            "market_place.id AS market_place_id, " +
            "market_place.type AS type, " +
            "market_place.name AS market_place_name " +
            "FROM market_place_config JOIN market_place ON market_place_config.market_place_id = market_place.id " +
            "WHERE " +
            "market_place_config.is_delete = 0 AND market_place.is_delete = 0 " +
            "AND market_place_config.is_api_sandbox = :isSandbox AND " +
            "market_place.type = :#{T(com.bbb.core.model.database.type.MarketType).BBB.getValue()}")
    MarketToList findAllIdActiveBBB(
            @Param(value = "isSandbox") boolean isSandbox
    );
}
