package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingPackageDetails {
    @JacksonXmlProperty(localName = "PackageDepth")
    private Package packageDepth;
    @JacksonXmlProperty(localName = "PackageLength")
    private Package packageLength;
    @JacksonXmlProperty(localName = "PackageWidth")
    private Package packageWidth;
    @JacksonXmlProperty(localName = "ShippingIrregular")
    private boolean shippingIrregular;
    @JacksonXmlProperty(localName = "ShippingPackage")
    private String shippingPackage;
    @JacksonXmlProperty(localName = "WeightMajor")
    private Package weightMajor;
    @JacksonXmlProperty(localName = "WeightMinor")
    private Package weightMinor;
    @JacksonXmlProperty(localName = "OutOfStockControl")
    private boolean outOfStockControl;
    @JacksonXmlProperty(localName = "eBayPlus")
    private boolean eBayPlus;
    @JacksonXmlProperty(localName = "eBayPlusEligible")
    private boolean eBayPlusEligible;

    public Package getPackageDepth() {
        return packageDepth;
    }

    public void setPackageDepth(Package packageDepth) {
        this.packageDepth = packageDepth;
    }

    public Package getPackageLength() {
        return packageLength;
    }

    public void setPackageLength(Package packageLength) {
        this.packageLength = packageLength;
    }

    public Package getPackageWidth() {
        return packageWidth;
    }

    public void setPackageWidth(Package packageWidth) {
        this.packageWidth = packageWidth;
    }

    public boolean isShippingIrregular() {
        return shippingIrregular;
    }

    public void setShippingIrregular(boolean shippingIrregular) {
        this.shippingIrregular = shippingIrregular;
    }

    public String getShippingPackage() {
        return shippingPackage;
    }

    public void setShippingPackage(String shippingPackage) {
        this.shippingPackage = shippingPackage;
    }

    public Package getWeightMajor() {
        return weightMajor;
    }

    public void setWeightMajor(Package weightMajor) {
        this.weightMajor = weightMajor;
    }

    public Package getWeightMinor() {
        return weightMinor;
    }

    public void setWeightMinor(Package weightMinor) {
        this.weightMinor = weightMinor;
    }

    public boolean isOutOfStockControl() {
        return outOfStockControl;
    }

    public void setOutOfStockControl(boolean outOfStockControl) {
        this.outOfStockControl = outOfStockControl;
    }

    public boolean iseBayPlus() {
        return eBayPlus;
    }

    public void seteBayPlus(boolean eBayPlus) {
        this.eBayPlus = eBayPlus;
    }

    public boolean iseBayPlusEligible() {
        return eBayPlusEligible;
    }

    public void seteBayPlusEligible(boolean eBayPlusEligible) {
        this.eBayPlusEligible = eBayPlusEligible;
    }
}
