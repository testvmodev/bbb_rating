package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Phone {
    @JsonProperty("Number")
    private String number;

    public Phone() {}

    public Phone(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
