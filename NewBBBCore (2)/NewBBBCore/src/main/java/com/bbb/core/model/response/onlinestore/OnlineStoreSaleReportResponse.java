package com.bbb.core.model.response.onlinestore;

import com.bbb.core.model.response.onlinestore.dashboard.OnlineStoreDailySaleResponse;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.joda.time.YearMonth;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Immutable
public class OnlineStoreSaleReportResponse {
    @Id
//    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentYearMonthAsString")
    private YearMonth month;
    private Float totalSale;
    @Transient
    private List<OnlineStoreDailySaleResponse> days;

    public YearMonth getMonth() {
        return month;
    }

    public void setMonth(YearMonth month) {
        this.month = month;
    }

    public Float getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(Float totalSale) {
        this.totalSale = totalSale;
    }

    public List<OnlineStoreDailySaleResponse> getDays() {
        return days;
    }

    public void setDays(List<OnlineStoreDailySaleResponse> days) {
        this.days = days;
    }
}
