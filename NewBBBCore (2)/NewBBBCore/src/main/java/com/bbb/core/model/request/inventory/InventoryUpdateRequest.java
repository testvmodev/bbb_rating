package com.bbb.core.model.request.inventory;


import com.bbb.core.model.database.type.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryUpdateRequest {
    private Long bicycleId;
    private String description;
    private String imageDefault;

    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
    private Float currentListPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private float discountedPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;


    private ConditionInventory condition;
    private String partnerId;
    private String sellerId;
    private String serialNumber;
    private Long inventoryTypeId;
    private String title;
    private Boolean createPo;
    private Long sizeInventoryId;
    private StatusInventory status;
    private StageInventory stage;
    private Boolean bestOffer;
    private Boolean isCustomQuote;
    private Long age;

    private Long typeId;

    private Float weight;
    private Boolean isPreList;
    private Boolean isCheckInOverride;
    private LocalDate dayHold;
    private Boolean isOverrideCreatePo;
    private Boolean isNonComplianceNoti;
    private RefundReturnTypeInventory refundReturnType;
    private Float priceRole;
    private Float listingAge;
    private LocalDate firstListedDate;
    private LocalDate dateSold;
    private String masterMultiListing;
    private Boolean isReviseItem;
    private RecordTypeInventory recordType;
    private String partnerEmail;
    private String note;
    private String site;
    private Long stockLocationId;
    private LocalDateTime dateReceived;
    private Float conditionScore;
    private Float privatePartyValue;
    private Float valueAdditionalComponent;

    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }


    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Long getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(Long inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCreatePo() {
        return createPo;
    }

    public void setCreatePo(Boolean createPo) {
        this.createPo = createPo;
    }

    public Long getSizeInventoryId() {
        return sizeInventoryId;
    }

    public void setSizeInventoryId(Long sizeInventoryId) {
        this.sizeInventoryId = sizeInventoryId;
    }


    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public Boolean getBestOffer() {
        return bestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        this.bestOffer = bestOffer;
    }

    public Boolean getCustomQuote() {
        return isCustomQuote;
    }

    public void setCustomQuote(Boolean customQuote) {
        isCustomQuote = customQuote;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public void setDiscountedPrice(float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getCurrentListPrice() {
        return currentListPrice;
    }

    public void setCurrentListPrice(Float currentListPrice) {
        this.currentListPrice = currentListPrice;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }


    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Boolean getPreList() {
        return isPreList;
    }

    public void setPreList(Boolean preList) {
        isPreList = preList;
    }

    public Boolean getCheckInOverride() {
        return isCheckInOverride;
    }

    public void setCheckInOverride(Boolean checkInOverride) {
        isCheckInOverride = checkInOverride;
    }

    public LocalDate getDayHold() {
        return dayHold;
    }

    public void setDayHold(LocalDate dayHold) {
        this.dayHold = dayHold;
    }

    public Boolean getOverrideCreatePo() {
        return isOverrideCreatePo;
    }

    public void setOverrideCreatePo(Boolean overrideCreatePo) {
        isOverrideCreatePo = overrideCreatePo;
    }

    public Boolean getNonComplianceNoti() {
        return isNonComplianceNoti;
    }

    public void setNonComplianceNoti(Boolean nonComplianceNoti) {
        isNonComplianceNoti = nonComplianceNoti;
    }

    public RefundReturnTypeInventory getRefundReturnType() {
        return refundReturnType;
    }

    public void setRefundReturnType(RefundReturnTypeInventory refundReturnType) {
        this.refundReturnType = refundReturnType;
    }

    public Float getPriceRole() {
        return priceRole;
    }

    public void setPriceRole(Float priceRole) {
        this.priceRole = priceRole;
    }

    public Float getListingAge() {
        return listingAge;
    }

    public void setListingAge(Float listingAge) {
        this.listingAge = listingAge;
    }

    public LocalDate getFirstListedDate() {
        return firstListedDate;
    }

    public void setFirstListedDate(LocalDate firstListedDate) {
        this.firstListedDate = firstListedDate;
    }

    public LocalDate getDateSold() {
        return dateSold;
    }

    public void setDateSold(LocalDate dateSold) {
        this.dateSold = dateSold;
    }

    public String getMasterMultiListing() {
        return masterMultiListing;
    }

    public void setMasterMultiListing(String masterMultiListing) {
        this.masterMultiListing = masterMultiListing;
    }

    public Boolean getReviseItem() {
        return isReviseItem;
    }

    public void setReviseItem(Boolean reviseItem) {
        isReviseItem = reviseItem;
    }

    public RecordTypeInventory getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordTypeInventory recordType) {
        this.recordType = recordType;
    }

    public String getPartnerEmail() {
        return partnerEmail;
    }

    public void setPartnerEmail(String partnerEmail) {
        this.partnerEmail = partnerEmail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getStockLocationId() {
        return stockLocationId;
    }

    public void setStockLocationId(Long stockLocationId) {
        this.stockLocationId = stockLocationId;
    }

    public LocalDateTime getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Float getConditionScore() {
        return conditionScore;
    }

    public void setConditionScore(Float conditionScore) {
        this.conditionScore = conditionScore;
    }

    public Float getPrivatePartyValue() {
        return privatePartyValue;
    }

    public void setPrivatePartyValue(Float privatePartyValue) {
        this.privatePartyValue = privatePartyValue;
    }

    public Float getValueAdditionalComponent() {
        return valueAdditionalComponent;
    }

    public void setValueAdditionalComponent(Float valueAdditionalComponent) {
        this.valueAdditionalComponent = valueAdditionalComponent;
    }
}
