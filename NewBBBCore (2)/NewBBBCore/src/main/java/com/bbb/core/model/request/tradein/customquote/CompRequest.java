package com.bbb.core.model.request.tradein.customquote;


import javax.validation.constraints.NotNull;

public class CompRequest {
    @NotNull
    private Long compId;
    @NotNull
    private String value;

    public Long getCompId() {
        return compId;
    }

    public void setCompId(Long compId) {
        this.compId = compId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
