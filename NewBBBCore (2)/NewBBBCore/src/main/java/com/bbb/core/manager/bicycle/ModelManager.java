package com.bbb.core.manager.bicycle;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.BicycleBrand;
import com.bbb.core.model.database.BicycleModel;
import com.bbb.core.model.request.model.ModelCreateRequest;
import com.bbb.core.model.request.model.ModelUpdateRequest;
import com.bbb.core.model.request.sort.BicycleModelSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.brand.ModelResponse;
import com.bbb.core.model.response.model.BicycleModelResponse;
import com.bbb.core.repository.BicycleBrandRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicyclemodel.BicycleModelRepository;
import com.bbb.core.repository.bicyclemodel.BicycleModelResponseRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ModelManager implements MessageResponses {
    @Autowired
    private BicycleModelRepository bicycleModelRepository;
    @Autowired
    private BicycleBrandRepository bicycleBrandRepository;
    @Autowired
    private BicycleModelResponseRepository bicycleModelResponseRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;

    public Object getModels(List<Long> brandIds, String nameSearch, BicycleModelSortField sortField, Sort sortType, Pageable pageable) throws ExceptionResponse {
        if (pageable.getPageSize()<=0||pageable.getPageNumber()<0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, PAGE_OR_SIZE_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        ListObjResponse<ModelResponse> response = new ListObjResponse<>();
        List<Long> temList = new ArrayList<>();
        temList.add(0L);
//        HashMap<String, String> o = new HashMap<>();
//        o.put()

        boolean isBrandNull = brandIds == null || brandIds.size() == 0;
        if (!isBrandNull){
            List<Long> countBrands = bicycleModelResponseRepository.checkListBrandIs(brandIds);
            if (countBrands.size() < brandIds.size()){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND
                );
            }
        }
        response.setData(
                bicycleModelResponseRepository.findAllBicycleModel(
                        isBrandNull, isBrandNull ? temList : brandIds,
                        nameSearch == null ? null : "%" + nameSearch + "%",
                        sortField, sortType,
                        pageable.getOffset(), pageable.getPageSize())
        );
        response.setTotalItem(
                bicycleModelResponseRepository.getTotalCount(
                        isBrandNull, isBrandNull ? temList : brandIds,
                        nameSearch == null ? null : "%" + nameSearch + "%"
                )
        );
        response.setPage(pageable.getPageNumber());
        response.setPageSize(pageable.getPageSize());
        response.updateTotalPage();
        return response;
    }

    public Object getModelDetail(long modelId) throws ExceptionResponse {
//        BicycleModel brand = bicycleModelRepository.findOneNotDelete(modelId);
        ModelResponse response = bicycleModelResponseRepository.getDetailModel(modelId) ;
        if (response == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MODEL_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        return response;
    }

    public Object createModel(ModelCreateRequest model) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        return createModelIgnoreRole(model);
    }

    public BicycleModel createModelIgnoreRole(ModelCreateRequest model) throws ExceptionResponse {
        if (model == null || !model.validateBrand()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MODEL_NAME_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if (model.getBrandId() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_IS_REQUEST),
                    HttpStatus.BAD_REQUEST);
        }
        if (bicycleBrandRepository.findOneNotDelete(model.getBrandId()) == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        checkExisted(model.getBrandId(), model.getName());

        BicycleModel bicycleBrand = new BicycleModel();
        bicycleBrand.setApproved(false);
        bicycleBrand.setName(model.getName().trim());
        bicycleBrand.setBrandId(model.getBrandId());
        return bicycleModelRepository.save(bicycleBrand);
    }

    public Object deleteModel(long id) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleModel bicycleModel = bicycleModelRepository.findOneNotDelete(id);
        if (bicycleModel == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MODEL_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        int countModel = bicycleRepository.getCountModel(bicycleModel.getId());
        if ( countModel > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM,(countModel==1?THERE_IS:THERE_ARE+countModel )+ BICYCLE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        bicycleModel.setDelete(true);
        bicycleModelRepository.save(bicycleModel);
        return new MessageResponse(SUCCESS);
    }

    public Object updateModel(long modelId, ModelUpdateRequest model) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        if (model == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MODEL_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        BicycleModel oldModel = bicycleModelRepository.findOneNotDelete(modelId);
        if (oldModel == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MODEL_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        if (model.getBrandId() != null) {
            if (bicycleBrandRepository.findOne(model.getBrandId()) == null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            oldModel.setBrandId(model.getBrandId());
        }
        if (!StringUtils.isBlank(model.getName())) {
            if (!model.getName().trim().equals(oldModel.getName())) {
                checkExisted(oldModel.getBrandId(), model.getName());
                oldModel.setName(model.getName().trim());
            }
        }
        if (model.getApproved() != null) {
            oldModel.setApproved(model.getApproved());
        }
//        oldModel.setLastUpdate(LocalDateTime.now());
        return bicycleModelRepository.save(oldModel);
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getModelsFromBrandYear(Long brandId, Long yearId) throws ExceptionResponse {
        return contentCommonRepository.findAllModelFromBrandYear(brandId, yearId).stream().map(ContentCommon::getId).collect(Collectors.toList());
    }

    public Object getAllModelsFromBrand(long brandId) throws ExceptionResponse {
        BicycleBrand brand = bicycleBrandRepository.findOneNotDelete(brandId);
        if (brand == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.BRAND_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return contentCommonRepository.findAllModelFromBrand(brandId);
    }

    private void checkExisted(long brandId, String modelName) throws ExceptionResponse {
        int count = bicycleModelRepository.getTotalCountByBrandIdName(brandId, modelName);
        if (count > 0 ) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, MODEL_NAME_EXIST),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
