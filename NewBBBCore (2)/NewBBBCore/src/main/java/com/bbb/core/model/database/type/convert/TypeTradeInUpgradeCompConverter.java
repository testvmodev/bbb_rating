package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TypeTradeInUpgradeComp;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TypeTradeInUpgradeCompConverter implements AttributeConverter<TypeTradeInUpgradeComp, String> {
    @Override
    public String convertToDatabaseColumn(TypeTradeInUpgradeComp typeTradeInUpgradeComp) {
        if ( typeTradeInUpgradeComp == null ){
            return null;
        }
        return typeTradeInUpgradeComp.getValue();
    }

    @Override
    public TypeTradeInUpgradeComp convertToEntityAttribute(String s) {
        return TypeTradeInUpgradeComp.findByValue(s);
    }
}
