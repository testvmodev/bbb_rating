package com.bbb.core.controller.schedule;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.service.schedule.ScheduleJobLogService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ScheduleJobLogController {
    @Autowired
    private ScheduleJobLogService service;

    @GetMapping(Constants.URL_JOB_LOGS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getLogs(
            @RequestParam(value = "jobId", required = false) Long jobId,
            Pageable pageable
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getLogs(jobId, pageable));
    }

    @PostMapping(Constants.URL_JOB_LOGS_PURGE)
    public ResponseEntity purgeLogs(
            @RequestParam(value = "jobId", required = false) Long jobId,
            @RequestParam(value = "olderThan") LocalDateTime olderThan
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.purgeLog(jobId, olderThan));
    }
}
