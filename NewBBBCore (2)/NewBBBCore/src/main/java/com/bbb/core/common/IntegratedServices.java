package com.bbb.core.common;

import com.bbb.core.common.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegratedServices {
    @Autowired
    public IntegratedServices(AppConfig appConfig) {
        if (appConfig.getEndpoint() != null) {
            AppConfig.EndpointConfig endpoint = appConfig.getEndpoint();
            BASE_SERVICE = endpoint.getBaseApiUrl();
            BASE_SERVICE_ML = endpoint.getBaseValueGuideApiUrl();
            BILLING_API_PREFIX = endpoint.getBillingApiPrefix() != null ? endpoint.getBillingApiPrefix() : BILLING_API_PREFIX;
            AUTHENTICATION_API_PREFIX = endpoint.getAuthApiPrefix() != null ? endpoint.getAuthApiPrefix() : AUTHENTICATION_API_PREFIX;
            NOTIFY_API_PREFIX = endpoint.getNotifyApiPrefix() != null ? endpoint.getNotifyApiPrefix() : NOTIFY_API_PREFIX;
            SUBSCRIPTION_API_PREFIX = endpoint.getSubscriptionApiPrefix() != null ? endpoint.getSubscriptionApiPrefix() : SUBSCRIPTION_API_PREFIX;
            VALUE_GUIDE_API_PREFIX = endpoint.getValueGuideApiPrefix() != null ? endpoint.getValueGuideApiPrefix() : VALUE_GUIDE_API_PREFIX;
            SHIPMENT_API_PREFIX = endpoint.getShipmentApiPrefix() != null ? endpoint.getShipmentApiPrefix() : SHIPMENT_API_PREFIX;

            BASE_WEB_APP = endpoint.getBaseWebappUrl() != null ? endpoint.getBaseWebappUrl() : BASE_WEB_APP;
        }
    }

//    String VALUE_GUIDE_PUBLIC_KEY = "8875154e-6713-42de-a821-1f559df112ef";
    public static String VALUE_GUIDE_PUBLIC_KEY = "fa5a63ed-69e9-4c44-b873-0c85c87f31dd";
    public static String VALUE_GUIDE_API_KEY = "6BBCD495-DEAD-487B-8822-DA487BC2A688";

    public static String BASE_SERVICE = "https://apidev-v2.bicyclebluebook.com";
    public static String BASE_SERVICE_ML = "https://api-v2.bicyclebluebook.com";
    private static String BILLING_API_PREFIX = "";
    private static String AUTHENTICATION_API_PREFIX = "";
    private static String NOTIFY_API_PREFIX = "";
    private static String SUBSCRIPTION_API_PREFIX = "";
    private static String VALUE_GUIDE_API_PREFIX = "";
    private static String SHIPMENT_API_PREFIX = "";


//    public static String BASE_URL_BILLING = BASE_SERVICE + BILLING_API_PREFIX;
//    public static String BASE_URL_AUTHENTICATION = BASE_SERVICE + AUTHENTICATION_API_PREFIX;
//    public static String BASE_URL_NOTIFY = BASE_SERVICE + NOTIFY_API_PREFIX;
//    public static String BASE_URL_SUBSCRIPTION = BASE_SERVICE + SUBSCRIPTION_API_PREFIX;
//    public static String BASE_URL_TRADE_IN_VALUE = "https://tradein-widget.bicyclebluebook.com/tradein/v2";
//    public static String BASE_URL_VALUE_GUIDE_V2 = BASE_SERVICE + VALUE_GUIDE_API_PREFIX;
//    public static String BASE_URL_SHIPMENT = BASE_SERVICE + SHIPMENT_API_PREFIX;

    public static String getBaseUrlBilling() {
        return BASE_SERVICE + BILLING_API_PREFIX;
    }

    public static String getBaseUrlAuthentication() {
        return BASE_SERVICE + AUTHENTICATION_API_PREFIX;
    }

    public static String getBaseUrlNotify() {
        return BASE_SERVICE + NOTIFY_API_PREFIX;
    }

    public static String getBaseUrlSubscription() {
        return BASE_SERVICE + SUBSCRIPTION_API_PREFIX;
    }

    public static String getBaseUrlValueGuideV2() {
        return BASE_SERVICE_ML + VALUE_GUIDE_API_PREFIX;
    }

    public static String getBaseUrlShipment() {
        return BASE_SERVICE + SHIPMENT_API_PREFIX;
    }

    public static String BASE_WEB_APP = "";

    public static final String API = "/api";
    public static final String PRIVATE = "/private";
    public static final String ADMIN = "/admin";
    public static final String AUTHENTICATE = "/authenticate";
    public static final String CART = "/cart";
    public static final String V1 = "/v1";
    public static final String ADD = "/add";
    public static final String MAIL = "/mail";
    public static final String SEND = "/send";
    public static final String ID = "id";
    public static final String PATH_ID = "/{" + ID + "}";
    public static final String USER = "/user";
    public static final String ITEM = "/item";
    public static final String SINGLE = "/single";
    public static final String BUY_NOW = "/buy-now";
    public static final String MARKET_PLACE = "/marketplace";
    public static final String AUCTION = "/auction";
    public static final String IS_SHOPPING = "/is-shopping";
    public static final String FULL_INFO = "/full-info";
    public static final String WAREHOUSE = "/warehouse";
    public static final String STATUS = "/status";
    public static final String COMMON = "/common";
    public static final String GOOGLE_SERVICE = "/google-service";
    public static final String PARTNER = "/partner";
    public static final String LIST_BY_ID_S = "/list-by-ids";
    public static final String LIST_INFO = "/list-info";
    public static final String EVENT = "/event";
    public static final String PUBLIC = "/public";
    public static final String MARKET_CHANGE = "/register-market-change";
    public static final String INVENTORY_CHANGE = "/register-inventory-change";
    public static final String MARKET_DELETED = "/register-market-deleted";
    public static final String MARKET_EXPIRED = "/register-market-expired";
    public static final String SUBSCRIPTION = "/subscription";
    public static final String ORDER = "/order";
    public static final String SHIPMENTS = "/shipments";
    public static final String CONFIG = "/config";
    public static final String ACTIVE = "/active";


    public static final String TRADE_IN_VALUE = "/gettradeinvaluesVMODEV";

    public static final String PREDICTION_PRICE = "/getPredictionPrice";


    public static final String URL_SEND_MAIL = API + V1 + MAIL + SEND;
    public static final String URL_GET_USER_DETAIL = API + V1 + USER + PATH_ID;
    public static final String URL_GET_USERS = API + V1 + USER;
    public static final String URL_GET_USER_DETAIL_FULL_INFO = API + V1 + USER + FULL_INFO;
    public static final String URL_GET_USERS_DETAIL_FULL_INFO = API + V1 + USER + LIST_INFO;

    public static final String URL_GET_WAREHOUSE_DETAIL = API + V1 + WAREHOUSE + PATH_ID;

    public static final String URL_COMMON_GOOGLE_SERVICE = API + V1 + COMMON + GOOGLE_SERVICE;

    public static final String URL_CART_ADD_SINGLE = API + V1 + CART + ADD;
    public static final String URL_CART_STATUS = API + V1 + CART + STATUS + PATH_ID;
    public static final String URL_CARTS_STATUSES = API + V1 + CART + STATUS;
    public static final String URL_CART_ITEM_SINGLE = API + V1 + CART + ITEM + SINGLE;
    public static final String URL_LIST_PARTNER_DETAIL = API + V1 + PARTNER + LIST_BY_ID_S;
    public static final String URL_LIST_ADMIN_DETAIL = API + V1 + ADMIN + LIST_INFO;


    public static final String URL_ORDER_DETAIL = API + V1 + ORDER + PATH_ID;


    public static final String URL_EVENT_LISTING_UPDATE = API + V1 + EVENT + PUBLIC + MARKET_CHANGE;
    public static final String URL_EVENT_LISTING_DELETE = API + V1 + EVENT + PUBLIC + MARKET_DELETED;
    public static final String URL_EVENT_LISTING_EXPIRE = API + V1 + EVENT + PUBLIC + MARKET_EXPIRED;
    public static final String URL_EVENT_INVENTORY_UPDATE = API + V1 + EVENT + PUBLIC + INVENTORY_CHANGE;

    public static final String URL_EVENT_SUBSCRIPTION = API + V1 + SUBSCRIPTION + EVENT + PUBLIC;



    public static final String URL_SHIPMENT_CREATE = PRIVATE + API + SHIPMENTS;
    public static final String URL_SHIPMENT_CONFIG_ACTIVED = PRIVATE + API + SHIPMENTS + CONFIG + ACTIVE;



    //placeholder
    public static final String TRADE_IN_ID = "{tradeInId}";
    public static final String BICYCLE_TITLE = "{bicycleTitle}";
    public static final String CUSTOM_QUOTE_REVIEWED_OLD_VALUE = "{customQuoteReviewedOldValue}";
    public static final String CUSTOM_QUOTE_REVIEWED_NEW_VALUE = "{customQuoteReviewedNewValue}";


    public static final String GOOGLE_SERVICE_CATEGORY_INFO = "get_info";
    public static final String GOOGLE_ADDRESS_POLITICAL = "political";
    public static final String GOOGLE_ADDRESS_COUNTRY = "country";
    public static final String GOOGLE_ADDRESS_AREA_1 = "administrative_area_level_1";
    public static final String GOOGLE_ADDRESS_AREA_2 = "administrative_area_level_2";
    public static final String GOOGLE_ADDRESS_LOCALITY = "locality";
    public static final String GOOGLE_ADDRESS_POSTAL_CODE = "postal_code";

    public static final String CUSTOM_QUOTE_NEW_TEMPLATE = "custom-quote-new";
    public static final String CUSTOM_QUOTE_REVIEWED_TEMPLATE = "custom-quote-reviewed";
    public static final String CUSTOM_QUOTE_CHANGE_VALUE_TEMPLATE = "custom-quote-changed";
    public static final String CUSTOM_QUOTE_CHANGE_VALUE_MESSAGE = "Value for Trade in: " + TRADE_IN_ID + " for bike " + BICYCLE_TITLE + " has been changed from $" + CUSTOM_QUOTE_REVIEWED_OLD_VALUE + " to $" + CUSTOM_QUOTE_REVIEWED_NEW_VALUE + " by a BBB user";

    public static final String BICYCLE_BOOT_NAME = "BICYCLEBLUEBOOK.COM";

    public static final String SUBSCRIPTION_ACTION_PUBLISH = "publish";
    public static final String SUBSCRIPTION_ACTION_RENEW = "renew";
    public static final String SUBSCRIPTION_ACTION_UPDATE = "update";
    public static final String SUBSCRIPTION_ACTION_START = "start";

    public static final String SUBSCRIPTION_TYPE_MARKET = "market";
    public static final String SUBSCRIPTION_TYPE_AUCTION = "auction";

    public static final String TEMPLATE_AUCTION_EXPIRE = "auction-expired";
    public static final String TEMPLATE_AUCTION_ACCEPTED = "auction-accepted";
    public static final String TEMPLATE_OFFER_EXPIRE = "offer-expired";
    public static final String TEMPLATE_OFFER_ACCEPTED = "offer-accepted";
    public static final String TEMPLATE_OFFER_REJECTED_ACCEPTED = "offer-rejected-accepted";
    public static final String TEMPLATE_OFFER_MADE = "offer-made";
    public static final String TEMPLATE_OFFER_REJECT = "offer-rejected";
    public static final String TEMPLATE_OFFER_COUNTERED = "offer-countered";
    public static final String TEMPLATE_OFFER_BUYER_COUNTERED = "offer-buyer-countered";
    public static final String TEMPLATE_OFFER_BUYER_REJECT = "offer-buyer-rejected";
    public static final String TEMPLATE_OFFER_BUYER_ACCEPTED = "offer-buyer-accepted";
    public static final String TEMPLATE_AUCTION_STARTED = "auction-started";
    public static final String TEMPLATE_LISTING_EXPIRE_WARN = "expiration-warning";
    public static final String TEMPLATE_LISTING_EXPIRED = "expired-listing";
    public static final String TEMPLATE_LISTING_SHIPPED = "order-shipped";
    public static final String TEMPLATE_SCORECARD_AVAILABLE = "trade-in-available";
    public static final String TEMPLATE_CUSTOM_QUOTE_AVAILABLE = "custom-quote-available";

    public static final String DEFAULT_MAIL_EXCHANGE_TYPE = "default";

    public static String getPathViewCard() {
        return BASE_WEB_APP + CART;
    }

    public static final String PATH_BIKE_NAME = "{bike_name}";
    public static final String PATH_STATUS = "{status}";
    public static final String PATH_AUCTION_NAME = "{auction name}";
    public static final String SUBJECT_OFFER_ACCEPTED = "Your offer on " + PATH_BIKE_NAME + " at BicycleBlueBook.com has been " + PATH_STATUS + " !";
    public static final String SUBJECT_START_AUCTION = "Auction  " + PATH_AUCTION_NAME + " at BicycleBlueBook.com has been started!";
}
