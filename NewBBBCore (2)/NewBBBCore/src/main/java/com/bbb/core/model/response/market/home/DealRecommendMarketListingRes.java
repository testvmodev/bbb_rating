package com.bbb.core.model.response.market.home;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class DealRecommendMarketListingRes extends DealRecommendResponse{
    @Id
    private Long marketListingId;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }
}
