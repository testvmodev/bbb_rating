package com.bbb.core.repository.tracking;

import com.bbb.core.model.database.InventoryTrackingView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryTrackingViewRepository extends JpaRepository<InventoryTrackingView, Long> {

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM inventory_tracking_view " +
            "WHERE " +
            "inventory_tracking_view.user_id = :userId AND " +
            "inventory_tracking_view.inventory_id = :inventoryId"
    )
    int getNumberViewUser(
            @Param(value = "userId") String userId,
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM inventory_tracking_view " +
            "WHERE " +
            "inventory_tracking_view.user_id = :userId AND " +
            "inventory_tracking_view.inventory_id = :inventoryId"
    )
    InventoryTrackingView findOne(
            @Param(value = "userId") String userId,
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "inventory_tracking_view " +
            "WHERE inventory_tracking_view.user_id = :userId " +
            "ORDER BY inventory_tracking_view.last_update DESC OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<InventoryTrackingView> findAllRemove(
            @Param(value = "userId") String userId,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "inventory_tracking_view JOIN inventory ON  inventory_tracking_view.inventory_id = inventory.id " +
            "WHERE inventory_tracking_view.user_id = :userId AND " +
            "inventory.status = 'Active' AND " +
            "inventory.stage = 'Listed' " +
            "ORDER BY inventory_tracking_view.last_update DESC OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<InventoryTrackingView> findAllInventoryTrackingView(
            @Param(value = "userId") String userId,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

}
