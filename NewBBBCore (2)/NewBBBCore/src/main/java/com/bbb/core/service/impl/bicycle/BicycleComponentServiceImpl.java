package com.bbb.core.service.impl.bicycle;

import com.bbb.core.manager.bicycle.BicycleComponentManager;
import com.bbb.core.service.bicycle.BicycleComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BicycleComponentServiceImpl implements BicycleComponentService {
    @Autowired
    private BicycleComponentManager manager;
    @Override
    public Object importBicycleComponent(long offset, long size) {
        return manager.importBicycleComponent(offset, size);
    }
}
