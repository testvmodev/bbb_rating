package com.bbb.core.common.config.env;

public enum Environments {
    LOCAL("Local"),
    UNIT_TEST("Unit test"),
    DEVELOPMENT("Development"),
    STAGING("Staging"),
    PRODUCTION("Production");

    String value;

    Environments(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
