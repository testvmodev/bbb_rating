package com.bbb.core.model.response.market.marketlisting;

public class CreateMarketListingResponse {
    private long itemId;


    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
}
