package com.bbb.core.model.response.market.mylisting;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.response.market.mylisting.detail.StatusMarketListingMessage;
import com.bbb.core.model.response.market.mylisting.embed.DraftEmbed;
import com.bbb.core.model.response.market.mylisting.embed.MarketListingEmbed;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;


import javax.persistence.*;

@Entity
@Immutable
@Table(name = "my_listing")
public class MyListingResponse {
    @Id
//    @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
//    @GeneratedValue(generator = "generator")
//    @AccessType(AccessType.Type.FIELD)
    private Long id;
    private DraftEmbed draft;
    private MarketListingEmbed done;
    @JoinColumn(insertable = false, updatable = false)
    private LocalDateTime postingTime;
    @JoinColumn(insertable = false, updatable = false)
    private Boolean isBestOffer;
    @JoinColumn(insertable = false, updatable = false)
    private String title;
    @JoinColumn(insertable = false, updatable = false)
    private StatusMarketListing statusMarketListing;
    @JoinColumn(insertable = false, updatable = false)
    private Float currentListedPrice;
    @JoinColumn(insertable = false, updatable = false)
    private String imageDefault;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }

    public DraftEmbed getDraft() {
        return draft;
    }

    public void setDraft(DraftEmbed draft) {
        this.draft = draft;
    }

    public MarketListingEmbed getDone() {
        return done;
    }

    public void setDone(MarketListingEmbed done) {
        this.done = done;
    }

    public LocalDateTime getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(LocalDateTime postingTime) {
        this.postingTime = postingTime;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    @JsonProperty
    public String statusMessage() {
        return StatusMarketListingMessage.getMessage(this.statusMarketListing);
    }
}
