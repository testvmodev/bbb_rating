package com.bbb.core.service.impl.rating;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.rating.RatingManager;
import com.bbb.core.model.request.rating.RatingCreateRequest;
import com.bbb.core.model.request.rating.RatingGetRequest;
import com.bbb.core.model.request.rating.RatingUpdateRequest;
import com.bbb.core.service.rating.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service

public class RatingServiceImpl implements RatingService {

    @Override
    public Object getRatingForBicycle(RatingGetRequest request) throws ExceptionResponse {
        return ratingManager.getRatingForBicycle(request);
    }

    @Override
    public Object getRatingTest(Long marketListingId, Long inventoryAuctionId, Pageable pageable) throws Exception {
        return ratingManager.getRatingTest(marketListingId, inventoryAuctionId, pageable);
    }

    @Override
    public Object getRatingForUser(Long marketListingId, Long inventoryAuctionId, String userId) throws Exception {
        return ratingManager.getRatingForUser(marketListingId, inventoryAuctionId, userId);
    }

    @Autowired
    private RatingManager ratingManager;

    @Override
    public Object createRating(RatingCreateRequest request, Long invetoryAuctionId, Long marketListingId) throws Exception {
        return ratingManager.createRating(request, invetoryAuctionId, marketListingId);
    }

    @Override
    public Object updateRating(RatingUpdateRequest request, Long id,Long inventoryAuctionId, Long marketListingId) throws Exception {
        return ratingManager.updateRating(request, id, inventoryAuctionId, marketListingId);
    }
}
