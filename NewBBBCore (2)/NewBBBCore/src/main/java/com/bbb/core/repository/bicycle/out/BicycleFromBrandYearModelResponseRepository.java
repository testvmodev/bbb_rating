package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.response.bicycle.BicycleFromBrandYearModelResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleFromBrandYearModelResponseRepository extends JpaRepository<BicycleFromBrandYearModelResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.id AS bicycle_id, bicycle.description AS  description, " +
            "bicycle.name AS name, bicycle.brand_id AS brand_id, bicycle_brand.name AS name_name, " +
            "bicycle.brand_id AS brand_id, bicycle_brand.name AS brand_name, " +
            "bicycle.model_id AS model_id, bicycle_model.name AS model_name, " +
            "bicycle.year_id AS year_id, bicycle_year.name AS year_name, " +
            "bicycle.type_id AS type_id,  bicycle_type.name AS type_name," +
            "bicycle.size_id AS size_id, bicycle_size.name AS size_name," +
            "bicycle.brake_type_id AS brake_type_id, brake_type.name AS brake_type_name, " +
            "bicycle.image_default AS image_default " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "LEFT JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "LEFT JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "LEFT JOIN brake_type ON bicycle.brake_type_id = brake_type.id " +
            "WHERE " +
            "bicycle.active = 1 AND " +
            "bicycle.is_delete = 0 AND " +
            "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
            "(:yearId IS NULL OR bicycle.year_id = :yearId) AND " +
            "(:modelId IS NULL OR bicycle.model_id = :modelId) " +
            "ORDER BY bicycle.last_update OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<BicycleFromBrandYearModelResponse> findAllBicycle(
            @Param(value = "brandId") Long brandId,
            @Param(value = "yearId") Long yearId,
            @Param(value = "modelId") Long modelId,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "WHERE " +
            "bicycle.active = 1 AND " +
            "bicycle.is_delete = 0 AND " +
            "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
            "(:yearId IS NULL OR bicycle.year_id = :yearId) AND " +
            "(:modelId IS NULL OR bicycle.model_id = :modelId)"
    )
    int getTotalCountBrandYearModel(
            @Param(value = "brandId") Long brandId,
            @Param(value = "yearId") Long yearId,
            @Param(value = "modelId") Long modelId
    );


    @Query(nativeQuery = true, value = "SELECT  " +
            "bicycle.id AS bicycle_id, bicycle.description AS  description, " +
            "bicycle.name AS name, bicycle.brand_id AS brand_id, bicycle_brand.name AS name_name, " +
            "bicycle.brand_id AS brand_id, bicycle_brand.name AS brand_name, " +
            "bicycle.model_id AS model_id, bicycle_model.name AS model_name, " +
            "bicycle.year_id AS year_id, bicycle_year.name AS year_name, " +
            "bicycle.type_id AS type_id,  bicycle_type.name AS type_name," +
            "bicycle.size_id AS size_id, bicycle_size.name AS size_name," +
            "bicycle.brake_type_id AS brake_type_id, brake_type.name AS brake_type_name, " +
            "bicycle.image_default AS image_default " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "LEFT JOIN bicycle_size ON bicycle.size_id = bicycle_size.id " +
            "LEFT JOIN brake_type ON bicycle.brake_type_id = brake_type.id " +
            "WHERE bicycle.active = 1 AND " +
            "bicycle.is_delete = 0 AND " +
            "bicycle.name LIKE :content " +
            "ORDER BY bicycle.last_update OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<BicycleFromBrandYearModelResponse> findAllBicycle(
            @Param(value = "content") String content,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*)  " +
            "FROM " +
            "bicycle " +
            "WHERE bicycle.active = 1 AND " +
            "bicycle.name LIKE :content"
    )
    int getTotalCountContent(
            @Param(value = "content") String content
    );

}
