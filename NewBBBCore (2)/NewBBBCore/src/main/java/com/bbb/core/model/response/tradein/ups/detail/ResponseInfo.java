package com.bbb.core.model.response.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseInfo {
    @JsonProperty("ResponseStatus")
    private ResponseStatus responseStatus;

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }
}
