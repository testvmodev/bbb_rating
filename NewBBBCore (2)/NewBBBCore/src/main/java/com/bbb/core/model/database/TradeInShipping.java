package com.bbb.core.model.database;


import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.database.type.InboundShippingType;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_shipping")
public class TradeInShipping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long tradeInId;
    private Integer length;
    private Integer width;
    private Integer height;
    private Float weight;
    private String fromAddress;
    private String fromCity;
    private String fromState;
    private String fromZipCode;
    private String trackingNumber;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;

    private CarrierType carrierType;
    private Float totalCharge;
    private Float billingWeight;
    private String pathLabel;
    private String fullLinkLabel;
    private String carrier;
    private String carrierTrackingNumber;
    private InboundShippingType shippingType;
    private Boolean isValidTrackingNumber;
    private String msgInvalidTrackingNumber;
    private String warehouseId;
    private String warehouseAddress;
    private String warehouseCity;
    private String warehouseState;
    private String warehouseZipCode;
    private String warehouseCountryCode;
    private String fedexTrackingIdType;
    private Boolean isScheduled;
    private LocalDate scheduleDate;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }


    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getFromZipCode() {
        return fromZipCode;
    }

    public void setFromZipCode(String fromZipCode) {
        this.fromZipCode = fromZipCode;
    }


    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public Float getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(Float totalCharge) {
        this.totalCharge = totalCharge;
    }

    public Float getBillingWeight() {
        return billingWeight;
    }

    public void setBillingWeight(Float billingWeight) {
        this.billingWeight = billingWeight;
    }

    public String getPathLabel() {
        return pathLabel;
    }

    public void setPathLabel(String pathLabel) {
        this.pathLabel = pathLabel;
    }

    public String getFullLinkLabel() {
        return fullLinkLabel;
    }

    public void setFullLinkLabel(String fullLinkLabel) {
        this.fullLinkLabel = fullLinkLabel;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getCarrier() {
        return carrier;
    }

    public String getCarrierTrackingNumber() {
        return carrierTrackingNumber;
    }

    public void setCarrierTrackingNumber(String carrierTrackingNumber) {
        this.carrierTrackingNumber = carrierTrackingNumber;
    }

    public InboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(InboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public Boolean getValidTrackingNumber() {
        return isValidTrackingNumber;
    }

    public void setValidTrackingNumber(Boolean validTrackingNumber) {
        isValidTrackingNumber = validTrackingNumber;
    }

    public String getMsgInvalidTrackingNumber() {
        return msgInvalidTrackingNumber;
    }

    public void setMsgInvalidTrackingNumber(String msgInvalidTrackingNumber) {
        this.msgInvalidTrackingNumber = msgInvalidTrackingNumber;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseAddress() {
        return warehouseAddress;
    }

    public void setWarehouseAddress(String warehouseAddress) {
        this.warehouseAddress = warehouseAddress;
    }

    public String getWarehouseCity() {
        return warehouseCity;
    }

    public void setWarehouseCity(String warehouseCity) {
        this.warehouseCity = warehouseCity;
    }

    public String getWarehouseState() {
        return warehouseState;
    }

    public void setWarehouseState(String warehouseState) {
        this.warehouseState = warehouseState;
    }

    public String getWarehouseZipCode() {
        return warehouseZipCode;
    }

    public void setWarehouseZipCode(String warehouseZipCode) {
        this.warehouseZipCode = warehouseZipCode;
    }

    public String getWarehouseCountryCode() {
        return warehouseCountryCode;
    }

    public void setWarehouseCountryCode(String warehouseCountryCode) {
        this.warehouseCountryCode = warehouseCountryCode;
    }

    public String getFedexTrackingIdType() {
        return fedexTrackingIdType;
    }

    public void setFedexTrackingIdType(String fedexTrackingIdType) {
        this.fedexTrackingIdType = fedexTrackingIdType;
    }

    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
}
