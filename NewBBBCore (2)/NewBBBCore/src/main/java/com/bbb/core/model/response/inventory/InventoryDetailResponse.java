package com.bbb.core.model.response.inventory;

import com.bbb.core.model.database.type.OutboundShippingType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class InventoryDetailResponse extends InventoryBaseInfo {
    @Id
    private long id;
    private Boolean isAllowLocalPickup;
    private Boolean isInsurance;
    private Float flatRate;
    private OutboundShippingType shippingType;
    private String zipCode;
    private String countryName;
    private String countryCode;
    private String stateName;
    private String stateCode;
    private String cityName;
    private String county;
    private String sellerId;
    @Column(name = "seller_is_bbb")
    private Boolean sellerIsBBB;
    @Transient
    private List<String> images;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }

    public Boolean getInsurance() {
        return isInsurance;
    }

    public void setInsurance(Boolean insurance) {
        isInsurance = insurance;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Boolean getSellerIsBBB() {
        return sellerIsBBB;
    }

    public void setSellerIsBBB(Boolean sellerIsBBB) {
        this.sellerIsBBB = sellerIsBBB;
    }
}
