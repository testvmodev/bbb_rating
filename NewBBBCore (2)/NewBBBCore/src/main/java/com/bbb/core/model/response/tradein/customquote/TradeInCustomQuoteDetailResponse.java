package com.bbb.core.model.response.tradein.customquote;

import com.bbb.core.model.database.*;
import com.bbb.core.model.response.tradein.TradeInUpgradeCompResponse;

import java.util.List;

public class TradeInCustomQuoteDetailResponse extends TradeInCustomQuote {
    private String sizeName;
    private TradeIn tradeIn;
    private List<TradeInUpgradeCompResponse> upgradeComps;
    private List<TradeInPriceConfig> tradeInPriceConfigs;
    private List<TradeInCustomQuoteConditionResponse> conditions;
    private List<TradeInImage> tradeInImages;
    private List<TradeInCustomQuoteCompResponse> comps;
    private String typeName;


    public TradeIn getTradeIn() {
        return tradeIn;
    }

    public void setTradeIn(TradeIn tradeIn) {
        this.tradeIn = tradeIn;
    }

    public List<TradeInUpgradeCompResponse> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<TradeInUpgradeCompResponse> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public List<TradeInPriceConfig> getTradeInPriceConfigs() {
        return tradeInPriceConfigs;
    }

    public void setTradeInPriceConfigs(List<TradeInPriceConfig> tradeInPriceConfigs) {
        this.tradeInPriceConfigs = tradeInPriceConfigs;
    }

    public List<TradeInCustomQuoteConditionResponse> getConditions() {
        return conditions;
    }

    public void setConditions(List<TradeInCustomQuoteConditionResponse> conditions) {
        this.conditions = conditions;
    }

    public List<TradeInImage> getTradeInImages() {
        return tradeInImages;
    }

    public void setTradeInImages(List<TradeInImage> tradeInImages) {
        this.tradeInImages = tradeInImages;
    }

    public List<TradeInCustomQuoteCompResponse> getComps() {
        return comps;
    }

    public void setComps(List<TradeInCustomQuoteCompResponse> comps) {
        this.comps = comps;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
