package com.bbb.core.model.request.saleforce;

import com.bbb.core.model.request.saleforce.create.SalesforceAddBikeRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;


public class ListingFullCreateRequest {
    @JsonProperty("AddBikeRequest")
    @NotNull
    private SalesforceAddBikeRequest addBikeRequest;

    public SalesforceAddBikeRequest getAddBikeRequest() {
        return addBikeRequest;
    }

    public void setAddBikeRequest(SalesforceAddBikeRequest addBikeRequest) {
        this.addBikeRequest = addBikeRequest;
    }
}
