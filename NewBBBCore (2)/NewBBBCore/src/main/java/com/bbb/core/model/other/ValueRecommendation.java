package com.bbb.core.model.other;

import com.fasterxml.jackson.annotation.JsonProperty;
//import com.google.gson.annotations.SerializedName;

public class ValueRecommendation {
    @JsonProperty(value = "Value")
//    @SerializedName(value = "Value")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
