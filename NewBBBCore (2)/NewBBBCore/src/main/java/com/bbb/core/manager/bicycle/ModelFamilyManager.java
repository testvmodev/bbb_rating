package com.bbb.core.manager.bicycle;

import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.model.family.ModelFamily;
import com.bbb.core.model.response.model.family.ModelFamilyResponse;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicyclemodel.out.ModelFamilyRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ModelFamilyManager {
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private ModelFamilyRepository modelFamilyRepository;

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getModelFamilies(long brandId) {
        List<ModelFamily> modelFamilies = modelFamilyRepository.findAllModelFamilyId(brandId);
        Map<String, List<ModelFamily>> mapFamilies = modelFamilies.stream().collect(Collectors.groupingBy(o -> o.getId().getFirstId() + o.getModelName()));
        List<ContentCommonId> models =
                mapFamilies.entrySet().stream().map(o -> {
                    ContentCommonId content = new ContentCommonId();
                    content.setId(o.getValue().get(0).getId().getFirstId());
                    content.setName(o.getValue().get(0).getModelName());
                    return content;
                }).collect(Collectors.toList());
//        CommonUtils.stream(models, (o1, o2) -> Long.compare(o1.getId(), o2.getId()));

        List<ContentCommonId> modelFamiliess =
                models
                        .stream()
                        .filter(mo -> !StringUtils.isBlank(mo.getName()))
                        .peek(model -> {
                            while (model.getName().contains("  ")) {
                                model.setName(model.getName().replaceAll(" {2}", " "));
                            }
                            model.setName(model.getName().trim());

                            final String modelNameRoot = model.getName();
                            final String nameUp = modelNameRoot.toUpperCase();

                            if (model.getName().contains(" ")) {

                                String strRemove = null;
                                if (nameUp.contains(ValueCommons.S_WORKS)) {
                                    strRemove = ValueCommons.S_WORKS;
                                } else {
                                    if (nameUp.contains(ValueCommons.WOMEN_S)) {
                                        strRemove = ValueCommons.WOMEN_S;
                                    } else {
                                        if (nameUp.contains(ValueCommons.MEN_S)) {
                                            strRemove = ValueCommons.MEN_S;
                                        } else {
                                            if (nameUp.contains(ValueCommons.BOY_S)) {
                                                strRemove = ValueCommons.BOY_S;
                                            } else {
                                                if (nameUp.contains(ValueCommons.GIRL_S)) {
                                                    strRemove = ValueCommons.GIRL_S;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (strRemove != null) {
                                    if (strRemove.equals(ValueCommons.S_WORKS) || nameUp.indexOf(strRemove) == 0) {

                                        int indexWork = nameUp.indexOf(strRemove);
                                        String modelName = nameUp.substring(indexWork + strRemove.length()).trim();
                                        if (modelName.contains("+")) {
                                            modelName = modelName.replaceAll("\\+", "").trim();
                                        }
                                        if (modelName.contains(" ")) {
                                            int indexSpace = modelName.indexOf(' ');
                                            modelName = modelName.substring(0, indexSpace).trim();
                                        }
                                        model.setName(modelName);
                                    }
                                }
                                int indexSpace = model.getName().indexOf(' ');
                                if (indexSpace > 0) {
                                    model.setName(model.getName().substring(0, indexSpace).trim());
                                }
                            }
                            String modelNameUp = model.getName().toUpperCase();
                            int indexMo = nameUp.indexOf(modelNameUp);
                            model.setName(modelNameRoot.substring(indexMo, indexMo + modelNameUp.length()));
                        })
                        .filter(mo -> !StringUtils.isBlank(mo.getName()))
                        .collect(Collectors.toList());

        Map<String, List<ContentCommonId>> mapModel = modelFamiliess.stream().collect(Collectors.groupingBy(co -> co.getName().toUpperCase()));
        List<ModelFamilyResponse> familyResponses = new ArrayList<>();
        mapModel.forEach((family, ms) -> {
            ModelFamilyResponse response = new ModelFamilyResponse();
            ContentCommonId id = CommonUtils.findObject(modelFamiliess, mo -> mo.getName().toUpperCase().equals(family));
            if (id != null) {
                if (id.getName().equals("Women's")) {
                    System.out.println("test");
                }
                response.setFamilyName(id.getName());
            } else {
                response.setFamilyName(family);
            }
            response.setCountFamily(ms.size());

            List<Long> modelIds = ms.stream().map(ContentCommonId::getId).collect(Collectors.toList());
            List<ContentCommonId> years =
                    modelFamilies.stream().filter(o -> modelIds.contains(o.getId().getFirstId())).map(o -> {
                        ContentCommonId yearContent = new ContentCommonId();
                        yearContent.setId(o.getId().getSecondId());
                        yearContent.setName(o.getYearName());
                        return yearContent;
                    }).collect(Collectors.toList());
            CommonUtils.stream(years, (o1, o2)->Long.compare(o1.getId(), o2.getId()));
            years.sort(Comparator.comparingLong(ContentCommonId::getId));
            response.setYears(years);



            familyResponses.add(response);
        });
        familyResponses.sort(Comparator.comparing(ModelFamilyResponse::getFamilyName));
        return familyResponses;
    }
}
