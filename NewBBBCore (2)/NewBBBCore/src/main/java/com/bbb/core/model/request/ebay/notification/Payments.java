package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Payments {
    @JacksonXmlProperty(localName = "Payment")
    private Payment payment;

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
