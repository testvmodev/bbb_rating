package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class Payment {
    @JacksonXmlProperty(localName = "PaymentStatus")
    private String paymentStatus;
    @JacksonXmlProperty(localName = "Payer")
    private TypeObject payer;
    @JacksonXmlProperty(localName = "Payee")
    private TypeObject payee;
    @JacksonXmlProperty(localName = "PaymentTime")
    private LocalDateTime paymentTime;
    @JacksonXmlProperty(localName = "PaymentAmount")
    private Currency paymentAmount;
    @JacksonXmlProperty(localName = "ReferenceID")
    private TypeObject referenceID;
    @JacksonXmlProperty(localName = "FeeOrCreditAmount")
    private Currency feeOrCreditAmount;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public TypeObject getPayer() {
        return payer;
    }

    public void setPayer(TypeObject payer) {
        this.payer = payer;
    }

    public TypeObject getPayee() {
        return payee;
    }

    public void setPayee(TypeObject payee) {
        this.payee = payee;
    }

    public LocalDateTime getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(LocalDateTime paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Currency getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Currency paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public TypeObject getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(TypeObject referenceID) {
        this.referenceID = referenceID;
    }

    public Currency getFeeOrCreditAmount() {
        return feeOrCreditAmount;
    }

    public void setFeeOrCreditAmount(Currency feeOrCreditAmount) {
        this.feeOrCreditAmount = feeOrCreditAmount;
    }
}
