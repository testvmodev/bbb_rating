package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.response.bicycle.BicycleBaseInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface InventoryBaseInfoRepository extends JpaRepository<BicycleBaseInfo, Long> {


    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
            "bicycle.id AS bicycle_id, " +
            "bicycle.model_id AS bicycle_model_id, bicycle_model.name AS bicycle_model_name," +
            "bicycle.brand_id AS bicycle_brand_id, bicycle_brand.name AS bicycle_brand_name, " +
            "bicycle.year_id AS bicycle_year_id, bicycle_year.name AS bicycle_year_name, " +
            "bicycle.type_id AS bicycle_type_id, bicycle_type.name AS bicycle_type_name " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "WHERE bicycle.id = :bicycleId"
    )
    BicycleBaseInfo findOneFromBicycleId(
            @Param(value = "bicycleId") long bicycleId
    );
}
