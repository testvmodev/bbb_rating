package com.bbb.core.model.response.brandmodelyear;


import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
public class BrandModelYear {
//    @Transient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long brandId;
    private Long modelId;
    private Long yearId;
    private String brandName;
    private String modelName;
    private String yearName;

    @Transient
    private boolean bicycleMightExist;

    //just fake id for an entity. no need getter
//    public Long getId() {
//        return id;
//    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public boolean isBicycleMightExist() {
        return bicycleMightExist;
    }

    public void setBicycleMightExist(boolean bicycleMightExist) {
        this.bicycleMightExist = bicycleMightExist;
    }
}
