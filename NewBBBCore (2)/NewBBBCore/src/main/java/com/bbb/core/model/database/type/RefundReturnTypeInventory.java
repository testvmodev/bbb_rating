package com.bbb.core.model.database.type;

public enum RefundReturnTypeInventory {
    RETURN("return"),
    REFUND("refund");

    private final String value;

    RefundReturnTypeInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static RefundReturnTypeInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "return":
                return RETURN;
            case "refund":
                return REFUND;
            default:
                return null;
        }
    }
}
