package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.BrandManager;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.brand.BrandUpdateRequest;
import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BrandServiceImpl implements BrandService {
    private BrandManager manager;

    @Autowired
    public BrandServiceImpl(BrandManager manager) {
        this.manager = manager;
    }

    @Override
    public Object getBrands(String nameSearch, Sort sortType, BicycleBrandSortField sortField, Pageable pageable) throws ExceptionResponse {
        return manager.getBrands(nameSearch,sortType, sortField, pageable);
    }

    @Override
    public Object createBrand(BrandCreateRequest brand) throws ExceptionResponse {
        return manager.createBrand(brand);
    }

    @Override
    public Object deleteBrand(long id) throws ExceptionResponse {
        return manager.deleteBrand(id);
    }

    @Override
    public Object getBrandDetail(long brandId) throws ExceptionResponse {
        return manager.getBrandDetail(brandId);
    }

    @Override
    public Object updateBrand(long brandId, BrandUpdateRequest brand) throws ExceptionResponse {
        return manager.updateBrand(brandId, brand);
    }

    @Override
    public Object deleteLogoBrand(long id) throws ExceptionResponse {
        return manager.deleteLogoBrand(id);
    }

    @Override
    public Object getAllBrands() throws ExceptionResponse {
        return manager.getAllBrands();
    }
}
