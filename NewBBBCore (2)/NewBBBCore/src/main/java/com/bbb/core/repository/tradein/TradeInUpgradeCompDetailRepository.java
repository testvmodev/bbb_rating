package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInUpgradeCompDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TradeInUpgradeCompDetailRepository extends JpaRepository<TradeInUpgradeCompDetail, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_upgrade_comp_detail where trade_in_upgrade_comp_detail.trade_in_id = :tradeInId")
    List<TradeInUpgradeCompDetail> findAllByTradeInId(
            @Param(value = "tradeInId") long tradeInId
    );

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(nativeQuery = true, value = "DELETE trade_in_upgrade_comp_detail WHERE trade_in_upgrade_comp_detail.trade_in_id = :tradeInId")
    void deleteByTradeInId(
            @Param(value = "tradeInId") long tradeInId
    );

    @Query(nativeQuery = true, value = "SELECT DISTINCT trade_in_upgrade_comp_detail.trade_in_upgrade_comp_id FROM trade_in_upgrade_comp_detail where trade_in_upgrade_comp_detail.trade_in_id = :tradeInId")
    List<Long> findAllIdByTradeInId(
            @Param(value = "tradeInId") long tradeInId
    );
}
