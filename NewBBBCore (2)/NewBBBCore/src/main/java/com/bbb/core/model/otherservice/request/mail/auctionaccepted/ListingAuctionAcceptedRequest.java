package com.bbb.core.model.otherservice.request.mail.auctionaccepted;

public class ListingAuctionAcceptedRequest {
    private BikeAuctionAcceptedRequest bike;

    public BikeAuctionAcceptedRequest getBike() {
        return bike;
    }

    public void setBike(BikeAuctionAcceptedRequest bike) {
        this.bike = bike;
    }
}
