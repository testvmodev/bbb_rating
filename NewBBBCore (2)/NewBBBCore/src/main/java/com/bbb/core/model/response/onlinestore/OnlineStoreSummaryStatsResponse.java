package com.bbb.core.model.response.onlinestore;

import com.bbb.core.model.response.onlinestore.dashboard.OnlineStoreDashboardListingStatsEmbed;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class OnlineStoreSummaryStatsResponse {
    @Id
    private String storefrontId;
    private OnlineStoreDashboardListingStatsEmbed listings;
    private int openOffers;

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public OnlineStoreDashboardListingStatsEmbed getListings() {
        return listings;
    }

    public void setListings(OnlineStoreDashboardListingStatsEmbed listings) {
        this.listings = listings;
    }

    public int getOpenOffers() {
        return openOffers;
    }

    public void setOpenOffers(int openOffers) {
        this.openOffers = openOffers;
    }
}
