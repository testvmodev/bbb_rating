package com.bbb.core.repository.onlinestore.market;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.sort.OnlineStoreMyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.onlinestore.OnlineStoreListingResponse;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OnlineStoreListingResponseRepository extends JpaRepository<OnlineStoreListingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT my_listing.* FROM " +
            "(SELECT " +
            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +

            "market_listing.time_listed AS posting_time, " +
            "market_listing.is_best_offer AS is_best_offer, " +
            "inventory.title AS title, " +
            "market_listing.status AS status_market_listing, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.image_default AS image_default, " +

            //fake draft columns
            "NULL AS draft_id, " +
            "NULL AS content, " +

            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "bicycle.id AS bicycle_id, " +
            "inventory_bicycle.type_id AS bicycle_type_id, " +

            "inventory.bicycle_size_name AS size_name, " +
            "bicycle.name AS bicycle_name," +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +

            "inventory.status AS status_inventory, " +
            "inventory.stage AS stage_inventory, " +
            "inventory.name AS inventory_name, " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +

            "total_view.views AS views, " +
            "total_offer.offers AS offers " +
            
            "FROM market_listing " +

            "JOIN inventory " +
                "ON market_listing.inventory_id = inventory.id " +
                "AND inventory.storefront_id = :#{#storefrontId} " +
                "AND inventory.is_delete = 0 " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle_type.name = inventory.bicycle_type_name " +

            "CROSS APPLY (" +
                "SELECT ISNULL(SUM(tracking.count_tracking), 0) AS views " +
                "FROM tracking " +
                "WHERE tracking.market_listing_id = market_listing.id " +
            ") AS total_view " +

            "CROSS APPLY (" +
                "SELECT COUNT(offer.id) AS offers " +
                "FROM offer " +
                "WHERE offer.market_listing_id = market_listing.id " +
                "AND offer.is_delete = 0 " +
            ") AS total_offer " +


            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "JOIN inventory_comp_type " +
                "ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
            "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                "inventory_comp_detail.inventory_id AS inventory_id, " +
                "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
            "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                "inventory_comp_detail.inventory_id AS inventory_id, " +
                "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
            "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            "WHERE market_listing.market_place_id = :marketPlaceId " +
            "AND (:content IS NULL OR (" +
                "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), inventory.current_listed_price) LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), market_listing.id) LIKE :#{'%' + #content + '%'})) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                "market_listing.status IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapListingStatusToString(#statuses)}) " +
            "AND (:fromDay IS NULL OR CAST(market_listing.time_listed AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(market_listing.time_listed AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) " +

            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.time_listed END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN market_listing.time_listed END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).OFFER_COUNT.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN total_offer.offers END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).OFFER_COUNT.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN total_offer.offers END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).VIEWS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN total_view.views END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).VIEWS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN total_view.views END DESC " +

            "OFFSET 0 ROWS FETCH NEXT :#{#offset + #size} ROWS ONLY " +

            "UNION ALL " +

            "SELECT " +

            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +

            "my_listing_draft.last_update AS posting_time, " +
            "ISNULL(my_listing_draft.is_best_offer, CAST(0 AS BIT)) AS is_best_offer, " +
            "my_listing_draft.title AS title, " +
            ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} AS status_market_listing, " +
            "my_listing_draft.current_listed_price AS current_listed_price, " +
            "my_listing_image_draft.image AS image_default, " +

            "my_listing_draft.id AS draft_id, " +
            "my_listing_draft.content AS content, " +

            //fake listing columns
            "NULL AS market_listing_id, " +
            "NULL AS inventory_id, " +
            "NULL AS bicycle_id, " +
            "NULL AS bicycle_type_id, " +

            "NULL AS size_name, " +
            "NULL AS bicycle_name," +
            "NULL AS bicycle_brand_name, " +
            "NULL AS bicycle_model_name, " +
            "NULL AS bicycle_year_name, " +
            "NULL AS bicycle_type_name, " +

            "NULL AS status_inventory, " +
            "NULL AS stage_inventory, " +
            "NULL AS inventory_name, " +
            "NULL AS type_inventory_id, " +
            "NULL AS type_inventory_name, " +
            "NULL AS condition, " +
            "NULL AS serial_number, " +

            "NULL AS views, " +
            "NULL AS offers " +

            "FROM my_listing_draft " +

            "OUTER APPLY " +
            "(SELECT TOP 1 * FROM my_listing_image_draft " +
                "WHERE my_listing_image_draft.listing_draft_id = my_listing_draft.id) " +
            "AS my_listing_image_draft " +

            "WHERE my_listing_draft.storefront_id = :storefrontId " +
            "AND my_listing_draft.is_delete = 0 " +
            "AND (:content IS NULL OR ((" +
                "my_listing_draft.title IS NOT NULL " +
                "AND my_listing_draft.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'}) " +
                "OR CONVERT(NVARCHAR(255), my_listing_draft.current_listed_price) LIKE :#{'%' + #content + '%'} " +
                ")" +
            ") " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapListingStatusToString(#statuses)}) " +
            "AND (:fromDay IS NULL OR CAST(my_listing_draft.last_update AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(my_listing_draft.last_update AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) " +

            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing_draft.last_update END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
            "THEN my_listing_draft.last_update END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.title, '') END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.title, '') END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.current_listed_price, 0) END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.current_listed_price, 0) END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN is_best_offer END DESC " +

            "OFFSET 0 ROWS FETCH NEXT :#{#offset + #size} ROWS ONLY " +

            ") AS my_listing " +


            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.market_listing_id END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.market_listing_id END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                //TODO spring-data bug
//                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()}) " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing.posting_time END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = 'ID') " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing.posting_time END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.title END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.title END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.current_listed_price END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.current_listed_price END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.status_market_listing END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.status_market_listing END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.is_best_offer END DESC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).OFFER_COUNT.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.offers END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).OFFER_COUNT.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.offers END DESC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).VIEWS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.views END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.OnlineStoreMyListingSortField).VIEWS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.views END DESC " +

            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#size} ROWS ONLY"
    )
    List<OnlineStoreListingResponse> findListingsSorted(
            @Param("storefrontId") String storefrontId,
            @Param("marketPlaceId") Long bbbMarketPlaceId,
            @Param("content") String content,
            @Param("statuses") List<StatusMarketListing> statuses,
            @Param("fromDay") LocalDate fromDay,
            @Param("toDay") LocalDate toDay,
            @Param("sortField") OnlineStoreMyListingSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("offset") long offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(my_listing.id) FROM " +
            "(SELECT " +
            //fake id
            "ABS(CHECKSUM(NewId())) AS id " +

            "FROM market_listing " +

            "JOIN inventory " +
                "ON market_listing.inventory_id = inventory.id " +
                "AND inventory.storefront_id = :#{#storefrontId} " +
                "AND inventory.is_delete = 0 " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle_type.name = inventory.bicycle_type_name " +

            "CROSS APPLY (" +
                "SELECT ISNULL(SUM(tracking.count_tracking), 0) AS views " +
                "FROM tracking " +
                "WHERE tracking.market_listing_id = market_listing.id " +
            ") AS total_view " +

            "CROSS APPLY (" +
                "SELECT COUNT(offer.id) AS offers " +
                "FROM offer " +
                "WHERE offer.market_listing_id = market_listing.id " +
                "AND offer.is_delete = 0 " +
            ") AS total_offer " +


//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS size_name " +
//                "FROM inventory_comp_detail " +
//                "JOIN inventory_comp_type " +
//                "ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
//            "AS frame_size " +
//            "ON inventory.id = frame_size.inventory_id " +
//
//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS brake_type_name " +
//                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
//            "AS brake_type " +
//            "ON inventory.id = brake_type.inventory_id " +
//
//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS fragment_material_name " +
//                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
//                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
//            "AS frame_material " +
//            "ON inventory.id = frame_material.inventory_id " +

            "WHERE market_listing.market_place_id = :marketPlaceId " +
            "AND (:content IS NULL OR (" +
                "inventory.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), inventory.current_listed_price) LIKE :#{'%' + #content + '%'} OR " +
                "CONVERT(NVARCHAR(255), market_listing.id) LIKE :#{'%' + #content + '%'})) " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                "market_listing.status IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapListingStatusToString(#statuses)}) " +
            "AND (:fromDay IS NULL OR CAST(market_listing.time_listed AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(market_listing.time_listed AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) " +


            "UNION ALL " +


            "SELECT " +

            //fake id
            "ABS(CHECKSUM(NewId())) AS id " +

            "FROM my_listing_draft " +

            "OUTER APPLY " +
            "(SELECT TOP 1 * FROM my_listing_image_draft " +
            "WHERE my_listing_image_draft.listing_draft_id = my_listing_draft.id) " +
            "AS my_listing_image_draft " +

            "WHERE my_listing_draft.storefront_id = :storefrontId " +
            "AND my_listing_draft.is_delete = 0 " +
            "AND (:content IS NULL OR ((" +
                "my_listing_draft.title IS NOT NULL " +
                "AND my_listing_draft.title collate SQL_Latin1_General_CP1_CI_AS LIKE :#{'%' + #content + '%'}) " +
                "OR CONVERT(NVARCHAR(255), my_listing_draft.current_listed_price) LIKE :#{'%' + #content + '%'} " +
                ")" +
            ") " +
            "AND (1 = :#{@queryUtils.isEmptyList(#statuses) ? 1 : 0} OR " +
                ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} IN :#{@queryUtils.isEmptyList(#statuses) ? @queryUtils.fakeStrings() :@queryUtils.mapListingStatusToString(#statuses)}) " +
            "AND (:fromDay IS NULL OR CAST(my_listing_draft.last_update AS DATE) >= :#{#fromDay == null ? @queryUtils.getCurrentDate() : #fromDay}) " +
            "AND (:toDay IS NULL OR CAST(my_listing_draft.last_update AS DATE) <= :#{#toDay == null ? @queryUtils.getCurrentDate() : #toDay}) " +

            ") AS my_listing "
    )
    int countListings(
            @Param("storefrontId") String storefrontId,
            @Param("marketPlaceId") Long bbbMarketPlaceId,
            @Param("content") String content,
            @Param("statuses") List<StatusMarketListing> statuses,
            @Param("fromDay") LocalDate fromDay,
            @Param("toDay") LocalDate toDay
    );
}
