package com.bbb.core.model.database;


public class UserSession {

  private long id;
  private long deviceType;
  private java.sql.Timestamp lastLogin;
  private String token;
  private String deviceToken;
  private long userId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(long deviceType) {
    this.deviceType = deviceType;
  }


  public java.sql.Timestamp getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(java.sql.Timestamp lastLogin) {
    this.lastLogin = lastLogin;
  }


  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }


  public String getDeviceToken() {
    return deviceToken;
  }

  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }


  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

}
