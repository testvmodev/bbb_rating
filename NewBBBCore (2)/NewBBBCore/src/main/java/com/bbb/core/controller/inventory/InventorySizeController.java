package com.bbb.core.controller.inventory;

import com.bbb.core.common.Constants;
import com.bbb.core.service.inventory.InventorySizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventorySizeController {
    @Autowired
    private InventorySizeService service;

    @GetMapping(value = Constants.URL_INVENTORY_SIZE)
    public ResponseEntity getAllInventorySize() {
        return new ResponseEntity<>(service.getAllInventorySize(),
                HttpStatus.OK);
    }
}
