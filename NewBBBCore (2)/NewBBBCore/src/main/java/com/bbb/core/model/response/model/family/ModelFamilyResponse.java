package com.bbb.core.model.response.model.family;


import com.bbb.core.model.response.embedded.ContentCommonId;

import java.util.List;

public class ModelFamilyResponse {
    private String familyName;
    private int countFamily;
    private List<ContentCommonId> years;

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public int getCountFamily() {
        return countFamily;
    }

    public void setCountFamily(int countFamily) {
        this.countFamily = countFamily;
    }

    public List<ContentCommonId> getYears() {
        return years;
    }

    public void setYears(List<ContentCommonId> years) {
        this.years = years;
    }
}
