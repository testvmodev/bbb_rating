package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dimension {
    @JsonProperty("UnitOfMeasurement")
    private UnitOfMeasurement unitOfMeasurement;
    @JsonProperty("Length")
    private String length;
    @JsonProperty("Width")
    private String width;
    @JsonProperty("Height")
    private String height;

    public Dimension(UnitOfMeasurement unitOfMeasurement, String length, String width, String height) {
        this.unitOfMeasurement = unitOfMeasurement;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public UnitOfMeasurement getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public String getLength() {
        return length;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }
}
