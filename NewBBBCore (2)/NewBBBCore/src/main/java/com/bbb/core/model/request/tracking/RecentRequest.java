package com.bbb.core.model.request.tracking;

import com.bbb.core.model.database.type.MarketType;
import org.joda.time.LocalDateTime;

import javax.validation.constraints.NotNull;

public class RecentRequest {
    @NotNull
    private Long id;

    @NotNull
    private LocalDateTime timeViewed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getTimeViewed() {
        return timeViewed;
    }

    public void setTimeViewed(LocalDateTime timeViewed) {
        this.timeViewed = timeViewed;
    }
}
