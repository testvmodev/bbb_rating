package com.bbb.core.model.response.tradein.ups.detail.fault;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FaultDetail {
    @JsonProperty(value = "Errors")
    private FaultErrors errors;

    public FaultErrors getErrors() {
        return errors;
    }

    public void setErrors(FaultErrors errors) {
        this.errors = errors;
    }
}
