package com.bbb.core.controller.bid;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.request.bid.offer.*;
import com.bbb.core.model.request.bid.offer.offerbid.OfferBidRequest;
import com.bbb.core.model.request.bid.offer.offerbid.OffersReceiverRequest;
import com.bbb.core.model.request.bid.offer.test.OfferType;
import com.bbb.core.model.request.sort.*;
import com.bbb.core.service.bid.OfferService;
import io.swagger.annotations.*;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class OfferController {
    @Autowired
    private OfferService service;

    @GetMapping(value = Constants.URL_GET_OFFERS)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getOffers(
//            @RequestParam(value = "buyerId", required = false) String buyerId,
            @RequestParam(value = "status", required = false) StatusOffer status,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @RequestParam(value = "marketListingId", required = false) Long marketListingId,
            @RequestParam(value = "fieldSort", required = true, defaultValue = "CREATED_DATE") OfferSort fieldSort,
            @RequestParam(value = "sort", required = true, defaultValue = "DESC") Sort sort,
            Pageable page
    ) {
        return new ResponseEntity<>(service.getOffers(
                new OfferRequest(status, inventoryAuctionId, marketListingId, sort, fieldSort), page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_OFFERS_RECEIVER_MY_LISTING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getOffersReceiverMyListing(
            @RequestParam(value = "fieldSort",  defaultValue = "CREATED_DATE") OfferReceiverFieldSort fieldSort,
            @RequestParam(value = "sort", defaultValue = "DESC") Sort sort,
            Pageable page
    ) throws ExceptionResponse{
        return new ResponseEntity<>(
                service.getOffersReceiverMyListing(new OffersReceiverRequest(fieldSort, sort),  page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_OFFERS_ONLINE_STORE_RECEIVED)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getOffersOnlineStore(
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "statuses", required = false) List<StatusOffer> statuses,
            @RequestParam(value = "fromDay", required = false) Long fromDay,
            @RequestParam(value = "toDay", required = false) Long toDay,
            @RequestParam(value = "fieldSort",  defaultValue = "LAST_UPDATE") OfferReceiverFieldSort fieldSort,
            @RequestParam(value = "sortType", defaultValue = "DESC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        OffersOnlineStoreRequest request = new OffersOnlineStoreRequest();
        request.setContent(content);
        request.setStatuses(statuses);
        LocalDateTime from = CommonUtils.unixTimestampToLocalDateTime(fromDay);
        request.setFromDay(from != null ? from.toLocalDate() : null);
        LocalDateTime to = CommonUtils.unixTimestampToLocalDateTime(toDay);
        request.setToDay(to != null ? to.toLocalDate() : null);
        request.setFieldSort(fieldSort);
        request.setSortType(sortType);
        request.setPageable(pageable);

        return ResponseEntity.ok(service.getOffersOnlineStore(request));
    }

    @GetMapping(value = Constants.URL_GET_OFFERS_MARKET_LISTING)
    public ResponseEntity getOffersMarketListing(
            @PathVariable(Constants.ID) long marketListingId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getOffersMarketListing(marketListingId));
    }


    @PostMapping(value = Constants.URL_OFFER_CREATE)
    public ResponseEntity createOffer(
//            @RequestParam(value = "buyerId") String buyerId,
            @RequestParam(value = "offerPrice") float offerPrice,
            @ApiParam("@Deprecated, use marketListingId")
            @RequestParam(value = "inventoryId", required = false) Long inventoryId,
            @ApiParam("@Deprecated, use inventoryAuctionId")
            @RequestParam(value = "auctionId", required = false) Long auctionId,
            @RequestParam(value = "inventoryAuctionId", required = false) Long inventoryAuctionId,
            @RequestParam(value = "marketListingId", required = false) Long marketListingId
    ) throws ExceptionResponse {
        OfferCreateRequest request = new OfferCreateRequest();
//        request.setBuyerId(buyerId);
        request.setBuyerId(CommonUtils.getUserLogin());
        request.setOfferPrice(offerPrice);
        request.setInventoryId(inventoryId);
        request.setAuctionId(auctionId);
        request.setInventoryAuctionId(inventoryAuctionId);
        request.setMarketListingId(marketListingId);

        return new ResponseEntity<>(service.createOffer(
                request),
                HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_OFFER)
    public ResponseEntity updateOffer(
            @PathVariable(value = Constants.ID) long offerId,
            @RequestBody OfferUpdateRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateOffer(
                offerId,
                request
        ),
                HttpStatus.OK);
    }


//    @PutMapping(value = Constants.URL_OFFER_BUYER)
//    public ResponseEntity updateOfferBuyer(
//            @PathVariable(value = Constants.ID) long offerId,
//            @RequestParam(value = "isAccepted") boolean isAccepted
//    ) throws ExceptionResponse{
//        return new ResponseEntity<>(service.updateOfferBuyer(
//                offerId,
//                isAccepted
//        ),
//                HttpStatus.OK);
//    }

    @GetMapping(value = Constants.URL_OFFER)
    public ResponseEntity getDetailOffer(
            @PathVariable(value = Constants.ID) long offerId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getDetailOffer(offerId),
                HttpStatus.OK);
    }

    @PatchMapping(Constants.URL_OFFER_UPDATE_PAID)
    public ResponseEntity payOfferAccepted(
            @PathVariable(Constants.ID) long offerId,
            @RequestParam("isPaid") boolean isPaid
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.payOfferAccepted(offerId, isPaid));
    }

    @PostMapping(value = Constants.URL_SECRET_MARKET_LISTING_INVENTORY_AUCTIONS)
    public ResponseEntity getMarketListingAuctionsService(
            @RequestBody List<SecretMarketLisitingAuctionRequest> requests
            ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getMarketListingAuctionsService(
                requests
        ),
                HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_OFFER_BIDS)
    public ResponseEntity getOffersBid(
            @RequestParam(value = "statusBids", required = false) List<StatusOffer> statusBids,
            @RequestParam(value = "inventoryIds", required = false) List<Long> inventoryIds,
            @RequestParam(value = "sortType", required = false, defaultValue = "DESC") Sort sortType,
            @RequestParam(value = "sortField", required = false, defaultValue = "CREATED_TIME") InventoryOfferFieldSort sortField,
            @RequestParam(value = "isWin", required = false) Boolean isWin,
            Pageable page) throws ExceptionResponse {
        OfferBidRequest request = new OfferBidRequest();
        request.setStatusBids(statusBids);
        request.setInventoryIds(inventoryIds);
        request.setSortType(sortType);
        request.setSortField(sortField);
        request.setWin(isWin);
        return new ResponseEntity<>(service.getOffersBid(request, page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_OFFER_BID)
    public ResponseEntity getOfferBid(
            @PathVariable(value = Constants.ID) long inventoryAuctionId,
            @RequestParam(value = "fieldSort", required = false, defaultValue = "CREATED_TIME")OfferBidFieldSort offerBidFieldSort,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") Sort sortType

            ) throws ExceptionResponse {

        return new ResponseEntity<>(service.getOfferBid(inventoryAuctionId, offerBidFieldSort, sortType),
                HttpStatus.OK);
    }

    @ApiOperation(value = "Inventory auction's bids", notes = "Get all bids of a inventory auction")
    @GetMapping(Constants.URL_OFFERS_OF_INVENTORY_AUCTION)
    public ResponseEntity getInventoryAuctionBids(
            @ApiParam(value = "Id of inventory auction")
            @PathVariable(value = Constants.ID) long inventoryAuctionId,
            @RequestParam(value = "fieldSort", required = false, defaultValue = "CREATED_TIME")OfferBidFieldSort offerBidFieldSort,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") Sort sortType
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getInventoryAuctionBids(inventoryAuctionId, offerBidFieldSort, sortType));
    }

    @PostMapping(Constants.URL_SECRET_OFFER_INTEGRATION_TEST)
    public ResponseEntity offerIntegrationTest(
            @RequestParam(value = "offerType") OfferType offerType
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.offerIntegrationTest(offerType));
    }

    @DeleteMapping(value = Constants.URL_OFFER_CART)
    public ResponseEntity removeCartOffer(
            @PathVariable(value = Constants.ID) long offerId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.removeCartOffer(offerId));
    }

    @GetMapping(Constants.URL_USER_OFFERS)
    @ApiOperation(value = "getUserOffers", notes = "Get all offers user received or made")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getUserOffers(
            @ApiParam(value = "user id")
            @PathVariable(value = Constants.ID) String userId,
            @RequestParam(value = "sortField", defaultValue = "CREATED_DATE") UserOfferSort sortField,
            @RequestParam(value = "sortType", defaultValue = "DESC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        UserOfferRequest request = new UserOfferRequest();
        request.setUserId(userId);
        request.setSortField(sortField);
        request.setSortType(sortType);
        request.setPageable(pageable);

        return ResponseEntity.ok(service.getUserOffers(request));
    }

    @GetMapping(Constants.URL_USER_BIDS)
    @ApiOperation(value = "getUserBids", notes = "Get all offers user bids and those inventory auction")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getUserBids(
            @ApiParam(value = "user id")
            @PathVariable(value = Constants.ID) String userId,
            Pageable pageable
    ) throws ExceptionResponse {
        UserBidRequest request = new UserBidRequest();
        request.setUserId(userId);
        request.setPageable(pageable);
        return ResponseEntity.ok(service.getUserBids(request));
    }

    @GetMapping(Constants.URL_GET_OFFER_SECRET)
    public ResponseEntity getOfferDetailSecret(
            @PathVariable(value = Constants.ID) long offerId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getOfferDetailSecret(offerId));
    }
}
