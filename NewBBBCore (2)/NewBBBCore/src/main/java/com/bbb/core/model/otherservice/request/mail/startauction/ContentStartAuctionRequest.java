package com.bbb.core.model.otherservice.request.mail.startauction;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ContentStartAuctionRequest {
    private AuctionStartRequest auction;
    private List<ListingStartAuctionRequest> listings;
    @JsonProperty("base_path")
    private String basePath;
    public AuctionStartRequest getAuction() {
        return auction;
    }

    public void setAuction(AuctionStartRequest auction) {
        this.auction = auction;
    }

    public List<ListingStartAuctionRequest> getListings() {
        return listings;
    }

    public void setListings(List<ListingStartAuctionRequest> listings) {
        this.listings = listings;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
