package com.bbb.core.manager.templaterest;

import com.bbb.core.common.Constants;
import org.springframework.stereotype.Component;

@Component
public class RestLogServiceManager extends RestTemplateManager {
    public RestLogServiceManager() {
        super(Constants.URL_LOG_SERVICE_BASE);
    }
}
