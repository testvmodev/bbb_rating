package com.bbb.core.model.request.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingChargesPayment {
    @JacksonXmlProperty(localName = "PaymentType")
    private String paymentType;
    @JacksonXmlProperty(localName = "Payor")
    private Payor payor;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Payor getPayor() {
        return payor;
    }

    public void setPayor(Payor payor) {
        this.payor = payor;
    }
}
