package com.bbb.core.model.response.model;

public class BicycleModelResponse {
    private long bicycleId;
    private String bicycleName;
    private String bicycleImageDefault;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleImageDefault() {
        return bicycleImageDefault;
    }

    public void setBicycleImageDefault(String bicycleImageDefault) {
        this.bicycleImageDefault = bicycleImageDefault;
    }
}
