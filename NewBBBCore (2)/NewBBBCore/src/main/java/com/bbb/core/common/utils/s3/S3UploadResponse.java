package com.bbb.core.common.utils.s3;

public class S3UploadResponse<T> {
    private String fullKey;
    private T otherData;

    public S3UploadResponse(String fullKey, T otherData) {
        this.fullKey = fullKey;
        this.otherData = otherData;
    }

    public S3UploadResponse() {
    }

    public String getFullKey() {
        return fullKey;
    }

    public void setFullKey(String fullKey) {
        this.fullKey = fullKey;
    }

    public T getOtherData() {
        return otherData;
    }

    public void setOtherData(T otherData) {
        this.otherData = otherData;
    }
}
