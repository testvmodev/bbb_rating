package com.bbb.core.service.tradein;

import com.bbb.core.model.database.TradeInImageType;

import java.util.List;

public interface TradeInImageTypeService {
    List<TradeInImageType> getImagesTypes();
}
