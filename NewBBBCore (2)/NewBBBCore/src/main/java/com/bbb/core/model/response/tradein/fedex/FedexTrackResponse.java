package com.bbb.core.model.response.tradein.fedex;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.response.tradein.fedex.detail.track.CompletedTrackDetails;
import com.bbb.core.model.response.tradein.fedex.detail.Notification;
import com.bbb.core.model.response.tradein.fedex.detail.type.Severity;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "ProcessShipmentReply", namespace = FedExValue.TRACK_ROOT_NAMESPACE)
public class FedexTrackResponse {
    @JacksonXmlProperty(localName = "HighestSeverity")
    private String highestSeverityRaw;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Notifications")
    private List<Notification> notifications;
    @JacksonXmlProperty(localName = "CompletedTrackDetails")
    private CompletedTrackDetails completedTrackDetails;

    public Severity getHighestSeverity() {
        return Severity.findByValue(highestSeverityRaw);
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public CompletedTrackDetails getCompletedTrackDetails() {
        return completedTrackDetails;
    }

    public void setCompletedTrackDetails(CompletedTrackDetails completedTrackDetails) {
        this.completedTrackDetails = completedTrackDetails;
    }

    public boolean isSuccess() {
        try {
            return getHighestSeverity() == Severity.SUCCESS
                    && completedTrackDetails.hasTrackDetail();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isDelivered() {
        return completedTrackDetails.hasTrackDetail()
                && completedTrackDetails.getTrackDetails().isDelivered();
    }
}
