package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class ShippingMyAccountRequest implements MessageResponses {
    private String carrier;
    private String carrierTrackingNumber;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getCarrierTrackingNumber() {
        return carrierTrackingNumber;
    }

    public void setCarrierTrackingNumber(String carrierTrackingNumber) {
        this.carrierTrackingNumber = carrierTrackingNumber;
    }


    public boolean checkValid() throws ExceptionResponse {
        if (StringUtils.isBlank(carrier)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, SELF_CARRIER_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (StringUtils.isBlank(carrierTrackingNumber)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, SELF_NUMBER_TRACKING_MUST_IS_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        return true;
    }
}
