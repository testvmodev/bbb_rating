package com.bbb.core.model.request.tradein.fedex.detail;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.request.tradein.fedex.detail.ship.FedexLabelSpecification;
import com.bbb.core.model.request.tradein.fedex.detail.ship.ShippingChargesPayment;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class RequestedShipment {
    private LocalDateTime shipTimestamp;
    @JacksonXmlProperty(localName = "ShipTimestamp")
    private String shipTimestampString;
    @JacksonXmlProperty(localName = "DropoffType")
    private String dropoffType;
    @JacksonXmlProperty(localName = "ServiceType")
    private String serviceType;
    @JacksonXmlProperty(localName = "PackagingType")
    private String packagingType;
    @JacksonXmlProperty(localName = "Shipper")
    private Shipper shipper;
    @JacksonXmlProperty(localName = "Recipient")
    private Recipient recipient;
    @JacksonXmlProperty(localName = "ShippingChargesPayment")
    private ShippingChargesPayment shippingChargesPayment;
    @JacksonXmlProperty(localName = "LabelSpecification")
    private FedexLabelSpecification fedexLabelSpecification;
    @JacksonXmlProperty(localName = "PackageCount")
    private int packageCount = FedExValue.PACKAGE_PER_SHIP_DEFAULT;
    @JacksonXmlProperty(localName = "RequestedPackageLineItems")
    private RequestedPackageLineItems requestedPackageLineItems;

//    public LocalDateTime getShipTimestamp() {
//        return shipTimestamp;
//    }

    public void setShipTimestamp(LocalDateTime shipTimestamp) {
        this.shipTimestamp = shipTimestamp;
    }

    public String getShipTimestampString() {
        return shipTimestamp.toString();
    }

    public String getDropoffType() {
        return dropoffType;
    }

    public void setDropoffType(String dropoffType) {
        this.dropoffType = dropoffType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public ShippingChargesPayment getShippingChargesPayment() {
        return shippingChargesPayment;
    }

    public void setShippingChargesPayment(ShippingChargesPayment shippingChargesPayment) {
        this.shippingChargesPayment = shippingChargesPayment;
    }

    public FedexLabelSpecification getFedexLabelSpecification() {
        return fedexLabelSpecification;
    }

    public void setFedexLabelSpecification(FedexLabelSpecification fedexLabelSpecification) {
        this.fedexLabelSpecification = fedexLabelSpecification;
    }

    public int getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(int packageCount) {
        this.packageCount = packageCount;
    }

    public RequestedPackageLineItems getRequestedPackageLineItems() {
        return requestedPackageLineItems;
    }

    public void setRequestedPackageLineItems(RequestedPackageLineItems requestedPackageLineItems) {
        this.requestedPackageLineItems = requestedPackageLineItems;
    }
}
