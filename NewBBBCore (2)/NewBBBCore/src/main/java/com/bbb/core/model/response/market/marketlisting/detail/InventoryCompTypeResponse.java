package com.bbb.core.model.response.market.marketlisting.detail;

import com.bbb.core.model.database.embedded.InventoryCompDetailId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class InventoryCompTypeResponse {
    @EmbeddedId
    private InventoryCompDetailId id;
    private String name;
    private String value;

    public InventoryCompDetailId getId() {
        return id;
    }

    public void setId(InventoryCompDetailId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
