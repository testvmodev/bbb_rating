package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PackageWeight {
    @JsonProperty("UnitOfMeasurement")
    private UnitOfMeasurement unitOfMeasurement;
    @JsonProperty("Weight")
    private String weight;

    public PackageWeight(UnitOfMeasurement unitOfMeasurement, String weight) {
        this.unitOfMeasurement = unitOfMeasurement;
        this.weight = weight;
    }

    public UnitOfMeasurement getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public String getWeight() {
        return weight;
    }
}
