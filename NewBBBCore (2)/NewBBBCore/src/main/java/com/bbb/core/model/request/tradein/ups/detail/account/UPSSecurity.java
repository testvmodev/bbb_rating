package com.bbb.core.model.request.tradein.ups.detail.account;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UPSSecurity {
    @JsonProperty("UsernameToken")
    private UsernameToken usernameToken;
    @JsonProperty("ServiceAccessToken")
    private ServiceAccessToken serviceAccessToken;

    public UPSSecurity(String username, String password, String accessLicenseNumber) {
        this.usernameToken = new UsernameToken(username, password);
        this.serviceAccessToken = new ServiceAccessToken(accessLicenseNumber);
    }

    public UsernameToken getUsernameToken() {
        return usernameToken;
    }

    public void setUsernameToken(UsernameToken usernameToken) {
        this.usernameToken = usernameToken;
    }

    public ServiceAccessToken getServiceAccessToken() {
        return serviceAccessToken;
    }

    public void setServiceAccessToken(ServiceAccessToken serviceAccessToken) {
        this.serviceAccessToken = serviceAccessToken;
    }
}
