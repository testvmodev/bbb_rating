package com.bbb.core.repository.market.out.mylisting;

import com.bbb.core.model.response.market.mylisting.MyListingDraftDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MyListingDraftDetailResponseRepository extends JpaRepository<MyListingDraftDetailResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM my_listing_draft " +
            "WHERE my_listing_draft.id = :myListingDraftId " +
            "AND is_delete = 0")
    MyListingDraftDetailResponse findOne(
            @Param("myListingDraftId") long myListingDraftId
    );
}
