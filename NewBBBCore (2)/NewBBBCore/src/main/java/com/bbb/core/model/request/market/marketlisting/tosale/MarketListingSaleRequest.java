package com.bbb.core.model.request.market.marketlisting.tosale;

import com.bbb.core.model.database.type.UserRoleType;

public class MarketListingSaleRequest {
    private Long marketListingId;
    private Long offerId;
    private String buyerDisplayName;
    private String buyerId;
    private String buyerEmail;
    private UserRoleType buyerRole;
    private String orderId;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getBuyerDisplayName() {
        return buyerDisplayName;
    }

    public void setBuyerDisplayName(String buyerDisplayName) {
        this.buyerDisplayName = buyerDisplayName;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public UserRoleType getBuyerRole() {
        return buyerRole;
    }

    public void setBuyerRole(UserRoleType buyerRole) {
        this.buyerRole = buyerRole;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
