package com.bbb.core.model.response.ebay;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Fees {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Fee")
    private List<Fee> Fees;

    public List<Fee> getFees() {
        return Fees;
    }

    public void setFees(List<Fee> fees) {
        Fees = fees;
    }
}
