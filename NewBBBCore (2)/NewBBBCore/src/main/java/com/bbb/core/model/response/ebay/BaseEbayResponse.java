package com.bbb.core.model.response.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

import java.util.List;

public class BaseEbayResponse {
    @JacksonXmlProperty(localName = "Timestamp")
    private LocalDateTime timestamp;
    @JacksonXmlProperty(localName = "Ack")
    private String ack;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Errors")
    private List<Error> errors;
    @JacksonXmlProperty(localName = "Version")
    private int version;
    @JacksonXmlProperty(localName = "Build")
    private String build;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }
}
