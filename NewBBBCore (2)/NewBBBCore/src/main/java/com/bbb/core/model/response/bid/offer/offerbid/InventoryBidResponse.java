package com.bbb.core.model.response.bid.offer.offerbid;

import com.bbb.core.model.database.type.StatusInventory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryBidResponse {
    @Id
    private long inventoryAuctionId;
    private long offerId;
    private String inventoryId;
    private long auctionId;
    private String inventoryTitle;
    private String imageDefault;
    private Float maxBid;
    private int numberBidOfAccount;
    private long leftTime;
    private StatusInventory status;
    private Float buyItNow;
    private Boolean isWinner;
    @JsonIgnore
    private String winnerId;
    private LocalDateTime endDate;
    private LocalDateTime startDate;
    private String inventoryName;
    private String title;
    private String sellerId;
    private String partnerId;
    private Float currentListedPrice;



    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Float getMaxBid() {
        return maxBid;
    }

    public void setMaxBid(Float maxBid) {
        this.maxBid = maxBid;
    }

    public int getNumberBidOfAccount() {
        return numberBidOfAccount;
    }

    public void setNumberBidOfAccount(int numberBidOfAccount) {
        this.numberBidOfAccount = numberBidOfAccount;
    }

    public long getLeftTime() {
        return leftTime;
    }

    public void setLeftTime(long leftTime) {
        this.leftTime = leftTime;
    }
    public Boolean getWinner() {
        return isWinner;
    }

    public void setWinner(Boolean winner) {
        isWinner = winner;
    }


    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public Float getBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(Float buyItNow) {
        this.buyItNow = buyItNow;
    }

    public long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }



    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
