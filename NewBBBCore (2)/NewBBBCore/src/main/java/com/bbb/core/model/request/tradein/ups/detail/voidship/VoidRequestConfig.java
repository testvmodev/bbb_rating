package com.bbb.core.model.request.tradein.ups.detail.voidship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VoidRequestConfig {
    @JsonProperty("TransactionReference")
    private TransactionReference transactionReference;

    public VoidRequestConfig() {}

    public VoidRequestConfig(String customerContext) {
        transactionReference = new TransactionReference(customerContext);
    }

    public TransactionReference getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(TransactionReference transactionReference) {
        this.transactionReference = transactionReference;
    }
}
