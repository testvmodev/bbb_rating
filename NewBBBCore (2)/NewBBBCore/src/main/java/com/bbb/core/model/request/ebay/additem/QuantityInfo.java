package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class QuantityInfo {
    @JacksonXmlProperty(localName = "MinimumRemnantSet")
    private int minimumRemnantSet;

    public int getMinimumRemnantSet() {
        return minimumRemnantSet;
    }

    public void setMinimumRemnantSet(int minimumRemnantSet) {
        this.minimumRemnantSet = minimumRemnantSet;
    }
}
