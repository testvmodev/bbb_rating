package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingServiceOptions {
    @JacksonXmlProperty(localName = "FreeShipping")
    private boolean freeShipping;
    @JacksonXmlProperty(localName = "ShippingService")
    private String shippingService;
    @JacksonXmlProperty(localName = "ShippingServicePriority")
    private int shippingServicePriority;

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getShippingService() {
        return shippingService;
    }

    public void setShippingService(String shippingService) {
        this.shippingService = shippingService;
    }

    public int getShippingServicePriority() {
        return shippingServicePriority;
    }

    public void setShippingServicePriority(int shippingServicePriority) {
        this.shippingServicePriority = shippingServicePriority;
    }
}
