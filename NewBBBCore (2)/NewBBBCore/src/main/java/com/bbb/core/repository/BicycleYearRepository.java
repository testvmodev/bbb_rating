package com.bbb.core.repository;

import com.bbb.core.model.database.BicycleYear;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BicycleYearRepository extends CrudRepository<BicycleYear, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM bicycle_year " +
            "WHERE id = ?1 AND is_delete = 0")
    BicycleYear findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM bicycle_year " +
            "WHERE (:yearName IS NULL OR bicycle_year.name LIKE  :yearName)")
    List<BicycleYear> findAllName(@Param(value = "yearName") String yearName);


    BicycleYear findFirstByName(String name);

}
