package com.bbb.core.model.request.onlinestore;

import org.joda.time.LocalDate;

public class OnlineStoreSaleReportDetailRequest {
    private LocalDate fromDay;
    private LocalDate toDay;

    public LocalDate getFromDay() {
        return fromDay;
    }

    public void setFromDay(LocalDate fromDay) {
        this.fromDay = fromDay;
    }

    public LocalDate getToDay() {
        return toDay;
    }

    public void setToDay(LocalDate toDay) {
        this.toDay = toDay;
    }
}
