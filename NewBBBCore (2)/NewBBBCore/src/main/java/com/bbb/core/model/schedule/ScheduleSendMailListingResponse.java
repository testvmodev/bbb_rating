package com.bbb.core.model.schedule;

import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;

public class ScheduleSendMailListingResponse {
    private long marketListingId;
    private SendingMailResponse sendingMailResponse;
    private String rawErrorResponse;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public SendingMailResponse getSendingMailResponse() {
        return sendingMailResponse;
    }

    public void setSendingMailResponse(SendingMailResponse sendingMailResponse) {
        this.sendingMailResponse = sendingMailResponse;
    }

    public String getRawErrorResponse() {
        return rawErrorResponse;
    }

    public void setRawErrorResponse(String rawErrorResponse) {
        this.rawErrorResponse = rawErrorResponse;
    }
}
