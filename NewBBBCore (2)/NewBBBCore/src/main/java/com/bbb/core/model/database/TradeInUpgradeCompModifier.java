package com.bbb.core.model.database;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TradeInUpgradeCompModifier {
    @Id
    private long tradeInUpgradeCompId;
    private Float minMsrp;
    private Float maxMsrp;
    private Float percentModifier;


    public long getTradeInUpgradeCompId() {
        return tradeInUpgradeCompId;
    }

    public void setTradeInUpgradeCompId(long tradeInUpgradeCompId) {
        this.tradeInUpgradeCompId = tradeInUpgradeCompId;
    }

    public Float getMinMsrp() {
        return minMsrp;
    }

    public void setMinMsrp(Float minMsrp) {
        this.minMsrp = minMsrp;
    }

    public Float getMaxMsrp() {
        return maxMsrp;
    }

    public void setMaxMsrp(Float maxMsrp) {
        this.maxMsrp = maxMsrp;
    }

    public Float getPercentModifier() {
        return percentModifier;
    }

    public void setPercentModifier(Float percentModifier) {
        this.percentModifier = percentModifier;
    }
}
