package com.bbb.core.model.request.brand;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class BrandCreateRequest {
    private String name;
    private MultipartFile brandLogo;
    private Float valueModifier;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(MultipartFile brandLogo) {
        this.brandLogo = brandLogo;
    }

    public boolean validateBrand() {
        return !StringUtils.isBlank(name);
    }

    public Float getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(Float valueModifier) {
        this.valueModifier = valueModifier;
    }
}
