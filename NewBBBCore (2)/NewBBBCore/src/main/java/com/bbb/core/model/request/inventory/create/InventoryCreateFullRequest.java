package com.bbb.core.model.request.inventory.create;

import javax.validation.constraints.NotNull;
import java.util.List;

public class InventoryCreateFullRequest {
    @NotNull
    private InventoryBaseCreateRequest baseInfo;
    @NotNull
    private InventoryLocationShippingCreateRequest location;
    //shipping now moved to shipment service
//    private InventoryShippingCreatedRequest shipping;
    private List<InventoryUpgradeCompCreateRequest> upgradeComps;
    private List<InventoryCompDetailCreateRequest> comps;

    public InventoryBaseCreateRequest getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(InventoryBaseCreateRequest baseInfo) {
        this.baseInfo = baseInfo;
    }

//    public InventoryShippingCreatedRequest getShipping() {
//        return shipping;
//    }
//
//    public void setShipping(InventoryShippingCreatedRequest shipping) {
//        this.shipping = shipping;
//    }

    public InventoryLocationShippingCreateRequest getLocation() {
        return location;
    }

    public void setLocation(InventoryLocationShippingCreateRequest location) {
        this.location = location;
    }

    public List<InventoryUpgradeCompCreateRequest> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<InventoryUpgradeCompCreateRequest> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public List<InventoryCompDetailCreateRequest> getComps() {
        return comps;
    }

    public void setComps(List<InventoryCompDetailCreateRequest> comps) {
        this.comps = comps;
    }
}
