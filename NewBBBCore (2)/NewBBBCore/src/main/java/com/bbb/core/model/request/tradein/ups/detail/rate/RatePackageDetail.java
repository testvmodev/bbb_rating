package com.bbb.core.model.request.tradein.ups.detail.rate;

import com.bbb.core.model.request.tradein.ups.detail.Dimension;
import com.bbb.core.model.request.tradein.ups.detail.PackageType;
import com.bbb.core.model.request.tradein.ups.detail.PackageWeight;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RatePackageDetail {
    @JsonProperty("PackagingType")
    private PackageType packageType;
    @JsonProperty("Dimensions")
    private Dimension dimensions;
    @JsonProperty("PackageWeight")
    private PackageWeight packageWeight;

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public Dimension getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    public PackageWeight getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(PackageWeight packageWeight) {
        this.packageWeight = packageWeight;
    }
}
