package com.bbb.core.model.request.bid.offer.offerbid;

import com.bbb.core.model.request.sort.OfferReceiverFieldSort;
import com.bbb.core.model.request.sort.Sort;

public class OffersReceiverRequest {
    private OfferReceiverFieldSort fieldSort;
    private Sort sortType;

    public OffersReceiverRequest(OfferReceiverFieldSort fieldSort, Sort sortType) {
        this.fieldSort = fieldSort;
        this.sortType = sortType;
    }

    public OfferReceiverFieldSort getFieldSort() {
        return fieldSort;
    }

    public void setFieldSort(OfferReceiverFieldSort fieldSort) {
        this.fieldSort = fieldSort;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

}
