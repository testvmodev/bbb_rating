package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Weight {
    @JacksonXmlProperty(localName = "Units")
    private String units;
    @JacksonXmlProperty(localName = "Value")
    private float value;

    public Weight() {}

    public Weight(String units, float value) {
        this.units = units;
        this.value = value;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
