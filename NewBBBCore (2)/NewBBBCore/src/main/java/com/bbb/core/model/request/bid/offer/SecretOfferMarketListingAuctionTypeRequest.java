package com.bbb.core.model.request.bid.offer;

public enum SecretOfferMarketListingAuctionTypeRequest {
    AUCTION, MARKET_LISTING, OFFER
}
