package com.bbb.core.service.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.listingwizard.ListingWizardType;
import com.bbb.core.model.request.market.marketlisting.CreateMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingInvToSalRequest;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ListingWizardService {
    Object getListingWizards(ListingWizardType type, List<MarketListingInvToSalRequest> request, Pageable page) throws ExceptionResponse;
    Object createMarketListing(CreateMarketListingRequest requests) throws ExceptionResponse;

    Object deListMarketListing(List<Long> marketListingIds) throws ExceptionResponse;

    Object startAsyncMarketListing(List<Long> marketListingIds, HttpServletRequest servletRequest) throws ExceptionResponse;
    Object removeQueueMarketListing(List<Long> marketListingIds) throws ExceptionResponse;
}
