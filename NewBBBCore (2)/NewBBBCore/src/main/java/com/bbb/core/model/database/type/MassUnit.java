package com.bbb.core.model.database.type;

public enum MassUnit {
    G("g"),
    KG("kg"),
    LBS("lbs");

    private final String value;

    MassUnit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MassUnit findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "g":
                return G;
            case "kg":
                return KG;
            case "lbs":
                return LBS;
            default:
                return null;
        }
    }
}
