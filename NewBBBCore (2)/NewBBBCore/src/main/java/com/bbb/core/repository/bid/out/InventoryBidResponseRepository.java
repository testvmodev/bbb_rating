package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.offer.offerbid.InventoryBidResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryBidResponseRepository extends JpaRepository<InventoryBidResponse,Long> {
//    private long inventoryAuctionId;
//    private String inventoryId;
//    private long auctionId;
//    private String inventoryTitle;
//    private String imageDefault;
//    private int numberBidOfAccount;
//    private long leftTime;
//    private StatusInventory status;
//    private Float buyItNow;
    @Query(nativeQuery = true, value =

                        "SELECT " +
                        "inventory_auction.id AS inventory_auction_id, " +
                        "inventory_auction.inventory_id AS inventory_id, " +
                        "inventory.title AS inventory_title, " +
                        "inventory.image_default AS image_default, " +
                        "inventory_auction_user.count_bid_user AS number_bid_of_account, " +
                        "inventory_auction_user.max_created_time AS max_created_time, " +
                        "inventory.status AS status, " +
                        "inventory_auction.buy_it_now AS buy_it_now, " +
                        "inventory_auction.auction_id AS auction_id, " +
                        "inventory_auction.winner_id AS winner_id, " +
                        "max_offer.max_price_offer AS max_bid," +
                         "left_time_auction.left_time AS left_time, " +
                        "(CASE WHEN inventory_auction.winner_id IS NULL THEN NULL ELSE (CASE WHEN inventory_auction.winner_id = :buyerId THEN 1 ELSE 0 END) END) AS is_winner, " +
                         "auction.end_date AS end_date," +
                         "auction.start_date AS start_date, " +
                         "inventory_auction_user.max_offer_id AS offer_id, " +
                         "inventory.name AS inventory_name, " +
                         "inventory.title AS title, " +
                         "inventory.seller_id AS seller_id, " +
                         "inventory.partner_id AS partner_id, " +
                         "inventory.current_listed_price AS current_listed_price " +
                        "FROM " +
                            "(SELECT " +
                            "offer.inventory_auction_id AS  inventory_auction_id, " +
                            "MAX(offer.created_time) AS max_created_time, " +
                            "MAX(offer.id) AS max_offer_id, " +
                            "COUNT(offer.id) AS count_bid_user " +
                            "FROM " +
                            "offer " +
                            "JOIN " +
                            "inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                            "WHERE " +
                            "offer.buyer_id = :buyerId AND " +
                            "offer.is_delete = 0 AND " +
                            "(:isInventoryNull = 1 OR inventory_auction.inventory_id IN :inventoryIds) AND " +
                            "(:isStatusBidNull = 1 OR offer.status IN :statusBids) AND " +
                            "(:isWinner IS NULL OR " +
                            "(:isWinner = 1 AND inventory_auction.winner_id = :buyerId) OR " +
                            "(:isWinner = 0 AND ((inventory_auction.winner_id IS NULL) OR (inventory_auction.winner_id IS NOT NULL AND inventory_auction.winner_id <> :buyerId) ))) " +
                            "GROUP BY offer.inventory_auction_id ) " +
                            "AS inventory_auction_user " +
                        "JOIN  " +
                            "(SELECT " +
                            "offer.inventory_auction_id AS inventory_auction_id, " +
                            "MAX(offer.offer_price) AS max_price_offer, " +
                            "COUNT(offer.inventory_auction_id) AS count_offer " +
                            "FROM " +
                            "offer " +
                            "WHERE " +
                            "offer.inventory_auction_id IS NOT NULL AND " +
                            "offer.is_delete = 0 " +
                            "GROUP BY " +
                            "offer.inventory_auction_id) " +
                            "AS max_offer "+
                        "ON inventory_auction_user.inventory_auction_id = max_offer.inventory_auction_id " +
//                        "LEFT JOIN  " +
//                             "(SELECT " +
//                             "offer.inventory_auction_id AS inventory_auction_id, " +
//                             "MAX(offer.offer_price) AS max_price_offer " +
//                             "FROM " +
//                             "offer " +
//                             "WHERE " +
//                             "offer.inventory_auction_id IS NOT NULL AND " +
//                             "offer.is_delete = 0 AND " +
//                             "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                             "GROUP BY " +
//                             "offer.inventory_auction_id) " +
//                             "AS max_price_offer "+
//                        "ON inventory_auction_user.inventory_auction_id = max_price_offer.inventory_auction_id " +
                        "JOIN " +
                                "inventory_auction ON inventory_auction_user.inventory_auction_id = inventory_auction.id  " +
                        "JOIN " +
                                "auction ON inventory_auction.auction_id = auction.id "+
                        "JOIN " +
                                "inventory ON inventory_auction.inventory_id = inventory.id " +
                         "JOIN " +
                                "(SELECT (CASE WHEN DATEDIFF_BIG(MILLISECOND, :now, auction.end_date) < 0 THEN 0 ELSE DATEDIFF_BIG(MILLISECOND, :now, auction.end_date) END ) AS left_time, auction.id as auction_id FROM auction) AS left_time_auction " +
                                "ON inventory_auction.auction_id = left_time_auction.auction_id "+


                        "ORDER BY " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).ID.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory_auction.inventory_id END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).ID.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory_auction.inventory_id END ASC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory.title END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TITLE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory.title END ASC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TIME_LEFT.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN left_time_auction.left_time END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TIME_LEFT.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN left_time_auction.left_time END ASC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).MAX_BID_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN max_offer.max_price_offer END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).MAX_BID_PRICE.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN max_offer.max_price_offer END ASC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TOTAL_BID_USER.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory_auction_user.count_bid_user END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).TOTAL_BID_USER.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory_auction_user.count_bid_user END ASC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).CREATED_TIME.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN inventory_auction_user.max_created_time END DESC, " +
                        "CASE WHEN :fieldSort = :#{T(com.bbb.core.model.request.sort.InventoryOfferFieldSort).CREATED_TIME.name()} AND :sort = :#{T(com.bbb.core.model.request.sort.Sort).ASC.name()} THEN inventory_auction_user.max_created_time END ASC, " +
                        "CASE WHEN :isWinNotNull = 1  AND :isWinner = 1 THEN auction.end_date END DESC, "   +
                         "inventory_auction_user.max_created_time DESC " +
                        "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY "


    )
    List<InventoryBidResponse> findAllAuctionInventory(
            @Param(value = "buyerId") String buyerId,
            @Param(value = "now") String now,
            @Param(value = "isInventoryNull") boolean isInventoryNull,
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "isStatusBidNull") boolean isStatusBidNull,
            @Param(value = "statusBids") List<String> statusBids,
            @Param(value = "isWinNotNull") boolean isWinNotNull,
            @Param(value = "isWinner") Boolean isWinner,
            @Param(value = "fieldSort")String fieldSort,
            @Param(value = "sort")String sort,
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );


    @Query(nativeQuery = true, value =

            "SELECT COUNT(inventory_auction.id) " +
                    "FROM " +
                        "(SELECT " +
                        "offer.inventory_auction_id AS  inventory_auction_id, " +
                        "MAX(offer.created_time) AS max_created_time " +
                        "FROM " +
                        "offer " +
                        "JOIN " +
                        "inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
                        "WHERE " +
                        "offer.buyer_id = :buyerId AND " +
                        "offer.is_delete = 0 AND " +
                        "(:isInventoryNull = 1 OR inventory_auction.inventory_id IN :inventoryIds) AND " +
                        "(:isStatusBidNull = 1 OR offer.status IN :statusBids) AND " +
                        "(:isWinner IS NULL OR (:isWinner = 1 AND inventory_auction.winner_id = :buyerId) OR " +
                        "(:isWinner = 0 AND ((inventory_auction.winner_id IS NULL) OR (inventory_auction.winner_id IS NOT NULL AND inventory_auction.winner_id <> :buyerId) ))) " +
                        "GROUP BY offer.inventory_auction_id ) " +
                        "AS inventory_auction_user " +
//                    "JOIN  " +
//                        "(SELECT " +
//                        "offer.inventory_auction_id AS inventory_auction_id, " +
//                        "MAX(offer.offer_price) AS max_price_offer, " +
//                        "COUNT(offer.inventory_auction_id) AS count_offer " +
//                        "FROM " +
//                        "offer " +
//                        "WHERE " +
//                        "offer.inventory_auction_id IS NOT NULL " +
//                        "GROUP BY " +
//                        "offer.inventory_auction_id) " +
//                        "AS max_offer "+
//                    "ON inventory_auction_user.inventory_auction_id = max_offer.inventory_auction_id " +
                    "JOIN " +
                        "inventory_auction ON inventory_auction_user.inventory_auction_id = inventory_auction.id  " +
                    "JOIN " +
                        "auction ON inventory_auction.auction_id = auction.id "+
                    "JOIN " +
                        "inventory ON inventory_auction.inventory_id = inventory.id"


    )
    int countAuctionInventory(
            @Param(value = "buyerId") String buyerId,
            @Param(value = "isInventoryNull") boolean isInventoryNull,
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "isStatusBidNull") boolean isStatusBidNull,
            @Param(value = "statusBids") List<String> statusBids,
            @Param(value = "isWinner") Boolean isWinner
    );

//    @Query(nativeQuery = true, value =
//            "SELECT TOP 1 " +
//            "inventory_auction.id AS inventory_auction_id, " +
//            "inventory_auction.inventory_id AS inventory_id, " +
//            "inventory.title AS inventory_title, " +
//            "inventory.image_default AS image_default, " +
//            "inventory_auction_user.count_bid_user AS number_bid_of_account, " +
//            "offer.created_time AS max_created_time, " +
//            "inventory.status AS status, " +
//            "inventory_auction.buy_it_now AS buy_it_now, " +
//            "inventory_auction.auction_id AS auction_id, " +
//            "inventory_auction.winner_id AS winner_id, " +
////            "max_offer.max_price_offer AS max_bid," +
//            "DATEDIFF_BIG(MILLISECOND, :now, auction.end_date) AS left_time, " +
//            "(CASE WHEN inventory_auction.winner_id IS NULL THEN NULL ELSE (CASE WHEN inventory_auction.winner_id = :userId THEN 1 ELSE 0 END) END) AS is_winner, " +
//            "auction.end_date AS end_date," +
//            "auction.start_date AS start_date, " +
//            "offer.id AS offer_id, " +
//            "inventory.name AS inventory_name, " +
//            "inventory.title AS title " +
//            "FROM " +
//            "offer " +
//             "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
//             "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
//              "JOIN auction ON inventory_auction.auction_id = auction.id " +
//              "JOIN " +
//                    "(SELECT offer.inventory_auction_id AS inventory_auction_id, COUNT(offer.id) AS count_bid_user, MAX(offer.offer_price) AS max_price_offer FROM offer WHERE offer.buyer_id = : userId GROUP BY offer.inventory_auction_id ) AS inventory_auction_user " +
//                    "ON offer.inventory_auction_id = inventory_auction_user.inventory_auction_id " +
//            "WHERE " +
//             "offer.id = :offerId AND " +
//             "offer.buyer_id = :userId"
//    )
//    InventoryBidResponse findOne(
//            @Param(value = "offerId") long offerId,
//            @Param(value = "userId") String userId,
//            @Param(value = "now") String now
//    );
}
