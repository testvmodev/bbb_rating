package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StageInventory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StageInventoryConverter implements AttributeConverter<StageInventory, String> {
    @Override
    public String convertToDatabaseColumn(StageInventory stageInventory) {
        if (stageInventory == null) {
            return null;
        }
        return stageInventory.getValue();
    }

    @Override
    public StageInventory convertToEntityAttribute(String value) {
        return StageInventory.findByValue(value);
    }
}
