package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.FavouriteType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class FavouriteTypeConverter implements AttributeConverter<FavouriteType, String> {
    @Override
    public String convertToDatabaseColumn(FavouriteType favouriteType) {
        if (favouriteType == null) return null;
        return favouriteType.getValue();
    }

    @Override
    public FavouriteType convertToEntityAttribute(String s) {
        return FavouriteType.findByValue(s);
    }
}
