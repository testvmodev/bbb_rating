package com.bbb.core.repository.market.out.mylisting;

import com.bbb.core.model.database.MyListingImageDraft;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.market.mylisting.MyListingResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MyListingResponseRepository extends JpaRepository<MyListingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "(" +
            "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.model_id  AS model_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.year_id AS year_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
            "bicycle.type_id AS type_id, " +
            "inventory.serial_number AS serial_number, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS market_type, " +

            "inventory.image_default AS image_default, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.partner_id AS partner_id, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "inventory.condition AS condition, " +
            "frame_size.size_name AS size_name, " +
            "inventory.status AS status_inventory, " +
            "inventory.stage AS stage_inventory, " +

            //sort
            "inventory.title AS title, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "market_listing.is_best_offer AS is_best_offer, " +
            "market_listing.status AS status_market_listing, " +

            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +

            //fake columns of draft
            "NULL AS draft_id, " +
            "NULL AS content, " +

            //sort
            "market_listing.time_listed AS posting_time " +

            "FROM market_listing " +


            "JOIN inventory " +
                "ON market_listing.inventory_id = inventory.id " +
                "AND inventory.seller_id = :#{#userID} " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "JOIN inventory_comp_type " +
                "ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +


            "WHERE market_listing.market_place_id = :#{#marketPlaceId} " +
            "AND market_listing.is_delete = 0 " +
//            "AND market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.name()} " +

            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.time_listed END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.time_listed END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END DESC " +

            "OFFSET 0 ROWS FETCH NEXT :#{#offset + #size} ROWS ONLY " +


            "UNION ALL " +


            "SELECT " +

            //fake columns of market listing
            "NULL AS market_listing_id, " +
            "NULL AS inventory_id, " +
            "NULL AS bicycle_id, " +
            "NULL AS model_id, " +
            "NULL AS brand_id, " +
            "NULL AS year_id, " +
            "NULL AS type_id, " +
            "NULL AS serial_number, " +
            "NULL AS bicycle_name, " +
            "NULL AS bicycle_model_name, " +
            "NULL AS bicycle_brand_name, " +
            "NULL AS bicycle_year_name, " +
            "NULL AS bicycle_type_name, " +
            "NULL AS market_place_id, " +
            "NULL AS market_type, " +

            //image from draft
            "my_listing_image_draft.image AS image_default, " +

            //fake columns of market listing
            "NULL AS brake_type_id, " +
            "NULL AS brake_name, " +
            "NULL AS frame_material_id, " +
            "NULL AS frame_material_name, " +

//            "NULL AS is_best_offer, " +
            "NULL AS partner_id, " +
            "NULL AS inventory_name,  " +
            "NULL AS type_inventory_id, " +
            "NULL AS type_inventory_name, " +
            "NULL AS msrp_price, " +
            "NULL AS bbb_value, " +
            "NULL AS override_trade_in_price, " +
            "NULL AS initial_list_price, " +
            "NULL AS cogs_price, " +
            "NULL AS flat_price_change, " +
            "NULL AS discounted_price, " +
            "NULL AS best_offer_auto_accept_price, " +
            "NULL AS minimum_offer_auto_accept_price, " +
            "NULL AS condition, " +
            "NULL AS size_name, " +
            "NULL AS status_inventory, " +
            "NULL AS stage_inventory, " +

            //sort
            "my_listing_draft.title AS title, " +
            "my_listing_draft.current_listed_price AS current_listed_price, " +
            "ISNULL(my_listing_draft.is_best_offer, CAST(0 AS BIT)) AS is_best_offer, " +
            ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} AS status_market_listing, " +

            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +


            //draft
            "my_listing_draft.id AS draft_id, " +
            "my_listing_draft.content AS content, " +
            //sort
            "my_listing_draft.last_update AS posting_time " +

            "FROM my_listing_draft " +

            "OUTER APPLY " +
                "(SELECT TOP 1 * FROM my_listing_image_draft " +
                "WHERE my_listing_image_draft.listing_draft_id = my_listing_draft.id) " +
            "AS my_listing_image_draft " +

            "WHERE my_listing_draft.is_delete = 0 " +
            "AND my_listing_draft.created_by = :#{#userID} " +

            "ORDER BY " +

//            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
//                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
//                "THEN my_listing_draft.id END ASC , " +
//            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
//                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
//                "THEN my_listing_draft.id END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing_draft.last_update END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing_draft.last_update END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.title, '') END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.title, '') END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.current_listed_price, 0) END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN ISNULL(my_listing_draft.current_listed_price, 0) END DESC, " +

            //doesnt need to sort, all status here is draft
//            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
//                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
//                "THEN my_listing.status_market_listing END, " +
//            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
//                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
//                "THEN my_listing.status_market_listing END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN is_best_offer END DESC " +

            "OFFSET 0 ROWS FETCH NEXT :#{#offset + #size} ROWS ONLY " +

            ") AS my_listing " +


            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.market_listing_id END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.market_listing_id END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing.posting_time END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "OR :#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()}) " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} " +
                "THEN my_listing.posting_time END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.title END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.title END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.current_listed_price END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.current_listed_price END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.status_market_listing END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.status_market_listing END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN my_listing.is_best_offer END DESC " +

            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#size} ROWS ONLY"
    )
    List<MyListingResponse> findByUser(
            @Param("userID") String userID,
            @Param("marketPlaceId") Long bbbMarketPlaceId,
            @Param("sortField") MyListingSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("offset") long offset,
            @Param("size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM " +
            "(" +
            "SELECT " +
//            "market_listing.id AS market_listing_id, " +
//            "inventory.id AS inventory_id, " +
//            "inventory.bicycle_id AS bicycle_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
//            "inventory_type.id AS type_id, " +
//            "inventory.serial_number AS serial_number, " +
//            "bicycle.name AS bicycle_name, " +
//            "inventory.bicycle_model_name AS bicycle_model_name, " +
//            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inventory.bicycle_year_name AS bicycle_year_name, " +
//            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
//            "market_listing.market_place_id AS market_place_id, " +
//            "market_place.type AS type, " +
//
//            "inventory.image_default AS image_default, " +
//
//            "brake_type.inventory_comp_type_id AS brake_type_id, " +
//            "brake_type.brake_type_name AS brake_name, " +
//            "frame_material.inventory_comp_type_id AS frame_material_id, " +
//            "frame_material.fragment_material_name AS frame_material_name, " +
//
//            "market_listing.status AS status_market_listing," +
//            "market_listing.is_best_offer AS listing_is_best_offer, " +
//            "inventory.partner_id AS partner_id, " +
//            "inventory.name AS inventory_name,  " +
//            "inventory.type_id AS type_inventory_id, " +
//            "inventory_type.name AS type_inventory_name, " +
//            "inventory.msrp_price AS msrp_price, " +
//            "inventory.bbb_value AS bbb_value, " +
//            "inventory.override_trade_in_price AS override_trade_in_price, " +
//            "inventory.initial_list_price AS initial_list_price, " +
//            "inventory.cogs_price AS cogs_price, " +
//            "inventory.flat_price_change AS flat_price_change, " +
//            "inventory.discounted_price AS discounted_price, " +
//            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
//            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
//            "inventory.condition AS condition, " +
//            "inventory_size.inventory_id AS size_id, " +
//            "inventory_size.size_name AS size_name, " +
//
//            //sort
//            "inventory.title AS title, " +
//            "inventory.current_listed_price AS current_listed_price, " +
//
//            //fake id
            "ABS(CHECKSUM(NewId())) AS id " +
//
//            //fake columns of draft
//            "NULL AS draft_id, " +
//            "NULL AS content, " +
//            "NULL AS draft_is_best_offer, " +
//
//            //sort
//            "market_listing.created_time AS created_time " +
//
            "FROM market_listing " +


            "JOIN inventory " +
                "ON market_listing.inventory_id = inventory.id " +
                "AND inventory.seller_id = :#{#userID} " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "JOIN inventory_comp_type " +
                "ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +


            "WHERE market_listing.market_place_id = :#{#marketPlaceId} " +
            "AND market_listing.is_delete = 0 " +
//            "AND market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.name()} " +


            "UNION ALL " +


            "SELECT " +
//
//            //fake columns of market listing
//            "NULL AS market_listing_id, " +
//            "NULL AS inventory_id, " +
//            "NULL AS bicycle_id, " +
//            "NULL AS model_id, " +
//            "NULL AS brand_id, " +
//            "NULL AS year_id, " +
//            "NULL AS type_id, " +
//            "NULL AS serial_number, " +
//            "NULL AS bicycle_name, " +
//            "NULL AS bicycle_model_name, " +
//            "NULL AS bicycle_brand_name, " +
//            "NULL AS bicycle_year_name, " +
//            "NULL AS bicycle_type_name, " +
//            "NULL AS bicycle_size_name, " +
//            "NULL AS market_place_id, " +
//            "NULL AS type, " +
//            "NULL AS image_default, " +
//            "NULL AS brake_type_id, " +
//            "NULL AS brake_name, " +
//            "NULL AS frame_material_id, " +
//            "NULL AS frame_material_name, " +
//
//            ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.name()} AS status_market_listing, " +
//            "NULL AS listing_is_best_offer, " +
//            "NULL AS partner_id, " +
//            "NULL AS inventory_name,  " +
//            "NULL AS type_inventory_id, " +
//            "NULL AS type_inventory_name, " +
//            "NULL AS msrp_price, " +
//            "NULL AS bbb_value, " +
//            "NULL AS override_trade_in_price, " +
//            "NULL AS initial_list_price, " +
//            "NULL AS cogs_price, " +
//            "NULL AS flat_price_change, " +
//            "NULL AS discounted_price, " +
//            "NULL AS best_offer_auto_accept_price, " +
//            "NULL AS minimum_offer_auto_accept_price, " +
//            "NULL AS condition, " +
//            "NULL AS size_id, " +
//            "NULL AS size_name, " +
//
//            "NULL AS title, " +
//            "NULL AS current_listed_price, " +

            //fake id
            "ABS(CHECKSUM(NewId())) AS id " +


            //draft
//            "my_listing_draft.id AS draft_id, " +
//            "my_listing_draft.content AS content, " +
//            "my_listing_draft.is_best_offer AS draft_is_best_offer, " +
//            "my_listing_draft.created_time AS created_time " +

            "FROM my_listing_draft " +
            "WHERE my_listing_draft.is_delete = 0 " +
            "AND my_listing_draft.created_by = :#{#userID} " +
//            "ORDER BY my_listing_draft.created_time " +
//            "OFFSET :#{#pageable.offset} ROWS FETCH NEXT :#{#pageable.pageSize} ROWS ONLY" +
            ") AS my_listing "
    )
    int countAllByUser(
            @Param("userID") String userID,
            @Param("marketPlaceId") Long bbbMarketPlaceId
    );
}
