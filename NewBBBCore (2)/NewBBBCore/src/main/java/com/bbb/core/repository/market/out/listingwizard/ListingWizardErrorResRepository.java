package com.bbb.core.repository.market.out.listingwizard;

import com.bbb.core.model.response.market.listingwizard.ListingWizardQueueResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingWizardErrorResRepository extends JpaRepository<ListingWizardQueueResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.status AS status_market_listing, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id " +
            "FROM " +
            "market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).QUEUE} AND " +
            "market_listing.is_delete = 0 " +
            "ORDER BY market_listing.start_queue_time ASC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<ListingWizardQueueResponse> findAllMarketListingQueue(
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );
}
