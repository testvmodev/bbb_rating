package com.bbb.core;

import com.bbb.core.common.Constants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootApplication
@EnableSpringDataWebSupport
@PropertySource("classpath:application.properties")
@EnableScheduling
@EnableAsync(proxyTargetClass = true)
@EnableCaching
public class Main {
    @PostConstruct
    public void init() {
        DateTimeZone.setDefault(DateTimeZone.UTC);
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));   // It will set UTC timezone
//        System.out.println("Spring boot application running in UTC timezone :" + new Date());   // It will print UTC timezone
    }
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
