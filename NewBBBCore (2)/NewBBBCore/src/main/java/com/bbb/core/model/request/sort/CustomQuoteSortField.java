package com.bbb.core.model.request.sort;

public enum CustomQuoteSortField {
    CREATED_TIME, LAST_UPDATE, TITLE, STATUS
}
