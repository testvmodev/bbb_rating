package com.bbb.core.model.response.tradein.detail.detailstep.stepthree;

import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.request.tradein.tradin.ShippingBicycleBlueBookRequest;
import org.joda.time.LocalDate;

public class ShippingBicycleBlueBookResponse extends ShippingBicycleBlueBookRequest {
    private CarrierType carrierType;
    private String fullLinkLabel;
    private Boolean isScheduled;
    private LocalDate scheduleDate;

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getFullLinkLabel() {
        return fullLinkLabel;
    }

    public void setFullLinkLabel(String fullLinkLabel) {
        this.fullLinkLabel = fullLinkLabel;
    }
}
