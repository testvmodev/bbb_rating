package com.bbb.core.manager.user;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.user.UserVerifySafeActionResponse;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.user.AccountRepository;
import com.bbb.core.repository.user.UserBasicStatsResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserManager {
    @Autowired
    private AuthManager authManager;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserBasicStatsResponseRepository userBasicStatsResponseRepository;

    public Object updateStatusActive(String userId, boolean isActive) throws ExceptionResponse {
        Account account = findAccountAndNotDelted(userId);

        UserDetailFullResponse user = authManager.getUserFullInfo(userId);

        if (!user.getRole().equals(UserRoleType.NORMAL.getValue()) &&
                !user.getRole().equals(UserRoleType.WHOLESALER.getValue())
        ) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    MessageResponses.DELETE_ACCOUNT_INVALID_ROLE,
                    HttpStatus.BAD_REQUEST
            );
        }

        List<Offer> notAcceptOffers = new ArrayList<>();
        List<Offer> acceptedOffers = new ArrayList<>();

        List<MarketListing> myListings = new ArrayList<>();
        List<Long> myListingIds = null;
        List<Inventory> myListingInventories = new ArrayList<>();

        if (!isActive) {
            if (user.getRole().equals(UserRoleType.NORMAL.getValue())) { //personal listing
                //auth service should check existed carts before call this which mean no sale pending listings,
                //can skip checking them
                myListings = marketListingRepository.findAllPersonalListing(userId, StatusMarketListing.LISTED);
                List<Offer> otherOffers = null;

                if (myListings.size() > 0) {
                    myListingIds = myListings.stream()
                            .map(MarketListing::getId)
                            .collect(Collectors.toList());
                    otherOffers = offerRepository.findActiveByAuctionOrListing(
                            null, myListingIds
                    );

                    List<Long> inventoryIds = myListings.stream()
                            .map(MarketListing::getInventoryId)
                            .collect(Collectors.toList());
                    myListingInventories = inventoryRepository.findAllByIds(inventoryIds);
                }

                List<Offer> userOffers = offerRepository.findAllActiveOfUser(userId);

                filterAcceptedOrNot(acceptedOffers, notAcceptOffers, userOffers, otherOffers);

                myListingInventories.forEach(inventory -> {
                    inventory.setStatus(StatusInventory.IN_ACTIVE);
                });
                myListings.forEach(marketListing -> {
                    marketListing.setStatus(StatusMarketListing.DE_LISTED);
                });
            } else if (user.getRole().equals(UserRoleType.WHOLESALER.getValue())) { //inventory auction
                List<Offer> offers = offerRepository.findAllActiveOfUser(userId);

                filterAcceptedOrNot(acceptedOffers, notAcceptOffers, offers);
            }
            account.setStatus(StatusAccount.INACTIVE);
        } else {
            if (account.getStatus() != StatusAccount.INACTIVE) {
                throw new ExceptionResponse(
                        ObjectError.INVALID_ACTION,
                        MessageResponses.ACCOUNT_INVALID_STATUS,
                        HttpStatus.BAD_REQUEST
                );
            }
            if (user.getRole().equals(UserRoleType.NORMAL.getValue())) { //personal listing
                myListings = marketListingRepository.findAllPersonalListing(userId, StatusMarketListing.DE_LISTED);

                if (myListings.size() > 0) {
                    List<Long> inventoryIds = myListings.stream()
                            .map(MarketListing::getInventoryId)
                            .collect(Collectors.toList());
                    myListingInventories = inventoryRepository.findAllByIds(inventoryIds);

                    myListingInventories.forEach(inventory -> {
                        inventory.setStatus(StatusInventory.ACTIVE);
                    });
                    myListings.forEach(marketListing -> {
                        marketListing.setStatus(StatusMarketListing.LISTED);
                    });
                }
            } else if (user.getRole().equals(UserRoleType.WHOLESALER.getValue())) {
                //nothing
            }
            account.setStatus(StatusAccount.ACTIVE);
        }

        if (acceptedOffers.size() > 0) {
            sendMailAndUpdateInvAucOrListing(acceptedOffers, myListingIds);
        }

        if (!isActive) {
            //keep offer history, dont soft delete them
//            List<Offer> offersNotAcceptAndAcceptedDelete =
//                    notAcceptOffers.stream().peek(o->o.setDelete(true))
//                    .collect(Collectors.toList());
//            offersNotAcceptAndAcceptedDelete.addAll(
//                    acceptedOffers.stream().peek(o->o.setDelete(true))
//                            .collect(Collectors.toList())
//            );

            offerManager.rejectAll(notAcceptOffers, acceptedOffers);
        }

        if (myListings.size() > 0) {
            marketListingRepository.saveAll(myListings);
        }
        if (myListingInventories.size() > 0) {
            inventoryRepository.saveAll(myListingInventories);
        }
        accountRepository.save(account);

        return new MessageResponse("Success");
    }

    public Object deletedAccount(String userId) throws ExceptionResponse {
        Account account = findAccountAndNotDelted(userId);

        List<Offer> notAcceptOffers = new ArrayList<>();
        List<Offer> acceptedOffers = new ArrayList<>();

        List<MarketListing> myListings = new ArrayList<>();
        List<Long> myListingIds = null;
        List<Inventory> myListingInventories = new ArrayList<>();

        account.setStatus(StatusAccount.DELETED);
        UserDetailFullResponse user = authManager.getUserFullInfo(userId);

        if (user.getRole().equals(UserRoleType.NORMAL.getValue())) { //personal listing
            //auth service should check existed carts before call this which mean no sale pending listings,
            //can skip checking them
            myListings = marketListingRepository.findAllPersonalListing(userId, null); //null = delete all
            List<Offer> otherOffers = null;

            if (myListings.size() > 0) {
                myListingIds = myListings.stream()
                        .map(MarketListing::getId)
                        .collect(Collectors.toList());
                otherOffers = offerRepository.findActiveByAuctionOrListing(
                        null, myListingIds
                );

                List<Long> inventoryIds = myListings.stream()
                        .map(MarketListing::getInventoryId)
                        .collect(Collectors.toList());
                myListingInventories = inventoryRepository.findAllByIds(inventoryIds);
            }

            List<Offer> userOffers = offerRepository.findAllActiveOfUser(userId);

            filterAcceptedOrNot(acceptedOffers, notAcceptOffers, userOffers, otherOffers);

            myListingInventories.forEach(inventory -> {
                inventory.setStatus(StatusInventory.IN_ACTIVE);
            });
            myListings.forEach(marketListing -> {
                marketListing.setDelete(true);
                marketListing.setStatus(StatusMarketListing.DE_LISTED);
            });
        } else if (user.getRole().equals(UserRoleType.WHOLESALER.getValue())) { //inventory auction
            List<Offer> offers = offerRepository.findAllActiveOfUser(userId);

            filterAcceptedOrNot(acceptedOffers, notAcceptOffers, offers);
        } else {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    MessageResponses.DELETE_ACCOUNT_INVALID_ROLE,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (acceptedOffers.size() > 0) {
            sendMailAndUpdateInvAucOrListing(acceptedOffers, myListingIds);
        }

        setDeleteAll(notAcceptOffers, acceptedOffers);
        offerManager.rejectAll(notAcceptOffers, acceptedOffers);

        if (myListings.size() > 0) {
            marketListingRepository.saveAll(myListings);
            inventoryRepository.saveAll(myListingInventories);
        }

        return new MessageResponse("Success");
    }

    public Object verifySafe(String userId) throws ExceptionResponse {
        UserVerifySafeActionResponse response = new UserVerifySafeActionResponse();
        response.setMarketListing(
                marketListingRepository.countAllUserListing(userId, StatusMarketListing.SALE_PENDING) > 0
        );
        response.setOffer(
                offerRepository.countUserOffers(userId, StatusOffer.ACCEPTED, false) > 0
        );
        return response;
    }

    public Object getUserBasicStats(String userId) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        return userBasicStatsResponseRepository.findOne(userId);
    }

    private Account findAccountAndNotDelted(String userId) throws ExceptionResponse {
        Account account = accountRepository.findOne(userId);

        if (account != null && account.getStatus() == StatusAccount.DELETED) {
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION,
                    MessageResponses.ACCOUNT_DELETED,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (account == null) {
            account = new Account();
            account.setUserId(userId);
        }

        return account;
    }

    private void filterAcceptedOrNot(
            List<Offer> resultAccepted, List<Offer> resultNotAccepted,
            List<Offer>... offerLists
    ) {
        if (resultAccepted == null) {
            resultAccepted.size(); //throw null exception and halt api processing
        }
        if (resultNotAccepted == null) {
            resultNotAccepted.size(); //throw null exception and halt api processing
        }

        for (List<Offer> offers : offerLists) {
            if (offers != null && offers.size() > 0) {
                offers.forEach(offer -> {
                    if (offer.getStatus() == StatusOffer.ACCEPTED) {
                        resultAccepted.add(offer);
                    } else {
                        resultNotAccepted.add(offer);
                    }
                });
            }
        }
    }

    private void setDeleteAll(List<Offer>... offerLists) {
        for (List<Offer> offers : offerLists) {
            if (offers != null) {
                for (Offer o : offers) {
                    o.setDelete(true);
                }
            }
        }
    }

    private void sendMailAndUpdateInvAucOrListing(
            List<Offer> acceptedOffers, List<Long> myListingIds
    ) {
        List<InventoryAuction> inventoryAuctions = new ArrayList<>();
        List<MarketListing> salePendingListings = new ArrayList<>();
        List<Inventory> salePendingInventories = new ArrayList<>();

        updateOtherInvAuctionOrListing(
                acceptedOffers, myListingIds,
                inventoryAuctions, salePendingListings, salePendingInventories
        );

//        sendMailRejectAcceptedOffer(acceptedOffers);

        if (inventoryAuctions.size() > 0) {
            inventoryAuctionRepository.saveAll(inventoryAuctions);
        }
        if (salePendingListings.size() > 0) {
            marketListingRepository.saveAll(salePendingListings);
        }
        if (salePendingInventories.size() > 0) {
            inventoryRepository.saveAll(salePendingInventories);
        }
    }

    private void updateOtherInvAuctionOrListing(
            List<Offer> acceptedOffers, List<Long> myListingIds, //input
            @NotNull List<InventoryAuction> resultInventoryAuctions, //output
            @NotNull List<MarketListing> resultSalePendingListings, //output
            @NotNull List<Inventory> resultSalePendingInventories  //output
    ) {
        //make sure not null
        resultInventoryAuctions.size();
        resultSalePendingListings.size();
        resultSalePendingInventories.size();

        List<Long> inventoryAuctionIds = new ArrayList<>();
        List<Long> salePendingListingIds = new ArrayList<>();

        List<InventoryAuction> inventoryAuctions = new ArrayList<>();
        List<MarketListing> salePendingListings = new ArrayList<>();
        List<Inventory> salePendingInventories = new ArrayList<>();

        acceptedOffers.forEach(offer -> {
            if (offer.getInventoryAuctionId() != null) {
                inventoryAuctionIds.add(offer.getInventoryAuctionId());
            } else {
                salePendingListingIds.add(offer.getMarketListingId());
            }
        });

        if (inventoryAuctionIds.size() > 0) {
            inventoryAuctions = inventoryAuctionRepository.findAllFromIds(inventoryAuctionIds, true, false);
        }
        if (inventoryAuctions.size() > 0) {
            List<Long> inactiveIds = inventoryAuctions.stream()
                    .map(InventoryAuction::getId)
                    .collect(Collectors.toList());
            List<Long> reactiveInventoryAuctionIds = inventoryAuctionRepository.findInactiveByIdsCanReactive(
                    inactiveIds
            );
            inventoryAuctions.forEach(inventoryAuction -> {
                inventoryAuction.setWinnerId(null);
            });
            if (reactiveInventoryAuctionIds.size() > 0) {
                inventoryAuctions.forEach(inventoryAuction -> {
                    inventoryAuction.setInactive(false);
                });
            }
            resultInventoryAuctions.addAll(inventoryAuctions);
        }

        if (salePendingListingIds.size() > 0) {
            //remove sale pending listing of this deleting account
            if (myListingIds != null && myListingIds.size() > 0) {
                myListingIds.forEach(id -> {
                    salePendingListingIds.remove(id);
                });
            }
            if (salePendingListingIds.size() > 0) {
                salePendingListings = marketListingRepository.findAllByIds(salePendingListingIds);
                if (salePendingListings.size() > 0) {
                    salePendingListings.forEach(marketListing -> {
                        marketListing.setStatus(StatusMarketListing.LISTED);
                    });
                    resultSalePendingListings.addAll(salePendingListings);

                    List<Long> inventoryIds = salePendingListings.stream()
                            .map(MarketListing::getInventoryId)
                            .collect(Collectors.toList());
                    salePendingInventories = inventoryRepository.findAllByIds(inventoryIds);
                    if (salePendingInventories.size() > 0) {
                        salePendingInventories.forEach(inventory -> {
                            inventory.setStage(StageInventory.LISTED);
                        });
                        resultSalePendingInventories.addAll(salePendingInventories);
                    }
                }
            }
        }
    }

//    private void sendMailRejectAcceptedOffer(List<Offer> offers) {
//
//    }
}
