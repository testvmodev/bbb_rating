package com.bbb.core.model.database;

import com.bbb.core.model.database.type.ConditionInventory;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_decline_detail")
public class TradeInDeclineDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long tradeInId;
    private long reasonDeclineId;
    private String comment;
    private String priceExpected;

    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public long getReasonDeclineId() {
        return reasonDeclineId;
    }

    public void setReasonDeclineId(long reasonDeclineId) {
        this.reasonDeclineId = reasonDeclineId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPriceExpected() {
        return priceExpected;
    }

    public void setPriceExpected(String priceExpected) {
        this.priceExpected = priceExpected;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(value = GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }
}
