package com.bbb.core.controller;

import com.bbb.core.common.Constants;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.config.http.CacheServletInputStream;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.market.MarketListingManager;
import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.response.BicycleBrandResponse;
import com.bbb.core.repository.reout.BicycleBrandResponseRepository;
import io.reactivex.Observable;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.query.spi.sql.NativeSQLQueryReturn;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class HomeController {
    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    private BicycleBrandResponseRepository bicycleBrandResponseRepository;
    @Autowired
    private MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter;
//    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    HomeController(EntityManagerFactory entityManagerFactory) {
        sessionFactory = ConfigDataSource.getSessionFactory(entityManagerFactory);
    }

    @GetMapping(value = "/")
    public String home() {
        return "Well come to BBB core";
    }

//    @GetMapping(value = "/demo")
//    public List<BicycleBrandResponse> demo(
//            @RequestParam(value = "localDateTime", required = false)
////                    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
//                   LocalDateTime localDateTime,
//            @RequestParam(value = "testValue", required = false) long testValue,
//            HttpServletRequest request
//            ){
//        Locale locale = request.getLocale();
//        LOG.info("addConverter time zone name: " + locale.getDisplayName());
//        LOG.info("addConverter time zone country: " + locale.getCountry());
//        LOG.info("addConverter time zone language: " + locale.getDisplayLanguage());
//
//        Calendar calenda = Calendar.getInstance(locale);
//        TimeZone timeZone = calenda.getTimeZone();
//        LOG.info("addConverter time zone id: " + timeZone.getID());
//        LOG.info("addConverter time zone name: " + timeZone.getDisplayName());
//
//        String ipAddress = request.getRemoteAddr();
//        LOG.info("demo ipAddress: " +ipAddress);
//        return bicycleBrandResponseRepository.findAllByIds();
//    }

    @PostMapping(value = "/demo")
    public String test(
            HttpServletRequest request
    ) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS+zzzzzz");
        try {
//            URL url = new URL("https://smartetailing.s3.amazonaws.com/large-images.zip");
//            InputStream input = url.openStream();
//            InputStreamReader inputReader = new InputStreamReader(input, "UTF-8");
//            BufferedReader reader = new BufferedReader(inputReader);
//            System.out.println(reader.lines().count());
            boolean isSuccess = Observable.zip(
                    CommonUtils.getOb(() -> {
                        LOG.info("Downloading SE 1");
                        String url = "https://s3.amazonaws.com/bbb-v2-dev/test/SE_Bicycle_03_11.csv";
                        Map<Long, List<CSVRecord>> compSpecMapImporting = new HashMap<>();
                        try {
                            URL data = new URL(url);
                            InputStream stream = data.openStream();
                            InputStreamReader reader = new InputStreamReader(stream);
                            CSVParser parser = CSVFormat.DEFAULT.parse(reader);

                            if (parser.iterator().hasNext()) {
                                //skip first row which is title
                                parser.iterator().next();
                            }
                            LOG.info("Reading SE 1");
                            while (parser.iterator().hasNext()) {
                                CSVRecord record = parser.iterator().next();
                                Long smartEtailingId = Long.valueOf(record.get(1));

                                if (!compSpecMapImporting.containsKey(smartEtailingId)) {
                                    compSpecMapImporting.put(smartEtailingId, new ArrayList<>());
                                }

                                compSpecMapImporting.get(smartEtailingId).add(record);
                            }
                            LOG.info("Done reading SE 1");
                            parser.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return compSpecMapImporting;
                    }),
                    CommonUtils.getOb(() -> {
                        LOG.info("Downloading SE 2");
                        String url = "https://s3.amazonaws.com/bbb-v2-dev/test/SE_Bicycle_03_11.csv";
                        Map<Long, List<CSVRecord>> compSpecMapImporting = new HashMap<>();
                        try {
                            URL data = new URL(url);
                            InputStream stream = data.openStream();
                            InputStreamReader reader = new InputStreamReader(stream);
                            CSVParser parser = CSVFormat.DEFAULT.parse(reader);

                            if (parser.iterator().hasNext()) {
                                //skip first row which is title
                                parser.iterator().next();
                            }
                            LOG.info("Reading SE 2");
                            while (parser.iterator().hasNext()) {
                                CSVRecord record = parser.iterator().next();
                                Long smartEtailingId = Long.valueOf(record.get(1));

                                if (!compSpecMapImporting.containsKey(smartEtailingId)) {
                                    compSpecMapImporting.put(smartEtailingId, new ArrayList<>());
                                }

                                compSpecMapImporting.get(smartEtailingId).add(record);
                            }
                            LOG.info("Done reading SE 2");
                            parser.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return compSpecMapImporting;
                    }),
                    CommonUtils.getOb(() -> {
                        LOG.info("Downloading SE 3");
                        String url = "https://s3.amazonaws.com/bbb-v2-dev/test/SE_Bicycle_03_11.csv";
                        Map<Long, List<CSVRecord>> compSpecMapImporting = new HashMap<>();
                        try {
                            URL data = new URL(url);
                            InputStream stream = data.openStream();
                            InputStreamReader reader = new InputStreamReader(stream);
                            CSVParser parser = CSVFormat.DEFAULT.parse(reader);

                            if (parser.iterator().hasNext()) {
                                //skip first row which is title
                                parser.iterator().next();
                            }
                            LOG.info("Reading SE 3");
                            while (parser.iterator().hasNext()) {
                                CSVRecord record = parser.iterator().next();
                                Long smartEtailingId = Long.valueOf(record.get(1));

                                if (!compSpecMapImporting.containsKey(smartEtailingId)) {
                                    compSpecMapImporting.put(smartEtailingId, new ArrayList<>());
                                }

                                compSpecMapImporting.get(smartEtailingId).add(record);
                            }
                            LOG.info("Done reading SE 3");
                            parser.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return compSpecMapImporting;
                    }),
                    (o1, o2, o3) -> true
            ).blockingFirst();
            Thread.sleep(60000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Hello Java spring";
    }

    @GetMapping(value = "/demo")
    public Object testGet(

    ) {
        Session session = sessionFactory.openSession();
        SQLQuery sqlQuery = session.createSQLQuery("SELECT TOP 100 * FROM bicycle");
        List result = sqlQuery.addEntity(Bicycle.class).list();
        session.close();
        return result;
    }

}
