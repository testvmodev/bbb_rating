package com.bbb.core.model.request.ebay.additems;

import com.bbb.core.model.request.ebay.additem.Item;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AddItemRequestContainer {
    @JacksonXmlProperty(localName = "MessageID")
    private String messageID;
    @JacksonXmlProperty(localName = "Item")
    private Item item;

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
