package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SalesTax {
    @JacksonXmlProperty(localName = "SalesTaxPercent")
    private float salesTaxPercent;
    @JacksonXmlProperty(localName = "SalesTaxState")
    private String salesTaxState;

    public float getSalesTaxPercent() {
        return salesTaxPercent;
    }

    public void setSalesTaxPercent(float salesTaxPercent) {
        this.salesTaxPercent = salesTaxPercent;
    }

    public String getSalesTaxState() {
        return salesTaxState;
    }

    public void setSalesTaxState(String salesTaxState) {
        this.salesTaxState = salesTaxState;
    }
}
