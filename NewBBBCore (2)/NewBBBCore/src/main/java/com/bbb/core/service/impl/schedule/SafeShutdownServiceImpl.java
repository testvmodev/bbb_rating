package com.bbb.core.service.impl.schedule;

import com.bbb.core.manager.schedule.SafeShutdownManager;
import com.bbb.core.service.schedule.SafeShutdownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SafeShutdownServiceImpl implements SafeShutdownService {
    @Autowired
    private SafeShutdownManager manager;

    public Object safeShutdown() {
        return manager.safeShutdown();
    }
}
