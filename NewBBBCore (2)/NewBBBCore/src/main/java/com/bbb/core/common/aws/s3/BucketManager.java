package com.bbb.core.common.aws.s3;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BucketManager {
    private AmazonS3 amazonS3USEAST1;
    private AmazonS3 amazonS3USEAST2;
    private AmazonS3 amazonS3USWEST1;
    private AmazonS3 amazonS3USWEST2;
    private AmazonS3 amazonS3EUCenter1;
    private AmazonS3 amazonS3EUWest1;
    private AmazonS3 amazonS3EUWest2;
    private AmazonS3 amazonS3APSouth1;
    private AmazonS3 amazonS3APSoutheast1;
    private AmazonS3 amazonS3APSoutheast2;
    private AmazonS3 amazonS3APNortheast1;
    private AmazonS3 amazonS3ApNortheast2;
    private AmazonS3 amazonS3SAEast1;
    private AmazonS3 amazonS3CNNorth1;
    private AmazonS3 amazonS3CACentral1;
    private AmazonS3 amazonS3GovCloud;

    @Autowired
    public BucketManager(AmazonS3 amazonS3USEAST1, AmazonS3 amazonS3USEAST2, AmazonS3 amazonS3USWEST1,
                         AmazonS3 amazonS3USWEST2, AmazonS3 amazonS3EUCenter1, AmazonS3 amazonS3EUWest1, AmazonS3 amazonS3EUWest2,
                         AmazonS3 amazonS3APSouth1, AmazonS3 amazonS3APSoutheast1, AmazonS3 amazonS3APSoutheast2,
                         AmazonS3 amazonS3APNortheast1, AmazonS3 amazonS3ApNortheast2, AmazonS3 amazonS3SAEast1,
                         AmazonS3 amazonS3CNNorth1, AmazonS3 amazonS3CACentral1, AmazonS3 amazonS3GovCloud) {
        this.amazonS3USEAST1 = amazonS3USEAST1;
        this.amazonS3USEAST2 = amazonS3USEAST2;
        this.amazonS3USWEST1 = amazonS3USWEST1;
        this.amazonS3USWEST2 = amazonS3USWEST2;
        this.amazonS3EUCenter1 = amazonS3EUCenter1;
        this.amazonS3EUWest1 = amazonS3EUWest1;
        this.amazonS3EUWest2 = amazonS3EUWest2;
        this.amazonS3APSouth1 = amazonS3APSouth1;
        this.amazonS3APSoutheast1 = amazonS3APSoutheast1;
        this.amazonS3APSoutheast2 = amazonS3APSoutheast2;
        this.amazonS3APNortheast1 = amazonS3APNortheast1;
        this.amazonS3ApNortheast2 = amazonS3ApNortheast2;
        this.amazonS3SAEast1 = amazonS3SAEast1;
        this.amazonS3CNNorth1 = amazonS3CNNorth1;
        this.amazonS3CACentral1 = amazonS3CACentral1;
        this.amazonS3GovCloud = amazonS3GovCloud;
    }

    AmazonS3 getAmazonS3ClientByBucketDefault() {
        return getAmazonS3ClientByRegion(Regions.US_EAST_1);
    }

    AmazonS3 getAmazonS3ClientByBucketDefault(Regions region) {
        return getAmazonS3ClientByRegion(region);
    }

    private AmazonS3 getAmazonS3ClientByRegion(Regions region) {
        switch (region) {
            case GovCloud:
                return amazonS3GovCloud;
            case US_EAST_1:
                return amazonS3USEAST1;
            case US_EAST_2:
                return amazonS3USEAST2;
            case US_WEST_1:
                return amazonS3USWEST1;
            case US_WEST_2:
                return amazonS3USWEST2;
            case EU_WEST_1:
                return amazonS3EUWest1;
            case EU_WEST_2:
                return amazonS3EUWest2;
            case EU_CENTRAL_1:
                return amazonS3EUCenter1;
            case AP_SOUTH_1:
                return amazonS3APSouth1;
            case AP_SOUTHEAST_1:
                return amazonS3APSoutheast1;
            case AP_SOUTHEAST_2:
                return amazonS3APSoutheast2;
            case AP_NORTHEAST_1:
                return amazonS3APNortheast1;
            case AP_NORTHEAST_2:
                return amazonS3ApNortheast2;
            case SA_EAST_1:
                return amazonS3SAEast1;
            case CN_NORTH_1:
                return amazonS3CNNorth1;
            case CA_CENTRAL_1:
                return amazonS3CACentral1;
            default:
                return amazonS3CACentral1;
        }
    }
}
