package com.bbb.core.manager.schedule;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.model.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SafeShutdownManager {
    @Autowired
    private ScheduleManager scheduleManager;

    public Object safeShutdown() {
        scheduleManager.shutDown();
        return new MessageResponse(MessageResponses.SAFE_SHUTDOWN_SUCCESS);
    }
}
