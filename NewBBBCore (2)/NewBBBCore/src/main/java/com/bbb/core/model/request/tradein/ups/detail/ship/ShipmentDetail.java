package com.bbb.core.model.request.tradein.ups.detail.ship;

import com.bbb.core.model.request.tradein.ups.detail.ShipAddress;
import com.bbb.core.model.request.tradein.ups.detail.ShipService;
import com.bbb.core.model.request.tradein.ups.detail.Shipper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipmentDetail {
    @JsonProperty("Shipper")
    private Shipper shipper;
    @JsonProperty("ShipTo")
    private ShipAddress shipTo;
    @JsonProperty("ShipFrom")
    private ShipAddress shipFrom;
    @JsonProperty("Package")
    private ShipPackageDetail shipPackageDetail;
    @JsonProperty("PaymentInformation")
    private PaymentInformation paymentInformation;
    @JsonProperty("Service")
    private ShipService service;

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public ShipAddress getShipTo() {
        return shipTo;
    }

    public void setShipTo(ShipAddress shipTo) {
        this.shipTo = shipTo;
    }

    public ShipAddress getShipFrom() {
        return shipFrom;
    }

    public void setShipFrom(ShipAddress shipFrom) {
        this.shipFrom = shipFrom;
    }

    public ShipPackageDetail getShipPackageDetail() {
        return shipPackageDetail;
    }

    public void setShipPackageDetail(ShipPackageDetail shipPackageDetail) {
        this.shipPackageDetail = shipPackageDetail;
    }

    public PaymentInformation getPaymentInformation() {
        return paymentInformation;
    }

    public void setPaymentInformation(PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    @JsonIgnore
    public ShipService getShipService() {
        return service;
    }

    public void setShipService(ShipService service) {
        this.service = service;
    }
}
