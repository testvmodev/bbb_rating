package com.bbb.core.security.authen.jwt;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class SecretAPIAuthentication extends AbstractAuthenticationToken {
    private String secretKey;

    public SecretAPIAuthentication(String secretKey) {
        super(null);
        this.secretKey = secretKey;
    }

    @Override
    public String getCredentials() {
        return secretKey;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
