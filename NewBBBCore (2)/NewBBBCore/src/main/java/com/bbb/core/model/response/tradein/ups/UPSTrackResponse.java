package com.bbb.core.model.response.tradein.ups;

import com.bbb.core.model.response.tradein.ups.detail.track.TrackResponse;
import com.bbb.core.model.response.tradein.ups.detail.fault.Fault;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UPSTrackResponse {
    @JsonProperty("Fault")
    private Fault fault;
    @JsonProperty("TrackResponse")
    private TrackResponse trackResponse;

    public Fault getFault() {
        return fault;
    }

    public void setFault(Fault fault) {
        this.fault = fault;
    }

    public TrackResponse getTrackResponse() {
        return trackResponse;
    }

    public void setTrackResponse(TrackResponse trackResponse) {
        this.trackResponse = trackResponse;
    }

    public boolean isSuccess() {
        try {
            return fault == null && trackResponse.getResponseInfo().getResponseStatus().getCode() == 1;
        } catch (Exception e) {
            return false;
        }
    }

    @JsonIgnore
    public String getFaultMessage() {
        return fault.getDetail().getErrors().getErrorDetail().getPrimaryErrorCode().getDescription();
    }
}
