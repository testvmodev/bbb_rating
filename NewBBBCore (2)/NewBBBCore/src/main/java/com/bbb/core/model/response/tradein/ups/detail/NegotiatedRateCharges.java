package com.bbb.core.model.response.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NegotiatedRateCharges {
    @JsonProperty("TotalCharge")
    private Money totalCharges;

    public Money getTotalCharges() {
        return totalCharges;
    }
}
