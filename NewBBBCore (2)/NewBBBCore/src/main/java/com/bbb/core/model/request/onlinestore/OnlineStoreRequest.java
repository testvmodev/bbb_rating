package com.bbb.core.model.request.onlinestore;

import com.bbb.core.model.request.sort.Sort;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;

public class OnlineStoreRequest {
    @NotNull
    private List<String> storefrontIds;
    @NotNull
    private Sort sortForSale;
    @JsonIgnore
    private Pageable pageable;

    public List<String> getStorefrontIds() {
        return storefrontIds;
    }

    public void setStorefrontIds(List<String> storefrontIds) {
        this.storefrontIds = storefrontIds;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public Sort getSortForSale() {
        return sortForSale;
    }

    public void setSortForSale(Sort sortForSale) {
        this.sortForSale = sortForSale;
    }
}
