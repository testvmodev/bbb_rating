package com.bbb.core.model.response.tradein.detail.detailstep;

import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.embedded.ContentCommonId;

import java.util.List;

public class TradeInImageGroupResponse {
    private long tradeInId;
    private long imageTypeId;
    private String imageTypeName;
    private String imageDefault;
    private boolean isRequire;
    private List<ContentCommonId> fullLinks;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public long getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(long imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    public String getImageTypeName() {
        return imageTypeName;
    }

    public void setImageTypeName(String imageTypeName) {
        this.imageTypeName = imageTypeName;
    }

    public List<ContentCommonId> getFullLinks() {
        return fullLinks;
    }

    public void setFullLinks(List<ContentCommonId> fullLinks) {
        this.fullLinks = fullLinks;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public boolean isRequire() {
        return isRequire;
    }

    public void setRequire(boolean require) {
        isRequire = require;
    }
}
