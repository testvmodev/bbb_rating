package com.bbb.core.model.request.inventory;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;

public class InventoryCreateRequest {
    private long bicycleId;
    private String description;
    private String imageDefault;
    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float flatPriceChange;
    private ConditionInventory condition;
    private Float cogsPrice;
    private String partnerId;
    private String sellerId;
    private String serialNumber;
    private long inventoryTypeId;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private boolean createPo;
    private long sizeInventoryId;
    private String color;
    private StatusInventory status;
    private StageInventory stage;
    private boolean bestOffer;
    private boolean isCustomQuote;


    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }


    public boolean isCreatePo() {
        return createPo;
    }

    public void setCreatePo(boolean createPo) {
        this.createPo = createPo;
    }

    public long getSizeInventoryId() {
        return sizeInventoryId;
    }

    public void setSizeInventoryId(long sizeInventoryId) {
        this.sizeInventoryId = sizeInventoryId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public long getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(long inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public boolean isBestOffer() {
        return bestOffer;
    }

    public void setBestOffer(boolean bestOffer) {
        this.bestOffer = bestOffer;
    }

    public boolean isCustomQuote() {
        return isCustomQuote;
    }

    public void setCustomQuote(boolean customQuote) {
        isCustomQuote = customQuote;
    }
}
