package com.bbb.core.model.otherservice.request.mail.offer.expireaccept;


import com.bbb.core.model.otherservice.request.mail.offer.content.BikeMailRequest;

public class ListingExpireRequest {
    private long id;
//    private Long auction_inv_id;
    private BikeMailRequest bike;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BikeMailRequest getBike() {
        return bike;
    }

    public void setBike(BikeMailRequest bike) {
        this.bike = bike;
    }

//    public Long getAuction_inv_id() {
//        return auction_inv_id;
//    }
//
//    public void setAuction_inv_id(Long auction_inv_id) {
//        this.auction_inv_id = auction_inv_id;
//    }
}
