package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInUpgradeCompModifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TradeInUpgradeCompModifierRepository extends JpaRepository<TradeInUpgradeCompModifier, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_upgrade_comp_modifier " +
            "WHERE " +
            "(min_msrp IS NOT NULL OR max_msrp IS NOT NULL) " +
            "AND percent_modifier IS NOT NULL"
    )
    List<TradeInUpgradeCompModifier> findAll();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_upgrade_comp_modifier " +
            "WHERE trade_in_upgrade_comp_modifier.trade_in_upgrade_comp_id = :upgradeCompId " +
            "AND (min_msrp IS NOT NULL OR max_msrp IS NOT NULL) " +
            "AND percent_modifier IS NOT NULL " +
            "AND (min_msrp IS NULL OR min_msrp < :msrp) " + //note min_msrp < msrp not min_msrp <= msrp
            "AND (max_msrp IS NULL OR max_msrp >= :msrp)"
    )
    TradeInUpgradeCompModifier findOne(
            @Param("upgradeCompId") long upgradeCompId,
            @Param("msrp") Float msrp
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_upgrade_comp_modifier " +
            "WHERE trade_in_upgrade_comp_modifier.trade_in_upgrade_comp_id IN :upgradeCompIds " +
            "AND (min_msrp IS NOT NULL OR max_msrp IS NOT NULL) " +
            "AND percent_modifier IS NOT NULL " +
            "AND (min_msrp IS NULL OR min_msrp < :msrp) " + //note min_msrp < msrp not min_msrp <= msrp
            "AND (max_msrp IS NULL OR max_msrp >= :msrp)"
    )
    List<TradeInUpgradeCompModifier> findAll(
            @Param("upgradeCompIds") List<Long> upgradeCompIds,
            @Param("msrp") Float msrp
    );
}
