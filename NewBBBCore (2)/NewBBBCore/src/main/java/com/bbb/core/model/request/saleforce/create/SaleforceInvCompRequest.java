package com.bbb.core.model.request.saleforce.create;

import javax.validation.constraints.NotBlank;

public class SaleforceInvCompRequest {
    @NotBlank
    private String compTypeName;
    @NotBlank
    private String value;

    public String getCompTypeName() {
        return compTypeName;
    }

    public void setCompTypeName(String compTypeName) {
        this.compTypeName = compTypeName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
