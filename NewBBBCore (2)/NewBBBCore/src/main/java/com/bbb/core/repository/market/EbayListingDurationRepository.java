package com.bbb.core.repository.market;

import com.bbb.core.model.database.EbayListingDuration;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EbayListingDurationRepository extends CrudRepository<EbayListingDuration, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM ebay_listing_duration " +
            "WHERE is_delete = 0")
    List<EbayListingDuration> findAll();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM ebay_listing_duration " +
            "WHERE id IN :durationIds " +
            "AND is_delete = 0")
    List<EbayListingDuration> findAllNotDelete(
            @Param(value = "durationIds") List<Long> durationIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(ebay_listing_duration.id) " +
            "FROM ebay_listing_duration " +
            "WHERE id IN :durationIds " +
            "AND is_delete = 0")
    int countNotDelete(
            @Param(value = "durationIds") List<Long> durationIds
    );
}
