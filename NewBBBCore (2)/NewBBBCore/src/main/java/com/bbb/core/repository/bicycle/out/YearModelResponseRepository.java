package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.response.bicycle.YearModelId;
import com.bbb.core.model.response.bicycle.YearModelResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface YearModelResponseRepository extends JpaRepository<YearModelResponse, YearModelId> {
    //year bicycle
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.year_id AS year_id,  bicycle_year.name AS year_name,  bicycle.model_id AS model_id, bicycle.brand_id AS brand_id " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "WHERE " +
            "bicycle.model_id IN :modelIds AND " +
            "bicycle.brand_id IN :brandIds AND " +
            "bicycle.active = 1 AND " +
            "bicycle.is_delete = 0 " +
            "GROUP BY bicycle.year_id, bicycle_year.name, bicycle.model_id, bicycle.brand_id"

    )
    List<YearModelResponse> findAllYearModel(
            @Param(value = "modelIds") List<Long> modelIds,
            @Param(value = "brandIds") List<Long> brandIds
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.year_id AS year_id,  bicycle_year.name AS year_name,  bicycle.model_id AS model_id, bicycle.brand_id AS brand_id " +
            "FROM " +
            "bicycle " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "WHERE " +
            "bicycle.model_id = :modelId AND " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.active = 1 AND " +
            "bicycle.is_delete = 0 " +
            "GROUP BY bicycle.year_id, bicycle_year.name, bicycle.model_id, bicycle.brand_id"
    )
    List<YearModelResponse> findAllYearModel(
            @Param(value = "modelId") long modelId,
            @Param(value = "brandId") long brandId
    );
}
