package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.response.bid.offer.type.UserOfferType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Immutable
public class UserOfferResponse {
    @Id
    private long offerId;
    private String buyerId;
    @Transient
    private String buyerDisplay;
    private float offerPrice;
    private StatusOffer offerStatus;
    private LocalDateTime createdTime;


    private int marketListingId;
    private long marketPlaceId;
    private MarketType marketType;
    private StatusMarketListing statusMarketListing;

    private long inventoryId;
    private String imageDefault;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private String inventoryTitle;
    private Float currentListedPrice;
    private String sellerId;

    private long bicycleId;
    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    private String bicycleSizeName;

    @JsonIgnore
    private String requestedUserId;


    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerDisplay() {
        return buyerDisplay;
    }

    public void setBuyerDisplay(String buyerDisplay) {
        this.buyerDisplay = buyerDisplay;
    }

    public float getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(float offerPrice) {
        this.offerPrice = offerPrice;
    }

    public StatusOffer getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(StatusOffer offerStatus) {
        this.offerStatus = offerStatus;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public int getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(int marketListingId) {
        this.marketListingId = marketListingId;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public MarketType getMarketType() {
        return marketType;
    }

    public void setMarketType(MarketType marketType) {
        this.marketType = marketType;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getInventoryTitle() {
        return inventoryTitle;
    }

    public void setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getBicycleSizeName() {
        return bicycleSizeName;
    }

    public void setBicycleSizeName(String bicycleSizeName) {
        this.bicycleSizeName = bicycleSizeName;
    }

    public String getRequestedUserId() {
        return requestedUserId;
    }

    public void setRequestedUserId(String requestedUserId) {
        this.requestedUserId = requestedUserId;
    }

    public UserOfferType getOfferType() {
        if (requestedUserId == null || buyerId == null) return null;

        if (requestedUserId.equals(buyerId)) {
            return UserOfferType.MADE;
        }

        if (sellerId == null) return null;

        if (requestedUserId.equals(sellerId)) {
            return UserOfferType.RECEIVED;
        }

        return null;
    }
}
