package com.bbb.core.model.database.type;

public interface InventoryTypeValue {
    String TRADE_IN_TYPE = "Trade-in";
    String OPEN_TERMS = "Open Terms";
    String PTP = "PTP";
}
