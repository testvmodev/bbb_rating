package com.bbb.core.service.impl.bicycle;


import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.TypeManager;
import com.bbb.core.model.request.bicycletype.BicycleTypeGetRequest;
import com.bbb.core.model.request.bicycletype.TypeCreateRequest;
import com.bbb.core.model.request.bicycletype.TypeUpdateRequest;
import com.bbb.core.model.request.sort.BicycleTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private TypeManager manager;

    @Override
    public Object getBicycleTypes(BicycleTypeGetRequest request) throws ExceptionResponse {
        return manager.getBicycleType(request);
    }

    @Override
    public Object getDetailBicycleType(long typeId) throws ExceptionResponse{
        return manager.getDetailBicycleType(typeId);
    }

    @Override
    public Object createType(TypeCreateRequest request) throws ExceptionResponse {
        return manager.createType(request);
    }

    @Override
    public Object updateType(long typeId, TypeUpdateRequest request) throws ExceptionResponse {
        return manager.updateType(typeId, request);
    }

    @Override
    public Object deleteType(long typeId) throws ExceptionResponse {
        return manager.deleteType(typeId);
    }
}
