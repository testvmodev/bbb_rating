package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class Buyer {
    @JacksonXmlProperty(localName = "AboutMePage")
    private boolean aboutMePage;
    @JacksonXmlProperty(localName = "EIASToken")
    private String eIASToken;
    @JacksonXmlProperty(localName = "Email")
    private String email;
    @JacksonXmlProperty(localName = "FeedbackScore")
    private int feebackScore;
    @JacksonXmlProperty(localName = "PositiveFeedbackPercent")
    private float positiveFeedbackPercent;
    @JacksonXmlProperty(localName = "FeedbackPrivate")
    private boolean feeedbackPrivate;
    @JacksonXmlProperty(localName = "FeedbackRatingStar")
    private String feedbackRatingStar;
    @JacksonXmlProperty(localName = "IDVerified")
    private boolean iDVerified;
    @JacksonXmlProperty(localName = "eBayGoodStanding")
    private boolean eBayGoodStanding;
    @JacksonXmlProperty(localName = "NewUser")
    private boolean newUser;
    @JacksonXmlProperty(localName = "RegistrationDate")
    private LocalDateTime registrationDate;
    @JacksonXmlProperty(localName = "Site")
    private String site;
    @JacksonXmlProperty(localName = "Status")
    private String status;
    @JacksonXmlProperty(localName = "UserID")
    private String userId;
    @JacksonXmlProperty(localName = "UserIDChanged")
    private boolean userIDChanged;
    @JacksonXmlProperty(localName = "UserIDLastChanged")
    private LocalDateTime userIDLastChanged;
    @JacksonXmlProperty(localName = "VATStatus")
    private String vATStatus;
    @JacksonXmlProperty(localName = "BuyerInfo")
    private BuyerInfo buyerInfo;
    @JacksonXmlProperty(localName = "UserAnonymized")
    private String userAnonymized;
    @JacksonXmlProperty(localName = "UserFirstName")
    private String userFirstName;
    @JacksonXmlProperty(localName = "UserLastName")
    private String userLastName;

    public boolean isAboutMePage() {
        return aboutMePage;
    }

    public void setAboutMePage(boolean aboutMePage) {
        this.aboutMePage = aboutMePage;
    }

    public String geteIASToken() {
        return eIASToken;
    }

    public void seteIASToken(String eIASToken) {
        this.eIASToken = eIASToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFeebackScore() {
        return feebackScore;
    }

    public void setFeebackScore(int feebackScore) {
        this.feebackScore = feebackScore;
    }

    public float getPositiveFeedbackPercent() {
        return positiveFeedbackPercent;
    }

    public void setPositiveFeedbackPercent(float positiveFeedbackPercent) {
        this.positiveFeedbackPercent = positiveFeedbackPercent;
    }

    public boolean isFeeedbackPrivate() {
        return feeedbackPrivate;
    }

    public void setFeeedbackPrivate(boolean feeedbackPrivate) {
        this.feeedbackPrivate = feeedbackPrivate;
    }

    public String getFeedbackRatingStar() {
        return feedbackRatingStar;
    }

    public void setFeedbackRatingStar(String feedbackRatingStar) {
        this.feedbackRatingStar = feedbackRatingStar;
    }

    public boolean isiDVerified() {
        return iDVerified;
    }

    public void setiDVerified(boolean iDVerified) {
        this.iDVerified = iDVerified;
    }

    public boolean iseBayGoodStanding() {
        return eBayGoodStanding;
    }

    public void seteBayGoodStanding(boolean eBayGoodStanding) {
        this.eBayGoodStanding = eBayGoodStanding;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isUserIDChanged() {
        return userIDChanged;
    }

    public void setUserIDChanged(boolean userIDChanged) {
        this.userIDChanged = userIDChanged;
    }

    public LocalDateTime getUserIDLastChanged() {
        return userIDLastChanged;
    }

    public void setUserIDLastChanged(LocalDateTime userIDLastChanged) {
        this.userIDLastChanged = userIDLastChanged;
    }

    public String getvATStatus() {
        return vATStatus;
    }

    public void setvATStatus(String vATStatus) {
        this.vATStatus = vATStatus;
    }

    public BuyerInfo getBuyerInfo() {
        return buyerInfo;
    }

    public void setBuyerInfo(BuyerInfo buyerInfo) {
        this.buyerInfo = buyerInfo;
    }

    public String getUserAnonymized() {
        return userAnonymized;
    }

    public void setUserAnonymized(String userAnonymized) {
        this.userAnonymized = userAnonymized;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }
}
