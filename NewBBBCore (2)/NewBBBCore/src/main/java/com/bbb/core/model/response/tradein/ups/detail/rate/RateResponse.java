package com.bbb.core.model.response.tradein.ups.detail.rate;

import com.bbb.core.model.response.tradein.ups.detail.ResponseInfo;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RateResponse {
    @JsonProperty("Response")
    private ResponseInfo responseInfo;
    @JsonProperty("RatedShipment")
    private RatedShipment ratedShipment;

    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    public void setResponseInfo(ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    public RatedShipment getRatedShipment() {
        return ratedShipment;
    }

    public void setRatedShipment(RatedShipment ratedShipment) {
        this.ratedShipment = ratedShipment;
    }
}
