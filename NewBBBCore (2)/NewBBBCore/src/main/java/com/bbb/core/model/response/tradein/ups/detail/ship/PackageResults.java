package com.bbb.core.model.response.tradein.ups.detail.ship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PackageResults {
    @JsonProperty("TrackingNumber")
    private String trackingNumber;
    @JsonProperty("ShippingLabel")
    private ShippingLabel shippingLabel;

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public ShippingLabel getShippingLabel() {
        return shippingLabel;
    }
}
