package com.bbb.core.repository.schedule;

import com.bbb.core.model.schedule.OfferMarketListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferMarketListingRepository extends JpaRepository<OfferMarketListing, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id AS offer_id, " +
            "offer.market_listing_id AS market_listing_id, " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "offer.status AS status, " +
            "offer.buyer_id AS buyer_id, " +
            "inventory.name AS inventory_name, " +
            "inventory.image_default AS image_default_inv, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.partner_id AS partner_id " +

            "FROM offer " +

            "JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +

            "WHERE offer.market_listing_id IS NOT NULL " +
            "AND offer.status = :#{T(com.bbb.core.model.database.type.StatusOffer).ACCEPTED.getValue()} " +
            "AND offer.is_delete = 0 " +
            "AND offer.is_paid = 0 " +
            "AND DATEDIFF(MINUTE, offer.last_update, GETDATE()) >= :expirationTime "
    )
    List<OfferMarketListing> findAcceptedOfferMarketListingNotYetPaid(
            @Param(value = "expirationTime") long expirationTime
    );



}
