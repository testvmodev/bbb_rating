package com.bbb.core.repository;

import com.bbb.core.model.database.BicycleImage;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface BicycleImageRepository extends CrudRepository<BicycleImage, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_image")
    List<BicycleImage> finAll();

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_image WHERE bicycle_image.bicycle_id = :bicycleId")
    List<BicycleImage> finAll(@Param(value = "bicycleId") long bicycleId);

    List<BicycleImage> save(List<BicycleImage> images);

    @Query(nativeQuery = true, value = "SELECT bicycle_image.id, bicycle_image.bicycle_id, bicycle_image.uri FROM " +
            "bicycle_image LEFT JOIN bicycle ON bicycle_image.bicycle_id = bicycle.id " +
            "WHERE bicycle.id IS NULL")
    List<BicycleImage> removeAllBicycleImageNoIntoBicycle();

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_image WHERE bicycle_id = :idBicycle")
    int countImage(@Param("idBicycle") long idBicycle);
}
