package com.bbb.core.common;

import com.bbb.core.model.database.type.UserRoleType;

import java.util.Arrays;
import java.util.List;

public interface PermissionRoles {
    List<UserRoleType> P2P_ROLES = Arrays.asList(
            UserRoleType.NORMAL,
            UserRoleType.ONLINE_STORE
    );
    List<UserRoleType> ADMIN_ROLES = Arrays.asList(
            UserRoleType.ADMIN,
            UserRoleType.ROOT
    );
    List<UserRoleType> PARTNER_ROLES = Arrays.asList(
            UserRoleType.PARTNER_ADMIN,
            UserRoleType.PARTNER_MANAGER,
            UserRoleType.PARTNER_EMPLOYEE
    );
}
