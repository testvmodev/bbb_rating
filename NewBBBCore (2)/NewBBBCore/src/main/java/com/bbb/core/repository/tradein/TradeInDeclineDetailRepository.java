package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInDeclineDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TradeInDeclineDetailRepository extends JpaRepository<TradeInDeclineDetail, Long> {

}
