package com.bbb.core.model.otherservice.request.mail.listingexpirewarn;

import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentListingExpireWarnRequest {
    private UserMailRequest user;
    private ListingExpireWarnInfo listing;
    @JsonProperty("base_path")
    private String basePath;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingExpireWarnInfo getListing() {
        return listing;
    }

    public void setListing(ListingExpireWarnInfo listing) {
        this.listing = listing;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
