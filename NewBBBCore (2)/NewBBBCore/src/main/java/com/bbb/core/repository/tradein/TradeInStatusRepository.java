package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeInStatusRepository extends JpaRepository<TradeInStatus, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM trade_in_status " +
            "WHERE id = ?1")
    TradeInStatus findOne(long id);
}
