package com.bbb.core.model.request.bid.offer;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.request.sort.OfferReceiverFieldSort;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ObjectError;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import java.util.List;

public class OffersOnlineStoreRequest {
    private String content;
    private List<StatusOffer> statuses;
    private LocalDate fromDay;
    private LocalDate toDay;
    private OfferReceiverFieldSort fieldSort;
    private Sort sortType;
    private Pageable pageable;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<StatusOffer> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<StatusOffer> statuses) {
        this.statuses = statuses;
    }

    public LocalDate getFromDay() {
        return fromDay;
    }

    public void setFromDay(LocalDate fromDay) {
        this.fromDay = fromDay;
    }

    public LocalDate getToDay() {
        return toDay;
    }

    public void setToDay(LocalDate toDay) {
        this.toDay = toDay;
    }

    public OfferReceiverFieldSort getFieldSort() {
        return fieldSort;
    }

    public void setFieldSort(OfferReceiverFieldSort fieldSort) {
        this.fieldSort = fieldSort;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public void validate() throws ExceptionResponse {
        if (fromDay != null && toDay != null) {
            if (fromDay.compareTo(toDay) > 0) {
                throw new ExceptionResponse(
                        ObjectError.ERROR_PARAM,
                        MessageResponses.FROM_DAY_CANT_HIGHER_THAN_TO_DAY,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }
}
