package com.bbb.core.common.fedex;

import com.bbb.core.model.request.tradein.fedex.detail.ship.FedexLabelSpecification;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FedExConfig implements FedExValue {
    @Bean
    public FedexLabelSpecification fedexLabelSpecification() {
        return new FedexLabelSpecification(
                FedExValue.LABEL_FORMAT_TYPE_DEFAULT,
                FedExValue.LABEL_IMAGE_TYPE_DEFAULT,
                FedExValue.LABEL_STOCK_TYPE_DEFAULT
        );
    }
}
