package com.bbb.core.service.inventory;

import com.bbb.core.common.exception.ExceptionResponse;

import java.util.List;

public interface ComponentInventoryService {
    Object getInventoriesSearch();
    Object getAllBicycleModel(List<Long> brandIds) throws ExceptionResponse;
}
