package com.bbb.core.model.request.sort;

public enum OnlineStoreMyListingSortField {
    POSTING_TIME,
    STATUS,
    ID,
    TITLE,
    PRICE,
    BEST_OFFER,
    OFFER_COUNT,
    VIEWS
}
