package com.bbb.core.model.otherservice.request.mail.listingshipped;

public class BikeListingShipped {
    private String imageUrl;
    private String fullName;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
