package com.bbb.core.common.fedex;

import com.bbb.core.manager.templaterest.FedExRest;
import com.bbb.core.model.database.TradeInShipping;
import com.bbb.core.model.database.TradeInShippingAccount;
import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.otherservice.response.warehouse.ShippingAddressResponse;
import com.bbb.core.model.otherservice.response.warehouse.WarehouseResponse;
import com.bbb.core.model.request.tradein.fedex.FedexDeleteShipRequest;
import com.bbb.core.model.request.tradein.tradin.ShippingBicycleBlueBookRequest;
import com.bbb.core.model.request.tradein.tradin.TradeInCalculateShipRequest;
import com.bbb.core.model.request.tradein.fedex.FedexRateRequest;
import com.bbb.core.model.request.tradein.fedex.FedexProcessShipRequest;
import com.bbb.core.model.request.tradein.fedex.FedexTrackRequest;
import com.bbb.core.model.request.tradein.fedex.detail.*;
import com.bbb.core.model.request.tradein.fedex.detail.ship.FedexLabelSpecification;
import com.bbb.core.model.request.tradein.fedex.detail.ship.Payor;
import com.bbb.core.model.request.tradein.fedex.detail.ship.ResponsibleParty;
import com.bbb.core.model.request.tradein.fedex.detail.ship.ShippingChargesPayment;
import com.bbb.core.model.request.tradein.fedex.detail.track.PackageIdentifier;
import com.bbb.core.model.request.tradein.fedex.detail.track.SelectionDetails;
import com.bbb.core.model.response.tradein.fedex.FedexDeleteShipResponse;
import com.bbb.core.model.response.tradein.fedex.FedexProcessShipResponse;
import com.bbb.core.model.response.tradein.fedex.FedexRateResponse;
import com.bbb.core.model.response.tradein.fedex.FedexTrackResponse;
import com.bbb.core.model.response.tradein.fedex.detail.ship.TrackingId;
import com.bbb.core.repository.tradein.ShippingAccountRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class FedExManager {
    @Autowired
    private FedExRest fedexClient;
    @Autowired
    private FedexLabelSpecification fedexLabelSpecification;
    @Autowired
    private ShippingAccountRepository shippingAccountRepository;

    public FedexProcessShipResponse createShip(
            ShippingBicycleBlueBookRequest shipCreateReq,
            TradeInShippingAccount shippingAccount,
            UserDetailFullResponse userInfo,
            WarehouseResponse warehouse
    ) {
        FedexProcessShipRequest fedexRequest = new FedexProcessShipRequest();

        RequestedShipment requestedShipment = new RequestedShipment();
//        SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");
        requestedShipment.setShipTimestamp(LocalDateTime.now());
        requestedShipment.setDropoffType(FedExValue.DROP_OFF_TYPE_DEFAULT);
        requestedShipment.setServiceType(shippingAccount.getServiceCode());
        requestedShipment.setPackagingType(FedExValue.PACKAGING_TYPE_DEFAULT);

        Shipper shipper = new Shipper();
        String shipperName = userInfo.getPartner().getName();
        String shipperPhone = userInfo.getPartner().getPhone();
        shipper.setContact(new Contact(shipperName, null, shipperPhone));
        Address shipperAddress = new Address();
        shipperAddress.setStreetLines(shipCreateReq.getFromAddress());
        shipperAddress.setCity(shipCreateReq.getFromCity());
        shipperAddress.setStateProvinceCode(shipCreateReq.getFromState());
        shipperAddress.setPostalCode(shipCreateReq.getFromZipCode());
        shipper.setAddress(shipperAddress);
        requestedShipment.setShipper(shipper);

        Recipient recipient = new Recipient();
        recipient.setContact(new Contact(
                null,
                FedExValue.BBB_NAME_DEFAULT,
                shippingAccount.getPhone()
        ));
        recipient.setAddress(buildBBBAddress(warehouse.getShippingAddress()));
        requestedShipment.setRecipient(recipient);

        ShippingChargesPayment chargesPayment = new ShippingChargesPayment();
        chargesPayment.setPaymentType(FedExValue.PAYMENT_TYPE_DEFAULT);
        chargesPayment.setPayor(new Payor(new ResponsibleParty(shippingAccount.getAccountNumber())));
        requestedShipment.setShippingChargesPayment(chargesPayment);

        requestedShipment.setFedexLabelSpecification(fedexLabelSpecification);

        RequestedPackageLineItems packageItems = new RequestedPackageLineItems();
        packageItems.setWeight(new Weight(FedExValue.WEIGHT_UNIT, shipCreateReq.getWeight()));
        packageItems.setDimensions(new Dimensions(
                Math.round(shipCreateReq.getLength()),
                Math.round(shipCreateReq.getWidth()),
                Math.round(shipCreateReq.getHeight()),
                FedExValue.LENGTH_UNIT)
        );
        requestedShipment.setRequestedPackageLineItems(packageItems);

        fedexRequest.setRequestedShipment(requestedShipment);

        return createShip(fedexRequest, shippingAccount);
    }

    public FedexProcessShipResponse createShip(FedexProcessShipRequest request, TradeInShippingAccount shippingAccount) {
        WebAuthenticationDetail webAuthenticationDetail = buildWebAuthentication(shippingAccount);
        ClientDetail clientDetail = buildClientDetail(shippingAccount);
        Version version = new Version(FedExValue.SERVICE_ID_SHIP, FedExValue.SHIP_VERSION_MAJOR);

        request.setWebAuthenticationDetail(webAuthenticationDetail);
        request.setClientDetail(clientDetail);
        request.setVersion(version);

        return fedexClient.processShipment(request);
    }

    public FedexRateResponse rateShip(
            TradeInCalculateShipRequest request,
            TradeInShippingAccount shippingAccount,
            WarehouseResponse warehouse
    ) {
        FedexRateRequest fedexRequest = new FedexRateRequest();

//        SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");
        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setShipTimestamp(LocalDateTime.now());
        requestedShipment.setDropoffType(FedExValue.DROP_OFF_TYPE_DEFAULT);
        requestedShipment.setServiceType(shippingAccount.getServiceCode());
        requestedShipment.setPackagingType(FedExValue.PACKAGING_TYPE_DEFAULT);


        Address shipperAddress = new Address();
        shipperAddress.setStreetLines(request.getFromAddress());
        shipperAddress.setCity(request.getFromCity());
        shipperAddress.setStateProvinceCode(request.getFromState());
        shipperAddress.setPostalCode(request.getFromZipCode());
        Shipper shipper = new Shipper();
        shipper.setAddress(shipperAddress);
        //doesnt need contact for rating
        requestedShipment.setShipper(shipper);

        Recipient recipient = new Recipient();
        recipient.setAddress(buildBBBAddress(warehouse.getShippingAddress()));
        requestedShipment.setRecipient(recipient);

        RequestedPackageLineItems packageItems = new RequestedPackageLineItems();
        packageItems.setWeight(new Weight(FedExValue.WEIGHT_UNIT, request.getWeight()));
        packageItems.setDimensions(new Dimensions(
                Math.round(request.getLength()),
                Math.round(request.getWidth()),
                Math.round(request.getHeight()),
                FedExValue.LENGTH_UNIT)
        );
        requestedShipment.setRequestedPackageLineItems(packageItems);

        fedexRequest.setRequestedShipment(requestedShipment);
        return rateShip(fedexRequest, shippingAccount);
    }

    public FedexRateResponse rateShip(FedexRateRequest request, TradeInShippingAccount shippingAccount) {
        WebAuthenticationDetail webAuthenticationDetail = buildWebAuthentication(shippingAccount);
        ClientDetail clientDetail = buildClientDetail(shippingAccount);
        Version version = new Version(FedExValue.SERVICE_ID_RATE, FedExValue.RATE_VERSION_MAJOR);

        request.setWebAuthenticationDetail(webAuthenticationDetail);
        request.setClientDetail(clientDetail);
        request.setVersion(version);

        return fedexClient.rate(request);
    }

    public FedexTrackResponse getTrack(String trackingNumber) {
        SelectionDetails selectionDetails = new SelectionDetails();
        PackageIdentifier packageIdentifier = new PackageIdentifier(FedExValue.TRACK_IDENTIFIER_TYPE_DEFAULT, trackingNumber);
        selectionDetails.setPackageIdentifier(packageIdentifier);

        FedexTrackRequest fedexRequest = new FedexTrackRequest();
        fedexRequest.setSelectionDetails(selectionDetails);

        return getTrack(fedexRequest);
    }

    public FedexTrackResponse getTrack(FedexTrackRequest request) {
        TradeInShippingAccount shippingAccount = shippingAccountRepository.findOnePreferActive(CarrierType.FEDEX.getValue());

        WebAuthenticationDetail webAuthenticationDetail = buildWebAuthentication(shippingAccount);
        ClientDetail clientDetail = buildClientDetail(shippingAccount);

        Version version = new Version(FedExValue.SERVICE_ID_TRACK, FedExValue.TRACK_VERSION_MAJOR);

        request.setWebAuthenticationDetail(webAuthenticationDetail);
        request.setClientDetail(clientDetail);
        request.setVersion(version);
        request.setProcessingOptions(FedExValue.TRACK_OPTION_FULL_EVENTS);

        return fedexClient.getTrack(request);
    }

    public FedexDeleteShipResponse cancelShip(TradeInShipping tradeInShipping) {
        TrackingId trackingId = new TrackingId();
        trackingId.setTrackingIdType(tradeInShipping.getFedexTrackingIdType());
        trackingId.setTrackingNumber(tradeInShipping.getTrackingNumber());

        FedexDeleteShipRequest fedexRequest = new FedexDeleteShipRequest();
        fedexRequest.setTrackingId(trackingId);
        fedexRequest.setDeletionControl(FedExValue.DELETION_CONTROL_DEFAULT);

        return cancelShip(fedexRequest);
    }

    public FedexDeleteShipResponse cancelShip(FedexDeleteShipRequest request) {
        TradeInShippingAccount shippingAccount = shippingAccountRepository.findOnePreferActive(CarrierType.FEDEX.getValue());

        WebAuthenticationDetail webAuthenticationDetail = buildWebAuthentication(shippingAccount);
        ClientDetail clientDetail = buildClientDetail(shippingAccount);
        Version version = new Version(FedExValue.SERVICE_ID_SHIP, FedExValue.SHIP_VERSION_MAJOR);

        request.setWebAuthenticationDetail(webAuthenticationDetail);
        request.setClientDetail(clientDetail);
        request.setVersion(version);

        return fedexClient.deleteShip(request);
    }

    private WebAuthenticationDetail buildWebAuthentication(TradeInShippingAccount shippingAccount) {
        UserCredential userCredential = new UserCredential(
                shippingAccount.getApiKey(),
                shippingAccount.getApiPassword()
        );

        WebAuthenticationDetail webAuthenticationDetail = new WebAuthenticationDetail();
        webAuthenticationDetail.setUserCredential(userCredential);

        return webAuthenticationDetail;
    }

    private ClientDetail buildClientDetail(TradeInShippingAccount shippingAccount) {
        ClientDetail clientDetail = new ClientDetail(
                shippingAccount.getAccountNumber(),
                shippingAccount.getMeterNumber()
        );

        return clientDetail;
    }

//    private Address buildBBBAddress(TradeInShippingAccount shippingAccount) {
//        Address bbb = new Address();
//        bbb.setStreetLines(shippingAccount.getAddressLine());
//        bbb.setCity(shippingAccount.getCity());
//        bbb.setStateProvinceCode(shippingAccount.getStateCode());
//        bbb.setPostalCode(shippingAccount.getZipCode());
//        return bbb;
//    }
    private Address buildBBBAddress(ShippingAddressResponse shippingAddress) {
        Address bbb = new Address();
        bbb.setStreetLines(shippingAddress.getAddress());
        bbb.setCity(shippingAddress.getCity());
        bbb.setStateProvinceCode(shippingAddress.getState());
        bbb.setPostalCode(shippingAddress.getZipCode());
        bbb.setCountryCode(shippingAddress.getCountryCode());
        return bbb;
    }
}
