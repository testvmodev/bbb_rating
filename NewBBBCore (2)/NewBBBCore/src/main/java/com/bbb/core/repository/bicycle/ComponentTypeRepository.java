package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.ComponentType;
import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ComponentTypeRepository extends JpaRepository<ComponentType, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 component_type.id FROM component_type WHERE component_type.name = :name")
    Long findIdByName(@Param(value = "name") String name);

    @Query(nativeQuery = true, value = "SELECT * FROM component_type WHERE is_delete = 0 " +
            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type.name END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentTypeSortField).NAME.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type.name END DESC ")
    List<ComponentType> findAll(@Param("sortType")Sort sortType, @Param("sortField")ComponentTypeSortField sortField);

    @Query(nativeQuery = true, value = "SELECT * FROM component_type WHERE is_delete = 0 AND id =:typeId ")
    ComponentType findById(@Param("typeId") long typeId);

    @Query(nativeQuery = true, value = "SELECT * FROM component_type WHERE is_delete = 0 AND name = :name ")
    ComponentType findByName(@Param("name")String name);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM component_type WHERE is_delete = 0 AND for_only_bicycle_type_id =:typeId")
    int getCountByType(@Param("typeId") long typeId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM component_type WHERE is_delete = 0 AND component_category_id =:cateId")
    int getCountByCateId(@Param("cateId") long cateId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM component_type " +
            "WHERE is_delete = 0"
    )
    int countAll();
}
