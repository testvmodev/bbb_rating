package com.bbb.core.common.utils.s3;

import org.springframework.web.multipart.MultipartFile;

public class S3UploadImage<T> {
    private String filePrefix;
    private String folder;
    private MultipartFile file;
    private T otherData;
    public S3UploadImage(String filePrefix, String folder, MultipartFile file) {
        this.filePrefix = filePrefix;
        this.folder = folder;
        this.file = file;
    }

    public S3UploadImage(String filePrefix, String folder, MultipartFile file, T otherData) {
        this.filePrefix = filePrefix;
        this.folder = folder;
        this.file = file;
        this.otherData = otherData;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFilePrefix() {
        return filePrefix;
    }

    public void setFilePrefix(String filePrefix) {
        this.filePrefix = filePrefix;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public T getOtherData() {
        return otherData;
    }

    public void setOtherData(T otherData) {
        this.otherData = otherData;
    }
}
