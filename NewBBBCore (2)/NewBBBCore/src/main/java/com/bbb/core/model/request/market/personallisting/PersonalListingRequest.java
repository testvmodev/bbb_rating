package com.bbb.core.model.request.market.personallisting;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.BicycleSize;
import com.bbb.core.model.database.BicycleType;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.bbb.core.model.response.ObjectError;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.List;

public class PersonalListingRequest {
    @NotNull
    private String brandName;
    @NotNull
    private String modelName;
    @NotNull
    private String yearName;
    @NotNull
    private Long bicycleTypeId;
//    @NotNull
//    private Long bicycleSizeId;
    @NotNull
    private List<CompRequest> compRequests;
    private String description;
    @NotNull
    private ConditionInventory condition;
    @NotNull
    private Float salePrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    @NotNull
    private String emailPaypal;
    @NotNull
    private Boolean isLocalPickupShipping;
    private Boolean isRequireInsuranceShipping;
    private Float flatRate;
    @NotNull
    private String zipCode;
    @NotNull
    private Boolean isBestOffer;
    private OutboundShippingType shippingType;
    @NotNull @NotBlank
    private String country;
    @NotNull @NotBlank
    private String state;
    @NotNull @NotBlank
    private String cityName;
    @NotNull @NotBlank
    private String addressLine;
    private List<Long> myListingImageIds;

    private Float eBikeMileage;
    private Float eBikeHours;

    //result after validate
    @JsonIgnore
    private BicycleSize bicycleSize;
    @JsonIgnore
    private BicycleType bicycleType;
    @JsonIgnore
    private String countryCode;
    @JsonIgnore
    private String stateCode;
    @Transient
    private Boolean needValidate;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

//    public Long getBicycleSizeId() {
//        return bicycleSizeId;
//    }

//    public void setBicycleSizeId(Long bicycleSizeId) {
//        this.bicycleSizeId = bicycleSizeId;
//    }

    public List<CompRequest> getCompRequests() {
        return compRequests;
    }

    public void setCompRequests(List<CompRequest> compRequests) {
        this.compRequests = compRequests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getEmailPaypal() {
        return emailPaypal;
    }

    public void setEmailPaypal(String emailPaypal) {
        this.emailPaypal = emailPaypal;
    }

    public Boolean getLocalPickupShipping() {
        return isLocalPickupShipping;
    }

    public void setLocalPickupShipping(Boolean localPickupShipping) {
        isLocalPickupShipping = localPickupShipping;
    }

    public Boolean getRequireInsuranceShipping() {
        return isRequireInsuranceShipping;
    }

    public void setRequireInsuranceShipping(Boolean requireInsuranceShipping) {
        isRequireInsuranceShipping = requireInsuranceShipping;
    }

    public Float getFlatRate() {
        return flatRate;
    }

    public void setFlatRate(Float flatRate) {
        this.flatRate = flatRate;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        if (zipCode != null) {
            zipCode = zipCode.trim();
        }
        this.zipCode = zipCode;
    }

    public Boolean getIsBestOffer() {
        return isBestOffer;
    }

    public void setIsBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (country != null) {
            country = country.trim();
        }
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        if (state != null) {
            state = state.trim();
        }
        this.state = state;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        if (cityName != null) {
            cityName = cityName.trim();
        }
        this.cityName = cityName;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        if (addressLine != null) {
            addressLine = addressLine.trim();
        }
        this.addressLine = addressLine;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    @JsonIgnore
    public BicycleSize getBicycleSize() {
        return bicycleSize;
    }

    @JsonIgnore
    public void setBicycleSize(BicycleSize bicycleSize) {
        this.bicycleSize = bicycleSize;
    }

    @JsonIgnore
    public BicycleType getBicycleType() {
        return bicycleType;
    }

    @JsonIgnore
    public void setBicycleType(BicycleType bicycleType) {
        this.bicycleType = bicycleType;
    }

    @JsonIgnore
    public String getCountryCode() {
        return countryCode;
    }

    @JsonIgnore
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonIgnore
    public String getStateCode() {
        return stateCode;
    }

    @JsonIgnore
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @JsonIgnore
    public Boolean getNeedValidate() {
        return needValidate;
    }

    @JsonProperty
    public void setNeedValidate(Boolean needValidate) {
        this.needValidate = needValidate;
    }

    @JsonIgnore
    public List<Long> getMyListingImageIds() {
        return myListingImageIds;
    }

    public void setMyListingImageIds(List<Long> myListingImageIds) {
        this.myListingImageIds = myListingImageIds;
    }

    public void checkNullSomeEmpty() throws ExceptionResponse {
//        @NotNull
//        private String brandName;
//        @NotNull
//        private String modelName;
//        @NotNull
//        private String yearName;
        if (
                emailPaypal == null || emailPaypal.trim().equals("") ||
                        brandName == null || brandName.trim().equals("") ||
                        modelName== null || modelName.trim().equals("") ||
                        yearName == null || yearName.trim().equals("") ||
                        zipCode== null|| zipCode.trim().equals("")||
                        salePrice == null
        ){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM,
                    "emailPaypal AND brandName AND modelName AND modelName AND yearName AND salePrice AND zipCode must not empty"),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
