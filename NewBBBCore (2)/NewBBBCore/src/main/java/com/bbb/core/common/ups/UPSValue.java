package com.bbb.core.common.ups;

public interface UPSValue {
    String USERNAME = "BBB-San Jose";
    String PASSWORD = "1590Berryessa";
    String ACCESS_TOKEN = "5CB926735B76950A";
    String ACCOUNT_ID = "R89257";

    String BBB_NAME_DEFAULT = "Bicycle blue book";
    String BBB_ADDRESS_LINE_DEFAULT = "2240 Paragon Drive";
    String BBB_CITY_DEFAULT = "San Jose";
    String BBB_STATE_PROVINCE_CODE_DEFAULT = "CA";
    String BBB_POSTAL_CODE_DEFAULT = "95127";
    String BBB_COUNTRY_CODE_DEFAULT = "US";

    String IMAGE_FORMAT = "PNG";
    String PACKAGE_TYPE_DEFAULT = "02"; //(Other Package)
    String SHIPPING_SERVICE_DEFAULT = "03"; //(Ground)
    String PAYMENT_CHARGE_TYPE_DEFAULT = "01"; //(Transportation)
    String SHIP_REQUEST_CONFIG_DEFAULT = "validate"; //(validate street, city, postal)
    String RATE_REQUEST_CONFIG_DEFAULT = "rate";
    String TRACK_REQUEST_CONFIG_DEFAULT = "01"; //(all activities)

    String SHIP_FROM_CLIENT_NAME = "From client";

    String SHIP = "/Ship";
    String PICKUP = "/Pickup";
    String TRACKING = "/Track";

    //development testing only
    String BASE_TEST = "https://wwwcie.ups.com/rest";
    String URL_SHIP_TEST = BASE_TEST + SHIP;
    String URL_PICKUP_TEST = BASE_TEST + PICKUP;
    String URL_TRACKING_TEST = BASE_TEST + TRACKING;

    //production ready
    String BASE_ENDPOINT = "https://onlinetools.ups.com/rest";
    String URL_SHIP = BASE_ENDPOINT + SHIP;
    String URL_PICKUP = BASE_ENDPOINT + PICKUP;
    String URL_TRACKING = BASE_ENDPOINT + TRACKING;
}
