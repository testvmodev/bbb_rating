package com.bbb.core.model.otherservice.response.shipping;

import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentType;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class ShipmentResponse {
    private long id;
    private Long inventoryId;
    private Long tradeInId;
    private ShipmentType shipmentType;
    private InboundShippingType inboundShippingType;
    private OutboundShippingType outboundShippingType;
    private CarrierType carrierType;
    private String manualCarrier;
    private Long shippingConfigId;
    private String trackingNumber;
    private boolean isValidTrackingNumber;
    private String fedexTrackingIdType;
    private Float billingWeight;
    private Float totalChargeAmount;
    private String totalChargeCurrency;
    private String s3PathLabel;
    private String fullLinkLabel;
    private String lastTrackingStatusCode;
    private String lastTrackingStatusDescription;
    private LocalDateTime lastTrackingStatusUpdateTime;
    private Boolean isScheduled;
    private LocalDate scheduleDate;
    private Boolean isDelivered;
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(Long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public ShipmentType getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(ShipmentType shipmentType) {
        this.shipmentType = shipmentType;
    }

    public InboundShippingType getInboundShippingType() {
        return inboundShippingType;
    }

    public void setInboundShippingType(InboundShippingType inboundShippingType) {
        this.inboundShippingType = inboundShippingType;
    }

    public OutboundShippingType getOutboundShippingType() {
        return outboundShippingType;
    }

    public void setOutboundShippingType(OutboundShippingType outboundShippingType) {
        this.outboundShippingType = outboundShippingType;
    }

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getManualCarrier() {
        return manualCarrier;
    }

    public void setManualCarrier(String manualCarrier) {
        this.manualCarrier = manualCarrier;
    }

    public Long getShippingConfigId() {
        return shippingConfigId;
    }

    public void setShippingConfigId(Long shippingConfigId) {
        this.shippingConfigId = shippingConfigId;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public boolean isValidTrackingNumber() {
        return isValidTrackingNumber;
    }

    public void setValidTrackingNumber(boolean validTrackingNumber) {
        isValidTrackingNumber = validTrackingNumber;
    }

    public String getFedexTrackingIdType() {
        return fedexTrackingIdType;
    }

    public void setFedexTrackingIdType(String fedexTrackingIdType) {
        this.fedexTrackingIdType = fedexTrackingIdType;
    }

    public Float getBillingWeight() {
        return billingWeight;
    }

    public void setBillingWeight(Float billingWeight) {
        this.billingWeight = billingWeight;
    }

    public Float getTotalChargeAmount() {
        return totalChargeAmount;
    }

    public void setTotalChargeAmount(Float totalChargeAmount) {
        this.totalChargeAmount = totalChargeAmount;
    }

    public String getTotalChargeCurrency() {
        return totalChargeCurrency;
    }

    public void setTotalChargeCurrency(String totalChargeCurrency) {
        this.totalChargeCurrency = totalChargeCurrency;
    }

    public String getS3PathLabel() {
        return s3PathLabel;
    }

    public void setS3PathLabel(String s3PathLabel) {
        this.s3PathLabel = s3PathLabel;
    }

    public String getFullLinkLabel() {
        return fullLinkLabel;
    }

    public void setFullLinkLabel(String fullLinkLabel) {
        this.fullLinkLabel = fullLinkLabel;
    }

    public String getLastTrackingStatusCode() {
        return lastTrackingStatusCode;
    }

    public void setLastTrackingStatusCode(String lastTrackingStatusCode) {
        this.lastTrackingStatusCode = lastTrackingStatusCode;
    }

    public String getLastTrackingStatusDescription() {
        return lastTrackingStatusDescription;
    }

    public void setLastTrackingStatusDescription(String lastTrackingStatusDescription) {
        this.lastTrackingStatusDescription = lastTrackingStatusDescription;
    }

    public LocalDateTime getLastTrackingStatusUpdateTime() {
        return lastTrackingStatusUpdateTime;
    }

    public void setLastTrackingStatusUpdateTime(LocalDateTime lastTrackingStatusUpdateTime) {
        this.lastTrackingStatusUpdateTime = lastTrackingStatusUpdateTime;
    }

    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Boolean getDelivered() {
        return isDelivered;
    }

    public void setDelivered(Boolean delivered) {
        isDelivered = delivered;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
