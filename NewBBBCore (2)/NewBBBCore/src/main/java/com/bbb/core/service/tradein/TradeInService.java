package com.bbb.core.service.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.s3.S3DownloadImage;
import com.bbb.core.model.request.bicycle.BicycleFromBrandYearModelRequest;
import com.bbb.core.model.request.tradein.tradin.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface TradeInService {
//    Object createTradeIn(TradeInCreateRequest request) throws ExceptionResponse;

    Object postTradeIn(long tradeInId, List<MultipartFile> images, boolean isSave) throws ExceptionResponse;

    S3DownloadImage getTradeInImage(String keyHash) throws ExceptionResponse;

    Object calcShipping(TradeInCalculateShipRequest request) throws ExceptionResponse;

    Object createShipping(TradeInShippingCreateRequest request) throws ExceptionResponse;

    Object getTradeInBicycleDetail(BicycleFromBrandYearModelRequest request) throws ExceptionResponse;

    Object getAllTradeInReasonDecline();

    Object declineTradeIn(DeclineTradeInRequest request) throws ExceptionResponse;

    Object createTradeIn(TradeInCreateRequestTwo request) throws ExceptionResponse, MethodArgumentNotValidException;

    Object updateStepDetail(long tradeInId, TradeInUpdateDetailRequest request) throws ExceptionResponse;

    Object getTradeInDetail(long tradeInId) throws ExceptionResponse;

    Object updateStepSummary(long tradeInId, TradeInSummaryRequest request) throws ExceptionResponse;

    Object getTradeIns(TradeInsRequest request) throws ExceptionResponse;

    Object getAllStatusTradeIn();

    Object getDetailTradeIn(long tradeId, int indexStep) throws ExceptionResponse;

    Object archiveTradeIn(long tradeInId, boolean isArchive) throws ExceptionResponse;

    Object deleteImageTradeIn(long tradeId, List<Long> imageImageTypeIds, List<Long> imageIds) throws ExceptionResponse;

    Object getTradeInCostCalculator(float privateParty) throws ExceptionResponse;

    Object cancelTradeIn(long tradeInId, boolean isCancel) throws ExceptionResponse;

    Object cancelTradeIns(List<Long> tradeInIds, boolean isCancel) throws ExceptionResponse;

    Object getTradeInCommon(long tradeInId) throws ExceptionResponse;

    Object getCountIncomplete() throws ExceptionResponse;
}
