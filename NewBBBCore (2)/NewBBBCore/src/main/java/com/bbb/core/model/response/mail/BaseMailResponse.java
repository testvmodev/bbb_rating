package com.bbb.core.model.response.mail;

public class BaseMailResponse {
    private String mail;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
