package com.bbb.core.repository.bicycle.out;

import com.bbb.core.model.response.bicycle.BicycleComponentResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface BicycleComponentResponseRepository extends JpaRepository<BicycleComponentResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_component.id AS id, " +
            "bicycle_component.component_type_id AS component_type_id, " +
            "component_type.name AS component_name, " +
            "bicycle_component.component_value AS component_value, " +
            "component_category.name AS category_name, " +
            "bicycle_type.id AS bicycle_type_id, " +
            "bicycle_type.name AS bicycle_type_name " +

            "FROM bicycle_component " +

            "JOIN component_type ON bicycle_component.component_type_id = component_type.id " +
            "JOIN component_category ON component_type.component_category_id = component_category.id " +
            "JOIN bicycle ON bicycle.id = :bicycleId " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +

            "WHERE bicycle_component.bicycle_id = :bicycleId " +
            "AND (component_type.is_delete IS NULL OR component_type.is_delete != 1) " +
            "AND (component_type.for_only_bicycle_type_id IS NULL OR component_type.for_only_bicycle_type_id = bicycle.type_id) " +
            "ORDER BY component_type.sort_order DESC"
    )
    List<BicycleComponentResponse> findAllByBicycleId(@Param(value = "bicycleId") long bicycleId);
}
