package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInCancel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeInCancelRepository extends JpaRepository<TradeInCancel, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_cancel " +
            "WHERE " +
            "trade_in_cancel.trade_in_id=:tradeInId AND trade_in_cancel.in_active = :inActive")
    List<TradeInCancel> findAllByInActive(
            @Param(value = "tradeInId") long tradeInId,
            @Param(value = "inActive") boolean inActive
    );

    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_cancel " +
            "WHERE " +
            "trade_in_cancel.trade_in_id IN :tradeInIds AND trade_in_cancel.in_active = :inActive")
    List<TradeInCancel> findAllByInActive(
            @Param(value = "tradeInIds") List<Long> tradeInIds,
            @Param(value = "inActive") boolean inActive
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM trade_in_cancel " +
            "WHERE trade_in_cancel.trade_in_id = :tradeInId " +
            "AND trade_in_cancel.in_active = :inActive " +
            "ORDER BY trade_in_cancel.id DESC "
    )
    TradeInCancel findLast(
            @Param(value = "tradeInId") long tradeInId,
            @Param(value = "inActive") boolean inActive
    );
}
