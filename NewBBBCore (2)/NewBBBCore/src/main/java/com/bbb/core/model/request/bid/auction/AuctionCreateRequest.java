package com.bbb.core.model.request.bid.auction;

import org.joda.time.LocalDateTime;

public class AuctionCreateRequest {
    private LocalDateTime startDate;
    private LocalDateTime endDate;
//    private String createById;
    private String name;


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

//    public String getCreateById() {
//        return createById;
//    }
//
//    public void setCreateById(String createById) {
//        this.createById = createById;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
