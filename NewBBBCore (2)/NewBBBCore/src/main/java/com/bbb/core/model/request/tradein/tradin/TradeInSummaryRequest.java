package com.bbb.core.model.request.tradein.tradin;

import com.bbb.core.model.response.tradein.detail.TradeInOwner;
import com.bbb.core.model.response.tradein.detail.TradeInProof;

public class TradeInSummaryRequest {
    private boolean isSave;
    private TradeInOwner owner;
    private TradeInProof proof;

    public TradeInOwner getOwner() {
        return owner;
    }

    public void setOwner(TradeInOwner owner) {
        this.owner = owner;
    }

    public TradeInProof getProof() {
        return proof;
    }

    public void setProof(TradeInProof proof) {
        this.proof = proof;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }
}
