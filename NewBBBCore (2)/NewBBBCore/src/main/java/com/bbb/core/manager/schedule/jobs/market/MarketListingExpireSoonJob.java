package com.bbb.core.manager.schedule.jobs.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.ExpirationTimeConfig;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.MarketListingExpire;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.schedule.ScheduleSendMailListingResponse;
import com.bbb.core.repository.expirationtime.ExpirationTimeConfigRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.MarketListingExpireRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.CRON, defaultCron = "0 0 0 * * *")
public class MarketListingExpireSoonJob extends ScheduleJob {
    @Autowired
    private ExpirationTimeConfigRepository expirationTimeConfigRepository;
    @Autowired
    private MarketListingExpireRepository marketListingExpireRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private NotificationManager notificationManager;

    @Override
    public void run() {
        ExpirationTimeConfig firstListExpireTime = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.PERSONAL_LISTING_FIRST_LIST);
        ExpirationTimeConfig reListExpireTime = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.PERSONAL_LISTING_RENEW);

        if (firstListExpireTime == null || reListExpireTime == null) {
            setCustomLogString("Missing expiration time config for personal listing. Setup it before enable this job");
            return;
        }

        List<MarketListingExpire> expireSoon = marketListingExpireRepository.findExpireSoon(
                firstListExpireTime.getExpireAfterHours(),
                false
        );
        expireSoon.addAll(marketListingExpireRepository.findExpireSoon(
                reListExpireTime.getExpireAfterHours(),
                true
        ));

        if (expireSoon.size() > 0) {
            List<Long> marketListingIds = expireSoon.stream()
                    .filter(expire -> expire.getExpireSoonFailCount() == null ||
                            expire.getExpireSoonFailCount() < Constants.MAX_TRY_SEND_MAIL)
                    .map(MarketListingExpire::getId)
                    .collect(Collectors.toList());
            List<MarketListing> marketListings = marketListingRepository.findAllByIds(marketListingIds);

            List<Long> inventoryIds = marketListings.stream()
                    .map(MarketListing::getInventoryId)
                    .collect(Collectors.toList());
            List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIds);

            List<Observable<ScheduleSendMailListingResponse>> obSendMail = createObserverSendMail(marketListings, inventories);
            List<ScheduleSendMailListingResponse> responses;
//            List<SendingMailResponse> successResponses = new ArrayList();
            List<MarketListingExpire> successExpires = new ArrayList();
            List<MarketListingExpire> failExpires = new ArrayList();
            List<String> errorResponses = new ArrayList();
            responses = Observable.zip(obSendMail, result -> {
                List<ScheduleSendMailListingResponse> scheduleResponses = new ArrayList<>();
                for (Object o : result) {
                    if (o != null) {
                        ScheduleSendMailListingResponse response = (ScheduleSendMailListingResponse) o;
                        scheduleResponses.add(response);
                    }
                }

                return scheduleResponses;
            }).blockingFirst();

            Map<Long, MarketListingExpire> temp = expireSoon.stream()
                    .collect(Collectors.toMap(MarketListingExpire::getId, e -> e));
            for (ScheduleSendMailListingResponse response : responses) {
                if (response.getRawErrorResponse() == null) {
//                    successResponses.add(response.getSendingMailResponse());
                    MarketListingExpire expire = temp.get(response.getMarketListingId());
                    expire.setLastMailExpireSoon(LocalDateTime.now());
                    successExpires.add(expire);
                } else {
                    failExpires.add(temp.get(response.getMarketListingId()));
                    errorResponses.add(response.getRawErrorResponse());
                }
            }

            //exceed error limit, ignore
            expireSoon.stream()
                    .filter(expire -> expire.getExpireSoonFailCount() != null &&
                            expire.getExpireSoonFailCount() >= Constants.MAX_TRY_SEND_MAIL)
                    .forEach(expire -> successExpires.add(expire));

            if (successExpires.size() > 0) {
                marketListingExpireRepository.saveAll(successExpires);
            }

            if (failExpires.size() > 0) {
                failExpires.forEach(expire -> {
                    Integer count = expire.getExpireSoonFailCount();
                    count = count == null ? 1 : count + 1;
                    expire.setExpireSoonFailCount(count);
                });
                marketListingExpireRepository.saveAll(failExpires);
                //log as error
                this.setCustomLogString(String.join("\n", errorResponses));
                this.setFail(true);
            }
        }
    }

    public void onFail(Throwable throwable) {}

    private List<Observable<ScheduleSendMailListingResponse>> createObserverSendMail(
            List<MarketListing> marketListings, List<Inventory> inventories
    ) {
        List<Observable<ScheduleSendMailListingResponse>> obSendMail = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(4);

        List<Pair<MarketListing, Inventory>> contents = new ArrayList<>();
        marketListings.forEach(listing -> {
            Pair<MarketListing, Inventory> pair = new Pair<>();
            pair.setFirst(listing);
            for (Inventory inven : inventories) {
                if (inven.getId() == listing.getInventoryId()) {
                    pair.setSecond(inven);
                    break;
                }
            }
            contents.add(pair);
        });

        contents.forEach(pair -> {
            obSendMail.add(Observable.create((ObservableOnSubscribe<ScheduleSendMailListingResponse>) su -> {
                ScheduleSendMailListingResponse scheduleResponse = new ScheduleSendMailListingResponse();
                try {
                    SendingMailResponse sendingMailResponse = notificationManager.sendMailListingExpireWarn(
                            pair.getFirst(),
                            pair.getSecond()
                    );
                    scheduleResponse.setSendingMailResponse(sendingMailResponse);
                } catch (HttpClientErrorException e) {
                    scheduleResponse.setRawErrorResponse(e.getResponseBodyAsString());
                    e.printStackTrace();
                } finally {
                    scheduleResponse.setMarketListingId(pair.getFirst().getId());
                }
                su.onNext(scheduleResponse);
                su.onComplete();
            }).subscribeOn(Schedulers.from(executor)));
        });

        return obSendMail;
    }
}
