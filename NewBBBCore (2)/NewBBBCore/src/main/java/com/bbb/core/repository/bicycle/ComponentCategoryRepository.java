package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.ComponentCategory;
import com.bbb.core.model.request.sort.ComponentCategorySortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ComponentCategoryRepository extends JpaRepository<ComponentCategory, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM component_category " +
            "WHERE name = :name AND is_delete = 0"
    )
    ComponentCategory findOne(@Param("name") String name);

    @Query(nativeQuery = true, value = "SELECT * FROM component_category WHERE is_delete = 0")
    List<ComponentCategory> findAll ();

    @Query(nativeQuery = true, value = "SELECT * FROM component_category WHERE id=:cateId  AND is_delete = 0")
    ComponentCategory findById(@Param("cateId") long cateId);

    @Query(nativeQuery = true, value = "SELECT name FROM component_category  WHERE is_delete = 0")
    List<String> findListName();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM component_category " +
            "WHERE is_delete = 0 " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentCategorySortField).NAME.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_category.name END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentCategorySortField).NAME.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_category.name END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<ComponentCategory> findAllSorted(
            @Param("sortField") ComponentCategorySortField sortField,
            @Param("sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM component_category " +
            "WHERE is_delete = 0"
    )
    int countAll();
}
