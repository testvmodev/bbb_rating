package com.bbb.core.model.response.onlinestore.dashboard;

import org.hibernate.annotations.Immutable;
import org.joda.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
public class OnlineStoreDailySaleResponse {
    @Id
    private LocalDate date;
    private Float sale;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Float getSale() {
        return sale;
    }

    public void setSale(Float sale) {
        this.sale = sale;
    }
}
