package com.bbb.core.model.response.market.marketlisting;

import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.database.type.StatusMarketListing;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class MarketToList {
    @Id
    private long marketConfigId;
    private long marketPlaceId;
    private MarketType type;
    private String marketPlaceName;
    @Transient
    private StatusMarketListing status;

    public long getMarketConfigId() {
        return marketConfigId;
    }

    public void setMarketConfigId(long marketConfigId) {
        this.marketConfigId = marketConfigId;
    }

    public MarketType getType() {
        return type;
    }

    public void setType(MarketType type) {
        this.type = type;
    }

    public String getMarketPlaceName() {
        return marketPlaceName;
    }

    public void setMarketPlaceName(String marketPlaceName) {
        this.marketPlaceName = marketPlaceName;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public StatusMarketListing getStatus() {
        return status;
    }

    public void setStatus(StatusMarketListing status) {
        this.status = status;
    }

    public MarketToList clone(){
        MarketToList marketToList = new MarketToList();
        marketToList.marketConfigId = marketConfigId;
        marketToList.marketPlaceId = marketPlaceId;
        marketToList.type = type;
        marketToList.marketPlaceName = marketPlaceName;
        marketToList.status = status;
        return marketToList;
    }
}
