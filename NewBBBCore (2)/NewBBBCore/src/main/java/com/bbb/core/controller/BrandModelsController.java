//package com.bbb.core.controller;
//
//import com.bbb.core.model.BrandModels;
//import com.bbb.core.model.database.Inventory;
//import com.bbb.core.repository.BrandModelsRepository;
//import com.bbb.core.repository.inventory.InventoryRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.text.DecimalFormat;
//import java.util.List;
//
//@RestController
//public class BrandModelsController {
//    @Autowired
//    private BrandModelsRepository brandModelsRepository;
//
//    @Autowired
//    private InventoryRepository inventoryRepository;
//
//    @GetMapping(value = "/brandModels")
//    public ResponseEntity brandModels(Pageable pageable) {
////        List<BrandModels> brandModels = brandModelsRepository.finAllTem();
////        return new ResponseEntity <>(brandModels, HttpStatus.OK);
//        List<Inventory> inventories = inventoryRepository.findAllByIds();
//        DecimalFormat format = new DecimalFormat("##.##");
//        for (Inventory inventory : inventories) {
//            if (inventory.getCurrentListedPrice() != null) {
//                inventory.setMinimumOfferAutoAcceptPrice(Float.valueOf(format.format(inventory.getCurrentListedPrice()*0.6f)));
//                inventory.setBestOfferAutoAcceptPrice(Float.valueOf(format.format(inventory.getCurrentListedPrice()*0.95f)));
//            }
//
//        }
//        return new ResponseEntity<>(inventoryRepository.save(inventories), HttpStatus.OK);
//    }
//}
