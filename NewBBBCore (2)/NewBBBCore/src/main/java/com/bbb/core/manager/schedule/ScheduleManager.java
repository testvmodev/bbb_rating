package com.bbb.core.manager.schedule;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.SchedulerConfig;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.handler.JobFailHandler;
import com.bbb.core.manager.schedule.handler.JobLogHandler;
import com.bbb.core.manager.schedule.jobs.DelegatingJobLog;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.ScheduleJobConfig;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.schedule.ScheduleJobConfigRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Component
public class ScheduleManager {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleManager.class);
    private ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private SchedulerConfig schedulerConfig;
    @Autowired
    private ScheduleJobConfigRepository scheduleJobConfigRepository;
    @Autowired
    private JobLogHandler jobLogHandler;
    @Autowired
    private JobFailHandler jobFailHandler;
    private static ConcurrentMap<Class<? extends ScheduleJob>, Pair<ScheduleJob, ScheduledFuture>> jobs = new ConcurrentHashMap();
    private static Boolean shuttingDown = false;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        taskScheduler = schedulerConfig.threadPoolTaskScheduler();

        if (schedulerConfig.getEnable() == null || !schedulerConfig.getEnable()) {
            return;
        }

        Map<String, ScheduleJob> jobBeans = applicationContext.getBeansOfType(ScheduleJob.class);
        for (ScheduleJob job : jobBeans.values()) {
            loadJob(job);
        }


        //outdated config due to remove jobs from code or rename classes will be auto deleted
        if (schedulerConfig.getCleanUnused() != null && schedulerConfig.getCleanUnused()) {
            removeOldConfig();
        }
    }

    public List<ScheduleJobConfig> getRunningJobConfig() {
        return jobs.values().stream()
                .map(queue -> queue.getFirst().isRunning ? queue.getFirst().getConfig() : null)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<ScheduleJob> getInqueueJob() {
        return jobs.values().stream()
                .map(queue -> (queue.getFirst().isEnable() && !queue.getFirst().isRunning) ? queue.getFirst() : null)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public ScheduleJob getJob(String jobName) {
        Class classz = findJobByName(jobName);
        if (classz == null) {
            return null;
        } else {
            return jobs.get(classz).getFirst();
        }
    }

    public ScheduleJob getJob(Class<? extends ScheduleJob> classz) {
        if (jobs.containsKey(classz)) {
            return jobs.get(classz).getFirst();
        }
        return null;
    }

    public void updateConfig(ScheduleJobConfig config) throws ExceptionResponse {
        if (config == null) return;
        Pair<ScheduleJob, ScheduledFuture> queue;
        Class classz = findJobByName(config.getJobType());
        if (classz == null) return;
        else queue = jobs.get(classz);

        ScheduleJob job = queue.getFirst();

        ScheduleJobConfig oldConfig = job.getConfig();
        job.setConfig(config);
        if (config.isEnable()) {
            if (oldConfig.isEnable()) {
                job.buildTrigger();
                if (!job.isRunning) {
                    requeue(classz);
                }
            } else {
                jobs.get(classz).setSecond(enqueue(job));
            }
        } else {
            if (!cancel(job.getClass(), false)) {
                throw new ExceptionResponse(
                        ObjectError.UNKNOWN_ERROR,
                        MessageResponses.DEQUEUE_JOB_FAIL,
                        HttpStatus.INTERNAL_SERVER_ERROR
                );
            }
        }
        jobs.get(job.getClass()).setFirst(job);
    }

    public void shutDown() {
        synchronized (shuttingDown) {
            shuttingDown = true;
        }

        for (Pair<ScheduleJob, ScheduledFuture> queue : jobs.values()) {
            ScheduleJob job = queue.getFirst();
            ScheduledFuture future = queue.getSecond();

            if (future != null && !(future.isCancelled() || future.isDone()) && !job.isRunning) {
//                    (job.getConfig().getJobTimeType() == JobTimeType.FIX_RATE || !job.isRunning())) {
                boolean cancelSuccess = future.cancel(false);
                if (cancelSuccess) {
                    job.cancelTime = LocalDateTime.now();
                    jobLogHandler.logJobCancel(job);
                }
            }
            if (job.isRunning) {
                System.out.println(queue.getSecond().getClass().getName());
                System.out.println(queue.getSecond().getClass().getSimpleName() + " is running");

                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    jobFailHandler.handleError(e);
                }
            }
        }
    }

    public boolean isRunning(Class<? extends ScheduleJob> classz) {
        return jobs.containsKey(classz) && jobs.get(classz).getFirst().isRunning;
    }

    public boolean isRunning(String jobName) {
        Class classz = findJobByName(jobName);
        return classz != null && jobs.get(classz).getFirst().isRunning;
    }

    public static Boolean isShuttingDown() {
        synchronized (shuttingDown) {
            return shuttingDown;
        }
    }

    public boolean updateEnable(ScheduleJob job, boolean isEnable) {
        if (!jobs.containsKey(job.getClass())) return false;
        Pair<ScheduleJob, ScheduledFuture> queue = jobs.get(job.getClass());
        ScheduledFuture future = queue.getSecond();
        if (isEnable != job.getConfig().isEnable()) { //must diff with old config
            if (isEnable) {
                jobs.get(job.getClass()).setSecond(enqueue(job));
            } else {
                jobLogHandler.logJobCancel(job);
                return future.cancel(false);
            }
        }
        return true;
    }

    public void forceRun(Class<? extends ScheduleJob> clazz) {
        if (jobs.containsKey(clazz)) {
            ScheduleJob job =  jobs.get(clazz).getFirst();
            if (!job.isRunning) {
                if (cancel(clazz, false)) {
                    jobs.get(clazz).setSecond(enqueue(job, 0, true));
                }
            }
        }
    }

    private void loadJob(ScheduleJob job) {
        Class classz = findJobByName(job.getClass().getSimpleName());
        if ((classz != null && jobs.containsKey(classz)) || jobs.containsKey(job.getClass())) {
            throw new RuntimeException(MessageResponses.DUPLICATE_JOB);
        }

        Job setting = job.getClass().getAnnotation(Job.class);
        ScheduleJobConfig config = scheduleJobConfigRepository.findOne(job.getClass().getSimpleName());
        if (config == null) {
            config = new ScheduleJobConfig();
            config.setJobType(job.getClass().getSimpleName());
            config.setJobTimeType(JobTimeType.FIX_DELAY);
            if (setting != null) {
                config.setEnable(setting.preEnable());
                if (setting.defaultTimeType() != null) {
                    config.setJobTimeType(setting.defaultTimeType());
                }
                if (!StringUtils.isBlank(setting.defaultCron())
                        && CronSequenceGenerator.isValidExpression(setting.defaultCron())
                ) {
                    config.setJobTimeCron(setting.defaultCron());
                }
                if (setting.defaultTimeSeconds() > 0) {
                    config.setJobTimeSeconds(setting.defaultTimeSeconds());
                }
            }
            config = scheduleJobConfigRepository.save(config);
        }

        job.setConfig(config);
        Pair<ScheduleJob, ScheduledFuture> scheduler = new Pair();
        scheduler.setFirst(job);
        if (config.isEnable() && (setting == null || !setting.forceDisable())) {
            if (schedulerConfig.getFirstRunDelay() != null && schedulerConfig.getFirstRunDelay() > 0) {
                scheduler.setSecond(enqueue(job, schedulerConfig.getFirstRunDelay(), false));
            } else {
                scheduler.setSecond(enqueue(job));
            }
        }
        jobs.put(job.getClass(), scheduler);
    }

    private void requeue(Class<? extends ScheduleJob> classz) throws ExceptionResponse {
        if (!jobs.containsKey(classz)) return;
        if (!cancel(classz, false)) {
            throw new ExceptionResponse(
                    ObjectError.UNKNOWN_ERROR,
                    MessageResponses.DEQUEUE_JOB_FAIL,
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        jobs.get(classz).setSecond(enqueue(jobs.get(classz).getFirst()));
    }

    private ScheduledFuture enqueue(ScheduleJob job) {
        return enqueue(job, 0, false);
    }

    private ScheduledFuture enqueue(ScheduleJob job, int firstRunDelay, boolean forced) {
        job.buildTrigger(firstRunDelay, forced);
        return taskScheduler.schedule(new DelegatingJobLog(jobLogHandler, job), job);
    }

    private boolean cancel(Class<? extends ScheduleJob> classz, boolean force) {
        if (!jobs.containsKey(classz)) return false;
        Pair<ScheduleJob, ScheduledFuture> queue = jobs.get(classz);
        ScheduledFuture future = queue.getSecond();
        if (future == null || future.isCancelled() || future.isDone()) return true;

        return future.cancel(force);
    }

    private void removeOldConfig() {
        List<ScheduleJobConfig> configs = scheduleJobConfigRepository.findAll();
        if (configs.size() > jobs.size()) {
            List<ScheduleJobConfig> outdated = configs.stream()
                    .filter(config -> {
                        for (Class classz : jobs.keySet()) {
                            if (classz.getSimpleName().equals(config.getJobType())) {
                                return false;
                            }
                        }
                        return true;
                    })
                    .peek(config -> config.setDelete(true))
                    .collect(Collectors.toList());
            if (outdated.size() > 0) {
                scheduleJobConfigRepository.saveAll(outdated);
            }
        }
    }

    private Class<? extends ScheduleJob> findJobByName(String jobName) {
        for (Class<? extends ScheduleJob> classz : jobs.keySet()) {
            if (classz.getSimpleName().equals(jobName)) {
                return classz;
            }
        }
        return null;
    }
}
