package com.bbb.core.repository.market;

import com.bbb.core.model.database.LogEbayNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEbayNotificationRepository extends JpaRepository<LogEbayNotification, Long> {

}
