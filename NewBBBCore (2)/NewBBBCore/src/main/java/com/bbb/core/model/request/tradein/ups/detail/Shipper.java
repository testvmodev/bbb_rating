package com.bbb.core.model.request.tradein.ups.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Shipper extends ShipAddress {
    @JsonProperty("ShipperNumber")
    private String shipperNumber;

    public Shipper(String name, Address address, String shipperNumber) {
        super(name, address);
        this.shipperNumber = shipperNumber;
    }

    public String getShipperNumber() {
        return shipperNumber;
    }

    public void setShipperNumber(String shipperNumber) {
        this.shipperNumber = shipperNumber;
    }
}
