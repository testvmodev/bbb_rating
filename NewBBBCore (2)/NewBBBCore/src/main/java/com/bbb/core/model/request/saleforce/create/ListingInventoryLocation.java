package com.bbb.core.model.request.saleforce.create;

import javax.validation.constraints.NotNull;

public class ListingInventoryLocation {
    @NotNull
    private String zipCode;
    @NotNull
    private String country;
    @NotNull
    private String state;
    @NotNull
    private String cityName;
    @NotNull
    private String addressLine;
//    @NotNull
    private Boolean isAllowLocalPickup;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Boolean getAllowLocalPickup() {
        return isAllowLocalPickup;
    }

    public void setAllowLocalPickup(Boolean allowLocalPickup) {
        isAllowLocalPickup = allowLocalPickup;
    }
}
