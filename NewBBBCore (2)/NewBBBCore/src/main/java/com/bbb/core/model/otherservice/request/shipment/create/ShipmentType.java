package com.bbb.core.model.otherservice.request.shipment.create;

public enum ShipmentType {
    INBOUND("Inbound"),
    OUTBOUND("Outbound");

    private String value;

    ShipmentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
