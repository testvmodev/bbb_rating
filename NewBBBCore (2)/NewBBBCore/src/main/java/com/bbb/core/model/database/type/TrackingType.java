package com.bbb.core.model.database.type;

public enum  TrackingType {
    EBAY("Ebay"),
    BBB("BBB"),
    WHOLE_SALLER("Wholesaler"),
    BICYCLE("Bicycle");

    private final String value;

    TrackingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
