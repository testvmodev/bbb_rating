package com.bbb.core.repository.tradein;

import com.bbb.core.model.database.TradeInCustomQuoteCompDetail;
import com.bbb.core.model.database.embedded.TradeInCustomQuoteCompDetailId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TradeInCustomQuoteCompDetailRepository extends JpaRepository<TradeInCustomQuoteCompDetail, TradeInCustomQuoteCompDetailId> {
    @Query(nativeQuery = true, value = "SELECT * FROM trade_in_custom_quote_comp_detail WHERE trade_in_custom_quote_comp_detail.trade_in_custom_quote_id = :customQuoteId")
    List<TradeInCustomQuoteCompDetail> findAllByCustomQuote(
            @Param(value = "customQuoteId") long customQuoteId
    );

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "DELETE trade_in_custom_quote_comp_detail WHERE trade_in_custom_quote_comp_detail.trade_in_custom_quote_id = :customQuoteId")
    void deleteByCustomQuoteId(
            @Param(value = "customQuoteId") long customQuoteId
    );
}
