package com.bbb.core.model.request.ebay;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellingStatus {
    @JacksonXmlProperty(localName = "BidCount")
    private int bidCount;
    @JacksonXmlProperty(localName = "BidIncrement")
    private Currency bidIncrement;
    @JacksonXmlProperty(localName = "ConvertedCurrentPrice")
    private Currency convertedCurrentPrice;
    @JacksonXmlProperty(localName = "CurrentPrice")
    private Currency currentPrice;
    @JacksonXmlProperty(localName = "LeadCount")
    private int leadCount;
    @JacksonXmlProperty(localName = "MinimumToBid")
    private Currency minimumToBid;
    @JacksonXmlProperty(localName = "QuantitySold")
    private int quantitySold;
    @JacksonXmlProperty(localName = "ReserveMet")
    private boolean reserveMet;
    @JacksonXmlProperty(localName = "SecondChanceEligible")
    private boolean secondChanceEligible;
    @JacksonXmlProperty(localName = "ListingStatus")
    private String listingStatus;
    @JacksonXmlProperty(localName = "QuantitySoldByPickupInStore")
    private int quantitySoldByPickupInStore;

    public int getBidCount() {
        return bidCount;
    }

    public void setBidCount(int bidCount) {
        this.bidCount = bidCount;
    }

    public Currency getBidIncrement() {
        return bidIncrement;
    }

    public void setBidIncrement(Currency bidIncrement) {
        this.bidIncrement = bidIncrement;
    }

    public Currency getConvertedCurrentPrice() {
        return convertedCurrentPrice;
    }

    public void setConvertedCurrentPrice(Currency convertedCurrentPrice) {
        this.convertedCurrentPrice = convertedCurrentPrice;
    }

    public Currency getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Currency currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getLeadCount() {
        return leadCount;
    }

    public void setLeadCount(int leadCount) {
        this.leadCount = leadCount;
    }

    public Currency getMinimumToBid() {
        return minimumToBid;
    }

    public void setMinimumToBid(Currency minimumToBid) {
        this.minimumToBid = minimumToBid;
    }

    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }

    public boolean isReserveMet() {
        return reserveMet;
    }

    public void setReserveMet(boolean reserveMet) {
        this.reserveMet = reserveMet;
    }

    public boolean isSecondChanceEligible() {
        return secondChanceEligible;
    }

    public void setSecondChanceEligible(boolean secondChanceEligible) {
        this.secondChanceEligible = secondChanceEligible;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public int getQuantitySoldByPickupInStore() {
        return quantitySoldByPickupInStore;
    }

    public void setQuantitySoldByPickupInStore(int quantitySoldByPickupInStore) {
        this.quantitySoldByPickupInStore = quantitySoldByPickupInStore;
    }
}
