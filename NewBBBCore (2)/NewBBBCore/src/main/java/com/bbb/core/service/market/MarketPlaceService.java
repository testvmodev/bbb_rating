package com.bbb.core.service.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.market.MarketPlaceCreateRequest;

public interface MarketPlaceService {
    Object getAllMarketPlaces();

    Object getMarketPlaceDetail(long id) throws ExceptionResponse;

    Object postMarketPlace(MarketPlaceCreateRequest request) throws ExceptionResponse;

    Object updateMarketPlace(long id, MarketPlaceCreateRequest request) throws ExceptionResponse;

    Object deleteMarketPlace(long id) throws ExceptionResponse;
}
