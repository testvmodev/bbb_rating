package com.bbb.core.repository.reout;

import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.BicycleBrandResponse;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.bicycle.BrandResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BicycleBrandResponseRepository extends CrudRepository<BrandResponse, Long> {
    @Query(nativeQuery = true, value =
            "SELECT bicycle.id AS bicycle_id, bicycle.description as description, bicycle_brand.name as brand_name " +
                    "FROM bicycle join bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
                    "ORDER BY bicycle.id OFFSET 1 ROWS FETCH NEXT 20 ROWS ONLY")
    List<BrandResponse> findAll();

    //search bicycle brand
    @Query(nativeQuery = true,
            value = "SELECT bicycle_brand.id, bicycle_brand.name,bicycle_brand.brand_logo, bicycle_brand.value_modifier, bicycle_brand.last_update, bicycle_brand.is_approved " +
                    "FROM bicycle_brand " +
                    "WHERE " +
                    "(:nameSearch IS NULL OR bicycle_brand.name LIKE :nameSearch) AND " +
                    "bicycle_brand.is_delete = 0 " +
                    "ORDER BY " +
                    "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleBrandSortField).NAME.name()} " +
                    "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                    "THEN bicycle_brand.name END, " +
                    "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleBrandSortField).NAME.name()} " +
                    "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                    "THEN bicycle_brand.name END DESC " +
                    "OFFSET :offset ROWS FETCH NEXT :limit ROWS ONLY")
    List<BrandResponse> findAllBicycleBrand(
            @Param(value = "nameSearch") String nameSearch,
            @Param(value = "sortType") Sort sortType,
            @Param(value = "sortField") BicycleBrandSortField sortField,
            @Param(value = "offset") long offset,
            @Param(value = "limit") int limit);
}
