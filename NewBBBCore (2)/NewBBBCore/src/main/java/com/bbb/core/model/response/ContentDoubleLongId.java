package com.bbb.core.model.response;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ContentDoubleLongId implements Serializable {
    private long firstId;
    private long secondId;

    public long getFirstId() {
        return firstId;
    }

    public void setFirstId(long firstId) {
        this.firstId = firstId;
    }

    public long getSecondId() {
        return secondId;
    }

    public void setSecondId(long secondId) {
        this.secondId = secondId;
    }
}
