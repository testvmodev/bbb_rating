package com.bbb.core.controller.onlinestore;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.onlinestore.*;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.OnlineStoreMyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.onlinestore.OnlineStoreService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class OnlineStoreController {
    @Autowired
    private OnlineStoreService service;

    @PostMapping(Constants.URL_GET_ONLINE_STORE_TRENDING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getTrendingOnlineStores(
            @RequestBody(required = false) TrendingOnlineStoreRequest request,
            Pageable pageable
    ) throws ExceptionResponse {
        if (request == null) {
            request = new TrendingOnlineStoreRequest();
        }
        request.setPageable(pageable);
        return ResponseEntity.ok(service.getTrendingOnlineStores(request));
    }

    @PostMapping(Constants.URL_GET_ONLINE_STORE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getOnlineStores(
            @RequestBody @Valid OnlineStoreRequest request,
            Pageable pageable
    ) throws ExceptionResponse {
        request.setPageable(pageable);
        return ResponseEntity.ok(service.getOnlineStores(request));
    }

    @GetMapping(Constants.URL_GET_ONLINE_STORE_MY_LISTING)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getMyListings(
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "statuses", required = false) List<StatusMarketListing> statuses,
            @RequestParam(value = "fromDay", required = false) Long fromDay,
            @RequestParam(value = "toDay", required = false) Long toDay,
            @RequestParam(value = "sortField", defaultValue = "POSTING_TIME") OnlineStoreMyListingSortField sortField,
            @RequestParam(value = "sortType", defaultValue = "DESC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        OnlineStoreListingRequest request = new OnlineStoreListingRequest();
        request.setContent(content);
        request.setStatuses(statuses);
        LocalDateTime from = CommonUtils.unixTimestampToLocalDateTime(fromDay);
        request.setFromDay(from != null ? from.toLocalDate() : null);
        LocalDateTime to = CommonUtils.unixTimestampToLocalDateTime(toDay);
        request.setToDay(to != null ? to.toLocalDate() : null);
        request.setSortField(sortField);
        request.setSort(sortType);
        request.setPageable(pageable);
        return ResponseEntity.ok(service.getMyListings(request));
    }

    @GetMapping(Constants.URL_GET_ONLINE_STORE_MY_LISTING_SUMMARY)
    public ResponseEntity getMyListingSummary() throws ExceptionResponse {
        return ResponseEntity.ok(service.getMyListingSummary());
    }

    @GetMapping(Constants.URL_GET_ONLINE_STORE_SUMMARY)
    public ResponseEntity getStoreSummaryStats() throws ExceptionResponse {
        return ResponseEntity.ok(service.getStoreSummaryStats());
    }

    @GetMapping(Constants.URL_GET_ONLINE_STORE_SALE_REPORT)
    public ResponseEntity getStoreSaleReport(
            @RequestParam("month") YearMonth month
    ) throws ExceptionResponse {
        OnlineStoreSaleReportRequest request = new OnlineStoreSaleReportRequest();
        request.setMonth(month);
        return ResponseEntity.ok(service.getStoreSaleReport(request));
    }

    @GetMapping(Constants.URL_GET_ONLINE_STORE_SALE_REPORT_DETAIL)
    public ResponseEntity getStoreSaleReportDetail(
            @RequestParam(value = "fromDay", required = false) Long fromDay,
            @RequestParam(value = "toDay", required = false) Long toDay
    ) throws ExceptionResponse {
        OnlineStoreSaleReportDetailRequest request = new OnlineStoreSaleReportDetailRequest();
        request.setFromDay(CommonUtils.unixTimestampToLocalDateTime(fromDay).toLocalDate());
        request.setToDay(CommonUtils.unixTimestampToLocalDateTime(toDay).toLocalDate());

        return ResponseEntity.ok(service.getStoreSaleReportDetail(request));
    }
}
