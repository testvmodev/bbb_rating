package com.bbb.core.manager.admin;

import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.model.otherservice.response.admin.UserAdminInfoResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailResponse;
import com.bbb.core.model.response.ListObjResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AdminManager {
    @Autowired
    private AuthManager authManager;

    public ListObjResponse<UserDetailResponse> getActiveWholesaler() {
        Map<String, String> param = new HashMap<>();
        param.put("page", 1 + "");
        param.put("page_size", -1 + "");
        param.put("select", "email:1;role:1");
        param.put("sort", "date_created:-1");
        param.put("where", "role:wholesaler;status.active.is_active:1");
        param.put("pattern", "role:wholesale");

        return authManager.getUsers(param);
    }

    public List<UserAdminInfoResponse> getUserAdminInfo(List<String> userId){
        return authManager.getUserAdminInfo(userId);
    }
}
