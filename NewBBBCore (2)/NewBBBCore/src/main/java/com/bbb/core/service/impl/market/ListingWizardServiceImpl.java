package com.bbb.core.service.impl.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.market.ListingWizardManager;
import com.bbb.core.model.request.market.listingwizard.ListingWizardType;
import com.bbb.core.model.request.market.marketlisting.CreateMarketListingRequest;
import com.bbb.core.model.request.market.marketlisting.tosale.MarketListingInvToSalRequest;
import com.bbb.core.service.market.ListingWizardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class ListingWizardServiceImpl implements ListingWizardService {
    @Autowired
    private ListingWizardManager manager;
    @Override
    public Object getListingWizards(ListingWizardType type, List<MarketListingInvToSalRequest> request, Pageable page) throws ExceptionResponse {
        return manager.getListingWizards(type, request, page);
    }

    @Override
    public Object createMarketListing(CreateMarketListingRequest requests) throws ExceptionResponse {
        return manager.createMarketListing(requests);
    }

    @Override
    public Object deListMarketListing(List<Long> marketListingIds) throws ExceptionResponse {
        return manager.deListMarketListing(marketListingIds);
    }

    @Override
    public Object startAsyncMarketListing(List<Long> marketListingIds, HttpServletRequest servletRequest) throws ExceptionResponse {
        return manager.startAsyncMarketListing(marketListingIds, servletRequest);
    }

    @Override
    public Object removeQueueMarketListing(List<Long> marketListingIds) throws ExceptionResponse {
        return manager.removeQueueMarketListing(marketListingIds);
    }
}
