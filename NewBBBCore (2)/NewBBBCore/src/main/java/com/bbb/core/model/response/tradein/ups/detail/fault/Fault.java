package com.bbb.core.model.response.tradein.ups.detail.fault;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Fault {
    @JsonProperty(value = "faultcode")
    private String faultCode;
    @JsonProperty(value = "faultstring")
    private String faultString;
    @JsonProperty(value = "detail")
    private FaultDetail detail;

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }

    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    public FaultDetail getDetail() {
        return detail;
    }

    public void setDetail(FaultDetail detail) {
        this.detail = detail;
    }
}
