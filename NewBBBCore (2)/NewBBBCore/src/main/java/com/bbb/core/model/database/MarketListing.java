package com.bbb.core.model.database;


import com.bbb.core.common.Constants;
import com.bbb.core.model.database.type.ListingTypeEbay;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "market_listing")
public class MarketListing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long inventoryId;
    private StatusMarketListing status;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private LocalDateTime timeListed;
    private LocalDateTime timeDelisted;
    private LocalDateTime timeSold;
//    private TypeListingDuration listingDuration;
    private Long ebayListingDurationId;
    private long marketPlaceConfigId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Long itemId;
    private long marketPlaceId;
    private Long ebayCategoryId;
    private Long ebaySubCategoryId;
    private long trackingCount;
    private long countListingError;
    private String lastMsgError;
    private boolean isBestOffer;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float bestOfferAutoAcceptPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float minimumOfferAutoAcceptPrice;
    private ListingTypeEbay listingTypeEbay;
    private String paypalEmailSeller;
    private boolean isDelete;
    private Boolean isExpirable;
    private Boolean isRenew;
    private LocalDateTime startQueueTime;
    private String ownerId;
    private float rating;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public StatusMarketListing getStatus() {
        return status;
    }

    public void setStatus(StatusMarketListing status) {
        this.status = status;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LocalDateTime getTimeListed() {
        return timeListed;
    }

    public void setTimeListed(LocalDateTime timeListed) {
        this.timeListed = timeListed;
    }

    public LocalDateTime getTimeDelisted() {
        return timeDelisted;
    }

    public void setTimeDelisted(LocalDateTime timeDelisted) {
        this.timeDelisted = timeDelisted;
    }

    public LocalDateTime getTimeSold() {
        return timeSold;
    }

    public void setTimeSold(LocalDateTime timeSold) {
        this.timeSold = timeSold;
    }

//    public TypeListingDuration getListingDuration() {
//        return listingDuration;
//    }

//    public void setListingDuration(TypeListingDuration listingDuration) {
//        this.listingDuration = listingDuration;
//    }

    public Long getEbayListingDurationId() {
        return ebayListingDurationId;
    }

    public void setEbayListingDurationId(Long ebayListingDurationId) {
        this.ebayListingDurationId = ebayListingDurationId;
    }

    public long getMarketPlaceConfigId() {
        return marketPlaceConfigId;
    }

    public void setMarketPlaceConfigId(long marketPlaceConfigId) {
        this.marketPlaceConfigId = marketPlaceConfigId;
    }


    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public Long getEbayCategoryId() {
        return ebayCategoryId;
    }

    public void setEbayCategoryId(Long ebayCategoryId) {
        this.ebayCategoryId = ebayCategoryId;
    }

    public Long getEbaySubCategoryId() {
        return ebaySubCategoryId;
    }

    public void setEbaySubCategoryId(Long ebaySubCategoryId) {
        this.ebaySubCategoryId = ebaySubCategoryId;
    }

    public long getTrackingCount() {
        return trackingCount;
    }

    public void setTrackingCount(long trackingCount) {
        this.trackingCount = trackingCount;
    }

    public long getCountListingError() {
        return countListingError;
    }

    public void setCountListingError(long countListingError) {
        this.countListingError = countListingError;
    }

    public String getLastMsgError() {
        return lastMsgError;
    }

    public void setLastMsgError(String lastMsgError) {
        this.lastMsgError = lastMsgError;
    }

    public boolean isBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public ListingTypeEbay getListingTypeEbay() {
        return listingTypeEbay;
    }

    public void setListingTypeEbay(ListingTypeEbay listingTypeEbay) {
        this.listingTypeEbay = listingTypeEbay;
    }

    public String getPaypalEmailSeller() {
        return paypalEmailSeller;
    }

    public void setPaypalEmailSeller(String paypalEmailSeller) {
        this.paypalEmailSeller = paypalEmailSeller;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @JsonIgnore
    public Boolean getExpirable() {
        return isExpirable;
    }

    public void setExpirable(Boolean expirable) {
        isExpirable = expirable;
    }

    @JsonIgnore
    public Boolean getRenew() {
        return isRenew;
    }

    public void setRenew(Boolean renew) {
        isRenew = renew;
    }

    public LocalDateTime getStartQueueTime() {
        return startQueueTime;
    }

    public void setStartQueueTime(LocalDateTime startQueueTime) {
        this.startQueueTime = startQueueTime;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
