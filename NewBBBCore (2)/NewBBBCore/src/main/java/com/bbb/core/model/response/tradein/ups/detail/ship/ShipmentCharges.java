package com.bbb.core.model.response.tradein.ups.detail.ship;

import com.bbb.core.model.response.tradein.ups.detail.Money;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentCharges {
    @JsonProperty("TotalCharges")
    private Money totalCharges;

    public Money getTotalCharges() {
        return totalCharges;
    }
}
