package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.response.tradein.TradeInBicycleYearResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface TradeInBicycleYearResponseRepository extends JpaRepository<TradeInBicycleYearResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.id AS bicycle_id,  " +
            "bicycle.year_id AS year_id,  " +
            "bicycle_year.name AS year_name,  " +
            "bicycle.image_default AS image_default  " +
            "FROM bicycle JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "WHERE " +
            "bicycle.is_delete = 0 AND " +
            "bicycle.active = 1 AND " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.model_id = :modelId"
    )
    List<TradeInBicycleYearResponse> findAll(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId
    );
}
