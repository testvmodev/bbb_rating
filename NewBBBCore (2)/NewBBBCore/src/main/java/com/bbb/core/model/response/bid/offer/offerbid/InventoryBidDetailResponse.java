package com.bbb.core.model.response.bid.offer.offerbid;

import java.util.List;

public class InventoryBidDetailResponse {
    private InventoryBidResponse currentBid;
    private List<BidOfferResponse> bids;

    public InventoryBidResponse getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(InventoryBidResponse currentBid) {
        this.currentBid = currentBid;
    }

    public List<BidOfferResponse> getBids() {
        return bids;
    }

    public void setBids(List<BidOfferResponse> bids) {
        this.bids = bids;
    }
}
