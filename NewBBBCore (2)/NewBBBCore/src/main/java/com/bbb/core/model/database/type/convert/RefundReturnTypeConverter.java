package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.RefundReturnTypeInventory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RefundReturnTypeConverter implements AttributeConverter<RefundReturnTypeInventory, String> {
    @Override
    public String convertToDatabaseColumn(RefundReturnTypeInventory refundReturnType) {
        if ( refundReturnType == null ) {
            return null;
        }
        return refundReturnType.getValue();
    }

    @Override
    public RefundReturnTypeInventory convertToEntityAttribute(String s) {
        return RefundReturnTypeInventory.findByValue(s);
    }
}
