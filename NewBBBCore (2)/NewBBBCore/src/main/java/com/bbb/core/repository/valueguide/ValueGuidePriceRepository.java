package com.bbb.core.repository.valueguide;

import com.bbb.core.model.database.ValueGuidePrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ValueGuidePriceRepository extends JpaRepository<ValueGuidePrice, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM value_guide_price where bicycle_id = :bicycleId")
    ValueGuidePrice findOne(
            @Param(value = "bicycleId") long bicycleId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM value_guide_price where " +
            "value_guide_price.brand_id = :brandId AND " +
            "value_guide_price.model_id = :modelId AND "+
            "value_guide_price.year = :year"
    )
    ValueGuidePrice findOne(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "year") String year
    );
}
