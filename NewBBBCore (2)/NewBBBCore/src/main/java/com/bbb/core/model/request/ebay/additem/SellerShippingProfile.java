package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellerShippingProfile {
    @JacksonXmlProperty(localName = "ShippingProfileID")
    private long shippingProfileID;

    public long getShippingProfileID() {
        return shippingProfileID;
    }

    public void setShippingProfileID(long shippingProfileID) {
        this.shippingProfileID = shippingProfileID;
    }
}
