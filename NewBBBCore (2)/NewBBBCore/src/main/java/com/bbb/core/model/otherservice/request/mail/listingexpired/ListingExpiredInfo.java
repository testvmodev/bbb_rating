package com.bbb.core.model.otherservice.request.mail.listingexpired;

public class ListingExpiredInfo {
    private long id;
    private String mainImageURL;

    public ListingExpiredInfo() {}

    public ListingExpiredInfo(long id, String mainImageURL) {
        this.id = id;
        this.mainImageURL = mainImageURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMainImageURL() {
        return mainImageURL;
    }

    public void setMainImageURL(String mainImageURL) {
        this.mainImageURL = mainImageURL;
    }
}
