package com.bbb.core.manager.billing;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.manager.templaterest.RestBillingManager;
import com.bbb.core.model.otherservice.request.cart.CartAddRequest;
import com.bbb.core.model.otherservice.request.cart.CartRemoveRequest;
import com.bbb.core.model.otherservice.request.cart.CartStagesRequest;
import com.bbb.core.model.otherservice.request.event.EventInventoryUpdateRequest;
import com.bbb.core.model.otherservice.request.event.EventMarketListingDeleteRequest;
import com.bbb.core.model.otherservice.request.event.EventMarketListingExpireRequest;
import com.bbb.core.model.otherservice.request.event.EventMarketListingUpdateRequest;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.otherservice.response.cart.CartResponse;
import com.bbb.core.model.otherservice.response.cart.CartStageResponses;
import com.bbb.core.model.otherservice.response.order.OrderDetailResponse;
import com.bbb.core.model.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BillingManager {
    @Autowired
    private RestBillingManager restBillingManager;
    @Autowired
    private IntegratedServices integratedServices;

    public CartResponse addCart(String buyerId, String cartType, long offerId) {
        CartAddRequest request = new CartAddRequest();
        request.setBuyerId(buyerId);
        request.setCartType(cartType);
        request.setOfferId(offerId);
        request.setLocalPickup(false); //let buyer decide, default false
        return restBillingManager.postObject(integratedServices.URL_CART_ADD_SINGLE, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), request, CartResponse.class);
    }

    public CartResponse removeCart(String userId, long inventoryId) {
        CartRemoveRequest request = new CartRemoveRequest();
        request.setBuyerId(userId);
        request.setInventoryId(inventoryId);
        return restBillingManager.patchObject(integratedServices.URL_CART_ITEM_SINGLE, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), request, CartResponse.class);
    }

    //specific status of an user's cart if have
    public CartSingleStageResponse checkStatusCart(
            long inventoryId, String buyerId
    ) {
        Map<String, String> mapO = new HashMap<>();
        mapO.put("id", inventoryId + "");
        mapO.put("buyer_id", buyerId);
        CartSingleStageResponse res = restBillingManager.getObject(integratedServices.URL_CART_STATUS, Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(), mapO, CartSingleStageResponse.class);
        if (res != null && res.getStage() != null) {
            res.setStage(res.getStage().toUpperCase());
        }
        return res;
    }

    //status of a cart not specific to only one user
    public CartSingleStageResponse checkStatusCart(long inventoryId) {
        Map<String, String> params = new HashMap<>();
        params.put("id", String.valueOf(inventoryId));
        CartSingleStageResponse res = restBillingManager.getObject(
                integratedServices.URL_CART_STATUS,
                params,
                CartSingleStageResponse.class
        );
        if (res != null && res.getStage() != null) {
            res.setStage(res.getStage().toUpperCase());
        }
        return res;
    }

    public List<CartStageResponses> checkStatusCarts(List<Long> inventoryIds, String userId) {
        CartStagesRequest request = new CartStagesRequest(inventoryIds, userId);
        List<CartStageResponses> responses = restBillingManager.postObject(
                integratedServices.URL_CARTS_STATUSES,
                Constants.HEADER_SECRET_KEY, ConfigDataSource.getSecretKey(),
                request,
                new ParameterizedTypeReference<List<CartStageResponses>>() {}
        );
        return responses;
    }

    public OrderDetailResponse getOrderDetail(String orderId) {
        Map<String, String> params = new HashMap();
        params.put(IntegratedServices.ID, orderId);
        return restBillingManager.getObject(
                integratedServices.URL_ORDER_DETAIL,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                params, OrderDetailResponse.class);
    }

    @Async
    public MessageResponse marketListingUpdateAsync(long marketListingId) {
        EventMarketListingUpdateRequest request = new EventMarketListingUpdateRequest(marketListingId);

        return restBillingManager.postObject(
                integratedServices.URL_EVENT_LISTING_UPDATE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    @Async
    public MessageResponse marketListingDeleteAsync(long inventoryId) { //id of inven, not listing
        EventMarketListingDeleteRequest request = new EventMarketListingDeleteRequest(inventoryId);

        return restBillingManager.postObject(
                integratedServices.URL_EVENT_LISTING_DELETE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    public MessageResponse marketListingExpire(long inventoryId) {
        EventMarketListingExpireRequest request = new EventMarketListingExpireRequest(inventoryId);

        return restBillingManager.postObject(
                integratedServices.URL_EVENT_LISTING_EXPIRE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }

    @Async
    public MessageResponse inventoryUpdateAsync(long inventoryId) {
        EventInventoryUpdateRequest request = new EventInventoryUpdateRequest(inventoryId);

        return restBillingManager.postObject(
                integratedServices.URL_EVENT_INVENTORY_UPDATE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                request,
                MessageResponse.class
        );
    }
}
