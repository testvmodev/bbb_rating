package com.bbb.core.model.response.tradein;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TradeInBicycleYearResponse {
    @Id
    private long bicycleId;
    private long yearId;
    private String yearName;
    private String imageDefault;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }
}
