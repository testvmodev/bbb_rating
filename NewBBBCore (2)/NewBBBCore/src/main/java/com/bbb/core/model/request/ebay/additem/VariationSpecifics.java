package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class VariationSpecifics {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "NameValueList")
    private List<NameValueList> nameValueLists;

    public List<NameValueList> getNameValueLists() {
        return nameValueLists;
    }

    public void setNameValueLists(List<NameValueList> nameValueLists) {
        this.nameValueLists = nameValueLists;
    }
}
