package com.bbb.core.model.database;

import com.bbb.core.common.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "trade_in_price_config")
public class TradeInPriceConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @Column(columnDefinition = Constants.NUMERIC_3)
    private Float price;
    @JsonIgnore
    @Column(columnDefinition = Constants.NUMERIC_3)
    private Float percent;
    @JsonIgnore
    @Column(columnDefinition = Constants.NUMERIC_3)
    private Float additional;
    @JsonIgnore
    @Column(columnDefinition = Constants.NUMERIC_3)
    private Float maxValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    public Float getAdditional() {
        return additional;
    }

    public void setAdditional(Float additional) {
        this.additional = additional;
    }

    public Float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Float maxValue) {
        this.maxValue = maxValue;
    }

    public TradeInPriceConfig clone(){
        TradeInPriceConfig clone = new TradeInPriceConfig();
        clone.setId(id);
        clone.setName(name);
        clone.setPrice(price);
        clone.setPercent(percent);
        clone.setAdditional(additional);
        clone.setMaxValue(maxValue);
        return clone;
    }
}
