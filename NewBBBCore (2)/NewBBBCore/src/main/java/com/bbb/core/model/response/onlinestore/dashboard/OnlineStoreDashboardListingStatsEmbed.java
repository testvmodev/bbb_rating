package com.bbb.core.model.response.onlinestore.dashboard;

import javax.persistence.Embeddable;

@Embeddable
public class OnlineStoreDashboardListingStatsEmbed {
    private int allListing;
    private int forSale;
    private int expired;
    private int draft;
    private int salePending;
    private OnlineStoreDashboardListingSoldEmbed sold;

    public int getAllListing() {
        return allListing;
    }

    public void setAllListing(int allListing) {
        this.allListing = allListing;
    }

    public int getForSale() {
        return forSale;
    }

    public void setForSale(int forSale) {
        this.forSale = forSale;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public int getDraft() {
        return draft;
    }

    public void setDraft(int draft) {
        this.draft = draft;
    }

    public int getSalePending() {
        return salePending;
    }

    public void setSalePending(int salePending) {
        this.salePending = salePending;
    }

    public OnlineStoreDashboardListingSoldEmbed getSold() {
        return sold;
    }

    public void setSold(OnlineStoreDashboardListingSoldEmbed sold) {
        this.sold = sold;
    }
}
