package com.bbb.core.service.bid;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bid.auction.AuctionCreateRequest;
import com.bbb.core.model.request.bid.auction.AuctionRequest;
import com.bbb.core.model.request.bid.auction.AuctionUpdateRequest;
import org.springframework.data.domain.Pageable;

public interface AuctionService {
    Object getAuction(AuctionRequest request, Pageable pageable) throws ExceptionResponse;

    Object createAuction(AuctionCreateRequest request) throws ExceptionResponse;

    Object updateAuction(long auctionId, AuctionUpdateRequest request) throws ExceptionResponse;
}
