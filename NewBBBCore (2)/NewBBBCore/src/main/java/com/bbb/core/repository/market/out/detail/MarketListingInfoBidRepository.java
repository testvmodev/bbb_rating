package com.bbb.core.repository.market.out.detail;

import com.bbb.core.model.response.market.marketlisting.detail.MarketListingInfoBid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketListingInfoBidRepository extends JpaRepository<MarketListingInfoBid, Long> {
//    @Query(nativeQuery = true, value = "SELECT TOP 1 " +
//            "inventory_auction.id AS inventory_auction_id, " +
//            "inventory_auction.auction_id AS auction_id, " +
//            "max_offer.max_offer_price AS max_bid_price, " +
//            "inventory_auction.min AS min_bid_price " +
//            "FROM inventory_auction " +
//
//            "LEFT JOIN " +
//                "(SELECT offer.inventory_auction_id, MAX(offer.offer_price) AS max_offer_price FROM offer " +
//                "WHERE " +
////                "offer.inventory_id = :inventoryId AND " +
////                "offer.auction_id IS NOT NULL AND " +
//                "offer.is_delete = 0 AND " +
//                "offer.status = 'Pending' " +
//                "GROUP BY " +
//                "offer.inventory_auction_id " +
//                ") AS max_offer " +
//            "ON inventory_auction.id = max_offer. " +
//            "ON inventory_auction.inventory_id = max_offer.inventory_id AND inventory_auction.auction_id = max_offer.auction_id " +
//
//            "WHERE inventory_auction.inventory_id = :inventoryId AND " +
//            "inventory_auction.is_inactive = 0 AND " +
//            "inventory_auction.is_delete = 0 "
//    )
//    MarketListingInfoBid findOne(
//            @Param(value = "inventoryId") long inventoryId
//    );
}
