package com.bbb.core.service.salesforce;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import com.bbb.core.model.request.saleforce.ListingFullUpdateRequest;

public interface SalesforceService {
    Object createListingSalesforce(ListingFullCreateRequest request) throws ExceptionResponse;

    Object updateListingSalesforce(String salesforceId, ListingFullUpdateRequest request) throws ExceptionResponse;

    Object deListListingSalesforce(String salesforceId) throws ExceptionResponse;

    Object soldListingSalesforce(String salesforceId) throws ExceptionResponse;
}
