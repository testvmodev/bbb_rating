package com.bbb.core.model.request.ebay;

import com.bbb.core.common.fedex.FedExValue;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ProcessShipmentReply", namespace = "http://schemas.xmlsoap.org/soap/envelope")
public class Notification {

}
