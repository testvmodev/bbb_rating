package com.bbb.core.manager.schedule.jobs.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.ExpirationTimeConfig;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.MarketListingExpire;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.database.type.TypeExpirationTime;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.schedule.ScheduleSendMailListingResponse;
import com.bbb.core.repository.expirationtime.ExpirationTimeConfigRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.MarketListingExpireRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.CRON, defaultCron = "0 0 0,12 * * *")
public class MarketListingExpiredJob extends ScheduleJob {
    @Autowired
    private ExpirationTimeConfigRepository expirationTimeConfigRepository;
    @Autowired
    private MarketListingExpireRepository marketListingExpireRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private BillingManager billingManager;

    @Override
    public void run() {
        ExpirationTimeConfig firstListExpireTime = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.PERSONAL_LISTING_FIRST_LIST);
        ExpirationTimeConfig reListExpireTime = expirationTimeConfigRepository.findFirstByType(TypeExpirationTime.PERSONAL_LISTING_RENEW);

        if (firstListExpireTime == null || reListExpireTime == null) {
            setCustomLogString("Missing expiration time config for personal listing. Setup it before enable this job");
            return;
        }

        List<MarketListingExpire> expireSoon = marketListingExpireRepository.findExpired(
                firstListExpireTime.getExpireAfterHours(),
                false
        );
        expireSoon.addAll(marketListingExpireRepository.findExpired(
                reListExpireTime.getExpireAfterHours(),
                true
        ));

        if (expireSoon.size() > 0) {
            List<MarketListingExpire> successExpires = new ArrayList();
            List<MarketListingExpire> failExpires = new ArrayList();

            List<Long> marketListingIds = expireSoon.stream()
                    .map(MarketListingExpire::getId)
                    .collect(Collectors.toList());
            List<MarketListing> marketListings = marketListingRepository.findAllByIds(marketListingIds);
            if (marketListings.size() < 1) {
                throw new RuntimeException(MessageResponses.MARKET_LISTING_NOT_EXIST);
            }
            marketListings.forEach(listing -> listing.setStatus(StatusMarketListing.EXPIRED));

            List<Long> inventoryIds = marketListings.stream()
                    .map(MarketListing::getInventoryId)
                    .collect(Collectors.toList());
            List<Inventory> inventories = inventoryRepository.findAllByIds(inventoryIds);
            inventories.forEach(inven -> inven.setStage(StageInventory.READY_LISTED));
            if (marketListings.size() != inventories.size()) {
                throw new RuntimeException(MessageResponses.INVENTORY_AUCTION_NOT_EXIST);
            }

            Map<Long, MarketListingExpire> tempExpire = expireSoon.stream()
                    .collect(Collectors.toMap(MarketListingExpire::getId, e -> e));
            Map<Long, MarketListing> tempListing = marketListings.stream()
                    .collect(Collectors.toMap(MarketListing::getId, m -> m));
            Map<Long, Inventory> tempInventory = inventories.stream()
                    .collect(Collectors.toMap(Inventory::getId, i -> i));

            List<MarketListing> sendMailListings = new ArrayList();
            List<Inventory> sendMailInventories = new ArrayList();
            expireSoon.forEach(expire -> {
                if (expire.getExpiredFailCount() == null || expire.getExpiredFailCount() < Constants.MAX_TRY_SEND_MAIL) {
                    MarketListing marketListing = tempListing.get(expire.getId());
                    sendMailListings.add(marketListing);
                    sendMailInventories.add(tempInventory.get(marketListing.getInventoryId()));
                } else {
                    //skip if exceed error limit
                    successExpires.add(expire);
                }
            });

            List<Observable<ScheduleSendMailListingResponse>> obSendMail =
                    createObserverSendMail(sendMailListings, sendMailInventories);
            List<ScheduleSendMailListingResponse> responses;
            List<String> errorResponses = new ArrayList();

            responses = Observable.zip(obSendMail, result -> {
                List<ScheduleSendMailListingResponse> scheduleResponses = new ArrayList<>();
                for (Object o : result) {
                    if (o != null) {
                        ScheduleSendMailListingResponse response = (ScheduleSendMailListingResponse) o;
                        scheduleResponses.add(response);
                    }
                }
                return scheduleResponses;
            }).blockingFirst();

            for (ScheduleSendMailListingResponse response : responses) {
                if (response.getRawErrorResponse() == null) {
                    MarketListingExpire expire = tempExpire.get(response.getMarketListingId());
                    expire.setLastMailExpired(LocalDateTime.now());
                    successExpires.add(expire);
                } else {
                    errorResponses.add(response.getRawErrorResponse());
                    failExpires.add(tempExpire.get(response.getMarketListingId()));
                }
            }

            //higher priority for saving to db before sync events to other services
            List<MarketListing> successListings = new ArrayList();
            if (successExpires.size() > 0) {
                List<Inventory> successInventories = new ArrayList<>();

                successExpires.forEach(expire -> {
                    MarketListing marketListing = tempListing.get(expire.getId());
                    successListings.add(marketListing);
                    successInventories.add(tempInventory.get(marketListing.getInventoryId()));
                });

                List<Long> successListingIds = successListings.stream()
                        .map(MarketListing::getId)
                        .collect(Collectors.toList());
                offerManager.rejectOtherOffer(null, successListingIds, null);

                marketListingExpireRepository.saveAll(successExpires);
                marketListingRepository.saveAll(successListings);
                inventoryRepository.saveAll(successInventories);
            }

            if (failExpires.size() > 0) {
                failExpires.forEach(expire -> {
                    Integer count = expire.getExpiredFailCount();
                    count = count == null ? 1 : count + 1;
                    expire.setExpiredFailCount(count);
                });
                marketListingExpireRepository.saveAll(failExpires);
                //log as error
                this.setCustomLogString(String.join("\n", errorResponses));
                this.setFail(true);
            }

            if (successListings.size() > 0) {
                for (MarketListing expired : successListings) {
                    billingManager.marketListingExpire(expired.getInventoryId());
                }
            }
        }
    }

    public void onFail(Throwable throwable) {}

    private List<Observable<ScheduleSendMailListingResponse>> createObserverSendMail(
            List<MarketListing> marketListings, List<Inventory> inventories
    ) {
        List<Observable<ScheduleSendMailListingResponse>> obSendMail = new ArrayList();
        ExecutorService executor = Executors.newFixedThreadPool(4);

        List<Pair<MarketListing, Inventory>> contents = new ArrayList();
        marketListings.forEach(listing -> {
            Pair<MarketListing, Inventory> pair = new Pair();
            pair.setFirst(listing);
            for (Inventory inven : inventories) {
                if (inven.getId() == listing.getInventoryId()) {
                    pair.setSecond(inven);
                    break;
                }
            }
            contents.add(pair);
        });

        contents.forEach(pair -> obSendMail.add(Observable.create((ObservableOnSubscribe<ScheduleSendMailListingResponse>) su -> {
            ScheduleSendMailListingResponse scheduleResponse = new ScheduleSendMailListingResponse();
            try {
                SendingMailResponse sendingMailResponse = notificationManager.sendMailListingExpired(
                        pair.getFirst(),
                        pair.getSecond()
                );
                scheduleResponse.setSendingMailResponse(sendingMailResponse);
            } catch (HttpClientErrorException e) {
                scheduleResponse.setRawErrorResponse(e.getResponseBodyAsString());
                e.printStackTrace();
            } finally {
                scheduleResponse.setMarketListingId(pair.getFirst().getId());
            }
            su.onNext(scheduleResponse);
            su.onComplete();
        }).subscribeOn(Schedulers.from(executor))));

        return obSendMail;
    }
}
