package com.bbb.core.model.response;

import com.bbb.core.model.response.embedded.ContentCommonId;

import javax.persistence.*;

@Entity
public class ContentCommon {
    @EmbeddedId
    private ContentCommonId id;

    public ContentCommonId getId() {
        return id;
    }

    public void setId(ContentCommonId id) {
        this.id = id;
    }
}
