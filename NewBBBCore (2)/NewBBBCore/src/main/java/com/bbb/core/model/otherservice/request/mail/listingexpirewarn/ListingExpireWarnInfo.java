package com.bbb.core.model.otherservice.request.mail.listingexpirewarn;

public class ListingExpireWarnInfo {
    private long id;
    private BikeListingExpireWarn bike;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BikeListingExpireWarn getBike() {
        return bike;
    }

    public void setBike(BikeListingExpireWarn bike) {
        this.bike = bike;
    }
}
