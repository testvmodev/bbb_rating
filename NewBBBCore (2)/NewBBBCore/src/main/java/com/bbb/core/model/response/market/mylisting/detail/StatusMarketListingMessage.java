package com.bbb.core.model.response.market.mylisting.detail;

import com.bbb.core.model.database.type.StatusMarketListing;


import java.util.HashMap;
import java.util.Map;

public class StatusMarketListingMessage {
    private static Map<StatusMarketListing, String> statusListingMessage = new HashMap();
    static {
        Map<StatusMarketListing, String> messages = new HashMap();
        messages.put(StatusMarketListing.LISTED, "For sale");
        messages.put(StatusMarketListing.SOLD, "Sold");
        messages.put(StatusMarketListing.DRAFT, "Draft");
        messages.put(StatusMarketListing.SALE_PENDING, "Sale pending");

        statusListingMessage = messages;
    }

    public static String getMessage(StatusMarketListing status) {
        if (status == null) {
            return null;
        }
        if (statusListingMessage.containsKey(status)) {
            return statusListingMessage.get(status);
        }
        return status.getValue();
    }
}
