package com.bbb.core.model.response.tradein.fedex;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.response.tradein.fedex.detail.ship.CompletedShipmentDetail;
import com.bbb.core.model.response.tradein.fedex.detail.Notification;
import com.bbb.core.model.response.tradein.fedex.detail.type.Severity;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "ProcessShipmentReply", namespace = FedExValue.SHIP_ROOT_NAMESPACE)
public class FedexProcessShipResponse {
    @JacksonXmlProperty(localName = "HighestSeverity")
    private String highestSeverityRaw;

    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "Notifications")
    private List<Notification> notifications;
    @JacksonXmlProperty(localName = "CompletedShipmentDetail")
    private CompletedShipmentDetail completedShipmentDetail;

    public Severity getHighestSeverity() {
        return Severity.findByValue(highestSeverityRaw);
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public CompletedShipmentDetail getCompletedShipmentDetail() {
        return completedShipmentDetail;
    }

    public void setCompletedShipmentDetail(CompletedShipmentDetail completedShipmentDetail) {
        this.completedShipmentDetail = completedShipmentDetail;
    }

    public boolean isSuccess() {
        try {
            return getHighestSeverity() == Severity.SUCCESS;
        } catch (Exception e) {
            return false;
        }
    }
}
