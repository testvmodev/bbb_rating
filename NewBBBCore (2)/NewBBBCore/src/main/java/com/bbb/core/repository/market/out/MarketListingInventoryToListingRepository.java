package com.bbb.core.repository.market.out;

import com.bbb.core.model.response.market.marketlisting.MarketListingInventoryToListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingInventoryToListingRepository extends JpaRepository<MarketListingInventoryToListing, Long> {

    @Query(nativeQuery = true, value = "SELECT " +
            "COUNT(inventory.id) " +
            "FROM inventory LEFT JOIN " +
            "(" +
            "SELECT " +
            "market_listing.inventory_id AS inventory_id, " +
            "COUNT(market_listing.inventory_id) AS count_inventory " +
            "FROM " +
            "market_listing " +
            "WHERE " +
            "market_listing.market_place_config_id IN :configIds AND " +
            "market_listing.is_delete = 0 AND market_listing.status = 'Listed' " +
            "GROUP BY market_listing.inventory_id" +
            ") AS count_inv " +
            "ON inventory.id = count_inv.inventory_id " +
            "WHERE " +
            "inventory.status = 'Active' AND " +
            "(inventory.stage = 'Listed' OR inventory.stage = 'Ready to be Listed') AND " +
            "(count_inv.inventory_id IS NULL OR count_inv.count_inventory < :totalMarketPlace) "
    )
    int countTotal(
            @Param(value = "totalMarketPlace") int totalMarketPlace,
            @Param(value = "configIds") List<Long> configIds
    );


}
