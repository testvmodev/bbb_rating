package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.MassUnit;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MassUnitConvert implements AttributeConverter<MassUnit, String> {
    @Override
    public String convertToDatabaseColumn(MassUnit massUnit) {
        if (massUnit == null) {
            return null;
        }
        return massUnit.getValue();
    }

    @Override
    public MassUnit convertToEntityAttribute(String s) {
        return MassUnit.findByValue(s);
    }
}
