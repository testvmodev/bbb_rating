package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.response.tradein.fedex.detail.Notification;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class TrackDetails {
    @JacksonXmlProperty(localName = "Notification")
    private Notification notification;
    @JacksonXmlProperty(localName = "StatusDetail")
    private StatusDetail statusDetail;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Events")
    private List<Event> events;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public StatusDetail getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(StatusDetail statusDetail) {
        this.statusDetail = statusDetail;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public boolean isDelivered() {
        try {
            return statusDetail.getCode().equals(FedExValue.TRACK_DELIVERED_STATUS_CODE);
        } catch (Exception e) {
            return false;
        }
    }
}
