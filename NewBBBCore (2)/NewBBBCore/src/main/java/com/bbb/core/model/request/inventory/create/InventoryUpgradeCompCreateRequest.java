package com.bbb.core.model.request.inventory.create;

import com.bbb.core.model.database.type.TypeTradeInUpgradeComp;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class InventoryUpgradeCompCreateRequest {
    @NotNull
    @NotBlank
    private String name;
    @NotNull
    private Float percentValue;
    @NotNull
    private Boolean isUp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPercentValue() {
        return percentValue;
    }

    public void setPercentValue(Float percentValue) {
        this.percentValue = percentValue;
    }

    public Boolean getUp() {
        return isUp;
    }

    public void setUp(Boolean up) {
        isUp = up;
    }

}
