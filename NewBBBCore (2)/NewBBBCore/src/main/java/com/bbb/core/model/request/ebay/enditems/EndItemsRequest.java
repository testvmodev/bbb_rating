package com.bbb.core.model.request.ebay.enditems;

import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "EndItemsRequest", namespace = "urn:ebay:apis:eBLBaseComponents")
public class EndItemsRequest extends BaseEbayRequest {
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "EndItemRequestContainer")
    private List<EndItemRequestContainer> endItemRequestContainers;

    @JacksonXmlElementWrapper(useWrapping=false)
    public List<EndItemRequestContainer> getEndItemRequestContainers() {
        return endItemRequestContainers;
    }

    public void setEndItemRequestContainers(List<EndItemRequestContainer> endItemRequestContainers) {
        this.endItemRequestContainers = endItemRequestContainers;
    }
}
