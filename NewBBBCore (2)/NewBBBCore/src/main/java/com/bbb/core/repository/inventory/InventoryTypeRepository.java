package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryTypeRepository extends CrudRepository<InventoryType, Long> {
    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_type " +
            "WHERE id = ?1")
    InventoryType findOne(long id);

    @Query(nativeQuery = true, value = "SELECT  * FROM inventory_type")
    List<InventoryType> findAll();

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM inventory_type WHERE inventory_type.name = :inventoryTypeName")
    InventoryType findOneByName(
            @Param(value = "inventoryTypeName") String inventoryTypeName
    );
}
