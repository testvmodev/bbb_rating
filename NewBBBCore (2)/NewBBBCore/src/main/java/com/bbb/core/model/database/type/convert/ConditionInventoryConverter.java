package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.ConditionInventory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ConditionInventoryConverter implements AttributeConverter<ConditionInventory, String> {
    @Override
    public String convertToDatabaseColumn(ConditionInventory condition) {
        if (condition == null) {
            return null;
        }
        return condition.getValue();
    }

    @Override
    public ConditionInventory convertToEntityAttribute(String value) {
        return ConditionInventory.findByValue(value);
    }


}
