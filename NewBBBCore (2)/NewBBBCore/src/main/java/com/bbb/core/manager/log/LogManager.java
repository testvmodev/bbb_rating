package com.bbb.core.manager.log;

import com.bbb.core.common.Constants;
import com.bbb.core.manager.templaterest.RestLogServiceManager;
import com.bbb.core.model.request.log.LogServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class LogManager {
    private static final Logger LOG = LoggerFactory.getLogger(LogManager.class);
    @Autowired
    private RestLogServiceManager restLogServiceManager;


    @Async
    public void postLog(LogServiceRequest request) {
        String res = restLogServiceManager.postObject(Constants.URL_LOG, request, String.class, MediaType.APPLICATION_JSON);
        LOG.info(res);
    }
}
