package com.bbb.core.service.onlinestore;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.onlinestore.*;

public interface OnlineStoreService {
    Object getTrendingOnlineStores(TrendingOnlineStoreRequest request) throws ExceptionResponse;

    Object getOnlineStores(OnlineStoreRequest request) throws ExceptionResponse;

    Object getMyListings(OnlineStoreListingRequest request) throws ExceptionResponse;

    Object getMyListingSummary() throws ExceptionResponse;

    Object getStoreSummaryStats() throws ExceptionResponse;

    Object getStoreSaleReport(OnlineStoreSaleReportRequest request) throws ExceptionResponse;

    Object getStoreSaleReportDetail(OnlineStoreSaleReportDetailRequest request) throws ExceptionResponse;
}
