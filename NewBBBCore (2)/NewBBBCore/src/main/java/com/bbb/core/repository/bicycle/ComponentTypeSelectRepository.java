package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.ComponentTypeSelect;
import com.bbb.core.model.request.sort.ComponentValueSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ComponentTypeSelectRepository extends JpaRepository<ComponentTypeSelect, Long> {

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM component_type_select " +

            "WHERE component_type_id = :compTypeId AND is_delete = 0 " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentValueSortField).VALUE.name()} " +
            "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type_select.value END ASC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.ComponentValueSortField).VALUE.name()} " +
            "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
            "THEN component_type_select.value END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY"
    )
    List<ComponentTypeSelect> findValuesSorted(
            @Param("compTypeId") long compTypeId,
            @Param("sortField") ComponentValueSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT * FROM component_type_select WHERE id = :id AND is_delete = 0 ")
    ComponentTypeSelect getOne (@Param("id") long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM component_type_select WHERE is_delete = 0 AND component_type_id =:compTypeId")
    int getCountByCompTypeId(@Param("compTypeId") long compTypeId);
}
