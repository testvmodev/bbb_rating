package com.bbb.core.model.request.shipping;

public class ShippingFeeConfigUpdateRequest {
    private Long ebayShippingProfileId;
    private Float shippingFee;

    public Long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(Long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public Float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(Float shippingFee) {
        this.shippingFee = shippingFee;
    }
}
