package com.bbb.core.model.request.bid.inventoryauction;

public class InventoryAuctionCreateRequest {
    private long inventoryId;
    private long auctionId;
    private float min;
    private Float buyItNow;
    private boolean hasBuyItNow;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public Float getBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(Float buyItNow) {
        this.buyItNow = buyItNow;
    }

    public boolean isHasBuyItNow() {
        return hasBuyItNow;
    }

    public void setHasBuyItNow(boolean hasBuyItNow) {
        this.hasBuyItNow = hasBuyItNow;
    }
}
