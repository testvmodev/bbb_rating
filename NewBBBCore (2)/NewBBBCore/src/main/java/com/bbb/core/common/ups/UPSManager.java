package com.bbb.core.common.ups;

import com.bbb.core.model.database.TradeInShipping;
import com.bbb.core.model.database.TradeInShippingAccount;
import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.otherservice.response.warehouse.ShippingAddressResponse;
import com.bbb.core.model.otherservice.response.warehouse.WarehouseResponse;
import com.bbb.core.model.request.tradein.tradin.TradeInCalculateShipRequest;
import com.bbb.core.model.request.tradein.tradin.ShippingBicycleBlueBookRequest;
import com.bbb.core.model.request.tradein.ups.UPSRateRequest;
import com.bbb.core.model.request.tradein.ups.UPSShipRequest;
import com.bbb.core.model.request.tradein.ups.UPSTrackRequest;
import com.bbb.core.model.request.tradein.ups.UPSVoidShipRequest;
import com.bbb.core.model.request.tradein.ups.detail.*;
import com.bbb.core.model.request.tradein.ups.detail.account.UPSSecurity;
import com.bbb.core.model.request.tradein.ups.detail.rate.RatePackageDetail;
import com.bbb.core.model.request.tradein.ups.detail.rate.RateRequest;
import com.bbb.core.model.request.tradein.ups.detail.rate.UPSShipment;
import com.bbb.core.model.request.tradein.ups.detail.ship.*;
import com.bbb.core.model.request.tradein.ups.detail.track.TrackRequest;
import com.bbb.core.model.request.tradein.ups.detail.voidship.VoidRequestConfig;
import com.bbb.core.model.request.tradein.ups.detail.voidship.VoidShipmentRequest;
import com.bbb.core.model.response.tradein.ups.UPSRateResponse;
import com.bbb.core.model.response.tradein.ups.UPSShipResponse;
import com.bbb.core.manager.templaterest.UPSRest;
import com.bbb.core.model.database.type.LengthUnit;
import com.bbb.core.model.database.type.MassUnit;
import com.bbb.core.model.response.tradein.ups.UPSTrackResponse;
import com.bbb.core.model.response.tradein.ups.UPSVoidShipResponse;
import com.bbb.core.repository.tradein.ShippingAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UPSManager implements UPSValue {
    @Autowired
    private UPSRest upsClient;
    @Autowired
    private UPSLabelSpecification labelSpec;
    @Autowired
    private ShippingAccountRepository shippingAccountRepository;

    public UPSShipResponse createShip(
            ShippingBicycleBlueBookRequest shipCreateReq,
            TradeInShippingAccount shippingAccount,
            UserDetailFullResponse userInfo,
            WarehouseResponse warehouse
    ) {
        UPSShipRequest upsRequest = new UPSShipRequest();

        Dimension dimension = new Dimension(
                new UnitOfMeasurement(LengthUnit.IN.getValue().toUpperCase()),
                String.valueOf(shipCreateReq.getLength()),
                String.valueOf(shipCreateReq.getWeight()),
                String.valueOf(shipCreateReq.getHeight())
        );

        PackageWeight packageWeight = new PackageWeight(
                new UnitOfMeasurement(MassUnit.LBS.getValue().toUpperCase()),
                String.valueOf(shipCreateReq.getWeight())
        );

        ShipPackageDetail shipPackageDetail = new ShipPackageDetail();
        shipPackageDetail.setPackageType(new PackageType(PACKAGE_TYPE_DEFAULT));
        shipPackageDetail.setDimensions(dimension);
        shipPackageDetail.setPackageWeight(packageWeight);

        ShipmentCharge shipmentCharge = new ShipmentCharge();
        shipmentCharge.setType(PAYMENT_CHARGE_TYPE_DEFAULT);
        shipmentCharge.setBillShipper(new BillShipper(shippingAccount.getAccountNumber()));

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setShipmentCharge(shipmentCharge);

        Address bbbAddress = buildBBBAddress(warehouse.getShippingAddress());
        Address fromAddress = new Address(
                shipCreateReq.getFromAddress(),
                shipCreateReq.getFromCity(),
                shipCreateReq.getFromState(),
                String.valueOf(shipCreateReq.getFromZipCode()),
                UPSValue.BBB_COUNTRY_CODE_DEFAULT
        );

        ShipAddress shipTo = new ShipAddress(BBB_NAME_DEFAULT, bbbAddress);
        shipTo.setPhone(new Phone(shippingAccount.getPhone()));

        ShipAddress shipFrom = new ShipAddress(userInfo.getPartner().getName(), fromAddress);
        shipFrom.setPhone(new Phone(userInfo.getPartner().getPhone()));

        ShipmentDetail shipmentDetail = new ShipmentDetail();
        shipmentDetail.setShipper(new Shipper(BBB_NAME_DEFAULT, bbbAddress, shippingAccount.getAccountNumber()));
        shipmentDetail.setShipTo(shipTo);
        shipmentDetail.setShipFrom(shipFrom);
        shipmentDetail.setShipPackageDetail(shipPackageDetail);
        shipmentDetail.setPaymentInformation(paymentInformation);
        shipmentDetail.setShipService(new ShipService(shippingAccount.getServiceCode()));

        ShipmentRequest shipmentRequest = new ShipmentRequest(shipmentDetail, labelSpec);
        shipmentRequest.setRequestConfig(new RequestConfig(SHIP_REQUEST_CONFIG_DEFAULT));

        upsRequest.setShipmentRequest(shipmentRequest);

        return createShip(upsRequest, shippingAccount);
    }

    public UPSShipResponse createShip(UPSShipRequest upsRequest, TradeInShippingAccount shippingAccount) {
        upsRequest.setUpsSecurity(buildUPSSecurity(shippingAccount));
        return upsClient.createPublishRateShip(upsRequest);
    }

    public UPSRateResponse rateShip(
            TradeInCalculateShipRequest request,
            TradeInShippingAccount shippingAccount,
            WarehouseResponse warehouse
    ) {
        UPSRateRequest upsRequest = new UPSRateRequest();

        Dimension dimension = new Dimension(
                new UnitOfMeasurement(LengthUnit.IN.getValue().toUpperCase()),
                String.valueOf(request.getLength()),
                String.valueOf(request.getWeight()),
                String.valueOf(request.getHeight())
        );

        PackageWeight packageWeight = new PackageWeight(
                new UnitOfMeasurement(MassUnit.LBS.getValue().toUpperCase()),
                String.valueOf(request.getWeight())
        );

        RatePackageDetail packageDetail = new RatePackageDetail();
        packageDetail.setPackageType(new PackageType(PACKAGE_TYPE_DEFAULT));
        packageDetail.setDimensions(dimension);
        packageDetail.setPackageWeight(packageWeight);

        Address bbbAddress = buildBBBAddress(warehouse.getShippingAddress());
        Address fromAddress = new Address(
                request.getFromAddress(),
                request.getFromCity(),
                request.getFromState(),
                String.valueOf(request.getFromZipCode()),
                UPSValue.BBB_COUNTRY_CODE_DEFAULT
        );

        UPSShipment shipment = new UPSShipment();
        shipment.setShipper(new Shipper(BBB_NAME_DEFAULT, bbbAddress, shippingAccount.getAccountNumber()));
        shipment.setShipTo(new ShipAddress(bbbAddress));
        shipment.setShipFrom(new ShipAddress(fromAddress));
        shipment.setRatePackageDetail(packageDetail);
        shipment.setService(new ShipService(shippingAccount.getServiceCode()));

        RateRequest rateRequest = new RateRequest();
        rateRequest.setRequestConfig(new RequestConfig(RATE_REQUEST_CONFIG_DEFAULT));
        rateRequest.setUPSShipment(shipment);

        upsRequest.setRateRequest(rateRequest);

        return rateShip(upsRequest, shippingAccount);
    }

    public UPSRateResponse rateShip(UPSRateRequest upsRequest, TradeInShippingAccount shippingAccount) {
        upsRequest.setUpsSecurity(buildUPSSecurity(shippingAccount));
        return upsClient.rate(upsRequest);
    }

    public UPSTrackResponse getTrack(String trackingNumber) {
        UPSTrackRequest request = new UPSTrackRequest();
        TradeInShippingAccount shippingAccount = shippingAccountRepository.findOnePreferActive(CarrierType.UPS.getValue());

        TrackRequest trackRequest = new TrackRequest();
        trackRequest.setInquiryNumber(trackingNumber);
        trackRequest.setRequestConfig(new RequestConfig(UPSValue.TRACK_REQUEST_CONFIG_DEFAULT));

        request.setUpsSecurity(buildUPSSecurity(shippingAccount));
        request.setTrackRequest(trackRequest);

        return upsClient.track(request);
    }

    /**
     * Not yet tested. plz be aware
     * @param tradeInShipping shipping detail of trade-in
     * @return
     */
    public UPSVoidShipResponse cancelShip(TradeInShipping tradeInShipping) {
        VoidRequestConfig voidRequestConfig = new VoidRequestConfig("TradeIn-" + String.valueOf(tradeInShipping.getId()));
        VoidShipmentRequest voidShipmentRequest = new VoidShipmentRequest(tradeInShipping.getTrackingNumber());
        voidShipmentRequest.setRequest(voidRequestConfig);

        UPSVoidShipRequest upsRequest = new UPSVoidShipRequest();
        upsRequest.setVoidShipmentRequest(voidShipmentRequest);

        return cancelShip(upsRequest);
    }

    /**
     * Not yet tested. plz be aware
     * @param request
     * @return
     */
    public UPSVoidShipResponse cancelShip(UPSVoidShipRequest request) {
        TradeInShippingAccount shippingAccount = shippingAccountRepository.findOnePreferActive(CarrierType.UPS.getValue());
        request.setUpsSecurity(buildUPSSecurity(shippingAccount));

        return upsClient.voidShip(request);
    }

    private UPSSecurity buildUPSSecurity(TradeInShippingAccount shippingAccount) {
        return new UPSSecurity(
                shippingAccount.getUsername(),
                shippingAccount.getPassword(),
                shippingAccount.getApiKey()
        );
    }

//    private Address buildBBBAddress(TradeInShippingAccount shippingAccount) {
//        Address shipTo = new Address(
//                shippingAccount.getAddressLine(),
//                shippingAccount.getCity(),
//                shippingAccount.getStateCode(),
//                String.valueOf(shippingAccount.getZipCode()),
//                BBB_COUNTRY_CODE_DEFAULT
//        );
//        return shipTo;
//    }
    private Address buildBBBAddress(ShippingAddressResponse shippingAddress) {
        Address shipTo = new Address(
                shippingAddress.getAddress(),
                shippingAddress.getCity(),
                shippingAddress.getState(),
                shippingAddress.getZipCode(),
                shippingAddress.getCountryCode()
        );
        return shipTo;
    }
}
