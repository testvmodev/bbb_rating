package com.bbb.core.service.impl.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.market.MyListingManager;
import com.bbb.core.model.database.type.UserRoleType;
import com.bbb.core.model.request.market.personallisting.PersonalListingRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingShipmentCreateRequest;
import com.bbb.core.model.request.market.personallisting.PersonalListingUpdateRequest;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.market.MyListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class MyListingServiceImpl implements MyListingService {
    @Autowired
    private MyListingManager manager;

//    @Override
//    public Object createMyListingPTP(PersonalListingRequest request) throws ExceptionResponse, MethodArgumentNotValidException {
//        return manager.createMyListingPTP(request);
//    }

    @Override
    public Object getAllMyListings(MyListingSortField sortField, Sort sortType, Pageable pageable) throws ExceptionResponse {
        return manager.getAllMyListings(sortField, sortType, pageable);
    }

    @Override
    public Object getMyListingDetail(long marketListingId) throws ExceptionResponse {
        return manager.getMyListingDetail(marketListingId);
    }

    @Override
    public Object updateMyListing(
            long marketListingId, PersonalListingUpdateRequest request
    ) throws ExceptionResponse {
        return manager.updateMyListing(marketListingId, request);
    }

    @Override
    public Object deleteMyListing(long marketListingId) throws ExceptionResponse {
        return manager.deleteMyListing(marketListingId);
    }

    @Override
    public Object postImageMarketListingPTP(
            long marketListingId, List<MultipartFile> images
    ) throws ExceptionResponse {
        return manager.postImageMarketListingPTP(marketListingId, images);
    }


    @Override
    public Object addImageImageMarketListingPTP(
            long marketListingId, List<MultipartFile> images
    ) throws ExceptionResponse {
        return manager.addImageImageMarketListingPTP(marketListingId, images);
    }

    @Override
    public Object deleteImageMarketListingPTP(
            long marketListingId, List<Long> inventoryImageIds
    ) throws ExceptionResponse {
        return manager.deleteImageMarketListingPTP(marketListingId, inventoryImageIds);
    }

    @Override
    public Object getDraftDetail(long myListingDraftId) throws ExceptionResponse {
        return manager.getDraftDetail(myListingDraftId);
    }

    @Override
    public Object createDraftMyListing(
            PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return manager.createDraftMyListing(request);
    }

    @Override
    public Object updateDraftMyListing(
            long draftId, PersonalListingRequest request
    ) throws ExceptionResponse, MethodArgumentNotValidException {
        return manager.updateDraftMyListing(draftId, request);
    }

    @Override
    public Object deleteDraftMyListing(long draftId) throws ExceptionResponse {
        return manager.deleteDraftMyListing(draftId);
    }

    @Override
    public Object postImageDraftMyListing(
            long listingDraftId, List<MultipartFile> images
    ) throws ExceptionResponse {
        return manager.postImageDraftMyListing(listingDraftId, images);
    }

    @Override
    public Object deleteImageDraftMyListing(List<Long> imageDraftIds) throws ExceptionResponse {
        return manager.deleteImageDraftMyListing(imageDraftIds);
    }

    @Override
    public Object createListingFromDraft(
            long draftId
    ) throws ExceptionResponse, MethodArgumentNotValidException, NoSuchMethodException {
        return manager.createListingFromDraft(draftId);
    }

    @Override
    public Object relistMyListing(long marketListingId) throws ExceptionResponse {
        return manager.relistMyListing(marketListingId);
    }

    @Override
    public Object createShipment(PersonalListingShipmentCreateRequest request) throws ExceptionResponse {
        return manager.createShipment(request);
    }
}
