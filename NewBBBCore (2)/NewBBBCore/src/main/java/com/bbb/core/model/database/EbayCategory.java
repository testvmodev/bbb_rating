package com.bbb.core.model.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ebay_category")
public class EbayCategory {
    @Id
    private long id;
    private String name;
    private long parentId;
    private boolean isPrimary;
    private Long sortPrimary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public Long getSortPrimary() {
        return sortPrimary;
    }

    public void setSortPrimary(Long sortPrimary) {
        this.sortPrimary = sortPrimary;
    }
}
