package com.bbb.core.service.brandyear;

import com.bbb.core.common.exception.ExceptionResponse;

public interface BrandYearService {
    Object getModelFromBrandYear(long brandId, Long yearId,  String familyName) throws ExceptionResponse;
    Object getAllBicycleModelFromBicycle(long brandId) throws ExceptionResponse;
}
