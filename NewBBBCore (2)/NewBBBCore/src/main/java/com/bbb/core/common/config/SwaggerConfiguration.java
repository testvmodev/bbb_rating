package com.bbb.core.common.config;

import com.bbb.core.common.Constants;
import com.bbb.core.common.utils.CommonUtils;
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.security.IgnoredRequestCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.*;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private ConverterRegistry converterRegistry;
    @Autowired
    private Environment environment;
    @Autowired
    private AppConfig appConfig;


    @PostConstruct
    public void init() {
        converterRegistry.addConverter(new StringToLocalDateTimeConverter());
        converterRegistry.addConverter(new StringToLocalDateConverter());
    }

    @Bean
    public Docket api(ServletContext servletContext) {
        String contextPath = appConfig.getEndpoint() != null ? appConfig.getEndpoint().getBaseCoreApiUrl() : "";
        String basePath = appConfig.getEndpoint() != null ? appConfig.getEndpoint().getCoreApiPrefix() : null;
        if (contextPath.contains("localhost")) {
//            try {
//                contextPath = Inet4Address.getLocalHost().getHostAddress() + ":" + environment.getProperty("server.port");
//            contextPath = "192.168.2.125" + ":" + environment.getProperty("server.port");
            contextPath = null;
//            } catch (UnknownHostException e) {
//                e.printStackTrace();
//            }
        }
        List<Parameter> parameterBuilders = new ArrayList<>();
        parameterBuilders.add(
                new ParameterBuilder()
                        .name(Constants.HEADER_NAME_AUTH)
                        .description("Description of header")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .build());
        parameterBuilders.add(
                new ParameterBuilder()
                        .name(Constants.HEADER_SECRET_KEY)
                        .description("Description of header")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .build());
        if (contextPath == null) {
            return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.bbb.core.controller")).paths(PathSelectors.any())
                    .build()
                    .pathMapping("/")
                    .globalOperationParameters(parameterBuilders)
                    .ignoredParameterTypes(Pageable.class);
        } else {
            return new Docket(DocumentationType.SWAGGER_2)
                    .host(contextPath)
                    .pathProvider(new RelativePathProvider(servletContext) {
                        @Override
                        public String getApplicationBasePath() {
                            return (basePath != null ? basePath : super.getApplicationBasePath());
                        }
                    })
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.bbb.core.controller")).paths(PathSelectors.any())
                    .build()
                    .pathMapping("/")
                    .globalOperationParameters(parameterBuilders)
                    .ignoredParameterTypes(Pageable.class);
        }

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");



//        registry.addResourceHandler("/").addResourceLocations("classpath:/public/");
//
//        registry.addResourceHandler("/resources/" + "*").addResourceLocations("classpath:/excel/");

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:index.html");
        registry.addRedirectViewController("/configuration/ui", "/swagger-resources/configuration/ui");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
        resolver.setFallbackPageable(new PageRequest(0, Constants.PAGE_SIZE_DEFAULT));
        resolver.setOneIndexedParameters(true);
        argumentResolvers.add(resolver);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**");
        registry.addMapping("/api/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH");
    }


    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(CommonUtils.converterJack());
        super.configureMessageConverters(converters);
    }


    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        CommonUtils.updateTimeConvertObjectMapper(objectMapper);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Bean
    @Primary
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

//    @Bean
//    @Primary
//    public IgnoredRequestCustomizer optionsIgnoredRequestsCustomizer() {
//        return configurer -> {
//            List<RequestMatcher> matchers = new ArrayList<>();
//            matchers.add(new AntPathRequestMatcher("/**", "OPTIONS"));
//            configurer.requestMatchers(new OrRequestMatcher(matchers));
//        };
//    }

    @Bean
    @Primary
    public XmlMapper xmlMapper() {
        XmlFactory factory = new XmlFactory(new WstxInputFactory(), new WstxOutputFactory());
        XmlMapper xmlMapper =  new XmlMapper(factory);
        CommonUtils.updateTimeConvertXmlMapperEbay(xmlMapper);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return xmlMapper;
    }


}