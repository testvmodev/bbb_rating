package com.bbb.core.model.request.favourite;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.type.FavouriteType;
import com.bbb.core.model.response.ObjectError;
import org.springframework.http.HttpStatus;

public class FavouriteRemoveRequest implements MessageResponses {
    private Long marketListingId;
    private Long inventoryAuctionId;
    private FavouriteType favouriteType;

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public FavouriteType getFavouriteType() {
        return favouriteType;
    }

    public void setFavouriteType(FavouriteType favouriteType) {
        this.favouriteType = favouriteType;
    }

    public void validate() throws ExceptionResponse {
        String message = null;
        switch (favouriteType) {
            case MARKET_LIST:
                if (marketListingId == null) {
                    message = MARKET_LISTING_ID_MUST_NOT_NULL;
                }
                break;
            case AUCTION:
                if (inventoryAuctionId == null) {
                    message = INVENTORY_AUCTION_ID_MUST_NOT_NULL;
                }
                break;
            default:
                message = UNKNOWN_FAVOURITE_TYPE;
        }
        if (message != null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, message),
                    HttpStatus.BAD_REQUEST
            );
        }
    }
}
