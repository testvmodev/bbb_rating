package com.bbb.core.model.request.market.marketlisting;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateMarketListingRequest {
    private Float adjustPrice;
    @NotNull
    private List<CreateMarketListing> items;

    public Float getAdjustPrice() {
        return adjustPrice;
    }

    public void setAdjustPrice(Float adjustPrice) {
        this.adjustPrice = adjustPrice;
    }

    public List<CreateMarketListing> getItems() {
        return items;
    }

    public void setItems(List<CreateMarketListing> items) {
        this.items = items;
    }
}
