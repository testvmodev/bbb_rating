package com.bbb.core.controller.bicycle;

import com.bbb.core.service.bicycle.BicycleComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BicycleComponentController {
    @Autowired
    private BicycleComponentService service;

    @GetMapping(value = "/importBicycleComponent")
    public ResponseEntity importBicycleComponent(
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    ) {
        return new ResponseEntity<>(service.importBicycleComponent(offset, size), HttpStatus.OK);
    }
}
