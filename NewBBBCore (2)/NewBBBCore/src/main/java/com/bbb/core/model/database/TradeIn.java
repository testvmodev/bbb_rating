package com.bbb.core.model.database;

import com.bbb.core.model.database.type.ConditionInventory;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
@Table(name = "trade_in")
public class TradeIn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Long bicycleId;
    private String partnerId;
    private Float value;
    private String serialNumber;
    private Long brakeTypeId;
    private String color;
    private String ownerName;
    private String ownerAddress;
    private String ownerCity;
    private String ownerState;
    private String ownerZipCode;
    private String ownerEmail;
    private String ownerPhone;
    private String ownerLicensePassport;
    private String proofName;
    private LocalDateTime proofDate;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private Long statusId;
    private ConditionInventory condition;
    private boolean isArchived;
    private boolean isAdminArchived;
    private Long customQuoteId;
    private String title;
    private String partnerAddress;
    private String partnerName;
    private String userCreatedId;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Boolean isOversized;
    private Boolean isClean;
    private Long bicycleTypeId;
    private boolean isCancel;
    private boolean isSave;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(Long bicycleId) {
        this.bicycleId = bicycleId;
    }


    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }


    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }


    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }


    public String getOwnerCity() {
        return ownerCity;
    }

    public void setOwnerCity(String ownerCity) {
        this.ownerCity = ownerCity;
    }


    public String getOwnerState() {
        return ownerState;
    }

    public void setOwnerState(String ownerState) {
        this.ownerState = ownerState;
    }


    public String getOwnerZipCode() {
        return ownerZipCode;
    }

    public void setOwnerZipCode(String ownerZipCode) {
        this.ownerZipCode = ownerZipCode;
    }


    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }


    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }


    public String getOwnerLicensePassport() {
        return ownerLicensePassport;
    }

    public void setOwnerLicensePassport(String ownerLicensePassport) {
        this.ownerLicensePassport = ownerLicensePassport;
    }


    public String getProofName() {
        return proofName;
    }

    public void setProofName(String proofName) {
        this.proofName = proofName;
    }


    public LocalDateTime getProofDate() {
        return proofDate;
    }

    public void setProofDate(LocalDateTime proofDate) {
        this.proofDate = proofDate;
    }


    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }


    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }


    public Long getCustomQuoteId() {
        return customQuoteId;
    }

    public void setCustomQuoteId(Long customQuoteId) {
        this.customQuoteId = customQuoteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    public String getPartnerAddress() {
        return partnerAddress;
    }

    public void setPartnerAddress(String partnerAddress) {
        this.partnerAddress = partnerAddress;
    }

    public String getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(String userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public boolean isSave() {
        return isSave;
    }

    public void setSave(boolean save) {
        isSave = save;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public Boolean getOversized() {
        return isOversized;
    }

    public void setOversized(Boolean oversized) {
        isOversized = oversized;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public boolean isAdminArchived() {
        return isAdminArchived;
    }

    public void setAdminArchived(boolean adminArchived) {
        isAdminArchived = adminArchived;
    }
}
