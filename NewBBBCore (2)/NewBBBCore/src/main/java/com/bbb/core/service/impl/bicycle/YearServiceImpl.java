package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.YearManager;
import com.bbb.core.model.request.year.YearCreateRequest;
import com.bbb.core.model.request.year.YearUpdateRequest;
import com.bbb.core.service.bicycle.YearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YearServiceImpl implements YearService {
    @Autowired
    private YearManager manager;

    @Override
    public Object getYears(String yearName) {
        return manager.getYears(yearName);
    }

    @Override
    public Object createYear(YearCreateRequest request) throws ExceptionResponse {
        return manager.createYear(request);
    }

    @Override
    public Object updateYear(long yearId, YearUpdateRequest request) throws ExceptionResponse {
        return manager.updateYear(yearId, request);
    }

    @Override
    public Object deleteType(long yearId) throws ExceptionResponse {
        return manager.deleteType(yearId);
    }

    @Override
    public Object getYearFromBrandModel(Long brandId, Long modelId) throws ExceptionResponse {
        return manager.getYearFromBrandModel(brandId, modelId);
    }
}
