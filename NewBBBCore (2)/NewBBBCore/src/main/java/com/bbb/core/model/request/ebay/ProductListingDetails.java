package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ProductListingDetails {
    @JacksonXmlProperty(localName = "BrandMPN")
    private BrandMPN brandMPN;

    @JacksonXmlProperty(localName = "IncludeeBayProductDetails")
    private Boolean includeeBayProductDetails;

    public BrandMPN getBrandMPN() {
        return brandMPN;
    }

    public void setBrandMPN(BrandMPN brandMPN) {
        this.brandMPN = brandMPN;
    }

    public Boolean getIncludeeBayProductDetails() {
        return includeeBayProductDetails;
    }

    public void setIncludeeBayProductDetails(Boolean includeeBayProductDetails) {
        this.includeeBayProductDetails = includeeBayProductDetails;
    }
}
