package com.bbb.core.manager.templaterest;

import com.bbb.core.common.Constants;
import org.springframework.stereotype.Component;

@Component
public class RestSalesforceManager extends RestTemplateManager {
    public RestSalesforceManager() {
        super(Constants.URL_INVENTORY_SALESFORCE);
    }
}
