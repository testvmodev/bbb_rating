package com.bbb.core.model.request.ebay.fetchtoken;

import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "FetchTokenRequest", namespace = "urn:ebay:apis:eBLBaseComponents")
public class FetchTokenRequest extends BaseEbayRequest {
    @JacksonXmlProperty(localName = "SessionID")
    private String sessionID;

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
}
