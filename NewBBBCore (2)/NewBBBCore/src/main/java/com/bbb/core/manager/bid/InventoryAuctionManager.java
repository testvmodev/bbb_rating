package com.bbb.core.manager.bid;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.bid.async.InventoryAuctionManagerAsync;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.schedule.jobs.auction.AuctionExpireJob;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionCreateRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionRequest;
import com.bbb.core.model.request.bid.inventoryauction.InventoryAuctionUpdateRequest;
import com.bbb.core.model.response.ContentValueCommon;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionDetailResponse;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionResponse;
import com.bbb.core.model.response.bid.inventoryauction.StatusInventoryAuctionResponse;
import com.bbb.core.model.response.inventory.InventoryDetailResponse;
import com.bbb.core.model.response.market.marketlisting.detail.InventoryCompTypeResponse;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.bid.out.BidOfferResponseRepository;
import com.bbb.core.repository.bid.out.InventoryAuctionDetailResponseRepository;
import com.bbb.core.repository.bid.out.InventoryAuctionResponseRepository;
import com.bbb.core.repository.favourite.FavouriteRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeSelectRepository;
import com.bbb.core.repository.inventory.InventoryImageRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.inventory.out.InitialInventoryRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.detail.InventoryCompTypeResRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.reout.ContentValueCommonRepository;
import io.reactivex.Observable;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class InventoryAuctionManager implements MessageResponses {
    //region Beans
    @Autowired
    private InventoryAuctionResponseRepository inventoryAuctionResponseRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private ContentCommonRepository commonRepository;
    @Autowired
    private ContentValueCommonRepository contentValueCommonRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private InitialInventoryRepository initialInventoryRepository;
    @Autowired
    private FavouriteRepository favouriteRepository;
    @Autowired
    private InventoryAuctionDetailResponseRepository inventoryAuctionDetailResponseRepository;
    @Autowired
    private InventoryAuctionManagerAsync inventoryAuctionManagerAsync;
    @Autowired
    private InventoryCompTypeResRepository inventoryCompTypeResRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private AuctionExpireJob auctionExpireJob;
    @Autowired
    private BidOfferResponseRepository bidOfferResponseRepository;
    @Autowired
    private InventoryCompTypeSelectRepository inventoryCompTypeSelectRepository;
    //endregion

    public Object getInventoryAuctions(InventoryAuctionRequest request, Pageable page) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles()) &&
                !CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getStartPrice() != null && request.getStartPrice() < 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, START_PRICE_MUST_POSITIVE_ZERO),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getEndPrice() != null && request.getEndPrice() < 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, END_PRICE_MUST_POSITIVE_ZERO),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getStartPrice() != null && request.getEndPrice() != null) {
            if (request.getStartPrice() > request.getEndPrice()) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, START_PRICE_MUST_LESS_OR_EQUAL_THAN_END_PRICE),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        ListObjResponse<InventoryAuctionResponse> response = new ListObjResponse<>();
        List<Long> temList = new ArrayList<>();
        temList.add(0L);
        List<String> temListString = new ArrayList<>();
        temListString.add("Demo");
        List<String> frameSizes = inventoryCompTypeSelectRepository.findAllFromSizeCategory(request.getSizeNames()).stream()
                .map(comp -> comp.getValue())
                .collect(Collectors.toList());
        boolean isNullBicycle = request.getBicycleIds() == null || request.getBicycleIds().size() == 0;
        boolean isNullModel = request.getModelIds() == null || request.getModelIds().size() == 0;
        boolean isNullBrand = request.getBrandIds() == null || request.getBrandIds().size() == 0;
        boolean isNullType = request.getTypeBicycleNames() == null || request.getTypeBicycleNames().size() == 0;
        boolean isNullFrame = request.getFrameMaterialNames() == null || request.getFrameMaterialNames().size() == 0;
        boolean isNullBrake = request.getBrakeTypeNames() == null || request.getBrakeTypeNames().size() == 0;
        boolean isNullSize = frameSizes == null || frameSizes.size() == 0;
        boolean isGenderNull = request.getGenders() == null || request.getGenders().size() == 0;
        boolean isSuspensionNull = request.getSuspensions() == null || request.getSuspensions().size() == 0;
        boolean isWheelSizeNull = request.getWheelSizes() == null || request.getWheelSizes().size() == 0;
        boolean isNullCondition = request.getConditions() == null || request.getConditions().size() == 0;
        boolean isLocationRadiusNull = request.getLatitude() == null || request.getLongitude() == null || request.getRadius() == null;

        response.setData(
                inventoryAuctionResponseRepository.findAllSorted(
                        request.getAuctionId(),
                        request.getInventoryId(),
                        StringUtils.isBlank(request.getAuctionName()) ? null : "%" + request.getAuctionName() + "%",
                        isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                        isNullModel, isNullModel ? temList : request.getModelIds(),
                        isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                        isNullType, isNullType ? temListString : request.getTypeBicycleNames(),
                        isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                        isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                        isNullSize, isNullSize ? temListString : frameSizes,
                        isGenderNull, isGenderNull ? temListString : request.getGenders(),
                        isSuspensionNull, isSuspensionNull ? temListString : request.getSuspensions(),
                        isWheelSizeNull, isWheelSizeNull ? temListString : request.getWheelSizes(),

                        isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),
                        request.getName() == null ? null : "%" + request.getName() + "%",

                        request.getZipCode(),
                        request.getCityName(),
                        request.getState(),
                        request.getCountry(),
                        isLocationRadiusNull,
                        isLocationRadiusNull ? 0 :request.getLatitude(),
                        isLocationRadiusNull ? 0 : request.getLongitude(),
                        isLocationRadiusNull ? 0 : request.getRadius(),

                        request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                        request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),

                        request.getStartYearId(),
                        request.getEndYearId(),

                        request.getContent(),

                        new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),

                        request.getSortField().name(),
                        request.getSortType().name(),
                        page.getOffset(),
                        page.getPageSize())
        );
        if (response.getData().size() > 0) {
            for (InventoryAuctionResponse datum : response.getData()) {
                if (datum.getDuration() < 0) {
                    datum.setDuration(0);
                }
            }
            //check favourite
            if (CommonUtils.isLogined()) {
                List<Integer> favouriteAuction = favouriteRepository.filterInventoryAuctionFavourites(
                        CommonUtils.getUserLogin(),
                        response.getData().stream().map(res -> res.getId()).collect(Collectors.toList())
                );
                for (InventoryAuctionResponse res : response.getData()) {
                    if (favouriteAuction.contains(res.getId())) {
                        res.setFavourite(true);
                    } else {
                        res.setFavourite(false);
                    }
                }
            }
        }


        response.setTotalItem(
                inventoryAuctionResponseRepository.getNumberTotal(
                        request.getAuctionId(),
                        request.getInventoryId(),
                        StringUtils.isBlank(request.getAuctionName()) ? null : "%" + request.getAuctionName() + "%",

                        isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                        isNullModel, isNullModel ? temList : request.getModelIds(),
                        isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                        isNullType, isNullType ? temListString : request.getTypeBicycleNames(),
                        isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                        isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                        isNullSize, isNullSize ? temListString : frameSizes,
                        isGenderNull, isGenderNull ? temListString : request.getGenders(),
                        isSuspensionNull, isSuspensionNull ? temListString : request.getSuspensions(),
                        isWheelSizeNull, isWheelSizeNull ? temListString : request.getWheelSizes(),

                        isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),
                        request.getName() == null ? null : "%" + request.getName() + "%",

                        request.getZipCode(),
                        request.getCityName(),
                        request.getState(),
                        request.getCountry(),
                        isLocationRadiusNull,
                        isLocationRadiusNull ? 0 :request.getLatitude(),
                        isLocationRadiusNull ? 0 : request.getLongitude(),
                        isLocationRadiusNull ? 0 : request.getRadius(),

                        request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                        request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),

                        request.getStartYearId(),
                        request.getEndYearId(),

                        request.getContent(),

                        new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate())
                )
        );
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Object createInventoryAuction(InventoryAuctionCreateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.FORBIDDEN
            );
        }

        if (request.getMin() < 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MIN_MUST_POSITIVE),
                    HttpStatus.NOT_FOUND
            );
        }

        if (request.isHasBuyItNow()) {
            String errorMessage = null;
            if (request.getBuyItNow() == null) {
                errorMessage = BUY_IT_NOW_MUST_NOT_EMPTY;
            } else if (request.getBuyItNow() <= 0) {
                errorMessage = BUY_IT_NOW_MUST_POSITIVE;
            } else if (request.getBuyItNow() <= request.getMin()) {
                errorMessage = BUY_IT_NOW_MUST_HIGHER_THAN_MIN;
            }

            if (errorMessage != null) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, errorMessage),
                        HttpStatus.NOT_FOUND
                );
            }
        }

        Inventory inventory = inventoryRepository.findOne(request.getInventoryId());
        if (inventory == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        if ((inventory.getStage() != StageInventory.READY_LISTED && inventory.getStage() != StageInventory.LISTED) ||
                inventory.getStatus() != StatusInventory.ACTIVE) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_INVENTORY_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        if (inventory.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_DELETED),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (!(inventory.getStatus() == StatusInventory.ACTIVE || inventory.getStage() == StageInventory.LISTED)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY + " " + inventory.getStatus() + " " + inventory.getStage()),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (!inventoryRepository.checkExistInventoryCanListAuction(inventory.getId())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_TYPE_INVALID),
                    HttpStatus.BAD_REQUEST
            );
        }


        int countInventory = inventoryAuctionRepository.getNumberTotal(request.getInventoryId());
        if (countInventory > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_HAS_AUCTION),
                    HttpStatus.BAD_REQUEST
            );
        }
        Auction auction = auctionRepository.findOne(request.getAuctionId());
        if (auction == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, AUCTION_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }
        LocalDateTime now = LocalDateTime.now();
        if (auction.getEndDate().toDate().getTime() < now.toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, AUCTION_CLOSED),
                    HttpStatus.BAD_REQUEST
            );
        }


        //check exist market listing
        if (marketListingRepository.checkExistInventoryListing(inventory.getId())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_INTO_MARKET_LISTING),
                    HttpStatus.BAD_REQUEST
            );
        }


        InventoryAuction inventoryAuction = new InventoryAuction();
        inventoryAuction.setAuctionId(request.getAuctionId());
        inventoryAuction.setInventoryId(request.getInventoryId());
        inventoryAuction.setMin(request.getMin());
        inventoryAuction.setHasBuyItNow(request.isHasBuyItNow());
        inventoryAuction.setLastStageInventory(inventory.getStage());
        if (request.getBuyItNow() != null) {
            DecimalFormat dec = new DecimalFormat("##.##");
            try {
                inventoryAuction.setBuyItNow(dec.parse(dec.format(request.getBuyItNow())).floatValue());
            } catch (ParseException e) {
                e.printStackTrace();
                inventoryAuction.setBuyItNow(request.getBuyItNow());
            }
        }
        inventoryAuction = inventoryAuctionRepository.save(inventoryAuction);

        InventoryDetailResponse response = new InventoryDetailResponse();
        response.setId(inventory.getId());
//        response.setConditioin(inventory.getCondition());
        response.setBicycleId(inventory.getBicycleId());
        response.setImageDefault(inventory.getImageDefault());
//        response.setInventoryName(inventory.getName());
//        response.setInventoryTitle(inventory.getTitle());
//        response.setInitPrice(inventory.getInitialListPrice());
//        response.setCurrentPrice(inventory.getCurrentListedPrice());
        response.setMsrpPrice(inventory.getMsrpPrice());
//        response.setSerialNumber(inventory.getSerialNumber());
        response.setStatus(inventory.getStatus());
        response.setStage(inventory.getStage());
//        response.setCountTrackingView(inventory.getCountTrackingView());
        response.setSellerId(inventory.getSellerId());

        Bicycle bicycle = bicycleRepository.findOneNotDelete(inventory.getBicycleId());
        if (bicycle != null) {
            boolean isSuccess = Observable.zip(
                    CommonUtils.getOb(() -> initialInventoryRepository.initInventory(
                            bicycle.getModelId(),
                            bicycle.getBrandId(),
                            bicycle.getYearId(),
                            bicycle.getTypeId(),
                            bicycle.getSizeId()
                    )),
                    CommonUtils.getOb(() -> commonRepository.findAllImageInventory(inventory.getId()).stream().map(
                            con -> con.getId().getName()).collect(Collectors.toList())
                    ),
                    CommonUtils.getOb(() -> contentValueCommonRepository.findInventoryCopDetail(inventory.getId())
                            .stream().map(ContentValueCommon::getId).collect(Collectors.toList())
                    ),
                    (baseInven, images, comps) -> {
//                        response.setBrandName(baseInven.getBicycleBrandName());
//                        response.setModelName(baseInven.getBicycleModelName());
//                        response.setYearName(baseInven.getBicycleYearName());
//                        response.setTypeName(baseInven.getBicycleTypeName());
                        response.setSizeName(baseInven.getBicycleSizeName());
                        response.setImages(images);
//                        response.setCops(comps);
                        if (response.getImageDefault() == null && response.getImages().size() > 0) {
                            response.setImageDefault(response.getImages().get(0));
                        }
                        return true;
                    }
            ).blockingFirst();
        }

//        response.setHasBuyItNow(inventoryAuction.isHasBuyItNow());
//        response.setBuyItNow(inventoryAuction.getBuyItNow());
//        response.setAuctionId(auction.getId());

        if (inventory.getStage() != StageInventory.LISTED) {
            if (auction.isStarted()) {
                inventory.setStage(StageInventory.LISTED);
                inventoryRepository.save(inventory);
            }

        }
        inventoryAuctionManagerAsync.delistMarketListingQueue(inventory.getId());
        return response;
    }


    public Object updateInventoryAuction(long inventoryAuctionId, InventoryAuctionUpdateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }
        InventoryAuction inventoryAuction = inventoryAuctionRepository.findOne(inventoryAuctionId);
        if (inventoryAuction == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_HAS_AUCTION),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (inventoryAuction.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, INVENTORY_AUCTION_DELETED),
                    HttpStatus.BAD_REQUEST
            );
        }
        Auction auction = auctionRepository.findOne(inventoryAuction.getAuctionId());
        LocalDateTime now = LocalDateTime.now();
        if (now.toDate().getTime() >= auction.getStartDate().toDate().getTime() &&
                now.toDate().getTime() <= auction.getEndDate().toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, INVENTORY_AUCTION_IN_BID_CAN_NOT_UPDATE),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (now.toDate().getTime() > auction.getEndDate().toDate().getTime()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_DELETE, AUCTION_EXPIRED),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (request.getMin() != null) {
            inventoryAuction.setMin(request.getMin());
        }
//        if (request.getDelete() != null) {
//            inventoryAuction.setDelete(request.getDelete());
//        }
        if (request.getHasBuyItNow() != null) {
            inventoryAuction.setHasBuyItNow(request.getHasBuyItNow());
        }
        if (request.getBuyItNow() != null) {
            inventoryAuction.setBuyItNow(request.getBuyItNow());
        }
        return inventoryAuctionRepository.save(inventoryAuction);


    }

    public Object getInventoryAuction(long inventoryAuctionId) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles()) &&
                !CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                    HttpStatus.BAD_REQUEST
            );
        }

        InventoryAuctionDetailResponse response = inventoryAuctionDetailResponseRepository.findOne(
                inventoryAuctionId,
                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                null
        );
        if (response == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_AUCTION_NOT_EXIST),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (response.getDuration() <= 0 && !response.isExpire()) {
            auctionExpireJob.runCheckExpireAuction(response.getAuctionId());
            response = inventoryAuctionDetailResponseRepository.findOne(
                    inventoryAuctionId,
                    new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                    null
            );
        }

        return checkAndUpdateAuctionExpire(response, inventoryAuctionId);
    }

    private InventoryAuctionDetailResponse checkAndUpdateAuctionExpire(InventoryAuctionDetailResponse response, long inventoryAuctionId) throws ExceptionResponse {
        if (response == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_AUCTION_NOT_EXIST),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            LocalDateTime now = LocalDateTime.now();
            if (response.getStartDate().toDate().getTime() > now.toDate().getTime()
//                    || now.toDate().getTime() > response.getEndDate().toDate().getTime()
            ) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_AUCTION_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        updateStatusInventoryAuction(response);
        List<InventoryCompTypeResponse> inventoryCompTypeResponses = inventoryCompTypeResRepository.findAllCompInventory(response.getInventoryId());
        updateCompInventoryAuctionDetail(response, inventoryCompTypeResponses);

        if (CommonUtils.isLogined()) {
            String userId = CommonUtils.getUserLogin();
            response.setFavourite(favouriteRepository.isFavourite(userId, null, inventoryAuctionId));
            inventoryAuctionManagerAsync.createTracking(userId, inventoryAuctionId);
        }
        response.setImages(
                inventoryImageRepository.findAllByInventoryId(response.getInventoryId())
                        .stream().map(InventoryImage::getImage).collect(Collectors.toList())
        );
        response.setTimeCall(LocalDateTime.now());

        CartSingleStageResponse cart = billingManager.checkStatusCart(response.getInventoryId()); //checkStatusCart(response.getInventoryId(), CommonUtils.getUserLogin());
        if (cart != null) {
            response.setStageCart(cart.getStage());
        }
        return response;

    }

    private void updateStatusInventoryAuction(InventoryAuctionDetailResponse response) {
        LocalDateTime now = LocalDateTime.now();
        long currentTime = now.toDate().getTime();

        if (response.getStartDate().toDate().getTime() > currentTime) {
            response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.PENDING);
        } else {
            Offer maxOffer;
            if (response.getMaxOfferId() != null) {
                maxOffer = offerRepository.findOne(response.getMaxOfferId());
            } else {
                maxOffer = null;
            }
            if (maxOffer != null && maxOffer.getStatus() != StatusOffer.PENDING) {
                if (maxOffer.getStatus() == StatusOffer.ACCEPTED) {
                    response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.SALE_PENDING);
                } else {
                    if (maxOffer.getStatus() == StatusOffer.CLOSED_CART_EXPIRED) {
                        response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.CLOSED_CART_EXPIRED);
                    } else {
                        if (maxOffer.getStatus() == StatusOffer.COMPLETED) {
                            response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.SOLD);
                        }
                    }
                }
            }
            if (response.getStatusInventoryAuctionOffer() == null) {
                if (response.getEndDate().toDate().getTime() >= currentTime) {
                    response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.LISTED);
                } else {
                    if (maxOffer == null) {
                        response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.CLOSE);
                    } else {
                        if (maxOffer.getStatus() == StatusOffer.PENDING) {
                            response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.LISTED);
                        } else {
                            response.setStatusInventoryAuctionOffer(StatusInventoryAuctionResponse.CLOSED_CART_EXPIRED);
                        }

                    }
                }
            }
        }
    }

    public void updateCompInventoryAuctionDetail(InventoryAuctionDetailResponse response, List<InventoryCompTypeResponse> inventoryCompTypeResponses) {
        if (inventoryCompTypeResponses.size() > 0) {
            for (InventoryCompTypeResponse inventoryCompTypeRespons : inventoryCompTypeResponses) {
                if (inventoryCompTypeRespons.getName() == null) {
                    continue;
                }
                switch (inventoryCompTypeRespons.getName()) {
                    case ValueCommons.SIZE_NAME_INV_COMP:
                        response.setSizeName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.SUSPENSION_NAME_INV_COMP:
                        response.setSuspensionName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.FRAME_MATERIAL_NAME_INV_COMP:
                        response.setFrameMaterialName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.BRAKE_TYPE_NAME_INV_COMP:
                        response.setBrakeName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.BOTTOM_BRACKET_NAME_INV_COMP:
                        response.setBottomBracketName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.GENDER_NAME_INV_COMP:
                        response.setGenderName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.COLOR_INV_COMP:
                        response.setColorName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.HEAD_SET_INV_COMP:
                        response.setHeadsetName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.INVENTORY_WHEEL_SIZE_NAME:
                        response.setWheelSizeName(inventoryCompTypeRespons.getValue());
                        break;
                    default:
                        break;
                }

            }
        }
    }

    public Object getInventoryAuctionAllBid(long inventoryAuctionId) throws ExceptionResponse {
        return bidOfferResponseRepository.findAllBid(
                inventoryAuctionId,
                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate())
        );
    }
}
