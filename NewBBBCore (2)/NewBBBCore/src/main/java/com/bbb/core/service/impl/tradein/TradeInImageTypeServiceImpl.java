package com.bbb.core.service.impl.tradein;

import com.bbb.core.manager.tradein.TradeInImageTypeManager;
import com.bbb.core.model.database.TradeInImageType;
import com.bbb.core.service.tradein.TradeInImageTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeInImageTypeServiceImpl implements TradeInImageTypeService {
    @Autowired
    private TradeInImageTypeManager tradeInImageTypeManager;

    @Override
    public List<TradeInImageType> getImagesTypes() {
        return tradeInImageTypeManager.getImageTypes();
    }
}
