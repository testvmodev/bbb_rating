package com.bbb.core.model.request.tradein.fedex.detail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Contact {
    @JacksonXmlProperty(localName = "PersonName")
    private String personName;
    @JacksonXmlProperty(localName = "CompanyName")
    private String companyName;
    @JacksonXmlProperty(localName = "PhoneNumber")
    private String phoneNumber;
    @JacksonXmlProperty(localName = "EMailAddress")
    private String eMailAddress;

    public Contact() {}

    public Contact(String personName, String companyName, String phoneNumber) {
        this.personName = personName;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }
}
