package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventorySpec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventorySpecRepository extends JpaRepository<InventorySpec, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory_spec WHERE inventory_spec.inventory_id = :inventoryId")
    List<InventorySpec> findAll(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_spec WHERE inventory_spec.inventory_id IN :inventoriesId")
    List<InventorySpec> findAll(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );
}
