package com.bbb.core.model.otherservice.response.valueguide;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ValueGuideResponse {
    @JsonProperty("Condition")
    private String conditionInventory;
    @JsonProperty("Value")
    private Float value;
    @JsonProperty("ValueString")
    private String valueString;
    @JsonIgnore
    private Float averageValue;

    public String getConditionInventory() {
        return conditionInventory;
    }

    public void setConditionInventory(String conditionInventory) {
        this.conditionInventory = conditionInventory;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    @JsonIgnore
    public Float getAverageValue() {
        return averageValue;
    }

    public void setAverageValue(Float averageValue) {
        this.averageValue = averageValue;
    }
}
