package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Event {
    @JacksonXmlProperty(localName = "EventType")
    private String eventType;
    @JacksonXmlProperty(localName = "EventDescription")
    private String eventDescription;
    @JacksonXmlProperty(localName = "Address")
    private Address address;
    @JacksonXmlProperty(localName = "ArrivalLocation")
    private String arrivalLocation;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }
}
