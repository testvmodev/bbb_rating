package com.bbb.core.manager.bicycle;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.ComponentType;
import com.bbb.core.model.request.component.ComponentTypeCreateRequest;
import com.bbb.core.model.request.component.ComponentTypeGetRequest;
import com.bbb.core.model.request.component.ComponentTypeUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.component.ComponentTypeResponse;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.BicycleComponentRepository;
import com.bbb.core.repository.bicycle.ComponentCategoryRepository;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import com.bbb.core.repository.bicycle.ComponentTypeSelectRepository;
import com.bbb.core.repository.component.ComponentTypeResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;


@Component
public class ComponentTypeManager {
    @Autowired
    private ComponentTypeRepository componentTypeRepository;

    @Autowired
    private ComponentCategoryRepository componentCategoryRepository;
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private BicycleComponentRepository bicycleComponentRepository;
    @Autowired
    private ComponentTypeSelectRepository componentTypeSelectRepository;
    @Autowired
    private ComponentTypeResponseRepository componentTypeResponseRepository;

    //    public Object importComponentType() {
//        try {
//            Class.forName(Constants.DRIVER_SQL);
//            Connection conn = DriverManager.getConnection(Constants.PATH_DB_OLD_BICYCLE, Constants.USERNAME_OLD, Constants.PASSWORD_OLD);
//            String query = "SELECT * FROM [Bicycle].[ComponentType]";
//            Statement stmt = conn.createStatement();
//            ResultSet resultSet = stmt.executeQuery(query);
//            List<ComponentType> componentTypes = new ArrayList<>();
//            while (resultSet.next()){
//                long id = resultSet.getLong("Id");
//                String name = resultSet.getString("Name");
//                ComponentType componentType = new ComponentType();
//                componentType.setId(id);
//                componentType.setName(name);
//                componentTypes.add(componentType);
//            }
//            resultSet.close();
//            stmt.close();
//            conn.close();
//            return componentTypeRepository.saveAll(componentTypes);
//
//        } catch (ClassNotFoundException | SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public Object getComponentTypes(ComponentTypeGetRequest request) throws ExceptionResponse {
        ListObjResponse<ComponentTypeResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(componentTypeRepository.countAll());
        if (response.getTotalItem() > 0) {
            response.setData(componentTypeResponseRepository.findAll(
                    request.getSortField(),
                    request.getSortType(),
                    request.getPageable()
            ));
        }
        return response;
    }

    public Object getDetailComponentType(long typeId) throws ExceptionResponse{
        ComponentType componentType = componentTypeRepository.findById(typeId);
        if (componentType == null){
            throw new ExceptionResponse( new ObjectError(ObjectError.ERROR_PARAM,MessageResponses.COMPONENT_TYPE_NOT_EXIST ), HttpStatus.BAD_REQUEST);
        }

        return componentTypeResponseRepository.findOneDetail(typeId);
    }

    public Object createComponentType(ComponentTypeCreateRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentType componentType = new ComponentType();
        if (request == null || request.getName().trim().equals("")){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NAME_NOT_EMPTY), HttpStatus.BAD_REQUEST);
        }
        if (componentTypeRepository.findByName(request.getName()) != null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_EXIST), HttpStatus.BAD_REQUEST);
        }
        if (request.getComponentCategoryId() == null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_CATEGORY_ID_NOT_EMPTY ), HttpStatus.BAD_REQUEST);
        }
        if (componentCategoryRepository.findById(request.getComponentCategoryId())== null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_CATEGORY_NOT_EXIST ), HttpStatus.BAD_REQUEST);
        }
        componentType.setName(request.getName());
        componentType.setComponentCatsegoryId(request.getComponentCategoryId());
        if(request.getSortOrder() == null){
            componentType.setSortOrder(0);
        }
        componentType.setSortOrder(request.getSortOrder());
        if (request.getForOnlyBicycleTypeId() != null ){
            if (bicycleTypeRepository.findOne(request.getForOnlyBicycleTypeId()) == null){
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.BICYCLE_TYPE_NOT_EXIST), HttpStatus.BAD_REQUEST);
            }
            componentType.setForOnlyBicycleTypeId(request.getForOnlyBicycleTypeId());
        }
        componentType.setSelect(request.isSelect());
        componentType.setDelete(false);
        return componentTypeRepository.save(componentType);
    }

    public Object updateComponentType(long typeId, ComponentTypeUpdateRequest request) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentType componentType = componentTypeRepository.findById(typeId);
        if (componentType == null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_EXIST), HttpStatus.BAD_REQUEST);
        }
        if (request == null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NAME_NOT_EMPTY), HttpStatus.BAD_REQUEST);
        }

        if (request.getName()!= null){
            if(request.getName().trim().equals("")){
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NAME_NOT_EMPTY), HttpStatus.BAD_REQUEST);
            }
            componentType.setName(request.getName());
        }
        if (request.getSortOrder() != null ){
            componentType.setSortOrder(request.getSortOrder());
        }
        if (request.getComponentCategoryId() != null  ){
            if (componentCategoryRepository.findById(request.getComponentCategoryId())== null){
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_CATEGORY_NOT_EXIST ), HttpStatus.BAD_REQUEST);
            }
            componentType.setComponentCatsegoryId(request.getComponentCategoryId());
        }
        if (request.getForOnlyBicycleTypeId() != null ){
            if (bicycleTypeRepository.findOne(request.getForOnlyBicycleTypeId()) == null){
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.BICYCLE_TYPE_NOT_EXIST), HttpStatus.BAD_REQUEST);
            }
            componentType.setForOnlyBicycleTypeId(request.getForOnlyBicycleTypeId());
        }
        if (request.isSelect() != null){
            componentType.setSelect(request.isSelect());
        }
        return componentTypeRepository.save(componentType);
    }

    public Object deleteComponentType(long typeId) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentType componentType = componentTypeRepository.findById(typeId);
        if (componentType == null){
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_EXIST), HttpStatus.BAD_REQUEST);
        }
        int countBicycleComp = bicycleComponentRepository.getCountByCompId(typeId);
        if (countBicycleComp >0 ){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM,
                            (countBicycleComp==1? MessageResponses.THERE_IS:MessageResponses.THERE_ARE+ countBicycleComp )+
                                    MessageResponses.BICYCLE_COMPONENT_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        int countCompTypeSelect = componentTypeSelectRepository.getCountByCompTypeId(typeId);
        if (countCompTypeSelect >0 ){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM,
                            (countCompTypeSelect==1? MessageResponses.THERE_IS:MessageResponses.THERE_ARE+ countCompTypeSelect )+
                                    MessageResponses.COMPONENT_TYPE_SELECT_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        componentType.setDelete(true);
        componentTypeRepository.save(componentType);
        return MessageResponses.SUCCESS;
    }
}
