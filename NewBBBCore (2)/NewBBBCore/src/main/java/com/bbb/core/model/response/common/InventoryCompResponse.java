package com.bbb.core.model.response.common;

import com.bbb.core.model.database.InventoryCompType;
import com.bbb.core.model.database.InventoryCompTypeSelect;

import java.util.List;

public class InventoryCompResponse extends InventoryCompType {
    private List<InventoryCompTypeSelect> selects;
    private int sort;

    public List<InventoryCompTypeSelect> getSelects() {
        return selects;
    }

    public void setSelects(List<InventoryCompTypeSelect> selects) {
        this.selects = selects;
    }

    public void clone(InventoryCompType type){
        this.id = type.getId();
        this.name = type.getName();
        this.isSelect = type.isSelect();
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
