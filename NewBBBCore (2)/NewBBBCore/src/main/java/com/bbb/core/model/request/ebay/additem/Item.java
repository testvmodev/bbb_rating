package com.bbb.core.model.request.ebay.additem;

import com.bbb.core.model.request.ebay.*;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.bbb.core.model.request.ebay.BestOfferDetails;

public class Item {
    @JacksonXmlProperty(localName = "Country")
    private String country;

    @JacksonXmlProperty(localName = "Currency")
    private String currency;

    @JacksonXmlProperty(localName = "ConditionID")
    private long conditionId;

    @JacksonXmlProperty(localName = "DispatchTimeMax")
    private int dispatchTimeMax;

    @JacksonXmlProperty(localName = "ListingDuration")
    private String listingDuration;

    @JacksonXmlProperty(localName = "ListingType")
    private String listingType;

    @JacksonXmlProperty(localName = "PaymentMethods")
    private String paymentMethods;

    @JacksonXmlProperty(localName = "PayPalEmailAddress")
    private String payPalEmailAddress;

    @JacksonXmlProperty(localName = "PostalCode")
    private String postalCode;

    @JacksonXmlProperty(localName = "PrimaryCategory")
    private PrimaryCategory primaryCategory;


    @JacksonXmlProperty(localName = "SecondaryCategory")
    private PrimaryCategory secondaryCategory;

    @JacksonXmlProperty(localName = "Title")
    private String title;
    @JacksonXmlProperty(localName = "SubTitle")
    private String subTitle = "30 Day Guarantee / 24 Hr Processing";

    @JacksonXmlProperty(localName = "Description")
    private String description;

    @JacksonXmlProperty(localName = "StartPrice")
    private float startPrice;

    @JacksonXmlProperty(localName = "DiscountPriceInfo")
    private DiscountPriceInfo discountPriceInfo;

    @JacksonXmlProperty(localName = "CategoryMappingAllowed")
    private boolean categoryMappingAllowed = true;

    @JacksonXmlProperty(localName = "PictureDetails")
    private PictureDetails pictureDetails;


    @JacksonXmlProperty(localName = "Quantity")
    private int quantity;


    @JacksonXmlProperty(localName = "ItemSpecifics")
    private ItemSpecifics itemSpecifics;


    @JacksonXmlProperty(localName = "ReturnPolicy")
    private ReturnPolicy returnPolicy;

    @JacksonXmlProperty(localName = "ShippingDetails")
    private ShippingDetailsRequest shippingDetails;


//    @JacksonXmlProperty(localName = "Variations")
//    private Variations variations;

//    @JacksonXmlProperty(localName = "QuantityRestrictionPerBuyer")
//    private QuantityRestrictionPerBuyer quantityRestrictionPerBuyer;
//
//    @JacksonXmlProperty(localName = "QuantityInfo")
//    private QuantityInfo quantityInfo;

    @JacksonXmlProperty(localName = "SellerProfiles")
    private SellerProfiles sellerProfiles;

    @JacksonXmlProperty(localName = "Site")
    private String site;
    @JacksonXmlProperty(localName = "BestOfferDetails")
    private BestOfferDetails bestOfferDetails;
    @JacksonXmlProperty(localName = "ListingDetails")
    private ListingDetails listingDetails;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getDispatchTimeMax() {
        return dispatchTimeMax;
    }

    public void setDispatchTimeMax(int dispatchTimeMax) {
        this.dispatchTimeMax = dispatchTimeMax;
    }

    public String getListingDuration() {
        return listingDuration;
    }

    public void setListingDuration(String listingDuration) {
        this.listingDuration = listingDuration;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getPayPalEmailAddress() {
        return payPalEmailAddress;
    }

    public void setPayPalEmailAddress(String payPalEmailAddress) {
        this.payPalEmailAddress = payPalEmailAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public PrimaryCategory getPrimaryCategory() {
        return primaryCategory;
    }

    public void setPrimaryCategory(PrimaryCategory primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public PrimaryCategory getSecondaryCategory() {
        return secondaryCategory;
    }

    public void setSecondaryCategory(PrimaryCategory secondaryCategory) {
        this.secondaryCategory = secondaryCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PictureDetails getPictureDetails() {
        return pictureDetails;
    }

    public void setPictureDetails(PictureDetails pictureDetails) {
        this.pictureDetails = pictureDetails;
    }

    public ReturnPolicy getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(ReturnPolicy returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public ShippingDetailsRequest getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(ShippingDetailsRequest shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    public ItemSpecifics getItemSpecifics() {
        return itemSpecifics;
    }

    public void setItemSpecifics(ItemSpecifics itemSpecifics) {
        this.itemSpecifics = itemSpecifics;
    }

//    public Variations getVariations() {
//        return variations;
//    }
//
//    public void setVariations(Variations variations) {
//        this.variations = variations;
//    }

    public SellerProfiles getSellerProfiles() {
        return sellerProfiles;
    }

    public void setSellerProfiles(SellerProfiles sellerProfiles) {
        this.sellerProfiles = sellerProfiles;
    }

    public long getConditionId() {
        return conditionId;
    }

    public void setConditionId(long conditionId) {
        this.conditionId = conditionId;
    }

//    public QuantityRestrictionPerBuyer getQuantityRestrictionPerBuyer() {
//        return quantityRestrictionPerBuyer;
//    }
//
//    public void setQuantityRestrictionPerBuyer(QuantityRestrictionPerBuyer quantityRestrictionPerBuyer) {
//        this.quantityRestrictionPerBuyer = quantityRestrictionPerBuyer;
//    }
//
//    public QuantityInfo getQuantityInfo() {
//        return quantityInfo;
//    }
//
//    public void setQuantityInfo(QuantityInfo quantityInfo) {
//        this.quantityInfo = quantityInfo;
//    }

    public boolean isCategoryMappingAllowed() {
        return categoryMappingAllowed;
    }

    public void setCategoryMappingAllowed(boolean categoryMappingAllowed) {
        this.categoryMappingAllowed = categoryMappingAllowed;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(float startPrice) {
        this.startPrice = startPrice;
    }

    public DiscountPriceInfo getDiscountPriceInfo() {
        return discountPriceInfo;
    }

    public void setDiscountPriceInfo(DiscountPriceInfo discountPriceInfo) {
        this.discountPriceInfo = discountPriceInfo;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public BestOfferDetails getBestOfferDetails() {
        return bestOfferDetails;
    }

    public void setBestOfferDetails(BestOfferDetails bestOfferDetails) {
        this.bestOfferDetails = bestOfferDetails;
    }

    public ListingDetails getListingDetails() {
        return listingDetails;
    }

    public void setListingDetails(ListingDetails listingDetails) {
        this.listingDetails = listingDetails;
    }
}

