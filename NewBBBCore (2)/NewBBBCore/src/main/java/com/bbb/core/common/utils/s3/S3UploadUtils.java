package com.bbb.core.common.utils.s3;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.response.ObjectError;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class S3UploadUtils {
    private static final String BASE_PATH_S3 = "https://s3.amazonaws.com";


    public static <T> List<S3UploadResponse<T>> uploads(S3Manager s3Manager, List<S3UploadImage<T>> s3UploadImages) {
        if (s3UploadImages.size() == 1) {
            List<S3UploadResponse<T>> keys = new ArrayList<>();
            keys.add(upload(s3Manager, s3UploadImages.get(0)));
            return keys;
        } else {
            List<Observable<S3UploadResponse<T>>> os = new ArrayList<>();
            for (S3UploadImage<T> s3UploadImage : s3UploadImages) {
                os.add(uploadAsy(s3Manager, s3UploadImage));
            }
            return Observable.zip(os, osString -> {
                List<S3UploadResponse<T>> keys = new ArrayList<>();
                for (Object o : osString) {
                    keys.add((S3UploadResponse<T>) o);
                }
                return keys;
            }).blockingFirst();
        }
    }

    public static <T> S3UploadResponse<T> upload(S3Manager s3Manager, S3UploadImage<T> s3UploadImage) {
        String key = CommonUtils.getKeyHashEndNameFileImage();
        if (!StringUtils.isBlank(s3UploadImage.getFilePrefix())) {
            key = s3UploadImage.getFilePrefix() + "_" + key;
        }
        String fileName = s3UploadImage.getFile().getOriginalFilename();
        if (fileName.contains(".")) {
            String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
            key = key + "." + ext;
        }
        if (s3UploadImage.getFolder() != null) {
            key = s3UploadImage.getFolder() + "/" + key;
        }
        s3Manager.upload(s3UploadImage.getFile(), key);
        return new S3UploadResponse<>(key, s3UploadImage.getOtherData());
    }

    private static <T> Observable<S3UploadResponse<T>> uploadAsy(S3Manager s3Manager, S3UploadImage<T> s3UploadImage) {
        return Observable.create((ObservableOnSubscribe<S3UploadResponse<T>>) s -> {
            s.onNext(upload(s3Manager, s3UploadImage));
            s.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public static <T> List<S3UploadResponse<T>> upload(
            List<S3UploadFromUrl<T>> s3UploadUrls, S3Manager s3Manager
    ) throws ExceptionResponse {
        if (s3UploadUrls == null) return null;
        if (s3UploadUrls.size() < 1) {
            return new ArrayList<>();
        }

        if (s3UploadUrls.size() == 1) {
            return Collections.singletonList(upload(s3UploadUrls.get(0), s3Manager));
        } else {
            List<Observable<S3UploadResponse<T>>> os = new ArrayList<>();
            for (S3UploadFromUrl<T> s3UploadUrl : s3UploadUrls) {
                os.add(uploadAsync(s3UploadUrl, s3Manager));
            }
            return Observable.zip(os, osResponses -> {
                List<S3UploadResponse<T>> responses = new ArrayList<>();
                for (Object o : osResponses) {
                    responses.add((S3UploadResponse<T>)o);
                }
                return responses;
            }).blockingFirst();
        }
    }

    public static <T> S3UploadResponse<T> upload(
            S3UploadFromUrl<T> s3UploadUrl, S3Manager s3Manager
    ) throws ExceptionResponse {
        String key = CommonUtils.getKeyHashEndNameFileImage();
        try {
            URLConnection connection = s3UploadUrl.getUrl().openConnection();
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(5000);

            InputStream inputStream = connection.getInputStream();
            String contentType = connection.getContentType();
            int size = connection.getContentLength();

            if (!StringUtils.isBlank(s3UploadUrl.getPre())) {
                key = s3UploadUrl.getPre() + "_" + key;
            }
            String fileName = s3UploadUrl.getUrl().getFile();
            if (!StringUtils.isBlank(fileName)) {
                if (fileName.contains("/")) {
                    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                }
                if (fileName.contains(".")) {
                    String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
                    key += "." + fileExtension;
                }
            }
            if (!StringUtils.isBlank(s3UploadUrl.getFolder())) {
                key = s3UploadUrl.getFolder() + "/" + key;
            }

            s3Manager.upload(inputStream, size, contentType, key);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ExceptionResponse(
                    ObjectError.ERROR_REST_TEMPLATE,
                    MessageResponses.CANT_ACCESS_URL,
                    HttpStatus.BAD_REQUEST
            );
        }
        return new S3UploadResponse<T>(key, s3UploadUrl.getOtherData());
    }

    private static <T> Observable<S3UploadResponse<T>> uploadAsync(
            S3UploadFromUrl<T> s3UploadUrl, S3Manager s3Manager
    ) {
        return Observable.create((ObservableOnSubscribe<S3UploadResponse<T>>) s -> {
            s.onNext(upload(s3UploadUrl, s3Manager));
            s.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }

    public static String getFullLinkS3(AwsResourceConfig awsResourceConfig, String key) {
        return BASE_PATH_S3 + "/" + awsResourceConfig.getDefaultS3Bucket() + "/" + key;
    }
}
