package com.bbb.core.manager.market;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.bid.OfferManager;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.market.async.ListingWizardManagerAsync;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.tracking.TrackingManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.request.ebay.notification.EbayNotificationRequest;
import com.bbb.core.model.request.ebay.notification.GetItemTransactionsResponse;
import com.bbb.core.model.request.market.MarketListingRequest;
import com.bbb.core.model.request.market.UserMarketListingRequest;
import com.bbb.core.model.request.sort.InventorySortField;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.market.marketlisting.MarketListingResponse;
import com.bbb.core.model.response.market.marketlisting.UserMarketListingResponse;
import com.bbb.core.model.response.market.marketlisting.detail.InventoryCompTypeResponse;
import com.bbb.core.model.response.market.marketlisting.detail.MarketListingDetailResponse;
import com.bbb.core.model.response.market.marketlisting.detail.MarketListingSecretDetailResponse;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.favourite.FavouriteRepository;
import com.bbb.core.repository.inventory.*;
import com.bbb.core.repository.market.*;
import com.bbb.core.repository.market.out.MarketListingResponseRepository;
import com.bbb.core.repository.market.out.UserMarketListingResponseRepository;
import com.bbb.core.repository.market.out.detail.InventoryCompTypeResRepository;
import com.bbb.core.repository.market.out.detail.MarketListingDetailRepository;
import com.bbb.core.repository.market.out.detail.MarketListingSecretDetailResponseRepository;
import com.bbb.core.repository.tracking.TrackingRepository;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MarketListingManager implements MessageResponses, EbayValue {
    private static final Logger LOG = LoggerFactory.getLogger(MarketListingManager.class);
    //region Beans
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private XmlMapper xmlMapper;
    @Autowired
    private LogEbayNotificationRepository logEbayNotificationRepository;
    @Autowired
    private InventorySaleRepository inventorySaleRepository;
    @Autowired
    private MarketListingDetailRepository marketListingDetailRepository;
    @Autowired
    private InventoryCompTypeResRepository inventoryCompTypeResRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private MarketListingResponseRepository marketListingResponseRepository;
    @Autowired
    private FavouriteRepository favouriteRepository;
    @Autowired
    private TrackingRepository trackingRepository;
    @Autowired
    private OfferManager offerManager;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private TrackingManager trackingManager;
    @Autowired
    private ListingWizardManagerAsync listingWizardManagerAsync;
    @Autowired
    private UserMarketListingResponseRepository userMarketListingResponseRepository;
    @Autowired
    private InventoryCompTypeSelectRepository inventoryCompTypeSelectRepository;
    @Autowired
    private MarketListingSecretDetailResponseRepository marketListingSecretDetailResponseRepository;
    //endregion

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getMarketListings(MarketListingRequest request, Pageable page) throws ExceptionResponse {
        if (request.getStartPrice() != null && request.getEndPrice() != null) {
            if (request.getStartPrice() > request.getEndPrice()) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, START_PRICE_MUST_LESS_OR_EQUAL_THAN_END_PRICE),
                        HttpStatus.BAD_REQUEST);
            }
        }
        if (request.getStartYearId() != null && request.getEndYearId() != null) {
            if (request.getStartYearId() > request.getEndYearId()) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, START_YEAR_MUST_LESS_OR_EQUAL_THAN_END_YEAR),
                        HttpStatus.BAD_REQUEST);
            }
        }
        List<String> comps = new ArrayList<>();
        comps.add(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME);
        comps.add(ValueCommons.INVENTORY_BRAKE_TYPE_NAME);
        comps.add(ValueCommons.INVENTORY_SIZE_NAME);
        List<InventoryCompType> compInvs = inventoryCompTypeRepository.findAllByName(comps);


        ListObjResponse<MarketListingResponse> response = new ListObjResponse<>();
        List<Long> temList = new ArrayList<>();
        temList.add(0L);
        List<String> temListString = new ArrayList<>();
        temListString.add("Demo");
        List<String> frameSizes = inventoryCompTypeSelectRepository.findAllFromSizeCategory(request.getSizeNames()).stream()
                .map(comp -> comp.getValue())
                .collect(Collectors.toList());
        boolean isNullBicycle = request.getBicycleIds() == null || request.getBicycleIds().size() == 0;
        boolean isNullModel = request.getModelIds() == null || request.getModelIds().size() == 0;
        boolean isNullBrand = request.getBrandIds() == null || request.getBrandIds().size() == 0;
        boolean isNullType = request.getTypeIds() == null || request.getTypeIds().size() == 0;
        boolean isNullTypeBicycleName = request.getTypeBicycleNames() == null || request.getTypeBicycleNames().size() == 0;
        boolean isNullFrame = request.getFrameMaterialNames() == null || request.getFrameMaterialNames().size() == 0;
        boolean isNullBrake = request.getBrakeTypeNames() == null || request.getBrakeTypeNames().size() == 0;
        boolean isNullSize = frameSizes == null || frameSizes.size() == 0;
        boolean isGenderNull = request.getGenders() == null || request.getGenders().size() == 0;
        boolean isSuspensionNull = request.getSuspensions() == null || request.getSuspensions().size() == 0;
        boolean isWheelSizeNull = request.getWheelSizes() == null || request.getWheelSizes().size() == 0;
        boolean isNullCondition = request.getConditions() == null || request.getConditions().size() == 0;
        boolean isLocationRadiusNull = request.getLatitude() == null || request.getLongitude() == null || request.getRadius() == null;
        boolean isListingIdNull = request.getMarketListingIds() == null || request.getMarketListingIds().size() < 1;


        Long marketPlaceId = null;
        //best deal only show bbb listing
        if (request.getSortField() == InventorySortField.BEST_DEAL) {
            request.setMarketType(MarketType.BBB);
        }
        if (request.getMarketType() != null) {
            marketPlaceId = marketPlaceRepository.getMarketPlaceId(request.getMarketType().getValue());
            if (marketPlaceId == null) {
                //null mean this param wasn't used, but it did, should be no listings found
                marketPlaceId = -1l;
            }
        }

        response.setTotalItem(
                marketListingResponseRepository.getTotalCountMarketPlace(
                        isNullBicycle, isNullBicycle ? temList : request.getBicycleIds(),
                        isNullModel, isNullModel ? temList : request.getModelIds(),
                        isNullBrand, isNullBrand ? temList : request.getBrandIds(),
                        isNullType, isNullType ? temList : request.getTypeIds(),
                        isNullTypeBicycleName, isNullTypeBicycleName ? temListString : request.getTypeBicycleNames(),

                        isNullFrame, isNullFrame ? temListString : request.getFrameMaterialNames(),
                        isNullBrake, isNullBrake ? temListString : request.getBrakeTypeNames(),
                        isNullSize, isNullSize ? temListString : frameSizes,
                        isGenderNull, isGenderNull ? temListString : request.getGenders(),
                        isSuspensionNull, isSuspensionNull ? temListString : request.getSuspensions(),
                        isWheelSizeNull, isWheelSizeNull ? temListString : request.getWheelSizes(),

                        isNullCondition, isNullCondition ? temListString : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),

                        request.getStatusMarketListing() == null ? null : request.getStatusMarketListing().getValue(),
                        marketPlaceId,
                        request.getName(),

                        request.getStartPrice() == null, request.getStartPrice() == null ? 0 : request.getStartPrice(),
                        request.getEndPrice() == null, request.getEndPrice() == null ? 0 : request.getEndPrice(),
                        request.getStartYearId(),
                        request.getEndYearId(),

                        request.getSellerType(),

                        request.getZipCode(),
                        request.getCityName(),
                        request.getState(),
                        request.getCountry(),
                        isLocationRadiusNull,
                        isLocationRadiusNull ? 0 : request.getLatitude(),
                        isLocationRadiusNull ? 0 : request.getLongitude(),
                        isLocationRadiusNull ? 0 : request.getRadius(),

                        request.getContent(),

                        request.getStorefrontId(),

                        isListingIdNull, isListingIdNull ? temList : request.getMarketListingIds(),

                        request.getSortField().name()
                )
        );
        if (response.getTotalItem() > 0) {
            response.setData(
                    marketListingResponseRepository.findAllMarketPlaceSorted(
                            request.getBicycleIds(),
                            request.getModelIds(),
                            request.getBrandIds(),
                            request.getTypeIds(),
                            request.getTypeBicycleNames(),

                            request.getFrameMaterialNames(),
                            request.getBrakeTypeNames(),
                            frameSizes,
                            request.getGenders(),
                            request.getSuspensions(),
                            request.getWheelSizes(),

                            isNullCondition ? null : request.getConditions().stream().map(ConditionInventory::getValue).collect(Collectors.toList()),

                            request.getStatusMarketListing(),
                            marketPlaceId,
                            request.getName(),

                            request.getStartPrice(),
                            request.getEndPrice(),
                            request.getStartYearId(),
                            request.getEndYearId(),

                            request.getSellerType(),

                            request.getZipCode(),
                            request.getCityName(),
                            request.getState(),
                            request.getCountry(),
                            request.getLatitude(),
                            request.getLongitude(),
                            request.getRadius(),

                            request.getContent(),

                            request.getStorefrontId(),

                            request.getMarketListingIds(),

                            request.getSortField().name(),
                            request.getSortType().name(),
                            page.getOffset(),
                            page.getPageSize()
                    )
            );

            //set favourite
            if (response.getData().size() > 0 && CommonUtils.isLogined()) {
//                List<Long> favouriteListing = favouriteManager.checkFavourite(
//                        userContext.getId(),
//                        response.getData().stream().map(res -> res.getId()).collect(Collectors.toList())
//                );
                List<Integer> favouriteListing = favouriteRepository.filterMarketListingFavourites(
                        CommonUtils.getUserLogin(),
                        response.getData().stream().map(MarketListingResponse::getId).collect(Collectors.toList())
                );
                for (MarketListingResponse res : response.getData()) {
                    if (favouriteListing.contains(res.getId())) {
                        res.setFavourite(true);
                    } else {
                        res.setFavourite(false);
                    }
                }
            }
        } else {
            response.setData(new ArrayList<>());
        }
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getMarketListingDetail(long marketListingId, boolean isView) throws ExceptionResponse {
        boolean isLogin = CommonUtils.isLogined();
        String userId;
        if (isLogin) {
            userId = CommonUtils.getUserLogin();
        } else {
            userId = null;
        }

        MarketListingDetailResponse response = marketListingDetailRepository.findOne(
                marketListingId,
                userId
        );
        if (response == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST), HttpStatus.NOT_FOUND);
        }

        if (response.getMarketType() == MarketType.EBAY) {
            if (CommonUtils.isLogined()) {
                if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.NO_PERMISSION, YOU_DONT_HAVE_PERMISSION),
                            HttpStatus.FORBIDDEN
                    );
                }
            } else {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.NO_PERMISSION, YOU_DONT_HAVE_PERMISSION),
                        HttpStatus.FORBIDDEN
                );
            }
        }

        List<Observable<Object>> obs = new ArrayList<>();
        obs.add(CommonUtils.getObObj(() -> {
            List<InventoryCompTypeResponse> inventoryCompTypeResponses =
                    inventoryCompTypeResRepository.findAllCompInventory(response.getInventoryId());
            marketListingManagerAsync.updateCompMarketListingDetailNotAsyn(response, inventoryCompTypeResponses);
            return inventoryCompTypeResponses;
        }));
        obs.add(CommonUtils.getObObj(() -> {
            List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoryId(
                    response.getInventoryId()
            );
            response.setImages(inventoryImages.stream()
                    .map(InventoryImage::getImage)
                    .collect(Collectors.toList())
            );
            return inventoryImages;
        }));
        if (userId != null) {
//            obs.add(CommonUtils.getObObj(() -> {
//                Favourite favourite = favouriteRepository.findOne(userId, response.getMarketListingId(), null);
//                response.setFavourite(favourite != null);
//                return favourite;
//            }));

            if (CommonUtils.isSeller(response.getSellerId(), response.getPartnerId(), response.getStorefrontId())) {
                obs.add(CommonUtils.getObObj(() -> {
                            InventorySale inventorySale = inventorySaleRepository.findOne(response.getInventoryId());
                            response.setSale(inventorySale);
                            return inventorySale == null ? false : inventorySale;
                }));
                obs.add(CommonUtils.getObObj(() -> {
                    int offers = offerRepository.countListingOffers(response.getMarketListingId());
                    response.setOfferCount(offers);
                    return offers;
                }));
            }
        }
        Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            if (isView) {
                MarketListing marketListing = marketListingRepository.findOneInMarketPlace(
                        marketListingId,
                        MarketType.BBB
                );
                if (marketListing != null) {
                    //TODO optimize update tracking count to only 1 query
                    marketListing.setTrackingCount(marketListing.getTrackingCount() + 1);
                    marketListingRepository.save(marketListing);
                    //end

                    if (userId != null) {
                        Tracking tracking = trackingRepository.findOneMarketListing(userId, marketListingId);
                        if (tracking == null) {
                            tracking = new Tracking();
                        }
                        tracking.setLastViewed(LocalDateTime.now());
                        tracking.setMarketListingId(marketListingId);
                        tracking.setUserId(userId);
                        tracking.setType(TrackingType.BBB);
                        tracking.setCountTracking(tracking.getCountTracking() + 1);
                        trackingRepository.save(tracking);
                    }
                }

                t.onNext(true);
                t.onComplete();
            }
        }).subscribeOn(Schedulers.newThread())
                .subscribe();
//        if (CommonUtils.isLogined()) {
//            CartSingleStageResponse cart = billingManager.checkStatusCart(response.getInventoryId(), CommonUtils.getUserLogin());
//            if (cart != null) {
//                response.setStageCart(cart.getStage());
//            }
//        }

        Observable.zip(obs, o -> o).blockingFirst();

        return response;
    }


    public Object notificationEbay(String contentNotification) throws Exception {
        LogEbayNotification ebayNotification = new LogEbayNotification();
        try {
            EbayNotificationRequest convert = xmlMapper.readValue(contentNotification, EbayNotificationRequest.class);
            io.reactivex.Observable.create((ObservableOnSubscribe<Boolean>) t -> {
                handlerEbayNotification(convert);
                t.onNext(true);
                t.onComplete();
            }).subscribeOn(Schedulers.newThread())
                    .subscribe();
            if (convert.getBody().getItemResponse() != null) {
                ebayNotification.setType(convert.getBody().getItemResponse().getNotificationEventName());
                ebayNotification.setItemId(convert.getBody().getItemResponse().getItem().getItemID());
            } else {
                if (convert.getBody().getItemTransactionsResponse() != null) {
                    ebayNotification.setType(convert.getBody().getItemTransactionsResponse().getNotificationEventName());
                    ebayNotification.setItemId(convert.getBody().getItemTransactionsResponse().getItem().getItemID());
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            ebayNotification.setContent(contentNotification);
            logEbayNotificationRepository.save(ebayNotification);
            throw e;
        }
        ebayNotification.setContent(contentNotification);
        logEbayNotificationRepository.save(ebayNotification);
        return "Success";
    }

    private void handlerEbayNotification(EbayNotificationRequest ebayNotification) {
        if (ebayNotification.getBody() == null) {
            return;
        }
        if (ebayNotification.getBody().getItemTransactionsResponse() != null) {
            switch (ebayNotification.getBody().getItemTransactionsResponse().getNotificationEventName()) {
                case AUCTION_CHECKOUT_COMPLETE:
                    handlerEbayComplete(ebayNotification.getBody().getItemTransactionsResponse());
                    break;
                case ITEM_UNSOLD:
                    long itemId = ebayNotification.getBody().getItemTransactionsResponse().getItem().getItemID();
                    MarketListing marketListing = marketListingRepository.findOneByItemId(itemId);
                    if ( marketListing != null && marketListing.getStatus() == StatusMarketListing.LISTED){
                        handlerEbayUnSold(ebayNotification.getBody().getItemTransactionsResponse());
                    }

                    break;
                case END_OF_AUCTION:
                    itemId = ebayNotification.getBody().getItemTransactionsResponse().getItem().getItemID();
                    marketListing = marketListingRepository.findOneByItemId(itemId);
                    if ( marketListing != null ){
                        if ( marketListing.getStatus() == StatusMarketListing.LISTED){
                            handlerEbayUnSold(ebayNotification.getBody().getItemTransactionsResponse());
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void handlerEbayUnSold(GetItemTransactionsResponse response) {
        long itemId = response.getItem().getItemID();
        MarketListing marketListing = marketListingRepository.findOneByItemId(itemId);
        if (marketListing == null) {
            return;
        }
        List<Long> marketListingIds = new ArrayList<>();
        marketListingIds.add(marketListing.getId());
        marketListingRepository.updateStatusMarketListing(marketListingIds, StatusMarketListing.DE_LISTED.getValue());
        List<MarketListing> marketListings = new ArrayList<>();
        marketListings.add(marketListing);
        listingWizardManagerAsync.updateWhenInventoryWhenDeListedAsync(new ArrayList<>(), marketListings);
    }

    private void handlerEbayComplete(GetItemTransactionsResponse response) {
        long itemId = response.getItem().getItemID();
        MarketListing marketListing = marketListingRepository.findOneByItemId(itemId);
        if (marketListing == null) {
            return;
        }

        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());
        if (inventory.getStage() != StageInventory.LISTED) {
            marketListing.setStatus(StatusMarketListing.ERROR_SYNC);
            marketListingRepository.save(marketListing);
            return;
        }

        List<MarketListing> marketListings =
                marketListingRepository.findAllByInventoryListedPending(marketListing.getInventoryId());
        if (marketListings.size() == 0) {
            return;
        }
        for (MarketListing listing : marketListings) {
            if (listing.getItemId() != null && listing.getItemId() == itemId) {
                listing.setStatus(StatusMarketListing.SOLD);
                if (response.getTransactionArray() != null && response.getTransactionArray().getTransactions() != null &&
                        response.getTransactionArray().getTransactions().size() > 0) {
                    listing.setTimeSold(response.getTransactionArray().getTransactions().get(0).getCreatedDate());
                } else {
                    listing.setTimeSold(LocalDateTime.now());
                }
            } else {
                listing.setStatus(StatusMarketListing.DE_LISTED);
                listing.setTimeDelisted(LocalDateTime.now());
            }
        }
        marketListingRepository.saveAll(marketListings);

        inventory.setStatus(StatusInventory.SOLD);
        inventory.setStage(StageInventory.SOLD);
        inventoryRepository.save(inventory);

        //cancel offer
        cancelOffer(marketListings.stream().map(MarketListing::getId).collect(Collectors.toList()));
        //end
    }

    public void cancelOffer(List<Long> marketListingIds) {
        offerManager.rejectOtherOffer(null, marketListingIds, null);
    }


    public Object getRecommends() throws ExceptionResponse {
        if (!CommonUtils.isLogined() || !CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            return trackingManager.getMarketListingRecommendation();
        }
        return trackingManager.getInventoryAuctionRecommendation();
    }

    public Object getSecretMarketListing(long marketListingId) {
        List<MarketListingSecretDetailResponse> temps = marketListingSecretDetailResponseRepository.findAllNotDelete(Collections.singletonList(marketListingId));
        if (temps.size() > 0) {
            return temps.get(0);
        }
        return null;
    }

    public Object getSecretMarketListings(List<Long> marketListingIds) {
        if (marketListingIds.size() < 1) {
            return new ArrayList<>();
        }
        List<MarketListingSecretDetailResponse> responses = marketListingSecretDetailResponseRepository.findAllNotDelete(marketListingIds);
        //update comps
        //update sale
        //update count
        return responses;
    }

    public Object getUserMarketListings(UserMarketListingRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        ListObjResponse<UserMarketListingResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(userMarketListingResponseRepository.countBySeller(
                request.getUserId(),
                request.getMarketListingStatuses()
        ));

        if (response.getTotalItem() > 0) {
            response.setData(userMarketListingResponseRepository.findAllBySeller(
                    request.getUserId(), request.getMarketListingStatuses(),
                    request.getSortField(), request.getSortType(),
                    request.getPageable()
            ));
        }

        return response;
    }

    public Object getMarketListingsForMessage(List<Long> marketListingIds) throws ExceptionResponse {
        if (marketListingIds.size() < 1) {
            return new ArrayList<>();
        }
        return marketListingResponseRepository.findAllByIds(marketListingIds);
    }

}

