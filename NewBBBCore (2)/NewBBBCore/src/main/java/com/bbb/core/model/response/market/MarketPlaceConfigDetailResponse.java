package com.bbb.core.model.response.market;

import org.joda.time.LocalDateTime;

import javax.persistence.*;

@Entity
public class MarketPlaceConfigDetailResponse{
    @Id
    private Long id;
    private long marketPlaceId;
    private LocalDateTime createdTime;
    private LocalDateTime lastUpdate;
    private boolean isDelete;
    private String name;
    private String ebayAppId;
    private String ebayCertId;
    private String ebayDevId;
    private String ebayToken;
    private Boolean isEbayApiSandbox;
    private String marketPlaceName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEbayAppId() {
        return ebayAppId;
    }

    public void setEbayAppId(String ebayAppId) {
        this.ebayAppId = ebayAppId;
    }

    public String getEbayCertId() {
        return ebayCertId;
    }

    public void setEbayCertId(String ebayCertId) {
        this.ebayCertId = ebayCertId;
    }

    public String getEbayDevId() {
        return ebayDevId;
    }

    public void setEbayDevId(String ebayDevId) {
        this.ebayDevId = ebayDevId;
    }

    public String getEbayToken() {
        return ebayToken;
    }

    public void setEbayToken(String ebayToken) {
        this.ebayToken = ebayToken;
    }

    public Boolean getEbayApiSandbox() {
        return isEbayApiSandbox;
    }

    public void setEbayApiSandbox(Boolean ebayApiSandbox) {
        isEbayApiSandbox = ebayApiSandbox;
    }

    public String getMarketPlaceName() {
        return marketPlaceName;
    }

    public void setMarketPlaceName(String marketPlaceName) {
        this.marketPlaceName = marketPlaceName;
    }
}
