package com.bbb.core.model.response.component;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BicycleComponentTypeDetail {
    @Id
    private long id;
    private long bicycleId;
    private long componentTypeId;
    private String componentName;
    private String componentValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getComponentTypeId() {
        return componentTypeId;
    }

    public void setComponentTypeId(long componentTypeId) {
        this.componentTypeId = componentTypeId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentValue() {
        return componentValue;
    }

    public void setComponentValue(String componentValue) {
        this.componentValue = componentValue;
    }
}
