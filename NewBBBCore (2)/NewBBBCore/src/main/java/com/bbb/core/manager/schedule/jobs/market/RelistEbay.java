package com.bbb.core.manager.schedule.jobs.market;

import com.bbb.core.common.Constants;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.manager.market.async.ListingWizardManagerAsync;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.FIX_RATE, defaultTimeSeconds = 5 * 60)
public class RelistEbay extends ScheduleJob {
    public static final long MAX_TIME_LISTING_EBAY = 15 * 60 * 1000L;
    public static final long MAX_COUNT_LISTING_EBAY = 5L;

    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private ListingWizardManagerAsync listingWizardManagerAsync;
    @Autowired
    private AppConfig appConfig;


    @Override
    public void run() {
        MarketToList marketToList = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToList == null) {
            return;
        }
        if (EbayValue.MAX_ITEMS_RELIST_EBAY > 0) {
            SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
            String now = format.format(LocalDateTime.now().toDate());
            List<MarketListing> marketListings = marketListingRepository.findAllToReListEbay(
                    marketToList.getMarketConfigId(),
                    StatusMarketListing.LISTING_ERROR.getValue(),
//                StatusMarketListing.LISTING.getValue(),
                    now,
                    MAX_TIME_LISTING_EBAY,
                    MAX_COUNT_LISTING_EBAY,
                    0,
                    EbayValue.MAX_ITEMS_RELIST_EBAY
            );
            if (marketListings.size() > 0) {
                for (MarketListing marketListing : marketListings) {
                    if (marketListing.getStatus() == StatusMarketListing.LISTING_ERROR) {
                        marketListing.setStatus(StatusMarketListing.LISTING);
                    } else {
                        marketListing.setCountListingError(marketListing.getCountListingError() + 1);
                    }
                }
                marketListingRepository.saveAll(marketListings);
                marketListingManagerAsync.listEbay(marketListings, null);
            }
            //delete market than max error listing
            deleteMarketListingEbayThanMax(marketToList);
        }
        detectExpireTimeListedEbay(marketToList);
    }

    public void onFail(Throwable throwable) {}

    private void detectExpireTimeListedEbay(MarketToList marketToList) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.FORMAT_DATE_TIME);
        String now = format.format(LocalDateTime.now().toDate());
        List<MarketListing> marketListingsExpire = marketListingRepository.findAllMarketListingEbayExpire(now, marketToList.getMarketPlaceId());
        if (marketListingsExpire.size() > 0) {
            marketListingRepository.updateStatusMarketListing(marketListingsExpire.stream().map(MarketListing::getId).collect(Collectors.toList()), StatusMarketListing.DE_LISTED.getValue());
            listingWizardManagerAsync.updateWhenInventoryWhenDeListedAsync(new ArrayList<>(), marketListingsExpire);
        }
    }

    private void deleteMarketListingEbayThanMax(MarketToList marketToList) {
        marketListingRepository.updateDeleteMarketListingEbayMaxError(
                marketToList.getMarketPlaceId(),
                MAX_COUNT_LISTING_EBAY
        );
    }
}
