package com.bbb.core.manager.common;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.ConverterSqlUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.partner.PartnerDetail;
import com.bbb.core.model.request.common.CommonComponentType;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.NumberString;
import com.bbb.core.model.response.common.*;
import com.bbb.core.model.response.ebay.additem.CategoryEbayResponse;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.model.response.tradein.ConditionDetailResponse;
import com.bbb.core.repository.BicycleTypeRepository;
import com.bbb.core.repository.bicycle.BicycleSizeCategoryRepository;
import com.bbb.core.repository.common.NumberStringRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeRepository;
import com.bbb.core.repository.inventory.InventoryCompTypeSelectRepository;
import com.bbb.core.repository.inventory.InventoryTypeRepository;
import com.bbb.core.repository.market.EbayCategoryRepository;
import com.bbb.core.repository.market.EbayListingDurationRepository;
import com.bbb.core.repository.market.MarketListingFilterRepository;
import com.bbb.core.repository.market.out.MarketConfigCommonResRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.tradein.TradeInStatusRepository;
import com.bbb.core.repository.tradein.TradeInUpgradeCompRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CommonManager {
    @Autowired
    private BicycleTypeRepository bicycleTypeRepository;
    @Autowired
    private TradeInUpgradeCompRepository tradeInUpgradeCompRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InventoryCompTypeSelectRepository inventoryCompTypeSelectRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private TradeInStatusRepository tradeInStatusRepository;
    @Autowired
    private MarketConfigCommonResRepository marketConfigCommonResRepository;
    @Autowired
    private EbayCategoryRepository ebayCategoryRepository;
    @Autowired
    private MarketListingFilterRepository marketListingFilterRepository;
    @Autowired
    private NumberStringRepository numberStringRepository;
//    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private EbayListingDurationRepository ebayListingDurationRepository;
    @Autowired
    private BicycleSizeCategoryRepository bicycleSizeCategoryRepository;

    @Autowired
    CommonManager(EntityManagerFactory entityManagerFactory) {
        sessionFactory = ConfigDataSource.getSessionFactory(entityManagerFactory);
    }


    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public Object getCommonComponents(List<CommonComponentType> components) {
        Map<String, Object> response = new HashMap<>();
        if (components.contains(CommonComponentType.CONDITION)) {
            List<ConditionDetailResponse> conditions = new ArrayList<>();
            conditions.add(new ConditionDetailResponse(ConditionInventory.EXCELLENT, Constants.EXCELLENT_PERCENT, Constants.EXCELLENT_MSG, null));
            conditions.add(new ConditionDetailResponse(ConditionInventory.VERY_GOOD, Constants.VERY_GOOD_PERCENT, Constants.VERY_GOOD_MSG, null));
            conditions.add(new ConditionDetailResponse(ConditionInventory.GOOD, Constants.GOOD_PERCENT, Constants.GOOD_MSG, null));
            conditions.add(new ConditionDetailResponse(ConditionInventory.FAIR, Constants.FAIR_PERCENT, Constants.FAIR_MSG, null));
            response.put("condition", conditions);
        }
        if (components.contains(CommonComponentType.BICYCLE_TYPE)) {
//            List<ContentCommonId> contents = bicycleTypeRepository.findAll().stream().map(type -> {
//                ContentCommonId id = new ContentCommonId();
//                id.setId(type.getId());
//                id.setName(type.getName());
//                return id;
//            }).collect(Collectors.toList());
//            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("type", bicycleTypeRepository.findAll());
        }
        if (components.contains(CommonComponentType.UPGRADE_COMPONENT)) {
            response.put("upgradeComponents", tradeInUpgradeCompRepository.findAll());
        }

        if (components.contains(CommonComponentType.COMPONENT_TYPE_CUSTOM_QUOTE)) {
            response.put("componentCustomQuote", getComps());
        }

        if (components.contains(CommonComponentType.BRAND_BICYCLE)) {
            List<ContentCommonId> contents = contentCommonRepository.findAllBrandBicycle()
                    .stream().map(ContentCommon::getId)
                    .collect(Collectors.toList());
            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("brandBicycle", contents);

        }
        if (components.contains(CommonComponentType.STATUS_TRADE_IN)) {
            response.put("statusTradeIns", tradeInStatusRepository.findAll());
        }
        if (components.contains(CommonComponentType.CONFIG_MARKET)) {
            response.put("marketConfigs", marketConfigCommonResRepository.findAll());
        }

        if (components.contains(CommonComponentType.EBAY_CATEGORY_PRIMARY)) {

            List<ContentCommon> contentCommons = contentCommonRepository.findAllEbayCategoriesPrimary();
            List<CategoryEbayResponse> categoryEbayResponses = contentCommons.stream().map(ca -> {
                CategoryEbayResponse categoryEbayResponse = new CategoryEbayResponse();
                categoryEbayResponse.setPrimaryId(ca.getId().getId());
                categoryEbayResponse.setPrimaryName(ca.getId().getName());
                return categoryEbayResponse;
            }).collect(Collectors.toList());

            List<Long> primaryIds = contentCommons.stream().map(content -> content.getId().getId()).collect(Collectors.toList());
            if (primaryIds.size() > 0) {
                Map<Long, List<EbayCategory>> mapCategory = ebayCategoryRepository.findAllChild(primaryIds)
                        .stream().collect(Collectors.groupingBy(EbayCategory::getParentId));
                mapCategory.forEach((primaryId, childs) -> {
                    CategoryEbayResponse categoryEbayResponse =
                            CommonUtils.findObject(categoryEbayResponses, o -> o.getPrimaryId() == primaryId);
                    if (categoryEbayResponse != null) {
                        categoryEbayResponse.setChilds(
                                childs.stream().map(child -> {
                                    ContentCommonId id = new ContentCommonId();
                                    id.setId(child.getId());
                                    id.setName(child.getName());
                                    return id;
                                }).collect(Collectors.toList())
                        );
                    }
                });

                categoryEbayResponses.stream().peek(o -> {
                    if (o.getChilds() == null) {
                        o.setChilds(new ArrayList<>());
                    }
                }).collect(Collectors.toList());
            }

            response.put("ebayCategoriesPrimary", categoryEbayResponses);
        }
        if (components.contains(CommonComponentType.ALL_BRAND_BICYCLE)) {
            List<ContentCommonId> contents = contentCommonRepository.findAllBicycleBrand().stream().map(ContentCommon::getId).collect(Collectors.toList());
            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("allBrandBicycle", contents);
        }
        if (components.contains(CommonComponentType.ALL_MODEL_BICYCLE)) {
            List<ContentCommonId> contents = contentCommonRepository.findAllBicycleModel().stream().map(ContentCommon::getId).collect(Collectors.toList());
            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("allModelBicycle", contents);
        }

        if (components.contains(CommonComponentType.ALL_SIZE_INV)) {
//            response.put("sizeInv", contentCommonRepository.findAllSizeBicycle().stream().map(ContentCommon::getId).collect(Collectors.toList()));
            response.put("sizeInv", bicycleSizeCategoryRepository.findAllCategories().stream()
                    .map(cat -> {
                        ContentCommonId content = new ContentCommonId();
                        content.setId(cat.getId());
                        content.setName(cat.getDisplayName());

                        return content;
                    }).collect(Collectors.toList()));
        }
        if (components.contains(CommonComponentType.ALL_FRAME_MATERIAL_INV)) {
            response.put("frameMaterial", contentCommonRepository.findAllInvComp(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME).stream().map(ContentCommon::getId).collect(Collectors.toList()));
        }
        if (components.contains(CommonComponentType.ALL_BRAKE_TYPE_INV)) {
            List<ContentCommonId> contents = contentCommonRepository.findAllInvComp(ValueCommons.INVENTORY_BRAKE_TYPE_NAME).stream().map(ContentCommon::getId).collect(Collectors.toList());
//            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("brakeType", contents);
        }

        if (components.contains(CommonComponentType.ALL_YEAR_BICYCLE)) {
            List<ContentCommonId> contents = contentCommonRepository.findAllBicycleYear().stream().map(ContentCommon::getId).collect(Collectors.toList());
            contents.sort(Comparator.comparing(ContentCommonId::getName));
            response.put("year", contents);
        }

        if (components.contains(CommonComponentType.FILTER_INVENTORY_ACTIVE)) {
            response.put("categoriesActive", getListFilterActive());
        }
        if (components.contains(CommonComponentType.FILTER_INVENTORY_INACTIVE)) {
            response.put("categoriesInActive", getListFilterInActive());
        }
        if ( components.contains(CommonComponentType.EBAY_LISTING_TYPE)){
            List<ListingTypeEbay> ebayTypes = new ArrayList<>();
            ebayTypes.add(ListingTypeEbay.FIXED_PRICE_ITEM);
//            ebayTypes.add(ListingTypeEbay.AUCTION);
            response.put("ebayTypes", ebayTypes);

        }

        if (components.contains(CommonComponentType.LISTING_EBAY_DURATION)) {
//            response.put("listingEbayDuration", TypeListingDuration.values());
            response.put("listingEbayDuration", ebayListingDurationRepository.findAll());
        }
        if (components.contains(CommonComponentType.DETAIL_BICYCLE_MY_LISTING)) {
            BicycleDetailMyListingResponse res = new BicycleDetailMyListingResponse();
            res.setComps(getCompBicycleDetailMyListing());
            res.setTypes(getTypeBicycle());
            response.put("bicycleDetailComp",res);
        }

        if (components.contains(CommonComponentType.ALL_SUSPENSION)) {
            response.put("suspension", contentCommonRepository.findAllInvComp(ValueCommons.SUSPENSION_NAME_INV_COMP));
        }

        if (components.contains(CommonComponentType.ALL_GENDER)) {
            response.put("gender", contentCommonRepository.findAllInvComp(ValueCommons.GENDER_NAME_INV_COMP));
        }

        if (components.contains(CommonComponentType.ALL_WHEEL_SIZE)) {
            response.put("wheelSize", contentCommonRepository.findAllInvComp(ValueCommons.INVENTORY_WHEEL_SIZE_NAME));
        }

        return response;
    }

    private List<MarketListingFilterResponse> getListFilterInActive() {
        List<MarketListingFilter> marketListingFilters = marketListingFilterRepository.findAllInActive();
        return getFilterMarketListing(marketListingFilters);
    }

    private List<MarketListingFilterResponse> getListFilterActive() {
        List<MarketListingFilter> marketListingFilters = marketListingFilterRepository.findAllActive();
        return getFilterMarketListing(marketListingFilters);
    }

    private List<MarketListingFilterResponse> getFilterMarketListing(List<MarketListingFilter> marketListingFilters) {
        List<MarketListingFilter> comps = marketListingFilters.stream()
                .filter(filter -> filter.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_COMP_DETAIL)
                .collect(Collectors.toList());
        List<MarketListingFilter> fields = marketListingFilters.stream()
                .filter(filter -> filter.getTypeTable() == MarketListingFilterTypeTable.INVENTORY).collect(Collectors.toList());
        MarketListingFilter filterInventoryType = CommonUtils.findObject(marketListingFilters, field -> field.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_TYPE);
        MarketListingFilter filterInventoryError = CommonUtils.findObject(marketListingFilters, field -> field.getTypeTable() == MarketListingFilterTypeTable.INVENTORY_ERROR);
        return Observable.zip(
                Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                    List<MarketListingFilterResponse> resOne = getFilterInvComps(comps);
                    t.onNext(resOne);
                    t.onComplete();
                }).subscribeOn(Schedulers.newThread()),
                Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                    List<MarketListingFilterResponse> resTwo = getFilterField(fields);


                    t.onNext(resTwo);
                    t.onComplete();
                }).subscribeOn(Schedulers.newThread()),
                Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                    List<MarketListingFilterResponse> resThree = new ArrayList<>();
                    if (filterInventoryType != null) {
                        resThree.add(getFilterFieldInventoryType(filterInventoryType));
                    }
                    t.onNext(resThree);
                    t.onComplete();
                }).subscribeOn(Schedulers.newThread()),
                Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                    List<MarketListingFilterResponse> resFour = new ArrayList<>();
                    if (filterInventoryError != null) {
                        resFour.add(getFilterFieldInventoryError(filterInventoryError));
                    }
                    t.onNext(resFour);
                    t.onComplete();
                }).subscribeOn(Schedulers.newThread()),
                (resOne, resTwo, resThree, resFour) -> {
                    resOne.addAll(resTwo);
                    resOne.addAll(resThree);
                    resOne.addAll(resFour);
                    return resOne;
                }
        ).blockingFirst();
    }

    private String getQueryField(MarketListingFilter filter) {
        String name = filter.getFieldName();
        name = name.substring(name.lastIndexOf(".") + 1);
        return "SELECT * FROM (SELECT ABS(CHECKSUM(NewId())) AS id,  " + filter.getFieldName() + " AS name, " + ConverterSqlUtils.convertStringToStringQuery(filter.getName()) + " AS type FROM inventory " +
                "WHERE inventory.is_delete = 0 AND " +
                "inventory.status = " + ConverterSqlUtils.convertStringToStringQuery(StatusInventory.ACTIVE.getValue()) + " AND " +
                "(inventory.stage = " + ConverterSqlUtils.convertStringToStringQuery(StageInventory.READY_LISTED.getValue()) + " OR inventory.stage = " + ConverterSqlUtils.convertStringToStringQuery(StageInventory.LISTED.getValue()) + ") " +
                "GROUP BY " + filter.getFieldName() + " ) AS " + name + " ";
    }

    private List<MarketListingFilterResponse> getFilterField(List<MarketListingFilter> fields) {
        if (fields.size() == 0) {
            return new ArrayList<>();
        }

        List<MarketListingFilter> filterRangeIns =
                fields.stream().filter(filter -> filter.getType() == MarketListingFilterType.RANGE_IN)
                        .collect(Collectors.toList());
        List<MarketListingFilterResponse> responses = new ArrayList<>();
        Session session = null;
        if (filterRangeIns.size() > 0) {
            String query = getQueryField(filterRangeIns.get(0));
            for (int i = 1; i < filterRangeIns.size(); i++) {
                query = query + " UNION " + getQueryField(filterRangeIns.get(i));
            }
            session = sessionFactory.openSession();
            List<MarketListingFilterNotComp> resultQueryField =
                    session.createSQLQuery(query).addEntity(MarketListingFilterNotComp.class).list();

            Map<String, List<MarketListingFilterNotComp>> mapResult =
                    resultQueryField.stream().collect(Collectors.groupingBy(MarketListingFilterNotComp::getType));
            mapResult.forEach((type, resultMap) -> {
                MarketListingFilterResponse response = new MarketListingFilterResponse();
                List<OperatorMarketListingFilter> operators = new ArrayList<>();
                operators.add(OperatorMarketListingFilter.NOT_EQUAL);
                operators.add(OperatorMarketListingFilter.EQUAL);
                operators.add(OperatorMarketListingFilter.RANGE_IN);
                operators.add(OperatorMarketListingFilter.NOT_RANGE_IN);
                response.setOperators(operators);
                response.setTypeTable(MarketListingFilterTypeTable.INVENTORY);
                response.setValues(
                        resultMap.stream().map(MarketListingFilterNotComp::getName).filter(Objects::nonNull).sorted((String::compareTo)).collect(Collectors.toList())
                );
                MarketListingFilter filter = CommonUtils.findObject(fields, fi -> fi.getName().equals(type));
                if (filter != null) {
                    response.setId(filter.getId());
                }
                response.setName(type);
                if ( response.getName().equals(ValueCommons.PARTNER) && response.getValues() != null && response.getValues().size() > 0 ){
                    ListObjResponse<PartnerDetail> partners = authManager.getPartnerDetail(response.getValues());
                    if ( partners.getData() != null && partners.getData().size() > 0 ) {
                        response.setValueNames(partners.getData().stream().map(parter->{
                            ContentDoubleString content = new ContentDoubleString();
                            content.setId(parter.getId());
                            content.setName(parter.getName());
                            return content;
                        }).collect(Collectors.toList()));
                    }

                }
                responses.add(response);
            });
        }

        List<MarketListingFilter> filterRangeOrDate =
                fields.stream().filter(filter -> filter.getType() == MarketListingFilterType.RANGE || filter.getType() == MarketListingFilterType.RANGE_DATE_TIME)
                        .collect(Collectors.toList());
        for (MarketListingFilter filter : filterRangeOrDate) {
            MarketListingFilterResponse response = new MarketListingFilterResponse();
            List<OperatorMarketListingFilter> operators = new ArrayList<>();
            if (filter.getType() == MarketListingFilterType.RANGE) {
                operators.add(OperatorMarketListingFilter.RANGE);
            } else {
                operators.add(OperatorMarketListingFilter.RANGE_DATE);
            }
            response.setOperators(operators);
            response.setTypeTable(MarketListingFilterTypeTable.INVENTORY);
            MarketListingFilter filterNew = CommonUtils.findObject(fields, fi -> fi.getName().equals(filter.getName()));
            if (filterNew != null) {
                response.setId(filterNew.getId());
            }
            response.setName(filter.getName());
            responses.add(response);
        }
        if (session != null) {
            session.close();
        }
        return responses;

    }

    private MarketListingFilterResponse getFilterFieldInventoryType(MarketListingFilter field) {
        List<InventoryType> inventoryTypes = inventoryTypeRepository.findAll();
        MarketListingFilterResponse res = new MarketListingFilterResponse();
        res.setId(field.getId());
        res.setName(field.getName());
        res.setTypeTable(MarketListingFilterTypeTable.INVENTORY_TYPE);
        res.setOperators(new ArrayList<>());
        res.getOperators().add(OperatorMarketListingFilter.RANGE_IN);
        res.getOperators().add(OperatorMarketListingFilter.EQUAL);
        res.getOperators().add(OperatorMarketListingFilter.NOT_EQUAL);
        res.getOperators().add(OperatorMarketListingFilter.NOT_RANGE_IN);
        res.setValues(
                inventoryTypes.stream().filter(invType -> !invType.getName().equals(ValueCommons.PTP))
                        .map(InventoryType::getName)
                        .collect(Collectors.toList())
        );
        return res;
    }

    private MarketListingFilterResponse getFilterFieldInventoryError(MarketListingFilter filter) {
        MarketListingFilterResponse res = new MarketListingFilterResponse();
        res.setId(filter.getId());
        res.setName(filter.getName());
        res.setOperators(new ArrayList<>());
        res.setTypeTable(MarketListingFilterTypeTable.INVENTORY_ERROR);
        res.setOperators(new ArrayList<>());
        res.getOperators().add(OperatorMarketListingFilter.EQUAL);
        res.setValues(new ArrayList<>());
        res.getValues().add(ValueCommons.SHOW_ITEM_WITHOUT_ERRORS);
        res.getValues().add(ValueCommons.SHOW_ITEM_IN_ERRORS);
        return res;
    }

    private List<MarketListingFilterResponse> getFilterInvComps(List<MarketListingFilter> comps) {
        if (comps.size() == 0) {
            return new ArrayList<>();
        }
        List<String> nameComps = comps.stream().map(MarketListingFilter::getName).collect(Collectors.toList());
        CommonUtils.stream(nameComps);

        List<InventoryCompType> invCompTypes = inventoryCompTypeRepository.findAllByName(nameComps);
        List<InventoryCompType> invCompTypesSelect =
                invCompTypes.stream().filter(InventoryCompType::isSelect)
                        .collect(Collectors.toList());
        List<InventoryCompType> invCompTypesNotSelect =
                invCompTypes.stream().filter(comp -> !comp.isSelect())
                        .collect(Collectors.toList());
        //response return
        return
                Observable.zip(
                        //comp select
                        Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                            List<MarketListingFilterResponse> resOnes = new ArrayList<>();
                            if (invCompTypesSelect.size() > 0) {
                                //get component select

                                List<Long> selectTypeIds = invCompTypesSelect.stream().map(InventoryCompType::getId).collect(Collectors.toList());
                                List<InventoryCompTypeSelect> inventoryCompTypeSelects = inventoryCompTypeSelectRepository.findAllByInventoryCompTypeIds(selectTypeIds);
                                Map<Long, List<InventoryCompTypeSelect>> mapCompSelect = inventoryCompTypeSelects.stream().collect(Collectors.groupingBy(InventoryCompTypeSelect::getInventoryCompTypeId));
                                mapCompSelect.forEach((compId, compSelects) -> {
                                    InventoryCompType compType = CommonUtils.findObject(invCompTypesSelect, select -> select.getId() == compId);

                                    String name;
                                    MarketListingFilterResponse response = new MarketListingFilterResponse();
                                    if (compType != null) {
                                        name = compType.getName();
                                        MarketListingFilter filter = CommonUtils.findObject(comps, comp -> comp.getName().equals(compType.getName()));
                                        response.setId(filter.getId());
                                    } else {
                                        name = null;
                                    }

//                                    response.setId(compId);
                                    response.setTypeTable(MarketListingFilterTypeTable.INVENTORY_COMP_DETAIL);
                                    response.setName(name);
                                    response.setValues(
                                            compSelects.stream().map(InventoryCompTypeSelect::getValue).collect(Collectors.toList())
                                    );
                                    List<OperatorMarketListingFilter> operators = new ArrayList<>();
                                    operators.add(OperatorMarketListingFilter.NOT_EQUAL);
                                    operators.add(OperatorMarketListingFilter.EQUAL);
                                    operators.add(OperatorMarketListingFilter.RANGE_IN);
                                    operators.add(OperatorMarketListingFilter.NOT_RANGE_IN);
                                    response.setOperators(operators);
                                    resOnes.add(response);
                                });
                            }
                            t.onNext(resOnes);
                            t.onComplete();
                        }).subscribeOn(Schedulers.newThread()),
                        //comp non select
                        Observable.create((ObservableOnSubscribe<List<MarketListingFilterResponse>>) t -> {
                            List<MarketListingFilterResponse> resOnes = new ArrayList<>();
                            if (invCompTypesNotSelect.size() > 0) {
                                List<Long> invCompTypesNotSelectIds = invCompTypesNotSelect.stream().map(InventoryCompType::getId).collect(Collectors.toList());
                                List<NumberString> numberStrings = numberStringRepository.findAllFromInventoryCompTypeIdsNonSelect(invCompTypesNotSelectIds);
                                Map<Long, List<NumberString>> mapNotSelect = numberStrings.stream().collect(Collectors.groupingBy(nu -> nu.getId().getId()));
                                mapNotSelect.forEach((compId, numbers) -> {
                                    MarketListingFilterResponse response = new MarketListingFilterResponse();
                                    response.setId(compId);
                                    response.setTypeTable(MarketListingFilterTypeTable.INVENTORY_COMP_DETAIL);
                                    InventoryCompType compType = CommonUtils.findObject(invCompTypesNotSelect, select -> select.getId() == compId);
                                    String name;
                                    if (compType != null) {
                                        name = compType.getName();
                                    } else {
                                        name = null;
                                    }
                                    response.setName(name);
                                    response.setValues(
                                            numbers.stream().map(nu -> nu.getId().getValue()).collect(Collectors.toList())
                                    );
                                    List<OperatorMarketListingFilter> operators = new ArrayList<>();
                                    operators.add(OperatorMarketListingFilter.NOT_EQUAL);
                                    operators.add(OperatorMarketListingFilter.EQUAL);
                                    operators.add(OperatorMarketListingFilter.RANGE_IN);
                                    operators.add(OperatorMarketListingFilter.NOT_RANGE_IN);
                                    response.setOperators(operators);
                                    resOnes.add(response);
                                });
                            }
                            t.onNext(resOnes);
                            t.onComplete();

                        }).subscribeOn(Schedulers.newThread()),
                        (resultOne, resultTwo) -> {
                            resultOne.addAll(resultTwo);
                            return resultOne;
                        }
                ).blockingFirst();


    }

    private List<InventoryCompResponse> getCompBicycleDetailMyListing() {
        List<String> componentNameCustomQuotes = new ArrayList<>();
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME);
        componentNameCustomQuotes.add(ValueCommons.GENDER_NAME_INV_COMP);
        componentNameCustomQuotes.add(ValueCommons.BRAKE_TYPE_NAME_INV_COMP);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRAME_SIZE_NAME);
        componentNameCustomQuotes.add(ValueCommons.SUSPENSION_NAME_INV_COMP);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_WHEEL_SIZE_NAME);

        List<InventoryCompType> inventoryComponents = inventoryCompTypeRepository.findAllByName(componentNameCustomQuotes);
        List<InventoryCompResponse> inventoryCompResponses = inventoryComponents.stream().map(type -> {
                    InventoryCompResponse inventoryCompResponse = new InventoryCompResponse();
                    if (type.getName() != null) {
                        switch (type.getName()) {
                            case ValueCommons.INVENTORY_FRAME_MATERIAL_NAME:
                                inventoryCompResponse.setSort(1);
                                break;
                            case ValueCommons.GENDER_NAME_INV_COMP:
                                inventoryCompResponse.setSort(2);
                                break;
                            case ValueCommons.BRAKE_TYPE_NAME_INV_COMP:
                                inventoryCompResponse.setSort(3);
                                break;
                            case ValueCommons.INVENTORY_FRAME_SIZE_NAME:
                                inventoryCompResponse.setSort(4);
                                break;
                            case ValueCommons.SUSPENSION_NAME_INV_COMP:
                                inventoryCompResponse.setSort(5);
                                break;
                            default:
                                inventoryCompResponse.setSort(6);
                                break;
                        }
                    }
                    inventoryCompResponse.clone(type);
                    return inventoryCompResponse;
                }
        ).collect(Collectors.toList());

        inventoryCompResponses.sort(Comparator.comparingLong(InventoryCompResponse::getSort));
        List<Long> compIdsSelect = inventoryComponents.stream().filter(InventoryCompType::isSelect).map(InventoryCompType::getId).collect(Collectors.toList());
        if (compIdsSelect.size() > 0) {
            List<InventoryCompTypeSelect> inventoryCompTypeSelects = inventoryCompTypeSelectRepository.findAllByInventoryCompTypeIds(compIdsSelect);
            Map<Long, List<InventoryCompTypeSelect>> mapComps = inventoryCompTypeSelects.stream()
                    .sorted((type1, type2) -> type1.getOrderSort() - type2.getOrderSort())
                    .collect(Collectors.groupingBy(InventoryCompTypeSelect::getInventoryCompTypeId));
            mapComps.forEach((id, typeSelects) -> {
                InventoryCompResponse res = CommonUtils.findObject(inventoryCompResponses, o -> o.getId() == id);
                if (res != null) {
                    res.setSelects(typeSelects);
                }
            });
        }
        return inventoryCompResponses;
    }

    private List<InventoryCompTypeSelect> getTypeBicycle(){
        List<BicycleType> bicycleTypes = bicycleTypeRepository.findAll();
        List<InventoryCompTypeSelect> selects = bicycleTypes.stream().map(o->{
            InventoryCompTypeSelect select = new InventoryCompTypeSelect();
            select.setId(o.getId());
            select.setValue(o.getName());
            return select;
        }).collect(Collectors.toList());
        return selects;
    }

    public List<InventoryCompResponse> getComps() {
        List<String> componentNameCustomQuotes = new ArrayList<>();
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRAME_MATERIAL_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_SHIFTERS_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRONT_DERAILLEUR_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_REAR_DERAILLEUR_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRONT_BRAKE_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_REAR_BRAKE_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_CRANKSET_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRONT_SOCK_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_REAR_SOCK_NAME);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_WHEELS_NAME);
        componentNameCustomQuotes.add(ValueCommons.INV_COMP_HANDLEBAR_NAME);
        componentNameCustomQuotes.add(ValueCommons.INV_COMP_STEM_NAME);
        componentNameCustomQuotes.add(ValueCommons.INV_COMP_CASSETTE_NAME);
        componentNameCustomQuotes.add(ValueCommons.BRAKE_TYPE_NAME_INV_COMP);
        componentNameCustomQuotes.add(ValueCommons.INVENTORY_FRAME_SIZE_NAME);
        componentNameCustomQuotes.add(ValueCommons.SUSPENSION_NAME_INV_COMP);

        List<InventoryCompType> inventoryComponents = inventoryCompTypeRepository.findAllByName(componentNameCustomQuotes);
        List<InventoryCompResponse> inventoryCompResponses = inventoryComponents.stream().map(type -> {
            InventoryCompResponse inventoryCompResponse = new InventoryCompResponse();
            if (type.getName() != null) {
                switch (type.getName()) {
                    case ValueCommons.INVENTORY_FRAME_MATERIAL_NAME:
                        inventoryCompResponse.setSort(1);
                        break;
                    case ValueCommons.INVENTORY_SHIFTERS_NAME:
                        inventoryCompResponse.setSort(2);
                        break;
                    case ValueCommons.INVENTORY_FRONT_DERAILLEUR_NAME:
                        inventoryCompResponse.setSort(3);
                        break;
                    case ValueCommons.INVENTORY_REAR_DERAILLEUR_NAME:
                        inventoryCompResponse.setSort(4);
                        break;
                    case ValueCommons.INVENTORY_FRONT_BRAKE_NAME:
                        inventoryCompResponse.setSort(5);
                        break;
                    case ValueCommons.INVENTORY_REAR_BRAKE_NAME:
                        inventoryCompResponse.setSort(6);
                        break;
                    case ValueCommons.INVENTORY_CRANKSET_NAME:
                        inventoryCompResponse.setSort(7);
                        break;
                    case ValueCommons.INVENTORY_FRONT_SOCK_NAME:
                        inventoryCompResponse.setSort(8);
                        break;
                    case ValueCommons.INVENTORY_REAR_SOCK_NAME:
                        inventoryCompResponse.setSort(9);
                        break;
                    case ValueCommons.INVENTORY_WHEELS_NAME:
                        inventoryCompResponse.setSort(10);
                        break;
                    default:
                        inventoryCompResponse.setSort(11);
                        break;
                }
            }

            inventoryCompResponse.clone(type);
            return inventoryCompResponse;
        }).collect(Collectors.toList());
        inventoryCompResponses.sort(Comparator.comparingLong(InventoryCompResponse::getSort));
        List<Long> compIdsSelect = inventoryComponents.stream().filter(InventoryCompType::isSelect).map(InventoryCompType::getId).collect(Collectors.toList());
        if (compIdsSelect.size() > 0) {
            List<InventoryCompTypeSelect> inventoryCompTypeSelects = inventoryCompTypeSelectRepository.findAllByInventoryCompTypeIds(compIdsSelect);
            Map<Long, List<InventoryCompTypeSelect>> mapComps = inventoryCompTypeSelects.stream().collect(Collectors.groupingBy(InventoryCompTypeSelect::getInventoryCompTypeId));
            mapComps.forEach((id, typeSelects) -> {
                InventoryCompResponse res = CommonUtils.findObject(inventoryCompResponses, o -> o.getId() == id);
                if (res != null) {
                    res.setSelects(typeSelects);
                }
            });
        }
        return inventoryCompResponses;
    }
}
