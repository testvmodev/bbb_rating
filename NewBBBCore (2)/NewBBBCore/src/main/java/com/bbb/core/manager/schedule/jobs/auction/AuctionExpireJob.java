package com.bbb.core.manager.schedule.jobs.auction;

import com.bbb.core.common.Constants;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.schedule.annotation.Job;
import com.bbb.core.manager.schedule.jobs.ScheduleJob;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.JobTimeType;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusOffer;
import com.bbb.core.model.otherservice.response.ErrorRestResponse;
import com.bbb.core.model.otherservice.response.cart.CartResponse;
import com.bbb.core.model.otherservice.response.mail.SendingMailResponse;
import com.bbb.core.model.schedule.OfferInventoryAuction;
import com.bbb.core.model.schedule.ScheduleAddCartResponse;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.market.InventoryLocationShippingRepository;
import com.bbb.core.repository.schedule.OfferInventoryAuctionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Job(preEnable = true, defaultTimeType = JobTimeType.FIX_DELAY, defaultTimeSeconds = 300)
public class AuctionExpireJob extends ScheduleJob {
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private OfferInventoryAuctionRepository offerInventoryAuctionRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;


    @Override
    public void run() {
        runCheckExpireAuction(null);
    }

    public synchronized void runCheckExpireAuction(Long auctionId){
        String currentDate = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate());
        List<Auction> auctions = auctionRepository.getAuctionExpireNoYetUpdate(currentDate, auctionId);
        if (auctions.size() > 0) {
            for (Auction auction : auctions) {
                auction.setExpired(true);
            }
            List<Long> auctionIds = auctions.stream().map(Auction::getId).collect(Collectors.toList());
            List<InventoryAuction> inventoryAuctions = inventoryAuctionRepository.getActiveInventoryAuctions(auctionIds)
                    .stream()
                    .peek(au -> au.setInactive(true)).collect(Collectors.toList());
            List<Long> inventoryAuctionIds = inventoryAuctions.stream().map(InventoryAuction::getId).collect(Collectors.toList());
            List<OfferInventoryAuction> pendingOffers = new ArrayList();

            if (inventoryAuctionIds.size() > 0) {
                pendingOffers = offerInventoryAuctionRepository.findAllOfferInventoryAuctionMax(inventoryAuctionIds);
            }

            if (pendingOffers.size() > 0) {
//                if (offerIdsSuccess.size() > 0) {
                List<Offer> offersAccept =
                        offerRepository.findAllByIds(pendingOffers.stream().map(OfferInventoryAuction::getOfferId).collect(Collectors.toList()))
                                .stream()
                                .peek(offer -> {
                                    offer.setStatus(StatusOffer.ACCEPTED);
                                    offer.setLastUpdate(null);
                                })
                                .collect(Collectors.toList());
                updateStageInventoryOfferAccept(offersAccept, inventoryAuctions);
                for (InventoryAuction inventoryAuction : inventoryAuctions) {
                    for (Offer offer : offersAccept) {
                        if (inventoryAuction.getId() == offer.getInventoryAuctionId()) {
                            inventoryAuction.setWinnerId(offer.getBuyerId());
                            break;
                        }
                    }
                }
                List<Offer> offersReject =
                        offerRepository.findAllByNotIdsAndPendingOrCounted(offersAccept.stream().map(Offer::getId).collect(Collectors.toList()),
                                inventoryAuctionIds)
                                .stream()
                                .peek(offer -> {
                                    offer.setStatus(StatusOffer.REJECTED);
                                })
                                .collect(Collectors.toList());
                if (offersReject.size() > 0) {
                    offerRepository.saveAll(offersReject);
                }
                if (offersAccept.size() > 0) {
                    offerRepository.saveAll(offersAccept);
                    //update inventory accepted to sale pending
                    List<Long> invAuctionIdAccepteds = offersAccept.stream().map(Offer::getInventoryAuctionId).collect(Collectors.toList());
                    List<Long> inventoryIdAccepteds = CommonUtils.findObjects(inventoryAuctions, o->invAuctionIdAccepteds.contains(o.getId())).stream().map(InventoryAuction::getInventoryId).collect(Collectors.toList());
                    CommonUtils.stream(invAuctionIdAccepteds);
                    List<Inventory> inventoriesAccepted = inventoryRepository.findAllByIds(inventoryIdAccepteds)
                            .stream().peek(
                                    inv->inv.setStage(StageInventory.SALE_PENDING)
                            ).collect(Collectors.toList());
                    if (inventoriesAccepted.size() > 0) {
                        inventoryRepository.saveAll(inventoriesAccepted);
                    }
                    //end

                    //map to inven location
//                    Map<Long, InventoryLocationShipping> locations = inventoryLocationShippingRepository.findAll(inventoryIdAccepteds)
//                            .stream()
//                            .collect(Collectors.toMap(InventoryLocationShipping::getInventoryId, s -> s));
//                    Map<Long, InventoryAuction> tempInvAuc = inventoryAuctions.stream()
//                            .collect(Collectors.toMap(InventoryAuction::getId, ia -> ia));
//                    Map<Long, InventoryLocationShipping> tempOfferLocations = offersAccept.stream().collect(
//                            Collectors.toMap(
//                                    Offer::getId,
//                                    o -> locations.get(tempInvAuc.get(o.getInventoryAuctionId()).getInventoryId()))
//                            );
                    Map<Long, OfferInventoryAuction> tempOfferInv = pendingOffers.stream()
                            .collect(Collectors.toMap(OfferInventoryAuction::getOfferId, o -> o));
                    //add cart async
                    List<Observable<ScheduleAddCartResponse>> observables = offersAccept.stream()
                            .map(o -> createObservableAddCartAndSendMail(tempOfferInv.get(o.getId())))
                            .collect(Collectors.toList());
                    Observable.zip(observables, listAddCart -> {
                        List<ScheduleAddCartResponse> addCartResponses = new ArrayList<>();
                        for (Object o : listAddCart) {
                            addCartResponses.add((ScheduleAddCartResponse) o);
                        }
                        return addCartResponses;
                    }).subscribe();
                    //end
                }

            }

            List<Long> inventoriesIdNotAccepted = new ArrayList<>();
            for (InventoryAuction inventoryAuction : inventoryAuctions) {
                boolean isHas = false;
                for (OfferInventoryAuction pendingOffer : pendingOffers) {
                    if (inventoryAuction.getInventoryId() == pendingOffer.getInventoryId()) {
                        isHas = true;
                        break;
                    }
                }
                if (!isHas) {
                    inventoriesIdNotAccepted.add(inventoryAuction.getInventoryId());
                }
            }
            if (inventoriesIdNotAccepted.size() > 0) {
                List<Inventory> inventories = inventoryRepository.findAllByIds(inventoriesIdNotAccepted);
                List<Long> inventoriesId = inventories.stream().map(Inventory::getId).collect(Collectors.toList());

                if (inventories.size() > 0) {
                    List<Integer> inventoryNotListing = inventoryRepository.getAllInventoryNotInMarketListing(inventoriesId);
                    if (inventoryNotListing.size() > 0) {
                        List<Inventory> invNotListing =
                                inventories.stream().filter(inv -> inventoryNotListing.contains((int) inv.getId()))
                                        .peek(inv -> {
                                            InventoryAuction inventoryAuction = CommonUtils.findObject(inventoryAuctions, invAuction -> invAuction.getInventoryId() == inv.getId());
                                            if (inventoryAuction != null && inventoryAuction.getLastStageInventory() != null) {
                                                inv.setStage(inventoryAuction.getLastStageInventory());
                                            } else {
                                                inv.setStage(StageInventory.READY_LISTED);
                                            }
                                        })
                                        .collect(Collectors.toList());
                        if (invNotListing.size() > 0) {
                            inventoryRepository.saveAll(invNotListing);
                        }
                    }
                }
            }

            inventoryAuctionRepository.saveAll(inventoryAuctions);
            auctionRepository.saveAll(auctions);
        }
    }

    private void updateStageInventoryOfferAccept(List<Offer> offerAccepted, List<InventoryAuction> inventoryAuctions) {
        List<Long> inventoryIds = new ArrayList<>();
        if (offerAccepted.size() > 0) {
            for (InventoryAuction inventoryAuction : inventoryAuctions) {
                inventoryIds.add(inventoryAuction.getInventoryId());
            }
        }
        CommonUtils.stream(inventoryIds);
        if (inventoryIds.size() > 0) {
            inventoryRepository.updateStage(inventoryIds, StageInventory.SALE_PENDING.getValue());
        }
    }

    public void onFail(Throwable throwable) {
    }

    private Observable<ScheduleAddCartResponse> createObservableAddCartAndSendMail(
            OfferInventoryAuction offer
//            , InventoryLocationShipping inventoryLocationShipping
    ) {
        return Observable.create((ObservableOnSubscribe<ScheduleAddCartResponse>) su -> {
            ScheduleAddCartResponse scheduleAddCartResponse = new ScheduleAddCartResponse();
            scheduleAddCartResponse.setOfferId(offer.getOfferId());
            try {
                CartResponse cartAdditionResponse = billingManager.addCart(
                        offer.getBuyerId(),
                        ValueCommons.CART_TYPE_AUCTION,
                        offer.getOfferId()
//                        inventoryLocationShipping.isAllowLocalPickup()
                );
                scheduleAddCartResponse.setCartAddResponse(cartAdditionResponse);
            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                scheduleAddCartResponse.setCartErrorResponse(msgError);
            }
            try {
                SendingMailResponse sendingMailResponse = notificationManager.sendMailAuctionAccepted(offer);
                scheduleAddCartResponse.setSendingMailResponse(sendingMailResponse);
            } catch (HttpClientErrorException ex) {
                ErrorRestResponse msgError =
                        objectMapper.readValue(new String(ex.getResponseBodyAsByteArray()), ErrorRestResponse.class);
                msgError.setStatusHttp(ex.getStatusCode().value());
                scheduleAddCartResponse.setMailErrorResponse(msgError);
            }

            su.onNext(scheduleAddCartResponse);
            su.onComplete();
        }).subscribeOn(Schedulers.newThread());
    }
}
