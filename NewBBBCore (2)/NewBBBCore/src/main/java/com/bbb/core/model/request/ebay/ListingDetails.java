package com.bbb.core.model.request.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlElementRef;
import java.time.LocalDateTime;

public class ListingDetails {
    @JacksonXmlProperty(localName = "Adult")
    private Boolean adult;
    @JacksonXmlProperty(localName = "BindingAuction")
    private Boolean bindingAuction;
    @JacksonXmlProperty(localName = "CheckoutEnabled")
    private Boolean checkoutEnabled;
    @JacksonXmlProperty(localName = "ConvertedBuyItNowPrice")
    private Price convertedBuyItNowPrice;
    @JacksonXmlProperty(localName = "ConvertedStartPrice")
    private Price convertedStartPrice;
    @JacksonXmlProperty(localName = "ConvertedReservePrice")
    private Price convertedReservePrice;
    @JacksonXmlProperty(localName = "HasReservePrice")
    private Price hasReservePrice;
    @JacksonXmlProperty(localName = "StartTime")
    private LocalDateTime startTime;
    @JacksonXmlProperty(localName = "EndTime")
    private LocalDateTime endTime;
    @JacksonXmlProperty(localName = "ViewItemURL")
    private String viewItemURL;
    @JacksonXmlProperty(localName = "HasUnansweredQuestions")
    private Boolean hasUnansweredQuestions;
    @JacksonXmlProperty(localName = "HasPublicMessages")
    private Boolean hasPublicMessages;
    @JacksonXmlProperty(localName = "ViewItemURLForNaturalSearch")
    private String viewItemURLForNaturalSearch;
    @JacksonXmlProperty(localName = "BestOfferAutoAcceptPrice")
    private BestOfferAutoAcceptPrice bestOfferAutoAcceptPrice;
    @JacksonXmlProperty(localName = "MinimumBestOfferPrice")
    private BestOfferAutoAcceptPrice minimumBestOfferPrice;

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public Boolean getBindingAuction() {
        return bindingAuction;
    }

    public void setBindingAuction(Boolean bindingAuction) {
        this.bindingAuction = bindingAuction;
    }

    public Boolean getCheckoutEnabled() {
        return checkoutEnabled;
    }

    public void setCheckoutEnabled(Boolean checkoutEnabled) {
        this.checkoutEnabled = checkoutEnabled;
    }

    public Price getConvertedBuyItNowPrice() {
        return convertedBuyItNowPrice;
    }

    public void setConvertedBuyItNowPrice(Price convertedBuyItNowPrice) {
        this.convertedBuyItNowPrice = convertedBuyItNowPrice;
    }

    public Price getConvertedStartPrice() {
        return convertedStartPrice;
    }

    public void setConvertedStartPrice(Price convertedStartPrice) {
        this.convertedStartPrice = convertedStartPrice;
    }

    public Price getConvertedReservePrice() {
        return convertedReservePrice;
    }

    public void setConvertedReservePrice(Price convertedReservePrice) {
        this.convertedReservePrice = convertedReservePrice;
    }

    public Price getHasReservePrice() {
        return hasReservePrice;
    }

    public void setHasReservePrice(Price hasReservePrice) {
        this.hasReservePrice = hasReservePrice;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getViewItemURL() {
        return viewItemURL;
    }

    public void setViewItemURL(String viewItemURL) {
        this.viewItemURL = viewItemURL;
    }

    public Boolean getHasUnansweredQuestions() {
        return hasUnansweredQuestions;
    }

    public void setHasUnansweredQuestions(Boolean hasUnansweredQuestions) {
        this.hasUnansweredQuestions = hasUnansweredQuestions;
    }

    public Boolean getHasPublicMessages() {
        return hasPublicMessages;
    }

    public void setHasPublicMessages(Boolean hasPublicMessages) {
        this.hasPublicMessages = hasPublicMessages;
    }

    public String getViewItemURLForNaturalSearch() {
        return viewItemURLForNaturalSearch;
    }

    public void setViewItemURLForNaturalSearch(String viewItemURLForNaturalSearch) {
        this.viewItemURLForNaturalSearch = viewItemURLForNaturalSearch;
    }

    public BestOfferAutoAcceptPrice getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(BestOfferAutoAcceptPrice bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public BestOfferAutoAcceptPrice getMinimumBestOfferPrice() {
        return minimumBestOfferPrice;
    }

    public void setMinimumBestOfferPrice(BestOfferAutoAcceptPrice minimumBestOfferPrice) {
        this.minimumBestOfferPrice = minimumBestOfferPrice;
    }
}

