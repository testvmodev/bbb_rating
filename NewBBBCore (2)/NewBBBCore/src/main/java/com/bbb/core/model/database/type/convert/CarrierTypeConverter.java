package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.CarrierType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CarrierTypeConverter implements AttributeConverter<CarrierType, String> {
    @Override
    public String convertToDatabaseColumn(CarrierType carrierType) {
        if ( carrierType == null ) {
            return null;
        }
        return carrierType.getValue();
    }

    @Override
    public CarrierType convertToEntityAttribute(String s) {
        return CarrierType.findByValue(s);
    }
}
