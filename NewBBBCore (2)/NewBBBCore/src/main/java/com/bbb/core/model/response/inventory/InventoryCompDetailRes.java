package com.bbb.core.model.response.inventory;

import com.bbb.core.model.database.embedded.InventoryCompDetailId;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryCompDetailRes {
    @Id
    private InventoryCompDetailId id;
    private String name;
    private String value;

    public InventoryCompDetailId getId() {
        return id;
    }

    public void setId(InventoryCompDetailId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
