package com.bbb.core.manager.market;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.MarketPlaceConfig;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.request.market.MarketPlaceConfigCreateRequest;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.market.MarketPlaceConfigDetailResponse;
import com.bbb.core.repository.market.MarketPlaceConfigRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.bbb.core.repository.market.out.MarketPlaceConfigResponseRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MarketPlaceConfigManager {
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private MarketPlaceConfigResponseRepository marketPlaceConfigResponseRepository;
    @Autowired
    private AppConfig appConfig;

    public Object getAllMarketPlaceConfig() {
        return marketPlaceConfigResponseRepository.findAll();
    }

    public Object getMarketPlaceConfigDetail(long id) throws ExceptionResponse {
        MarketPlaceConfigDetailResponse response = marketPlaceConfigResponseRepository.findOne(id);
        if (response == null) {
            noConfigThrowException();
        }
        return response;
    }

    public Object postMarketPlaceConfig(MarketPlaceConfigCreateRequest request) throws ExceptionResponse {
        MarketPlace marketPlace = marketPlaceRepository.findOne(request.getMarketPlaceId());
        if (marketPlace == null || marketPlace.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(
                            ObjectError.ERROR_NOT_FOUND,
                            MessageResponses.MARKET_PLACE_CONFIG_NOT_EXIST
                    ),
                    HttpStatus.NOT_FOUND
            );
        }

        List<MarketPlaceConfig> configs = marketPlaceConfigRepository.findAllByMarketPlaceId(
                request.getMarketPlaceId(),
                appConfig.getEbay().isSandbox());
        if (configs.size() > 0) {
            throw new ExceptionResponse(
                    new ObjectError(
                            ObjectError.ERROR_NOT_FOUND,
                            MessageResponses.MARKET_CONFIG_ALREADY_EXISTED
                    ),
                    HttpStatus.BAD_REQUEST
            );
        }

        request.setName(validateName(request.getName()));

        MarketPlaceConfig marketPlaceConfig = new MarketPlaceConfig();
        marketPlaceConfig.setMarketPlaceId(request.getMarketPlaceId());
        marketPlaceConfig.setName(request.getName());
        marketPlaceConfig.setEbayAppId(request.getEbayAppId());
        marketPlaceConfig.setEbayCertId(request.getEbayCertId());
        marketPlaceConfig.setEbayDevId(request.getEbayDevId());
        marketPlaceConfig.setEbayToken(request.getEbayToken());
        marketPlaceConfig.setEbayApiSandbox(request.isEbayApiSandbox());

        if (marketPlace.getType() == MarketType.EBAY) {
            validateEbay(marketPlaceConfig);
        }

        return marketPlaceConfigRepository.save(marketPlaceConfig);
    }

    public Object updateMarketPlaceConfig(long id, MarketPlaceConfigCreateRequest request) throws ExceptionResponse {
        MarketPlaceConfig marketPlaceConfig = findMarketPlaceConfig(id);
        MarketPlace marketPlace = marketPlaceRepository.findOne(
                request.getMarketPlaceId() == null ? marketPlaceConfig.getMarketPlaceId() : request.getMarketPlaceId()
        );

        if (marketPlace == null || marketPlace.isDelete()) {
            throw new ExceptionResponse(
                    new ObjectError(
                            ObjectError.ERROR_NOT_FOUND,
                            MessageResponses.MARKET_PLACE_CONFIG_NOT_EXIST
                    ),
                    HttpStatus.NOT_FOUND
            );
        }

        if (request.getMarketPlaceId() != null && request.getMarketPlaceId() != marketPlaceConfig.getMarketPlaceId()) {
            List<MarketPlaceConfig> configs = marketPlaceConfigRepository.findAllByMarketPlaceId(
                    request.getMarketPlaceId(),
                    appConfig.getEbay().isSandbox());
            if (configs.size() > 0) {
                throw new ExceptionResponse(
                        new ObjectError(
                                ObjectError.ERROR_NOT_FOUND,
                                MessageResponses.MARKET_CONFIG_ALREADY_EXISTED
                        ),
                        HttpStatus.BAD_REQUEST
                );
            }
            marketPlaceConfig.setMarketPlaceId(request.getMarketPlaceId());
        }
        if (request.getName() != null) {
            request.setName(validateName(request.getName()));
            marketPlaceConfig.setName(request.getName());
        }
        if (request.getEbayAppId() != null) {
            marketPlaceConfig.setEbayAppId(request.getEbayAppId());
        }
        if (request.getEbayCertId() != null) {
            marketPlaceConfig.setEbayCertId(request.getEbayCertId());
        }
        if (request.getEbayDevId() != null) {
            marketPlaceConfig.setEbayDevId(request.getEbayDevId());
        }
        if (request.getEbayToken() != null) {
            marketPlaceConfig.setEbayToken(request.getEbayToken());
        }
        if (request.isEbayApiSandbox() != null) {
            marketPlaceConfig.setEbayApiSandbox(request.isEbayApiSandbox());
        }

        if (marketPlace.getName().toUpperCase().contains("EBAY")) {
            validateEbay(marketPlaceConfig);
        }

        return marketPlaceConfigRepository.save(marketPlaceConfig);
    }

    public Object deleteMarketPlaceConfig(long id) throws ExceptionResponse {
        MarketPlaceConfig marketPlaceConfig = findMarketPlaceConfig(id);
        marketPlaceConfig.setDelete(true);
        marketPlaceConfigRepository.save(marketPlaceConfig);
        return new MessageResponse(MessageResponses.MARKET_PLACE_CONFIG_DELETED);
    }

    private MarketPlaceConfig findMarketPlaceConfig(long id) throws ExceptionResponse {
        MarketPlaceConfig marketPlaceConfig = marketPlaceConfigRepository.findOne(id);
        if (marketPlaceConfig == null || marketPlaceConfig.isDelete()) {
            noConfigThrowException();
        }
        return marketPlaceConfig;
    }

    private String validateName(String name) throws ExceptionResponse {
        if (StringUtils.isBlank(name)) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.MARKET_CONFIG_NAME_MUST_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST
            );
        }
        return name.trim();
    }

    public void validateEbay(MarketPlaceConfig config) throws ExceptionResponse {
        String message = null;
        if (StringUtils.isBlank(config.getEbayAppId())) {
            message = MessageResponses.EBAY_APP_ID_MUST_NOT_EMPTY;
        }
        if (StringUtils.isBlank(config.getEbayCertId())) {
            message = MessageResponses.EBAY_CERT_ID_MUST_NOT_EMPTY;
        }
        if (StringUtils.isBlank(config.getEbayDevId())) {
            message = MessageResponses.EBAY_DEV_ID_MUST_NOT_EMPTY;
        }
        if (StringUtils.isBlank(config.getEbayToken())) {
            message = MessageResponses.EBAY_TOKEN_MUST_NOT_EMPTY;
        }
//        if (config.isEbayApiSandbox() == null) {
//            message = MessageResponses.EBAY_API_SANDBOX_MUST_NOT_NULL;
//        }
        if (message != null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, message),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void noConfigThrowException() throws ExceptionResponse {
        throw new ExceptionResponse(
                new ObjectError(
                        ObjectError.ERROR_NOT_FOUND,
                        MessageResponses.MARKET_PLACE_CONFIG_NOT_EXIST
                ),
                HttpStatus.NOT_FOUND
        );
    }

    @Async
    public void deleteMarketPlaceConfigAsync(MarketPlace marketPlace) {
        List<MarketPlaceConfig> configs = marketPlaceConfigRepository.findAllByMarketPlaceId(
                marketPlace.getId(),
                appConfig.getEbay().isSandbox());
        if (configs.size() > 0) {
            for (MarketPlaceConfig config : configs) {
                config.setDelete(true);
            }
            marketPlaceConfigRepository.saveAll(configs);
        }
    }
}
