package com.bbb.core.model.request.component;

import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class ComponentTypeGetRequest {
    private ComponentTypeSortField sortField;
    private Sort sortType;
    private Pageable pageable;

    public ComponentTypeSortField getSortField() {
        return sortField;
    }

    public void setSortField(ComponentTypeSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
