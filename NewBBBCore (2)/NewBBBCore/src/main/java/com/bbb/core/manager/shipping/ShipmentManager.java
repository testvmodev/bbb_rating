package com.bbb.core.manager.shipping;

import com.bbb.core.common.Constants;
import com.bbb.core.common.IntegratedServices;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.manager.templaterest.RestShipmentManager;
import com.bbb.core.model.database.InventoryLocationShipping;
import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.otherservice.request.shipment.ShipmentCreateRequest;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentFromAddress;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentToAddress;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentType;
import com.bbb.core.model.otherservice.response.order.OrderShippingAddress;
import com.bbb.core.model.otherservice.response.shipping.ShipmentDetailResponse;
import com.bbb.core.model.otherservice.response.shipping.ShipmentErrorResponse;
import com.bbb.core.model.otherservice.response.shipping.ShipmentResponse;
import com.bbb.core.model.otherservice.response.shipping.ShippingConfigResponse;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.request.market.personallisting.PersonalListingShipmentCreateRequest;
import com.bbb.core.model.request.tradein.tradin.TradeInShippingCreateRequest;
import com.bbb.core.model.response.ObjectError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ShipmentManager {
    @Autowired
    private RestShipmentManager restShipmentManager;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;

    public ShipmentResponse createInboundShipment(
            TradeInShippingCreateRequest request,
            UserDetailFullResponse userDetailFullResponse
    ) throws ExceptionResponse {
        ShipmentCreateRequest shipmentRequest = new ShipmentCreateRequest();
        shipmentRequest.setTradeInId(request.getTradeInId());
        shipmentRequest.setType(ShipmentType.INBOUND);
        shipmentRequest.setInboundShippingType(request.getShippingType());

        if (request.getShippingType() == InboundShippingType.BICYCLE_BLUE_BOOK_TYPE) {
            shipmentRequest.setLength(request.getBlueBook().getLength());
            shipmentRequest.setWidth(request.getBlueBook().getWidth());
            shipmentRequest.setHeight(request.getBlueBook().getHeight());
            shipmentRequest.setWeight(request.getBlueBook().getWeight());
            shipmentRequest.setScheduled(request.getBlueBook().getScheduled());
            shipmentRequest.setScheduleDate(request.getBlueBook().getScheduleDate());


            ShipmentFromAddress fromAddress = new ShipmentFromAddress();
            fromAddress.setLine(request.getBlueBook().getFromAddress());
            fromAddress.setCity(request.getBlueBook().getFromCity());
            fromAddress.setStateCode(request.getBlueBook().getFromState());
            fromAddress.setZipCode(request.getBlueBook().getFromZipCode());
            fromAddress.setCountryCode(Constants.DEFAULT_PARTNER_BICYCLE_COUNTRY_CODE);
            fromAddress.setCompanyName(userDetailFullResponse.getPartner().getName());
            fromAddress.setPhone(userDetailFullResponse.getPartner().getPhone());
            shipmentRequest.setFromAddress(fromAddress);
        }


        shipmentRequest.setManualCarrier(request.getMyAccount().getCarrier());
        shipmentRequest.setManualTrackingNumber(request.getMyAccount().getCarrierTrackingNumber());

        return createShipment(shipmentRequest);
    }

    public ShipmentResponse createOutboundShipment(
            PersonalListingShipmentCreateRequest request,
            long inventoryId,
            String sellerPersonal, String sellerCompany, String phone,
            InventoryLocationShipping locationShipping,
            OrderShippingAddress buyerAddress
    ) throws ExceptionResponse {
        ShipmentCreateRequest shipRequest = new ShipmentCreateRequest();
        shipRequest.setInventoryId(inventoryId);
        shipRequest.setType(ShipmentType.OUTBOUND);
        shipRequest.setOutboundShippingType(locationShipping.getShippingType());
        shipRequest.setLength(Constants.DEFAULT_OUTBOUND_PACKAGE_LENGTH);
        shipRequest.setWidth(Constants.DEFAULT_OUTBOUND_PACKAGE_WIDTH);
        shipRequest.setHeight(Constants.DEFAULT_OUTBOUND_PACKAGE_HEIGHT);
        shipRequest.setWeight(Constants.DEFAULT_OUTBOUND_PACKAGE_WEIGHT);
        shipRequest.setScheduled(false);

        if (locationShipping.getShippingType() == OutboundShippingType.BICYCLE_BLUE_BOOK_TYPE) {
            ShipmentFromAddress fromAddress = new ShipmentFromAddress();
            fromAddress.setPersonalName(sellerPersonal);
            fromAddress.setCompanyName(sellerCompany);
            fromAddress.setPhone(phone);
            fromAddress.setLine(locationShipping.getAddressLine());
            fromAddress.setCity(locationShipping.getCityName());
            fromAddress.setStateCode(locationShipping.getStateCode());
            fromAddress.setZipCode(locationShipping.getZipCode());
            fromAddress.setCountryCode(locationShipping.getCountryCode());

            shipRequest.setFromAddress(fromAddress);

            ShipmentToAddress toAddress = new ShipmentToAddress();
            toAddress.setPersonalName(buyerAddress.getRecipientName());
            toAddress.setPhone(buyerAddress.getPhone());
            toAddress.setLine(buyerAddress.getLine1());
            toAddress.setCity(buyerAddress.getCity());
            toAddress.setStateCode(buyerAddress.getState());
            toAddress.setZipCode(buyerAddress.getPostalCode());
            //default buyer in US
            toAddress.setCountryCode(FedExValue.COUNTRY_CODE_DEFAULT);

            shipRequest.setToAddress(toAddress);
        } else {
            shipRequest.setManualCarrier(request.getCarrier());
            shipRequest.setManualTrackingNumber(request.getTrackingNumber());
        }

        return createShipment(shipRequest);
    }

    public List<ShipmentDetailResponse> getTradeInShipmentDetails(List<Long> tradeInIds) {
        Map<String, String> pa = new HashMap<>();
        String param = tradeInIds.stream().map(id -> id.toString()).collect(Collectors.joining(","));
        pa.put("tradeInIds", param);

        return restShipmentManager.getObject(
                IntegratedServices.URL_SHIPMENT_CREATE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                pa,
                new ParameterizedTypeReference<List<ShipmentDetailResponse>>() {
                }
        );
    }

    public List<ShipmentDetailResponse> getInventoryShipmentDetails(List<Long> inventoryIds) {
        Map<String, String> pa = new HashMap<>();
        String param = inventoryIds.stream().map(id -> id.toString()).collect(Collectors.joining(","));
        pa.put("inventoryIds", param);

        return restShipmentManager.getObject(
                IntegratedServices.URL_SHIPMENT_CREATE,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                pa,
                new ParameterizedTypeReference<List<ShipmentDetailResponse>>() {
                }
        );
    }

    public ShippingConfigResponse getActiveShippingConfig() {
        return restShipmentManager.getObject(
                IntegratedServices.URL_SHIPMENT_CONFIG_ACTIVED,
                Constants.HEADER_SECRET_KEY,
                ConfigDataSource.getSecretKey(),
                new HashMap<>(),
                ShippingConfigResponse.class
        );
    }

    private ShipmentResponse createShipment(ShipmentCreateRequest request) throws ExceptionResponse {
        ShipmentResponse response;
        try {
            response = restShipmentManager.postObject(
                    IntegratedServices.URL_SHIPMENT_CREATE,
                    Constants.HEADER_SECRET_KEY,
                    ConfigDataSource.getSecretKey(),
                    request,
                    ShipmentResponse.class
            );
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            e.printStackTrace();
            ShipmentErrorResponse error = null;
            try {
                error = objectMapper.readValue(e.getResponseBodyAsString(), ShipmentErrorResponse.class);
            } catch (Exception ex) {
                throw new ExceptionResponse(
                        ObjectError.UNKNOWN_ERROR,
                        MessageResponses.UNKNOWN_ERROR_FROM_SHIPMENT_SERVICE,
                        HttpStatus.BAD_REQUEST
                );
            }
            if (error != null) {
                throw new ExceptionResponse(
                        ObjectError.UNKNOWN_ERROR,
                        error.getMessage(),
                        HttpStatus.BAD_REQUEST
                );
            } else {
                throw new ExceptionResponse(
                        ObjectError.UNKNOWN_ERROR,
                        MessageResponses.UNKNOWN_ERROR_FROM_SHIPMENT_SERVICE,
                        HttpStatus.BAD_REQUEST
                );
            }
        }

        return response;
    }
}
