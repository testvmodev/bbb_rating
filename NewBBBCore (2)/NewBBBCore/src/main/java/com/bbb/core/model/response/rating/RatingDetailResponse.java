package com.bbb.core.model.response.rating;

import com.bbb.core.model.response.ListObjResponse;

public class RatingDetailResponse {
    private String bicycleImages;
    private String bicycleYear;
    private String bicycleBrand;
    private String bicycleModel;
    private String bicycleType;
    private String bicycleSize;
    private float avgRating;
    private ListObjResponse<RatingResponse> ratingResponse;

    public RatingDetailResponse(){
    }

    public RatingDetailResponse(String bicycleImages, String bicycleYear, String bicycleBrand,
                                String bicycleModel, String bicycleType, String bicycleSize) {
        this.bicycleImages = bicycleImages;
        this.bicycleYear = bicycleYear;
        this.bicycleBrand = bicycleBrand;
        this.bicycleModel = bicycleModel;
        this.bicycleType = bicycleType;
        this.bicycleSize = bicycleSize;
    }

    public String getBicycleImages() {
        return bicycleImages;
    }

    public void setBicycleImages(String bicycleImages) {
        this.bicycleImages = bicycleImages;
    }

    public String getBicycleYear() {
        return bicycleYear;
    }

    public void setBicycleYear(String bicycleYear) {
        this.bicycleYear = bicycleYear;
    }

    public String getBicycleBrand() {
        return bicycleBrand;
    }

    public void setBicycleBrand(String bicycleBrand) {
        this.bicycleBrand = bicycleBrand;
    }

    public String getBicycleModel() {
        return bicycleModel;
    }

    public void setBicycleModel(String bicycleModel) {
        this.bicycleModel = bicycleModel;
    }

    public String getBicycleType() {
        return bicycleType;
    }

    public void setBicycleType(String bicycleType) {
        this.bicycleType = bicycleType;
    }

    public String getBicycleSize() {
        return bicycleSize;
    }

    public void setBicycleSize(String bicycleSize) {
        this.bicycleSize = bicycleSize;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public ListObjResponse<RatingResponse> getRatingResponse() {
        return ratingResponse;
    }

    public void setRatingResponse(ListObjResponse<RatingResponse> ratingResponse) {
        this.ratingResponse = ratingResponse;
    }
}
