package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.offer.OfferMarAuc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferMarAucRepository extends JpaRepository<OfferMarAuc, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "offer.id AS id, " +
            "(CASE WHEN offer.market_listing_id IS NULL THEN inventory_auction.inventory_id ELSE market_listing.inventory_id END ) AS inventory_id, " +
            "offer.market_listing_id AS market_listing_id, " +
            "offer.inventory_auction_id AS inventory_auction_id " +
            "FROM offer " +
            "LEFT JOIN market_listing ON offer.market_listing_id = market_listing.id " +
            "LEFT JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
            "WHERE offer.id IN :offerIds AND " +
            "(offer.inventory_auction_id IS NOT NULL OR offer.market_listing_id IS NOT NULL)")
    List<OfferMarAuc> findAllInventoryIdByOfferId(
            @Param(value = "offerIds")List<Long> offerIds
    );
}
