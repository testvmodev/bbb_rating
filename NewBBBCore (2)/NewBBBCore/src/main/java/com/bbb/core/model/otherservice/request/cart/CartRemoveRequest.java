package com.bbb.core.model.otherservice.request.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartRemoveRequest {
    @JsonProperty("inventory_id")
    private long inventoryId;
    @JsonProperty("buyer_id")
    private String buyerId;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }
}
