package com.bbb.core.model.request.mail.customquote;

public class ChangeValueCustomQuoteContentMailRequest {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
