package com.bbb.core.service.rating;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.rating.RatingCreateRequest;
import com.bbb.core.model.request.rating.RatingGetRequest;
import com.bbb.core.model.request.rating.RatingUpdateRequest;
import org.springframework.data.domain.Pageable;

public interface RatingService {

    Object getRatingForBicycle(RatingGetRequest request) throws ExceptionResponse;

    Object getRatingTest(Long marketListingId, Long inventoryAuctionId, Pageable pageable) throws Exception;

    Object getRatingForUser(Long marketListingId, Long inventoryAuctionId, String userId) throws Exception;

    Object createRating(RatingCreateRequest request, Long invetoryAuctionId, Long marketListingId) throws Exception;

    Object updateRating(RatingUpdateRequest request, Long id, Long inventoryAuctionId, Long marketListingId) throws Exception;
}
