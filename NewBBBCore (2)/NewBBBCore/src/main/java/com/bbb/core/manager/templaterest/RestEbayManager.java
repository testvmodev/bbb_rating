package com.bbb.core.manager.templaterest;

import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.common.utils.Third;
import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.MarketPlaceConfig;
import com.bbb.core.model.database.type.MarketType;
import com.bbb.core.model.request.ebay.BaseEbayRequest;
import com.bbb.core.model.request.ebay.RequesterCredentials;
import com.bbb.core.model.request.ebay.fetchtoken.FetchTokenRequest;
import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.bbb.core.model.response.ebay.Error;
import com.bbb.core.model.response.ebay.fetchtoken.FetchTokenResponse;
import com.bbb.core.repository.market.MarketPlaceConfigRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RestEbayManager extends RestTemplateManager {
    private static final Logger LOG = LoggerFactory.getLogger(RestEbayManager.class);
    private final XmlMapper xmlMapper;
    private final MarketPlaceConfigRepository marketPlaceConfigRepository;
    private final MarketPlaceRepository marketPlaceRepository;
    private boolean isSandbox;

    @Autowired
    public RestEbayManager(AppConfig appConfig, XmlMapper xmlMapper,
                           MarketPlaceConfigRepository marketPlaceConfigRepository,
                           MarketPlaceRepository marketPlaceRepository
    ) {
        super(appConfig.getEbay().getEndpoint());
        this.xmlMapper = xmlMapper;
        this.marketPlaceConfigRepository = marketPlaceConfigRepository;
        this.marketPlaceRepository = marketPlaceRepository;
        this.isSandbox = appConfig.getEbay().isSandbox();
    }

    public <Request extends BaseEbayRequest, T extends BaseEbayResponse> Third<String, String, T> postTradeIn(Request request, String typeApi, Class<T> clazzResponse) {
        MarketPlaceConfig marketPlaceConfig = getMarketPlaceEbay();
        RequesterCredentials requesterCredentials = new RequesterCredentials();
        requesterCredentials.seteBayAuthToken(marketPlaceConfig.getEbayToken());
        request.setRequesterCredentials(requesterCredentials);
        return postEbay(request, typeApi, marketPlaceConfig, clazzResponse);

    }

    private <Request extends BaseEbayRequest, T extends BaseEbayResponse> Third<String, String, T> postEbay(Request request, String typeApi, MarketPlaceConfig marketPlaceConfig, Class<T> clazzResponse) {
        String contentRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                CommonUtils.toXmlString(request, xmlMapper);
        System.out.println("content:\n " + contentRequest);
        Pair<String, T> response = postEbayRoot(contentRequest, typeApi, marketPlaceConfig, clazzResponse);
        if (response.getSecond() != null && response.getSecond().getErrors() != null && response.getSecond().getErrors().size() > 0) {
            for (Error error : response.getSecond().getErrors()) {
                if (EbayValue.ERROR_EBAY.equals(error.getSeverityCode()) &&
                        CommonUtils.checkEbayInvalid(error.getErrorCode())) {
                    String token = fetchToken();
                    if (token != null) {
                        request.getRequesterCredentials().seteBayAuthToken(token);
                        contentRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                CommonUtils.toXmlString(request, xmlMapper);
                        response = postEbayRoot(contentRequest, typeApi, marketPlaceConfig, clazzResponse);
                        LOG.info("postEbay response:\n " + response.getFirst());
                        return new Third<>(contentRequest, response.getFirst(), response.getSecond());
                    }
                    break;
                }
            }
        }
        LOG.info("postEbay response:\n " + response.getFirst());
        return new Third<>(contentRequest, response.getFirst(), response.getSecond());
    }

    private String fetchToken() {
        MarketPlaceConfig marketPlaceConfig = getMarketPlaceEbay();
        FetchTokenRequest fetchTokenRequest = new FetchTokenRequest();
        fetchTokenRequest.setSessionID(marketPlaceConfig.getSessionEbay());
        String contentRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                CommonUtils.toXmlString(fetchTokenRequest, xmlMapper);
        Pair<String, FetchTokenResponse> token =
                postEbayRoot(contentRequest, getMapHeaderEbayNotToken(marketPlaceConfig, EbayValue.FETCH_TOKEN),
                        FetchTokenResponse.class);
        LOG.info("fetchToken response: " + token.getFirst());
        if (token.getSecond() != null && token.getSecond().getErrors() != null && token.getSecond().getErrors().size() > 0) {
            for (Error error : token.getSecond().getErrors()) {
                if (EbayValue.ERROR_EBAY.equals(error.getSeverityCode())) {
                    return null;
                }
            }
        }
        if (token.getSecond() != null) {
            marketPlaceConfig.setEbayToken(token.getSecond().geteBayAuthToken());
            marketPlaceConfigRepository.save(marketPlaceConfig);
        }
        return token.getSecond().geteBayAuthToken();
    }

    private <T> Pair<String, T> postEbayRoot(String contentRequest, String typeApi, MarketPlaceConfig ebayConfig, Class<T> clazzResponse) {
        String content = postObject(
                EbayValue.URL_EBAY_NORMAL, getMapHeaderEbayNotToken(ebayConfig, typeApi), contentRequest, String.class,
                MediaType.APPLICATION_XML
        );
        T response = CommonUtils.toObjectXml(xmlMapper, content, clazzResponse);
        return new Pair<>(content, response);
    }

    private <T> Pair<String, T> postEbayRoot(String contentRequest, Map<String, String> mapHeaderEbay, Class<T> clazzResponse) {
        String content = postObject(
                EbayValue.URL_EBAY_NORMAL, mapHeaderEbay, contentRequest, String.class,
                MediaType.APPLICATION_XML
        );
        T response = CommonUtils.toObjectXml(xmlMapper, content, clazzResponse);
        return new Pair<>(content, response);
    }



    private Map<String, String> getMapHeaderEbayNotToken(MarketPlaceConfig marketPlaceConfig, String typeApi) {
        Map<String, String> mapHeader = new HashMap<>();
        mapHeader.put("X-EBAY-API-SITEID", "0");
        mapHeader.put("X-EBAY-API-COMPATIBILITY-LEVEL", "967");
        mapHeader.put("X-EBAY-API-CALL-NAME", typeApi);
//        mapHeader.put("X-EBAY-API-IAF-TOKEN", "AgAAAA**AQAAAA**aAAAAA**YqipWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GiDJSFogydj6x9nY+seQ**eBIEAA**AAMAAA**SacRPR0av/DB+RA3rOF96K6wye+xB/MgU0cFw5IfUx5Ozb/P1p/DFzG3D5Ar64v6fs5heVy5VjVIAhcnz8uxEg2DP0k+czwCicLcie+TVgb2k5aeYrCdtGzICgoCX8/jvxDE0A3TDVJ2EYIXynzLrFgM53XE8gP0eDVGZ+QhHWKyOCBB9oXkoyhGxU5NQA4tsq7OeEGO89lt1xg+lUPoh3i260GD4MIbiiErAQzYjc8fo0nt4TMYUCYBi6cUbJsZBxr9SrZaCYwcdBtbNoD1OuE/3iOgdOpSxBQ4LC/kXRGmv7zXr6WoTKjk+ekNPC/pdxIYLWPS6aG9emdwWibgiKSQaWOtkdUJKCtGPyP4ExTHuwE9Sjwoy3Rdy/Vubdt9osO9IO+lUgvtGGri/a0iDn93y7fD6Tpw4BZPA/IBrZfwkZDlc6Lo1iA8qaB3PMI/rpmX4lDaNhmVonsED0l7FYTmgKg912pVV3gUg88F4dbbtgcFl0goFF8Euwmr9MJP8HQUMTWLEflA+nXedAiWaKxmUiLEpDpfNtEgKQbrPIbPea+61nU5Vm4C+sAclClS8kAgLxWRDwpqcnfrUUedoB9gD3FRNiDseKMhiFrZ1EmDNeM0ukZIGN9cVUwGZ59/Gk9D+cTdsiFK4i8jYN30jph6avZgYQaH0MKzTM/hpAc6hmR3/XnMdZTyU9ghnwam4MXCKGp8yJ2ado0eHg5l7oSHQ9LILbRf+NdRLxUPJQbsqLnLcBg33/UR4c0Rh/5G");
        mapHeader.put("X-EBAY-API-APP-NAME", marketPlaceConfig.getEbayAppId());
        mapHeader.put("X-EBAY-API-DEV-NAME", marketPlaceConfig.getEbayDevId());
        mapHeader.put("X-EBAY-API-CERT-NAME", marketPlaceConfig.getEbayCertId());
        return mapHeader;
    }

    private MarketPlaceConfig getMarketPlaceEbay() {
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.EBAY.getValue());
        return marketPlaceConfigRepository.findOneByMarketPlaceId(marketPlace.getId(), isSandbox);

    }


}
