package com.bbb.core.model.response.market;

import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.MarketPlaceConfig;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarketPlaceDetailResponse {
    private MarketPlace marketPlace;
    private MarketPlaceConfig configuration;

    public MarketPlace getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(MarketPlace marketPlace) {
        this.marketPlace = marketPlace;
    }

    public MarketPlaceConfig getConfiguration() {
        return configuration;
    }

    public void setConfiguration(MarketPlaceConfig configuration) {
        this.configuration = configuration;
    }
}
