package com.bbb.core.repository.market;

import com.bbb.core.model.database.LogEbayMarketListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEbayMarketListingRepository extends JpaRepository<LogEbayMarketListing, Long> {

}
