package com.bbb.core.model.otherservice.response.valueguide;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PredictionPriceResponse {
    private Float depreciation;
    private Integer duration;
    private String make;
    @JsonProperty("market_place_type")
    private String marketPlaceType;
    private String model;
    private Float msrp;
    private Float prediction;
//    @JsonProperty("sale_date_time")
//    private String saleDateTime;
    @JsonProperty("sales_price")
    private Float salesPrice;
    private String sortModel;
    private String type;
    private Integer year;


    public Float getDepreciation() {
        return depreciation;
    }

    public void setDepreciation(Float depreciation) {
        this.depreciation = depreciation;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getMarketPlaceType() {
        return marketPlaceType;
    }

    public void setMarketPlaceType(String marketPlaceType) {
        this.marketPlaceType = marketPlaceType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Float getMsrp() {
        return msrp;
    }

    public void setMsrp(Float msrp) {
        this.msrp = msrp;
    }

    public Float getPrediction() {
        return prediction;
    }

    public void setPrediction(Float prediction) {
        this.prediction = prediction;
    }

    public Float getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Float salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getSortModel() {
        return sortModel;
    }

    public void setSortModel(String sortModel) {
        this.sortModel = sortModel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
