package com.bbb.core.model.otherservice.request.mail.offer.content;

public class SellerMailRequest {
    private String username;

    public SellerMailRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
