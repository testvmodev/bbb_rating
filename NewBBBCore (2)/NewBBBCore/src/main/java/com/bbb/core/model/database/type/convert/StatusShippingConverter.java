package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.StatusCarrier;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatusShippingConverter implements AttributeConverter<StatusCarrier, String> {
    @Override
    public String convertToDatabaseColumn(StatusCarrier statusCarrier) {
        if ( statusCarrier == null ){
            return null;
        }
        return statusCarrier.getValue();
    }

    @Override
    public StatusCarrier convertToEntityAttribute(String s) {
        return StatusCarrier.findByValue(s);
    }
}
