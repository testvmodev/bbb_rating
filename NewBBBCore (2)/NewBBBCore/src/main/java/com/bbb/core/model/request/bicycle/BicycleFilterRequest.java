package com.bbb.core.model.request.bicycle;

public class BicycleFilterRequest {
    private String brandName;
    private String modelName;
    private Long brandId;
    private Long modelId;
    private Long typeId;
    private Long fromYearId;
    private Long toYearId;
    private Boolean missImage;
    private Boolean missRetailPrice;

    public BicycleFilterRequest(Long brandId, Long modelId, Long typeId, Long fromYearId, Long toYearId, Boolean missImage, Boolean missRetailPrice) {
        this.brandId = brandId;
        this.modelId = modelId;
        this.typeId = typeId;
        this.fromYearId = fromYearId;
        this.toYearId = toYearId;
        this.missImage = missImage;
        this.missRetailPrice = missRetailPrice;
    }

    public BicycleFilterRequest() {
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getFromYearId() {
        return fromYearId;
    }

    public void setFromYearId(Long fromYearId) {
        this.fromYearId = fromYearId;
    }

    public Long getToYearId() {
        return toYearId;
    }

    public void setToYearId(Long toYearId) {
        this.toYearId = toYearId;
    }

    public Boolean getMissImage() {
        return missImage;
    }

    public void setMissImage(Boolean missImage) {
        this.missImage = missImage;
    }

    public Boolean getMissRetailPrice() {
        return missRetailPrice;
    }

    public void setMissRetailPrice(Boolean missRetailPrice) {
        this.missRetailPrice = missRetailPrice;
    }
}
