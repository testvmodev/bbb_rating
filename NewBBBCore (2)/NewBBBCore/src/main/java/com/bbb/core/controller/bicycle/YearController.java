package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.year.YearCreateRequest;
import com.bbb.core.model.request.year.YearUpdateRequest;
import com.bbb.core.service.bicycle.YearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class YearController {
    @Autowired
    private YearService service;


    @GetMapping(value = Constants.URL_GET_BICYCLE_YEAR)
    public ResponseEntity getYears(@RequestParam(value = "yearName", required = false) String yearName) {
        return new ResponseEntity<>(service.getYears(yearName), HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_BICYCLE_YEAR_CREATE)
    public ResponseEntity createYear(
            @RequestBody YearCreateRequest request
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.createYear(request), HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_BICYCLE_YEAR)
    public ResponseEntity updateYear(
            @PathVariable(value = Constants.ID) long yearId,
            @RequestBody YearUpdateRequest request
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.updateYear(yearId, request), HttpStatus.OK);
    }


    @DeleteMapping(value = Constants.URL_BICYCLE_YEAR)
    public ResponseEntity deleteYear(
            @PathVariable(value = Constants.ID) long yearId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.deleteType(yearId), HttpStatus.OK);
    }

    @GetMapping(value = Constants.YEAR_FROM_BRAND_MODEL)
    public ResponseEntity getYearFromBrandModel(
            @RequestParam(value = "brandId", required = false)Long brandId,
            @RequestParam(value = "modelId", required = false)Long modelId
    ) throws ExceptionResponse{
        return new ResponseEntity<>(service.getYearFromBrandModel(brandId,  modelId), HttpStatus.OK);
    }


}
