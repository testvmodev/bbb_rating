package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ReturnPolicy {
    @JacksonXmlProperty(localName = "ReturnsAcceptedOption")
    private String returnsAcceptedOption;
    @JacksonXmlProperty(localName = "RefundOption")
    private String refundOption;
    @JacksonXmlProperty(localName = "ReturnsWithinOption")
    private String returnsWithinOption;
    @JacksonXmlProperty(localName = "Description")
    private String description;
    @JacksonXmlProperty(localName = "ShippingCostPaidByOption")
    private String shippingCostPaidByOption;

    public String getReturnsAcceptedOption() {
        return returnsAcceptedOption;
    }

    public void setReturnsAcceptedOption(String returnsAcceptedOption) {
        this.returnsAcceptedOption = returnsAcceptedOption;
    }

    public String getRefundOption() {
        return refundOption;
    }

    public void setRefundOption(String refundOption) {
        this.refundOption = refundOption;
    }

    public String getReturnsWithinOption() {
        return returnsWithinOption;
    }

    public void setReturnsWithinOption(String returnsWithinOption) {
        this.returnsWithinOption = returnsWithinOption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShippingCostPaidByOption() {
        return shippingCostPaidByOption;
    }

    public void setShippingCostPaidByOption(String shippingCostPaidByOption) {
        this.shippingCostPaidByOption = shippingCostPaidByOption;
    }
}
