package com.bbb.core.model.response.tradein.ups.detail;

import com.bbb.core.model.response.tradein.ups.detail.type.TrackStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Status {
    @JsonProperty("Type")
    private String typeRaw;
    private TrackStatus trackStatus;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Code")
    private String code;

    @JsonProperty("TrackStatus")
    public TrackStatus getTrackStatus() {
        return TrackStatus.findByValue(typeRaw);
//        return trackStatus;
    }

    public void setTrackStatus(TrackStatus trackStatus) {
        this.trackStatus = trackStatus;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
