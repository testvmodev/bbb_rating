package com.bbb.core.service.impl.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.bicycle.ComponentTypeManager;
import com.bbb.core.model.request.component.ComponentTypeCreateRequest;
import com.bbb.core.model.request.component.ComponentTypeGetRequest;
import com.bbb.core.model.request.component.ComponentTypeUpdateRequest;
import com.bbb.core.model.request.sort.ComponentTypeSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.ComponentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComponentTypeServiceImpl implements ComponentTypeService {
    @Autowired
    private ComponentTypeManager manager;

//    @Override
//    public Object importComponentType() {
//        return manager.importComponentType();
//    }

    @Override
    public Object getComponentTypes(ComponentTypeGetRequest request) throws ExceptionResponse {
        return manager.getComponentTypes(request);
    }

    @Override
    public Object getComponentType(long typeId) throws ExceptionResponse {
        return manager.getDetailComponentType(typeId);
    }

    @Override
    public Object createComponentType(ComponentTypeCreateRequest request) throws ExceptionResponse {
        return manager.createComponentType(request);
    }

    @Override
    public Object updateComponentType(long typeId, ComponentTypeUpdateRequest request) throws ExceptionResponse {
        return manager.updateComponentType(typeId,request);
    }

    @Override
    public Object deleteComponentType(long typeId) throws ExceptionResponse{
        return manager.deleteComponentType(typeId);
    }
}
