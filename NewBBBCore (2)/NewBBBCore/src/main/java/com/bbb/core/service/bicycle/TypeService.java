package com.bbb.core.service.bicycle;


import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycletype.BicycleTypeGetRequest;
import com.bbb.core.model.request.bicycletype.TypeCreateRequest;
import com.bbb.core.model.request.bicycletype.TypeUpdateRequest;
import com.bbb.core.model.request.sort.BicycleTypeSortField;
import com.bbb.core.model.request.sort.Sort;

public interface TypeService {
    Object getBicycleTypes(BicycleTypeGetRequest request) throws ExceptionResponse;

    Object getDetailBicycleType (long typeId) throws  ExceptionResponse;

    Object createType(TypeCreateRequest request) throws ExceptionResponse;

    Object updateType(long typeId, TypeUpdateRequest request) throws ExceptionResponse;

    Object deleteType(long typeId) throws ExceptionResponse;
}
