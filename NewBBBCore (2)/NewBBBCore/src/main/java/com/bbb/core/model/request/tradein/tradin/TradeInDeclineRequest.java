package com.bbb.core.model.request.tradein.tradin;

public class TradeInDeclineRequest extends DeclineTradeIn {
    private String partnerId;
    private long bicycleId;
    private long reasonDeclineId;
}
