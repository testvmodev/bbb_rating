package com.bbb.core.model.database.type;

public enum UserRoleType {
    ROOT("root"),
    ADMIN("admin"),
    NORMAL("normal"),//// buyer & seller
    WHOLESALER("wholesaler"),
    TRADE_IN_PARTNER("trade_in_partner"),
    MARKETING_REP("marketing_rep"),
    WAREHOUSE_EMPLOYEE("warehouse_employee"),
    ONLINE_SALES_REP("online_sales_rep"),
    ROAD_SALES_REP("road_sales_rep"),
    CUSTOMER_SERVICE_REP("customer_service_rep"),
    PRODUCT_MANAGER("product_manager"),
    EXECUTIVE_STAFF("executive_staff"),
    PARTNER_ADMIN("partner_admin"),
    PARTNER_MANAGER("partner_manager"),
    PARTNER_EMPLOYEE("partner_employee"),
    ONLINE_STORE("online_store"),
    PARTNER("Partner");
    private final String value;

    UserRoleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static UserRoleType findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (UserRoleType userRoleType : UserRoleType.values()) {
            if (userRoleType.getValue().equals(value)) {
                return userRoleType;
            }
        }
        return null;
    }
}
