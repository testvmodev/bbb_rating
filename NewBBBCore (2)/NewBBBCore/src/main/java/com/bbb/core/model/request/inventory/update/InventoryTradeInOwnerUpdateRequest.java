package com.bbb.core.model.request.inventory.update;

import org.joda.time.LocalDateTime;

public class InventoryTradeInOwnerUpdateRequest {
    private String name;
    private String phone;
    private String street;
    private String city;
    private String state;
    private String postCode;
    private String email;
    private String proofName;
    private LocalDateTime proofDate;
    private Long tradedBikeId;
    private String tradedForBike;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProofName() {
        return proofName;
    }

    public void setProofName(String proofName) {
        this.proofName = proofName;
    }

    public LocalDateTime getProofDate() {
        return proofDate;
    }

    public void setProofDate(LocalDateTime proofDate) {
        this.proofDate = proofDate;
    }

    public Long getTradedBikeId() {
        return tradedBikeId;
    }

    public void setTradedBikeId(Long tradedBikeId) {
        this.tradedBikeId = tradedBikeId;
    }

    public String getTradedForBike() {
        return tradedForBike;
    }

    public void setTradedForBike(String tradedForBike) {
        this.tradedForBike = tradedForBike;
    }
}
