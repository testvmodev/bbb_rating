package com.bbb.core.model.request.inventory.secret;

import com.bbb.core.model.request.inventory.update.InventoryBaseUpdateRequest;

public class InventoryUpdateSecretRequest {
    private InventoryBaseUpdateRequest baseInfo;

    public InventoryBaseUpdateRequest getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(InventoryBaseUpdateRequest baseInfo) {
        this.baseInfo = baseInfo;
    }
}
