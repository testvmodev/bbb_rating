package com.bbb.core.security.authen.jwt;

import com.bbb.core.common.Constants;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by ducnd on 6/25/17.
 */

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication instanceof JwtAuthenticationToken) {
            JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
//        Condition condition = User.USER.USERNAME.eq(token.getCredentials().getUsername());
//        UserRecord userRecord = baseManager.getDslContext().selectFrom(User.USER).where(condition).fetchOne();
//        if (userRecord == null) {
//            throw new AuthenticationServiceException("fount fount startauction");
//        }
//        if ( !token.getCredentials().getToken().equals(userRecord.getToken())) {
//            throw new AuthenticationServiceException("token invalidate");
//        }
            List<GrantedAuthority> listRowAuthority = new ArrayList<>();
            if (token.getCredentials().getUserRoles() != null) {
                listRowAuthority = token.getCredentials().getUserRoles().stream()
                        .map(role -> new SimpleGrantedAuthority(Constants.ROLE_PREFIX + role))
                        .collect(Collectors.toList());
            }
            return new UsernamePasswordAuthenticationToken(token.getCredentials(), null,
                    listRowAuthority);
//        return new JwtAuthenticationToken(token.getCredentials(),
//                listRowAuthority);
        } else if (authentication instanceof SecretAPIAuthentication) {
            SecretAPIAuthentication secret = (SecretAPIAuthentication) authentication;
            return new UsernamePasswordAuthenticationToken(secret.getCredentials(), null, new ArrayList<>());
        } else if (authentication instanceof GuestAuthentication) {
            return authentication;
        }
        //unknown authentication will be fail to authenticate
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthenticationToken.class.isAssignableFrom(authentication) ||
                SecretAPIAuthentication.class.isAssignableFrom(authentication) ||
                GuestAuthentication.class.isAssignableFrom(authentication);
    }
}
