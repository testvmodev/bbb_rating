package com.bbb.core.repository.market;

import com.bbb.core.model.database.MarketListingFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingFilterRepository extends JpaRepository<MarketListingFilter, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM market_listing_filter WHERE " +
            "market_listing_filter.is_delete = 0 AND " +
            "market_listing_filter.is_active = 1")
    List<MarketListingFilter> findAllActive();

    @Query(nativeQuery = true, value = "SELECT * FROM market_listing_filter WHERE " +
            "market_listing_filter.is_delete = 0 AND " +
            "market_listing_filter.is_active = 0")
    List<MarketListingFilter> findAllInActive();
}
