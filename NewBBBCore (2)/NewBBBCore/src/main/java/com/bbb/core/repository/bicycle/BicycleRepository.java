package com.bbb.core.repository.bicycle;

import com.bbb.core.model.database.Bicycle;
import com.bbb.core.model.request.sort.BicycleSortField;
import com.bbb.core.model.request.sort.Sort;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLInsert;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface BicycleRepository extends CrudRepository<Bicycle, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle " +
            "WHERE id = ?1")
    Bicycle findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle WHERE bicycle.id = ?1 AND bicycle.is_delete = 0")
    Bicycle findOneNotDelete(long id);

    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.* " +

            "FROM (" +
                "SELECT * " +
                "FROM bicycle " +
                "WHERE " +
                "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
                "(:modelId IS NULL OR bicycle.model_id = :modelId) AND " +
                "(:fromYearId IS NULL OR bicycle.year_id >= :fromYearId) AND " +
                "(:toYearId IS NULL OR bicycle.year_id <= :toYearId) AND " +
                "(:typeId IS NULL OR bicycle.type_id = :typeId) AND " +
                "(:hasParamMissingImage = 0 OR bicycle.image_default IS NULL) AND " +
                "(:missRetailPrice = 0 OR bicycle.retail_price IS NULL) AND "+
                "bicycle.is_delete = 0 " +
            ") AS bicycle " +

            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +

            "WHERE " +
            "(:brandName_ IS NULL OR bicycle_brand.name LIKE :#{'%' + #brandName_ + '%'}) " +
            "AND (:modelName IS NULL OR bicycle_model.name LIKE :#{'%' + #modelName + '%'}) " +

            "ORDER BY " +

            "CASE WHEN :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN " +
                "CASE :#{#sortField.name()} " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).ID.name()} THEN CAST(bicycle.id AS nvarchar(11)) " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).BRAND.name()} THEN bicycle_brand.name " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).MODEL.name()} THEN bicycle_model.name " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).YEAR.name()} THEN bicycle_year.name " +
                "ELSE CAST(bicycle.id AS nvarchar(11)) " +
                "END " +
            "END DESC, " +
            "CASE WHEN :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()} THEN " +
                "CASE :#{#sortField.name()} " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).ID.name()} THEN CAST(bicycle.id AS nvarchar(11)) " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).BRAND.name()} THEN bicycle_brand.name " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).MODEL.name()} THEN bicycle_model.name " +
                "WHEN :#{T(com.bbb.core.model.request.sort.BicycleSortField).YEAR.name()} THEN bicycle_year.name " +
                "ELSE CAST(bicycle.id AS nvarchar(11)) " +
                "END " +
            "END ASC " +

            "OFFSET :#{#offset} ROWS FETCH NEXT :#{#limit} ROWS ONLY ")
    List<Bicycle> findAllBy(
            //TODO brandName -> brandName_ to temp fix spring-data bug
            @Param("brandId") Long brandId, @Param("brandName_") String brandName,
            @Param("modelId") Long modelId, @Param("modelName") String modelName,
            @Param("typeId") Long typeId,
            @Param("fromYearId") Long fromYearId,
            @Param("toYearId") Long toYearId,
            @Param("hasParamMissingImage") boolean hasParamMissingImage,
            @Param("missRetailPrice") boolean missRetailPrice,
            @Param("sortField") BicycleSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM (" +
                "SELECT * " +
                "FROM bicycle " +
                "WHERE " +
                "(:brandId IS NULL OR bicycle.brand_id = :brandId) AND " +
                "(:modelId IS NULL OR bicycle.model_id = :modelId) AND " +
                "(:fromYearId IS NULL OR bicycle.year_id >= :fromYearId) AND " +
                "(:toYearId IS NULL OR bicycle.year_id <= :toYearId) AND " +
                "(:typeId IS NULL OR bicycle.type_id = :typeId) AND " +
                "(:hasParamMissingImage = 0 OR bicycle.image_default IS NULL) AND " +
                "(:missRetailPrice = 0 OR bicycle.retail_price IS NULL) AND "+
                "bicycle.is_delete = 0 " +
            ") AS bicycle " +

            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +

            "WHERE " +
            "(:brandName_ IS NULL OR bicycle_brand.name LIKE :#{'%' + #brandName_ + '%'}) " +
            "AND (:modelName IS NULL OR bicycle_model.name LIKE :#{'%' + #modelName + '%'}) "
    )
    int countAll(
            //TODO brandName -> brandName_ to temp fix spring-data bug
            @Param("brandId") Long brandId, @Param("brandName_") String brandName,
            @Param("modelId") Long modelId, @Param("modelName") String modelName,
            @Param("typeId") Long typeId,
            @Param("fromYearId") Long fromYearId,
            @Param("toYearId") Long toYearId,
            @Param("hasParamMissingImage") boolean hasParamMissingImage,
            @Param("missRetailPrice") boolean missRetailPrice
    );

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle")
    List<Bicycle> findAll();

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle " +
            "WHERE bicycle.brand_id = :brandId AND bicycle.is_delete = 0")
    int getCountBrand(@Param(value = "brandId") long brandId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle " +
            "WHERE bicycle.model_id = :modelId AND bicycle.is_delete = 0")
    int getCountModel(@Param(value = "modelId") long modelId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle " +
            "WHERE bicycle.type_id = :typeId AND bicycle.is_delete = 0")
    int getCountType(@Param(value = "typeId") long typeId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle " +
            "WHERE bicycle.year_id = :yearId AND bicycle.is_delete = 0")
    int getCountYear(@Param(value = "yearId") long yearId);

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle " +
            "WHERE " +
            "bicycle.is_delete = 0 AND " +
            "(:isActive IS NULL OR bicycle.active = :isActive) AND " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.model_id = :modelId AND " +
            "bicycle.year_id = :yearId"
    )
    Bicycle findOneByBrandModelYear(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId,
            @Param("isActive") Boolean isActive
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM bicycle " +
            "WHERE " +
            "bicycle.is_delete = 0 AND " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.model_id = :modelId AND " +
            "bicycle.year_id = :yearId"
    )
    int getCountByBrandModelYearIgnoreActive(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle " +
            "WHERE " +
            "bicycle.is_delete = 0 AND " +
            "bicycle.brand_id = :brandId AND " +
            "bicycle.model_id = :modelId AND " +
            "bicycle.year_id = :yearId"
    )
    Bicycle findOneByBrandModelYearIgnoreActive(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(id) FROM bicycle WHERE bicycle.brand_id = :brandId " +
            "AND bicycle.model_id = :modelId AND bicycle.year_id = :yearId AND bicycle.is_delete = 0")
    int getNumber(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId
    );

//    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
//            "FROM bicycle " +
//            "WHERE (:brandId IS NULL OR brand_id = :brandId)" +
//            "AND (:modelId IS NULL OR model_id = :modelId) " +
//            "AND (:yearId IS NULL OR type_id = :yearId) " +
//            "AND (:typeId IS NULL OR type_id = :typeId) " +
//            "AND (:sizeId IS NULL OR size_id = :sizeId) " +
//            "AND is_delete = 0 " +
//            "AND active = 1"
//    )
//    Bicycle findOne(
//            @Param(value = "brandId") Long brandId,
//            @Param(value = "modelId") Long modelId,
//            @Param(value = "yearId") Long yearId,
//            @Param(value = "typeId") Long typeId,
//            @Param(value = "sizeId") Long sizeId
//    );

    @Query(nativeQuery = true, value = "SELECT " +
            "CASE WHEN EXISTS(" +
                "SELECT * " +
                "FROM bicycle " +
                "WHERE (:brandId IS NULL OR brand_id = :brandId)" +
                "AND (:modelId IS NULL OR model_id = :modelId) " +
                "AND (:typeId IS NULL OR type_id = :typeId) " +
                "AND (:yearId IS NULL OR type_id = :yearId) " +
                "AND (:sizeId IS NULL OR size_id = :sizeId)) " +
            "THEN CAST(1 AS BIT) " +
            "ELSE CAST(0 AS BIT) " +
            "END")
    boolean exists(
            @Param(value = "brandId") Long brandId,
            @Param(value = "modelId") Long modelId,
            @Param(value = "typeId") Long typeId,
            @Param(value = "yearId") Long yearId,
            @Param(value = "sizeId") Long sizeId
    );
}
