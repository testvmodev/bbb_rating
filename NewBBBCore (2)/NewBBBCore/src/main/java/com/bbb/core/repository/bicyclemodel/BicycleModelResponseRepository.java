package com.bbb.core.repository.bicyclemodel;

import com.bbb.core.model.request.sort.BicycleModelSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.brand.ModelResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BicycleModelResponseRepository extends CrudRepository<ModelResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_model.id AS id, " +
            "bicycle_model.name AS name, " +
            "bicycle_model.last_update AS last_update, " +
            "bicycle_brand.id AS brand_id, " +
            "bicycle_brand.name AS brand_name, " +
            "bicycle_model.is_approved AS is_approved " +

            "FROM bicycle_model " +
            "JOIN bicycle_brand ON bicycle_model.brand_id = bicycle_brand.id " +

            "WHERE " +
            "bicycle_model.is_delete = 0 AND " +
            "(:isBrandIdNull = 1 OR bicycle_model.brand_id IN :brandIds) AND " +
            "(:nameSearch IS NULL OR bicycle_model.name LIKE  :nameSearch) AND " +
            "(bicycle_model.is_delete = 0) " +

            "ORDER BY " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).NAME.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_model.name END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).NAME.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_model.name END DESC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).APPROVED.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_model.is_approved END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).APPROVED.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_model.is_approved END DESC, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).BRAND.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_brand.name END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.BicycleModelSortField).BRAND.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN bicycle_brand.name END DESC " +

            "OFFSET :offset ROWS FETCH NEXT :limit ROWS ONLY")
    List<ModelResponse> findAllBicycleModel(
            @Param(value = "isBrandIdNull") boolean isBrandIdNull, @Param(value = "brandIds") List<Long> brandIds,
            @Param(value = "nameSearch") String nameSearch,
            @Param(value = "sortField") BicycleModelSortField sortField,
            @Param(value = "sortType") Sort sortType,
            @Param(value = "offset") long offset,
            @Param(value = "limit") int limit
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM bicycle_model " +
            "WHERE " +
            "bicycle_model.is_delete = 0 AND " +
            "(:isBrandIdNull = 1 OR bicycle_model.brand_id IN :brandIds) AND " +
            "(:nameSearch IS NULL OR bicycle_model.name LIKE  :nameSearch) AND " +
            "(bicycle_model.is_delete = 0)"
    )
    int getTotalCount(
            @Param(value = "isBrandIdNull") boolean isBrandIdNull, @Param(value = "brandIds") List<Long> brandIds,
            @Param(value = "nameSearch") String nameSearch
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_model.id AS id, " +
            "bicycle_model.name AS name, " +
            "bicycle_model.last_update AS last_update, " +
            "bicycle_brand.id AS brand_id, " +
            "bicycle_brand.name AS brand_name, " +
            "bicycle_model.is_approved AS is_approved " +

            "FROM bicycle_model " +
            "JOIN bicycle_brand ON bicycle_model.brand_id = bicycle_brand.id " +
            "WHERE bicycle_model.is_delete = 0 " +
            "AND bicycle_model.id = :id"
    )
    ModelResponse getDetailModel(@Param("id") long id);

    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle_model.id AS id, " +
            "bicycle_model.name AS name, " +
            "bicycle_model.last_update AS last_update, " +
            "bicycle_brand.id AS brand_id, " +
            "bicycle_brand.name AS brand_name, " +
            "bicycle_brand.is_approved AS is_approved " +

            "FROM bicycle_model " +
            "JOIN bicycle_brand ON bicycle_model.brand_id = bicycle_brand.id " +
            "WHERE bicycle_model.is_delete = 0 " +
            "AND bicycle_model.brand_id = :id "
    )
    List<ModelResponse> getListModelFromIdBrand(@Param("id") long id);

    @Query(nativeQuery = true, value = "SELECT brand_id FROM bicycle_model WHERE brand_id IN :brandIds GROUP BY brand_id")
    List<Long> checkListBrandIs (@Param("brandIds") List<Long> brandIds);
}
