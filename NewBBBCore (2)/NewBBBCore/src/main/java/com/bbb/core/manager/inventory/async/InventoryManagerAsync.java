package com.bbb.core.manager.inventory.async;

import com.bbb.core.common.utils.Action1;
import com.bbb.core.common.utils.GoogleServiceUtils;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.embedded.InventoryCompDetailId;
import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.otherservice.response.common.GoogleServiceResponse;
import com.bbb.core.model.otherservice.response.common.google.AddressComponent;
import com.bbb.core.model.otherservice.response.common.google.GeometryLocation;
import com.bbb.core.model.request.inventory.create.InventoryCompDetailCreateRequest;
import com.bbb.core.model.request.inventory.create.InventoryLocationShippingCreateRequest;
import com.bbb.core.model.request.inventory.create.InventoryShippingCreatedRequest;
import com.bbb.core.model.request.inventory.create.InventoryUpgradeCompCreateRequest;
import com.bbb.core.model.request.inventory.update.InventoryTradeInOwnerUpdateRequest;
import com.bbb.core.repository.inventory.*;
import com.bbb.core.repository.market.InventoryLocationShippingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class InventoryManagerAsync {
    @Autowired
    private InventoryShippingRepository inventoryShippingRepository;
    @Autowired
    private InventoryUpgradeCompRepository inventoryUpgradeCompRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private InventoryCompDetailRepository inventoryCompDetailRepository;
    @Autowired
    private InventoryTradeInOwnerRepository inventoryTradeInOwnerRepository;
    @Autowired
    private InventoryCompTypeRepository inventoryCompTypeRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryLocationShippingRepository inventoryLocationShippingRepository;

    @Async
    public void createUpgradeComponent(long inventoryId, List<InventoryUpgradeCompCreateRequest> request) {
        if (request != null && request.size() > 0) {
            List<InventoryUpgradeComp> upComps = request.stream().map(up -> {
                InventoryUpgradeComp upgradeComp = new InventoryUpgradeComp();
                upgradeComp.setInventoryId(inventoryId);
                upgradeComp.setName(up.getName());
                upgradeComp.setPercentValue(up.getPercentValue());
                return upgradeComp;
            }).collect(Collectors.toList());
            inventoryUpgradeCompRepository.saveAll(upComps);
        }
    }

    @Async
    public void createInventoryShipping(long inventoryId, InventoryShippingCreatedRequest request) {
        InventoryShipping shipping = new InventoryShipping();
        shipping.setInventoryId(inventoryId);
        shipping.setTrackingInBound(request.getTrackingInbound());
        shipping.setCarrier(request.getCarrier());
        if (request.getCarrier() != null) {
            if (request.getCarrier().toUpperCase().equals(CarrierType.UPS.name())) {
                shipping.setCarrierType(CarrierType.UPS);
            } else {
                if (request.getCarrier().toUpperCase().equals(CarrierType.FEDEX.name())) {
                    shipping.setCarrierType(CarrierType.FEDEX);
                }
            }
        }
        shipping.setShippingLocationInBound(request.getShippingLocationInBound());
        shipping.setStatusInBound(request.getStatusInBound());
        shipping.setTrackingDateInBound(request.getTrackingDateInBound());
        shipping.setShippingType(request.getShippingType());
        shipping.setTrackingOutBound(request.getTrackingOutBound());
        shipping.setStatusOutBound(request.getStatusOutBound());
        shipping.setShippingLocationOutBound(request.getShippingLocationOutBound());
        shipping.setTrackingDateOutBound(request.getTrackingDateOutBound());
        shipping.setStatusCarrier(request.getStatusCarrier());
        inventoryShippingRepository.save(shipping);
    }

    @Async
    public void createInventoryLocation(
            long inventoryId,
            InventoryLocationShippingCreateRequest request,
            GoogleServiceResponse googleResponse) {
        AddressComponent country = GoogleServiceUtils.getCountryAddress(googleResponse);
        AddressComponent state = GoogleServiceUtils.getAddressLevel1(googleResponse);
        AddressComponent city = GoogleServiceUtils.getCity(googleResponse);
        AddressComponent county = GoogleServiceUtils.getAddressLevel2(googleResponse);

        InventoryLocationShipping inventoryLocationShipping = new InventoryLocationShipping();
        inventoryLocationShipping.setInventoryId(inventoryId);
        inventoryLocationShipping.setInsurance(request.getInsurance());
        inventoryLocationShipping.setZipCode(request.getZipCode());
        inventoryLocationShipping.setFlatRate(request.getFlatRate());
        inventoryLocationShipping.setAllowLocalPickup(request.getAllowLocalPickup());
        inventoryLocationShipping.setShippingType(request.getShippingType());
        inventoryLocationShipping.setCountryName(country.getLongName());
        inventoryLocationShipping.setCountryCode(country.getShortName());
        inventoryLocationShipping.setStateName(state.getLongName());
        inventoryLocationShipping.setStateCode(state.getShortName());
        inventoryLocationShipping.setCityName(city.getLongName());
        inventoryLocationShipping.setAddressLine(request.getAddressLine());
        if (county != null) {
            inventoryLocationShipping.setCounty(county.getLongName());
        }
        try {
            if (googleResponse.getResults().size() > 0) {
                GeometryLocation geometryLocation = googleResponse.getResults().get(0).getGeometry().getLocation();
                inventoryLocationShipping.setLatitude(geometryLocation.getLat());
                inventoryLocationShipping.setLongitude(geometryLocation.getLng());
            }
        } catch (NullPointerException e) {
            //no geometry location from google map
        }

        inventoryLocationShippingRepository.save(inventoryLocationShipping);
    }

    @Async
    public void deleteInventoryImageAsync(List<InventoryImage> inventoryImages) {
        inventoryImageRepository.deleteAll(inventoryImages);
    }


    public void updateInventoryCompsDetail(Inventory inventory, List<InventoryCompDetailCreateRequest> comps) {
        if (comps == null || comps.size() < 1) return;
//        inventoryCompDetailRepository.deleteAllByInventoryId(inventory.getId());
        createCompsSync(inventory.getId(), comps);
        //update price via component type
        Float valuePrice = null;
        if (inventory.getOverrideTradeInPrice() != null) {
            valuePrice = inventory.getOverrideTradeInPrice();
        } else {
            if (inventory.getBbbValue() != null) {
                valuePrice = inventory.getBbbValue();
            }
        }
        if (valuePrice == null) {
            return;
        }

        float additionalValue = 0;
        List<InventoryCompType> inventoryCompTypes = inventoryCompTypeRepository.findAllByInventoryIdId(inventory.getId());
        if (inventoryCompTypes.size() > 0) {
            for (InventoryCompType inventoryCompType : inventoryCompTypes) {
                additionalValue += inventoryCompType.getAdditionalValue();
            }
        }
        inventory.setInitialListPrice((valuePrice + additionalValue) / 0.6f);
        inventory.setDiscountedPrice((valuePrice + additionalValue) / 0.6f);
        inventory.setCurrentListedPrice(inventory.getDiscountedPrice() + (inventory.getFlatPriceChange() == null ? 0 : inventory.getFlatPriceChange()));
        inventory.setCogsPrice(valuePrice + additionalValue);
    }

    @Async
    public void updateInventoryUpgradeCompsAsync(long inventoryId, List<InventoryUpgradeCompCreateRequest> upgradeComps) {
        if (upgradeComps == null || upgradeComps.size() < 1) return;

        inventoryUpgradeCompRepository.deleteAllByInventoryId(inventoryId);

        createUpgradeComponent(inventoryId, upgradeComps);
    }

    @Async
    public void updateInventoryShipping(long inventoryId, InventoryShippingCreatedRequest request) {
        if (request == null) return;

        InventoryShipping inventoryShipping = inventoryShippingRepository.findByInventoryId(inventoryId);
        if (inventoryShipping == null) {
            inventoryShipping = new InventoryShipping();
        }

        if (request.getTrackingInbound() != null) {
            inventoryShipping.setTrackingInBound(request.getTrackingInbound());
        }
        if (request.getStatusInBound() != null) {
            inventoryShipping.setStatusInBound(request.getStatusInBound());
        }
        if (request.getShippingLocationInBound() != null) {
            inventoryShipping.setShippingLocationInBound(request.getShippingLocationInBound());
        }
        if (request.getTrackingDateInBound() != null) {
            inventoryShipping.setTrackingDateInBound(request.getTrackingDateInBound());
        }
        if (request.getTrackingOutBound() != null) {
            inventoryShipping.setTrackingOutBound(request.getTrackingOutBound());
        }
        if (request.getStatusOutBound() != null) {
            inventoryShipping.setStatusOutBound(request.getStatusOutBound());
        }
        if (request.getShippingLocationOutBound() != null) {
            inventoryShipping.setShippingLocationOutBound(request.getShippingLocationOutBound());
        }
        if (request.getShippingLocationOutBound() != null) {
            inventoryShipping.setShippingLocationOutBound(request.getShippingLocationOutBound());
        }
        if (request.getCarrier() != null) {
            inventoryShipping.setCarrier(request.getCarrier());
            if (request.getCarrier().toUpperCase().equals(CarrierType.UPS.name())) {
                inventoryShipping.setCarrierType(CarrierType.UPS);
            } else {
                if (request.getCarrier().toUpperCase().equals(CarrierType.FEDEX.name())) {
                    inventoryShipping.setCarrierType(CarrierType.FEDEX);
                }
            }
        }
        if (request.getShippingType() != null) {
            inventoryShipping.setShippingType(request.getShippingType());
        }
        if (request.getStatusCarrier() != null) {
            inventoryShipping.setStatusCarrier(request.getStatusCarrier());
        }

        inventoryShippingRepository.save(inventoryShipping);
    }

    @Async
    public void updateInventoryTradeInOwnerAsync(long inventoryId, InventoryTradeInOwnerUpdateRequest request) {
        if (request == null) return;

        InventoryTradeInOwner inventoryTradeInOwner = inventoryTradeInOwnerRepository.findByInventoryId(inventoryId);
        if (inventoryTradeInOwner == null) {
            inventoryTradeInOwner = new InventoryTradeInOwner();
        }

        if (request.getName() != null) {
            inventoryTradeInOwner.setName(request.getName());
        }
        if (request.getPhone() != null) {
            inventoryTradeInOwner.setPhone(request.getPhone());
        }
        if (request.getStreet() != null) {
            inventoryTradeInOwner.setStreet(request.getStreet());
        }
        if (request.getCity() != null) {
            inventoryTradeInOwner.setCity(request.getCity());
        }
        if (request.getState() != null) {
            inventoryTradeInOwner.setState(request.getState());
        }
        if (request.getPostCode() != null) {
            inventoryTradeInOwner.setPostCode(request.getPostCode());
        }
        if (request.getEmail() != null) {
            inventoryTradeInOwner.setEmail(request.getEmail());
        }
        if (request.getProofName() != null) {
            inventoryTradeInOwner.setProofName(request.getProofName());
        }
        if (request.getProofDate() != null) {
            inventoryTradeInOwner.setProofDate(request.getProofDate());
        }
        if (request.getTradedBikeId() != null) {
            inventoryTradeInOwner.setTradedBikeId(request.getTradedBikeId());
        }
        if (request.getTradedForBike() != null) {
            inventoryTradeInOwner.setTradedForBike(request.getTradedForBike());
        }

        inventoryTradeInOwnerRepository.save(inventoryTradeInOwner);
    }

    @Async
    public void createCompsAsync(long inventoryId, List<InventoryCompDetailCreateRequest> request, Action1<Long> action) {
        createCompsSync(inventoryId, request);
        if (action != null ) {
            action.call(inventoryId);
        }
    }


    public void createCompsSync(long inventoryId, List<InventoryCompDetailCreateRequest> request) {
        if (request != null && request.size() > 0) {
            List<InventoryCompDetail> comps = request.stream().map(comp -> {
                InventoryCompDetail cop = new InventoryCompDetail();
                InventoryCompDetailId id = new InventoryCompDetailId();
                id.setInventoryCompTypeId(comp.getInventoryCompTypeId());
                id.setInventoryId(inventoryId);
                cop.setId(id);
                cop.setValue(comp.getValue());
                return cop;
            }).collect(Collectors.toList());
            if (comps.size() > 0) {
                inventoryCompDetailRepository.saveAll(comps);
            }
        }
    }
}
