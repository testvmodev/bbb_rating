package com.bbb.core.model.response.tradein.detail.detailstep;

import com.bbb.core.model.database.TradeInCompDetail;
import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.request.tradein.tradin.DeclineTradeIn;
import com.bbb.core.model.response.bicycle.BicycleBaseInfo;
import com.bbb.core.model.response.bicycle.BicycleComponentResponse;
import com.bbb.core.model.response.bicycle.BicycleSpecResponse;
import com.bbb.core.model.response.tradein.TradeInUpgradeCompResponse;
import com.bbb.core.model.response.tradein.detail.TradeInOwner;
import com.bbb.core.model.response.tradein.detail.TradeInProof;

import java.util.List;

public class TradeInStepOneTwoResponse {
    private long tradeInId;
    private Float tradeValue;
    private ConditionInventory condition;
    private Boolean isClean;
    private Float eBikeMileage;
    private Float eBikeHours;
    private Long bicycleTypeId;
    private BicycleBaseInfo bicycleBaseInfo;
    private List<TradeInCompDetail> tradeInComponents;
    private List<BicycleComponentResponse> bicycleComponents;
    private List<BicycleSpecResponse> spec;

    private List<TradeInUpgradeCompResponse> upgradeComps;
    private TradeInOwner owner;
    private TradeInProof proof;
    private DeclineTradeIn decline;

    public long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public Float getTradeValue() {
        return tradeValue;
    }

    public void setTradeValue(Float tradeValue) {
        this.tradeValue = tradeValue;
    }

    public Boolean getClean() {
        return isClean;
    }

    public void setClean(Boolean clean) {
        isClean = clean;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public BicycleBaseInfo getBicycleBaseInfo() {
        return bicycleBaseInfo;
    }

    public void setBicycleBaseInfo(BicycleBaseInfo bicycleBaseInfo) {
        this.bicycleBaseInfo = bicycleBaseInfo;
    }

    public List<TradeInCompDetail> getTradeInComponents() {
        return tradeInComponents;
    }

    public void setTradeInComponents(List<TradeInCompDetail> tradeInComponents) {
        this.tradeInComponents = tradeInComponents;
    }

    public List<BicycleComponentResponse> getBicycleComponents() {
        return bicycleComponents;
    }

    public void setBicycleComponents(List<BicycleComponentResponse> bicycleComponents) {
        this.bicycleComponents = bicycleComponents;
    }

    public List<BicycleSpecResponse> getSpec() {
        return spec;
    }

    public void setSpec(List<BicycleSpecResponse> spec) {
        this.spec = spec;
    }

    public List<TradeInUpgradeCompResponse> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<TradeInUpgradeCompResponse> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public TradeInOwner getOwner() {
        return owner;
    }

    public void setOwner(TradeInOwner owner) {
        this.owner = owner;
    }

    public TradeInProof getProof() {
        return proof;
    }

    public void setProof(TradeInProof proof) {
        this.proof = proof;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public DeclineTradeIn getDecline() {
        return decline;
    }

    public void setDecline(DeclineTradeIn decline) {
        this.decline = decline;
    }

    public Long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(Long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }
}
