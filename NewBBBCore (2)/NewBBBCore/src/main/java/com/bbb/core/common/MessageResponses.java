package com.bbb.core.common;

public interface MessageResponses {
    //placeholder
    String ACTION = "{action}";




    //message
    String BICYCLE_NOT_EXIST = "Bicycle not exist";
    String BICYCLE_DELETED = "Bicycle deleted";
    String RETAIL_PRICE_MUST_POSITIVE = "Retail price must be  >= 0";
    String BICYCLE_NOT_ACTIVE = "Bicycle not active";
    String VALUE_INVALID = "Value is invalid";
    String NO_PRODUCTS_MATCH = "No products match";
    String THERE_IS ="There is a";
    String THERE_ARE = "There are ";
    String VALUE_NOT_EXIST = "Value not exist";
    String PAGE_OR_SIZE_INVALID = "Page number or size is invalid";
    String IMAGE_NOT_FORMAT = "Image is not properly formatted";
    String FILE_TOO_BIG = "File too big";


    String INVENTORY = "Inventory";

    String BRAND_NOT_EMPTY = "Brand not empty";
    String BRAND_NAME_NOT_EMPTY = "Brand name not empty";
    String BRAND_ID_NOT_EXIST = "BrandId not exist";
    String BRAND_ID_IS_REQUEST = "BrandId is required";
    String BRAND_NOT_EXIST = "Brand not exist";
    String BRAND_DELETED = "Brand deleted";
    String BRAND_NAME_EXIST ="Brand's name was existed";

    String MODEL_NOT_EMPTY = "Model not empty";
    String MODEL_NAME_NOT_EMPTY = "Model name not empty";
    String MODEL_ID_NOT_EXIST = "Model dose not exist";
    String MODEL_NAME_EXIST = "Model name existed";
    String MODEL_DELETED = "Model deleted";
    String MODEL_NOT_BELONG_BRAND = "Model not belong brand";
    String MODEL_NOT_EXIST = "Model not exist";


    String BRAND_MODEL_YEAR_NOT_EXIST = "brandId, modelId or yearId not exists";

    //Component category
    String COMPONENT_CATEGORY_NOT_EXIST = "Component category not exist";
    String CATEGORY_NAME_NOT_EMPTY  = "Component category name not empty";
    String CATEGORY_NAME_IS_EXIST = "Name of component category was exist";

    //Component Type
    String COMPONENT_TYPE_NAME_NOT_EMPTY = "Component type name not empty";
    String COMPONENT_CATEGORY_ID_NOT_EMPTY = "Component category id not empty";
    String COMPONENT_TYPE_NOT_EXIST = "Component type not exist";
    String COMPONENT_TYPE_EXIST = "Component type was exist";



    String COMPONENT_TYPE_NOT_SELECT = "Component type not select";
    String COMPONENT_TYPE_NOT_NULL = "Component type not null";
//    String COMPONENT_CATEGORY_NOT_EXIST = "Component category not exist";

    String COMPONENT_TYPE_VALUE_NOT_EXIST = "Component type value not exist";
    String COMPONENT_TYPE_VALUE_MUST_NOT_EMPTY = "Component type value mustn't empty";

    //inventory
    String INVENTORY_ID_NOT_EXIST = "Inventory id not exist";
    String INVENTORY_ID_NOT_EXIST_OR_STATUS_OR_STAGE_INVALID = "Inventory id not exist or status and stage are invalid";
//    String THIS_INVENTORY_ACCEPTED_OR_COMPLETED = "this inventory aceepted or completed";
    String INVENTORY_OF_SALESFORCE_NOT_FOUND = "Inventory from salesforce not found";


    String YEAR_NOT_EMPTY = "Year not empty";
    String YEAR_NAME_NOT_EMPTY = "Year name not empty";
    String YEAR_NAME_EXIST = "Year name exist";
    String YEAR_ID_NOT_EXIST = "yearId not exist";
    String YEAR_NOT_NULL = "Year not null";
    String YEAR_DELETED = "Year deleted";
    String YEAR_INVALID = "Year must be number";
    String YEAR_NOT_EXIST = "Year not exist";

    //rating
    String RATING_EXISTED= "Rating is already exist";
    String RATING_ID_NOT_EXIST = "Rating does not exist!";
    String INVENTORY_AUCTION_ID_AND_MARKETLISTING_ID_DO_NOT_EMPTY = "InventoryAuctionId or MarketListingId do not empty!!!";

    //type
    String TYPE_ID_NOT_EXIST = "Type id not exist";
    String TYPE_NAME_NOT_NULL = "Type name not null";
    String TYPE_NAME_IS_EXIST = "Type name is exist";
    String TYPE_DELETED = "Type deleted";
    String TYPE_NOT_EXIST ="Type not exist";


    String BICYCLE_TYPE_NOT_EXIST = "Bicycle type does not exist";


    String VALUE_MODIFIER_INVALID = "Value modifier is invalid";

    String INVENTORY_TYPE_NOT_EXIST = "Inventory type not found";
    String INVENTORY_DELETED = "Inventory deleted";

    //inventory type
    String INVENTORY_TYPE_ID_NOT_EXIST = "Inventory type not exist";

    //using this object
    String MODEL_USING_OBJECT = " model(s) using this object";
    String COMPONENT_TYPE_USING_OBJECT = " component type(s) using this object ";
    String TYPE_USING_OBJECT = " type(s) using this object ";
    String BRAND_USING_OBJECT = " brand(s) using this object";
    String BICYCLE_COMPONENT_USING_OBJECT = " bicycle component(s) using this object";
    String BICYCLE_USING_OBJECT = " bicycle(s) using this object";
    String COMPONENT_TYPE_SELECT_USING_OBJECT = " component type select(s) using this object";
    String BICYCLE_IMAGE_USING_OBJECT = "image(s) using this object";

    String INVENTORY_PAYING = "Can not save because this item is being paid by someone";

    //inventory size
    String INVENTORY_SIZE_ID_NOT_EXIST = "Inventory size not exist";


    String OVERRIDE_BBB_VALUE_MUST_LESS_THAN_OVERRIDE_TRADE_IN = "Override trade in price mus less than trade in price";
    String STAGE_AND_STATUS_INVALID = "Invalid stage and status";



    String START_PRICE_MUST_LESS_OR_EQUAL_THAN_END_PRICE = "Start price must less or equal end price";
    String START_YEAR_MUST_LESS_OR_EQUAL_THAN_END_YEAR = "Start year must less or equal end year";

    //offer
    String OFFER_PRICE_MUST_MORE_THAN_ZERO = "Offer price must more than zero";
    String OFFER_ID_NOT_FOUND = "Offer not found";
    String OFFER_ID_MUST_BE_MARKET_LISTING = "Offer must be marketListing";
    String STATUS_OFFER_NOT_NULL = "Status offer not null";
    String OFFER_ID_NOT_FOUND_OR_YOU_DO_NOT_HAVE_PERMISSION = "offerId not found or you do not have permission";
    String STATUS_OFFER_INVALID = "Invalid status offer";
    String OFFER_DO_NOT_HAS_MARKET_LISTING = "Offer do not has marketListing";
    String YOU_CAN_NOT_OFFER_THIS_INVENTORY_BECAUSE_YOU_SELL_THIS_INVENTORY = "You can not offer this inventory because you sell this inventory";

    String TIME_INVALID = "Time is invalid";
    String CREATED_BY_ID_NOT_NULL = "createdById not null";
    String NAME_NOT_NULL = "name not null";
    String NAME_NOT_EMPTY = "name not empty";
    String END_DATE_AND_START_DATE_INVALID = "Invalid end date and start date";
    String START_DATE_MUST_MORE_THAN_CURRENT_TIME = "Start date must bigger current date";

    String OFFER_PRICE_MUST_MORE_THAN_MINIMUM_AUTO_ACCEPT_PRICE = "Offer price must >= min price";
    String BID_PRICE_MUST_MORE_THAN_HIGHEST_OFFER = "The bid price must be higher than the current highest price of ";
    String OFFER_PRICE_MUST_MORE_THAN_HIGHEST_OFFER = "The bid price must be higher than the current highest price of ";
    String BID_PRICE_MUST_MORE_THAN_MIN_PRICE = "Offer price must be greater min Price";

    String MARKET_LISTING_ACCEPTED_OR_COMPLETED = "Market listing accepted or completed";
    String OFFER_NOT_MARKET_LISTING = "Not an offer of market listing";
    String YOUR_MUST_TRANSMIT_COUNTED_PRICE = "You must transmit countedPrice";
    String COUNTED_PRICE_MUST_THAN_ZERO = "CountedPrice must be >= 0";

    String DEPRECATED_OFFER_API_INVALID_USAGE = "Using both deprecated and new API. Change to only new API";
    String DEPRECATED_OFFER_API_INVALID_USAGE_BOTH_ID = "Can use only either inventoryAuctionId or marketListingId";
    String DEPRECATED_OFFER_API_INVALID_USAGE_NO_ID = "Either inventoryAuctionId or marketListingId must not null";

    String CREATE_OFFER_ACCOUNT_NOT_ACTIVE = "Your account is not active";

    String CANT_SAVE_OFFER_SALE_PENDING = "Can not " + ACTION + " because this item is being paid by someone";

    String REASON_CANCEL_REQUIRED = "Reason cancel is required";

    //auction
    String AUCTION_ID_NOT_FOUND = "Auction id not found";
    String AUCTION_WAS_BEGINNING = "Start date must smaller End date";
    String AUCTION_HAS_END = "The auction has ended";
    String AUCTION_EXPIRED = "Auction expired";
    String AUCTION_PENDING = "Auction pending";
    String AUCTION_CLOSED = "Auction closed";
    String AUTION_NAME_INVALID = "Invalid auction name";
    String STATUS_AUCTION_INVALID = "Invalid status auction";

    //inventory auction
    String INVENTORY_AUCTION_NOT_EXIST = "Inventory auction does not exist";
    String INVENTORY_AUCTION_DELETED = "Inventory auction deleted";
    String INVENTORY_AUCTION_IN_BID_CAN_NOT_UPDATE = "Inventory auction in bid, can not update";
    String BUY_IT_NOW_MUST_POSITIVE = "Buy it now value must > 0";
    String BUY_IT_NOW_MUST_NOT_EMPTY = "Buy it now must not empty";
    String BUY_IT_NOW_MUST_HIGHER_THAN_MIN = "Buy it now value must > min";
    String INVENTORY_INTO_MARKET_LISTING = "Inventory is being into market listing";
    String INVENTORY_AUCTION_NOT_ACTIVE = "Inventory auction is not active";
    String INVENTORY_NOT_INTO_AUCTION = "This inventory is not auctioned";

    //bicycle size
    String BICYCLE_SIZE_NOT_EXIST = "Bicycle size not exist";


    //trade in
    String TRADE_IN_NOT_EXIST = "Trade-In not exist";
    String TRADE_COMPLETED = "Trade-In completed";
    String TRADE_DECLINED = "Trade-In declined";
    String CLEAN_MUST_NOT_EMPTY = "Clean must not be empty";
    String TRADE_IN_IMAGE_TYPE_NOT_EXIST = "Trade-In image type not exist";
    String STATUS_TRADE_IN_INVALID = "Status Trade-In invalid";
    String TRADE_IN_STATUS_MUST_IS_SHIPPING = "Trade in status must be Shipping";
    String STATUS_TRADE_IN_TRADE_IN_INVALID = "Status tradeIn invalid";
    String IMAGE_EXTENSION_NOT_SUPPORT = "Unsupported image extension";
    String IMAGE_NOT_FOUND = "Image not found";
    String OWNER_NAME_MUST_IS_NOT_NULL = "Owner name must be not null";
    String OWNER_ADDRESS_MUST_IS_NOT_NULL = "Owner address must be not null";
    String OWNER_CITY_MUST_IS_NOT_NULL = "Owner city must be not null";
    String OWNER_STATE_MUST_IS_NOT_NULL = "Owner state must be not null";
    String OWNER_ZIPCODE_MUST_POSITIVE = "Owner ZipCode be invalid";
    String OWNER_EMAIL_MUST_IS_NOT_NULL = "Owner email must be not null";
    String OWNER_PHONE_MUST_IS_NOT_NULL = "Owner phone must be not null";
    String OWNER_LICENCES_PASSPORT_MUST_IS_NOT_NULL = "Owner licence or passport must be not null";
    String OWNER_SERIAL_MUST_IS_NOT_NULL = "Owner serial must be not null";
    String PROOF_NAME_MUST_IS_NOT_NULL = "Proof name must be not null";
    String PROOF_DATE_MUST_IS_NOT_NULL = "Proof date must be not null";
    String SELF_CARRIER_MUST_IS_NOT_EMPTY = "Carrier must be not empty";
    String SELF_NUMBER_TRACKING_MUST_IS_NOT_EMPTY = "trackingNumber must be not empty";
    String LENGTH_MUST_IS_NOT_EMPTY_AND_POSITIVE = "Length must be not null and > 0";
    String WIDTH_MUST_IS_NOT_EMPTY_AND_POSITIVE = "Width must be not null and > 0";
    String HEIGHT_MUST_IS_NOT_EMPTY_AND_POSITIVE = "Height must be not null and > 0";
    String WEIGHT_MUST_IS_NOT_EMPTY_AND_POSITIVE = "Weight must be not null and > 0";
    String FROM_ADDRESS_MUST_IS_NOT_EMPTY = "Address must be not empty";
    String FROM_CITY_MUST_IS_NOT_EMPTY = "City must be not empty";
    String FROM_STATE_MUST_IS_NOT_EMPTY = "State must be not empty";
    String FROM_ZIP_CODE_MUST_IS_NOT_EMPTY = "ZipCode must be not null";
    String SCHEDULE_DATE_MUST_NOT_EMPTY = "scheduleDate must not be empty";
    String MUST_ENOUGH_IMAGE_TYPE_REQUIRE = "Must upload the required images";
    String TRADE_IN_UPGRADE_COMP_INVALID = "TradeIn upgrade comp invalid";
    String UPGRADE_COMPONENT_INVALID = "Invalid upgrade/downgrade components";
    String TRADE_IN_VALUE_MUST_POSITIVE = "TradeIn value must be > 0";
    String TRADE_IN_VALUE_NOT_EMPTY = "TradeIn value must not empty";
    String SCORE_CARD_INVALID = "Scorecard is invalid";
    String RETAIL_PRICE__MUST_POSITIVE = "Retail price must be > 0";
    String TRADE_IN_VALUE__MUST_POSITIVE = "TradeIn value must be > 0";
    String STEP_TRADE_IN_INVALID = "Step tradeIn is invalid";
    String YOU_MUST_UPLOAD_IMAGE_REQUIRE = "You must upload all image require";
    String NOT_PARTNER_OR_NO_PHONE = "You are not Partner or your shop does not have phone";
    String EMPLOYEE_NAME_NOT_EMPTY = "Employee name must not empty";
    String EMPLOYEE_EMAIL_NOT_EMPTY = "Employee email must not empty";
    String EMPLOYEE_LOCATION_NOT_EMPTY = "Employee location must not empty";
    String OWNER_NAME_NOT_EMPTY = "Owner name must not empty";
    String UPLOAD_IMAGE_REQUIRED_BEFORE_SUBMIT = "You must upload all required image before submitting custom quote";
    String MAX_IMAGE_UPLOAD_EXCEED = "Image files exceed max limit";


    String IMAGE_MUST_NOT_EMPTY = "Image must not empty";
    String NAME_FILE_INVALID = "Name file is invalid";

    String UNKNOWN_ERROR_FROM_SHIPMENT_SERVICE = "Unable to create shipment";

    String CARRIER_NOT_SUPPORT = "Carrier is not supported";
    String SHIPPING_ACCOUNT_NOT_EXIST = "Shipping carrier account does not exist";
    String SHIPPING_ACCOUNT_ACTIVATED = "Account activated";
    String SHIPPING_ACCOUNT_DELETED = "Account deleted";
    String USERNAME_MUST_NOT_EMPTY = "Username must be not empty";
    String PASSWORD_MUST_NOT_EMPTY = "Password must be not empty";
    String API_PASSWORD_MUST_NOT_EMPTY = "API password must be not empty";
    String METER_NUMBER_MUST_NOT_EMPTY = "Meter number must be not empty";

    String TRADE_IN_SHIPPING_NOT_EXIST = "TradeIn shipping does not exist";
    String NO_VALID_TRACKING_NUMBDER = "Shipping does not have a valid tracking number";
    String FEDEX_TRACKING_NO_ID = "Fedex shipping does not have tracking id type";
    String FEDEX_ERROR_UNKNOWN_REASON = "Fedex error";

    //reason
    String REASON_NOT_EXIST = "Reason not exist";

    String INVALID_PARAM = "Invalid parameter";

    String INVENTORY_HAS_AUCTION = "Inventory has auction";

    String MIN_MUST_POSITIVE = "Min value mus >= 0";

    String FORMAT_VALUE_INVALID = "Invalid format value";

    String START_PRICE_MUST_POSITIVE_ZERO = "Start price must >= 0";
    String END_PRICE_MUST_POSITIVE_ZERO = "End price must >= 0";

    String SUCCESS = "Success";

    String MESSAGE_MUST_LOGIN = "You must login to access this function.";

    //inventory craete
    String MSR_PRICE_MUST_BE_POSITIVE = "msrPrice must be > 0";
    String COGS_PRICE_MUST_BE_POSITIVE = "cogsPrice must be > 0";
    String FLAT_PRICE_CHANGE_MUST_BE_POSITIVE = "flatPriceChange must be > 0";
    String PRICE_ROLE_MUST_BE_POSITIVE = "priceRole must be > 0";
    String STOCKE_LOCATION_ID_NOT_EXIST = "Stock location not found";
    String CARRIER_MUST_BE_NOT_NULL = "carrier must not be null";
    String PERCENT_VALUE_MUST_BE_POSITIVE = "Percent value must be > 0";
    String COMPONENT_INVALIDATE = "Component invenlide";
    String MUST_NOT_BE_EMPTY = "must not be empty";
    String CAN_NOT_CREATE_INVENTORY = "Can not create inventory";
    String STATUS_INVENTORY_INVALID = "Invalid status inventory";
    String INVENTORY_TYPE_NOT_FOUND = "Inventory type not found";

    //model family
    String YEAR_ID_MUST_BE_NOT_NULL = "YearId must not be null";

    //custom quote
    String YEAR_BRAND_MODEL_MUST_BE_NOT_NULL = "Year, brand, model must not null";
    String NOT_FOUND_BRAND_MODEL_YEAR = "Not found brand or model or year";
    String NOT_FOUND_CUSTOM_QUOTE = "Not found custom quote";
    String YOU_CAN_NOT_UPDATE_SUMMARY = "You can not update summary";
    String TRADE_IN_INVALID_CUSTOM_QUOTE_STATUS = "TradeIn invalid custom quote status";
    String TRADE_IN_CUSTOM_QUOTE_NOT_FOUND = "TradeIn custom quote not found";
    String ROLE_INVALID = "Invalid role";
    String YOU_MUST_CHOOSE_YEAR_ID_OR_YEAR_NAME = "You must choose yearId or Year name";
    String YOU_MUST_CHOOSE_BRAND_ID_OR_BRAND_NAME = "You must choose BrandId or Brand name";
    String YEAR_NAME_INVALID = "Invalid year name";
    String COMPONENT_TYPE_INVALID = "Invalid component type";
    String VALUE_COMPONENT_TYPE_INVALID = "Invalid component type";
    String BICYCLE_ID_MUST_BE_NOT_NULL = "BicycleId must not be null";
    String CONDITION_MUST_BE_NOT_NULL = "Condition must not be null";
    String VALUE_MUST_BE_NOT_NULL = "Value must not be null";
    String PRIVATE_PARTY_VALUE_MUST_BE_POSITIVE = "Private party value must be positive";
    String MISSING_REQUIRED_COMPONENTS = "Missing required component(s)";
    String COMPONENT_ID_MUST_NOT_NULL = "Component id must not be empty";
    String COMPONENT_ID_MUST_NOT_DUPLICATE = "Component id must not be duplicate";

    //inventory detail
    String SUSPENSION_MUST_NOT_EMPTY = "Suspension is required";
    String EBIKE_MILEAGE_MUST_BE_POSITIVE = "E-Bike mileage must not be < 0";
    String EBIKE_HOUR_MUST_BE_POSITIVE = "E-Bike hours must not be < 0";

    //market
    String MARKET_PLACE_CONFIG_NOT_EXIST = "Market place does not exist";
    String MARKET_PLACE_DELETED = "Deleted market place";
    String MARKET_PLACE_NOT_EXIST = "Market place does not exist";
    String MARKET_PLACE_CONFIG_DELETED = "Deleted market place configuration";
    String INVENTORY_ID_MUST_DUPLICATE = "Inventory must not duplicate";
    String INVENTORY_MUST_NOT_BE_PTP = "Inventory must not be PTP";
    String STATUS_MARKET_LISTING_INVALID = "Invalid status market listing";
    String INVENTORY_TYPE_INVALID = "Invalid inventory type";
    String MARKET_PLACE_INVALID = "Invalid market place";
    String MARKET_LISTING_NOT_EXIST = "Market listing not exist";
    String MARKET_LISTING = "Market listing";
    String DELETED = "delete";
    String YOU_CAN_NOT_BUY_THIS_MARKET_LISTING = "You can not buy this market listing";
    String ID_MUST_BE_UNIQUE = "Id must be unique";
    String ID_NOT_EXIST = "Id not exist";
    String TYPE_TABLE_INVALID = "Invalid type table";
    String TYPE_OPERATOR_INVALID = "Invalid type operator";
    String INVENTORY_IS_BEING_AUCTION = "Inventory is being auction";
    String YOUR_MUST_TRANSMIT_OFFER_ID_OR_MARKET_LISTING_ID = "You must transmit offerId or marketListingId";

    String MARKET_NAME_MUST_NOT_EMPTY = "Market name must not empty";
    String MARKET_CONFIG_NAME_MUST_NOT_EMPTY = "Market configuration name must not empty";
    String MARKET_CONFIG_ALREADY_EXISTED = "Configuration of this market place have been already existed";
    String MARKET_PLACE_ID_MUST_NOT_NULL = "marketPlaceId must not null";
    String EBAY_APP_ID_MUST_NOT_EMPTY = "ebayAppId must not empty";
    String EBAY_CERT_ID_MUST_NOT_EMPTY = "ebayCertId must not empty";
    String EBAY_DEV_ID_MUST_NOT_EMPTY = "ebayDevId must not empty";
    String EBAY_TOKEN_MUST_NOT_EMPTY = "ebayToken must not empty";
    String EBAY_API_SANDBOX_MUST_NOT_NULL = "isEbayApiSandbox must not null";
    String BRAND_ONLY_IS_ID_OR_NAME = "Brand must be either id or name";
    String MODEL_ONLY_IS_ID_OR_NAME = "Model must be either id or name";
    String YEAR_ONLY_IS_ID_OR_NAME = "Year must be either id or name";
    String COMPONENT_VALUE_MUST_BE_NOT_BLANK = "Component value must not be blank";
    String TRADE_IN_STATUS_INVALID = "Invalid tradeIn status";
    String TRADE_IN_CANCELED = "TradeIn canceled";
    String LIST_REQUEST_NOT_EMPTY = "List request not empty";
    String SHIPPING_TYPE_MUST_NOT_EMPTY = "ShippingType must not empty";


    //favourite
    String MARKET_LISTING_ID_MUST_NOT_NULL = "marketListingId must not null";
    String INVENTORY_AUCTION_ID_MUST_NOT_NULL = "inventoryAuctionId must not null";
    String UNKNOWN_FAVOURITE_TYPE = "Unknown favourite type";
    String ALREADY_FAVOURITE = "Already favourite item";
    String NOT_FAVOURITE = "Item does not exist or not favourite";


    String EMAIL_INVALID = "Email is invalid";
    String YOU_DONT_HAVE_PERMISSION = "You don't have permission to access this resource.";
    String CURRENT_LISTED_PRICE_INVALID = "CurrentListedPrice is invalid";
    String PARTNER_ID_INVALID_OR_MUST_BE_NOT_EMPTY_PARTNER = "PartnerId is invalid or must not be empty partner";
    String BBB_VALUE_MUST_BE_POSITIVE = "BBB value must be > 0";
    String IF_HAS_SUB_CATEGORY_YOU_MUST_HAS_PRIMARY_CATEGORY = "If has sub category you must has primary category";
    String EBAY_CATEGORY_PRIMARY_INVALID = "Ebay category primary is invalid";
    String BBB_CATEGORY_PRIMARY_INVALID = "BBB category primary is invalid";
    String YOUR_MUST_BE_PARTNER = "You must be parter";
    String IS_BEST_OFFER_MUST_NOT_EMPTY = "isBestOffer  must not empty";
    String EBAY_LISTING_DURATION_INVALID = "Ebay listing duration is invalid";

    //market listing
    String FLAT_RATE_MUST_POSITIVE = "Flat rate price must positive";
    String YOU_MISS_MIN_PRICE_OR_BEST_PRICE = "You are missing min price or best price";
    String MIN_PRICE_MUST_BE_LESS_THAN_MAX_PRICE = "Min price must be less than max price";
    String MIN_PRICE_MUST_BE_HIGHER_THAN_ZERO = "Min price must be > 0";
    String MIN_OFFER_PRICE_MUST_LESS_THAN_SALE_PRICE = "Min offer price must < sale price";
    String BEST_OFFER_PRICE_MUST_EQUAL_OR_LESS_THAN_SALE_PRICE = "Best offer price must <= sale price";
    String DURATION_EBAY_NOT_EMPTY = "Duration ebay not empty";
    String INVENTORY_ACTIVE_IN_AUCTION = "Inventory active in auction";
    String INVENTORY_HAVE_NOT_IMAGE = "Inventory have not image";
    String LISTED_ON = "Listed on";
    String LISTED_ON_EBAY = "Listed on Ebay";

    String SALE_PRICE_MUST_POSITIVE = "Sale price must positive";
    String MY_LISTING_DRAFT_NOT_EXIST = "My listing draft does not exist";
    String MY_LISTING_DRAFT_SAVE_FAIL = "Could not save draft for my listing";
    String DRAFT_IMAGE_NOT_FOUND = "Draft image(s) not found";
    String EMPTY_DRAFT = "Can not save an empty draft";
    String SHIPPING_LOCATION_INVALID = "Invalid shipping location";
    String SHIPPING_COUNTRY_INVALID = "Invalid country";
    String SHIPPING_STATE_INVALID = "Invalid state";
    String SHIPPING_CITY_INVALID = "Invalid city";
    String SHIPPING_ZIP_CODE_INVALID = "Invalid zipcode";

    String INVENTORY_LOCATION_SHIPPING_NOT_FOUND = "Inventory location shipping not found";
    String INVENTORY_COMPONENT_NOT_FOUND = "Some component(s) are not found, id:";
    String NOT_ALLOW_DELETE_ALL_IMAGE = "Not allow to delete all images";

    String NOT_ALLOW_CHANGE_LISTING_STATUS = "Not allow to change to this listing status";
    String CANT_SAVE_LISTING_SALE_PENDING_OR_PAY_PENDING = "Someone is buying this item, can not " + ACTION;

    //job queue
    String SAFE_SHUTDOWN_SUCCESS = "All jobs done. Safe to shutdown now";
    String DEQUEUE_JOB_FAIL = "Something went wrong, could not dequeue job";
    String DUPLICATE_JOB = "Queue same jobs or jobs having same name are not allowed";
    String JOB_CONFIG_NOT_EXIST = "Job configuration does not exist";
    String JOB_CONFIG_OUTDATED = "Unknown job configuration";
    String SECOND_INVALID = "Invalid second value";
    String MINUTE_INVALID = "Invalid minute value";
    String HOUR_INVALID = "Invalid hour value";
    String DAY_OF_WEEK_INVALID = "Invalid day of week value";
    String DAY_OF_MONTH_INVALID = "Invalid day of month value";
    String MONTH_INVALID = "Invalid month value";
    String CRON_EXPRESSION_NOT_EMPTY = "Cron expression must not empty";
    String NO_VALID_CRON = "Either fullCronString or buildCronSupport must be provided";
    String JOB_TIME_SECOND_INVALID = "Job time seconds must >= 60";
    String JOB_RUNNING_SAME_TIME = "Duplicate running job at same time";


    String DELETE_ACCOUNT_INVALID_ROLE = "This operation does not support for this user role";
    String ACCOUNT_DELETED = "Account deleted";
    String ACCOUNT_INVALID_STATUS = "Invalid account status";


    String SHIPPING_FEE_CONFIG_ALREADY_EXISTED = "Already existed shipping fee config for this bicycle type";
    String SHIPPING_FEE_CONFIG_NOT_EXIST = "Shipping fee config does not exist";

    String INVALID_LOCATION_INFO_NON_PTP = "shippingType, isAllowLocalPickup, flatRate are only PTP inventory type";



    String SALE_INFO_NOT_FOUND = "Sale info of market listing is not found";
    String SHIPPING_METHOD_LOCAL_PICKUP = "Shipping method is local pickup";
    String MANUAL_SHIPPING_REQUIRED = "Shipping carrier and tracking number is required";
    String P2P_ALREADY_SHIP = "Listing was already shipped";


    String INVALID_TOKEN = "Invalid JWT token";


    String FROM_DAY_CANT_HIGHER_THAN_TO_DAY = "Start day must not be higher than end day";


    String CANT_ACCESS_URL = "Invalid url or can't be accessed";

    //test
    String TEST_OFFER_DELETED = "Unfortunately, offer for testing was deleted";



    String YOUR_MUST_TRANSMIT_MARKET_LISTING_ID_QUEUE = "You must transmit marketListing queue";
    String ALL_STATUS_MARKET_MUST_BE_QUEUE = "All status market listing must be queue";
    String TYPE_OF_INVENTORY_MUST_BE_NOT_EMPTY = "Type of inventory must not be empty";
    String TYPE_NAME_INVENTORY = "Type name inventory";
    String SHIPPING_CONFIG_FEE_INVENTORY = "Shipping config fee inventory";
    String INVALID = "Invalid";
    String CONFIG_SHIPPING_TYPE_FEE_IS_EMPTY = "Config shipping fee type is empty";
}
