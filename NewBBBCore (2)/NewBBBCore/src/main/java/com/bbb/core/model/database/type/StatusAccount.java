package com.bbb.core.model.database.type;

public enum StatusAccount {
    ACTIVE("Active"),
    INACTIVE("Inactive"),
    DELETED("Deleted");

    private String value;

    StatusAccount(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StatusAccount findByValue(String value) {
        if (value == null) return null;

        for (StatusAccount statusAccount : StatusAccount.values()) {
            if (statusAccount.getValue().equals(value)) {
                return statusAccount;
            }
        }
        return null;
    }
}
