package com.bbb.core.security.authen.usercontext;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.security.authen.extractor.TokenExtractor;
import com.bbb.core.security.authen.jwt.JwtAuthenticationToken;
import com.bbb.core.security.authen.jwt.JwtTokenFactory;
import com.bbb.core.security.exception.JwtExpiredTokenException;
import com.bbb.core.security.user.UserContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class UserContextManager {
    private static final Logger LOG = LoggerFactory.getLogger(UserContextManager.class);
    @Autowired
    private TokenExtractor tokenExtractor;
    @Autowired
    private JwtTokenFactory jwtTokenFactory;

    public UserContext getUserContext(HttpServletRequest httpServletRequest) throws ExceptionResponse {
        String tokenPayload = httpServletRequest.getHeader(Constants.HEADER_NAME_AUTH);
        LOG.info("attemptAuthentication tokenPayload: " + tokenPayload);
        if (!StringUtils.isBlank(tokenPayload) && tokenPayload.contains(Constants.HEADER_PREFIX)) {
            try {
                String token = tokenExtractor.extract(tokenPayload);
                if (!StringUtils.isBlank(token)) {
                    return new JwtAuthenticationToken(jwtTokenFactory.parseToken(token)).getCredentials();
                }
            } catch (AuthenticationServiceException ex) {
                ex.printStackTrace();
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, ex.getMessage()),
                        HttpStatus.UNAUTHORIZED
                );
            } catch (BadCredentialsException | JwtExpiredTokenException e) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, e.getMessage()),
                        HttpStatus.UNAUTHORIZED
                );
            }
        }

        return null;
    }
}
