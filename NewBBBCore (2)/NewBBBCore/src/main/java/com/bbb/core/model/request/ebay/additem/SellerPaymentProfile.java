package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SellerPaymentProfile {
    @JacksonXmlProperty(localName = "PaymentProfileID")
    private long paymentProfileID;

    public long getPaymentProfileID() {
        return paymentProfileID;
    }

    public void setPaymentProfileID(long paymentProfileID) {
        this.paymentProfileID = paymentProfileID;
    }
}
