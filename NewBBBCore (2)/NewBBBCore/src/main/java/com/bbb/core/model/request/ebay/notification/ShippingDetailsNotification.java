package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.request.ebay.additem.ShippingDetailsRequest;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShippingDetailsNotification extends ShippingDetailsRequest {
    @JacksonXmlProperty(localName = "SellingManagerSalesRecordNumber")
    private int sellingManagerSalesRecordNumber;
    @JacksonXmlProperty(localName = "ThirdPartyCheckout")
    private boolean thirdPartyCheckout;
    @JacksonXmlProperty(localName = "TaxTable")
    private TaxTable taxTable;
    @JacksonXmlProperty(localName = "GetItFast")
    private boolean itFast;

    public int getSellingManagerSalesRecordNumber() {
        return sellingManagerSalesRecordNumber;
    }

    public void setSellingManagerSalesRecordNumber(int sellingManagerSalesRecordNumber) {
        this.sellingManagerSalesRecordNumber = sellingManagerSalesRecordNumber;
    }

    public boolean isThirdPartyCheckout() {
        return thirdPartyCheckout;
    }

    public void setThirdPartyCheckout(boolean thirdPartyCheckout) {
        this.thirdPartyCheckout = thirdPartyCheckout;
    }

    public TaxTable getTaxTable() {
        return taxTable;
    }

    public void setTaxTable(TaxTable taxTable) {
        this.taxTable = taxTable;
    }

    public boolean isItFast() {
        return itFast;
    }

    public void setItFast(boolean itFast) {
        this.itFast = itFast;
    }
}
