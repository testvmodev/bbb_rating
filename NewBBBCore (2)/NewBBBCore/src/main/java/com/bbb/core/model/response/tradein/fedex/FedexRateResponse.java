package com.bbb.core.model.response.tradein.fedex;

import com.bbb.core.common.fedex.FedExValue;
import com.bbb.core.model.response.tradein.fedex.detail.Money;
import com.bbb.core.model.response.tradein.fedex.detail.Notification;
import com.bbb.core.model.response.tradein.fedex.detail.type.Severity;
import com.bbb.core.model.response.tradein.fedex.detail.rate.RateReplyDetails;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "ProcessShipmentReply", namespace = FedExValue.RATE_ROOT_NAMESPACE)
public class FedexRateResponse {
    @JacksonXmlProperty(localName = "HighestSeverity")
    private String highestSeverityRaw;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Notifications")
    private List<Notification> notifications;
    @JacksonXmlProperty(localName = "RateReplyDetails")
    private RateReplyDetails rateReplyDetails;

    public Severity getHighestSeverity() {
        return Severity.findByValue(highestSeverityRaw);
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public RateReplyDetails getRateReplyDetails() {
        return rateReplyDetails;
    }

    public void setHighestSeverityRaw(String highestSeverityRaw) {
        this.highestSeverityRaw = highestSeverityRaw;
    }

    public boolean isSuccess() {
        try {
            return getHighestSeverity() == Severity.SUCCESS;
        } catch (Exception e) {
            return false;
        }
    }

    public Money getTotalCharge() {
        return rateReplyDetails.getRatedShipmentDetails().getShipmentRateDetail().getTotalCharges();
    }
}
