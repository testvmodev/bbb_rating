package com.bbb.core.model.request.sort;

public enum InventoryAuctionSort {
    MAX_PRICE_OFFER("max_price_offer"),
    DURATION_AUCTION("duration_auction");
    private final String value;

    InventoryAuctionSort(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
