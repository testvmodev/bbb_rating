package com.bbb.core.manager.schedule.jobs;

import com.bbb.core.manager.schedule.ScheduleManager;
import com.bbb.core.model.database.ScheduleJobConfig;
import com.bbb.core.repository.schedule.ScheduleJobConfigRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public abstract class ScheduleJob implements Runnable, Trigger {
    @Autowired
    protected ScheduleManager scheduleManager;
    @Autowired
    private ScheduleJobConfigRepository scheduleJobConfigRepository;
    private ScheduleJobConfig config;
    public boolean isRunning = false;
    protected boolean forcedRun;
    private boolean isFirstRun = false;
    private int firstRunDelay = 0; //seconds
    public LocalDateTime startTime;
    public LocalDateTime finishTime;
    public LocalDateTime failTime;
    public LocalDateTime cancelTime;
    protected boolean isFail;
    private LocalDateTime lastQueueTime;
    private LocalDateTime lastQueueToRunAt;
    private String customLogString;
    private Float customLogNumber;
    private Trigger trigger;
    private TriggerContext lastTriggerContext;

    @Override
    public final Date nextExecutionTime(TriggerContext triggerContext) {
        lastTriggerContext = triggerContext;
        if (ScheduleManager.isShuttingDown() || config == null) return null; //when shutting down, wont queue new task
        try {
            if (trigger == null) return null;

            lastQueueTime = LocalDateTime.now();
            Date queueToRunAt;
            if (forcedRun) {
                forcedRun = false;
                queueToRunAt = new Date();
            } else {
                queueToRunAt = trigger.nextExecutionTime(triggerContext);
            }
            lastQueueToRunAt = LocalDateTime.fromDateFields(queueToRunAt);
            if (isFirstRun) {
                lastQueueToRunAt.plusSeconds(firstRunDelay);
                isFirstRun = false;
            }

            return lastQueueToRunAt.toDate();
        } catch (Exception e) {
            return null; //never queue
        }
    }

    /**
     * When an unhandled exception was thrown
     * @param throwable
     */
    protected abstract void onFail(Throwable throwable);

    public void buildTrigger() {
        buildTrigger(0, false);
    }

    public void buildTrigger(int firstRunDelay, boolean forceRun) {
        if (config == null) return;
        this.firstRunDelay = firstRunDelay;
        forcedRun = forceRun;
        switch (config.getJobTimeType()) {
            case FIX_DELAY:
                if (config.getJobTimeSeconds() != null) {
                    trigger = new PeriodicTrigger(config.getJobTimeSeconds(), TimeUnit.SECONDS);
                }
                break;
            case FIX_RATE:
                if (config.getJobTimeSeconds() != null) {
                    trigger = new PeriodicTrigger(config.getJobTimeSeconds(), TimeUnit.SECONDS);
                    ((PeriodicTrigger) trigger).setFixedRate(true);
                }
                break;
            case CRON:
                if (!StringUtils.isBlank(config.getJobTimeCron()) &&
                        CronSequenceGenerator.isValidExpression(config.getJobTimeCron())
                ) {
                    trigger = new CronTrigger(config.getJobTimeCron());
                }
                break;
        }
    }

    public void disable() {
        if (config == null) {
            ScheduleJobConfig config = scheduleJobConfigRepository.findOne(this.getClass().getSimpleName());
        }
        if (config != null) {
            if (!config.isEnable()) return;
            scheduleManager.updateEnable(this, false);
            trigger = null;
            config.setEnable(false);
            config = scheduleJobConfigRepository.save(config);
        }
    }

    public void enable() {
        if (config == null) {
            ScheduleJobConfig config = scheduleJobConfigRepository.findOne(this.getClass().getSimpleName());
        }
        if (config != null) {
            if (config.isEnable()) return;
            scheduleManager.updateEnable(this, true);
            config.setEnable(true);
            config = scheduleJobConfigRepository.save(config);
        }
    }

    public final boolean isEnable() {
        return config != null && config.isEnable();
    }

    public final ScheduleJobConfig getConfig() {
        return config;
    }

    public final void setConfig(ScheduleJobConfig config) {
        this.config = config;
    }

    public String getCustomLogString() {
        return customLogString;
    }

    protected void setCustomLogString(String customLogString) {
        this.customLogString = customLogString;
    }

    public Float getCustomLogNumber() {
        return customLogNumber;
    }

    protected void setCustomLogNumber(Float customLogNumber) {
        this.customLogNumber = customLogNumber;
    }

    public boolean isFail() {
        return isFail;
    }

    protected void setFail(boolean isFail) {
        this.isFail = isFail;
    }

    public LocalDateTime getLastQueueTime() {
        return lastQueueTime;
    }

    public LocalDateTime getLastQueueToRunAt() {
        return lastQueueToRunAt;
    }
}
