package com.bbb.core.model.response.market.home.recentview;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RecentViewMarketListingResponse extends RecentViewResponse {
    @Id
    private long marketListingId;

    public long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(long marketListingId) {
        this.marketListingId = marketListingId;
    }
}
