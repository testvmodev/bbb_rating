package com.bbb.core.repository.market.out;

import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.sort.MyListingSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.market.marketlisting.UserMarketListingResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMarketListingResponseRepository extends JpaRepository<UserMarketListingResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS market_type, " +
            "market_listing.status AS status_market_listing, " +
            "market_listing.time_listed AS time_listed, " +

            "inventory.id AS inventory_id, " +
            "inventory.image_default AS image_default, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.title AS inventory_title, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +

            "inventory.bicycle_id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +

            "inventory.msrp_price AS msrp_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "market_listing.is_best_offer AS is_best_offer, " +

            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id " +

            "FROM market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id AND inventory.seller_id = :sellerId " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +

            "WHERE " +
            "market_listing.is_delete = 0 " +
            "AND (1 = :#{@queryUtils.isEmptyList(#listingStatuses) ? 1 : 0} OR " +
                "market_listing.status IN :#{@queryUtils.isEmptyList(#listingStatuses) ? @queryUtils.fakeStrings() : @queryUtils.mapListingStatusToString(#listingStatuses)}) " +


            "ORDER BY " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).ID.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.id END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.time_listed END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).POSTING_TIME.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.time_listed END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).TITLE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.title END DESC , " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END ASC , " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).PRICE.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN inventory.current_listed_price END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).STATUS.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.status END DESC, " +

            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} != :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END, " +
            "CASE WHEN (:#{#sortField.name()} = :#{T(com.bbb.core.model.request.sort.MyListingSortField).BEST_OFFER.name()} " +
                "AND :#{#sortType.name()} = :#{T(com.bbb.core.model.request.sort.Sort).DESC.name()}) " +
                "THEN market_listing.is_best_offer END DESC " +

            "OFFSET :#{#pageable.getOffset()} ROWS FETCH NEXT :#{#pageable.getPageSize()} ROWS ONLY "
    )
    List<UserMarketListingResponse> findAllBySeller(
            @Param("sellerId") String sellerId,
            @Param("listingStatuses") List<StatusMarketListing> listingStatuses,
            @Param("sortField") MyListingSortField sortField,
            @Param("sortType") Sort sortType,
            @Param("pageable") Pageable pageable
    );

    @Query(nativeQuery = true, value = "SELECT COUNT (*) " +
            "FROM market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id AND inventory.seller_id = :sellerId " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +

            "WHERE market_listing.is_delete = 0 " +
            "AND (1 = :#{@queryUtils.isEmptyList(#listingStatuses) ? 1 : 0} OR " +
            "market_listing.status IN :#{@queryUtils.isEmptyList(#listingStatuses) ? @queryUtils.fakeStrings() : @queryUtils.mapListingStatusToString(#listingStatuses)}) "
    )
    int countBySeller(
            @Param("sellerId") String sellerId,
            @Param("listingStatuses") List<StatusMarketListing> listingStatuses
    );
}
