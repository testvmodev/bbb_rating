package com.bbb.core.model.response.tradein.bicycle;

import com.bbb.core.model.database.TradeInPriceConfig;
import com.bbb.core.model.database.TradeInUpgradeCompModifier;
import com.bbb.core.model.response.bicycle.BicycleComponentResponse;
import com.bbb.core.model.response.bicycle.BicycleSpecResponse;
import com.bbb.core.model.response.tradein.TradeInBicycleYearResponse;
import com.bbb.core.model.response.tradein.TradeInUpgradeCompResponse;

import java.util.List;

public class TradeInBicycleDetailResponse {
    private long bicycleId;
    private Float msrp;
    private String imageDefault;
    private List<String> images;
    private List<BicycleComponentResponse> components;
    private List<BicycleSpecResponse> spec;
//    private float tradeInValue;
    private List<TradeInBicycleYearResponse> otherBicycles;
    private List<TradeInUpgradeCompResponse> upgradeComps;
    private List<TradeInUpgradeCompModifier> upgradeCompModifiers;
    private List<ConditionTradeInBicycleResponse> conditions;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public Float getMsrp() {
        return msrp;
    }

    public void setMsrp(Float msrp) {
        this.msrp = msrp;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<BicycleComponentResponse> getComponents() {
        return components;
    }

    public void setComponents(List<BicycleComponentResponse> components) {
        this.components = components;
    }

    public List<BicycleSpecResponse> getSpec() {
        return spec;
    }

    public void setSpec(List<BicycleSpecResponse> spec) {
        this.spec = spec;
    }

//    public float getTradeInValue() {
//        return tradeInValue;
//    }
//
//    public void setTradeInValue(float tradeInValue) {
//        this.tradeInValue = tradeInValue;
//    }

    public List<TradeInBicycleYearResponse> getOtherBicycles() {
        return otherBicycles;
    }

    public void setOtherBicycles(List<TradeInBicycleYearResponse> otherBicycles) {
        this.otherBicycles = otherBicycles;
    }

    public List<TradeInUpgradeCompResponse> getUpgradeComps() {
        return upgradeComps;
    }

    public void setUpgradeComps(List<TradeInUpgradeCompResponse> upgradeComps) {
        this.upgradeComps = upgradeComps;
    }

    public List<TradeInUpgradeCompModifier> getUpgradeCompModifiers() {
        return upgradeCompModifiers;
    }

    public void setUpgradeCompModifiers(List<TradeInUpgradeCompModifier> upgradeCompModifiers) {
        this.upgradeCompModifiers = upgradeCompModifiers;
    }

    public List<ConditionTradeInBicycleResponse> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionTradeInBicycleResponse> conditions) {
        this.conditions = conditions;
    }
}
