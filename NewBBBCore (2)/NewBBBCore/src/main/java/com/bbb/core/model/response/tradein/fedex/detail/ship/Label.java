package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Label {
    @JacksonXmlProperty(localName = "Type")
    private String type;
    @JacksonXmlProperty(localName = "ShippingDocumentDisposition")
    private String shippingDocumentDisposition;
    @JacksonXmlProperty(localName = "ImageType")
    private String imageType;
    @JacksonXmlProperty(localName = "CopiesToPrint")
    private int copiesToPrint;
    @JacksonXmlProperty(localName = "Parts")
    private LabelPart labelPart;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShippingDocumentDisposition() {
        return shippingDocumentDisposition;
    }

    public void setShippingDocumentDisposition(String shippingDocumentDisposition) {
        this.shippingDocumentDisposition = shippingDocumentDisposition;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public int getCopiesToPrint() {
        return copiesToPrint;
    }

    public void setCopiesToPrint(int copiesToPrint) {
        this.copiesToPrint = copiesToPrint;
    }

    public LabelPart getLabelPart() {
        return labelPart;
    }

    public void setLabelPart(LabelPart labelPart) {
        this.labelPart = labelPart;
    }
}
