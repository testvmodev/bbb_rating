package com.bbb.core.controller.salesforce;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.saleforce.ListingFullCreateRequest;
import com.bbb.core.model.request.saleforce.ListingFullUpdateRequest;
import com.bbb.core.service.salesforce.SalesforceService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class SalesforceController {
    @Autowired
    private SalesforceService service;

    @PostMapping(Constants.URL_MARKET_LISTING_SALESFORCE)
    public ResponseEntity createMarketListingListingSalesforce(
            @RequestBody @Valid ListingFullCreateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.createListingSalesforce(request));
    }

    @PutMapping(Constants.URL_MARKET_LISTING_SALESFORCE_PATH_ID)
    public ResponseEntity updateListingSalesforce(
            @ApiParam(value = "salesforce id")
            @PathVariable(Constants.ID) String salesforceId,
            @RequestBody ListingFullUpdateRequest request
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.updateListingSalesforce(salesforceId, request));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_SALESFORCE_PATH_ID)
    public ResponseEntity deListListingSalesforce(
            @PathVariable(Constants.ID) String salesforceId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.deListListingSalesforce(salesforceId));
    }

    @DeleteMapping(Constants.URL_MARKET_LISTING_SALESFORCE_SOLD)
    public ResponseEntity soldListingSalesforce(
            @PathVariable(Constants.ID) String salesforceId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.soldListingSalesforce(salesforceId));
    }
}
