package com.bbb.core.repository.tracking;

import com.bbb.core.model.database.Tracking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrackingRepository extends JpaRepository<Tracking, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM tracking WHERE " +
            "tracking.user_id = :userId AND " +
            "tracking.market_listing_id = :marketListingId ")
    Tracking findOneMarketListing(
            @Param(value = "userId") String userId,
            @Param(value = "marketListingId") long marketListingId
    );


    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM tracking WHERE " +
            "tracking.user_id = :userId AND " +
            "tracking.bicycle_id = :bicycleId ")
    Tracking findOneBicycle(
            @Param(value = "userId") String userId,
            @Param(value = "bicycleId") long bicycleId
    );


    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM tracking " +
            "WHERE user_id = :userId " +
            "AND inventory_auction_id = :inventoryAuctionId "
    )
    Tracking findOneAuction(
            @Param("userId") String userId,
            @Param("inventoryAuctionId") long inventoryAuctionId
    );

    @Query(nativeQuery = true, value = "SELECT * FROM tracking WHERE " +
            "tracking.user_id = :userId AND tracking.market_listing_id IN :marketListingIds")
    List<Tracking> findAllTrackingMarketListing(
            @Param(value = "userId") String userId,
            @Param(value = "marketListingIds") List<Long> marketListingIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM tracking WHERE " +
            "tracking.user_id = :userId AND tracking.inventory_auction_id IN :inventoryAuctionIds")
    List<Tracking> findAllTrackingInventoryAuction(
            @Param(value = "userId") String userId,
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds
    );

    @Query(nativeQuery = true, value = "SELECT * FROM tracking WHERE " +
            "tracking.user_id = :userId AND tracking.bicycle_id IN :bicycleIds")
    List<Tracking> findAllTrackingBicycle(
            @Param(value = "userId") String userId,
            @Param(value = "bicycleIds") List<Long> bicycleIds
    );
}
