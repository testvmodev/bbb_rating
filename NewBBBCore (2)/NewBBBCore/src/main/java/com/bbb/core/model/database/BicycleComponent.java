package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "bicycle_component")
public class BicycleComponent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long bicycleId;
    private long componentTypeId;
    private String componentValue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getComponentTypeId() {
        return componentTypeId;
    }

    public void setComponentTypeId(long componentTypeId) {
        this.componentTypeId = componentTypeId;
    }

    public String getComponentValue() {
        return componentValue;
    }

    public void setComponentValue(String componentValue) {
        this.componentValue = componentValue;
    }
}
