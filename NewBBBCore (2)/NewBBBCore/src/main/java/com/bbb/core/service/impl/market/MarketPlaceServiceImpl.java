package com.bbb.core.service.impl.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.market.MarketPlaceManager;
import com.bbb.core.model.request.market.MarketPlaceCreateRequest;
import com.bbb.core.service.market.MarketPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarketPlaceServiceImpl implements MarketPlaceService {
    @Autowired
    private MarketPlaceManager manager;

    @Override
    public Object getAllMarketPlaces() {
        return manager.getAllMarketPlaces();
    }

    @Override
    public Object getMarketPlaceDetail(long id) throws ExceptionResponse {
        return manager.getMarketPlaceDetail(id);
    }

    @Override
    public Object postMarketPlace(MarketPlaceCreateRequest request) throws ExceptionResponse {
        return manager.postMarketPlace(request);
    }

    @Override
    public Object updateMarketPlace(long id, MarketPlaceCreateRequest request) throws ExceptionResponse {
        return manager.updateMarketPlace(id, request);
    }

    @Override
    public Object deleteMarketPlace(long id) throws ExceptionResponse {
        return manager.deleteMarketPlace(id);
    }
}
