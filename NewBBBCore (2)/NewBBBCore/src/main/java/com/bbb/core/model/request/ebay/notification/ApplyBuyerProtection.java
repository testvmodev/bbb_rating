package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ApplyBuyerProtection {
    @JacksonXmlProperty(localName = "BuyerProtectionSource")
    private String buyerProtectionSource;
    @JacksonXmlProperty(localName = "BuyerProtectionStatus")
    private String buyerProtectionStatus;

    public String getBuyerProtectionSource() {
        return buyerProtectionSource;
    }

    public void setBuyerProtectionSource(String buyerProtectionSource) {
        this.buyerProtectionSource = buyerProtectionSource;
    }

    public String getBuyerProtectionStatus() {
        return buyerProtectionStatus;
    }

    public void setBuyerProtectionStatus(String buyerProtectionStatus) {
        this.buyerProtectionStatus = buyerProtectionStatus;
    }
}
