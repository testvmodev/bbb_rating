package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.request.tradein.tradin.DeclineTradeIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
public interface DeclineTradeInRepository extends JpaRepository<DeclineTradeIn, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "trade_in.bicycle_id AS bicycle_id, " +
            "trade_in_decline_detail.reason_decline_id AS reason_decline_id, " +
            "trade_in_decline_detail.comment AS comment, " +
            "trade_in_decline_detail.price_expected AS price_expected, " +
            "trade_in.value AS price_suggest, " +
            "trade_in.condition AS condition, " +
            "trade_in_reason_decline.reason AS reason_name " +
            "FROM " +
            "trade_in_decline_detail " +
            "JOIN trade_in ON trade_in_decline_detail.trade_in_id = trade_in.id " +
            "JOIN trade_in_reason_decline ON trade_in_decline_detail.reason_decline_id = trade_in_reason_decline.id " +
            "WHERE " +
            "trade_in_decline_detail.trade_in_id = :tradeInId")
    DeclineTradeIn findOne(
            @Param(value = "tradeInId") long tradeId
    );
}
