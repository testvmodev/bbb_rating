package com.bbb.core.model.response.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class Fee {
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlProperty(localName = "Fee")
    private Currency feeChild;
    @JacksonXmlProperty(localName = "PromotionalDiscount")
    private Currency promotionalDiscount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getFeeChild() {
        return feeChild;
    }

    public void setFeeChild(Currency feeChild) {
        this.feeChild = feeChild;
    }

    public Currency getPromotionalDiscount() {
        return promotionalDiscount;
    }

    public void setPromotionalDiscount(Currency promotionalDiscount) {
        this.promotionalDiscount = promotionalDiscount;
    }
}
