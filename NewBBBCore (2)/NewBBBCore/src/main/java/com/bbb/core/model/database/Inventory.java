package com.bbb.core.model.database;

import com.bbb.core.common.Constants;
import com.bbb.core.model.database.type.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

@Entity
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long bicycleId;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
    private String bicycleSizeName;
    private String description;
    private Long imageDefaultId;
    private String imageDefault;
    private StatusInventory status;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float msrpPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float overrideTradeInPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float initialListPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float currentListedPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float cogsPrice;
    @Column(columnDefinition = Constants.NUMERIC)
    private Float flatPriceChange;
    @Column(columnDefinition = Constants.NUMERIC)
    private float discountedPrice;
    @CreatedDate
    @Generated(GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private boolean isDelete;
    private String partnerId;
    private String sellerId;
    private StageInventory stage;
    private String serialNumber;
    @Generated(GenerationTime.INSERT)
    private String name;

    private Long listingDuration;
    private long typeId;

    private String title;
    private boolean isCustomQuote;
    private boolean isCreatePo;
    private Long age;
    private ConditionInventory condition;
    private long countTrackingView;

    private Long scoreCardId;

//    private Float weight;
    private Boolean isPreList;
    private Boolean isCheckInOverride;
    private LocalDate dayHold;
    private Boolean isOverrideCreatePo;
    private Boolean isNonComplianceNoti;
    private RefundReturnTypeInventory refundReturnType;
    private Float priceRole;
    private Float listingAge;
    private LocalDate firstListedDate;
    private LocalDate dateSold;
    private String masterMultiListing;
    private Boolean isReviseItem;
    private RecordTypeInventory recordType;
    private String partnerEmail;
    private String note;
    private String site;
    private Long stockLocationId;
    private LocalDateTime dateReceived;
    private Float conditionScore;
    private Float privatePartyValue;
    private Float valueAdditionalComponent;
    private Float bbbValue;
    private String searchGuideRecommendation;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime firstListedTime;
    private Float valueGuidePrice;
    private Float dealPercent;
    private Float eBikeMileage;
    private Float eBikeHours;
    private String storefrontId;
    private String salesforceId;
    private Boolean isOversized;
    @Column(name = "seller_is_bbb")
    private boolean sellerIsBBB;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getImageDefaultId() {
        return imageDefaultId;
    }

    public void setImageDefaultId(Long imageDefaultId) {
        this.imageDefaultId = imageDefaultId;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }


    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
        if ( currentListedPrice != null && valueGuidePrice != null ){
            dealPercent = currentListedPrice * 100 / valueGuidePrice;
        }
    }


    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    @Generated(GenerationTime.INSERT)
    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    @Generated(GenerationTime.INSERT)
    public void setName(String name) {
        this.name = name;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Long getListingDuration() {
        return listingDuration;
    }

    public void setListingDuration(Long listingDuration) {
        this.listingDuration = listingDuration;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCustomQuote() {
        return isCustomQuote;
    }

    public void setCustomQuote(boolean customQuote) {
        isCustomQuote = customQuote;
    }

    public boolean isCreatePo() {
        return isCreatePo;
    }

    public void setCreatePo(boolean isCreatePo) {
        this.isCreatePo = isCreatePo;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }


    public long getCountTrackingView() {
        return countTrackingView;
    }

    public void setCountTrackingView(long countTrackingView) {
        this.countTrackingView = countTrackingView;
    }

    public Long getScoreCardId() {
        return scoreCardId;
    }

    public void setScoreCardId(Long scoreCardId) {
        this.scoreCardId = scoreCardId;
    }

    public Boolean getPreList() {
        return isPreList;
    }

    public void setPreList(Boolean preList) {
        isPreList = preList;
    }

    public Boolean getCheckInOverride() {
        return isCheckInOverride;
    }

    public void setCheckInOverride(Boolean checkInOverride) {
        isCheckInOverride = checkInOverride;
    }

    public LocalDate getDayHold() {
        return dayHold;
    }

    public void setDayHold(LocalDate dayHold) {
        this.dayHold = dayHold;
    }

    public Boolean getOverrideCreatePo() {
        return isOverrideCreatePo;
    }

    public void setOverrideCreatePo(Boolean overrideCreatePo) {
        isOverrideCreatePo = overrideCreatePo;
    }

    public Boolean getNonComplianceNoti() {
        return isNonComplianceNoti;
    }

    public void setNonComplianceNoti(Boolean nonComplianceNoti) {
        isNonComplianceNoti = nonComplianceNoti;
    }

    public RefundReturnTypeInventory getRefundReturnType() {
        return refundReturnType;
    }

    public void setRefundReturnType(RefundReturnTypeInventory refundReturnType) {
        this.refundReturnType = refundReturnType;
    }

    public Float getPriceRole() {
        return priceRole;
    }

    public void setPriceRole(Float priceRole) {
        this.priceRole = priceRole;
    }

    public Float getListingAge() {
        return listingAge;
    }

    public void setListingAge(Float listingAge) {
        this.listingAge = listingAge;
    }

    public LocalDate getFirstListedDate() {
        return firstListedDate;
    }

    public void setFirstListedDate(LocalDate firstListedDate) {
        this.firstListedDate = firstListedDate;
    }

    public LocalDate getDateSold() {
        return dateSold;
    }

    public void setDateSold(LocalDate dateSold) {
        this.dateSold = dateSold;
    }

    public String getMasterMultiListing() {
        return masterMultiListing;
    }

    public void setMasterMultiListing(String masterMultiListing) {
        this.masterMultiListing = masterMultiListing;
    }

    public Boolean getReviseItem() {
        return isReviseItem;
    }

    public void setReviseItem(Boolean reviseItem) {
        isReviseItem = reviseItem;
    }

    public RecordTypeInventory getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordTypeInventory recordType) {
        this.recordType = recordType;
    }

    public String getPartnerEmail() {
        return partnerEmail;
    }

    public void setPartnerEmail(String partnerEmail) {
        this.partnerEmail = partnerEmail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getStockLocationId() {
        return stockLocationId;
    }

    public void setStockLocationId(Long stockLocationId) {
        this.stockLocationId = stockLocationId;
    }

    public LocalDateTime getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Float getConditionScore() {
        return conditionScore;
    }

    public void setConditionScore(Float conditionScore) {
        this.conditionScore = conditionScore;
    }

    public Float getPrivatePartyValue() {
        return privatePartyValue;
    }

    public void setPrivatePartyValue(Float privatePartyValue) {
        this.privatePartyValue = privatePartyValue;
    }

    public Float getValueAdditionalComponent() {
        return valueAdditionalComponent;
    }

    public void setValueAdditionalComponent(Float valueAdditionalComponent) {
        this.valueAdditionalComponent = valueAdditionalComponent;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public String getSearchGuideRecommendation() {
        return searchGuideRecommendation;
    }

    public void setSearchGuideRecommendation(String searchGuideRecommendation) {
        this.searchGuideRecommendation = searchGuideRecommendation;
    }

    public String getBicycleSizeName() {
        return bicycleSizeName;
    }

    public void setBicycleSizeName(String bicycleSizeName) {
        this.bicycleSizeName = bicycleSizeName;
    }

    public LocalDateTime getFirstListedTime() {
        return firstListedTime;
    }

    @Generated(GenerationTime.ALWAYS)
    public void setFirstListedTime(LocalDateTime firstListedTime) {
        this.firstListedTime = firstListedTime;
    }

    public Float getValueGuidePrice() {
        return valueGuidePrice;
    }

    public void setValueGuidePrice(Float valueGuidePrice) {
        this.valueGuidePrice = valueGuidePrice;
        if ( currentListedPrice != null && valueGuidePrice != null ){
            dealPercent = currentListedPrice * 100 / valueGuidePrice;
        }
    }

    public Float getDealPercent() {
        return dealPercent;
    }

    public void setDealPercent(Float dealPercent) {
        this.dealPercent = dealPercent;
    }

    public Float geteBikeMileage() {
        return eBikeMileage;
    }

    public void seteBikeMileage(Float eBikeMileage) {
        this.eBikeMileage = eBikeMileage;
    }

    public Float geteBikeHours() {
        return eBikeHours;
    }

    public void seteBikeHours(Float eBikeHours) {
        this.eBikeHours = eBikeHours;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public String getSalesforceId() {
        return salesforceId;
    }

    public void setSalesforceId(String salesforceId) {
        this.salesforceId = salesforceId;
    }

    public Boolean getOversized() {
        return isOversized;
    }

    public void setOversized(Boolean oversized) {
        isOversized = oversized;
    }

    public boolean getSellerIsBBB() {
        return sellerIsBBB;
    }

    public void setSellerIsBBB(boolean sellerIsBBB) {
        this.sellerIsBBB = sellerIsBBB;
    }
}
