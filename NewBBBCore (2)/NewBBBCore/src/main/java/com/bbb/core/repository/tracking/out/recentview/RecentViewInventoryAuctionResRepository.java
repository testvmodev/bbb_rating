package com.bbb.core.repository.tracking.out.recentview;

import com.bbb.core.model.response.market.home.recentview.RecentViewInventoryAuctionResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecentViewInventoryAuctionResRepository extends JpaRepository<RecentViewInventoryAuctionResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory_type.name AS inventory_type_name, " +
            ":marketType AS market_type , "+
            "max_price_offer.count_bid AS number_bids, " +
            "inventory_auction.min AS min_bid, " +
            "'last_view_time' = CONVERT(DATETIME, NULL), " +
            "inventory.bicycle_size_name AS size_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "max_price_offer.max_offer_price AS current_highest_bid, " +
            "auction.end_date AS end_date, " +
            "auction.start_date AS start_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM " +
            "inventory_auction " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory_type.id = inventory.type_id " +

            "LEFT JOIN "+
                "(SELECT DISTINCT " +
                    "offer.inventory_auction_id AS inventory_auction_id, "+
                    "MAX(offer.offer_price) AS max_offer_price, " +
                    "COUNT(offer.id) AS count_bid " +
                "FROM offer " +
                "WHERE offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL " +
                "GROUP BY offer.inventory_auction_id ) AS max_price_offer " +
            "ON inventory_auction.inventory_id = max_price_offer.inventory_auction_id " +

            "WHERE "+
            "inventory_auction.is_delete = 0 AND "+
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) < 0   AND DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date)  > 0 AND "+
            "inventory_auction.id IN :inventoryAuctionIds AND "+
            "inventory_auction.is_inactive = 0 AND "+
            "inventory_auction.is_delete = 0"
    )
    List<RecentViewInventoryAuctionResponse> findAllInventoryAuction(
            @Param(value = "inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param(value = "now") String now,
            @Param(value = "marketType") String marketType
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "tracking.inventory_auction_id AS inventory_auction_id, " +
            "inventory.id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory_type.name AS inventory_type_name, " +
            "tracking.type AS market_type, " + //auction tracking type same value as market type, but not all tracking type
            "inventory_auction.min  AS min_bid, " +
            "max_price_offer.count_bid AS number_bids, "+
            "tracking.last_viewed AS last_view_time, "+
            "inventory.bicycle_size_name AS size_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "max_price_offer.max_offer_price AS current_highest_bid, " +
            "auction.end_date AS end_date, " +
            "auction.start_date AS start_date, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration " +
            "FROM tracking " +
            "JOIN inventory_auction ON tracking.inventory_auction_id = inventory_auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
            "JOIN inventory_type ON inventory_type.id = inventory.type_id " +
            "JOIN auction ON inventory_auction.auction_id = auction.id " +

            "LEFT JOIN " +
                "(SELECT DISTINCT " +
                    "inventory_auction.inventory_id AS max_inventory_id, " +
                    "inventory_auction.auction_id AS max_auction_id,  " +
                    "MAX(offer.offer_price) AS max_offer_price, " +
                    "COUNT(offer.id) AS count_bid " +
                    "FROM offer " +
                    "JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id  " +
                    "WHERE offer.is_delete = 0 AND inventory_auction.auction_id IS NOT NULL " +
                    "GROUP BY inventory_auction.inventory_id, inventory_auction.auction_id ) AS max_price_offer " +
            "ON inventory_auction.inventory_id = max_price_offer.max_inventory_id AND inventory_auction.auction_id = max_price_offer.max_auction_id " +

            "WHERE " +
            "tracking.user_id = :userId  AND " +
            "(:marketType IS NULL OR tracking.type = :marketType) AND " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.start_date) < 0   AND DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date)  > 0 AND "+
            "inventory_auction.is_inactive = 0 AND "+
            "inventory_auction.is_delete = 0 " +
            "ORDER BY tracking.last_viewed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY")
    List<RecentViewInventoryAuctionResponse> findAllTracking(
            @Param(value = "userId") String userId,
            @Param(value = "marketType") String marketType,
            @Param(value = "now") String now,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );
}
