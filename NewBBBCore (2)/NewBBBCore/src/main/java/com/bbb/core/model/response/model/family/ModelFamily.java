package com.bbb.core.model.response.model.family;

import com.bbb.core.model.response.ContentDoubleLongId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class ModelFamily {
    @EmbeddedId
    private ContentDoubleLongId id;
    private String modelName;
    private String yearName;

    public ContentDoubleLongId getId() {
        return id;
    }

    public void setId(ContentDoubleLongId id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }
}
