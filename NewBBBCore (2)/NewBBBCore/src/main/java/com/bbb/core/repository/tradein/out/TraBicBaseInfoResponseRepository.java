package com.bbb.core.repository.tradein.out;

import com.bbb.core.model.response.tradein.detail.TraBicBaseInfoResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TraBicBaseInfoResponseRepository extends JpaRepository<TraBicBaseInfoResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "bicycle.brand_id AS brand_id, bicycle_brand.name AS brand_name, bicycle.model_id AS model_id, " +
            "bicycle_model.name AS model_name, bicycle.year_id AS year_id, bicycle_year.name AS year_name, " +
            "bicycle.type_id AS type_id, bicycle_type.name AS type_name " +
            "FROM " +
            "bicycle JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
            "WHERE bicycle.id = :bicycleId"
    )
    TraBicBaseInfoResponse findOne(@Param(value = "bicycleId") long bicycleId);
}
