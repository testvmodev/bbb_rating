package com.bbb.core.service.tracking;


import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.tracking.RecentMarketListingRequest;
import com.bbb.core.model.request.tracking.RecentRequest;
import com.bbb.core.security.user.UserContext;

import java.util.List;

public interface TrackingService {
    Object getDeals() throws ExceptionResponse;

    Object getRecentTrackings(List<RecentMarketListingRequest> recent) throws ExceptionResponse;

    Object getBicycleRecentTrackings(List<RecentRequest> recent) throws ExceptionResponse;
}
