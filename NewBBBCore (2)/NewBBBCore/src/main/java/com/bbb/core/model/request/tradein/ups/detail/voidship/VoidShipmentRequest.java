package com.bbb.core.model.request.tradein.ups.detail.voidship;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VoidShipmentRequest {
    @JsonProperty("VoidShipment")
    private VoidShipment voidShipment;
    @JsonProperty("Request")
    private VoidRequestConfig request;

    public VoidShipmentRequest() {}

    public VoidShipmentRequest(String shipmentIdentificationNumber) {
        voidShipment = new VoidShipment(shipmentIdentificationNumber);
    }

    public VoidShipment getVoidShipment() {
        return voidShipment;
    }

    public void setVoidShipment(VoidShipment voidShipment) {
        this.voidShipment = voidShipment;
    }

    public VoidRequestConfig getRequest() {
        return request;
    }

    public void setRequest(VoidRequestConfig request) {
        this.request = request;
    }
}
