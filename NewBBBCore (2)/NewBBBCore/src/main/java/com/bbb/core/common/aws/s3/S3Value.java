package com.bbb.core.common.aws.s3;

import com.bbb.core.common.Constants;

public interface S3Value {
    String FOLDER_TRADEIN_ORIGINAL = Constants.ORIGIN_FOLDER_S3 + "/" + Constants.TRADE_IN_FOLDER;
    String FOLDER_TRADEIN_LARGE = Constants.LARGE_FOLDER_S3 + "/" + Constants.TRADE_IN_FOLDER;
    String FOLDER_TRADEIN_SMALL = Constants.SMALL_FOLDER_S3 + "/" + Constants.TRADE_IN_FOLDER;
    String FOLDER_TRADEIN_SHIPPING = Constants.SHIPPING_FOLDER_S3 + "/" + Constants.TRADE_IN_FOLDER;

    String FOLDER_MARKET_LISTING_PTP_ORIGINAL = Constants.ORIGIN_FOLDER_S3 + "/" + Constants.MARKET_LISTING_PTP_FOLDER;
    String FOLDER_MARKET_LISTING_PTP_LARGE = Constants.LARGE_FOLDER_S3 + "/" + Constants.MARKET_LISTING_PTP_FOLDER;
    String FOLDER_MARKET_LISTING_PTP_SMALL = Constants.SMALL_FOLDER_S3 + "/" + Constants.MARKET_LISTING_PTP_FOLDER;

    String FOLDER_BRAND_LOGO_ORIGINAL = Constants.ORIGIN_FOLDER_S3 + "/" + Constants.BRAND_LOGO_FOLDER;
    String FOLDER_BRAND_LOGO_LARGE = Constants.LARGE_FOLDER_S3 + "/" + Constants.BRAND_LOGO_FOLDER;
    String FOLDER_BRAND_LOGO_SMALL = Constants.SMALL_FOLDER_S3 + "/" + Constants.BRAND_LOGO_FOLDER;

    String FOLDER_BICYCLE_ORIGINAL = Constants.ORIGIN_FOLDER_S3 + "/" + Constants.BICYCLE_FOLDER;
    String FOLDER_BICYCLE_LARGE = Constants.LARGE_FOLDER_S3 + "/" + Constants.BICYCLE_FOLDER;
    String FOLDER_BICYCLE_SMALL = Constants.SMALL_FOLDER_S3 + "/" + Constants.BICYCLE_FOLDER;

    String PREFIX_DRAFT_LISTING_PTP = "draft";
    String JPG_EXTENTION = "jpg";
    String PNG_EXTENTION = "png";
    String IMAGE_COMMON_TYPE = "image/*";
}
