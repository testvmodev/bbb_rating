package com.bbb.core.manager.tradein;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.TradeInImage;
import com.bbb.core.model.database.TradeInImageType;
import com.bbb.core.model.request.tradein.tradin.TradeInImageGroupRequest;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.tradein.TradeInImageRepository;
import com.bbb.core.repository.tradein.TradeInImageTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TradeInBaseManager implements MessageResponses {
    @Autowired
    protected TradeInImageTypeRepository tradeInImageTypeRepository;
    @Autowired
    protected S3Manager s3Manager;
    @Autowired
    protected TradeInImageRepository tradeInImageRepository;

    protected List<TradeInImageGroupRequest> getGroupImage(List<MultipartFile> images, List<TradeInImageType> tradeInImageTypesRequireFull) throws ExceptionResponse {
        List<TradeInImageGroupRequest> groupRequests = new ArrayList<>();


        for (MultipartFile image : images) {
            String nameImage = image.getOriginalFilename();
            long tradeInImageTypeId;
            if (nameImage.contains("_")) {
                try {
                    tradeInImageTypeId = Long.valueOf(nameImage.substring(0, nameImage.indexOf("_")));
                } catch (NumberFormatException e) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, NAME_FILE_INVALID),
                            HttpStatus.BAD_REQUEST
                    );
                }
            } else {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, NAME_FILE_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }

            TradeInImageGroupRequest tradeInImage = CommonUtils.findObject(groupRequests, g -> g.getImageTypeId() == tradeInImageTypeId);
            if (tradeInImage == null) {
                List<MultipartFile> files = new ArrayList<>();
                files.add(image);
                tradeInImage = new TradeInImageGroupRequest(tradeInImageTypeId, files);
                TradeInImageType tradeInImageType = CommonUtils.findObject(tradeInImageTypesRequireFull, tr -> tr.getId() == tradeInImageTypeId);
                tradeInImage.setRequire(tradeInImageType != null);
                groupRequests.add(tradeInImage);
            } else {
                tradeInImage.getImage().add(image);
            }
        }
        return groupRequests;
    }

    protected void checkValidTradeInImage(List<TradeInImage> tradeInImagesInserted,
                                          List<TradeInImageGroupRequest> groupRequests,
                                          List<TradeInImageType> tradeInImageTypesRequireFull, boolean isSave) throws ExceptionResponse {
        List<Long> imageTypeIdsInserted = tradeInImagesInserted.stream().map(TradeInImage::getImageTypeId).collect(Collectors.toList());
        List<Long> imageTypeIdsRequire = groupRequests.stream()
                .filter(TradeInImageGroupRequest::isRequire)
                .map(TradeInImageGroupRequest::getImageTypeId).collect(Collectors.toList());
        if (imageTypeIdsInserted.size() > 0) {
            CommonUtils.stream(imageTypeIdsInserted);
            for (int i = 0; i < imageTypeIdsInserted.size(); i++) {
                long imageTypeId = imageTypeIdsInserted.get(i);
                boolean isHas = false;
                for (TradeInImageType tradeInImageType : tradeInImageTypesRequireFull) {
                    if (imageTypeId == tradeInImageType.getId()) {
                        isHas = true;
                        break;
                    }
                }
                if (!isHas) {
                    imageTypeIdsInserted.remove(i);
                    i--;
                }
            }
            if (imageTypeIdsInserted.size() > 0) {
                imageTypeIdsRequire.addAll(imageTypeIdsInserted);
            }
        }
        //check enoungth require
        if (!isSave && imageTypeIdsRequire.size() < tradeInImageTypesRequireFull.size()) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, MUST_ENOUGH_IMAGE_TYPE_REQUIRE),
                    HttpStatus.BAD_REQUEST
            );
        }
//        end
    }

    @Async
    protected void deleteImageTradeIn(long tradeId, List<Long> tradeImageTypeIds) {
        List<String> allKeyDelete = new ArrayList<>();
        for (Long tradeInImageTypeId : tradeImageTypeIds) {
            List<String> keyFulls = s3Manager.getListObject(
                    S3Value.FOLDER_TRADEIN_ORIGINAL + "/" + Constants.TRADE_IN_FOLDER + "_" + tradeId + "_" + tradeInImageTypeId + "_")
                    .getObjectSummaries()
                    .stream().map(S3ObjectSummary::getKey).collect(Collectors.toList());
            addFullKeyS3Delete(allKeyDelete, keyFulls);
        }
        if (allKeyDelete.size() > 0) {
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_ORIGINAL, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_LARGE, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_SMALL, allKeyDelete);
        }
    }

    @Async
    protected void deleteImageTradeIn(List<String> fullImages) {
        List<String> allKeyDelete = new ArrayList<>();
        addFullKeyS3Delete(allKeyDelete, fullImages);
        if (allKeyDelete.size() > 0) {
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_ORIGINAL, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_LARGE, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_SMALL, allKeyDelete);
        }
    }

    @Async
    protected void deleteImageTradeInQuote(long tradeIdQuote, List<Long> tradeImageTypeIds) {
        List<String> allKeyDelete = new ArrayList<>();
        for (Long tradeInImageTypeId : tradeImageTypeIds) {
            List<String> keyFulls = s3Manager.getListObject(
                    S3Value.FOLDER_TRADEIN_ORIGINAL + "/" + Constants.TRADE_IN_QUOTE_FOLDER + "_" + tradeIdQuote + "_" + tradeInImageTypeId + "_")
                    .getObjectSummaries()
                    .stream().map(S3ObjectSummary::getKey).collect(Collectors.toList());
            addFullKeyS3Delete(allKeyDelete, keyFulls);
        }
        if (allKeyDelete.size() > 0) {
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_ORIGINAL, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_LARGE, allKeyDelete);
            s3Manager.deleteObjectAsync(S3Value.FOLDER_TRADEIN_SMALL, allKeyDelete);
        }
    }

    private void addFullKeyS3Delete(List<String> allKeyDelete, List<String> keyFulls) {
        if (keyFulls.size() > 0) {
            keyFulls = keyFulls.stream().map(key -> {
                if (key.contains("/")) {
                    return key.substring(key.lastIndexOf("/") + 1);
                } else {
                    return key;
                }
            }).collect(Collectors.toList());
            allKeyDelete.addAll(keyFulls);
        }
    }

}
