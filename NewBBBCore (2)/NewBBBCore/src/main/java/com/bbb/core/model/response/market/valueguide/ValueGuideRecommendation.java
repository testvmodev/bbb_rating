package com.bbb.core.model.response.market.valueguide;

import com.bbb.core.model.database.type.ConditionInventory;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ValueGuideRecommendation {
    @Id
    private long id;
    private ConditionInventory condition;
    private String typeName;
    private String sizeName;
    private String brandName;
    private String yearName;
    private String frameMaterialName;
    private String brakeTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public String getBrakeTypeName() {
        return brakeTypeName;
    }

    public void setBrakeTypeName(String brakeTypeName) {
        this.brakeTypeName = brakeTypeName;
    }
}
