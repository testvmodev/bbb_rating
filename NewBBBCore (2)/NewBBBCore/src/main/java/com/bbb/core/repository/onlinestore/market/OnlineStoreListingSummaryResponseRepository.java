package com.bbb.core.repository.onlinestore.market;

import com.bbb.core.model.response.onlinestore.OnlineStoreListingSummaryResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OnlineStoreListingSummaryResponseRepository extends JpaRepository<OnlineStoreListingSummaryResponse, String> {
    @Query(nativeQuery = true, value = "SELECT " +
            ":storefrontId AS storefront_id, " +
            "COUNT(my_listing.id) AS all_listing, " +
            "ISNULL(" +
            "SUM(CASE WHEN (my_listing.status IS NOT NULL AND my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()}) " +
            "THEN 1 ELSE 0 END), " +
            "0) AS for_sale, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SOLD.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS sold, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).EXPIRED.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS expired, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS draft, " +
            "ISNULL(" +
            "SUM(CASE WHEN my_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).SALE_PENDING.getValue()} " +
            "THEN 1 ELSE 0 END), " +
            "0) AS sale_pending " +

            "FROM " +

            "(SELECT " +
            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +
            "market_listing.status " +

            "FROM market_listing " +

            "JOIN inventory " +
                "ON market_listing.inventory_id = inventory.id " +
                "AND inventory.storefront_id = :#{#storefrontId} " +
                "AND inventory.is_delete = 0 " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "WHERE market_listing.is_delete = 0 " +


            "UNION ALL " +


            "SELECT " +
            //fake id
            "ABS(CHECKSUM(NewId())) AS id, " +
            ":#{T(com.bbb.core.model.database.type.StatusMarketListing).DRAFT.getValue()} AS status " +

            "FROM my_listing_draft " +
            "WHERE my_listing_draft.storefront_id = :storefrontId " +
            "AND my_listing_draft.is_delete = 0 " +

            ") AS my_listing "
    )
    OnlineStoreListingSummaryResponse getSummary(@Param("storefrontId") String storefrontId);
}
