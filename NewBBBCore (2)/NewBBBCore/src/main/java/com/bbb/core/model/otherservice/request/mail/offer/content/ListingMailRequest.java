package com.bbb.core.model.otherservice.request.mail.offer.content;

public class ListingMailRequest {
    private String askingPrice;
    private long id;
    private BikeMailRequest bike;

    public String getAskingPrice() {
        return askingPrice;
    }

    public void setAskingPrice(String askingPrice) {
        this.askingPrice = askingPrice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BikeMailRequest getBike() {
        return bike;
    }

    public void setBike(BikeMailRequest bike) {
        this.bike = bike;
    }
}
