package com.bbb.core.repository;

import com.bbb.core.model.database.BicycleBrand;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BicycleBrandRepository extends CrudRepository<BicycleBrand, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_brand " +
            "WHERE id = ?1 AND is_delete = 0")
    BicycleBrand findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_brand WHERE is_delete = 0 ORDER BY name ASC")
    List<BicycleBrand> findAll();


    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_brand " +
            "WHERE " +
            "(?1 IS NULL OR bicycle_brand.name LIKE ?1) AND " +
            "bicycle_brand.is_delete = 0")
    int getTotalCount(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_brand WHERE bicycle_brand.id = ?1 AND bicycle_brand.is_delete = 0")
    BicycleBrand findOneNotDelete(long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_brand where bicycle_brand.id = :brandId AND bicycle_brand.is_delete = 0")
    int getNumberBrand(@Param(value = "brandId") long brandId);

    @Query(nativeQuery = true, value = "SELECT * FROM bicycle_brand WHERE bicycle_brand.id = :brandId AND bicycle_brand.is_delete = 0 AND bicycle_brand.is_approved = 1")
    BicycleBrand findOneActive(
            @Param(value = "brandId") long brandId
    );

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM bicycle_brand WHERE bicycle_brand.name = :brandName AND bicycle_brand.is_delete = 0 AND bicycle_brand.is_approved = 1")
    BicycleBrand findOneByNameActive(
            @Param(value = "brandName") String brandName
    );

    /**
     * find one brand by the its name that is not deleted. priority the one that is approved
     * @param brandName
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM bicycle_brand " +
            "WHERE name = :brandName AND is_delete = 0 " +
            "ORDER BY is_approved DESC "
    )
    BicycleBrand findOneByName(@Param(value = "brandName") String brandName);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM bicycle_brand WHERE id=:brandId AND is_delete = 0")
    int getCountBrand(@Param("brandId") long brandId);

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM bicycle_brand " +
            "WHERE " +
            "(:id IS NULL OR bicycle_brand.id = :id) " +
            "AND (:name IS NULL OR bicycle_brand.name LIKE :#{'%' + #name + '%'}) " +
            "AND bicycle_brand.is_delete = 0 " +
            "ORDER BY is_approved DESC"
    )
    BicycleBrand findOneNotDelete(
            @Param("id") Long id,
            @Param("name") String name
    );
}
