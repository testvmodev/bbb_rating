package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PaginationResult {
    @JacksonXmlProperty(localName = "TotalNumberOfPages")
    private int totalNumberOfPages;
    @JacksonXmlProperty(localName = "TotalNumberOfEntries")
    private int totalNumberOfEntries;

    public int getTotalNumberOfPages() {
        return totalNumberOfPages;
    }

    public void setTotalNumberOfPages(int totalNumberOfPages) {
        this.totalNumberOfPages = totalNumberOfPages;
    }

    public int getTotalNumberOfEntries() {
        return totalNumberOfEntries;
    }

    public void setTotalNumberOfEntries(int totalNumberOfEntries) {
        this.totalNumberOfEntries = totalNumberOfEntries;
    }
}
