package com.bbb.core.model.response.tradein.fedex.detail.track;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Location {
    @JacksonXmlProperty(localName = "Residential")
    private boolean residential;

    public boolean isResidential() {
        return residential;
    }

    public void setResidential(boolean residential) {
        this.residential = residential;
    }
}
