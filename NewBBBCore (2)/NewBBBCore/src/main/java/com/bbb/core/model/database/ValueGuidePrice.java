package com.bbb.core.model.database;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ValueGuidePrice {
    @Id
    private long bicycleId;
    private long typeId;
    private String typeName;
    private long brandId;
    private String brandName;
    private long modelId;
    private String modelName;
    private String year;
    private String priceExcellent;
    private String priceVeryGood;
    private String priceGood;
    private String priceFair;

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public long getModelId() {
        return modelId;
    }

    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPriceExcellent() {
        return priceExcellent;
    }

    public void setPriceExcellent(String priceExcellent) {
        this.priceExcellent = priceExcellent;
    }

    public String getPriceVeryGood() {
        return priceVeryGood;
    }

    public void setPriceVeryGood(String priceVeryGood) {
        this.priceVeryGood = priceVeryGood;
    }

    public String getPriceGood() {
        return priceGood;
    }

    public void setPriceGood(String priceGood) {
        this.priceGood = priceGood;
    }

    public String getPriceFair() {
        return priceFair;
    }

    public void setPriceFair(String priceFair) {
        this.priceFair = priceFair;
    }
}
