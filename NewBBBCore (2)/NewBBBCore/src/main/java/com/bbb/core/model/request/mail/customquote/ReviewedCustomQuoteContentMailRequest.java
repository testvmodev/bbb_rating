package com.bbb.core.model.request.mail.customquote;

public class ReviewedCustomQuoteContentMailRequest {

    private String trade_in_link;
    private String trade_in_id;
    private String employee_email;

    public String getTrade_in_link() {
        return trade_in_link;
    }

    public void setTrade_in_link(String trade_in_link) {
        this.trade_in_link = trade_in_link;
    }

    public String getTrade_in_id() {
        return trade_in_id;
    }

    public void setTrade_in_id(String trade_in_id) {
        this.trade_in_id = trade_in_id;
    }

    public String getEmployee_email() {
        return employee_email;
    }

    public void setEmployee_email(String employee_email) {
        this.employee_email = employee_email;
    }
}
