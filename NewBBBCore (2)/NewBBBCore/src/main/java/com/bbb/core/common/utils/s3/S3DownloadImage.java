package com.bbb.core.common.utils.s3;

import org.springframework.http.MediaType;

public class S3DownloadImage {
    private byte[] data;
    private MediaType mediaType;

    public S3DownloadImage(byte[] data, MediaType mediaType) {
        this.data = data;
        this.mediaType = mediaType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }
}
