package com.bbb.core.model.request.market.personallisting;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.request.market.personallisting.update.PersonalListingUpdateBicycle;
import com.bbb.core.model.request.market.personallisting.update.PersonalListingUpdateShipping;
import com.bbb.core.model.request.tradein.customquote.CompRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class PersonalListingUpdateRequest {
    @JsonProperty("bicycle")
    private PersonalListingUpdateBicycle bicycleRequest;
    private PersonalListingUpdateShipping shipping;
    @ApiModelProperty(notes = "add new components or update existed components")
    private List<CompRequest> updateComps;
//    @ApiModelProperty(notes = "remove existed components of this inventory - id of comp type")
//    private List<Long> removeCompTypeIds;
    private String description;
    private ConditionInventory condition;
    private Float salePrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;
    private String emailPaypal;
    private Boolean isBestOffer;
    @ApiModelProperty(notes = "Deprecated. Removing soon")
    private StatusMarketListing statusMarketListing;

    public PersonalListingUpdateBicycle getBicycleRequest() {
        return bicycleRequest;
    }

    public void setBicycleRequest(PersonalListingUpdateBicycle bicycleRequest) {
        this.bicycleRequest = bicycleRequest;
    }

    public PersonalListingUpdateShipping getShipping() {
        return shipping;
    }

    public void setShipping(PersonalListingUpdateShipping shipping) {
        this.shipping = shipping;
    }

    public List<CompRequest> getUpdateComps() {
        return updateComps;
    }

    public void setUpdateComps(List<CompRequest> updateComps) {
        this.updateComps = updateComps;
    }

//    public List<Long> getRemoveCompTypeIds() {
//        return removeCompTypeIds;
//    }
//
//    public void setRemoveCompTypeIds(List<Long> removeCompTypeIds) {
//        this.removeCompTypeIds = removeCompTypeIds;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getEmailPaypal() {
        return emailPaypal;
    }

    public void setEmailPaypal(String emailPaypal) {
        this.emailPaypal = emailPaypal;
    }

    public Boolean getIsBestOffer() {
        return isBestOffer;
    }

    public void setIsBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }
}
