package com.bbb.core.model.database.type;

public enum ConditionInventory {
    EXCELLENT("Excellent"),
    VERY_GOOD("Very Good"),
    GOOD("Good"),
    FAIR("Fair"),
    POOR("Poor"),
    USED("Used"),
    NEW_OTHER("NEW/OTHER");

    private final String value;

    ConditionInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ConditionInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        for (ConditionInventory conditionInventory : ConditionInventory.values()) {
            if (conditionInventory.getValue().equals(value)) {
                return conditionInventory;
            }
        }
        return null;
    }
}
