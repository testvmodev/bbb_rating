package com.bbb.core.model.request.inventory.create;

import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.StatusCarrier;
import com.bbb.core.model.database.type.StatusShippingInBound;
import org.joda.time.LocalDateTime;

public class InventoryShippingCreatedRequest {
    private String trackingInbound;
    private StatusShippingInBound statusInBound;
    private String shippingLocationInBound;
    private LocalDateTime trackingDateInBound;
    private String trackingOutBound;
    private String statusOutBound;
    private String shippingLocationOutBound;
    private LocalDateTime trackingDateOutBound;
    private String carrier;
    private InboundShippingType shippingType;
    private StatusCarrier statusCarrier;

    public String getTrackingInbound() {
        return trackingInbound;
    }

    public void setTrackingInbound(String trackingInbound) {
        this.trackingInbound = trackingInbound;
    }

    public StatusShippingInBound getStatusInBound() {
        return statusInBound;
    }

    public void setStatusInBound(StatusShippingInBound statusInBound) {
        this.statusInBound = statusInBound;
    }

    public String getShippingLocationInBound() {
        return shippingLocationInBound;
    }

    public void setShippingLocationInBound(String shippingLocationInBound) {
        this.shippingLocationInBound = shippingLocationInBound;
    }

    public LocalDateTime getTrackingDateInBound() {
        return trackingDateInBound;
    }

    public void setTrackingDateInBound(LocalDateTime trackingDateInBound) {
        this.trackingDateInBound = trackingDateInBound;
    }

    public String getTrackingOutBound() {
        return trackingOutBound;
    }

    public void setTrackingOutBound(String trackingOutBound) {
        this.trackingOutBound = trackingOutBound;
    }

    public String getStatusOutBound() {
        return statusOutBound;
    }

    public void setStatusOutBound(String statusOutBound) {
        this.statusOutBound = statusOutBound;
    }

    public String getShippingLocationOutBound() {
        return shippingLocationOutBound;
    }

    public void setShippingLocationOutBound(String shippingLocationOutBound) {
        this.shippingLocationOutBound = shippingLocationOutBound;
    }

    public LocalDateTime getTrackingDateOutBound() {
        return trackingDateOutBound;
    }

    public void setTrackingDateOutBound(LocalDateTime trackingDateOutBound) {
        this.trackingDateOutBound = trackingDateOutBound;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }


    public InboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(InboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public StatusCarrier getStatusCarrier() {
        return statusCarrier;
    }

    public void setStatusCarrier(StatusCarrier statusCarrier) {
        this.statusCarrier = statusCarrier;
    }
}
