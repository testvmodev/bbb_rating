package com.bbb.core.model.response.bid.offer;

import com.bbb.core.model.database.Offer;

public class CreateOfferResponse extends Offer {
//    private Integer totalOffer;

    public CreateOfferResponse() {
//        super();
    }

    public CreateOfferResponse(Offer offer) {
//        super();
        setId(offer.getId());
        setBuyerId(offer.getBuyerId());
//        setInventoryId(offer.getInventoryId());
        setOfferPrice(offer.getOfferPrice());
        setStatus(offer.getStatus());
        setCreatedTime(offer.getCreatedTime());
        setLastUpdate(offer.getLastUpdate());
        setDelete(offer.isDelete());
//        setAuctionId(offer.getAuctionId());
        setMarketListingId(offer.getMarketListingId());
        setInventoryAuctionId(offer.getInventoryAuctionId());
    }

//    public Integer getTotalOffer() {
//        return totalOffer;
//    }
//
//    public void setTotalOffer(Integer totalOffer) {
//        this.totalOffer = totalOffer;
//    }
}
