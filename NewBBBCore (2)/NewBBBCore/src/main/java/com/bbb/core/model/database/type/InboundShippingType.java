package com.bbb.core.model.database.type;

public enum InboundShippingType {
    BICYCLE_BLUE_BOOK_TYPE("BicycleBlueBook.com"),
    MY_ACCOUNT_TYPE("My Account"),
    DROP_OFF_BICYCLE_BLUE_BOOK_TYPE("Drop-off at BicycleBlueBook.com");
    private final String value;

    InboundShippingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static InboundShippingType findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "BicycleBlueBook.com":
                return BICYCLE_BLUE_BOOK_TYPE;
            case "My Account":
                return MY_ACCOUNT_TYPE;
            case "Drop-off at BicycleBlueBook.com":
                return DROP_OFF_BICYCLE_BLUE_BOOK_TYPE;
            default:
                return null;
        }
    }
}
