package com.bbb.core.service.impl.tradein;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.tradein.ShippingAccountManager;
import com.bbb.core.model.request.tradein.tradin.ShippingAccountCreateRequest;
import com.bbb.core.service.tradein.ShippingAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShippingAccountServiceImpl implements ShippingAccountService {
    @Autowired
    private ShippingAccountManager manager;

    @Override
    public Object getAccounts() {
        return manager.getAccounts();
    }

    @Override
    public Object addAccount(ShippingAccountCreateRequest request) throws ExceptionResponse {
        return manager.addAccount(request);
    }

    @Override
    public Object deleteAccount(long id) throws ExceptionResponse {
        return manager.deleteAccount(id);
    }

    @Override
    public Object activate(long id) throws ExceptionResponse {
        return manager.activate(id);
    }
}
