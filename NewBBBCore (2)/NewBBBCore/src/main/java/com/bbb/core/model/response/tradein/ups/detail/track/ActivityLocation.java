package com.bbb.core.model.response.tradein.ups.detail.track;

import com.bbb.core.model.response.tradein.ups.detail.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ActivityLocation {
    @JsonProperty("Address")
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
