package com.bbb.core.model.response.shipping;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryShippingProfileResponse {
    private long inventoryId;
    @Id
    private long bicycleTypeId;
    private long ebayShippingProfileId;
    private float shippingFee;
    private String label;
    private String description;

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public long getEbayShippingProfileId() {
        return ebayShippingProfileId;
    }

    public void setEbayShippingProfileId(long ebayShippingProfileId) {
        this.ebayShippingProfileId = ebayShippingProfileId;
    }

    public float getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(float shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
