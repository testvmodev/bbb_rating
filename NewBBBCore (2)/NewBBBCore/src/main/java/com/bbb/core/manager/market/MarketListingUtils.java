package com.bbb.core.manager.market;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.ShippingFeeConfig;
import com.bbb.core.model.response.ContentCommon;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.embedded.ContentCommonId;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.shipping.ShippingFeeConfigRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MarketListingUtils implements MessageResponses {
    public static Pair<List<Pair<Long, ShippingFeeConfig>>, List<Pair<Long, String>>> getListPairInventoryIdShipConfigBicycleType(
            List<Inventory> inventorys, ContentCommonRepository contentCommonRepository, ShippingFeeConfigRepository shippingFeeConfigRepository) {
        List<String> bicycleTypeNames = inventorys.stream().map(Inventory::getBicycleTypeName).filter(o->!StringUtils.isBlank(o)).collect(Collectors.toList());
        List<Pair<Long, ShippingFeeConfig>> pairShippingFees = new ArrayList<>();
        List<Pair<Long, String>> inventoryIdsErrorType = new ArrayList<>();
        if (bicycleTypeNames.size() == 0) {
            inventoryIdsErrorType.addAll(
                    inventorys.stream().map(o->new Pair<>(o.getId(), TYPE_NAME_INVENTORY + " " + o.getId() +" " + INVALID)).collect(Collectors.toList())
            );
            return new Pair<>(pairShippingFees, inventoryIdsErrorType);
        }
        List<Inventory> inventoriesValid = new ArrayList<>();
        for (Inventory inventory : inventorys) {
            if ( StringUtils.isBlank(inventory.getBicycleTypeName())){
                inventoryIdsErrorType.add(new Pair<>(inventory.getId(), TYPE_NAME_INVENTORY + " " + inventory.getId() +" " + INVALID));
            }else {
                inventoriesValid.add(inventory);
            }
        }

        CommonUtils.stream(bicycleTypeNames);
        List<ContentCommonId> bicycleTypeContents = contentCommonRepository.findBicycleTypeByNames(bicycleTypeNames).stream().map(ContentCommon::getId).collect(Collectors.toList());
        List<Long> typeIds = bicycleTypeContents.stream().map(ContentCommonId::getId).collect(Collectors.toList());
        CommonUtils.stream(typeIds);
        List<ShippingFeeConfig> shippingFeeConfigs = shippingFeeConfigRepository.findAll(typeIds);
        for ( int i =0; i< inventoriesValid.size(); i++){
            int index = i;
            ContentCommonId contentType = CommonUtils.findObject(bicycleTypeContents, o->o.getName().equals(inventoriesValid.get(index).getBicycleTypeName()));
            if (contentType == null ){
                inventoryIdsErrorType.add(new Pair<>(inventoriesValid.get(i).getId(), TYPE_NAME_INVENTORY + " " + inventoriesValid.get(i).getId() +" " + INVALID));
                inventoriesValid.remove(i);
                i--;
            }else {
                ShippingFeeConfig shippingFeeConfig = CommonUtils.findObject(shippingFeeConfigs, o->o.getBicycleTypeId() == contentType.getId());
                if (shippingFeeConfig == null ){
                    inventoryIdsErrorType.add(new Pair<>(inventoriesValid.get(i).getId(), TYPE_NAME_INVENTORY + " " + inventoriesValid.get(i).getId() +" " + INVALID));
                    inventoriesValid.remove(i);
                    i--;
                }else {
                    pairShippingFees.add(new Pair<>(inventoriesValid.get(i).getId(), shippingFeeConfig));
                }
            }
        }

        return new Pair<>(pairShippingFees, inventoryIdsErrorType);
    }
}
