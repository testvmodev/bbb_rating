package com.bbb.core.repository.reout;

import com.bbb.core.model.response.brandmodelyear.BrandModelYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandModelYearRepository extends JpaRepository<BrandModelYear, Long> {
    //todo can error brand, model conflict
    @Query(nativeQuery = true, value =
            "SELECT :id AS id, brand_id, brand_name, model_id, model_name, year_id, year_name FROM " +
                    "(SELECT DISTINCT bicycle_brand.id AS brand_id, bicycle_brand.name AS brand_name FROM bicycle_brand " +
                    "WHERE ((:brandId IS NOT NULL AND bicycle_brand.id = :brandId) OR bicycle_brand.name = :brandName) AND bicycle_brand.is_delete = 0) AS brand " +
                    "FULL JOIN "+
                    "(SELECT DISTINCT bicycle_model.id AS model_id, bicycle_model.name AS model_name FROM bicycle_model " +
                    "WHERE ((:modelId IS NOT NULL AND bicycle_model.id = :modelId) OR bicycle_model.name = :modelName) AND bicycle_model.is_delete = 0) AS model " +
                    "ON 1 IS NOT NULL "+
                    "FULL JOIN "+
                    "(SELECT DISTINCT bicycle_year.id AS year_id, bicycle_year.name AS year_name FROM bicycle_year " +
                    "WHERE  bicycle_year.name = :yearName AND bicycle_year.is_delete = 0) AS year " +
                    "ON 1 IS NOT NULL "

    )
    BrandModelYear findOne(
            @Param(value = "brandId") Long brandId,
            @Param(value = "brandName") String brandName,
            @Param(value = "modelId") Long modelId,
            @Param(value = "modelName") String modelName,
            @Param(value = "yearName") String yearName,
            @Param(value = "id") long id

    );



    @Query(nativeQuery = true, value =
            "SELECT :id AS id, brand.brand_id, brand_name, model_id, model_name, year_id, year_name FROM " +
                    "(SELECT DISTINCT bicycle_brand.id AS brand_id, bicycle_brand.name AS brand_name FROM bicycle_brand WHERE bicycle_brand.name = :brandName) AS brand " +
                    "LEFT JOIN " +
                    "(SELECT DISTINCT bicycle_model.id AS model_id, bicycle_model.name AS model_name, bicycle_model.brand_id as brand_id FROM bicycle_model WHERE bicycle_model.name = :modelName) AS model " +
                    "ON model.model_name = :modelName and brand.brand_id = model.brand_id " +
                    "LEFT JOIN " +
                    "(SELECT DISTINCT bicycle_year.id AS year_id, bicycle_year.name AS year_name FROM bicycle_year WHERE bicycle_year.name = :yearName) AS year " +
                    "ON year.year_name = :yearName"
    )
    BrandModelYear findOne(
            @Param(value = "brandName") String brandName,
            @Param(value = "modelName") String modelName,
            @Param(value = "yearName") String yearName,
            @Param(value = "id") long id
    );

    @Query(nativeQuery = true, value = "SELECT * FROM " +
            "(SELECT 1 AS id, " +
            "bicycle_brand.id AS brand_id, " +
            "bicycle_brand.name AS brand_name " +
            "FROM bicycle_brand WHERE bicycle_brand.id = :brandId) AS brand " +
            "LEFT JOIN " +
            "(SELECT bicycle_model.id AS model_id, " +
            "bicycle_model.name AS model_name " +
            "FROM bicycle_model WHERE bicycle_model.id = :modelId) AS model ON 1 = 1 " +
            "LEFT JOIN " +
            "(SELECT bicycle_year.id AS year_id, " +
            "bicycle_year.name AS year_name " +
            "FROM bicycle_year WHERE bicycle_year.id = :yearId) AS year ON 1 = 1"
    )
    BrandModelYear findOne(
            @Param(value = "brandId") long brandId,
            @Param(value = "modelId") long modelId,
            @Param(value = "yearId") long yearId
    );
}
