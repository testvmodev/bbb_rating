package com.bbb.core.manager.bicycle;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.ComponentCategory;
import com.bbb.core.model.request.component.ComponentCategoryCreateRequest;
import com.bbb.core.model.request.component.ComponentCategoryGetRequest;
import com.bbb.core.model.request.component.ComponentCategoryUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.bicycle.ComponentCategoryRepository;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static com.bbb.core.common.MessageResponses.*;

@Component
public class ComponentCategoryManager {
    @Autowired
    private ComponentCategoryRepository componentCategoryRepository;
    @Autowired
    private ComponentTypeRepository componentTypeRepository;

    public Object getComponentCategories(ComponentCategoryGetRequest request) throws ExceptionResponse {
        ListObjResponse<ComponentCategory> response = new ListObjResponse(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(componentCategoryRepository.countAll());
        if (response.getTotalItem() > 0) {
            response.setData(
                    componentCategoryRepository.findAllSorted(
                            request.getSortField(),
                            request.getSortType(),
                            request.getPageable()
                    )
            );
        }
        return response;
    }

    public Object getDetailComponentCategory(long cateId) throws ExceptionResponse{
        ComponentCategory category = componentCategoryRepository.findById(cateId);
        if (category == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, COMPONENT_CATEGORY_NOT_EXIST), HttpStatus.NOT_FOUND);

        }
        return category;
    }

    public Object createComponentCategory(ComponentCategoryCreateRequest request) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        return createCategoryIgnoreRole(request);
    }

    public Object createCategoryIgnoreRole(ComponentCategoryCreateRequest request) throws  ExceptionResponse{
        if (request.getName() == null || !request.validateCate()){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, CATEGORY_NAME_NOT_EMPTY), HttpStatus.NOT_FOUND
            );
        }
        if (componentCategoryRepository.findListName().contains(request.getName())){
            throw  new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, CATEGORY_NAME_IS_EXIST), HttpStatus.BAD_REQUEST
            );
        }
        ComponentCategory category = new ComponentCategory();
        category.setName(request.getName().trim());
        return componentCategoryRepository.save(category);
    }

    public Object updateComponentCategory(long cateId, ComponentCategoryUpdateRequest request)throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentCategory oldCategory = componentCategoryRepository.findById(cateId);
        if (oldCategory == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, COMPONENT_CATEGORY_NOT_EXIST), HttpStatus.NOT_FOUND);

        }
        if (request == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, CATEGORY_NAME_NOT_EMPTY), HttpStatus.NOT_FOUND
            );
        }
        if (componentCategoryRepository.findListName().contains(request.getName())){
            throw  new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, CATEGORY_NAME_IS_EXIST), HttpStatus.BAD_REQUEST
            );
        }
        oldCategory.setName(request.getName());
        return componentCategoryRepository.save(oldCategory);
    }

    public Object deleteComponentCategory(long cateId) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentCategory oldCategory = componentCategoryRepository.findById(cateId);
        if (oldCategory == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, COMPONENT_CATEGORY_NOT_EXIST), HttpStatus.NOT_FOUND);

        }
        int countCompType = componentTypeRepository.getCountByCateId(cateId);
        if (countCompType > 0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, (countCompType==1?THERE_IS:THERE_ARE+ countCompType )+ TYPE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        oldCategory.setDelete(true);
        componentCategoryRepository.save(oldCategory);
        return new MessageResponse(SUCCESS);
    }
}
