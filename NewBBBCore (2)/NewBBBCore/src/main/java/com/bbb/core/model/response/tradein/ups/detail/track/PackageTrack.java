package com.bbb.core.model.response.tradein.ups.detail.track;

import com.bbb.core.model.response.tradein.ups.detail.track.Activity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PackageTrack {
    @JsonProperty("TrackingNumber")
    private String trackingNumber;
    @JsonProperty("Activity")
    private List<Activity> activities;

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
