package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Variation {
    @JacksonXmlProperty(localName = "SKU")
    private String sku;
    @JacksonXmlProperty(localName = "StartPrice")
    private float startPrice;
    @JacksonXmlProperty(localName = "Quantity")
    private int quantity;
    @JacksonXmlProperty(localName = "VariationSpecifics")
    private VariationSpecifics variationSpecifics;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(float startPrice) {
        this.startPrice = startPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public VariationSpecifics getVariationSpecifics() {
        return variationSpecifics;
    }

    public void setVariationSpecifics(VariationSpecifics variationSpecifics) {
        this.variationSpecifics = variationSpecifics;
    }
}
