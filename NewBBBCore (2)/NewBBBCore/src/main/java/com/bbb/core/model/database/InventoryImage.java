package com.bbb.core.model.database;

import javax.persistence.*;

@Entity
@Table(name = "inventory_image")
public class InventoryImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Long inventoryId;
    private String image;

    public InventoryImage() {}

    public InventoryImage(Long inventoryId, String image) {
        this.inventoryId = inventoryId;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public String getIdInventorySaleForce() {
//        return idInventorySaleForce;
//    }
//
//    public void setIdInventorySaleForce(String idInventorySaleForce) {
//        this.idInventorySaleForce = idInventorySaleForce;
//    }
}
