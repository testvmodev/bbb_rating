package com.bbb.core.model.request.ebay.additem;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CalculatedShippingRate {
    @JacksonXmlProperty(localName = "OriginatingPostalCode")
    private String originatingPostalCode;
    @JacksonXmlProperty(localName = "MeasurementUnit")
    private String measurementUnit;
    @JacksonXmlProperty(localName = "PackageDepth")
    private int packageDepth;
    @JacksonXmlProperty(localName = "PackageLength")
    private int packageLength;
    @JacksonXmlProperty(localName = "PackageWidth")
    private int packageWidth;
    @JacksonXmlProperty(localName = "ShippingPackage")
    private String shippingPackage;
    @JacksonXmlProperty(localName = "WeightMajor")
    private WeightCalculateShip weightMajor;
    @JacksonXmlProperty(localName = "WeightMinor")
    private WeightCalculateShip weightMinor;

    public String getOriginatingPostalCode() {
        return originatingPostalCode;
    }

    public void setOriginatingPostalCode(String originatingPostalCode) {
        this.originatingPostalCode = originatingPostalCode;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public int getPackageDepth() {
        return packageDepth;
    }

    public void setPackageDepth(int packageDepth) {
        this.packageDepth = packageDepth;
    }

    public int getPackageLength() {
        return packageLength;
    }

    public void setPackageLength(int packageLength) {
        this.packageLength = packageLength;
    }

    public int getPackageWidth() {
        return packageWidth;
    }

    public void setPackageWidth(int packageWidth) {
        this.packageWidth = packageWidth;
    }

    public String getShippingPackage() {
        return shippingPackage;
    }

    public void setShippingPackage(String shippingPackage) {
        this.shippingPackage = shippingPackage;
    }

    public WeightCalculateShip getWeightMajor() {
        return weightMajor;
    }

    public void setWeightMajor(WeightCalculateShip weightMajor) {
        this.weightMajor = weightMajor;
    }

    public WeightCalculateShip getWeightMinor() {
        return weightMinor;
    }

    public void setWeightMinor(WeightCalculateShip weightMinor) {
        this.weightMinor = weightMinor;
    }
}
