package com.bbb.core.model.otherservice.request.mail.offer.content;

public class BuyerMailRequest {
    private String username;

    public BuyerMailRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
