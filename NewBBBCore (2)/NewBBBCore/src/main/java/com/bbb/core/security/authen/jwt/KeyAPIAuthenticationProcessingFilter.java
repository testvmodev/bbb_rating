package com.bbb.core.security.authen.jwt;

import com.bbb.core.common.Constants;
import com.bbb.core.common.config.ConfigDataSource;
import com.bbb.core.security.authen.ajax.SecretKeyAuthenticationFailureHandler;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class KeyAPIAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
    private SecretKeyAuthenticationFailureHandler failureHandler;

    public KeyAPIAuthenticationProcessingFilter(RequestMatcher requiresAuthenticationRequestMatcher,
                                                   SecretKeyAuthenticationFailureHandler failureHandler) {
        super(requiresAuthenticationRequestMatcher);
        this.failureHandler = failureHandler;

    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String secretKey = httpServletRequest.getHeader(Constants.HEADER_SECRET_KEY);
        if (!ConfigDataSource.getSecretKey().equals(secretKey)) {
            throw new BadCredentialsException("The API key was not found or not the expected value.");
        }
        return getAuthenticationManager().authenticate(new SecretAPIAuthentication(secretKey));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authResult);
        SecurityContextHolder.setContext(context);
        chain.doFilter(request, response);

    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();

        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        failureHandler.onAuthenticationFailure(request, response, failed);
    }

}
