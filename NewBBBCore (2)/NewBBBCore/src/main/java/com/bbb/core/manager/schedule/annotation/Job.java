package com.bbb.core.manager.schedule.annotation;

import com.bbb.core.model.database.type.JobTimeType;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Job {
    /**
     * Should this job be enabled by default? Can be overridden by configuration from database
     */
    boolean preEnable() default false;

    /**
     * Ignore setting from database, always disable the job. Useful for local debugging
     */
    boolean forceDisable() default false;

    JobTimeType defaultTimeType() default JobTimeType.FIX_DELAY;

    long defaultTimeSeconds() default 0;

    String defaultCron() default "";
}
