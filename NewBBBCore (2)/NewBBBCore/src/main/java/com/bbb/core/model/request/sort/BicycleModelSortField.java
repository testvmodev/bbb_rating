package com.bbb.core.model.request.sort;

public enum  BicycleModelSortField {
    NAME("name"),
    BRAND("brand_id"),
    APPROVED("is_approved");
    private final String value;

    BicycleModelSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
