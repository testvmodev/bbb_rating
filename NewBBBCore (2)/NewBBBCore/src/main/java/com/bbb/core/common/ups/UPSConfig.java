package com.bbb.core.common.ups;

import com.bbb.core.model.request.tradein.ups.detail.ship.LabelImageFormat;
import com.bbb.core.model.request.tradein.ups.detail.ship.UPSLabelSpecification;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UPSConfig implements UPSValue {
    @Bean
    public UPSLabelSpecification upsLabelSpecification() {
        return new UPSLabelSpecification(new LabelImageFormat(IMAGE_FORMAT));
    }
}
