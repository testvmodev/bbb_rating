package com.bbb.core.model.response;

import com.bbb.core.model.response.embedded.ContentValueCommonId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ContentValueCommon {
    @EmbeddedId
    private ContentValueCommonId id;

    public ContentValueCommonId getId() {
        return id;
    }

    public void setId(ContentValueCommonId id) {
        this.id = id;
    }
}
