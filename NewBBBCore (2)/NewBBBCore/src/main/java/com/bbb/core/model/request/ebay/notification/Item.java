package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.request.ebay.PictureDetails;
import com.bbb.core.model.request.ebay.PrimaryCategory;
import com.bbb.core.model.request.ebay.Seller;
import com.bbb.core.model.request.ebay.SellingStatus;
import com.bbb.core.model.request.ebay.additem.ReturnPolicy;
import com.bbb.core.model.request.ebay.additem.SellerProfiles;
import com.bbb.core.model.request.ebay.additem.ShippingDetailsRequest;
import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

import java.util.List;

public class Item {
    @JacksonXmlProperty(localName = "AutoPay")
    private boolean autoPay;
    @JacksonXmlProperty(localName = "BuyerProtection")
    private String buyerProtection;
    @JacksonXmlProperty(localName = "BuyItNowPrice")
    private Currency buyItNowPrice;
    @JacksonXmlProperty(localName = "Country")
    private String country;
    @JacksonXmlProperty(localName = "Currency")
    private String currency;
    @JacksonXmlProperty(localName = "Description")
    private String description;
    @JacksonXmlProperty(localName = "GiftIcon")
    private String giftIcon;
    @JacksonXmlProperty(localName = "HitCounter")
    private String hitCounter;
    @JacksonXmlProperty(localName = "ItemID")
    private long itemID;
    @JacksonXmlProperty(localName = "ListingDetails")
    private ListingDetails listingDetails;
    @JacksonXmlProperty(localName = "ListingDesigner")
    private ListingDesigner listingDesigner;
    @JacksonXmlProperty(localName = "ListingDuration")
    private String listingDuration;
    @JacksonXmlProperty(localName = "ListingType")
    private String listingType;
    @JacksonXmlProperty(localName = "Location")
    private String location;
    @JacksonXmlProperty(localName = "PaymentMethods")
    private String paymentMethods;
    @JacksonXmlProperty(localName = "PayPalEmailAddress")
    private String payPalEmailAddress;
    @JacksonXmlProperty(localName = "PrimaryCategory")
    private PrimaryCategory primaryCategory;
    @JacksonXmlProperty(localName = "PrivateListing")
    private boolean privateListing;
    @JacksonXmlProperty(localName = "SecondaryCategory")
    private PrimaryCategory secondaryCategory;
    @JacksonXmlProperty(localName = "ShipmentTrackingNumber")
    private String shipmentTrackingNumber;

    @JacksonXmlProperty(localName = "Quantity")
    private int quantity;
    @JacksonXmlProperty(localName = "ReservePrice")
    private Currency reservePrice;
    @JacksonXmlProperty(localName = "ReviseStatus")
    private ReviseStatus reviseStatus;
    @JacksonXmlProperty(localName = "Seller")
    @JsonProperty(value = "seller")
    private Seller seller;
    @JacksonXmlProperty(localName = "SellingStatus")
    private SellingStatus sellingStatus;
    @JacksonXmlProperty(localName = "ShippingDetails")
    private ShippingDetailsRequest shippingDetails;
    @JacksonXmlProperty(localName = "ShipToLocations")
    private String shipToLocations;
    @JacksonXmlProperty(localName = "Site")
    private String site;
    @JacksonXmlProperty(localName = "StartPrice")
    private Currency startPrice;
    @JacksonXmlProperty
    private LocalDateTime timeLeft;
    @JacksonXmlProperty(localName = "Title")
    private String title;
    @JacksonXmlProperty(localName = "HitCount")
    private String hitCount;
    @JacksonXmlProperty(localName = "LocationDefaulted")
    private boolean locationDefaulted;
    @JacksonXmlProperty(localName = "GetItFast")
    private boolean getItFast;
    @JacksonXmlProperty(localName = "BuyerResponsibleForShipping")
    private boolean buyerResponsibleForShipping;
    @JacksonXmlProperty(localName = "PostalCode")
    private String postalCode;
    @JacksonXmlProperty(localName = "PictureDetails")
    private PictureDetails PictureDetails;
    @JacksonXmlProperty
    private int dispatchTimeMax;
    @JacksonXmlProperty(localName = "ProxyItem")
    private boolean proxyItem;
    @JacksonXmlProperty(localName = "ApplyBuyerProtection")
    private ApplyBuyerProtection applyBuyerProtection;
    @JacksonXmlProperty(localName = "BuyerGuaranteePrice")
    private Currency buyerGuaranteePrice;
    @JacksonXmlProperty(localName = "IntangibleItem")
    private boolean intangibleItem;
    @JacksonXmlProperty(localName = "ReturnPolicy")
    private ReturnPolicy returnPolicy;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "PaymentAllowedSite")
    private List<String> paymentAllowedSite;
    @JacksonXmlProperty(localName = "ConditionID")
    private long conditionID;
    @JacksonXmlProperty(localName = "ConditionDisplayName")
    private String conditionDisplayName;
    @JacksonXmlProperty(localName = "PostCheckoutExperienceEnabled")
    private boolean postCheckoutExperienceEnabled;
    @JacksonXmlProperty(localName = "SellerProfiles")
    private SellerProfiles sellerProfiles;
    @JacksonXmlProperty(localName = "ShippingPackageDetails")
    private ShippingPackageDetails shippingPackageDetails;

    public boolean isAutoPay() {
        return autoPay;
    }

    public void setAutoPay(boolean autoPay) {
        this.autoPay = autoPay;
    }

    public String getBuyerProtection() {
        return buyerProtection;
    }

    public void setBuyerProtection(String buyerProtection) {
        this.buyerProtection = buyerProtection;
    }

    public Currency getBuyItNowPrice() {
        return buyItNowPrice;
    }

    public void setBuyItNowPrice(Currency buyItNowPrice) {
        this.buyItNowPrice = buyItNowPrice;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGiftIcon() {
        return giftIcon;
    }

    public void setGiftIcon(String giftIcon) {
        this.giftIcon = giftIcon;
    }

    public long getItemID() {
        return itemID;
    }

    public void setItemID(long itemID) {
        this.itemID = itemID;
    }

    public String getHitCounter() {
        return hitCounter;
    }

    public void setHitCounter(String hitCounter) {
        this.hitCounter = hitCounter;
    }

    public ListingDetails getListingDetails() {
        return listingDetails;
    }

    public void setListingDetails(ListingDetails listingDetails) {
        this.listingDetails = listingDetails;
    }

    public ListingDesigner getListingDesigner() {
        return listingDesigner;
    }

    public void setListingDesigner(ListingDesigner listingDesigner) {
        this.listingDesigner = listingDesigner;
    }

    public String getListingDuration() {
        return listingDuration;
    }

    public void setListingDuration(String listingDuration) {
        this.listingDuration = listingDuration;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getPayPalEmailAddress() {
        return payPalEmailAddress;
    }

    public void setPayPalEmailAddress(String payPalEmailAddress) {
        this.payPalEmailAddress = payPalEmailAddress;
    }

    public PrimaryCategory getPrimaryCategory() {
        return primaryCategory;
    }

    public void setPrimaryCategory(PrimaryCategory primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public boolean isPrivateListing() {
        return privateListing;
    }

    public void setPrivateListing(boolean privateListing) {
        this.privateListing = privateListing;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Currency getReservePrice() {
        return reservePrice;
    }

    public void setReservePrice(Currency reservePrice) {
        this.reservePrice = reservePrice;
    }

    public ReviseStatus getReviseStatus() {
        return reviseStatus;
    }

    public void setReviseStatus(ReviseStatus reviseStatus) {
        this.reviseStatus = reviseStatus;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public SellingStatus getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(SellingStatus sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    public ShippingDetailsRequest getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(ShippingDetailsRequest shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    public String getShipToLocations() {
        return shipToLocations;
    }

    public void setShipToLocations(String shipToLocations) {
        this.shipToLocations = shipToLocations;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Currency getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Currency startPrice) {
        this.startPrice = startPrice;
    }

    public LocalDateTime getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(LocalDateTime timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHitCount() {
        return hitCount;
    }

    public void setHitCount(String hitCount) {
        this.hitCount = hitCount;
    }

    public boolean isLocationDefaulted() {
        return locationDefaulted;
    }

    public void setLocationDefaulted(boolean locationDefaulted) {
        this.locationDefaulted = locationDefaulted;
    }

    public boolean isGetItFast() {
        return getItFast;
    }

    public void setGetItFast(boolean getItFast) {
        this.getItFast = getItFast;
    }

    public boolean isBuyerResponsibleForShipping() {
        return buyerResponsibleForShipping;
    }

    public void setBuyerResponsibleForShipping(boolean buyerResponsibleForShipping) {
        this.buyerResponsibleForShipping = buyerResponsibleForShipping;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public com.bbb.core.model.request.ebay.PictureDetails getPictureDetails() {
        return PictureDetails;
    }

    public void setPictureDetails(com.bbb.core.model.request.ebay.PictureDetails pictureDetails) {
        PictureDetails = pictureDetails;
    }

    public int getDispatchTimeMax() {
        return dispatchTimeMax;
    }

    public void setDispatchTimeMax(int dispatchTimeMax) {
        this.dispatchTimeMax = dispatchTimeMax;
    }

    public boolean isProxyItem() {
        return proxyItem;
    }

    public void setProxyItem(boolean proxyItem) {
        this.proxyItem = proxyItem;
    }

    public ApplyBuyerProtection getApplyBuyerProtection() {
        return applyBuyerProtection;
    }

    public void setApplyBuyerProtection(ApplyBuyerProtection applyBuyerProtection) {
        this.applyBuyerProtection = applyBuyerProtection;
    }

    public Currency getBuyerGuaranteePrice() {
        return buyerGuaranteePrice;
    }

    public void setBuyerGuaranteePrice(Currency buyerGuaranteePrice) {
        this.buyerGuaranteePrice = buyerGuaranteePrice;
    }

    public boolean isIntangibleItem() {
        return intangibleItem;
    }

    public void setIntangibleItem(boolean intangibleItem) {
        this.intangibleItem = intangibleItem;
    }

    public ReturnPolicy getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(ReturnPolicy returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public List<String> getPaymentAllowedSite() {
        return paymentAllowedSite;
    }

    public void setPaymentAllowedSite(List<String> paymentAllowedSite) {
        this.paymentAllowedSite = paymentAllowedSite;
    }

    public long getConditionID() {
        return conditionID;
    }

    public void setConditionID(long conditionID) {
        this.conditionID = conditionID;
    }

    public String getConditionDisplayName() {
        return conditionDisplayName;
    }

    public void setConditionDisplayName(String conditionDisplayName) {
        this.conditionDisplayName = conditionDisplayName;
    }

    public boolean isPostCheckoutExperienceEnabled() {
        return postCheckoutExperienceEnabled;
    }

    public void setPostCheckoutExperienceEnabled(boolean postCheckoutExperienceEnabled) {
        this.postCheckoutExperienceEnabled = postCheckoutExperienceEnabled;
    }

    public SellerProfiles getSellerProfiles() {
        return sellerProfiles;
    }

    public void setSellerProfiles(SellerProfiles sellerProfiles) {
        this.sellerProfiles = sellerProfiles;
    }

    public ShippingPackageDetails getShippingPackageDetails() {
        return shippingPackageDetails;
    }

    public void setShippingPackageDetails(ShippingPackageDetails shippingPackageDetails) {
        this.shippingPackageDetails = shippingPackageDetails;
    }

    public PrimaryCategory getSecondaryCategory() {
        return secondaryCategory;
    }

    public void setSecondaryCategory(PrimaryCategory secondaryCategory) {
        this.secondaryCategory = secondaryCategory;
    }

    public String getShipmentTrackingNumber() {
        return shipmentTrackingNumber;
    }

    public void setShipmentTrackingNumber(String shipmentTrackingNumber) {
        this.shipmentTrackingNumber = shipmentTrackingNumber;
    }
}
