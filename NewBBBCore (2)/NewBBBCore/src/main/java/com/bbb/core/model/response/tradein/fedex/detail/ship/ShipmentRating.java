package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.bbb.core.model.response.tradein.fedex.detail.ShipmentRateDetail;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ShipmentRating {
    @JacksonXmlProperty(localName = "ActualRateType")
    private String actualRateType;
    @JacksonXmlProperty(localName = "ShipmentRateDetails")
    private ShipmentRateDetail shipmentRateDetail;

    public String getActualRateType() {
        return actualRateType;
    }

    public void setActualRateType(String actualRateType) {
        this.actualRateType = actualRateType;
    }

    public ShipmentRateDetail getShipmentRateDetail() {
        return shipmentRateDetail;
    }

    public void setShipmentRateDetail(ShipmentRateDetail shipmentRateDetail) {
        this.shipmentRateDetail = shipmentRateDetail;
    }
}
