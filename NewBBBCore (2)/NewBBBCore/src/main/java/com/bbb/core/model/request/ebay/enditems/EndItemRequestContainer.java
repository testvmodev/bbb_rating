package com.bbb.core.model.request.ebay.enditems;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class EndItemRequestContainer {
    @JacksonXmlProperty(localName = "ItemID")
    private long itemID;
    @JacksonXmlProperty(localName = "EndingReason")
    private String endingReason = "NotAvailable";
    @JacksonXmlProperty(localName = "MessageID")
    private int messageID;

    public long getItemID() {
        return itemID;
    }

    public void setItemID(long itemID) {
        this.itemID = itemID;
    }

    public String getEndingReason() {
        return endingReason;
    }

    public void setEndingReason(String endingReason) {
        this.endingReason = endingReason;
    }

    public int getMessageID() {
        return messageID;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }
}
