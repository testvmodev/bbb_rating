package com.bbb.core.model.database.type;

public enum StatusCarrier {
    I("In Transit"),
    D("Delivered"),
    X("Exception"),
    P("Pickup"),
    M("Manifest Pickup");
    private final String value;

    StatusCarrier(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StatusCarrier findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "In Transit":
                return I;
            case "Delivered":
                return D;
            case "Exception":
                return X;
            case "Pickup":
                return P;
            case "Manifest Pickup":
                return M;
            default:
                return null;
        }
    }

}
