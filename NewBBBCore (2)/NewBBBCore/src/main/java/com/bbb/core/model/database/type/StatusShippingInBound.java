package com.bbb.core.model.database.type;

public enum StatusShippingInBound {
    INVALID_TRACKING_NUMBER("Invalid tracking number"),
    SHIPPED("Shipped"),
    DELIVERED("Delivered"),
    RETURNED("Returned"),
    CANCELLED("Cancelled");

    private final String value;

    StatusShippingInBound(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StatusShippingInBound findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "Invalid tracking number":
                return INVALID_TRACKING_NUMBER;
            case "Shipped":
                return SHIPPED;
            case "Delivered":
                return DELIVERED;
            case "Cancelled":
                return CANCELLED;
            case "Returned":
                return RETURNED;
            default:
                return null;
        }
    }
}
