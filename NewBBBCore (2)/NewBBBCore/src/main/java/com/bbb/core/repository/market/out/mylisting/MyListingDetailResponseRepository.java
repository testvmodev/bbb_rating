package com.bbb.core.repository.market.out.mylisting;

import com.bbb.core.model.response.market.mylisting.MyListingDetailResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MyListingDetailResponseRepository extends JpaRepository<MyListingDetailResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "inventory.id AS inventory_id, " +
            "inventory.bicycle_id AS bicycle_id, " +
            "inventory_bicycle.model_id  AS model_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.year_id AS year_id, " +
            "inventory_bicycle.type_id AS bicycle_type_id, " +
            "inventory_bicycle.size_id AS bicycle_size_id, " +
//            "bicycle_model.id  AS model_id, " +
//            "bicycle_brand.id AS brand_id, " +
//            "bicycle_year.id AS year_id, " +
//            "bicycle_type.id AS bicycle_type_id, " +
//            "bicycle_size.id AS bicycle_size_id, " +
            "inventory.type_id AS inventory_type_id, " +
            "inventory.serial_number AS serial_number, " +
            "bicycle.name AS bicycle_name, " +
            "inventory.bicycle_model_name AS bicycle_model_name, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
            "inventory.bicycle_year_name AS bicycle_year_name, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "market_listing.market_place_id AS market_place_id, " +
            "market_place.type AS type, " +

            "inventory.image_default AS image_default, " +

            "brake_type.inventory_comp_type_id AS brake_type_id, " +
            "brake_type.brake_type_name AS brake_name, " +
            "frame_material.inventory_comp_type_id AS frame_material_id, " +
            "frame_material.fragment_material_name AS frame_material_name, " +

            "market_listing.status AS status_market_listing," +
            "market_listing.is_best_offer AS is_best_offer, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.description AS description, " +
            "inventory.name AS inventory_name,  " +
            "inventory.type_id AS type_inventory_id, " +
            "inventory_type.name AS type_inventory_name, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "market_listing.best_offer_auto_accept_price AS best_offer_auto_accept_price, " +
            "market_listing.minimum_offer_auto_accept_price AS minimum_offer_auto_accept_price, " +
            "inventory.condition AS condition, " +
            "inventory_size.inventory_comp_type_id AS inventory_size_id, " +
            "inventory_size.size_name AS inventory_size_name, " +

            "inventory.title AS title, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.is_delete AS is_delete, " +
            "inventory.e_bike_hours AS e_bike_hours, " +
            "inventory.e_bike_mileage AS e_bike_mileage," +

            "market_listing.paypal_email_seller AS paypal_email_seller, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.address_line AS address_line " +

            "FROM market_listing " +

            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "JOIN inventory_type " +
                "ON inventory_type.id = inventory.type_id " +
                "AND inventory_type.name = :#{T(com.bbb.core.model.database.type.InventoryTypeValue).PTP} " +
            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "JOIN bicycle_model ON bicycle.model_id = bicycle_model.id " +
//            "JOIN bicycle_brand ON bicycle.brand_id = bicycle_brand.id " +
//            "JOIN bicycle_year ON bicycle.year_id = bicycle_year.id " +
//            "JOIN bicycle_type ON inventory.bicycle_type_name = bicycle_type.name " +
//            "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +
            "JOIN market_place ON market_listing.market_place_id = market_place.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS size_name " +
                "FROM inventory_comp_detail " +
                "JOIN inventory_comp_type " +
                "ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME}) " +
                "AS inventory_size " +
            "ON inventory.id = inventory_size.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) " +
                "AS brake_type " +
            "ON inventory.id = brake_type.inventory_id " +

            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS fragment_material_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) " +
                "AS frame_material " +
            "ON inventory.id = frame_material.inventory_id " +

            "WHERE market_listing.id = :marketListingId " +
            "AND market_listing.is_delete = 0"
    )
    MyListingDetailResponse findOne(
            @Param("marketListingId") long marketListingId
    );
}
