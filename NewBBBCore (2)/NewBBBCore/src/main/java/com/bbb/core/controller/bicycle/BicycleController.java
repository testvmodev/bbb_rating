package com.bbb.core.controller.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycle.*;
import com.bbb.core.model.request.sort.BicycleSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.service.bicycle.BicycleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static com.bbb.core.common.Constants.URL_GET_BICYCLE;

@RestController
public class BicycleController {
    private final BicycleService service;

    @Autowired
    public BicycleController(BicycleService service) {
        this.service = service;
    }

    /**
     * @param pageable
     * @return
     */
    @GetMapping(value = URL_GET_BICYCLE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getBicycles(
            @RequestParam(value = "brandName", required = false) String brandName,
            @RequestParam(value = "modelName", required = false) String modelName,
            @RequestParam(value = "brandId", required = false) Long brandId,
            @RequestParam(value = "modelId", required = false) Long modelId,
            @RequestParam(value = "typeId", required = false) Long typeId,
            @RequestParam(value = "fromYearId", required = false) Long fromYearId,
            @RequestParam(value = "toYearId", required = false) Long toYearId,
            @RequestParam(value = "missImage", defaultValue = "false") Boolean missImage,
            @RequestParam(value = "missRetailPrice", defaultValue = "false") Boolean missRetailPrice,
            @RequestParam(value = "sortField", defaultValue = "ID") BicycleSortField bicycleSortField,
            @RequestParam(value = "sortType", defaultValue = "ASC") Sort sortType,
            Pageable pageable
    ) throws ExceptionResponse {
        BicycleFilterRequest filter = new BicycleFilterRequest(brandId, modelId, typeId, fromYearId, toYearId, missImage, missRetailPrice);
        filter.setBrandName(brandName);
        filter.setModelName(modelName);
        BicycleGetRequest request = new BicycleGetRequest();
        request.setFilter(filter);
        request.setPageable(pageable);
        request.setSortField(bicycleSortField);
        request.setSortType(sortType);

        return new ResponseEntity<>(service.getBicycles(request),
                HttpStatus.OK);
    }

    @PostMapping(value = Constants.URL_BICYCLE_CREATE)
    public ResponseEntity createBicycle(
            @RequestParam(value = "brandId") long brandId,
            @RequestParam(value = "modelId") long modelId,
            @RequestParam(value = "typeId") long typeId,
            @RequestParam(value = "yearId") long yearId,
            @RequestParam(value = "sizeId") long sizeId,
            @RequestParam(value = "retailPrice") float retailPrice,
            @RequestParam(value = "images") List<String> images,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "name") String name,
            HttpServletRequest req
    ) throws ExceptionResponse {
        BicycleCreateRequest request = new BicycleCreateRequest();
        request.setBrandId(brandId);
        request.setModelId(modelId);
        request.setTypeId(typeId);
        request.setYearId(yearId);
        request.setRetailPrice(retailPrice);
        request.setSizeId(sizeId);
        if (images == null) {
            images = new ArrayList<>();
        }
        request.setImages(images);
        request.setDescription(description);
        request.setName(name);
        return new ResponseEntity<>(service.createBicycle(request),
                HttpStatus.OK);
    }

    @PutMapping(value = Constants.URL_BICYCLE)
    public ResponseEntity updateBicycle(
            @PathVariable(value = Constants.ID) long bicycleId,
            @RequestBody BicycleUpdateRequest bicycleUpdateRequest
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.updateBicycle(bicycleId, bicycleUpdateRequest),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_BICYCLE)
    public ResponseEntity getBicycleDeatil(
            @PathVariable(value = Constants.ID) long bicycleId
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getBicycleDetail(bicycleId),
                HttpStatus.OK);
    }

    @DeleteMapping(value = Constants.URL_BICYCLE)
    public ResponseEntity deleteBicycle(
            @PathVariable(value = Constants.ID) long id
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.deleteBicycle(id),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_BICYCLE_BRAND_YEAR_MODEL)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getBicyclesFromBrandYearModel(
            @RequestParam(value = "brandId", required = false) Long brandId,
            @RequestParam(value = "yearId", required = false) Long yearId,
            @RequestParam(value = "modelId", required = false) Long modelId,
            Pageable page
    ) throws ExceptionResponse {
        BicycleFromBrandYearModelRequest request = new BicycleFromBrandYearModelRequest();
        request.setBrandId(brandId);
        request.setYearId(yearId);
        request.setModelId(modelId);
        return new ResponseEntity<>(service.getBicyclesFromBrandYearModel(request, page),
                HttpStatus.OK);
    }


    @GetMapping(value = Constants.URL_GET_BICYCLE_CONTENT)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query"),
    })
    public ResponseEntity getBicyclesFromBrandYearModel(
            @RequestParam(value = "content") String content,
            Pageable page
    ) throws ExceptionResponse {
        return new ResponseEntity<>(service.getBicyclesFromContent(content, page),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_GET_BICYCLE_CONDITION)
    public ResponseEntity getAllCondition() throws ExceptionResponse{
        return new ResponseEntity<>(service.getAllCondition(),
                HttpStatus.OK);
    }

//    @GetMapping(value = "/importBicycleFromOlbDb")
    public ResponseEntity importBicycleFromOlbDb(
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    ) {
        service.importBicycleFromOlbDb(offset, size);
        return new ResponseEntity<>("SUCCESS",
                HttpStatus.OK);
    }

//    @GetMapping(value = "/removeImageIncorrect")
    public ResponseEntity removeImageIncorrect(){
        return new ResponseEntity<>(service.removeImageIncorrect(),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_BICYCLE_SEARCH_BASE_COMPONENT)
    public ResponseEntity getBicycleSearchBaseComponent() throws ExceptionResponse{
        return new ResponseEntity<>(service.getBicycleSearchBaseComponent(),
                HttpStatus.OK);
    }

    @GetMapping(value = Constants.URL_BICYCLE_RECOMMENDS)
    public ResponseEntity getBicycleRecommends(
            @RequestParam(value = "bicycleId") long bicycleId
    )throws ExceptionResponse{
        return new ResponseEntity<>(service.getBicycleRecommends(bicycleId),
                HttpStatus.OK);
    }

    @GetMapping(Constants.URL_BICYCLE_YEARS)
    public ResponseEntity getBicycleYearFromModel(
            @RequestParam("brandId") long brandId,
            @RequestParam("modelId") long modelId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getBicycleYearFromModel(brandId, modelId));
    }

    @GetMapping(Constants.URL_BICYCLE_VALUE)
    public ResponseEntity getPrivatePartyValues(
            @RequestParam(value = "brandId") long brandId,
            @RequestParam(value = "modelId") long modelId,
            @RequestParam(value = "yearId") long yearId
    ) throws ExceptionResponse {
        return ResponseEntity.ok(service.getValueGuidePrices(brandId, modelId, yearId));
    }
}
