package com.bbb.core.model.response.ebay;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class ErrorParameter {
    @JacksonXmlProperty(localName = "ParamID", isAttribute = true)
    private long paramID;
    @JacksonXmlProperty(localName = "Value")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getParamID() {
        return paramID;
    }

    public void setParamID(long paramID) {
        this.paramID = paramID;
    }
}
