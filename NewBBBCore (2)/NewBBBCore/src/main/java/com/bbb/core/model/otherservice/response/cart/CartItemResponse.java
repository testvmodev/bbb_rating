package com.bbb.core.model.otherservice.response.cart;

import com.bbb.core.model.database.type.ConditionInventory;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusInventory;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CartItemResponse {

    /**
     * offer_id : 368
     * bicycle_id : 3157221
     * title : 2012 lantest string
     * condition : EXCELLENT
     * bicycle_year_name : 2012
     * bicycle_type_name : Road
     * bicycle_brand_name : lantest
     * status : ACTIVE
     * inventory_id : 1159
     * name : INV-1159
     * local_pickup : true
     * bbb_value : 900
     * image_default : https://s3.amazonaws.com/bbb-v2-staging/original/marketListingPTP/marketListingPTP_305_c6cahh.jpg
     * cart_type : offer
     * bicycle_model_name : string
     * type_id : 8
     * currency : usd
     * price_offer : 800
     * current_listed_price : 900
     * images : "[]"
     * market_listing_id : 305
     * insurance : 0
     * initial_list_price : 900
     * shipping : 0
     * seller_id : 5bd16fd622d6360010aa7cd1
     * tax : 58
     * stage : SALE_PENDING
     * cogs_price : 900
     * quantity : 1
     * discounted_price : 900
     */

    private long offer_id;
    private long bicycle_id;
    private String title;
    private ConditionInventory condition;
    private String bicycle_year_name;
    private String bicycle_type_name;
    private String bicycle_brand_name;
    private StatusInventory status;
    private long inventory_id;
    private String name;
    @JsonProperty(value = "local_pickup")
    private boolean isLocalPickup;
    private Float bbb_value;
    private String image_default;
    private String cart_type;
    private String bicycle_model_name;
    private long type_id;
    private Float currency;
    private Float price_offer;
    private Float current_listed_price;
    private String images;
    private long market_listing_id;
    private Float insurance;
    private Float initial_list_price;
    private String shipping;
    private String seller_id;
    private Float tax;
    private StageInventory stage;
    private Float cogs_price;
    private Long quantity;
    private Float discounted_price;

    public long getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(long offer_id) {
        this.offer_id = offer_id;
    }

    public long getBicycle_id() {
        return bicycle_id;
    }

    public void setBicycle_id(long bicycle_id) {
        this.bicycle_id = bicycle_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public String getBicycle_year_name() {
        return bicycle_year_name;
    }

    public void setBicycle_year_name(String bicycle_year_name) {
        this.bicycle_year_name = bicycle_year_name;
    }

    public String getBicycle_type_name() {
        return bicycle_type_name;
    }

    public void setBicycle_type_name(String bicycle_type_name) {
        this.bicycle_type_name = bicycle_type_name;
    }

    public String getBicycle_brand_name() {
        return bicycle_brand_name;
    }

    public void setBicycle_brand_name(String bicycle_brand_name) {
        this.bicycle_brand_name = bicycle_brand_name;
    }

    public StatusInventory getStatus() {
        return status;
    }

    public void setStatus(StatusInventory status) {
        this.status = status;
    }

    public long getInventory_id() {
        return inventory_id;
    }

    public void setInventory_id(long inventory_id) {
        this.inventory_id = inventory_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLocalPickup() {
        return isLocalPickup;
    }

    public void setLocalPickup(boolean localPickup) {
        isLocalPickup = localPickup;
    }

    public Float getBbb_value() {
        return bbb_value;
    }

    public void setBbb_value(Float bbb_value) {
        this.bbb_value = bbb_value;
    }

    public String getImage_default() {
        return image_default;
    }

    public void setImage_default(String image_default) {
        this.image_default = image_default;
    }

    public String getCart_type() {
        return cart_type;
    }

    public void setCart_type(String cart_type) {
        this.cart_type = cart_type;
    }

    public String getBicycle_model_name() {
        return bicycle_model_name;
    }

    public void setBicycle_model_name(String bicycle_model_name) {
        this.bicycle_model_name = bicycle_model_name;
    }

    public long getType_id() {
        return type_id;
    }

    public void setType_id(long type_id) {
        this.type_id = type_id;
    }

    public Float getCurrency() {
        return currency;
    }

    public void setCurrency(Float currency) {
        this.currency = currency;
    }

    public Float getPrice_offer() {
        return price_offer;
    }

    public void setPrice_offer(Float price_offer) {
        this.price_offer = price_offer;
    }

    public Float getCurrent_listed_price() {
        return current_listed_price;
    }

    public void setCurrent_listed_price(Float current_listed_price) {
        this.current_listed_price = current_listed_price;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public long getMarket_listing_id() {
        return market_listing_id;
    }

    public void setMarket_listing_id(long market_listing_id) {
        this.market_listing_id = market_listing_id;
    }

    public Float getInsurance() {
        return insurance;
    }

    public void setInsurance(Float insurance) {
        this.insurance = insurance;
    }

    public Float getInitial_list_price() {
        return initial_list_price;
    }

    public void setInitial_list_price(Float initial_list_price) {
        this.initial_list_price = initial_list_price;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public Float getTax() {
        return tax;
    }

    public void setTax(Float tax) {
        this.tax = tax;
    }

    public StageInventory getStage() {
        return stage;
    }

    public void setStage(StageInventory stage) {
        this.stage = stage;
    }

    public Float getCogs_price() {
        return cogs_price;
    }

    public void setCogs_price(Float cogs_price) {
        this.cogs_price = cogs_price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Float getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(Float discounted_price) {
        this.discounted_price = discounted_price;
    }
}
