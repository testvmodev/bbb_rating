package com.bbb.core.model.request.bid.offer;

import org.springframework.data.domain.Pageable;

public class UserBidRequest {
    private String userId;
    private Pageable pageable;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }
}
