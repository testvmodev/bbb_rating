package com.bbb.core.model.request.ebay.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ContainingOrder {
    @JacksonXmlProperty(localName = "OrderID")
    private String orderID;
    @JacksonXmlProperty(localName = "OrderStatus")
    private String orderStatus;
    @JacksonXmlProperty(localName = "CancelStatus")
    private String cancelStatus;
    @JacksonXmlProperty(localName = "ExtendedOrderID")
    private String extendedOrderID;
    @JacksonXmlProperty(localName = "ContainseBayPlusTransaction")
    private boolean containseBayPlusTransaction;

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getExtendedOrderID() {
        return extendedOrderID;
    }

    public void setExtendedOrderID(String extendedOrderID) {
        this.extendedOrderID = extendedOrderID;
    }

    public boolean isContainseBayPlusTransaction() {
        return containseBayPlusTransaction;
    }

    public void setContainseBayPlusTransaction(boolean containseBayPlusTransaction) {
        this.containseBayPlusTransaction = containseBayPlusTransaction;
    }
}
