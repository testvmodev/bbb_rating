package com.bbb.core.manager.bicycle;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.aws.s3.S3Manager;
import com.bbb.core.common.aws.s3.S3Value;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.s3.S3UploadImage;
import com.bbb.core.common.utils.s3.S3UploadResponse;
import com.bbb.core.common.utils.s3.S3UploadUtils;
import com.bbb.core.model.database.BicycleBrand;
import com.bbb.core.model.request.sort.BicycleBrandSortField;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.bicycle.BrandResponse;
import com.bbb.core.model.request.brand.BrandCreateRequest;
import com.bbb.core.model.request.brand.BrandUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.MessageResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.BicycleBrandRepository;
import com.bbb.core.repository.bicycle.BicycleRepository;
import com.bbb.core.repository.bicyclemodel.BicycleModelRepository;
import com.bbb.core.repository.reout.BicycleBrandResponseRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.text.DecimalFormat;

@Component
public class BrandManager implements MessageResponses {
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private BicycleBrandRepository bicycleBrandRepository;
    @Autowired
    private BicycleRepository bicycleRepository;
    @Autowired
    private BicycleModelRepository bicycleModelRepository;
    @Autowired
    private S3Manager s3Manager;
    @Autowired
    private AwsResourceConfig awsResourceConfig;
    @Autowired
    private BicycleBrandResponseRepository responseRepository;

    //region API
    public Object getBrands(String nameSearch, Sort sortType, BicycleBrandSortField sortField, Pageable pageable) throws ExceptionResponse {
        if (pageable.getPageSize()<=0||pageable.getPageNumber()<0){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, PAGE_OR_SIZE_INVALID),
                    HttpStatus.NOT_FOUND
            );
        }
        ListObjResponse<BrandResponse> response = new ListObjResponse<>();

        if (StringUtils.isBlank(nameSearch)) {
            response.setData(
                    responseRepository.findAllBicycleBrand(null,sortType, sortField, pageable.getOffset(), pageable.getPageSize())
//                            .stream().map(ContentCommon::getId).collect(Collectors.toList())
            );
            response.setTotalItem(bicycleBrandRepository.getTotalCount(null));
        } else {
            response.setData(
                    responseRepository.findAllBicycleBrand("%" + nameSearch + "%",sortType, sortField, pageable.getOffset(), pageable.getPageSize())
//                            .stream().map(ContentCommon::getId).collect(Collectors.toList())
            );
            response.setTotalItem(bicycleBrandRepository.getTotalCount("%" + nameSearch + "%"));
        }
        response.setPage(pageable.getPageNumber());
        response.setPageSize(pageable.getPageSize());
        response.updateTotalPage();
        return response;
    }

    public Object createBrand(BrandCreateRequest brand) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        return createBrandIgnoreRole(brand);
    }

    public BicycleBrand createBrandIgnoreRole(BrandCreateRequest brand) throws ExceptionResponse {
        if (brand == null || !brand.validateBrand()) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, BRAND_NOT_EMPTY),
                    HttpStatus.BAD_REQUEST);
        }
        if(bicycleBrandRepository.findOneByName(brand.getName().trim())!= null){
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, BRAND_NAME_EXIST),
                    HttpStatus.BAD_REQUEST);
        }
        if (brand.getValueModifier() == null) {
            brand.setValueModifier(0.0f);
        }
        if (brand.getValueModifier() < 0.0 || brand.getValueModifier() > 100) {
            throw new ExceptionResponse(new ObjectError(
                    ObjectError.ERROR_PARAM, VALUE_MODIFIER_INVALID),
                    HttpStatus.BAD_REQUEST);
        }
        DecimalFormat format = new DecimalFormat("##.##");
        BicycleBrand bicycleBrand = new BicycleBrand();
        bicycleBrand.setName(brand.getName().trim());
        bicycleBrand.setApproved(false);
        bicycleBrand.setValueModifier(Float.valueOf(format.format(brand.getValueModifier())));
        bicycleBrand = bicycleBrandRepository.save(bicycleBrand);
        if (brand.getBrandLogo() != null) {
            String contentType = brand.getBrandLogo().getContentType();
            if (contentType.equals("image/jpeg") || contentType.equals("image/png")){
                bicycleBrand.setBrandLogo(S3UploadUtils.getFullLinkS3(
                        awsResourceConfig,
                        uploadLogo(bicycleBrand, brand.getBrandLogo()).getFullKey())
                );
            }else {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, IMAGE_NOT_FORMAT),
                        HttpStatus.BAD_REQUEST);
            }
        }
        return bicycleBrandRepository.save(bicycleBrand);
    }


    public Object deleteBrand(long id) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleBrand bicycleBrand = bicycleBrandRepository.findOneNotDelete(id);
        if (bicycleBrand == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, BRAND_NOT_EXIST),
                    HttpStatus.NOT_FOUND
            );
        }
        int countBrand = bicycleRepository.getCountBrand(bicycleBrand.getId());
        if ( countBrand > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM,(countBrand==1?THERE_IS:THERE_ARE+countBrand )+ BICYCLE_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        int countModel = bicycleModelRepository.getCount(bicycleBrand.getId());
        if (countModel > 0) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM,(countModel==1?THERE_IS:THERE_ARE+countModel)+ MODEL_USING_OBJECT),
                    HttpStatus.NOT_FOUND);
        }
        bicycleBrand.setDelete(true);
        bicycleBrandRepository.save(bicycleBrand);
        return new MessageResponse(SUCCESS);
    }

    public Object getBrandDetail(long brandId) throws ExceptionResponse {
        BicycleBrand brand = bicycleBrandRepository.findOneNotDelete(brandId);
        if (brand == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }
        return brand;
    }

    public Object updateBrand(long brandId, BrandUpdateRequest brand) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        if (brand == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, BRAND_NOT_EMPTY),
                    HttpStatus.NOT_FOUND);
        }

        BicycleBrand oldBrand = bicycleBrandRepository.findOneNotDelete(brandId);

        if (oldBrand == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, BRAND_ID_NOT_EXIST),
                    HttpStatus.BAD_REQUEST);
        }
        if (brand.getValueModifier() != null) {
            if (brand.getValueModifier() < 0 || brand.getValueModifier() > 100) {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, VALUE_MODIFIER_INVALID),
                        HttpStatus.BAD_REQUEST);
            }
            DecimalFormat format = new DecimalFormat("##.##");
            oldBrand.setValueModifier(Float.valueOf(format.format(brand.getValueModifier())));
        }

        if (!StringUtils.isBlank(brand.getName())) {
            if (!brand.getName().trim().equals(oldBrand.getName()) ){
                if(bicycleBrandRepository.findOneByName(brand.getName())!= null){
                    throw new ExceptionResponse(new ObjectError(
                            ObjectError.ERROR_PARAM, BRAND_NAME_EXIST),
                            HttpStatus.BAD_REQUEST);
                }
                oldBrand.setName(brand.getName().trim());
            }
        }
        if (brand.getApproved() != null) {
            oldBrand.setApproved(brand.getApproved());
        }
        if (brand.getBrandLogo() != null){
            String contentType = brand.getBrandLogo().getContentType();
            if (contentType.equals("image/jpeg") || contentType.equals("image/png")){
                oldBrand.setBrandLogo(S3UploadUtils.getFullLinkS3(
                        awsResourceConfig,
                        uploadLogo(oldBrand, brand.getBrandLogo()).getFullKey())
                );
            }else {
                throw new ExceptionResponse(new ObjectError(
                        ObjectError.ERROR_PARAM, IMAGE_NOT_FORMAT),
                        HttpStatus.BAD_REQUEST);
            }
        }
        return bicycleBrandRepository.save(oldBrand);
    }

    public Object deleteLogoBrand(long id) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        BicycleBrand bicycleBrand = bicycleBrandRepository.findOneNotDelete(id);
        bicycleBrand.setBrandLogo(null);
        bicycleBrandRepository.save(bicycleBrand);
        return new MessageResponse(SUCCESS);
    }
    //endregions

    private S3UploadResponse<Long> uploadLogo(BicycleBrand bicycleBrand, MultipartFile image) throws ExceptionResponse {
        S3UploadImage<Long> upload = new S3UploadImage<>(
                Constants.BRAND_LOGO_FOLDER + "_" + bicycleBrand.getId(),
                S3Value.FOLDER_BRAND_LOGO_ORIGINAL,
                image,
                bicycleBrand.getId()
        );
        return S3UploadUtils.upload(s3Manager, upload);
    }

    //util

    /**
     *
     * @param id
     * @return not deleted brand
     */
    public BicycleBrand findBrand(long id) throws ExceptionResponse {
        BicycleBrand bicycleBrand = bicycleBrandRepository.findOneNotDelete(id);
        if (bicycleBrand == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.BRAND_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return bicycleBrand;
    }

    public BicycleBrand findBrand(String name) throws ExceptionResponse {
        BicycleBrand bicycleBrand = bicycleBrandRepository.findOneByName(name);
        if (bicycleBrand == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.BRAND_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return bicycleBrand;
    }

    public BicycleBrand findBrandIgnoreDelete(long id) throws ExceptionResponse {
        BicycleBrand bicycleBrand = bicycleBrandRepository.findOne(id);
        if (bicycleBrand == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MessageResponses.BRAND_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }
        return bicycleBrand;
    }

    public Object getAllBrands() throws ExceptionResponse{
        return bicycleBrandRepository.findAll();
    }
}
