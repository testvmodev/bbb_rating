package com.bbb.core.model.response.common;

import com.bbb.core.model.database.type.MarketListingFilterTypeTable;

import java.util.List;

public class MarketListingFilterResponse {
    private Long id;
    private String name;
    private List<OperatorMarketListingFilter> operators;
    private List<String> values;
    private List<ContentDoubleString> valueNames;

    private MarketListingFilterTypeTable typeTable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public MarketListingFilterTypeTable getTypeTable() {
        return typeTable;
    }

    public void setTypeTable(MarketListingFilterTypeTable typeTable) {
        this.typeTable = typeTable;
    }

    public List<OperatorMarketListingFilter> getOperators() {
        return operators;
    }

    public void setOperators(List<OperatorMarketListingFilter> operators) {
        this.operators = operators;
    }

    public List<ContentDoubleString> getValueNames() {
        return valueNames;
    }

    public void setValueNames(List<ContentDoubleString> valueNames) {
        this.valueNames = valueNames;
    }
}
