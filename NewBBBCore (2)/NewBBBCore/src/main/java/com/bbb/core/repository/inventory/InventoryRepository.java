package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.Inventory;
import com.bbb.core.model.database.type.StageInventory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE id = ?1")
    Inventory findOne(long id);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory")
    List<Inventory> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.id IN :inventoryIds")
    List<Inventory> findAllByIds(@Param(value = "inventoryIds") List<Long> inventoryIds);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory " +
            "WHERE " +
            "inventory.id IN :inventoryIds AND " +
            "inventory.is_delete = 0 AND " +
            "inventory.status = :#{T(com.bbb.core.model.database.type.StatusInventory).ACTIVE.getValue()} AND " +
            "inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).READY_LISTED.getValue()}")
    List<Inventory> findAllByIdsActiveAndReadyToBeListed(@Param(value = "inventoryIds") List<Long> inventoryIds);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.id = :id AND inventory.is_delete = 0")
    Inventory findOneNotDelete(@Param(value = "id") long id);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.id = :inventoryId")
    Inventory findFirstById(@Param("inventoryId") long inventoryId);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory " +
            "WHERE inventory.score_card_id = :scoreCardId")
    Inventory findByScoreCardId(@Param("scoreCardId") long scoreCardId);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.size_id IS NOT NULL")
    List<Inventory> findAllInventorySizeNotNull();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.brake_type_id IS NOT NULL")
    List<Inventory> findAllInventoryBrakeTypeNotNull();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.frame_material_id IS NOT NULL")
    List<Inventory> findAllInventoryFrameMaterialNotNull();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE inventory.color IS NOT NULL")
    List<Inventory> findAllInventoryColorNotNull();

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.name = :inventoryName WHERE inventory.id = :inventoryId")
    void updateName(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "inventoryName") String inventoryName
    );


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.search_guide_recommendation = :recommendation WHERE inventory.id = :inventoryId")
    void updateSearchGuideRecommendation(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "recommendation") String recommendation
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.stage = :stage " +
            "WHERE " +
            "inventory.id IN :inventoryIds AND " +
            "inventory.stage <> :stage"
    )
    void updateStage(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "stage") String stage
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.stage = :stage WHERE " +
            "inventory.id = :inventoryId AND " +
            "inventory.stage <> :stage"
    )
    void updateStage(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "stage") String stage
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.status = :status " +
            "WHERE " +
            "inventory.id IN :inventoryIds AND " +
            "inventory.status <> :status"
    )
    void updateStatus(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "status") String status
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory SET inventory.status = :status " +
            "WHERE " +
            "inventory.id = :inventoryId AND " +
            "inventory.status <> :status"
    )
    void updateStatus(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "status") String status
    );


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory " +
            "SET inventory.status = :status, inventory.stage = :stage " +
            "WHERE " +
            "inventory.id IN :inventoryIds AND " +
            "(inventory.status <> :status OR inventory.stage <> :stage)")
    void updateStatusAndStage(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "status") String status,
            @Param(value = "stage") String stage
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE inventory " +
            "SET inventory.status = :status, inventory.stage = :stage " +
            "WHERE " +
            "inventory.id = :inventoryId AND " +
            "(inventory.status <> :status OR inventory.stage <> :stage)"
    )
    void updateStatusAndStage(
            @Param(value = "inventoryId") long inventoryId,
            @Param(value = "status") String status,
            @Param(value = "stage") String stage
    );


    @Query(nativeQuery = true, value = "SELECT COUNT(inventory.id) " +
            "FROM inventory JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "WHERE " +
            "inventory.id IN :inventoriesIds AND " +
            "inventory.is_delete = 0 AND " +
            "inventory.status = 'Active' AND " +
            "(inventory.stage = 'Listed' OR inventory.stage = 'Ready to be Listed') AND " +
            "inventory.name <> :#{T(com.bbb.core.common.ValueCommons).PTP}")
    int countInventoriesListingWizard(
            @Param(value = "inventoriesIds") List<Long> inventoriesIds
    );

    @Query(nativeQuery = true, value = "SELECT inventory.id FROM " +
            "inventory " +
            "WHERE inventory.id IN :inventoriesId AND " +
            "inventory.id NOT IN " +
            "(SELECT market_listing.inventory_id " +
            "FROM market_listing WHERE market_listing.inventory_id IN :inventoriesId AND market_listing.status = 'Listed' " +
            ")"
    )
    List<Integer> getAllInventoryNotInMarketListing(
            @Param(value = "inventoriesId") List<Long> inventoriesId
    );

    @Query(nativeQuery = true, value = "SELECT CASE WHEN " +
            "EXISTS(" +
            "SELECT * FROM inventory JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "WHERE " +
            "inventory.id = :inventoryId AND " +
            "inventory_type.name <> :#{T(com.bbb.core.common.ValueCommons).PTP} AND " +
            "inventory.is_delete = 0 " +
            ")" +
            "THEN " +
            "CAST(1 AS BIT) " +
            "ELSE CAST(0 AS BIT) END"
    )
    boolean checkExistInventoryCanListAuction(
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "CASE WHEN " +
                    "(SELECT COUNT(inventory.id) FROM " +
                    "inventory JOIN inventory_type ON inventory.type_id = inventory_type.id " +
                    "WHERE inventory.id IN :inventoryIds AND inventory_type.name = :#{T(com.bbb.core.common.ValueCommons).PTP}) > 0 " +
                    "THEN CAST(1 AS BIT) " +
                    "ELSE CAST(0 AS BIT) " +
                    "END "
    )
    boolean checkExistInventoryPTP(
            @Param(value = "inventoryIds") List<Long> inventoryIds
    );

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value =
            "UPDATE inventory " +
                    "SET " +
                    "inventory.status = :status, " +
                    "inventory.stage = :stage " +
                    "WHERE " +
                    "inventory.id IN :inventoryIds AND " +
                    "inventory.id NOT IN " +
                    "(SELECT market_listing.inventory_id " +
                    "FROM " +
                    "market_listing " +
                    "WHERE " +
                    "market_listing.inventory_id IN :inventoryIds AND " +
                    "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND " +
                    "market_listing.is_delete = 0" +
                    ") AND " +
                    "inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).LISTED.getValue()} AND " +
                    "inventory.is_delete = 0")
    void updateStatusAndStageFromInventoryIdMarketListingEnd(
            @Param(value = "inventoryIds") List<Long> inventoryIds,
            @Param(value = "status") String status,
            @Param(value = "stage") String stage
    );

    @Query(nativeQuery = true, value = "SELECT inventory.* FROM market_listing " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE market_listing.id = :marketListingId")
    Inventory findByMarketListingId(@Param("marketListingId") long marketListingId);

    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM inventory " +
            "WHERE inventory.salesforce_id = :salesforceId " +
            "AND (:isDelete IS NULL OR inventory.is_delete = :isDelete) " +
            "ORDER BY inventory.id DESC"
    )
    Inventory findLastSalesforceInventory(
            @Param("salesforceId") String salesforceId,
            @Param("isDelete") Boolean isDelete
    );

//    @Query(nativeQuery = true, value = "SELECT inventory.salesforce_id FROM " +
//            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
//            "WHERE market_listing.id = :marketListingId and market_listing.is_delete = 0 and inventory.is_delete = 0")
//    String findSalesforceIdFromMarketListingId(
//            @Param(value = "marketListingId") long marketListingId
//    );

//    @Query(nativeQuery = true, value = "SELECT inventory.salesforce_id FROM " +
//            "offer JOIN inventory_auction ON offer.inventory_auction_id = inventory_auction.id " +
//            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +
//            "WHERE offer.id = :offerId and offer.is_delete = 0 and inventory.is_delete = 0")
//    String findSalesforceIdFromOfferId(
//            @Param(value = "offerId") long offerId
//    );
}
