package com.bbb.core.model.request.bid.offer;


import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.response.ObjectError;
import org.springframework.http.HttpStatus;

public class OfferCreateRequest {
    private String buyerId;
    private float offerPrice;
    private Long inventoryId;
    private Long auctionId;
    private Long inventoryAuctionId;
    private Long marketListingId;

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public float getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(float offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getInventoryAuctionId() {
        return inventoryAuctionId;
    }

    public void setInventoryAuctionId(Long inventoryAuctionId) {
        this.inventoryAuctionId = inventoryAuctionId;
    }

    public Long getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(Long marketListingId) {
        this.marketListingId = marketListingId;
    }

    public void validate() throws ExceptionResponse {
        String error = null;
        if (inventoryAuctionId != null && auctionId != null) {
            error = MessageResponses.DEPRECATED_OFFER_API_INVALID_USAGE;
        }
        if (marketListingId != null && inventoryId != null) {
            error = MessageResponses.DEPRECATED_OFFER_API_INVALID_USAGE;
        }
        if (auctionId != null && inventoryId == null) {
            error = MessageResponses.DEPRECATED_OFFER_API_INVALID_USAGE;
        }
        if (inventoryAuctionId != null && marketListingId != null) {
            error = MessageResponses.DEPRECATED_OFFER_API_INVALID_USAGE_BOTH_ID;
        }
        if (inventoryId == null && auctionId == null &&
                inventoryAuctionId == null && marketListingId == null
        ) {
            error = MessageResponses.DEPRECATED_OFFER_API_INVALID_USAGE_NO_ID;
        }
        if (error != null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    error,
                    HttpStatus.BAD_REQUEST
            );
        }
    }
}
