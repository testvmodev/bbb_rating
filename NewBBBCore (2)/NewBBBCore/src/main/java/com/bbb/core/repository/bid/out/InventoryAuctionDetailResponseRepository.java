package com.bbb.core.repository.bid.out;

import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionDetailResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;

import java.util.List;

public interface InventoryAuctionDetailResponseRepository extends CrudRepository<InventoryAuctionDetailResponse, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.auction_id AS auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.min AS min, " +
            "inventory_auction.created_time AS created_time, " +
            "auction.name AS auction_name, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "inventory_auction.has_buy_it_now AS has_buy_it_now, " +
            "inventory_auction.buy_it_now AS buy_it_now, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
            "auction.is_expired AS is_expire, " +


            "(CASE WHEN offerResult.number_bids IS NULL THEN 0 ELSE offerResult.number_bids END) AS number_bids, " +
            "offerResult.current_highest_bid AS current_highest_bid, " +

            "inventory.title AS inventory_title, " +
            "inventory.image_default AS image_default, " +

            "bicycle.id AS bicycle_id, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.model_id AS model_id, " +
            "inventory_bicycle.year_id AS year_id, " +
            "bicycle.name AS bicycle_name, " +
//            "inv_model.id AS model_id, " +
            "inventory.bicycle_model_name as bicycle_model_name, " +
//            "inv_brand.id AS brand_id, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inv_year.id AS year_id , " +
            "inventory.bicycle_year_name AS bicycle_year_name," +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +

//            "brake_type.brake_type_name AS brake_name, " +
//            "frame_material.frame_material_id AS frame_material_id, " +
//            "frame_material.fragment_material_name AS frame_material_name, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.seller_is_bbb AS seller_is_bbb, " +
            "inventory.stage AS stage, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.value_additional_component AS value_additional_component, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +
            "inventory.description AS inventory_description, " +
            "inventory.is_delete AS is_delete, " +
            "offerResult.max_offer_id AS max_offer_id, " +
            "(CASE WHEN offerResult.number_bidders IS NULL THEN 0 ELSE offerResult.number_bidders END) AS number_bidders, " +

            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.address_line AS address_line, " +

            "shipping_fee_config.ebay_shipping_profile_id AS ebay_shipping_profile_id, " +
            "CASE inventory_location_shipping.shipping_type " +
            "WHEN :#{T(com.bbb.core.model.database.type.OutboundShippingType).FLAT_RATE_TYPE.getValue()} THEN inventory_location_shipping.flat_rate " +
            "ELSE shipping_fee_config.shipping_fee END AS shipping_fee, " +
            "shipping_fee_config.label AS shipping_profile_label, " +
            "shipping_fee_config.description AS shipping_profile_description " +

            "FROM inventory_auction " +

            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +

//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS size_name " +
//                "FROM inventory_comp_detail " +
//                "WHERE inventory_comp_detail.inventory_comp_type_id = :sizeId) " +
//                "AS inventory_size " +
//                "ON inventory.id = inventory_size.inventory_id "+
//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS brake_type_name " +
//                "FROM inventory_comp_detail " +
//                "WHERE inventory_comp_detail.inventory_comp_type_id = :brakeTypeId) " +
//                "AS brake_type " +
//                "ON inventory.id = brake_type.inventory_id "+
//            "LEFT JOIN " +
//                "(SELECT inventory_comp_detail.inventory_comp_type_id AS frame_material_id, " +
//                        "inventory_comp_detail.inventory_id AS inventory_id, " +
//                        "inventory_comp_detail.value AS fragment_material_name " +
//                "FROM inventory_comp_detail " +
//                "WHERE inventory_comp_detail.inventory_comp_type_id = :frameMaterialId) " +
//                "AS frame_material " +
//                "ON inventory.id = frame_material.inventory_id "+

            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
//            "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN shipping_fee_config ON inventory_bicycle.type_id = shipping_fee_config.bicycle_type_id " +

//            "OUTER APPLY " +
//                "(SELECT COUNT(*) AS number_bids, " +
//                        "MAX(offer.offer_price) AS current_highest_bid " +
//                "FROM offer " +
//                "WHERE offer.auction_id = auction.id AND offer.is_delete = 0 AND offer.inventory_id = inventory.id " +
//                ") AS offerResult " +

            "LEFT JOIN " +
                "(SELECT COUNT(*) AS number_bids, " +
                        "COUNT(DISTINCT offer.buyer_id) AS number_bidders, " +
                        "MAX(offer.offer_price) AS current_highest_bid, " +
                        "offer.inventory_auction_id AS inventory_auction_id, " +
                        "MAX(offer.id) AS max_offer_id " +
                "FROM offer " +
                "WHERE offer.inventory_auction_id = :inventoryAuctionId AND offer.is_delete = 0 GROUP BY offer.inventory_auction_id  " +
                ") AS offerResult " +
            "ON offerResult.inventory_auction_id = :inventoryAuctionId " +

//            "LEFT JOIN " +
//                "(SELECT " +
//                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "MAX(offer.offer_price) AS current_highest_bid  " +
//                "FROM offer " +
//                "WHERE " +
//                "offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL AND " +
//                "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                "GROUP BY offer.inventory_auction_id" +
//                ") AS max_offer_price " +
//            "ON inventory_auction.id = max_offer_price.inventory_auction_id " +

            "WHERE inventory_auction.id = :inventoryAuctionId " +
            "AND inventory_auction.is_delete = 0 " +
            "AND (:isInactive IS NULL OR inventory_auction.is_inactive = :isInactive)"
    )
    InventoryAuctionDetailResponse findOne(
            @Param("inventoryAuctionId") long inventoryAuctionId,
            @Param("now") String now,
            @Param("isInactive") Boolean isInactive
//            ,
//            @Param(value = "frameMaterialId") long frameMaterialId,
//            @Param(value = "brakeTypeId") long brakeTypeId,
//            @Param(value = "sizeId") long sizeId
    );


    @Query(nativeQuery = true, value = "SELECT " +
            "inventory_auction.id AS inventory_auction_id, " +
            "inventory_auction.auction_id AS auction_id, " +
            "inventory_auction.inventory_id AS inventory_id, " +
            "inventory_auction.min AS min, " +
            "inventory_auction.created_time AS created_time, " +
            "auction.name AS auction_name, " +
            "auction.start_date AS start_date, " +
            "auction.end_date AS end_date, " +
            "inventory_auction.has_buy_it_now AS has_buy_it_now, " +
            "inventory_auction.buy_it_now AS buy_it_now, " +
            "DATEDIFF_BIG(MILLISECOND, CONVERT(DATETIME, :now), auction.end_date) AS duration, " +
            "auction.is_expired AS is_expire, " +


            "(CASE WHEN offerResult.number_bids IS NULL THEN 0 ELSE offerResult.number_bids END) AS number_bids, " +
            "offerResult.current_highest_bid AS current_highest_bid, " +

            "inventory.title AS inventory_title, " +
            "inventory.image_default AS image_default, " +

            "bicycle.id AS bicycle_id, " +
            "bicycle.name AS bicycle_name, " +
            "inventory_bicycle.brand_id AS brand_id, " +
            "inventory_bicycle.model_id AS model_id, " +
            "inventory_bicycle.year_id AS year_id, " +
//            "inv_model.id AS model_id, " +
            "inventory.bicycle_model_name as bicycle_model_name, " +
//            "inv_brand.id AS brand_id, " +
            "inventory.bicycle_brand_name AS bicycle_brand_name, " +
//            "inv_year.id AS year_id , " +
            "inventory.bicycle_year_name AS bicycle_year_name," +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
//            "inventory.bicycle_size_name AS bicycle_size_name, " +
            "frame_size.frame_size_name AS frame_size, " +

            "inventory.status AS status, " +
            "inventory.partner_id AS partner_id, " +
            "inventory.seller_id AS seller_id, " +
            "inventory.storefront_id AS storefront_id, " +
            "inventory.seller_is_bbb AS seller_is_bbb, " +
            "inventory.stage AS stage, " +
            "inventory.name AS inventory_name, " +
            "inventory.title AS title, " +
            "inventory.msrp_price AS msrp_price, " +
            "inventory.bbb_value AS bbb_value, " +
            "inventory.override_trade_in_price AS override_trade_in_price, " +
            "inventory.initial_list_price AS initial_list_price, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.value_additional_component AS value_additional_component, " +
            "inventory.cogs_price AS cogs_price, " +
            "inventory.flat_price_change AS flat_price_change, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.condition AS condition, " +
            "inventory.serial_number AS serial_number, " +
            "inventory.description AS inventory_description, " +
            "inventory.is_delete AS is_delete, " +
            "offerResult.max_offer_id AS max_offer_id, " +
            "(CASE WHEN offerResult.number_bidders IS NULL THEN 0 ELSE offerResult.number_bidders END) AS number_bidders, " +

            "inventory_location_shipping.is_allow_local_pickup AS is_allow_local_pickup, " +
            "inventory_location_shipping.is_insurance AS is_insurance, " +
            "inventory_location_shipping.flat_rate AS flat_rate, " +
            "inventory_location_shipping.zip_code AS zip_code, " +
            "inventory_location_shipping.country_name AS country_name, " +
            "inventory_location_shipping.country_code AS country_code, " +
            "inventory_location_shipping.state_name AS state_name, " +
            "inventory_location_shipping.city_name AS city_name, " +
            "inventory_location_shipping.shipping_type AS shipping_type, " +
            "inventory_location_shipping.state_code AS state_code, " +
            "inventory_location_shipping.address_line AS address_line, " +

            "shipping_fee_config.ebay_shipping_profile_id AS ebay_shipping_profile_id, " +
            "CASE inventory_location_shipping.shipping_type " +
            "WHEN :#{T(com.bbb.core.model.database.type.OutboundShippingType).FLAT_RATE_TYPE.getValue()} THEN inventory_location_shipping.flat_rate " +
            "ELSE shipping_fee_config.shipping_fee END AS shipping_fee, " +
            "shipping_fee_config.label AS shipping_profile_label, " +
            "shipping_fee_config.description AS shipping_profile_description " +

            "FROM inventory_auction " +

            "JOIN auction ON inventory_auction.auction_id = auction.id " +
            "JOIN inventory ON inventory_auction.inventory_id = inventory.id " +

            "JOIN bicycle ON inventory.bicycle_id = bicycle.id " +
            "JOIN inventory_bicycle ON inventory.id = inventory_bicycle.inventory_id " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_brand " +
//                "WHERE bicycle_brand.name = inventory.bicycle_brand_name " +
//            ") AS inv_brand " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_model " +
//                "WHERE bicycle_model.name = inventory.bicycle_model_name " +
//                "AND bicycle_model.brand_id = inv_brand.id " +
//            ") AS inv_model " +
//            "CROSS APPLY (" +
//                "SELECT TOP 1 * FROM bicycle_year " +
//                "WHERE bicycle_year.name = inventory.bicycle_year_name " +
//            ") AS inv_year " +
//            "JOIN bicycle_type ON bicycle.type_id = bicycle_type.id " +
//            "LEFT JOIN bicycle_size ON inventory.bicycle_size_name = bicycle_size.name " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_comp_type_id AS inventory_comp_type_id, " +
                        "inventory_comp_detail.inventory_id AS inventory_id, " +
                        "inventory_comp_detail.value AS frame_size_name " +
                "FROM inventory_comp_detail JOIN inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_SIZE_NAME}) " +
                "AS frame_size " +
            "ON inventory.id = frame_size.inventory_id " +

            "JOIN inventory_type ON inventory.type_id = inventory_type.id " +
            "JOIN inventory_location_shipping ON inventory.id = inventory_location_shipping.inventory_id " +

            "LEFT JOIN shipping_fee_config ON inventory_bicycle.type_id = shipping_fee_config.bicycle_type_id " +

            "LEFT JOIN " +
                "(SELECT COUNT(*) AS number_bids, " +
                "COUNT(DISTINCT offer.buyer_id) AS number_bidders, " +
                "MAX(offer.offer_price) AS current_highest_bid, " +
                "offer.inventory_auction_id AS inventory_auction_id, " +
                "MAX(offer.id) AS max_offer_id " +
                "FROM offer " +
                "WHERE " +
                "offer.inventory_auction_id IN :inventoryAuctionIds AND offer.is_delete = 0 GROUP BY offer.inventory_auction_id  " +
                ") AS offerResult " +
            "ON offerResult.inventory_auction_id IN :inventoryAuctionIds " +

//            "LEFT JOIN " +
//                "(SELECT " +
//                "offer.inventory_auction_id AS inventory_auction_id, " +
//                "MAX(offer.offer_price) AS current_highest_bid  " +
//                "FROM offer " +
//                "WHERE " +
//                "offer.is_delete = 0 AND offer.inventory_auction_id IS NOT NULL AND " +
//                "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()} " +
//                "GROUP BY offer.inventory_auction_id" +
//                ") AS max_offer_price " +
//            "ON inventory_auction.id = max_offer_price.inventory_auction_id " +

            "WHERE inventory_auction.id IN :inventoryAuctionIds " +
            "AND inventory_auction.is_delete = 0"
    )
    List<InventoryAuctionDetailResponse> findAll(
            @Param("inventoryAuctionIds") List<Long> inventoryAuctionIds,
            @Param("now") String now
    );
}
