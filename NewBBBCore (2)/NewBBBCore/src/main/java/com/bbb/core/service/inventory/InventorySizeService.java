package com.bbb.core.service.inventory;

public interface InventorySizeService {
    Object getAllInventorySize();
}
