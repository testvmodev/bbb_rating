package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.ListingTypeEbay;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ListingTypeEbayConverter implements AttributeConverter<ListingTypeEbay, String> {
    @Override
    public String convertToDatabaseColumn(ListingTypeEbay listingTypeEbay) {
        if (listingTypeEbay == null ){
            return null;
        }
        return listingTypeEbay.getValue();
    }

    @Override
    public ListingTypeEbay convertToEntityAttribute(String s) {
        if (s == null ){
            return null;
        }
        for (ListingTypeEbay value : ListingTypeEbay.values()) {
            if (value.getValue().equals(s)){
                return value;
            }
        }
        return null;
    }
}
