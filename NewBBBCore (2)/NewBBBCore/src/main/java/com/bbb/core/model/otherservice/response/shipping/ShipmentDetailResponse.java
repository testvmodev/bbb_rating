package com.bbb.core.model.otherservice.response.shipping;

import com.bbb.core.model.database.type.CarrierType;
import com.bbb.core.model.database.type.InboundShippingType;
import com.bbb.core.model.database.type.OutboundShippingType;
import com.bbb.core.model.otherservice.request.shipment.create.ShipmentType;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class ShipmentDetailResponse {
    private long shipmentId;
    private Long inventoryId;
    private Long tradeInId;
    private ShipmentType shipmentType;
    private InboundShippingType inboundShippingType;
    private OutboundShippingType outboundShippingType;
    private CarrierType carrierType;
    private String manualCarrier;
    private Long shippingConfigId;
    private String trackingNumber;
    private Float billingWeight;
    private Float totalChargeAmount;
    private String totalChargeCurrency;
    private String fullLinkLabel;
    private String lastTrackingStatusCode;
    private String lastTrackingStatusDescription;
    private LocalDateTime lastTrackingStatusUpdateTime;
    private Boolean schedulePickup;
    private LocalDate scheduleDate;
    private Boolean isScheduled;
    private Boolean isDelivered;
    private boolean isActive = true;

    private String fromLine;
    private String fromCity;
    private String fromStateCode;
    private String fromZipCode;
    private String fromCountryCode;
    private String toLine;
    private String toCity;
    private String toStateCode;
    private String toZipCode;
    private String toCountryCode;

    private Integer length;
    private Integer width;
    private Integer height;
    private Float weight;

    public long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getTradeInId() {
        return tradeInId;
    }

    public void setTradeInId(Long tradeInId) {
        this.tradeInId = tradeInId;
    }

    public ShipmentType getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(ShipmentType shipmentType) {
        this.shipmentType = shipmentType;
    }

    public InboundShippingType getInboundShippingType() {
        return inboundShippingType;
    }

    public void setInboundShippingType(InboundShippingType inboundShippingType) {
        this.inboundShippingType = inboundShippingType;
    }

    public OutboundShippingType getOutboundShippingType() {
        return outboundShippingType;
    }

    public void setOutboundShippingType(OutboundShippingType outboundShippingType) {
        this.outboundShippingType = outboundShippingType;
    }

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }

    public String getManualCarrier() {
        return manualCarrier;
    }

    public void setManualCarrier(String manualCarrier) {
        this.manualCarrier = manualCarrier;
    }

    public Long getShippingConfigId() {
        return shippingConfigId;
    }

    public void setShippingConfigId(Long shippingConfigId) {
        this.shippingConfigId = shippingConfigId;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public Float getBillingWeight() {
        return billingWeight;
    }

    public void setBillingWeight(Float billingWeight) {
        this.billingWeight = billingWeight;
    }

    public Float getTotalChargeAmount() {
        return totalChargeAmount;
    }

    public void setTotalChargeAmount(Float totalChargeAmount) {
        this.totalChargeAmount = totalChargeAmount;
    }

    public String getTotalChargeCurrency() {
        return totalChargeCurrency;
    }

    public void setTotalChargeCurrency(String totalChargeCurrency) {
        this.totalChargeCurrency = totalChargeCurrency;
    }

    public String getFullLinkLabel() {
        return fullLinkLabel;
    }

    public void setFullLinkLabel(String fullLinkLabel) {
        this.fullLinkLabel = fullLinkLabel;
    }

    public String getLastTrackingStatusCode() {
        return lastTrackingStatusCode;
    }

    public void setLastTrackingStatusCode(String lastTrackingStatusCode) {
        this.lastTrackingStatusCode = lastTrackingStatusCode;
    }

    public String getLastTrackingStatusDescription() {
        return lastTrackingStatusDescription;
    }

    public void setLastTrackingStatusDescription(String lastTrackingStatusDescription) {
        this.lastTrackingStatusDescription = lastTrackingStatusDescription;
    }

    public LocalDateTime getLastTrackingStatusUpdateTime() {
        return lastTrackingStatusUpdateTime;
    }

    public void setLastTrackingStatusUpdateTime(LocalDateTime lastTrackingStatusUpdateTime) {
        this.lastTrackingStatusUpdateTime = lastTrackingStatusUpdateTime;
    }

    public Boolean getSchedulePickup() {
        return schedulePickup;
    }

    public void setSchedulePickup(Boolean schedulePickup) {
        this.schedulePickup = schedulePickup;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Boolean getScheduled() {
        return isScheduled;
    }

    public void setScheduled(Boolean scheduled) {
        isScheduled = scheduled;
    }

    public Boolean getDelivered() {
        return isDelivered;
    }

    public void setDelivered(Boolean delivered) {
        isDelivered = delivered;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getFromLine() {
        return fromLine;
    }

    public void setFromLine(String fromLine) {
        this.fromLine = fromLine;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromStateCode() {
        return fromStateCode;
    }

    public void setFromStateCode(String fromStateCode) {
        this.fromStateCode = fromStateCode;
    }

    public String getFromZipCode() {
        return fromZipCode;
    }

    public void setFromZipCode(String fromZipCode) {
        this.fromZipCode = fromZipCode;
    }

    public String getFromCountryCode() {
        return fromCountryCode;
    }

    public void setFromCountryCode(String fromCountryCode) {
        this.fromCountryCode = fromCountryCode;
    }

    public String getToLine() {
        return toLine;
    }

    public void setToLine(String toLine) {
        this.toLine = toLine;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToStateCode() {
        return toStateCode;
    }

    public void setToStateCode(String toStateCode) {
        this.toStateCode = toStateCode;
    }

    public String getToZipCode() {
        return toZipCode;
    }

    public void setToZipCode(String toZipCode) {
        this.toZipCode = toZipCode;
    }

    public String getToCountryCode() {
        return toCountryCode;
    }

    public void setToCountryCode(String toCountryCode) {
        this.toCountryCode = toCountryCode;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }
}
