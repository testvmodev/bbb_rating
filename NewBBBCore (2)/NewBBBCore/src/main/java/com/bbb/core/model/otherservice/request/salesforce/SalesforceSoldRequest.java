package com.bbb.core.model.otherservice.request.salesforce;

public class SalesforceSoldRequest {
    /**
     * ticketnumber : null
     * tax : 20.81
     * systemID : null
     * shippingType : null
     * shippingmethod : BBB
     * shippingCharge : null
     * shippingCarrierUsed : null
     * shipping : 75
     * sellerStreet : 1880 Dobbin Dr.
     * sellerState : CA
     * sellerPaypalEmail : bbb@vmodev.com
     * sellerName : Bicycle BlueBook.com
     * sellerLastName : BlueBook.com
     * sellerFirstName : Bicycle
     * sellerEmail : bikesales@bicyclebluebook.com
     * sellerCountry : US
     * sellerCity : San Jose
     * salesPrice : 225
     * salePriceBike : null
     * saleID : 1809
     * saleDateTime : 2019-06-25T19:27:25.000Z
     * outboundTracking : null
     * marketPlace : null
     * listingType : B2C
     * listingID : 6144
     * itemid : null
     * inventoryId : a0EK000000ChI9RMAV
     * Insurance : 0.94
     * grandTotal : 320.81
     * customerID : null
     * commissionFee : 11.25
     * buyerStreet : San jose
     * buyerState : CA
     * buyerPaypalEmail : bbb.personal1@gmail.com
     * buyerLastName : PhuongQA
     * buyerFirstName : Phuong
     * buyerEmail : minhanh.nguyenha@vmodev.com
     * buyerCountry : US
     * buyerCity : San Jose
     */

    private String stage;
    private Integer ticketnumber;
    private double tax;
    private String systemID;
    private String shippingType;
    private String shippingmethod;
    private String shippingCharge;
    private String shippingCarrierUsed;
    private float shipping;
    private String sellerStreet;
    private String sellerState;
    private String sellerPaypalEmail;
    private String sellerName;
    private String sellerLastName;
    private String sellerFirstName;
    private String sellerEmail;
    private String sellerCountry;
    private String sellerCity;
    private float salesPrice;
    private Float salePriceBike;
    private String saleID;
    private String saleDateTime;
    private String outboundTracking;
    private String marketPlace;
    private String listingType;
    private long listingID;
    private String itemid;
    private String inventoryId;
    private double Insurance;
    private double grandTotal;
    private String customerID;
    private double commissionFee;
    private String buyerStreet;
    private String buyerState;
    private String buyerPaypalEmail;
    private String buyerLastName;
    private String buyerFirstName;
    private String buyerEmail;
    private String buyerCountry;
    private String buyerCity;

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Integer getTicketnumber() {
        return ticketnumber;
    }

    public void setTicketnumber(Integer ticketnumber) {
        this.ticketnumber = ticketnumber;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public String getSystemID() {
        return systemID;
    }

    public void setSystemID(String systemID) {
        this.systemID = systemID;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingmethod() {
        return shippingmethod;
    }

    public void setShippingmethod(String shippingmethod) {
        this.shippingmethod = shippingmethod;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getShippingCarrierUsed() {
        return shippingCarrierUsed;
    }

    public void setShippingCarrierUsed(String shippingCarrierUsed) {
        this.shippingCarrierUsed = shippingCarrierUsed;
    }

    public float getShipping() {
        return shipping;
    }

    public void setShipping(float shipping) {
        this.shipping = shipping;
    }

    public String getSellerStreet() {
        return sellerStreet;
    }

    public void setSellerStreet(String sellerStreet) {
        this.sellerStreet = sellerStreet;
    }

    public String getSellerState() {
        return sellerState;
    }

    public void setSellerState(String sellerState) {
        this.sellerState = sellerState;
    }

    public String getSellerPaypalEmail() {
        return sellerPaypalEmail;
    }

    public void setSellerPaypalEmail(String sellerPaypalEmail) {
        this.sellerPaypalEmail = sellerPaypalEmail;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerLastName() {
        return sellerLastName;
    }

    public void setSellerLastName(String sellerLastName) {
        this.sellerLastName = sellerLastName;
    }

    public String getSellerFirstName() {
        return sellerFirstName;
    }

    public void setSellerFirstName(String sellerFirstName) {
        this.sellerFirstName = sellerFirstName;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getSellerCountry() {
        return sellerCountry;
    }

    public void setSellerCountry(String sellerCountry) {
        this.sellerCountry = sellerCountry;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public void setSellerCity(String sellerCity) {
        this.sellerCity = sellerCity;
    }

    public float getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(float salesPrice) {
        this.salesPrice = salesPrice;
    }

    public Float getSalePriceBike() {
        return salePriceBike;
    }

    public void setSalePriceBike(Float salePriceBike) {
        this.salePriceBike = salePriceBike;
    }

    public String getSaleID() {
        return saleID;
    }

    public void setSaleID(String saleID) {
        this.saleID = saleID;
    }

    public String getSaleDateTime() {
        return saleDateTime;
    }

    public void setSaleDateTime(String saleDateTime) {
        this.saleDateTime = saleDateTime;
    }

    public String getOutboundTracking() {
        return outboundTracking;
    }

    public void setOutboundTracking(String outboundTracking) {
        this.outboundTracking = outboundTracking;
    }

    public String getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(String marketPlace) {
        this.marketPlace = marketPlace;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public long getListingID() {
        return listingID;
    }

    public void setListingID(long listingID) {
        this.listingID = listingID;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public double getInsurance() {
        return Insurance;
    }

    public void setInsurance(double insurance) {
        Insurance = insurance;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public double getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(double commissionFee) {
        this.commissionFee = commissionFee;
    }

    public String getBuyerStreet() {
        return buyerStreet;
    }

    public void setBuyerStreet(String buyerStreet) {
        this.buyerStreet = buyerStreet;
    }

    public String getBuyerState() {
        return buyerState;
    }

    public void setBuyerState(String buyerState) {
        this.buyerState = buyerState;
    }

    public String getBuyerPaypalEmail() {
        return buyerPaypalEmail;
    }

    public void setBuyerPaypalEmail(String buyerPaypalEmail) {
        this.buyerPaypalEmail = buyerPaypalEmail;
    }

    public String getBuyerLastName() {
        return buyerLastName;
    }

    public void setBuyerLastName(String buyerLastName) {
        this.buyerLastName = buyerLastName;
    }

    public String getBuyerFirstName() {
        return buyerFirstName;
    }

    public void setBuyerFirstName(String buyerFirstName) {
        this.buyerFirstName = buyerFirstName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getBuyerCountry() {
        return buyerCountry;
    }

    public void setBuyerCountry(String buyerCountry) {
        this.buyerCountry = buyerCountry;
    }

    public String getBuyerCity() {
        return buyerCity;
    }

    public void setBuyerCity(String buyerCity) {
        this.buyerCity = buyerCity;
    }
}
