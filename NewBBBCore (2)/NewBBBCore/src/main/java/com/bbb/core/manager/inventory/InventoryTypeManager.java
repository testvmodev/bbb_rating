package com.bbb.core.manager.inventory;

import com.bbb.core.repository.inventory.InventoryTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InventoryTypeManager {
    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;

    public Object getAllInventoryType() {
        return inventoryTypeRepository.findAll();
    }
}
