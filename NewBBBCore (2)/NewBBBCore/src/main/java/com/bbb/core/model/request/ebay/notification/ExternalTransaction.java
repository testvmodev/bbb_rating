package com.bbb.core.model.request.ebay.notification;

import com.bbb.core.model.response.ebay.Currency;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

public class ExternalTransaction {
    @JacksonXmlProperty(localName = "ExternalTransactionID")
    private String externalTransactionID;
    @JacksonXmlProperty(localName = "ExternalTransactionTime")
    private LocalDateTime externalTransactionTime;
    @JacksonXmlProperty(localName = "FeeOrCreditAmount")
    private Currency feeOrCreditAmount;
    @JacksonXmlProperty(localName = "PaymentOrRefundAmount")
    private Currency paymentOrRefundAmount;
    @JacksonXmlProperty(localName = "ExternalTransactionStatus")
    private String externalTransactionStatus;

    public String getExternalTransactionID() {
        return externalTransactionID;
    }

    public void setExternalTransactionID(String externalTransactionID) {
        this.externalTransactionID = externalTransactionID;
    }

    public LocalDateTime getExternalTransactionTime() {
        return externalTransactionTime;
    }

    public void setExternalTransactionTime(LocalDateTime externalTransactionTime) {
        this.externalTransactionTime = externalTransactionTime;
    }

    public Currency getFeeOrCreditAmount() {
        return feeOrCreditAmount;
    }

    public void setFeeOrCreditAmount(Currency feeOrCreditAmount) {
        this.feeOrCreditAmount = feeOrCreditAmount;
    }

    public Currency getPaymentOrRefundAmount() {
        return paymentOrRefundAmount;
    }

    public void setPaymentOrRefundAmount(Currency paymentOrRefundAmount) {
        this.paymentOrRefundAmount = paymentOrRefundAmount;
    }

    public String getExternalTransactionStatus() {
        return externalTransactionStatus;
    }

    public void setExternalTransactionStatus(String externalTransactionStatus) {
        this.externalTransactionStatus = externalTransactionStatus;
    }
}
