package com.bbb.core.model.database.type.convert;

import com.bbb.core.model.database.type.TypeListingDuration;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TypeListAgeMarketListingConverter implements AttributeConverter<TypeListingDuration, String> {
    @Override
    public String convertToDatabaseColumn(TypeListingDuration typeListAgeMarketListing) {
        if (typeListAgeMarketListing == null) {
            return null;
        }
        return typeListAgeMarketListing.getValue();
    }

    @Override
    public TypeListingDuration convertToEntityAttribute(String s) {
        if (s == null) {
            return null;
        }
        return TypeListingDuration.findByValue(s);
    }
}
