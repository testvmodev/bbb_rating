package com.bbb.core.model.database.type;

//TODO optimize these hardcoded ids, might conflict with id in db
public enum StatusTradeIn {
    DECLINED_DETAIL(1),
    INCOMPLETE_SUMMARY(2),
    INCOMPLETE_UPLOAD_IMAGES(3),
    SHIPPING(4),
    COMPLETED(5),
    CUSTOM_QUOTE_INCOMPLETE(9),
    CUSTOM_QUOTE_PENDING_REVIEW(10),
    CUSTOM_QUOTE_VALUE_PROVIDED(11),
    CANCEL(12),
    INCOMPLETE_DETAIL(13);

    private final int value;

    StatusTradeIn(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static StatusTradeIn findByValue(int tradeId) {
        for (StatusTradeIn statusTradeIn : StatusTradeIn.values()) {
            if (statusTradeIn.getValue() == tradeId) {
                return statusTradeIn;
            }
        }
        return null;
    }

}
