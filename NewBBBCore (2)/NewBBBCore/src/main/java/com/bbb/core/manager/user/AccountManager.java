package com.bbb.core.manager.user;

import com.bbb.core.model.database.Account;
import com.bbb.core.model.database.type.StatusAccount;
import com.bbb.core.repository.user.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountManager {
    @Autowired
    private AccountRepository accountRepository;

    public boolean isAccountActive(String userId) {
        Account account = accountRepository.findOne(userId);
        if (account == null || account.getStatus() == StatusAccount.ACTIVE) {
            return true;
        }
        return false;
    }
}