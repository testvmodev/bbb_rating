package com.bbb.core.service.impl.favourite;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.favourite.FavouriteManager;
import com.bbb.core.model.request.favourite.FavouriteAddRequest;
import com.bbb.core.model.request.favourite.FavouriteRemoveRequest;
import com.bbb.core.service.favourite.FavouriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FavouriteServiceImpl implements FavouriteService {
    @Autowired
    private FavouriteManager manager;

    @Override
    public Object getFavourites(Pageable page) {
        return manager.getFavourites(page);
    }

    @Override
    public Object addFavourite(FavouriteAddRequest favouriteAddRequest) throws ExceptionResponse {
        return manager.addFavourite(favouriteAddRequest);
    }

    @Override
    public Object removeFavourite(FavouriteRemoveRequest favouriteRemoveRequest) throws ExceptionResponse {
        return manager.removeFavourite(favouriteRemoveRequest);
    }
}
