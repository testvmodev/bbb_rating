package com.bbb.core.model.request.tradein.ups.detail.track;

import com.bbb.core.model.request.tradein.ups.detail.RequestConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackRequest {
    @JsonProperty("Request")
    private RequestConfig requestConfig;
    @JsonProperty("InquiryNumber")
    private String inquiryNumber;

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    public String getInquiryNumber() {
        return inquiryNumber;
    }

    public void setInquiryNumber(String inquiryNumber) {
        this.inquiryNumber = inquiryNumber;
    }
}
