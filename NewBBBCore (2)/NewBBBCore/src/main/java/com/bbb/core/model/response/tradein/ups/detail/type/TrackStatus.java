package com.bbb.core.model.response.tradein.ups.detail.type;

public enum TrackStatus {
    DELIVERED("D"),
    IN_TRANSIT("I"),
    PICKUP("P"),
    MANIFEST_PICKUP("M"),
    EXCEPTION("E");

    private final String value;

    TrackStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TrackStatus findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "D":
                return DELIVERED;
            case "I":
                return IN_TRANSIT;
            case "P":
                return PICKUP;
            case "M":
                return MANIFEST_PICKUP;
            case "X":
                return EXCEPTION;
            default:
                return null;
        }
    }
}
