package com.bbb.core.repository.onlinestore.dashboard;

import com.bbb.core.model.response.onlinestore.OnlineStoreSaleReportResponse;
import org.joda.time.YearMonth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OnlineStoreSaleReportResponseRepository extends JpaRepository<OnlineStoreSaleReportResponse, YearMonth> {
    @Query(nativeQuery = true, value = "SELECT " +
            ":month AS month, " +
            "ISNULL(SUM(inventory_sale.price), 0) AS total_sale " +
            "FROM inventory_sale " +
            "JOIN inventory ON inventory_sale.inventory_id = inventory.id " +
            "AND inventory.storefront_id = :storefrontId " +

            "WHERE CONVERT(CHAR(7), inventory_sale.created_time, 120) = :#{#month.toString()} "
    )
    OnlineStoreSaleReportResponse getReport(
            @Param("storefrontId") String storefrontId,
            @Param("month") YearMonth month);
}
