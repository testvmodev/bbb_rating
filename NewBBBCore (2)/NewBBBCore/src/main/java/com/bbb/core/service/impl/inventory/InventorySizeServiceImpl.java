package com.bbb.core.service.impl.inventory;

import com.bbb.core.manager.inventory.InventorySizeManager;
import com.bbb.core.service.inventory.InventorySizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventorySizeServiceImpl implements InventorySizeService {
    @Autowired
    private InventorySizeManager manager;

    @Override
    public Object getAllInventorySize() {
        return manager.getAllInventorySize();
    }
}
