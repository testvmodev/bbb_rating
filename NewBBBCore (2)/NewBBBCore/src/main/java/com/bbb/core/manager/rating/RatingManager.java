package com.bbb.core.manager.rating;

import com.bbb.core.common.Constants;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.model.database.InventoryAuction;
import com.bbb.core.model.database.MarketListing;
import com.bbb.core.model.database.Rating;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.request.rating.RatingCreateRequest;
import com.bbb.core.model.request.rating.RatingGetRequest;
import com.bbb.core.model.request.rating.RatingUpdateRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bicycle.BicycleRatingResponse;
import com.bbb.core.model.response.rating.RatingDetailResponse;
import com.bbb.core.model.response.rating.RatingResponse;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.rating.BicycleRatingResponseRepository;
import com.bbb.core.repository.rating.RatingRepository;
import com.bbb.core.repository.rating.RatingResponseRepository;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

import static com.bbb.core.common.MessageResponses.*;

//import com.bbb.core.model.response.bicycle.BicycleRatingResponse;
//import com.bbb.core.model.response.market.marketlisting.rating.RatingDetailResponse;
//import com.bbb.core.model.response.market.marketlisting.rating.RatingResponse;

@Component
public class RatingManager {
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private RatingResponseRepository ratingResponseRepository;
    @Autowired
    private BicycleRatingResponseRepository bicycleRatingResponseRepository;
    @Autowired
    private AuthManager authManager;

    //--------------------GET RATING FROM INVENTORY AUCTION OR MARKET LISTING-----------------------
    public Object getRatingForBicycle(RatingGetRequest request) throws ExceptionResponse {
        //TODO fix null
        if (request.getMarketListingId() != null && request.getInventoryAuctionId() != null
         || request.getMarketListingId() == null && request.getInventoryAuctionId() == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, NO_PRODUCTS_MATCH),
                    HttpStatus.NOT_FOUND
            );
        }
        BicycleRatingResponse bicycle = bicycleRatingResponseRepository.getBicycle(
                request.getMarketListingId(), request.getInventoryAuctionId()
        );
        RatingDetailResponse response = new RatingDetailResponse(
                bicycle.getBicycleImages(), bicycle.getBicycleYear(),
                bicycle.getBicycleBrand(), bicycle.getBicycleModel(),
                bicycle.getBicycleType(),bicycle.getBicycleSize()
        );

        ListObjResponse<RatingResponse> listObjResponse = new ListObjResponse<>();
        listObjResponse.setTotalItem(ratingRepository.countAllRatingResponse(
                request.getMarketListingId(), request.getInventoryAuctionId())
        );
        if (listObjResponse.getTotalItem() > 0){
            List<RatingResponse> listData = ratingResponseRepository.findAllRatingResponse(
                    request.getMarketListingId(), request.getInventoryAuctionId(),
                    request.getPageable().getOffset(), request.getPageable().getPageSize()
            );
            if (listData.size() == 0) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, NO_PRODUCTS_MATCH),
                        HttpStatus.NOT_FOUND
                );
            }
            //set nameDisplay
            UserDetailFullResponse userFullInfo;
            for (RatingResponse r : listData){
                try {
                    userFullInfo = authManager.getUserFullInfo(r.getNameDisplay());
                    r.setNameDisplay(userFullInfo.getDisplayName());
                }catch (HttpClientErrorException.BadRequest e){
                    r.setNameDisplay("Unknow");
                }
            }
            listObjResponse.setData(listData);
        } else {
            listObjResponse.setData(new ArrayList<>());
        }
        if (request.getMarketListingId() != null) {
            MarketListing marketListing = marketListingRepository.findOne(request.getMarketListingId());
            response.setAvgRating(marketListing.getRating());
        }else {
            if (request.getInventoryAuctionId() != null) {
                InventoryAuction inventoryAuction = inventoryAuctionRepository.findOne(request.getInventoryAuctionId());
                response.setAvgRating(inventoryAuction.getRating());
            }
        }
        listObjResponse.setPageSize(request.getPageable().getPageSize());
        listObjResponse.setPage(request.getPageable().getPageNumber());
        listObjResponse.updateTotalPage();
        response.setRatingResponse(listObjResponse);
        return response;
    }


    //--------------------------GET RATING FOR USER FROM USER ID--------------------------------
    public Object getRatingForUser(Long marketListingId ,Long inventoryAuctionId, String userId) {
        RatingResponse ratingResponse = ratingResponseRepository.getOneByUserId(marketListingId, inventoryAuctionId, userId);
        if (ratingResponse == null){
            return ratingResponse;
        }
        try {
            UserDetailFullResponse userFullInfo = authManager.getUserFullInfo(ratingResponse.getNameDisplay());
            ratingResponse.setNameDisplay(userFullInfo.getDisplayName());
        }catch (HttpClientErrorException.BadRequest e){
            ratingResponse.setNameDisplay("Admin");
        }
        return ratingResponse;
    }

    //-----------------------------CREATE RATING--------------------------------------
    public Object createRating(RatingCreateRequest request,
                               Long invetoryAuctionId,
                               Long marketListingId
    ) throws ExceptionResponse {
        //Check rating exist!

        if (ratingResponseRepository.getOneByUserId(marketListingId,null,CommonUtils.getUserLogin()) != null){
            if (ratingResponseRepository.getOneByUserId(null,invetoryAuctionId,CommonUtils.getUserLogin()) != null
            && invetoryAuctionId != null){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, RATING_EXISTED),
                        HttpStatus.LOOP_DETECTED);
            }
            marketListingId = null;
        }
        Rating rating = new Rating();
        MarketListing marketListing = new MarketListing();
        InventoryAuction inventoryAuction = new InventoryAuction();
        if (marketListingId != null){
            marketListing= marketListingRepository.findOne(marketListingId);
            if (marketListing == null){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                        HttpStatus.NOT_FOUND);
            }
            rating.setMarketListingId(marketListing.getId());
        }else {
            if (invetoryAuctionId != null){
                inventoryAuction= inventoryAuctionRepository.findOne(invetoryAuctionId);
                if (inventoryAuction == null){
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_AUCTION_NOT_EXIST),
                            HttpStatus.NOT_FOUND);
                }
                rating.setInventoryAuctionId(invetoryAuctionId);
            }else {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_AUCTION_ID_AND_MARKETLISTING_ID_DO_NOT_EMPTY),
                        HttpStatus.NOT_FOUND);
            }
        }
        rating.setComment(request.getComment());
        rating.setTitle(request.getTitle());
        rating.setUserId(request.getUserId());
        if (request.getRate()>Constants.MAX_RATE) {
            rating.setRate(Constants.MAX_RATE);
        }else {
            if (request.getRate() < Constants.MIN_RATE) {
                rating.setRate(Constants.MIN_RATE);
            } else {
                rating.setRate(request.getRate());
            }
        }
        rating.setRateTime(LocalDateTime.now());
        ratingRepository.save(rating);

        //Save avg rating to InventoryAuction or MarketListing
        if (marketListingId != null) {
            marketListing.setRating(ratingRepository.avgRatingForMarket(marketListingId));
            marketListingRepository.save(marketListing);
        }else {
            inventoryAuction.setRating(ratingRepository.avgRatingForAuction(invetoryAuctionId));
            inventoryAuctionRepository.save(inventoryAuction);
        }
        return rating;
    }


    //-------------------------------------UPDATE RATING----------------------------------------------
    public Object updateRating(RatingUpdateRequest request,
                               Long id,
                               Long invetoryAuctionId,
                               Long marketListingId
    ) throws ExceptionResponse {
        Rating rating;
        MarketListing marketListing = new MarketListing();
        InventoryAuction inventoryAuction = new InventoryAuction();
        if (CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles()) && id != null) {
            rating= ratingRepository.findOneById(id);
        }else {
            if (marketListingId != null){
                marketListing= marketListingRepository.findOne(marketListingId);
                if (marketListing == null){
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, MARKET_LISTING_NOT_EXIST),
                            HttpStatus.NOT_FOUND);
                }
                rating = ratingRepository.findOneByUserIdAndMarketListingId(CommonUtils.getUserLogin(),marketListingId);

            }else {
                if (invetoryAuctionId != null){
                    inventoryAuction= inventoryAuctionRepository.findOne(invetoryAuctionId);
                    if (inventoryAuction == null){
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_AUCTION_NOT_EXIST),
                                HttpStatus.NOT_FOUND);
                    }
                    rating = ratingRepository.findOneByUserIdAndInventoryAuctionId(CommonUtils.getUserLogin(),invetoryAuctionId);
                }else {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_PARAM, INVENTORY_AUCTION_ID_AND_MARKETLISTING_ID_DO_NOT_EMPTY),
                            HttpStatus.NOT_FOUND);
                }
            }
        }
        if (rating == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, RATING_ID_NOT_EXIST),
                    HttpStatus.NOT_FOUND);
        }

        if (request.getComment() != null) {
            rating.setComment(request.getComment());
        }
        if (request.getTitle() != null) {
            rating.setTitle(request.getTitle());
        }
        if (request.getRate() != null) {
            if (request.getRate()>Constants.MAX_RATE) {
                rating.setRate(Constants.MAX_RATE);
            }else {
                if (request.getRate() < Constants.MIN_RATE) {
                    rating.setRate(Constants.MIN_RATE);
                } else {
                    rating.setRate(request.getRate());
                }
            }
        }
        rating.setRateTime(LocalDateTime.now());
        ratingRepository.save(rating);
        if (rating.getMarketListingId() != null) {
            marketListing= marketListingRepository.findOne(rating.getMarketListingId());
            marketListing.setRating(ratingRepository.avgRatingForMarket(rating.getMarketListingId()));
            marketListingRepository.save(marketListing);

        }else {
            inventoryAuction= inventoryAuctionRepository.findOne(rating.getInventoryAuctionId());
            inventoryAuction.setRating(ratingRepository.avgRatingForAuction(rating.getInventoryAuctionId()));
            inventoryAuctionRepository.save(inventoryAuction);
        }
        return rating;
    }

    //--------------------TEST-----------------------
    public Object getRatingTest(Long marketListingId ,Long inventoryAuctionId, Pageable pageable) {
        UserDetailFullResponse userFullInfo = authManager.getUserFullInfo("5afbdee608b4a678d0be9d78");
        UserDetailFullResponse userFullInfo1 = authManager.getUserFullInfo("5aefcf2aeb8f350b400d7897");
        System.out.println("-----------------------------" + userFullInfo.getDisplayName()
                +"---------"+ userFullInfo1.getDisplayName());
        return ratingRepository.avgAllRating(marketListingId, inventoryAuctionId);
    }
}
