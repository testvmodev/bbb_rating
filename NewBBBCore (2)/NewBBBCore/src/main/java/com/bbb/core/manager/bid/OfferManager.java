package com.bbb.core.manager.bid;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.manager.auth.AuthManager;
import com.bbb.core.manager.bid.async.OfferManagerAsync;
import com.bbb.core.manager.billing.BillingManager;
import com.bbb.core.manager.market.async.MarketListingManagerAsync;
import com.bbb.core.manager.notification.NotificationManager;
import com.bbb.core.manager.user.AccountManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.otherservice.response.cart.CartSingleStageResponse;
import com.bbb.core.model.otherservice.response.cart.CartStageResponses;
import com.bbb.core.model.otherservice.response.user.UserDetailFullResponse;
import com.bbb.core.model.request.bid.offer.*;
import com.bbb.core.model.request.bid.offer.offerbid.OfferBidRequest;
import com.bbb.core.model.request.bid.offer.offerbid.OffersReceiverRequest;
import com.bbb.core.model.request.bid.offer.test.OfferType;
import com.bbb.core.model.request.sort.OfferBidFieldSort;
import com.bbb.core.model.request.sort.Sort;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.bid.inventoryauction.InventoryAuctionDetailResponse;
import com.bbb.core.model.response.bid.offer.*;
import com.bbb.core.model.response.bid.offer.offerbid.InventoryBidResponse;
import com.bbb.core.model.response.bid.offer.offerbid.UserBidDetail;
import com.bbb.core.model.response.inventory.InventoryDetailResponse;
import com.bbb.core.model.response.market.marketlisting.detail.InventoryCompTypeResponse;
import com.bbb.core.model.response.market.marketlisting.detail.MarketListingDetailResponse;
import com.bbb.core.repository.bid.AuctionRepository;
import com.bbb.core.repository.bid.InventoryAuctionRepository;
import com.bbb.core.repository.bid.OfferRepository;
import com.bbb.core.repository.bid.out.*;
import com.bbb.core.repository.expirationtime.ExpirationTimeConfigRepository;
import com.bbb.core.repository.inventory.InventoryImageRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.inventory.out.InventoryDetailResRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.market.out.detail.InventoryCompTypeResRepository;
import com.bbb.core.repository.market.out.detail.MarketListingDetailRepository;
import com.bbb.core.repository.reout.ContentDoubleNumberRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.LocalDateTime;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class OfferManager implements MessageResponses {
    //region Beans
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InventoryAuctionRepository inventoryAuctionRepository;
    @Autowired
    private ContentDoubleNumberRepository contentDoubleNumberRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private BillingManager billingManager;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private InventoryBidResponseRepository inventoryBidResponseRepository;
    @Autowired
    private InventoryAuctionDetailResponseRepository inventoryAuctionDetailResponseRepository;
    @Autowired
    private MarketListingDetailRepository marketListingDetailRepository;
    @Autowired
    private InventoryCompTypeResRepository inventoryCompTypeResRepository;
    @Autowired
    private InventoryAuctionManager inventoryAuctionManager;
    @Autowired
    private MarketListingManagerAsync marketListingManagerAsync;
    @Autowired
    private BidOfferResponseRepository bidOfferResponseRepository;
    @Autowired
    private OfferResponseRepository offerResponseRepository;
    @Autowired
    private OfferMarAucRepository offerMarAucRepository;
    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper objectMapper;
    @Autowired
    private InventoryDetailResRepository inventoryDetailResRepository;
    @Autowired
    private ExpirationTimeConfigRepository expirationTimeConfigRepository;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private OfferManagerAsync offerManagerAsync;
    @Autowired
    private AccountManager accountManager;
    @Autowired
    private UserOfferResponseRepository userOfferResponseRepository;
    @Autowired
    private AuthManager authManager;
    @Autowired
    private UserBidResponseRepository userBidResponseRepository;
    @Autowired
    private UserBidDetailRepository userBidDetailRepository;
    @Autowired
    private ModelMapper modelMapper;
    //endregion

    public Object getOffers(OfferRequest request, Pageable page) {
        ListObjResponse<OfferResponse> response = new ListObjResponse<>();
        response.setTotalItem(
                offerResponseRepository.countTotalNumber(
                        CommonUtils.isLogined() ? CommonUtils.getUserLogin() : null,
                        request.getStatus() == null ? null : request.getStatus().getValue(),
                        request.getInventoryAuctionId(),
                        request.getMarketListingId()
                )
        );
        if (response.getTotalItem() > 0) {
            response.setData(
                    offerResponseRepository.findAll(
                            CommonUtils.isLogined() ? CommonUtils.getUserLogin() : null,
                            request.getStatus() == null ? null : request.getStatus().getValue(),
                            request.getInventoryAuctionId(),
                            request.getMarketListingId(),
                            request.getFieldSort().name(),
                            request.getSort().name(),
                            page.getOffset(),
                            page.getPageSize()
                    ));
        } else {
            response.setData(new ArrayList<>());
        }
        udpateLeftTimeOffer(response.getData());

        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    public Object getOffersReceiver(OffersReceiverRequest request, Pageable page) throws ExceptionResponse {
        ListObjResponse<OfferResponse> response = new ListObjResponse<>();
        response.setTotalItem(
                offerResponseRepository.countTotalNumberReceiverMyListing(CommonUtils.getUserLogin())
        );
        if (response.getTotalItem() > 0) {
            response.setData(
                    offerResponseRepository.findAllReceiverMyListing(
                            CommonUtils.getUserLogin(),
                            request.getFieldSort().name(),
                            request.getSortType().name(),
                            page.getOffset(),
                            page.getPageSize()
                    )
            );
        } else {
            response.setData(new ArrayList<>());
        }

        udpateLeftTimeOffer(response.getData());
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;
    }

    public Object getOffersOnlineStore(OffersOnlineStoreRequest request) throws ExceptionResponse {
        request.validate();

        ListObjResponse<OfferResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        String storefrontId = CommonUtils.getStorefrontId();
        if (storefrontId == null) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        response.setTotalItem(offerResponseRepository.countOnlineStoreReceived(
                storefrontId,
                request.getStatuses(),
                request.getContent(),
                request.getFromDay(),
                request.getToDay()
        ));

        if (response.getTotalItem() > 0) {
            response.setData(offerResponseRepository.findAllOnlineStoreReceived(
                    storefrontId,
                    request.getStatuses(),
                    request.getContent(),
                    request.getFromDay(),
                    request.getToDay(),
                    request.getFieldSort(),
                    request.getSortType(),
                    request.getPageable()
            ));

            if (response.getData().size() > 0) {
                udpateLeftTimeOffer(response.getData());
            }
        }

        return response;
    }

    public Object getOffersMarketListing(long marketListingId) throws ExceptionResponse {
        Inventory inventory = inventoryRepository.findByMarketListingId(marketListingId);
        if (inventory == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_NOT_FOUND,
                    MARKET_LISTING_NOT_EXIST,
                    HttpStatus.NOT_FOUND
            );
        }

        String storefrontId = CommonUtils.getStorefrontId();
        String userId = CommonUtils.getUserLogin();
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            if (storefrontId != null) {
                if (inventory.getStorefrontId() == null
                        || !inventory.getStorefrontId().equals(storefrontId)) {
                    throw new ExceptionResponse(
                            ObjectError.NO_PERMISSION,
                            YOU_DONT_HAVE_PERMISSION,
                            HttpStatus.FORBIDDEN
                    );
                }
            } else if (!inventory.getSellerId().equals(userId)) {
                throw new ExceptionResponse(
                        ObjectError.NO_PERMISSION,
                        YOU_DONT_HAVE_PERMISSION,
                        HttpStatus.FORBIDDEN
                );
            }
        }

        List<OfferResponse> responses = offerResponseRepository.findAllOffersMarketListing(marketListingId);

        udpateLeftTimeOffer(responses);

        return responses;
    }

    private void udpateLeftTimeOffer(List<OfferResponse> response) {
        if (response != null && response.size() > 0) {
            List<ExpirationTimeConfig> expirationTimeConfigs = expirationTimeConfigRepository.findFirstByOfferAuctionOrOfferMarketListing();
            ExpirationTimeConfig expirationTimeConfigAuction = CommonUtils.findObject(expirationTimeConfigs, o -> o.getType() == TypeExpirationTime.OFFER_AUCTION);
            ExpirationTimeConfig expirationTimeConfigMarketListing = CommonUtils.findObject(expirationTimeConfigs, o -> o.getType() == TypeExpirationTime.OFFER_MARKET_LISTING);
            LocalDateTime now = LocalDateTime.now();
            Map<String, List<OfferResponse>> mightExpired = new HashMap<>();
            for (OfferResponse datum : response) {
                if (datum.getInventoryAuctionId() != null) {
                    if (expirationTimeConfigAuction != null) {
                        if (datum.getStatus() == StatusOffer.ACCEPTED && datum.getLastUpdate() != null) {
                            long timeExpire = datum.getLastUpdate().toDate().getTime() + (long) ((double) expirationTimeConfigAuction.getExpireAfterHours() * Constants.HOUR);
                            datum.setLeftTimeExpire(timeExpire - now.toDate().getTime());
                            if (datum.getLeftTimeExpire() < 0) {
//                                datum.setStatus(StatusOffer.CLOSED_CART_EXPIRED);
                                if (!mightExpired.containsKey(datum.getBuyerId())) {
                                    mightExpired.put(datum.getBuyerId(), new ArrayList<>());
                                }
                                mightExpired.get(datum.getBuyerId()).add(datum);
                            }
                        }
                    }
                } else {
                    if (datum.getMarketListingId() != null) {
                        if (expirationTimeConfigMarketListing != null) {
                            if (datum.getStatus() == StatusOffer.ACCEPTED && datum.getLastUpdate() != null) {
                                long timeExpire = datum.getLastUpdate().toDate().getTime() + (long) ((double) expirationTimeConfigMarketListing.getExpireAfterHours() * Constants.HOUR);
                                datum.setLeftTimeExpire(timeExpire - now.toDate().getTime());
                                if (datum.getLeftTimeExpire() < 0) {
//                                    datum.setStatus(StatusOffer.CLOSED_CART_EXPIRED);
                                    if (!mightExpired.containsKey(datum.getBuyerId())) {
                                        mightExpired.put(datum.getBuyerId(), new ArrayList<>());
                                    }
                                    mightExpired.get(datum.getBuyerId()).add(datum);
                                }
                            }
                        }
                    }
                }
            }
            if (mightExpired.size() > 0) {
                mightExpired.forEach((u, o) -> {
                    List<Long> inventoryIds = o.stream().map(OfferResponse::getInventoryId)
                            .collect(Collectors.toList());
                    List<CartStageResponses> stages = billingManager.checkStatusCarts(inventoryIds, u);
                    stages.forEach(stage -> {
                        if (!stage.getStage().equals(ValueCommons.PAY)) {
                            OfferResponse res = CommonUtils.findObject(o, offer -> offer.getInventoryId() == stage.getInventoryId());
                            res.setStatus(StatusOffer.CLOSED_CART_EXPIRED);
                        }
                    });
                });
            }
        }
    }

    public Object createOffer(OfferCreateRequest request) throws ExceptionResponse {
        request.validate();

        if (!accountManager.isAccountActive(CommonUtils.getUserLogin())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    CREATE_OFFER_ACCOUNT_NOT_ACTIVE,
                    HttpStatus.FORBIDDEN
            );
        }

        MarketListing marketListing = null; //offer for market listing
        InventoryAuction inventoryAuction = null; //offer for auction
        String errorMessage = null;
        int errorCode = ObjectError.ERROR_NOT_FOUND;

        Inventory inventory = null;
        Auction auction = null;

        //new api, market listing case
        if (request.getMarketListingId() != null) {
            if (!CommonUtils.checkHaveRole(UserRoleType.NORMAL)) {
                errorMessage = YOU_DONT_HAVE_PERMISSION;
                errorCode = ObjectError.ERROR_PARAM;
            } else {
                marketListing = marketListingRepository.findOne(request.getMarketListingId());
                if (marketListing == null || marketListing.isDelete()) {
                    errorMessage = MARKET_LISTING_NOT_EXIST;
                } else if (marketListing.getStatus() != StatusMarketListing.LISTED) {
                    errorMessage = STATUS_MARKET_LISTING_INVALID;
                    errorCode = ObjectError.INVALID_ACTION;
                } else {
                    inventory = inventoryRepository.findOne(marketListing.getInventoryId());
                }
            }
        } else {
            if (!CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, YOU_DONT_HAVE_PERMISSION),
                        HttpStatus.BAD_REQUEST

                );
            }
            if (request.getInventoryAuctionId() != null) {
                inventoryAuction = inventoryAuctionRepository.findOne(request.getInventoryAuctionId());
                if (inventoryAuction == null || inventoryAuction.isDelete()) {
                    errorMessage = INVENTORY_AUCTION_NOT_EXIST;
                } else if (inventoryAuction.isInactive()) {
//                    errorMessage = INVENTORY_AUCTION_NOT_ACTIVE;
                    errorMessage = AUCTION_CLOSED;
                    errorCode = ObjectError.INVALID_ACTION;
                } else {
                    inventory = inventoryRepository.findOne(inventoryAuction.getInventoryId());
                }
                //end
            } else if (request.getInventoryId() != null) {
                inventory = inventoryRepository.findOne(request.getInventoryId());
                if (inventory == null) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_ID_NOT_EXIST),
                            HttpStatus.NOT_FOUND
                    );
                }
                if (inventory.isDelete()) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY_DELETED),
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (!(inventory.getStatus() == StatusInventory.ACTIVE || inventory.getStage() == StageInventory.LISTED)) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, INVENTORY + " " + inventory.getStatus() + " " + inventory.getStage()),
                            HttpStatus.BAD_REQUEST
                    );
                }
                if (request.getOfferPrice() <= 0) {
                    throw new ExceptionResponse(
                            new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_PRICE_MUST_MORE_THAN_ZERO),
                            HttpStatus.BAD_REQUEST
                    );
                }

                //deprecated api, market listing case
                if (request.getAuctionId() == null) {
//                marketListing = marketListingRepository.
                } else {  //deprecated api, auction case
                    auction = auctionRepository.findOne(request.getAuctionId());
                    if (auction == null) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_NOT_FOUND, AUCTION_ID_NOT_FOUND),
                                HttpStatus.BAD_REQUEST
                        );
                    }
                    LocalDateTime now = LocalDateTime.now();
                    if (auction.getStartDate().toDate().getTime() > now.toDate().getTime()) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, AUCTION_PENDING),
                                HttpStatus.BAD_REQUEST
                        );
                    }
                    if (auction.getEndDate().toDate().getTime() < now.toDate().getTime()) {
                        throw new ExceptionResponse(
                                new ObjectError(ObjectError.ERROR_PARAM, AUCTION_CLOSED),
                                HttpStatus.BAD_REQUEST
                        );
                    }
                    inventoryAuction = inventoryAuctionRepository.findOne(
                            request.getInventoryId(), request.getAuctionId());
                    if (inventoryAuction == null) {
                        errorMessage = INVENTORY_NOT_INTO_AUCTION;
                    }
                }
            }
        }

        if (errorMessage == null && CommonUtils.getUserLogin().equals(inventory.getSellerId())) {
            errorMessage = YOU_CAN_NOT_OFFER_THIS_INVENTORY_BECAUSE_YOU_SELL_THIS_INVENTORY;
        } else {
            if (errorMessage == null && CommonUtils.getPartnerId() != null) {
                if (inventory.getPartnerId() != null && inventory.getPartnerId().equals(CommonUtils.getPartnerId())) {
                    errorMessage = YOU_CAN_NOT_OFFER_THIS_INVENTORY_BECAUSE_YOU_SELL_THIS_INVENTORY;
                }
            }
            if (CommonUtils.getStorefrontId() != null) {
                if (inventory.getStorefrontId() != null && inventory.getStorefrontId().equals(CommonUtils.getStorefrontId())) {
                    errorMessage = YOU_CAN_NOT_OFFER_THIS_INVENTORY_BECAUSE_YOU_SELL_THIS_INVENTORY;
                }
            }
        }


        if (errorMessage != null) {
            throw new ExceptionResponse(
                    errorCode, errorMessage,
                    HttpStatus.BAD_REQUEST
            );
        }


        Offer offer = new Offer();
        offer.setBuyerId(request.getBuyerId());
        offer.setDelete(false);
        offer.setOfferPrice(request.getOfferPrice());
        //market listing case
        if (marketListing != null) {
            checkStatusListingAcceptedOffer(marketListing);

            //only checking listing status
//            if (offerRepository.getNumberAcceptedOrCompleted(marketListing.getId()) > 0) {
//                errorMessage = MARKET_LISTING_ACCEPTED_OR_COMPLETED;
//            }
            offer.setMarketListingId(marketListing.getId());

//            Double currentHighestOffer =  offerRepository.findMaxPriceOfferMarketListing(marketListing.getId());
//            if ( currentHighestOffer != null && request.getOfferPrice() <= currentHighestOffer) {
//                throw new ExceptionResponse(
//                        new ObjectError(ObjectError.ERROR_PARAM, BID_PRICE_MUST_MORE_THAN_HIGHEST_OFFER + currentHighestOffer),
//                        HttpStatus.BAD_REQUEST
//                );
//            }
            if (marketListing.isBestOffer()) {
                if (marketListing.getBestOfferAutoAcceptPrice() != null && request.getOfferPrice() >= marketListing.getBestOfferAutoAcceptPrice()) {
                    offer.setStatus(StatusOffer.ACCEPTED);

                } else {
                    if (marketListing.getMinimumOfferAutoAcceptPrice() != null && offer.getOfferPrice() < marketListing.getMinimumOfferAutoAcceptPrice()) {
                        errorMessage = OFFER_PRICE_MUST_MORE_THAN_MINIMUM_AUTO_ACCEPT_PRICE + " (" + marketListing.getMinimumOfferAutoAcceptPrice() + ")";
                    }
                }
            }
            if (errorMessage != null) {
                throw new ExceptionResponse(
                        errorCode, errorMessage,
                        HttpStatus.BAD_REQUEST
                );
            }
            //auction case
        } else if (inventoryAuction != null) {
            if (auction == null) {
                auction = auctionRepository.findOne(inventoryAuction.getAuctionId());
            }
            LocalDateTime now = LocalDateTime.now();
            if (auction != null && (auction.getStartDate().toDate().getTime() > now.toDate().getTime() ||
                    now.toDate().getTime() > auction.getEndDate().toDate().getTime())) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_AUCTION_INVALID),
                        HttpStatus.BAD_REQUEST
                );
            }
            offer.setInventoryAuctionId(inventoryAuction.getId());

            Double currentHighestOffer = contentDoubleNumberRepository.findMaxPriceOfferInventoryAuction(
                    inventoryAuction.getId()
            );
            if (currentHighestOffer != null && request.getOfferPrice() <= currentHighestOffer) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, BID_PRICE_MUST_MORE_THAN_HIGHEST_OFFER + currentHighestOffer),
                        HttpStatus.BAD_REQUEST
                );
            }
            if (request.getOfferPrice() < inventoryAuction.getMin()) {
//                offer.setStatus(StatusOffer.REJECTED);
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, BID_PRICE_MUST_MORE_THAN_MIN_PRICE + " (" + inventoryAuction.getMin() + ")"),
                        HttpStatus.BAD_REQUEST
                );
            }

            if (inventoryAuction.isHasBuyItNow() && inventoryAuction.getBuyItNow() != null &&
                    request.getOfferPrice() >= inventoryAuction.getBuyItNow()) {
                offer.setStatus(StatusOffer.ACCEPTED);
                inventoryAuction.setInactive(true);
                inventoryAuction.setWinnerId(request.getBuyerId());
                inventoryAuctionRepository.save(inventoryAuction);
            }
        }

        if (offer.getStatus() == null) {
            offer.setStatus(StatusOffer.PENDING);
            offer = offerRepository.save(offer);
        } else {
            if (offer.getStatus() == StatusOffer.ACCEPTED) {
                checkStatusCartAcceptedOffer(inventory.getId(), "make");
                offer = offerRepository.save(offer);
                //update delist
                if (marketListing != null) {
                    marketListingManagerAsync.endAllItemEbayAndDeList(marketListing.getInventoryId(), marketListing.getId());
                    marketListing.setStatus(StatusMarketListing.SALE_PENDING);

                } else {
                    if (inventoryAuction != null) {
                        marketListingManagerAsync.endAllItemEbayAndDeList(inventoryAuction.getInventoryId(), null);
                    }
                }
                inventory.setStage(StageInventory.SALE_PENDING);
                inventory = inventoryRepository.save(inventory);

                //add cart

                rejectOtherOffer(
                        inventoryAuction == null ? null : inventoryAuction.getId(),
                        marketListing == null ? null : marketListing.getId(),
                        null
                );

                offer = offerRepository.save(offer);

                //todo exception add cart and send email
                offerManagerAsync.addCardAnSendEmailFromBuyer(offer, inventory.getId());
            }
        }
        offerManagerAsync.sendEmailWhenCreateOffer(inventory, offer, CommonUtils.getUserLogin());
        return offer;
    }

    private void checkStatusListingAcceptedOffer(MarketListing marketListing) throws ExceptionResponse {
        if (marketListing.getStatus() == StatusMarketListing.SALE_PENDING) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.INVALID_ACTION, INVENTORY_PAYING),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void checkStatusListingAcceptedOffer(MarketListing marketListing, String action) throws ExceptionResponse {
        if (marketListing.getStatus() == StatusMarketListing.SALE_PENDING) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.INVALID_ACTION, CANT_SAVE_OFFER_SALE_PENDING.replace(ACTION, action)),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    private void checkStatusCartAcceptedOffer(long inventoryId, String action) throws ExceptionResponse {
        CartSingleStageResponse cart = billingManager.checkStatusCart(inventoryId);
        if (cart != null) {
            if (cart.getStage().equals(ValueCommons.PAY)) {
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.INVALID_ACTION, CANT_SAVE_OFFER_SALE_PENDING.replace(ACTION, action)),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }


    public void rejectOtherOffer(Long inventoryAuctionId, Long marketListingId, Long excludeOffer) {
        rejectOtherOffer(
                inventoryAuctionId == null ? null : Collections.singletonList(inventoryAuctionId),
                marketListingId == null ? null : Collections.singletonList(marketListingId),
                excludeOffer
        );
    }

    public void rejectOtherOffer(List<Long> inventoryAuctionIds, List<Long> marketListingIds, Long excludeOffer) {
        List<InventoryAuction> inventoryAuctions;
        boolean invAucNull = false, marketListNull = false;
        if (inventoryAuctionIds != null && inventoryAuctionIds.size() > 0) {
            inventoryAuctions = inventoryAuctionRepository.findListActive(inventoryAuctionIds);
            for (InventoryAuction inventoryAuction : inventoryAuctions) {
//            inventoryAuction.setDelete(true);
                inventoryAuction.setInactive(true);
                inventoryAuction.setLastUpdate(null);
            }
        } else {
            invAucNull = true;
            inventoryAuctionIds = Collections.singletonList(0l);
            inventoryAuctions = new ArrayList<>();
        }
        if (marketListingIds == null || marketListingIds.size() < 1) {
            marketListNull = true;
            marketListingIds = Collections.singletonList(0l);
        }
        List<Offer> offers =
                offerRepository.findPendingCounterByAuctionOrListing(
                        invAucNull, inventoryAuctionIds,
                        marketListNull, marketListingIds,
                        excludeOffer
                ).stream().peek(of -> {
                    of.setStatus(StatusOffer.REJECTED);
                    of.setLastUpdate(null);
                }).collect(Collectors.toList());
        if (inventoryAuctions.size() > 0) {
            inventoryAuctionRepository.saveAll(inventoryAuctions);
        }
        if (offers.size() > 0) {
            offerRepository.saveAll(offers);
        }
        //send email for buyer when offer reject
        offerManagerAsync.sendEmailOfferRejected(offers);
    }


    public void rejectAll(List<Offer>... offerLists) {
        List<Offer> rejectOffers = new ArrayList<>();
        List<Offer> acceptedOffers = new ArrayList<>();

        for (List<Offer> offers : offerLists) {
            if (offers != null && offers.size() > 0) {
                offers.forEach(offer -> {
                    if (offer.getStatus() == StatusOffer.ACCEPTED) {
                        acceptedOffers.add(offer);
                    }
                    offer.setStatus(StatusOffer.REJECTED);
                    rejectOffers.add(offer);
                });
            }
        }

        if (rejectOffers.size() > 0) {
            offerRepository.saveAll(rejectOffers);
        }

        if (acceptedOffers.size() > 0) {
            //TODO send mail about rejecting accepted offers
        }
    }

    public Object updateOffer(long offerId, OfferUpdateRequest request) throws ExceptionResponse {
        if (request.getStatus() == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_OFFER_NOT_NULL),
                    HttpStatus.BAD_REQUEST
            );
        }
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }
        if (offer.getStatus() == StatusOffer.COMPLETED) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }
        MarketListing marketListing = offer.getMarketListingId() == null ? null :
                marketListingRepository.findOne(offer.getMarketListingId());
        if (marketListing == null) {
            //allow admin manual update status of only market listing's offer
            throw new ExceptionResponse(
                    ObjectError.INVALID_ACTION, OFFER_NOT_MARKET_LISTING,
                    HttpStatus.BAD_REQUEST
            );
        }

        if (CommonUtils.getUserLogin().equals(offer.getBuyerId())) {
            offer = updateStatusOfferBuyer(offer, marketListing, request);
        } else {
            offer = updateStatusOfferSeller(offer, marketListing, request);
        }

        return offer;

    }

    private Offer updateStatusOfferSeller(Offer offer, MarketListing marketListing, OfferUpdateRequest request) throws ExceptionResponse {
        checkRoleUpdateOffer(offer, marketListing);
        switch (request.getStatus()) {
            case COUNTERED:
                checkStatusCartAcceptedOffer(marketListing.getInventoryId(), "counter");
                sellerCountedOffer(offer, request);
                break;
            case REJECTED:
                sellerRejectOffer(offer);
                break;
            case ACCEPTED:
                checkStatusListingAcceptedOffer(marketListing, "accept");
                checkStatusCartAcceptedOffer(marketListing.getInventoryId(), "accept");
                sellerAcceptOffer(offer, marketListing, request);
//                if (offer.getStatus() == StatusOffer.COUNTERED){
//                offerManagerAsync.addCardAnSendEmailFromSeller(offer, marketListing.getInventoryId());
//                }else {
//                    offerManagerAsync.addCardAnSendEmailFromBuyer(offer, marketListing.getInventoryId());
//                }
                break;
            case COMPLETED:
                checkStatusCartAcceptedOffer(marketListing.getInventoryId(), "complete");
                sellerCompleteOffer(offer, marketListing, request);
                break;
            case CANCELLED:
                if (request.getReasonCancel() == null) {
                    throw new ExceptionResponse(
                            ObjectError.ERROR_PARAM,
                            REASON_CANCEL_REQUIRED,
                            HttpStatus.BAD_REQUEST
                    );
                }
                //do not allow cancel when cart is paying
                checkStatusCartAcceptedOffer(marketListing.getInventoryId(), "cancel");
                offer.setReasonCancel(request.getReasonCancel());
                cancelOfferSeller(offer, marketListing, request);
                break;
            default:
                break;
        }

        offer.setStatus(request.getStatus());
        offer.setLastUpdate(null);

        offer = offerRepository.save(offer);

        switch (request.getStatus()) {
            case ACCEPTED:
                offerManagerAsync.addCardAnSendEmailFromSeller(offer, marketListing.getInventoryId());
                break;
            case REJECTED:
                offerManagerAsync.sendEmailOfferRejectFromSeller(offer, marketListing);
                break;
            case COUNTERED:
                //counted
                offerManagerAsync.sendEmailOfferCounted(offer);
                break;
            case CANCELLED:
                offerManagerAsync.sendMailCancelFromSeller(offer, marketListing);
                break;
            default:
                break;
        }
        return offer;
    }

    private void sellerRejectOffer(Offer offer) throws ExceptionResponse {
        if (offer.getStatus() != StatusOffer.PENDING) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
    }

    private void sellerCountedOffer(Offer offer, OfferUpdateRequest request) throws ExceptionResponse {
        if (request.getOfferPrice() == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOUR_MUST_TRANSMIT_COUNTED_PRICE), HttpStatus.BAD_REQUEST);
        }
        if (request.getOfferPrice() < 0) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, COUNTED_PRICE_MUST_THAN_ZERO), HttpStatus.BAD_REQUEST);
        }
        if (offer.getStatus() != StatusOffer.PENDING) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
        offer.setOfferPrice(request.getOfferPrice());
    }

    private void cancelOfferSeller(Offer offer, MarketListing marketListing, OfferUpdateRequest request) throws ExceptionResponse {
        if (offer.getStatus() != StatusOffer.ACCEPTED) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }

        List<Long> inventoryIds = new ArrayList<>();
        inventoryIds.add(marketListing.getInventoryId());
        marketListingManagerAsync.listEbayAsyncFromInventoryIds(inventoryIds);
        marketListing.setStatus(StatusMarketListing.LISTED);

        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());
        inventory.setStage(StageInventory.LISTED);

        marketListingRepository.save(marketListing);
        inventoryRepository.save(inventory);
    }

    private void sellerCompleteOffer(Offer offer, MarketListing marketListing, OfferUpdateRequest request) throws ExceptionResponse {
        if (offer.getStatus() != StatusOffer.ACCEPTED) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
        updateOfferMarketListingWhenOfferComplete(marketListing, offer.getId(), request.getStatus());
    }

    private void sellerAcceptOffer(Offer offer, MarketListing marketListing, OfferUpdateRequest request) throws ExceptionResponse {
        if (offer.getStatus() != StatusOffer.PENDING) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
        updateOfferMarketListingAccepted(marketListing, offer);
    }

    private Offer updateStatusOfferBuyer(Offer offer, MarketListing marketListing, OfferUpdateRequest request) throws ExceptionResponse {
        switch (request.getStatus()) {
            case PENDING:
                if (offer.getStatus() != StatusOffer.COUNTERED) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
                }
                if (request.getOfferPrice() == null || request.getOfferPrice() < 0) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, COUNTED_PRICE_MUST_THAN_ZERO), HttpStatus.BAD_REQUEST);
                }
                offer.setOfferPrice(request.getOfferPrice());
                offerManagerAsync.sendMailCounteredFromBuyer(offer, marketListing);
                break;
            case ACCEPTED:
                if (offer.getStatus() != StatusOffer.COUNTERED) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
                }
                checkStatusCartAcceptedOffer(marketListing.getInventoryId(), "accept");
                updateOfferMarketListingAccepted(marketListing, offer);
                offerManagerAsync.addCardAnSendEmailFromBuyer(offer, marketListing.getInventoryId());
                break;
            case REJECTED:
                if (offer.getStatus() != StatusOffer.COUNTERED) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
                }
                offerManagerAsync.sendMailRejectFromBuyer(offer);
                break;
            default:
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
        offer.setStatus(request.getStatus());
        return offerRepository.save(offer);


    }

    private void updateOfferMarketListingWhenOfferComplete(MarketListing marketListing, long offerId, StatusOffer statusOffer) {
        rejectOtherOffer(null, marketListing.getId(), offerId);
        marketListingManagerAsync.endAllItemEbayAndDeListComplete(marketListing.getInventoryId(), marketListing.getId());
        marketListing.setStatus(StatusMarketListing.SOLD);
        marketListingRepository.save(marketListing);
    }

    private void updateOfferMarketListingAccepted(MarketListing marketListing, Offer offer) throws ExceptionResponse {
        rejectOtherOffer(null, marketListing.getId(), offer.getId());
        marketListingManagerAsync.endAllItemEbayAndDeList(marketListing.getInventoryId(), marketListing.getId());
        marketListing.setStatus(StatusMarketListing.SALE_PENDING);
    }


    private void checkRoleUpdateOffer(Offer offer, MarketListing marketListing) throws ExceptionResponse {
        Inventory inventory = inventoryRepository.findOne(marketListing.getInventoryId());

        //inv from scorecard
        if (inventory.getPartnerId() != null) {
            if (CommonUtils.checkHaveRole(UserRoleType.PARTNER_ADMIN)) {
                if (!inventory.getPartnerId().equals(CommonUtils.getPartnerId())) {
                    throw new ExceptionResponse(new ObjectError(ObjectError.NO_PERMISSION, YOU_DONT_HAVE_PERMISSION), HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new ExceptionResponse(new ObjectError(ObjectError.NO_PERMISSION, YOU_DONT_HAVE_PERMISSION), HttpStatus.BAD_REQUEST);
            }
        //onlinestore
        } else if (inventory.getStorefrontId() != null) {
            if (CommonUtils.getStorefrontId() == null
                    || !inventory.getStorefrontId().equals(CommonUtils.getStorefrontId())
            ) {
                throw new ExceptionResponse(new ObjectError(ObjectError.NO_PERMISSION, YOU_DONT_HAVE_PERMISSION), HttpStatus.FORBIDDEN);
            }
        //p2p
        } else {
            if (!inventory.getSellerId().equals(CommonUtils.getUserLogin())) {
                throw new ExceptionResponse(
                        ObjectError.NO_PERMISSION,
                        YOU_DONT_HAVE_PERMISSION,
                        HttpStatus.FORBIDDEN
                );
            }
        }

    }

    private OfferDetailResponse cloneOfferToOfferDetailResponse(Offer offer) {
        OfferDetailResponse response = new OfferDetailResponse();
        response.setId(offer.getId());
        response.setBuyerId(offer.getBuyerId());
//        response.setInventoryId(offer.getInventoryId());
        response.setOfferPrice(offer.getOfferPrice());
        response.setStatus(offer.getStatus());
        response.setCreatedTime(offer.getCreatedTime());
        response.setLastUpdate(null);
//        response.setAuctionId(offer.getAuctionId());
        response.setInventoryAuctionId(offer.getInventoryAuctionId());
        response.setReasonCancel(offer.getReasonCancel());
        response.setMarketListingId(offer.getMarketListingId());
        return response;
    }

    public Object getDetailOffer(long offerId) throws ExceptionResponse {
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }

        OfferDetailResponse response = getOfferInventoryDetail(offer);

        return response;
    }

    private OfferDetailResponse getOfferInventoryDetail(Offer offer) {
        OfferDetailResponse response = cloneOfferToOfferDetailResponse(offer);

        InventoryDetailResponse inventory = null;

        if (offer.getInventoryAuctionId() != null) {
            InventoryAuction inventoryAuction =
                    inventoryAuctionRepository.findOne(offer.getInventoryAuctionId());
            inventory = inventoryDetailResRepository.findOnById(inventoryAuction.getInventoryId());
        } else if (offer.getMarketListingId() != null) {
            MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
            inventory = inventoryDetailResRepository.findOnById(marketListing.getInventoryId());
        }
        if (inventory != null) {
            List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoryId(inventory.getId());
            if (inventoryImages.size() > 0) {
                inventory.setImages(
                        inventoryImages.stream().map(InventoryImage::getImage).collect(Collectors.toList())
                );
            }
        }

        response.setInventory(inventory);

        return response;
    }

    public Object payOfferAccepted(long offerId, boolean isPaid) throws ExceptionResponse {
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }

        offer.setPaid(isPaid);
        return offerRepository.save(offer);
    }

    public Object getMarketListingAuctionsService(List<SecretMarketLisitingAuctionRequest> requests) {
        List<SecretMarketLisitingAuctionRequest> marketListings =
                requests.stream().filter(req -> req.getType() == SecretOfferMarketListingAuctionTypeRequest.MARKET_LISTING)
                        .collect(Collectors.toList());
        List<SecretMarketLisitingAuctionRequest> inventoryAuctions =
                requests.stream().filter(req -> req.getType() == SecretOfferMarketListingAuctionTypeRequest.AUCTION)
                        .collect(Collectors.toList());
        List<SecretMarketLisitingAuctionRequest> offersRequest =
                requests.stream().filter(req -> req.getType() == SecretOfferMarketListingAuctionTypeRequest.OFFER)
                        .collect(Collectors.toList());
        List<Object> responses = new ArrayList<>();
        List<MarketListingDetailResponse> marketListingDetailResponses;
        List<InventoryAuctionDetailResponse> ivnentoryAuctionDetailResponses;
        List<OfferDetailResponse> offerDetailResponses = null;
        if (marketListings.size() > 0) {
            marketListingDetailResponses = marketListingDetailRepository.findAll(
                    marketListings.stream()
                            .map(SecretMarketLisitingAuctionRequest::getId)
                            .collect(Collectors.toList()),
                    null
            );
        } else {
            marketListingDetailResponses = new ArrayList<>();
        }
        if (inventoryAuctions.size() > 0) {
            ivnentoryAuctionDetailResponses = inventoryAuctionDetailResponseRepository.findAll(
                    inventoryAuctions.stream().map(SecretMarketLisitingAuctionRequest::getId).collect(Collectors.toList()),
                    new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date())
            );
        } else {
            ivnentoryAuctionDetailResponses = new ArrayList<>();
        }

        if (offersRequest.size() > 0) {
            List<Long> offerIds = offersRequest.stream().map(SecretMarketLisitingAuctionRequest::getId).collect(Collectors.toList());
            CommonUtils.stream(offerIds);
            List<OfferMarAuc> offerMarAucs = offerMarAucRepository.findAllInventoryIdByOfferId(offerIds);
            List<Long> inventoryIds = offerMarAucs.stream().map(OfferMarAuc::getInventoryId).collect(Collectors.toList());
            CommonUtils.stream(inventoryIds);
            for (int i = 0; i < inventoryIds.size(); i++) {
                if (inventoryIds.get(i) == null) {
                    inventoryIds.remove(i);
                    i--;
                }
            }
            if (inventoryIds.size() > 0) {
                List<Offer> offers = offerRepository.findAllByIds(offerIds);
                List<InventoryDetailResponse> inventories = inventoryDetailResRepository.findAllByIds(inventoryIds);
                offerDetailResponses = new ArrayList<>();
                for (Offer offer : offers) {
                    OfferDetailResponse offerDetailResponse = cloneOfferToOfferDetailResponse(offer);
                    OfferMarAuc offerMarAuc = CommonUtils.findObject(offerMarAucs, o -> o.getId() == offer.getId());
                    if (offerMarAuc == null) {
                        continue;
                    }
                    offerDetailResponse.setInventory(
                            CommonUtils.findObject(inventories, inv -> inv.getId() == offerMarAuc.getInventoryId())
                    );
                    if (offerDetailResponse.getInventory() != null) {
                        offerDetailResponses.add(offerDetailResponse);
                    }
                }
            }
        }
        if (offerDetailResponses == null) {
            offerDetailResponses = new ArrayList<>();
        }

        responses.addAll(marketListingDetailResponses);
        responses.addAll(ivnentoryAuctionDetailResponses);
        responses.addAll(offerDetailResponses);
        if (responses.size() > 0) {
            List<Long> inventoryIds = marketListingDetailResponses.stream().map(MarketListingDetailResponse::getInventoryId).collect(Collectors.toList());
            inventoryIds.addAll(ivnentoryAuctionDetailResponses.stream().map(InventoryAuctionDetailResponse::getInventoryId).collect(Collectors.toList()));
            inventoryIds.addAll(offerDetailResponses.stream().map(o -> o.getInventory().getId()).collect(Collectors.toList()));
            CommonUtils.stream(inventoryIds);
            List<InventoryCompTypeResponse> compResponses = inventoryCompTypeResRepository.findAllCompInventories(inventoryIds);
            Map<Long, List<InventoryCompTypeResponse>> mapComps = compResponses.stream().collect(Collectors.groupingBy(comp -> comp.getId().getInventoryId()));
            mapComps.forEach((inventoryId, comps) -> {
                for (MarketListingDetailResponse marketListingDetailRespons : marketListingDetailResponses) {
                    if (marketListingDetailRespons.getInventoryId() == inventoryId) {
                        marketListingManagerAsync.updateCompMarketListingDetailNotAsyn(marketListingDetailRespons, comps);
                    }
                }
                for (InventoryAuctionDetailResponse ivnentoryAuctionDetailRespons : ivnentoryAuctionDetailResponses) {
                    if (ivnentoryAuctionDetailRespons.getInventoryId() == inventoryId) {
                        inventoryAuctionManager.updateCompInventoryAuctionDetail(ivnentoryAuctionDetailRespons, comps);
                    }
                }
            });

            List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoriesId(inventoryIds);
            Map<Long, List<InventoryImage>> mapInventoriesImage = inventoryImages.stream().collect(Collectors.groupingBy(InventoryImage::getInventoryId));
            mapInventoriesImage.forEach((inventoryId, invImages) -> {
                for (MarketListingDetailResponse marketListingDetailRespons : marketListingDetailResponses) {
                    if (marketListingDetailRespons.getInventoryId() == inventoryId) {
                        marketListingDetailRespons.setImages(
                                invImages.stream().map(InventoryImage::getImage).collect(Collectors.toList())
                        );
                    }
                }
                for (InventoryAuctionDetailResponse ivnentoryAuctionDetailRespons : ivnentoryAuctionDetailResponses) {
                    if (ivnentoryAuctionDetailRespons.getInventoryId() == inventoryId) {
                        ivnentoryAuctionDetailRespons.setImages(
                                invImages.stream().map(InventoryImage::getImage).collect(Collectors.toList())
                        );
                    }
                }
            });
        }

        return responses;
    }


    public Object getOffersBid(OfferBidRequest request, Pageable page) throws ExceptionResponse {
        if (!CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION), HttpStatus.BAD_REQUEST);
        }
        List<Long> temLong = new ArrayList<>();
        temLong.add(0L);
        List<String> temString = new ArrayList<>();
        temString.add("test");
        ListObjResponse<InventoryBidResponse> response = new ListObjResponse<>();
        String now = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date());
        response.setData(
                inventoryBidResponseRepository.findAllAuctionInventory(
                        CommonUtils.getUserLogin(),
                        now,
                        request.getInventoryIds() == null || request.getInventoryIds().size() == 0,
                        (request.getInventoryIds() == null || request.getInventoryIds().size() == 0) ? temLong : request.getInventoryIds(),
                        request.getStatusBids() == null || request.getStatusBids().size() == 0,
                        request.getStatusBids() == null || request.getStatusBids().size() == 0 ? temString : request.getStatusBids().stream().map(StatusOffer::getValue).collect(Collectors.toList()),
                        request.getWin() != null,
                        request.getWin(),
                        request.getSortField().name(),
                        request.getSortType().name(),
                        page.getOffset(),
                        page.getPageSize()
                )
        );
        response.setTotalItem(
                inventoryBidResponseRepository.countAuctionInventory(
                        CommonUtils.getUserLogin(),
                        request.getInventoryIds() == null || request.getInventoryIds().size() == 0,
                        (request.getInventoryIds() == null || request.getInventoryIds().size() == 0) ? temLong : request.getInventoryIds(),
                        request.getStatusBids() == null || request.getStatusBids().size() == 0,
                        request.getStatusBids() == null || request.getStatusBids().size() == 0 ? temString : request.getStatusBids().stream().map(StatusOffer::getValue).collect(Collectors.toList()),
                        request.getWin()
                ));
        response.setPageSize(page.getPageSize());
        response.setPage(page.getPageNumber());
        response.updateTotalPage();
        return response;

    }

    public Object getOfferBid(long inventoryAuctionId, OfferBidFieldSort fieldSort, Sort sortType) throws ExceptionResponse {
        if (!CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION), HttpStatus.FORBIDDEN);
        }
        if (fieldSort == null) {
            fieldSort = OfferBidFieldSort.CREATED_TIME;
        }
        if (sortType == null) {
            sortType = Sort.DESC;
        }

        return bidOfferResponseRepository.findAll(
                inventoryAuctionId,
                CommonUtils.getUserLogin(),
                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                fieldSort.name(),
                sortType.name()
        );

    }

    public Object updateOfferBuyer(long offerId, boolean isAccepted) throws ExceptionResponse {
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, OFFER_ID_NOT_FOUND), HttpStatus.BAD_REQUEST);
        }
        if (offer.getStatus() != StatusOffer.COUNTERED) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, STATUS_OFFER_INVALID), HttpStatus.BAD_REQUEST);
        }
        if (!offer.getBuyerId().equals(CommonUtils.getUserLogin())) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION), HttpStatus.BAD_REQUEST);
        }
        if (offer.getMarketListingId() == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, OFFER_DO_NOT_HAS_MARKET_LISTING), HttpStatus.BAD_REQUEST);
        }
        offer.setStatus(isAccepted ? StatusOffer.ACCEPTED : StatusOffer.REJECTED);
        if (isAccepted) {
            MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
            updateOfferMarketListingAccepted(marketListing, offer);
        }
        return offerRepository.save(offer);
    }

    //create accepted offer to support testing by other service
    public Object offerIntegrationTest(OfferType offerType) throws ExceptionResponse {
        Offer offer = null;
        if (offerType == OfferType.MARKET_LISTING) {
            offer = offerRepository.findOne(Constants.TEST_LISTING_OFFER_ID);
            if (offer != null) {
                offer.setStatus(StatusOffer.ACCEPTED);
            }
        } else if (offerType == OfferType.INVENTORY_AUCTION) {
            offer = offerRepository.findOne(Constants.TEST_AUCTION_OFFER_ID);
            if (offer != null) {
                offer.setStatus(StatusOffer.ACCEPTED);
            }
        }
        if (offer == null) {
            throw new ExceptionResponse(
                    ObjectError.UNKNOWN_ERROR,
                    MessageResponses.TEST_OFFER_DELETED,
                    HttpStatus.NOT_FOUND
            );
        } else {
            offer = offerRepository.save(offer);
        }
        return getOfferInventoryDetail(offer);
    }

    public Object removeCartOffer(long offerId) throws ExceptionResponse {
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null || offer.isDelete()) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND), HttpStatus.NOT_FOUND);
        }
        if (offer.getStatus() == StatusOffer.CANCELLED || offer.getStatus() == StatusOffer.COMPLETED) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, STATUS_OFFER_INVALID), HttpStatus.NOT_FOUND);
        }
        if ( offer.getStatus() == StatusOffer.CLOSED_CART_EXPIRED){
            return offer;
        }
        if (offer.getMarketListingId() != null) {
            return removeCartMarketListing(offer);
        } else {
            if (offer.getInventoryAuctionId() != null) {
                return removeCartInventoryAuction(offer);
            }
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_MUST_BE_MARKET_LISTING), HttpStatus.NOT_FOUND);
        }
    }

    private Object removeCartMarketListing(Offer offer) {
        updateOfferWhenRemoveCart(offer);
        MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
        if (marketListing.getStatus() != StatusMarketListing.LISTED) {
            marketListing.setStatus(StatusMarketListing.LISTED);
            marketListing = marketListingRepository.save(marketListing);
            inventoryRepository.updateStatusAndStage(marketListing.getInventoryId(), StatusInventory.ACTIVE.getValue(), StageInventory.LISTED.getValue());
            marketListingManagerAsync.listEbayAsyncFromInventoryId(marketListing.getInventoryId());
        }
        return offer;
    }

    private Object removeCartInventoryAuction(Offer offer) {
        StatusOffer oldStatusOffer = offer.getStatus();
        updateOfferWhenRemoveCart(offer);
        if (oldStatusOffer == StatusOffer.ACCEPTED) {
            Long inventoryId = inventoryAuctionRepository.findInventoryById(offer.getInventoryAuctionId());
            if (inventoryId != null) {
                inventoryRepository.updateStatusAndStage(inventoryId, StatusInventory.ACTIVE.getValue(), StageInventory.READY_LISTED.getValue());
            }
        }
        return offer;
    }

    private void updateOfferWhenRemoveCart(Offer offer) {
        offer.setStatus(StatusOffer.CLOSED_REMOVED_FROM_CART);
        offerRepository.updateStatusOffer(offer.getId(), offer.getStatus().getValue());
    }

    public Object getInventoryAuctionBids(
            long inventoryAuctionId,
            OfferBidFieldSort offerBidFieldSort, Sort sortType
    ) throws ExceptionResponse {
        if (!CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_PARAM, YOU_DONT_HAVE_PERMISSION), HttpStatus.BAD_REQUEST);
        }
        if (offerBidFieldSort == null) {
            offerBidFieldSort = OfferBidFieldSort.CREATED_TIME;
        }
        if (sortType == null) {
            sortType = Sort.DESC;
        }
        return bidOfferResponseRepository.findAll(
                inventoryAuctionId,
                null,
                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                offerBidFieldSort.name(),
                sortType.name()
        );
    }

    public Object getUserOffers(UserOfferRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        ListObjResponse<UserOfferResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(userOfferResponseRepository.countUserOffers(request.getUserId()));

        if (response.getTotalItem() > 0) {
            response.setData(userOfferResponseRepository.findAllUserOffers(
                    request.getUserId(),
                    request.getSortField(), request.getSortType(),
                    request.getPageable()
            ));

            if (response.getData().size() > 0) {
                List<String> userIds = response.getData().stream()
                        .map(UserOfferResponse::getBuyerId)
                        .collect(Collectors.toList());
                CommonUtils.stream(userIds);
                //map user id
                Map<String, UserDetailFullResponse> users = authManager.getUsersFullInfo(userIds).stream()
                        .collect(Collectors.toMap(UserDetailFullResponse::getId, u -> u));
                for (UserOfferResponse offer : response.getData()) {
                    offer.setBuyerDisplay(users.get(offer.getBuyerId()).getDisplayName());
                }
            }
        }

        return response;
    }

    public Object getUserBids(UserBidRequest request) throws ExceptionResponse {
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }

        ListObjResponse<UserBidResponse> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(userBidResponseRepository.countAllByUser(request.getUserId()));
        if (response.getTotalItem() > 0) {
            response.setData(userBidResponseRepository.findAllByUser(request.getUserId(), request.getPageable()));
            List<Long> inventoryAuctionIds = response.getData().stream()
                    .map(UserBidResponse::getInventoryAuctionId)
                    .collect(Collectors.toList());
            List<UserBidDetail> bids = userBidDetailRepository.findAll(request.getUserId(), inventoryAuctionIds);
            for (UserBidResponse res : response.getData()) {
                List<UserBidDetail> details = new ArrayList<>();
                bids.forEach(b -> {
                    if (b.getInventoryAuctionId() == res.getInventoryAuctionId()) {
                        details.add(b);
                    }
                });
                res.setBids(details);
            }
        }

        return response;
    }

    public Object getOfferDetailSecret(long offerId) throws ExceptionResponse {
        Offer offer = offerRepository.findOne(offerId);
        if (offer == null) {
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_NOT_FOUND, OFFER_ID_NOT_FOUND),
                    HttpStatus.BAD_REQUEST
            );
        }

        SecretOfferDetailResponse response = modelMapper.map(offer, SecretOfferDetailResponse.class);
        InventoryDetailResponse inventory = null;
        if (offer.getInventoryAuctionId() != null) {
            InventoryAuction inventoryAuction =
                    inventoryAuctionRepository.findOne(offer.getInventoryAuctionId());
            inventory = inventoryDetailResRepository.findOnById(inventoryAuction.getInventoryId());
        } else if (offer.getMarketListingId() != null) {
            MarketListing marketListing = marketListingRepository.findOne(offer.getMarketListingId());
            inventory = inventoryDetailResRepository.findOnById(marketListing.getInventoryId());
            response.setPaypalEmailSeller(marketListing.getPaypalEmailSeller());
        }
        if (inventory != null) {
            List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoryId(inventory.getId());
            if (inventoryImages.size() > 0) {
                inventory.setImages(
                        inventoryImages.stream().map(InventoryImage::getImage).collect(Collectors.toList())
                );
            }
        }
        response.setInventory(inventory);

        return response;
    }
}
