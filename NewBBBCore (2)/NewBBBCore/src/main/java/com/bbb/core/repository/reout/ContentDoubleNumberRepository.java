package com.bbb.core.repository.reout;

import com.bbb.core.model.response.ContentDoubleNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentDoubleNumberRepository extends JpaRepository<ContentDoubleNumber, Long> {
    //tracking view
    @Query(nativeQuery = true, value = "SELECT count_view.inventory_id AS id, count_view.count_view AS value " +
            "FROM " +
            "(SELECT " +
            "inventory_tracking_view.inventory_id AS inventory_id, COUNT(inventory_tracking_view.inventory_id) AS count_view " +
            "FROM " +
            "inventory_tracking_view  " +
            "GROUP BY inventory_tracking_view.inventory_id) AS count_view " +
            "JOIN inventory ON count_view.inventory_id = inventory.id " +
            "WHERE " +
            "inventory.status = 'Active' AND " +
            "inventory.stage = 'Listed' " +
            "ORDER BY count_view.count_view DESC OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<ContentDoubleNumber> findAllInventoryTrackingView(
            @Param(value = "offset") long offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM " +
            "(SELECT " +
            "inventory_tracking_view.inventory_id AS inventory_id " +
            "FROM " +
            "inventory_tracking_view JOIN inventory ON inventory_tracking_view.inventory_id = inventory.id " +
            "WHERE " +
            "inventory.status = 'Active' AND " +
            "inventory.stage = 'Listed' " +
            "GROUP BY inventory_tracking_view.inventory_id) AS count_view"
    )
    int getNumberInventoryTrackingView();


    @Query(nativeQuery = true, value = "SELECT COUNT(*) " +
            "FROM " +
            "(SELECT " +
            "inventory_tracking_view.inventory_id AS inventory_id " +
            "FROM " +
            "inventory_tracking_view JOIN inventory ON inventory_tracking_view.inventory_id =  inventory.id " +
            "WHERE " +
            "inventory_tracking_view.user_id = :userId AND " +
            "inventory.status = 'Active' AND " +
            "inventory.stage = 'Listed' " +
            "GROUP BY inventory_tracking_view.inventory_id) AS count_view"
    )
    int getNumberRecentView(
            @Param(value = "userId") String userId
    );
    //end

//    @Query(nativeQuery = true, value = "SELECT MAX(offer.offer_price) " +
//            "FROM offer " +
//            "WHERE offer.inventory_id = :inventoryId AND offer.auction_id = :auctionId " +
//            "GROUP BY offer.inventory_id, offer.auction_id")
//    Double findMaxPriceOfferInventoryAuction(@Param(value = "inventoryId") long inventoryId,
//                                             @Param(value = "auctionId") long auctionId);

    @Query(nativeQuery = true, value = "SELECT MAX(offer.offer_price) " +
            "FROM offer " +
            "WHERE " +
            "offer.inventory_auction_id = :inventoryAuctionId AND " +
            "offer.status <> :#{T(com.bbb.core.model.database.type.StatusOffer).REJECTED.getValue()}"
    )
    Double findMaxPriceOfferInventoryAuction(
            @Param(value = "inventoryAuctionId") long inventoryAuctionId
    );
}
