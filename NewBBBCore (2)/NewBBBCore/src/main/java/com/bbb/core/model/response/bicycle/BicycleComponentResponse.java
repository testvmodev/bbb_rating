package com.bbb.core.model.response.bicycle;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BicycleComponentResponse {
    @Id
    private long id;
    private long componentTypeId;
    private String componentName;
    private String componentValue;
    private String categoryName;
    @JsonIgnore
    private long bicycleTypeId;
    @JsonIgnore
    private String bicycleTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getComponentTypeId() {
        return componentTypeId;
    }

    public void setComponentTypeId(long componentTypeId) {
        this.componentTypeId = componentTypeId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentValue() {
        return componentValue;
    }

    public void setComponentValue(String componentValue) {
        this.componentValue = componentValue;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getBicycleTypeId() {
        return bicycleTypeId;
    }

    public void setBicycleTypeId(long bicycleTypeId) {
        this.bicycleTypeId = bicycleTypeId;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }
}
