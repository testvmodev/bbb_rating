package com.bbb.core.model.database.type;

public enum  MarketListingFilterTypeTable {
    INVENTORY("inventory"),
    INVENTORY_COMP_DETAIL("inventory_comp_detail"),
    INVENTORY_TYPE("inventory_type"),
    INVENTORY_ERROR("inventory_error"),
    INVENTORY_NONE("none");
    private final String value;

    MarketListingFilterTypeTable(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
