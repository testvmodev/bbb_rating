package com.bbb.core.repository.common;

import com.bbb.core.model.response.common.MarketListingFilterNotComp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketListingFilterNotCompRepository extends JpaRepository<MarketListingFilterNotComp, Long> {

    @Query(nativeQuery = true, value =
            //brand
            "SELECT * FROM " +
                    "(SELECT ABS(CHECKSUM(NewId())) AS id,  inventory.bicycle_brand_name AS name, :#{T(com.bbb.core.common.ValueCommons).BRAND} AS type " +
                    "FROM inventory " +
                    "WHERE " +
                    "inventory.is_delete = 0 AND " +
                    "inventory.status = :#{T(com.bbb.core.model.database.type.StatusInventory).ACTIVE.getValue()} AND " +
                    "(inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).READY_LISTED.getValue()} OR inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).LISTED.getValue()}) AND " +
                    ":isBrandNull = 0 " +
                    "GROUP BY inventory.bicycle_brand_name" +
                    ") AS brand " +

                    "UNION " +

                    //model
             "SELECT * FROM " +
                    "(SELECT ABS(CHECKSUM(NewId())) AS id, inventory.bicycle_model_name AS name, :#{T(com.bbb.core.common.ValueCommons).MODEL} AS type " +
                    "FROM inventory "+
                    "WHERE " +
                    "inventory.is_delete = 0 AND " +
                    "inventory.status = :#{T(com.bbb.core.model.database.type.StatusInventory).ACTIVE.getValue()} AND " +
                    "(inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).READY_LISTED.getValue()} OR inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).LISTED.getValue()}) AND " +
                    ":isModelNull = 0 " +
                    "GROUP BY inventory.bicycle_model_name" +
                    ") AS model " +

                    "UNION " +

                    //type
             "SELECT * FROM " +
                    "(SELECT ABS(CHECKSUM(NewId())) AS id,  inventory.bicycle_type_name AS name, :#{T(com.bbb.core.common.ValueCommons).TYPE_BICYCLE_INV} AS type " +
                    "FROM inventory " +
                    "WHERE " +
                    "inventory.is_delete = 0 AND " +
                    "inventory.status = :#{T(com.bbb.core.model.database.type.StatusInventory).ACTIVE.getValue()} AND " +
                    "(inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).READY_LISTED.getValue()} OR inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).LISTED.getValue()}) AND " +
                    ":isTypeNull = 0 " +
                    "GROUP BY  inventory.bicycle_type_name" +
                    ") AS type "+

                    "UNION " +
                    //size
            "SELECT * FROM " +
                    "(SELECT ABS(CHECKSUM(NewId())) AS id,  inventory.bicycle_size_name AS name, :#{T(com.bbb.core.common.ValueCommons).INVENTORY_SIZE_NAME} AS type " +
                    "FROM inventory " +
                    "WHERE " +
                    "inventory.is_delete = 0 AND " +
                    "inventory.status = :#{T(com.bbb.core.model.database.type.StatusInventory).ACTIVE.getValue()} AND " +
                    "(inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).READY_LISTED.getValue()} OR " +
                    "inventory.stage = :#{T(com.bbb.core.model.database.type.StageInventory).LISTED.getValue()}) AND " +
                    ":isSizeNull = 0 " +
                    "GROUP BY  inventory.bicycle_size_name" +
                    ") AS size"

    )
    List<MarketListingFilterNotComp> findAll(
            @Param(value = "isBrandNull") boolean isBrandNull,
            @Param(value = "isModelNull") boolean isModelNull,
            @Param(value = "isTypeNull") boolean isTypeNull,
            @Param(value = "isSizeNull") boolean isSizeNull
    );
}
