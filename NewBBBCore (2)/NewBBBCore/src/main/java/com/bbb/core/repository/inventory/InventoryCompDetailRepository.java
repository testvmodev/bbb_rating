package com.bbb.core.repository.inventory;

import com.bbb.core.model.database.InventoryCompDetail;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface InventoryCompDetailRepository extends CrudRepository<InventoryCompDetail, Long> {
    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_detail")
    List<InventoryCompDetail> findAll();

    @Query(nativeQuery = true, value = "SELECT * FROM inventory_comp_detail WHERE " +
            "inventory_comp_detail.inventory_comp_type_id = :inventoryCompTypeId")
    List<InventoryCompDetail> findAllFromCopType(@Param(value = "inventoryCompTypeId") long inventoryCompTypeId);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(nativeQuery = true, value = "DELETE from inventory_comp_detail " +
            "WHERE inventory_comp_detail.inventory_id = :inventoryId")
    void deleteAllByInventoryId(@Param("inventoryId") long inventoryId);

    @Query(nativeQuery = true, value = "SELECT TOP 1 * FROM " +
            "inventory_comp_detail join inventory_comp_type ON inventory_comp_detail.inventory_comp_type_id = inventory_comp_type.id " +
            "WHERE inventory_comp_type.name = :compName AND inventory_comp_detail.inventory_id = :inventoryId"
    )
    InventoryCompDetail findOneByCompNameAndInventoryId(
            @Param(value = "compName") String compName,
            @Param(value = "inventoryId") long inventoryId
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_comp_detail " +
            "WHERE inventory_id = :inventoryId " +
            "AND inventory_comp_type_id IN :typeIds")
    List<InventoryCompDetail> findByInventoryAndTypes(
            @Param("inventoryId") Long inventoryId,
            @Param("typeIds") List<Long> typeIds
    );

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM inventory_comp_detail " +
            "WHERE inventory_id = :inventoryId")
    List<InventoryCompDetail> findByInventory(
            @Param("inventoryId") long inventoryId
    );
}
