package com.bbb.core.model.otherservice.request.mail.offer.reject;

import com.bbb.core.model.otherservice.request.mail.offer.content.ListingMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.OfferMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.SellerMailRequest;
import com.bbb.core.model.otherservice.request.mail.offer.content.UserMailRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentOfferRejected {
    private UserMailRequest user;
    private ListingMailRequest listing;
    private OfferMailRequest offer;
    private SellerMailRequest seller;
    @JsonProperty(value = "base_path")
    private String basePath;

    public UserMailRequest getUser() {
        return user;
    }

    public void setUser(UserMailRequest user) {
        this.user = user;
    }

    public ListingMailRequest getListing() {
        return listing;
    }

    public void setListing(ListingMailRequest listing) {
        this.listing = listing;
    }

    public OfferMailRequest getOffer() {
        return offer;
    }

    public void setOffer(OfferMailRequest offer) {
        this.offer = offer;
    }

    public SellerMailRequest getSeller() {
        return seller;
    }

    public void setSeller(SellerMailRequest seller) {
        this.seller = seller;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
