package com.bbb.core.model.request.sort;

public enum  BicycleBrandSortField {
    NAME("name");

    private final String value;

    BicycleBrandSortField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
