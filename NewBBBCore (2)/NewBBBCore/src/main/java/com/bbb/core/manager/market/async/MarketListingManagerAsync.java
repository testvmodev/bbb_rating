package com.bbb.core.manager.market.async;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.config.AppConfig;
import com.bbb.core.common.ebay.EbayValue;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.common.utils.Single;
import com.bbb.core.common.utils.Third;
import com.bbb.core.controller.market.MarketListingCommon;
import com.bbb.core.manager.market.MarketListingUtils;
import com.bbb.core.manager.templaterest.RestEbayManager;
import com.bbb.core.model.database.*;
import com.bbb.core.model.database.type.StageInventory;
import com.bbb.core.model.database.type.StatusMarketListing;
import com.bbb.core.model.database.type.TypeLogEbayMarketListing;
import com.bbb.core.model.request.ebay.*;
import com.bbb.core.model.request.ebay.additem.*;
import com.bbb.core.model.request.ebay.additems.AddItemRequestContainer;
import com.bbb.core.model.request.ebay.additems.AddItemsRequest;
import com.bbb.core.model.request.ebay.enditem.EndItemRequest;
import com.bbb.core.model.request.ebay.enditems.EndItemRequestContainer;
import com.bbb.core.model.request.ebay.enditems.EndItemsRequest;
import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.bbb.core.model.response.ebay.additem.AddItemResponse;
import com.bbb.core.model.response.ebay.additems.AddItemsResponse;
import com.bbb.core.model.response.ebay.enditem.EndItemResponse;
import com.bbb.core.model.response.ebay.enditem.EndItemResponseContainer;
import com.bbb.core.model.response.ebay.enditem.EndItemsResponse;
import com.bbb.core.model.response.inventory.InventoryCompDetailRes;
import com.bbb.core.model.response.market.marketlisting.MarketToList;
import com.bbb.core.model.response.market.marketlisting.detail.InventoryCompTypeResponse;
import com.bbb.core.model.response.market.marketlisting.detail.MarketListingDetailResponse;
import com.bbb.core.repository.common.LogExceptionApiRepository;
import com.bbb.core.repository.inventory.InventoryImageRepository;
import com.bbb.core.repository.inventory.InventoryRepository;
import com.bbb.core.repository.inventory.InventorySpecRepository;
import com.bbb.core.repository.inventory.out.InventoryCompDetailResRepository;
import com.bbb.core.repository.market.EbayListingDurationRepository;
import com.bbb.core.repository.market.LogEbayMarketListingRepository;
import com.bbb.core.repository.market.MarketListingRepository;
import com.bbb.core.repository.market.MarketPlaceConfigRepository;
import com.bbb.core.repository.market.out.MarketToListRepository;
import com.bbb.core.repository.reout.ContentCommonRepository;
import com.bbb.core.repository.shipping.ShippingFeeConfigRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class MarketListingManagerAsync implements MessageResponses, EbayValue {
    private static final Logger LOG = LoggerFactory.getLogger(MarketListingManagerAsync.class);
    @Autowired
    private MarketPlaceConfigRepository marketPlaceConfigRepository;
    @Autowired
    private RestEbayManager restEbayManager;
    @Autowired
    private LogEbayMarketListingRepository logEbayMarketListingRepository;
    @Autowired
    private MarketListingRepository marketListingRepository;
    @Autowired
    private MarketToListRepository marketToListRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private InventoryImageRepository inventoryImageRepository;
    @Autowired
    private InventoryCompDetailResRepository inventoryCompDetailResRepository;
    @Autowired
    private InventorySpecRepository inventorySpecRepository;
    @Autowired
    private EbayListingDurationRepository ebayListingDurationRepository;
    @Autowired
    private LogExceptionApiRepository logExceptionApiRepository;
    @Autowired
    private ShippingFeeConfigRepository shippingFeeConfigRepository;
    @Autowired
    private ContentCommonRepository contentCommonRepository;
    @Autowired
    private AppConfig appConfig;


    public Pair<BaseEbayResponse, List<Long>> endAllItemEbayNoAsync(List<MarketListing> marketListings) {
        if (marketListings.size() > 0) {
            List<EndItemRequestContainer> conteContainers = new ArrayList<>();
            int index = -1;
            for (MarketListing marketListing : marketListings) {
                index++;
                if (marketListing.getItemId() == null) {
                    continue;
                }
                EndItemRequestContainer container = new EndItemRequestContainer();
                container.setItemID(marketListing.getItemId());
                container.setMessageID(index);
                conteContainers.add(container);
            }
            if (conteContainers.size() > 0) {
//                List<MarketPlaceConfig> marketPlaceConfigs = marketPlaceConfigRepository.findAllByMarketPlaceId(marketListings.get(0).getMarketPlaceId(), isSandbox);
                Third<Third<String, String, EndItemResponse>, Third<String, String, EndItemsResponse>, List<Long>>
                        res = endItems(conteContainers);
                if ((res.getFirst() != null && res.getFirst().getSecond() != null) || (res.getSecond() != null && res.getSecond().getSecond() != null)) {
                    LogEbayMarketListing logEbayMarketListing = new LogEbayMarketListing();
                    logEbayMarketListing.setRequest(res.getFirst() != null ? res.getFirst().getFirst() : res.getSecond().getFirst());
                    logEbayMarketListing.setResponse(res.getFirst() != null ? res.getFirst().getSecond() : res.getSecond().getSecond());
                    logEbayMarketListing.setMarketListingId(res.getFirst() != null ? null : marketListings.get(0).getId());
                    logEbayMarketListing.setType(res.getFirst() != null ? TypeLogEbayMarketListing.SINGLE : TypeLogEbayMarketListing.MULTI);
                    logEbayMarketListingRepository.save(logEbayMarketListing);
                }
                return res.getFirst() != null ? new Pair<>(res.getFirst().getThird(), res.getThird()) :
                        res.getSecond() != null ? new Pair<>(res.getSecond().getThird(), res.getThird()) : null;
            }
        }
        return null;
    }

    private Third<Third<String, String, EndItemResponse>, Third<String, String, EndItemsResponse>, List<Long>> endItems(List<EndItemRequestContainer> contentContainers) {
        List<Long> itemIdNotSuccesss = new ArrayList<>();
        if (contentContainers.size() == 1) {
            try {
                return new Third<>(endOneItem(contentContainers.get(0)), null, itemIdNotSuccesss);
            } catch (HttpClientErrorException | ResourceAccessException e) {
//                endItemRequestContainers.get(i).get
                itemIdNotSuccesss.add(contentContainers.get(0).getItemID());
                saveLogException(e, null);
                return new Third<>(null, null, itemIdNotSuccesss);
            }
        } else {
            if (contentContainers.size() <= EbayValue.MAX_END_ITEM) {
                EndItemsRequest endItemsRequest = new EndItemsRequest();
                endItemsRequest.setEndItemRequestContainers(contentContainers);
                try {
                    Third<String, String, EndItemsResponse> endItemsResponse = endItemsPrivate(contentContainers);
                    updateItemSuccess(endItemsResponse, itemIdNotSuccesss, contentContainers);
                    return new Third<>(null, endItemsResponse, itemIdNotSuccesss);
                } catch (HttpClientErrorException | ResourceAccessException e) {
                    itemIdNotSuccesss.addAll(contentContainers.stream().map(EndItemRequestContainer::getItemID).collect(Collectors.toList()));
                    saveLogException(e, null);
                    return new Third<>(null, null, itemIdNotSuccesss);
                }
            } else {
                int count = contentContainers.size() / EbayValue.MAX_END_ITEM;
                List<Observable<Third<String, String, EndItemsResponse>>> observables = new ArrayList<>();
                int add = 0;
                if (count * EbayValue.MAX_END_ITEM < contentContainers.size()) {
                    add++;
                }
                int numberThread;
                if (count + add <= 3) {
                    numberThread = count + add;
                } else {
                    numberThread = 3;
                }
                Executor executor = Executors.newFixedThreadPool(numberThread);
                for (int i = 0; i < count; i++) {
                    int start = i * EbayValue.MAX_END_ITEM;
                    int end = (i + 1) * EbayValue.MAX_END_ITEM;
                    observables.add(
                            Observable.create((ObservableOnSubscribe<Third<String, String, EndItemsResponse>>) t -> {
                                List<EndItemRequestContainer> endItemRequestContainers = contentContainers.subList(start, end);
                                try {
                                    Third<String, String, EndItemsResponse> endItemsResponse = endItemsPrivate(endItemRequestContainers);
                                    updateItemSuccess(endItemsResponse, itemIdNotSuccesss, contentContainers);
                                    t.onNext(endItemsResponse);
                                } catch (HttpClientErrorException | ResourceAccessException e) {
                                    itemIdNotSuccesss.addAll(endItemRequestContainers.stream().map(EndItemRequestContainer::getItemID).collect(Collectors.toList()));
                                    saveLogException(e, null);
                                }
                                t.onComplete();
                            }).subscribeOn(Schedulers.from(executor))
                    );
                }
                Third<String, String, EndItemResponse> responseThird = null;
                if (add == 1) {
                    int start = count * EbayValue.MAX_END_ITEM;
                    int end = contentContainers.size();
                    if (end - start == 1) {
                        try {
                            responseThird = endOneItem(contentContainers.get(start));
                            if (CommonUtils.getMessageError(responseThird.getThird().getErrors()) != null) {
                                itemIdNotSuccesss.add(contentContainers.get(start).getItemID());
                            }
                        } catch (HttpClientErrorException | ResourceAccessException e) {
                            itemIdNotSuccesss.add(contentContainers.get(start).getItemID());
                            saveLogException(e, null);
                        }
                    } else {
                        observables.add(
                                Observable.create((ObservableOnSubscribe<Third<String, String, EndItemsResponse>>) t -> {
                                    List<EndItemRequestContainer> endItemRequestContainers = contentContainers.subList(start, end);
                                    try {
                                        Third<String, String, EndItemsResponse> endItemsResponse = endItemsPrivate(endItemRequestContainers);
                                        updateItemSuccess(endItemsResponse, itemIdNotSuccesss, contentContainers);
                                        t.onNext(endItemsResponse);
                                    } catch (HttpClientErrorException | ResourceAccessException e) {
                                        itemIdNotSuccesss.addAll(endItemRequestContainers.stream().map(EndItemRequestContainer::getItemID).collect(Collectors.toList()));
                                        saveLogException(e, null);
                                    }
                                    t.onComplete();
                                }).subscribeOn(Schedulers.from(executor))
                        );
                    }
                }
                Single<Third<String, String, EndItemsResponse>> single = new Single<>();

                Observable.zip(observables,
                        os -> {
                            if (os == null) {
                                return new Third<>();
                            }
                            Third<String, String, EndItemsResponse> res = (Third<String, String, EndItemsResponse>) os[0];
                            for (int i = 1; i < os.length; i++) {
                                if (os[i] == null) {
                                    continue;
                                }
                                res.getThird().getEndItemResponseContainers().addAll(
                                        ((Third<String, String, EndItemsResponse>) os[i]).getThird().getEndItemResponseContainers()
                                );
                            }
                            return res;
                        })
                        .blockingSubscribe(sucess -> {
                            single.setValue((Third<String, String, EndItemsResponse>) sucess);
                        }, error -> {
                            single.setValue(null);
                        });

                if (responseThird != null) {
                    EndItemResponseContainer endRes = new EndItemResponseContainer();
                    endRes.setCorrelationID(contentContainers.size() - 1);
                    endRes.setErrors(responseThird.getThird().getErrors());
                    if (single.getValue() == null) {
                        single.setValue(new Third<>());
                        single.getValue().setThird(new EndItemsResponse());
                        single.getValue().getThird().setEndItemResponseContainers(new ArrayList<>());
                        single.getValue().getThird().setErrors(endRes.getErrors());
                    }
                    single.getValue().getThird().getEndItemResponseContainers().add(endRes);
                }
                return new Third<>(null, single.getValue(), itemIdNotSuccesss);
            }
        }
    }

    private void updateItemSuccess(Third<String, String, EndItemsResponse> endItemsResponse, List<Long> itemIdNotSuccesss, List<EndItemRequestContainer> contentContainers) {
        if (endItemsResponse != null && endItemsResponse.getThird() != null) {
            for (EndItemResponseContainer endItemResponseContainer : endItemsResponse.getThird().getEndItemResponseContainers()) {
                if (CommonUtils.getMessageError(endItemResponseContainer.getErrors()) != null) {
                    itemIdNotSuccesss.add(contentContainers.get(endItemResponseContainer.getCorrelationID()).getItemID());
                }
            }
        }
    }

    public void saveLogException(Exception e, String path) {
        LogExceptionApi logExceptionApi = new LogExceptionApi();
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        logExceptionApi.setContent(
                errors.toString()
        );
        logExceptionApi.setPath(path);
        logExceptionApi.setExceptionName(e.getClass().getSimpleName());
        logExceptionApiRepository.save(logExceptionApi);
    }

    private Third<String, String, EndItemResponse> endOneItem(EndItemRequestContainer endItemRequestContainer) {
        EndItemRequest endItemRequest = new EndItemRequest();
        endItemRequest.setItemID(endItemRequestContainer.getItemID());
        endItemRequest.setEndingReason("NotAvailable");
        return restEbayManager.postTradeIn(endItemRequest, EbayValue.END_ITEM, EndItemResponse.class);
    }

    private Third<String, String, EndItemsResponse> endItemsPrivate
            (List<EndItemRequestContainer> contentContainers) {
        EndItemsRequest endItemsRequest = new EndItemsRequest();
        endItemsRequest.setEndItemRequestContainers(contentContainers);
        return restEbayManager.postTradeIn(endItemsRequest, EbayValue.END_ITEMS, EndItemsResponse.class);
    }


    @Async
    public synchronized void endAllItemEbayAndDeList(long inventoryId, Long marketListingIdSold) {
        MarketToList marketToList = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToList == null) {
            return;
        }
        List<MarketListing> marketListings = marketListingRepository.findAllByInventoryIdActive(
                inventoryId
        );
        if (marketListings.size() == 0) {
            return;
        }
        List<MarketListing> marketListingEnds = new ArrayList<>();
        if (marketListingIdSold != null) {
            for (int i = 0; i < marketListings.size(); i++) {
                MarketListing marketListing = marketListings.get(i);
                if (marketListing.getId() == marketListingIdSold) {
                    marketListing.setStatus(StatusMarketListing.SALE_PENDING);
                } else {
                    if (marketListing.getStatus() == StatusMarketListing.LISTED && marketListing.getMarketPlaceConfigId() == marketToList.getMarketConfigId()) {
                        marketListingEnds.add(marketListing);
                    }
                    marketListing.setStatus(StatusMarketListing.DE_LISTED);
                }
            }
        }

        marketListingRepository.saveAll(marketListings);
        Inventory inventory = inventoryRepository.findOne(inventoryId);
        if (inventory != null) {
            inventory.setStage(StageInventory.SALE_PENDING);
            inventoryRepository.save(inventory);
        }


        if (marketListingEnds.size() > 0) {
            endAllItemEbayNoAsync(marketListingEnds);
        }
    }

    @Async
    public synchronized void endAllItemEbayAndDeListComplete(long inventoryId, Long marketListingIdSold) {
        MarketToList marketToList = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToList == null) {
            return;
        }
        List<MarketListing> marketListings = marketListingRepository.findAllMarketListing(
                inventoryId,
                marketToList.getMarketConfigId()
        );
        if (marketListings.size() == 0) {
            return;
        }
        List<MarketListing> marketListingEnds = new ArrayList<>();
        if (marketListingIdSold != null) {
            for (int i = 0; i < marketListings.size(); i++) {
                MarketListing marketListing = marketListings.get(i);
                if (marketListing.getId() == marketListingIdSold) {
                    marketListing.setStatus(StatusMarketListing.SOLD);
                } else {
                    if (marketListing.getStatus() == StatusMarketListing.LISTED) {
                        marketListingEnds.add(marketListing);
                    }
                    marketListing.setStatus(StatusMarketListing.DE_LISTED);
                }
            }
        }

        marketListingRepository.saveAll(marketListings);
        Inventory inventory = inventoryRepository.findOne(inventoryId);
        if (inventory != null) {
            inventory.setStage(StageInventory.SOLD);
            inventoryRepository.save(inventory);
        }

        if (marketListingEnds.size() > 0) {
            endAllItemEbayNoAsync(marketListingEnds);
        }
    }


    @Async
    public synchronized void listEbayAsyncFromInventoryId(long inventoryId) {
        List<Long> inventoryIds = new ArrayList<>();
        inventoryIds.add(inventoryId);
        listEbayAsyncFromInventoryIds(inventoryIds);
    }

    @Async
    public synchronized void listEbayAsyncFromInventoryIds(List<Long> inventoryIds) {
        MarketToList marketToList = marketToListRepository.findAllIdActiveEbay(appConfig.getEbay().isSandbox());
        if (marketToList == null) {
            return;
        }
        List<MarketListing> marketListings = marketListingRepository.findAllMarketListing(inventoryIds, marketToList.getMarketConfigId());
        if (marketListings.size() == 0) {
            return;
        }
        listEbay(marketListings, null);
    }

    @Async
    public synchronized void listEbayAsyncMarketListing(List<MarketListing> marketListings, String path) {
        listEbay(marketListings, path);
    }

    public synchronized void listEbay(List<MarketListing> marketListings, String pathApi) {
        List<Long> inventoriesId = marketListings.stream().map(MarketListing::getInventoryId).collect(Collectors.toList());
        List<Inventory> inventories = inventoryRepository.findAllByIds(
                inventoriesId
        );

        List<InventoryImage> inventoryImages = inventoryImageRepository.findAllByInventoriesId(inventoriesId);
        List<InventoryCompDetailRes> inventoryCompDetailRes = inventoryCompDetailResRepository.findAllComp(inventoriesId);
        List<InventorySpec> inventorySpecs = inventorySpecRepository.findAll(inventoriesId);

        AddItemRequest request;
        AddItemsRequest requests;
        List<Long> durationIds = marketListings.stream().map(MarketListing::getEbayListingDurationId)
                .filter(Objects::nonNull).collect(Collectors.toList());
        List<EbayListingDuration> ebayListingDurations;
        if (durationIds.size() > 0) {
            ebayListingDurations = ebayListingDurationRepository.findAllNotDelete(durationIds);
        } else {
            ebayListingDurations = new ArrayList<>();
        }
        CommonUtils.stream(durationIds);
        Pair<List<Pair<Long, ShippingFeeConfig>>, List<Pair<Long, String>>> shippingConfigFeeInventoryPair
                = MarketListingUtils.getListPairInventoryIdShipConfigBicycleType(inventories, contentCommonRepository, shippingFeeConfigRepository);
        List<Pair<Long, ShippingFeeConfig>> shippingConfigFeeInventory = shippingConfigFeeInventoryPair.getFirst();
        List<Long> inventoryIdValid = shippingConfigFeeInventory.stream().map(Pair::getFirst).collect(Collectors.toList());
        List<MarketListing> marketListingValid = new ArrayList<>();
        List<MarketListing> marketListingInValid = new ArrayList<>();
        for (MarketListing marketListing : marketListings) {
            if (inventoryIdValid.contains(marketListing.getInventoryId())) {
                marketListingValid.add(marketListing);
            } else {
                marketListingInValid.add(marketListing);
            }

        }
        if (marketListingValid.size() > 0) {
            if (marketListingValid.size() == 1) {
                request = new AddItemRequest();
                request.setErrorLanguage(ERROR_LANGUAGE);
                request.setWarningLevel(WARING_LEVEL);
                Pair<Long, ShippingFeeConfig> pairShippingFee = CommonUtils.findObject(shippingConfigFeeInventory, o -> o.getFirst() == marketListingValid.get(0).getInventoryId());
                Inventory inventory = CommonUtils.findObject(inventories, o -> o.getId() == pairShippingFee.getFirst());
                request.setItem(createItemEbay(
                        marketListingValid.get(0),
                        inventory,
                        inventoryImages.stream().map(InventoryImage::getImage).collect(Collectors.toList()),
                        inventoryCompDetailRes,
                        inventorySpecs,
                        CommonUtils.findObject(ebayListingDurations, o -> o.getId() == marketListingValid.get(0).getEbayListingDurationId()),
                        pairShippingFee.getSecond().getEbayShippingProfileId()
                ));
                requests = null;
            } else {
                requests = new AddItemsRequest();
                requests.setErrorLanguage(ERROR_LANGUAGE);
                requests.setWarningLevel(WARING_LEVEL);
                requests.setAddItemRequestContainers(new ArrayList<>());

                request = null;


                int messageId = 0;
                for (MarketListing marketListing : marketListingValid) {
                    Inventory inventory = CommonUtils.findObject(inventories, o -> o.getId() == marketListing.getInventoryId());
                    if (inventory != null) {
                        AddItemRequestContainer addItemRequestContainer = new AddItemRequestContainer();
                        addItemRequestContainer.setMessageID(messageId + "");
                        List<InventoryImage> images = CommonUtils.findObjects(inventoryImages, img -> img.getInventoryId() == marketListing.getInventoryId());
                        Pair<Long, ShippingFeeConfig> pairShippingFee = CommonUtils.findObject(shippingConfigFeeInventory, o -> o.getFirst() == marketListing.getInventoryId());
                        addItemRequestContainer.setItem(
                                createItemEbay(
                                        marketListing,
                                        inventory,
                                        images.stream().map(InventoryImage::getImage).collect(Collectors.toList()),
                                        CommonUtils.findObjects(inventoryCompDetailRes, comp -> comp.getId().getInventoryId() == marketListing.getInventoryId()),
                                        CommonUtils.findObjects(inventorySpecs, spec -> spec.getInventoryId() == marketListing.getInventoryId()),
                                        CommonUtils.findObject(ebayListingDurations, o -> o.getId() == marketListingValid.get(0).getEbayListingDurationId()),
                                        pairShippingFee.getSecond().getEbayShippingProfileId())
                        );
                        requests.getAddItemRequestContainers().add(addItemRequestContainer);
                    }
                    messageId++;
                }
            }
            Third<String, String, AddItemResponse> resEbay;
            Third<String, String, AddItemsResponse> resEbays;
            if (request != null) {
                try {
                    resEbay = restEbayManager.postTradeIn(
                            request,
                            EbayValue.ADD_ITEM, AddItemResponse.class);
                    resEbays = null;
                } catch (Exception e) {
                    updateListingErrorException(marketListingValid, e, pathApi);
                    return;
                }

            } else {
                try {
                    resEbays = restEbayManager.postTradeIn(
                            requests,
                            EbayValue.ADD_ITEMS, AddItemsResponse.class);
                    resEbay = null;
                } catch (Exception e) {
                    updateListingErrorException(marketListingValid, e, pathApi);
                    return;
                }

            }
            //test
            LogEbayMarketListing logEbayMarketListing = new LogEbayMarketListing();
            logEbayMarketListing.setRequest(resEbay != null ? resEbay.getFirst() : resEbays.getFirst());
            logEbayMarketListing.setResponse(resEbay != null ? resEbay.getSecond() : resEbays.getSecond());
            if (marketListingValid.size() == 1) {
                logEbayMarketListing.setType(TypeLogEbayMarketListing.SINGLE);
            } else {
                logEbayMarketListing.setType(TypeLogEbayMarketListing.MULTI);
            }

            String messageError = null;
            AddItemResponse addItemResponse = resEbay != null ? resEbay.getThird() : resEbays.getThird();
            if (addItemResponse != null && addItemResponse.getErrors() != null && addItemResponse.getErrors().size() > 0) {
                messageError = CommonUtils.getMessageError(addItemResponse.getErrors());
                LOG.info("messageError: " + messageError);
            }


            if (messageError != null) {
                if (marketListingValid.size() == 1) {
//                marketListings.get(0).setStatus(StatusMarketListing.LISTING_ERROR);
                    //todo udpate status is LISTING_ERROR
                    marketListingValid.get(0).setStatus(StatusMarketListing.ERROR);
                    marketListingValid.get(0).setCountListingError(marketListingValid.get(0).getCountListingError() + 1);
                    marketListingValid.get(0).setLastMsgError(messageError);
                } else {
                    MarketListingCommon.updateMarketListingEbay(marketListingValid, resEbays, inventories);
                }
                marketListingRepository.saveAll(marketListingValid);
                logEbayMarketListingRepository.save(logEbayMarketListing);
                return;
            }

            logEbayMarketListingRepository.save(logEbayMarketListing);
            if (marketListingValid.size() == 1) {
                marketListingValid.get(0).setStatus(StatusMarketListing.LISTED);
                marketListingValid.get(0).setItemId(resEbay.getThird().getItemID());
                marketListingValid.get(0).setCountListingError(marketListingValid.get(0).getCountListingError() + 1);
                marketListingValid.get(0).setLastMsgError(null);
                marketListingValid.get(0).setStartTime(resEbay.getThird().getStartTime());
                marketListingValid.get(0).setEndTime(resEbay.getThird().getEndTime());
                if (resEbay.getThird().getStartTime() != null) {
                    marketListingValid.get(0).setTimeListed(resEbay.getThird().getStartTime());
                } else {
                    marketListingValid.get(0).setTimeListed(LocalDateTime.now());
                }

                CommonUtils.findObject(inventories, o->o.getId() == marketListingValid.get(0).getInventoryId()).setStage(StageInventory.LISTED);
            } else {
                MarketListingCommon.updateMarketListingEbay(marketListingValid, resEbays, inventories);
            }
            inventoryRepository.saveAll(inventories);
            marketListingRepository.saveAll(marketListingValid);
        }
        if ( marketListingInValid.size() > 0 ){
            List<Pair<Long, String>> ebayErrors = shippingConfigFeeInventoryPair.getSecond();
            marketListingInValid = marketListingInValid.stream().peek(o->{
                o.setStatus(StatusMarketListing.ERROR);
                Pair<Long, String> er = CommonUtils.findObject(ebayErrors, os->os.getFirst() == o.getInventoryId());
                if (er != null ) {
                    o.setLastMsgError(er.getSecond());
                }
            }).collect(Collectors.toList());
            marketListingRepository.saveAll(marketListingInValid);
        }
    }

    private void updateListingErrorException(List<MarketListing> marketListings, Exception e, String path) {
        saveLogException(e, path);
        List<Long> marketListingIds = marketListings.stream().map(MarketListing::getId).collect(Collectors.toList());
        if (marketListingIds.size() == 0) {
            return;
        }
        marketListingRepository.updateStatusErrorAndLastMsgError(
                marketListingIds,
                e.getMessage()
        );
    }

    private Item createItemEbay(MarketListing ebay,
                                Inventory inventory,
                                List<String> linkImageInventories,
                                List<InventoryCompDetailRes> inventoryCompDetailRes,
                                List<InventorySpec> inventorySpecs,
                                EbayListingDuration ebayListingDuration,
                                long shippingConfigId) {
        Item item = new Item();
        item.setCountry(COUNTRY_US);
        item.setCurrency(CURRENCY_USD);
        item.setConditionId(CONDITION_ID_DEFAULT);
        item.setDispatchTimeMax(DISPATCH_TIME_MAX);
        item.setListingDuration(ebayListingDuration.getValue());

        item.setListingType(FIXED_PRICE_ITEM);
        item.setPaymentMethods(PAY_PAL);
        item.setPayPalEmailAddress(PAY_PAL_EMAIL_DEFAULT);
        item.setPostalCode(POST_CODE_DEFAULT + "");

        if (ebay.isBestOffer() && (ebay.getMinimumOfferAutoAcceptPrice() != null || ebay.getBestOfferAutoAcceptPrice() != null)) {
            item.setBestOfferDetails(new BestOfferDetails());
            item.getBestOfferDetails().setBestOfferEnabled(true);
            item.setListingDetails(new ListingDetails());
            if (ebay.getMinimumOfferAutoAcceptPrice() != null) {
                item.getListingDetails().setMinimumBestOfferPrice(new BestOfferAutoAcceptPrice());
                item.getListingDetails().getMinimumBestOfferPrice().setValue(ebay.getMinimumOfferAutoAcceptPrice());
            }
            if (ebay.getBestOfferAutoAcceptPrice() != null) {
                item.getListingDetails().setBestOfferAutoAcceptPrice(new BestOfferAutoAcceptPrice());
                item.getListingDetails().getBestOfferAutoAcceptPrice().setValue(ebay.getBestOfferAutoAcceptPrice());
            }

        }

        if (ebay.getEbayCategoryId() != null) {
            PrimaryCategory primaryCategory = new PrimaryCategory();
            primaryCategory.setCategoryID(ebay.getEbayCategoryId());
            item.setPrimaryCategory(primaryCategory);

            //ToDo
            //update sub category
//            if (ebay.getEbaySubCategoryId() != null) {
//                PrimaryCategory second = new PrimaryCategory();
//                second.setCategoryID(ebay.getEbaySubCategoryId());
//                item.setSecondaryCategory(second);
//            }
        }


        item.setTitle(inventory.getTitle() + " " + inventory.getName());

        String description = inventory.getDescription();
        description = description.replaceAll("&", "&amp;");
        description = description.replaceAll("[^\\x20-\\x7e]", "");
        item.setDescription(description);


        item.setStartPrice(inventory.getCurrentListedPrice());
        if (inventory.getDiscountedPrice() != inventory.getCurrentListedPrice()) {
            DiscountPriceInfo discountPriceInfo = new DiscountPriceInfo();
            discountPriceInfo.setOriginalRetailPrice(inventory.getCurrentListedPrice());
            item.setDiscountPriceInfo(discountPriceInfo);
        }
        item.setCategoryMappingAllowed(true);


        PictureDetails pictureDetails = new PictureDetails();
        pictureDetails.setPictureURL(linkImageInventories);
        item.setPictureDetails(
                pictureDetails
        );

        item.setQuantity(1);


        //itemSpecifics
        ItemSpecifics itemSpecifics = new ItemSpecifics();
        List<NameValueList> nameValueLists = new ArrayList<>();
        if (inventoryCompDetailRes.size() > 0) {
            for (InventoryCompDetailRes inventoryCompDetailRe : inventoryCompDetailRes) {
                List<String> values = new ArrayList<>();
                values.add(inventoryCompDetailRe.getValue());
                nameValueLists.add(new NameValueList(inventoryCompDetailRe.getName(), values));
            }
        }
        for (InventorySpec inventorySpec : inventorySpecs) {
            List<String> values = new ArrayList<>();
            values.add(inventorySpec.getValue());
            nameValueLists.add(new NameValueList(inventorySpec.getName(), values));
        }

        if (nameValueLists.size() > 0) {
            itemSpecifics.setNameValueLists(nameValueLists);
        }
        item.setItemSpecifics(itemSpecifics);


        ReturnPolicy returnPolicy = new ReturnPolicy();
        returnPolicy.setReturnsAcceptedOption(RETURN_ACCEPTED_OPTION_DEFAULT);
        returnPolicy.setRefundOption(REFUND_OPTION);
        returnPolicy.setReturnsWithinOption(RETURN_WITH_IN_OPTION);
        returnPolicy.setDescription(DESCRIPTION_RETURN);
        returnPolicy.setShippingCostPaidByOption(SHIPPING_COST_PAID_BY_OPTION);
        item.setReturnPolicy(returnPolicy);

        ShippingDetailsRequest shippingDetails = new ShippingDetailsRequest();
        CalculatedShippingRate calculatedShippingRate = new CalculatedShippingRate();
        calculatedShippingRate.setOriginatingPostalCode(POST_CODE_BBB);
        calculatedShippingRate.setMeasurementUnit(MEASUREMENT_UNIT);
        calculatedShippingRate.setPackageDepth(6);
        calculatedShippingRate.setPackageLength(7);
        calculatedShippingRate.setPackageWidth(7);
        calculatedShippingRate.setShippingPackage("PackageThickEnvelope");
        WeightCalculateShip weightMajor = new WeightCalculateShip();
        weightMajor.setMeasurementSystem(EbayValue.MEASUREMENT_SYSTEM);
        weightMajor.setUnit(EbayValue.UNIT_LBS);
        weightMajor.setValue(2);
        calculatedShippingRate.setWeightMajor(weightMajor);
        WeightCalculateShip weightMinor = new WeightCalculateShip();
        weightMinor.setMeasurementSystem(EbayValue.MEASUREMENT_SYSTEM);
        weightMinor.setUnit(EbayValue.UNIT_LBS);
        weightMajor.setValue(0);
        calculatedShippingRate.setWeightMinor(weightMinor);
        shippingDetails.setCalculatedShippingRate(calculatedShippingRate);
        shippingDetails.setPaymentInstructions(PAYMENT_INTRODUCTION);

        SalesTax salesTax = new SalesTax();
        salesTax.setSalesTaxPercent(8.75f);
        salesTax.setSalesTaxState(STATE_DEFAULT);
        shippingDetails.setSalesTax(salesTax);

        ShippingServiceOptions shippingServiceOptions = new ShippingServiceOptions();
        shippingServiceOptions.setFreeShipping(true);
        shippingServiceOptions.setShippingService(SHIPPING_SERVICE_DEFAULT);
        shippingServiceOptions.setShippingServicePriority(SHIPPING_SERVICE_PRIORITY);
        shippingDetails.setShippingServiceOptions(shippingServiceOptions);

        shippingDetails.setShippingType("Calculated");

        item.setShippingDetails(shippingDetails);


        item.setSite(SITE_US);
        SellerProfiles sellerProfiles = new SellerProfiles();
        SellerShippingProfile sellerShippingProfile = new SellerShippingProfile();
//        sellerShippingProfile.setShippingProfileID(SHIPPING_PROFILE_ID);
        sellerShippingProfile.setShippingProfileID(shippingConfigId);
        sellerProfiles.setSellerShippingProfile(sellerShippingProfile);

        SellerReturnProfile sellerReturnProfile = new SellerReturnProfile();
        sellerReturnProfile.setReturnProfileID(RETURN_PROFILE_ID);
        sellerProfiles.setSellerReturnProfile(sellerReturnProfile);

        SellerPaymentProfile sellerPaymentProfile = new SellerPaymentProfile();
        sellerPaymentProfile.setPaymentProfileID(PAYMENT_PROFILE_ID);
        sellerProfiles.setSellerPaymentProfile(sellerPaymentProfile);
        item.setSellerProfiles(sellerProfiles);
        return item;
    }

    public void updateCompMarketListingDetailNotAsyn(MarketListingDetailResponse
                                                             response, List<InventoryCompTypeResponse> inventoryCompTypeResponses) {
        if (inventoryCompTypeResponses.size() > 0) {
            for (InventoryCompTypeResponse inventoryCompTypeRespons : inventoryCompTypeResponses) {
                if (inventoryCompTypeRespons.getName() == null) {
                    continue;
                }
                switch (inventoryCompTypeRespons.getName()) {
                    case ValueCommons.SUSPENSION_NAME_INV_COMP:
                        response.setSuspensionName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.FRAME_MATERIAL_NAME_INV_COMP:
                        response.setFrameMaterialName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.BRAKE_TYPE_NAME_INV_COMP:
                        response.setBrakeName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.BOTTOM_BRACKET_NAME_INV_COMP:
                        response.setBottomBracketName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.GENDER_NAME_INV_COMP:
                        response.setGenderName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.COLOR_INV_COMP:
                        response.setColorName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.HEAD_SET_INV_COMP:
                        response.setHeadsetName(inventoryCompTypeRespons.getValue());
                        break;
                    case ValueCommons.INVENTORY_WHEEL_SIZE_NAME:
                        response.setWheelSizeName(inventoryCompTypeRespons.getValue());
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
