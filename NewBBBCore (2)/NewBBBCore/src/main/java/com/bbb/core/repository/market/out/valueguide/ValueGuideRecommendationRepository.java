package com.bbb.core.repository.market.out.valueguide;

import com.bbb.core.model.response.market.valueguide.ValueGuideRecommendation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ValueGuideRecommendationRepository extends JpaRepository<ValueGuideRecommendation, Long> {
//    private long id;
//    private String condition;
//    private String typeName;
//    private String sizeName;
//    private String brandName;
//    private String yearName;
//    private String frameMaterial;
//    private String brakeType;

    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS id, " +
            "inventory.condition AS condition, " +
            "inventory.bicycle_type_name AS type_name, " +
            "inventory.bicycle_size_name AS size_name, " +
            "inventory.bicycle_brand_name AS brand_name, " +
            "inventory.bicycle_year_name AS year_name, " +
            "inv_frame_mateiral.frame_material_name AS frame_material_name, " +
            "inv_brake_type.brake_type_name AS brake_type_name " +
            "FROM " +
            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS frame_material_name " +
                "FROM inventory_comp_type JOIN inventory_comp_detail ON inventory_comp_type.id = inventory_comp_detail.inventory_comp_type_id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_FRAME_MATERIAL_NAME}) AS inv_frame_mateiral "+
            "ON market_listing.inventory_id = inv_frame_mateiral.inventory_id "+
            "LEFT JOIN " +
                "(SELECT inventory_comp_detail.inventory_id AS inventory_id, inventory_comp_detail.value AS brake_type_name " +
                "FROM inventory_comp_type JOIN inventory_comp_detail ON inventory_comp_type.id = inventory_comp_detail.inventory_comp_type_id " +
                "WHERE inventory_comp_type.name = :#{T(com.bbb.core.common.ValueCommons).INVENTORY_BRAKE_TYPE_NAME}) AS inv_brake_type "+
            "ON market_listing.inventory_id = inv_brake_type.inventory_id "+
            "WHERE " +
            "inventory.bicycle_id = :bicycleId AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND " +
            "market_listing.market_place_id = :marketPlaceId "
    )
    List<ValueGuideRecommendation> findAll(
            @Param(value = "bicycleId") long bicycleId,
            @Param(value = "marketPlaceId") long marketPlaceId
    );
}
