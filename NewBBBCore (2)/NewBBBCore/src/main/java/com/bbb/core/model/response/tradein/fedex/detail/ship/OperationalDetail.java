package com.bbb.core.model.response.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class OperationalDetail {
    @JacksonXmlProperty(localName = "OriginLocationNumber")
    private long originLocationNumber;
    @JacksonXmlProperty(localName = "DestinationLocationNumber")
    private long destinationLocationNumber;
    @JacksonXmlProperty(localName = "IneligibleForMoneyBackGuarantee")
    private boolean ineligibleForMoneyBackGuarantee;
    @JacksonXmlProperty(localName = "DeliveryEligibilities")
    private String deliveryEligibilities;
    @JacksonXmlProperty(localName = "ServiceCode")
    private String serviceCode;
    @JacksonXmlProperty(localName = "PackagingCode")
    private String packagingCode;

    public long getOriginLocationNumber() {
        return originLocationNumber;
    }

    public void setOriginLocationNumber(long originLocationNumber) {
        this.originLocationNumber = originLocationNumber;
    }

    public long getDestinationLocationNumber() {
        return destinationLocationNumber;
    }

    public void setDestinationLocationNumber(long destinationLocationNumber) {
        this.destinationLocationNumber = destinationLocationNumber;
    }

    public boolean isIneligibleForMoneyBackGuarantee() {
        return ineligibleForMoneyBackGuarantee;
    }

    public void setIneligibleForMoneyBackGuarantee(boolean ineligibleForMoneyBackGuarantee) {
        this.ineligibleForMoneyBackGuarantee = ineligibleForMoneyBackGuarantee;
    }

    public String getDeliveryEligibilities() {
        return deliveryEligibilities;
    }

    public void setDeliveryEligibilities(String deliveryEligibilities) {
        this.deliveryEligibilities = deliveryEligibilities;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPackagingCode() {
        return packagingCode;
    }

    public void setPackagingCode(String packagingCode) {
        this.packagingCode = packagingCode;
    }
}
