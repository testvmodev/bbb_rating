package com.bbb.core.model.response.ebay.enditem;

import com.bbb.core.model.response.ebay.BaseEbayResponse;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

@JacksonXmlRootElement(localName = "EndItemResponse", namespace = "urn:ebay:apis:eBLBaseComponents")
public class EndItemResponse extends BaseEbayResponse {
    @JacksonXmlProperty(localName = "EndTime")
    private LocalDateTime endTime;
    @JacksonXmlProperty(localName = "StartTime")
    private LocalDateTime startTime;

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
}
