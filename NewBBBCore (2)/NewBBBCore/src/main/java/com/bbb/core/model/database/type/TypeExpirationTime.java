package com.bbb.core.model.database.type;

public enum TypeExpirationTime {
    OFFER_AUCTION("offer_auction"),
    OFFER_MARKET_LISTING("offer_market_listing"),
    PERSONAL_LISTING_FIRST_LIST("Personal listing fist time listed"),
    PERSONAL_LISTING_RENEW("Personal listing renewed");

    private final String value;

    TypeExpirationTime(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TypeExpirationTime findByValue(String value) {
        for (TypeExpirationTime type : TypeExpirationTime.values()) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        return null;
    }
}
