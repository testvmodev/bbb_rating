package com.bbb.core.manager.tracking;

import com.bbb.core.common.Constants;
import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.ValueCommons;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.common.utils.Pair;
import com.bbb.core.model.database.Favourite;
import com.bbb.core.model.database.MarketPlace;
import com.bbb.core.model.database.Tracking;
import com.bbb.core.model.database.type.*;
import com.bbb.core.model.request.tracking.RecentMarketListingRequest;
import com.bbb.core.model.request.tracking.RecentRequest;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.model.response.market.home.DealRecommendInvAucRes;
import com.bbb.core.model.response.market.home.DealRecommendMarketListingRes;
import com.bbb.core.model.response.market.home.DealRecommendResponse;
import com.bbb.core.model.response.market.home.recentview.RecentViewInventoryAuctionResponse;
import com.bbb.core.model.response.market.home.recentview.RecentViewMarketListingResponse;
import com.bbb.core.model.response.market.home.recentview.RecentViewResponse;
import com.bbb.core.model.response.tracking.RecentViewBicycleResponse;
import com.bbb.core.repository.favourite.FavouriteRepository;
import com.bbb.core.repository.market.MarketPlaceRepository;
import com.bbb.core.repository.market.out.honme.DealRecommendInvAucResRepository;
import com.bbb.core.repository.market.out.honme.DealRecommendMarketListingResRepository;
import com.bbb.core.repository.tracking.out.RecentViewBicycleResponseRepository;
import com.bbb.core.repository.tracking.TrackingRepository;
import com.bbb.core.repository.tracking.out.recentview.RecentViewInventoryAuctionResRepository;
import com.bbb.core.repository.tracking.out.recentview.RecentViewMarketListingResRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TrackingManager implements MessageResponses {
    @Autowired
    private DealRecommendMarketListingResRepository dealRecommendMarketListingResRepository;
    @Autowired
    private FavouriteRepository favouriteRepository;
    @Autowired
    private MarketPlaceRepository marketPlaceRepository;
    @Autowired
    private RecentViewInventoryAuctionResRepository recentViewInventoryAuctionResRepository;
    @Autowired
    private RecentViewMarketListingResRepository recentViewMarketListingResRepository;
    @Autowired
    private TrackingRepository trackingRepository;
    @Autowired
    private RecentViewBicycleResponseRepository recentViewBicycleResponseRepository;
    @Autowired
    private DealRecommendInvAucResRepository dealRecommendInvAucResRepository;

    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public Object getDeals() throws ExceptionResponse {
        boolean isFromMarketListing;
        if (CommonUtils.isLogined() && CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            isFromMarketListing = false;
        } else {
            isFromMarketListing = true;
        }
        List<DealRecommendResponse> responses = new ArrayList<>();
        if (isFromMarketListing) {
            MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
            if (marketPlace == null) {
                throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_DELETE, MARKET_PLACE_DELETED), HttpStatus.NOT_FOUND);
            }
            responses.addAll(dealRecommendMarketListingResRepository.findAllDeals(
                    marketPlace.getId(),
                    0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND
            ));
        } else {
            responses.addAll(
                    dealRecommendInvAucResRepository.findAllDealsFromOffer(
                            new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                            0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND
                    )
            );
            if (responses.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
                List<Long> tem = new ArrayList<>();
                tem.add(0L);

                responses.addAll(
                        dealRecommendInvAucResRepository.findAllDealsNotFromOffer(
                                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate()),
                                responses.size() == 0,
                                responses.size() == 0 ? tem : responses.stream().map(o -> ((DealRecommendInvAucRes) o).getInventoryAuctionId()).collect(Collectors.toList()),
                                0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND - responses.size()
                        )
                );
            }
        }

        if (isFromMarketListing) {
            if (CommonUtils.isLogined()) {
                List<Integer> marketListingIds = responses.stream().map(o -> ((DealRecommendMarketListingRes) o).getMarketListingId().intValue()).collect(Collectors.toList());
                if (marketListingIds.size() > 0) {
                    List<Integer> marketListingFavouriteIds =
                            favouriteRepository.filterMarketListingFavourites(CommonUtils.getUserLogin(), marketListingIds);
                    if (marketListingFavouriteIds.size() > 0) {
                        for (DealRecommendResponse response : responses) {
                            response.setFavourite(
                                    CommonUtils.contain(marketListingFavouriteIds, ((DealRecommendMarketListingRes) response).getMarketListingId(),
                                            (o1, o2) -> o1 - o2.intValue())
                            );
                        }
                    }
                }


            }
        } else {
            if (CommonUtils.isLogined()) {
                List<Integer> inventoryAuctionIds = responses.stream().map(o -> ((DealRecommendInvAucRes) o).getInventoryAuctionId().intValue()).collect(Collectors.toList());
                if (inventoryAuctionIds.size() > 0) {
                    List<Integer> inventoryAuctionFavouriteIds =
                            favouriteRepository.filterInventoryAuctionFavourites(CommonUtils.getUserLogin(), inventoryAuctionIds);
                    for (DealRecommendResponse response : responses) {
                        response.setFavourite(
                                CommonUtils.contain(inventoryAuctionFavouriteIds, ((DealRecommendInvAucRes) response).getInventoryAuctionId(),
                                        (o1, o2) -> o1 - o2.intValue())
                        );
                    }
                }

            }
        }

        return responses;
    }


    public Object getInventoryAuctionRecommendation() throws ExceptionResponse {
        List<Long> listTem = new ArrayList<>();
        listTem.add(0L);
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        if (marketPlace == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_DELETE, MARKET_PLACE_DELETED), HttpStatus.NOT_FOUND);
        }
        if (!CommonUtils.isLogined()) {
            return dealRecommendInvAucResRepository.findAllInventoryAuctionRecommendCommons(
                    false,
                    listTem,
                    new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                    0,
                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND);
        } else {
            List<DealRecommendInvAucRes> favourites =
                    dealRecommendInvAucResRepository.findAllFavourites(
                            false,
                            listTem,
                            CommonUtils.getUserLogin(),
                            new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                            0,
                            ValueCommons.MAX_SIZE_DEAL_RECOMMEND / 2);

            List<DealRecommendInvAucRes> recents =
                    dealRecommendInvAucResRepository.findAllRecents(
                            favourites.size() > 0,
                            favourites.size() == 0 ? listTem : favourites.stream().map(DealRecommendInvAucRes::getInventoryAuctionId).collect(Collectors.toList()),
                            CommonUtils.getUserLogin(),
                            new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                            0,
                            ValueCommons.MAX_SIZE_DEAL_RECOMMEND - favourites.size());

            List<DealRecommendInvAucRes> all = new ArrayList<>();
            all.addAll(favourites);
            all.addAll(recents);
            if (all.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
                if (favourites.size() == ValueCommons.MAX_SIZE_DEAL_RECOMMEND / 2) {
                    List<Long> inventoryAcutionIdsNotIn =
                            all.stream().map(DealRecommendInvAucRes::getInventoryAuctionId)
                                    .collect(Collectors.toList());
                    List<DealRecommendInvAucRes> remainFavourites =
                            dealRecommendInvAucResRepository.findAllFavourites(
                                    true,
                                    inventoryAcutionIdsNotIn,
                                    CommonUtils.getUserLogin(),
                                    new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                                    0,
                                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND - all.size());
                    all.addAll(
                            favourites.size(),
                            remainFavourites
                    );
                }
            }
            if (all.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
                List<Long> inventoryAcutionNotIn =
                        all.stream().map(DealRecommendInvAucRes::getInventoryAuctionId)
                                .collect(Collectors.toList());
                all.addAll(
                        dealRecommendInvAucResRepository.findAllInventoryAuctionRecommendCommons(
                                inventoryAcutionNotIn.size() > 0,
                                inventoryAcutionNotIn.size() == 0 ? listTem : inventoryAcutionNotIn,
                                new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(new Date()),
                                0,
                                ValueCommons.MAX_SIZE_DEAL_RECOMMEND - all.size())
                );
            }
            List<Integer> marketListingIds = all.stream().map(res -> res.getInventoryAuctionId().intValue()).collect(Collectors.toList());
            List<Integer> marketListingFavouriteIds =
                    favouriteRepository.filterMarketListingFavourites(CommonUtils.getUserLogin(), marketListingIds);
            for (DealRecommendInvAucRes response : all) {
                response.setFavourite(
                        CommonUtils.contain(marketListingFavouriteIds, response.getInventoryAuctionId(), (o1, o2) -> o1 - o2.intValue())
                );
            }
            return all;

        }
    }

    public Object getMarketListingRecommendation() throws ExceptionResponse {
        List<Long> listTem = new ArrayList<>();
        listTem.add(0L);
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        if (marketPlace == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_DELETE, MARKET_PLACE_DELETED), HttpStatus.NOT_FOUND);
        }
        if (!CommonUtils.isLogined()) {
            return dealRecommendMarketListingResRepository.findAllMarketListingRecommendCommons(
                    marketPlace.getId(),
                    false,
                    listTem,
                    0,
                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND);
        } else {
            List<DealRecommendMarketListingRes> favourites =
                    dealRecommendMarketListingResRepository.findAllFavourites(
                            marketPlace.getId(),
                            false,
                            listTem,
                            CommonUtils.getUserLogin(), 0,
                            ValueCommons.MAX_SIZE_DEAL_RECOMMEND / 2);

            List<DealRecommendMarketListingRes> recents =
                    dealRecommendMarketListingResRepository.findAllRecents(
                            favourites.size() > 0,
                            favourites.size() == 0 ? listTem : favourites.stream().map(DealRecommendMarketListingRes::getMarketListingId).collect(Collectors.toList()),
                            marketPlace.getId(),
                            CommonUtils.getUserLogin(), 0,
                            ValueCommons.MAX_SIZE_DEAL_RECOMMEND - favourites.size());
            List<DealRecommendMarketListingRes> all = new ArrayList<>();
            all.addAll(favourites);
            all.addAll(recents);
            if (all.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
                if (favourites.size() == ValueCommons.MAX_SIZE_DEAL_RECOMMEND / 2) {
                    List<Long> marketListingNotIn =
                            all.stream().map(DealRecommendMarketListingRes::getMarketListingId)
                                    .collect(Collectors.toList());
                    List<DealRecommendMarketListingRes> remainFavourites =
                            dealRecommendMarketListingResRepository.findAllFavourites(
                                    marketPlace.getId(),
                                    true,
                                    marketListingNotIn,
                                    CommonUtils.getUserLogin(),
                                    0,
                                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND - all.size());
                    all.addAll(
                            favourites.size(),
                            remainFavourites
                    );
                }
            }
            if (all.size() < ValueCommons.MAX_SIZE_DEAL_RECOMMEND) {
                List<Long> marketListingNotIn =
                        all.stream().map(DealRecommendMarketListingRes::getMarketListingId)
                                .collect(Collectors.toList());
                all.addAll(
                        dealRecommendMarketListingResRepository.findAllMarketListingRecommendCommons(
                                marketPlace.getId(),
                                marketListingNotIn.size() > 0,
                                marketListingNotIn.size() == 0 ? listTem : marketListingNotIn,
                                0,
                                ValueCommons.MAX_SIZE_DEAL_RECOMMEND - all.size())
                );
            }
            List<Integer> marketListingIds = all.stream().map(res -> res.getMarketListingId().intValue()).collect(Collectors.toList());
            List<Integer> marketListingFavouriteIds =
                    favouriteRepository.filterMarketListingFavourites(CommonUtils.getUserLogin(), marketListingIds);
            for (DealRecommendMarketListingRes response : all) {
                response.setFavourite(
                        CommonUtils.contain(marketListingFavouriteIds, response.getMarketListingId(), (o1, o2) -> o1 - o2.intValue())
                );
            }
            return all;
        }
    }

    public Object getRecentView(List<RecentMarketListingRequest> recents) throws ExceptionResponse {
        //remove the same item via id and marketListingType
        if (recents != null && recents.size() > 0) {
            for (int i = 0; i < recents.size() - 1; i++) {
                long time = recents.get(i).getTimeViewed().toDate().getTime();
                for (int j = i + 1; j < recents.size(); j++) {
                    if (recents.get(i).getId() == recents.get(j).getId() &&
                            recents.get(i).getMarketType() == recents.get(j).getMarketType()) {
                        if (time < recents.get(j).getTimeViewed().toDate().getTime()) {
                            recents.get(i).setTimeViewed(recents.get(j).getTimeViewed());
                        }
                        recents.remove(j);
                        j--;
                    }
                }
            }
        }
        //end

        if (!CommonUtils.isLogined()) {
            return getRecentTrackingMarketListing(recents);
        }
        if (CommonUtils.checkHaveRole(UserRoleType.WHOLESALER)) {
            return getRecentTrackingInventoryAuction(recents);
        } else {
            return getRecentTrackingMarketListing(recents);
        }
    }

    private Object getRecentTrackingInventoryAuction(List<RecentMarketListingRequest> recents) {
        //get auction
        recents = recents.stream().filter(recent -> recent.getMarketType() == TrackingType.WHOLE_SALLER).collect(Collectors.toList());
        List<Long> inventoryAuctionIds =
                recents.stream()
                        .map(RecentRequest::getId).collect(Collectors.toList());
        List<RecentViewInventoryAuctionResponse> inventoryAuctions;
        if (inventoryAuctionIds.size() > 0) {
            String currentDate = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate());
            inventoryAuctions =
                    recentViewInventoryAuctionResRepository.findAllInventoryAuction(inventoryAuctionIds, currentDate, MarketType.WHOLE_SALLER.getValue());
            for (RecentViewInventoryAuctionResponse marketListing : inventoryAuctions) {
                for (RecentMarketListingRequest recent : recents) {
                    if (recent.getId() == marketListing.getInventoryAuctionId()) {
                        marketListing.setLastViewTime(recent.getTimeViewed());
                        break;
                    }
                }
            }

        } else {
            inventoryAuctions = new ArrayList<>();
        }
        //end
        List<RecentViewInventoryAuctionResponse> responses = new ArrayList<>();
        if (CommonUtils.isLogined()) {
            String now = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).format(LocalDateTime.now().toDate());
            List<RecentViewInventoryAuctionResponse> recentsQuery =
                    recentViewInventoryAuctionResRepository.findAllTracking(
                            CommonUtils.getUserLogin(),
                            MarketType.WHOLE_SALLER.getValue(),
                            now,
                            0,
                            ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1
                    );
            for (RecentViewInventoryAuctionResponse recentViewResponse : recentsQuery) {
                boolean has = false;
                for (int i = 0; i < inventoryAuctions.size(); i++) {
                    RecentViewInventoryAuctionResponse inventoryAuction = inventoryAuctions.get(i);
                    if (inventoryAuction.getInventoryAuctionId() == recentViewResponse.getInventoryAuctionId()) {
                        if (recentViewResponse.getLastViewTime() != null && inventoryAuction.getLastViewTime() != null) {
                            if (recentViewResponse.getLastViewTime().toDate().getTime() > inventoryAuction.getLastViewTime().toDate().getTime()) {
                                responses.add(recentViewResponse);
                            } else {
                                responses.add(inventoryAuction);
                            }
                            inventoryAuctions.remove(i);
                            i--;
                            has = true;
                        }
                    }
                }

                if (!has) {
                    responses.add(recentViewResponse);
                }
            }
        }
        responses.addAll(inventoryAuctions);
        sortRecentView(responses);
        if (responses.size() > ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1) {
            responses = responses.subList(0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1);
        }

        //update favourite
        if (CommonUtils.isLogined()) {
            inventoryAuctionIds = responses.stream()
                    .map(RecentViewInventoryAuctionResponse::getInventoryAuctionId).collect(Collectors.toList());
            List<Favourite> favourites;
            if ( inventoryAuctionIds.size() > 0 ) {
                favourites = favouriteRepository.findAllFavouriteInventoryAuction(inventoryAuctionIds, CommonUtils.getUserLogin());
            }else {
                favourites = new ArrayList<>();
            }

            for (RecentViewInventoryAuctionResponse respons : responses) {
                for (Favourite favourite : favourites) {
                    if (respons.getInventoryAuctionId() == favourite.getInventoryAuctionId()) {
                        respons.setFavourite(true);
                        break;
                    }
                }
                if (respons.getFavourite() == null) {
                    respons.setFavourite(false);
                }
            }
            updateLastViewedInventoryAuctionAsyn(recents, CommonUtils.getUserLogin());
        }

        return responses;
    }

    private void sortRecentView(List<? extends RecentViewResponse> responses) {
        responses.sort((o1, o2) -> {
            if (o1.getLastViewTime() == null && o2.getLastViewTime() == null) {
                return 0;
            } else {
                if (o1.getLastViewTime() == null) {
                    return 1;
                } else {
                    if (o2.getLastViewTime() == null) {
                        return -1;
                    }
                }
            }
            return Long.compare(o2.getLastViewTime().toDate().getTime(), o1.getLastViewTime().toDate().getTime());
        });
    }


    private Object getRecentTrackingMarketListing(List<RecentMarketListingRequest> recents) throws ExceptionResponse {
        recents = recents.stream().filter(recent -> recent.getMarketType() == TrackingType.BBB).collect(Collectors.toList());
        MarketPlace marketPlace = marketPlaceRepository.findOneByType(MarketType.BBB.getValue());
        if (marketPlace == null) {
            throw new ExceptionResponse(new ObjectError(ObjectError.ERROR_DELETE, MARKET_PLACE_DELETED), HttpStatus.NOT_FOUND);
        }
        List<RecentViewMarketListingResponse> responses = new ArrayList<>();
        if (!CommonUtils.isLogined()) {
            if (recents.size() == 0) {
                return responses;
            }
        }
//        List<RecentViewResponse> auctions;
        List<RecentViewMarketListingResponse> marketListings;
        if (recents.size() > 0) {
            List<Long> marketListingIds =
                    recents.stream().filter(recent -> recent.getMarketType() == TrackingType.BBB)
                            .map(RecentRequest::getId).collect(Collectors.toList());

            if (marketListingIds.size() > 0) {
                marketListings =
                        recentViewMarketListingResRepository.findAllMarketListing(
                                StatusMarketListing.LISTED.getValue(),
                                marketListingIds,
                                MarketType.BBB.getValue(),
                                marketPlace.getId()
                        );

                for (RecentViewMarketListingResponse marketListing : marketListings) {
                    for (RecentMarketListingRequest recent : recents) {
                        if (recent.getMarketType() == TrackingType.BBB) {
                            if (recent.getId() == marketListing.getMarketListingId()) {
                                marketListing.setLastViewTime(recent.getTimeViewed());
                                break;
                            }
                        }
                    }
                }
            } else {
                marketListings = new ArrayList<>();
            }
        } else {
            marketListings = new ArrayList<>();
        }

        if (CommonUtils.isLogined()) {
            List<RecentViewMarketListingResponse> recentsQuery = recentViewMarketListingResRepository.findAllTracking(
                    CommonUtils.getUserLogin(),
                    MarketType.BBB.getValue(),
                    0,
                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1
            );
            for (RecentViewMarketListingResponse recentViewResponse : recentsQuery) {
                boolean has = false;

                for (int i = 0; i < marketListings.size(); i++) {
                    RecentViewMarketListingResponse marketListing = marketListings.get(i);
                    if (marketListing.getMarketListingId() == recentViewResponse.getMarketListingId()) {
                        if (recentViewResponse.getLastViewTime() != null && marketListing.getLastViewTime() != null) {
                            if (recentViewResponse.getLastViewTime().toDate().getTime() > marketListing.getLastViewTime().toDate().getTime()) {
                                responses.add(recentViewResponse);
                            } else {
                                responses.add(marketListing);
                            }
                            marketListings.remove(i);
                            i--;
                            has = true;
                        }
                    }
                }

                if (!has) {
                    responses.add(recentViewResponse);
                }
            }


        }
        responses.addAll(marketListings);

        sortRecentView(responses);
        if (responses.size() > ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1) {
            responses = responses.subList(0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1);
        }

        //update favourite
        if (CommonUtils.isLogined()) {
            List<Long> marketListingIds = responses.stream().filter(o -> o.getMarketType() == MarketType.BBB)
                    .map(RecentViewMarketListingResponse::getMarketListingId).collect(Collectors.toList());
            List<Favourite> favourites = favouriteRepository.findAllFavouriteMarketListing(marketListingIds, CommonUtils.getUserLogin());
            for (RecentViewMarketListingResponse respons : responses) {
                boolean isFarourite = false;
                for (Favourite favourite : favourites) {
                    if (respons.getMarketType() == MarketType.BBB) {
                        if (favourite.getType() == FavouriteType.MARKET_LIST) {
                            if (respons.getMarketListingId() == favourite.getMarketListingId()) {
                                isFarourite = true;
                                break;
                            }
                        }
                    }
                }
                respons.setFavourite(isFarourite);
            }
            updateLastViewedMarketListingAsync(recents, CommonUtils.getUserLogin());
        }

        return responses;
    }

    public Object getBicycleRecentTrackings(List<RecentRequest> recents) throws ExceptionResponse {
        //remove the same item via id and marketListingType
        if (recents != null && recents.size() > 0) {
            for (int i = 0; i < recents.size() - 1; i++) {
                long time = recents.get(i).getTimeViewed().toDate().getTime();
                for (int j = i + 1; j < recents.size(); j++) {
                    if (recents.get(i).getId() == recents.get(j).getId()) {
                        if (time < recents.get(j).getTimeViewed().toDate().getTime()) {
                            recents.get(i).setTimeViewed(recents.get(j).getTimeViewed());
                        }
                        recents.remove(j);
                        j--;
                    }
                }
            }
        }
        //end

        List<RecentViewBicycleResponse> bicycles;
        if (recents != null && recents.size() > 0) {

            //get auction
            List<Long> bicycleIds =
                    recents.stream()
                            .map(RecentRequest::getId).collect(Collectors.toList());

            if (bicycleIds.size() > 0) {
                bicycles =
                        recentViewBicycleResponseRepository.findAllBicycle(
                                bicycleIds
                        );
                for (RecentViewBicycleResponse bicycle : bicycles) {
                    for (RecentRequest recent : recents) {
                        if (recent.getId() == bicycle.getBicycleId()) {
                            bicycle.setLastViewTime(recent.getTimeViewed());
                            break;
                        }
                    }
                }

            } else {
                bicycles = new ArrayList<>();
            }
        } else {
            bicycles = new ArrayList<>();
        }

        List<RecentViewBicycleResponse> responses = new ArrayList<>();
        if (CommonUtils.isLogined()) {
            List<RecentViewBicycleResponse> recentsQuery = recentViewBicycleResponseRepository.findAllTracking(
                    CommonUtils.getUserLogin(),
                    0,
                    ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1
            );
            for (RecentViewBicycleResponse recentViewResponse : recentsQuery) {
                boolean has = false;
                for (int i = 0; i < bicycles.size(); i++) {
                    RecentViewBicycleResponse bicycle = bicycles.get(i);
                    if (bicycle.getBicycleId().equals(recentViewResponse.getBicycleId())) {
                        if (bicycle.getLastViewTime() != null && recentViewResponse.getLastViewTime() != null) {
                            if (bicycle.getLastViewTime().toDate().getTime() > recentViewResponse.getLastViewTime().toDate().getTime()) {
                                responses.add(bicycle);
                            } else {
                                responses.add(recentViewResponse);
                            }
                        } else {
                            responses.add(bicycle);
                        }
                        bicycles.remove(i);
                        i--;
                        has = true;
                    }
                }
                if (!has) {
                    responses.add(recentViewResponse);
                }
            }
        }
        responses.addAll(bicycles);

        responses.sort((o1, o2) -> {
            if (o1.getLastViewTime() == null && o2.getLastViewTime() == null) {
                return 0;
            } else {
                if (o1.getLastViewTime() == null) {
                    return 1;
                } else {
                    if (o2.getLastViewTime() == null) {
                        return -1;
                    }
                }
            }
            return Long.compare(o2.getLastViewTime().toDate().getTime(), o1.getLastViewTime().toDate().getTime());
        });
        if (responses.size() > ValueCommons.MAX_SIZE_DEAL_RECOMMEND + 1) {
            responses = responses.subList(0, ValueCommons.MAX_SIZE_DEAL_RECOMMEND);
        }
        if (CommonUtils.isLogined()) {
            updateLastViewedBicycleAsync(recents, CommonUtils.getUserLogin());
        }
        return responses;
    }

    private void updateLastViewedInventoryAuctionAsyn(List<RecentMarketListingRequest> recents, String userId) {
        Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            List<Pair<Long, LocalDateTime>> pairsInventoryAuction =
                    recents.stream()
                            .filter(recent -> recent.getMarketType() == TrackingType.WHOLE_SALLER)
                            .map(recent -> {
                                Pair<Long, LocalDateTime> pair = new Pair<>();
                                pair.setFirst(recent.getId());
                                pair.setSecond(recent.getTimeViewed());
                                return pair;
                            }).collect(Collectors.toList());

            if (pairsInventoryAuction.size() > 0) {
                List<Long> inventoryAuctionIds = pairsInventoryAuction.stream().map(Pair::getFirst).collect(Collectors.toList());
                List<Tracking> trackings = trackingRepository.findAllTrackingInventoryAuction(userId, inventoryAuctionIds);
                for (Tracking tracking : trackings) {
                    for (RecentRequest recent : recents) {
                        if (tracking.getInventoryAuctionId() == recent.getId()) {
                            tracking.setLastViewed(recent.getTimeViewed());
                        }
                    }
                }

                //create tracking when not found tracking
                if (inventoryAuctionIds.size() != trackings.size()) {
                    for (RecentMarketListingRequest recent : recents) {
                        boolean isHas = false;
                        for (Tracking tracking : trackings) {
                            if (tracking.getInventoryAuctionId() == recent.getId()) {
                                isHas = true;
                                break;
                            }
                        }
                        if (!isHas) {
                            Tracking tracking = new Tracking();
                            tracking.setLastViewed(recent.getTimeViewed());
                            tracking.setCountTracking(1);
                            tracking.setType(recent.getMarketType());
                            tracking.setInventoryAuctionId(recent.getId());
                            tracking.setUserId(userId);
                            trackings.add(tracking);
                        }
                    }
                }
                if (trackings.size() > 0) {
                    trackingRepository.saveAll(trackings);
                }
            }

            t.onNext(true);
            t.onComplete();
        }).subscribeOn(Schedulers.newThread())
                .subscribe();
    }

    private void updateLastViewedMarketListingAsync(List<RecentMarketListingRequest> recents, String userId) {
        Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            List<Pair<Long, LocalDateTime>> pairsMarketListing =
                    recents.stream()
                            .filter(recent -> recent.getMarketType() == TrackingType.BBB || recent.getMarketType() == TrackingType.EBAY)
                            .map(recent -> {
                                Pair<Long, LocalDateTime> pair = new Pair<>();
                                pair.setFirst(recent.getId());
                                pair.setSecond(recent.getTimeViewed());
                                return pair;
                            }).collect(Collectors.toList());

            if (pairsMarketListing.size() > 0) {
                List<Long> marketListingIds = pairsMarketListing.stream().map(Pair::getFirst).collect(Collectors.toList());
                List<Tracking> trackings = trackingRepository.findAllTrackingMarketListing(userId, marketListingIds);
                for (Tracking tracking : trackings) {
                    for (RecentRequest recent : recents) {
                        if (tracking.getMarketListingId() == recent.getId()) {
                            tracking.setLastViewed(recent.getTimeViewed());
                        }
                    }
                }

                //create tracking when not found tracking
                if (marketListingIds.size() != trackings.size()) {
                    for (RecentMarketListingRequest recent : recents) {
                        boolean isHas = false;
                        for (Tracking tracking : trackings) {
                            if (tracking.getMarketListingId() == recent.getId()) {
                                isHas = true;
                                break;
                            }
                        }
                        if (!isHas) {
                            Tracking tracking = new Tracking();
                            tracking.setLastViewed(recent.getTimeViewed());
                            tracking.setCountTracking(1);
                            tracking.setType(recent.getMarketType());
                            tracking.setMarketListingId(recent.getId());
                            tracking.setUserId(userId);
                            trackings.add(tracking);
                        }
                    }
                }
                if (trackings.size() > 0) {
                    trackingRepository.saveAll(trackings);
                }
            }

            t.onNext(true);
            t.onComplete();
        }).subscribeOn(Schedulers.newThread())
                .subscribe();
    }


    private void updateLastViewedBicycleAsync(List<RecentRequest> recents, String userId) {
        Observable.create((ObservableOnSubscribe<Boolean>) t -> {
            List<Pair<Long, LocalDateTime>> pairsBicycle =
                    recents.stream()
                            .map(recent -> {
                                Pair<Long, LocalDateTime> pair = new Pair<>();
                                pair.setFirst(recent.getId());
                                pair.setSecond(recent.getTimeViewed());
                                return pair;
                            }).collect(Collectors.toList());

            if (pairsBicycle.size() > 0) {
                List<Long> bicycleIds = pairsBicycle.stream().map(Pair::getFirst).collect(Collectors.toList());
                List<Tracking> trackings = trackingRepository.findAllTrackingBicycle(userId, bicycleIds);
                for (Tracking tracking : trackings) {
                    for (RecentRequest recent : recents) {
                        if (tracking.getBicycleId() == recent.getId()) {
                            tracking.setLastViewed(recent.getTimeViewed());
                        }
                    }
                }

                //create tracking when not found tracking
                if (bicycleIds.size() != trackings.size()) {
                    for (RecentRequest recent : recents) {
                        boolean isHas = false;
                        for (Tracking tracking : trackings) {
                            if (tracking.getBicycleId() == recent.getId()) {
                                isHas = true;
                                break;
                            }
                        }
                        if (!isHas) {
                            Tracking tracking = new Tracking();
                            tracking.setLastViewed(recent.getTimeViewed());
                            tracking.setCountTracking(1);
                            tracking.setType(TrackingType.BICYCLE);
                            tracking.setBicycleId(recent.getId());
                            tracking.setUserId(userId);
                            trackings.add(tracking);
                        }
                    }
                }
                if (trackings.size() > 0) {
                    trackingRepository.saveAll(trackings);
                }
            }

            t.onNext(true);
            t.onComplete();
        }).subscribeOn(Schedulers.newThread())
                .subscribe();
    }

}
