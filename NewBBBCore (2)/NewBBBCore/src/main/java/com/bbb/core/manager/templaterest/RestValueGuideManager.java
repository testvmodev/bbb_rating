package com.bbb.core.manager.templaterest;

import com.bbb.core.common.IntegratedServices;
import com.bbb.core.model.otherservice.response.valueguide.PredictionPriceResponse;
import com.bbb.core.model.otherservice.response.valueguide.VGBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RestValueGuideManager extends RestTemplateManager {
    @Autowired
    public RestValueGuideManager(IntegratedServices integratedServices) {
        super(integratedServices.getBaseUrlValueGuideV2());
    }

//    public List<ValueGuideResponse> getTradeInValue(long brandId, long modelId, long yearId) {
//        Map<String, String> params = new HashMap();
//        params.put("brandid", String.valueOf(brandId));
//        params.put("modelid", String.valueOf(modelId));
//        params.put("yearid", String.valueOf(yearId));
//        params.put("publickey", EndpointService.VALUE_GUIDE_PUBLIC_KEY);
//        params.put("apiKey", EndpointService.VALUE_GUIDE_API_KEY);
//        List<ValueGuideResponse> responses =
//                getObject(EndpointService.TRADE_IN_VALUE, "Content-Type", "application/json",params, new ParameterizedTypeReference<List<ValueGuideResponse>>() {});
//        return responses;
//    }

    public VGBaseResponse<PredictionPriceResponse> getPredictionPrice(
            String brand, String model, String year, Float msrp
    ) {
        Map<String, String> params = new HashMap();
        params.put("brand", brand);
        params.put("model", model);
        params.put("year", year);
        params.put("msrp", String.valueOf(msrp));

        VGBaseResponse<PredictionPriceResponse> response = getObject(
                IntegratedServices.PREDICTION_PRICE, params,
                new ParameterizedTypeReference<VGBaseResponse<PredictionPriceResponse>>() {});
        return response;
    }
}
