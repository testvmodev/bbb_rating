package com.bbb.core.manager.bicycle;

import com.bbb.core.common.MessageResponses;
import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.common.utils.CommonUtils;
import com.bbb.core.model.database.ComponentType;
import com.bbb.core.model.database.ComponentTypeSelect;
import com.bbb.core.model.request.bicycle.CompTypeSelectCreateRequest;
import com.bbb.core.model.request.bicycle.CompTypeSelectUpdateRequest;
import com.bbb.core.model.request.component.ComponentValuesGetRequest;
import com.bbb.core.model.response.ListObjResponse;
import com.bbb.core.model.response.ObjectError;
import com.bbb.core.repository.bicycle.ComponentTypeRepository;
import com.bbb.core.repository.bicycle.ComponentTypeSelectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ComponentTypeSelectManager {
    //region Beans
    @Autowired
    private ComponentTypeSelectRepository componentTypeSelectRepository;
    @Autowired
    private ComponentTypeRepository componentTypeRepository;
    //endregion

    //region API
    public Object createComponentValues(CompTypeSelectCreateRequest request) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        if (request.getComponentTypeId() == null ){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_NULL),
                    HttpStatus.BAD_REQUEST);
        }
        ComponentType compType = componentTypeRepository.findById((long) request.getComponentTypeId());
        if (compType == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_EXIST ),
                    HttpStatus.BAD_REQUEST);
        }
        if (!compType.isSelect()){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_SELECT ),
                    HttpStatus.BAD_REQUEST);
        }
        if (request.getValue() == null || request.getValue().trim().equals("")){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.VALUE_INVALID ),
                    HttpStatus.BAD_REQUEST);
        }
        ComponentTypeSelect typeSelect = new ComponentTypeSelect();
        typeSelect.setComponentTypeId(request.getComponentTypeId());
        typeSelect.setValue(request.getValue());
        typeSelect.setSortOrder(request.getSortOrder());
        typeSelect.setDelete(false);
        return componentTypeSelectRepository.save(typeSelect);
    }

    public Object getDetailComponentValues(ComponentValuesGetRequest request) throws ExceptionResponse{
        ListObjResponse<ComponentTypeSelect> response = new ListObjResponse<>(
                request.getPageable().getPageNumber(),
                request.getPageable().getPageSize()
        );
        response.setTotalItem(componentTypeSelectRepository.getCountByCompTypeId(request.getCompTypeId()));
        if (response.getTotalItem() > 0) {
            response.setData(componentTypeSelectRepository.findValuesSorted(
                    request.getCompTypeId(),
                    request.getSortField(),
                    request.getSortType(),
                    request.getPageable()
            ));
        }

        return response;
    }

    public Object getOneComponentValue(long componentValueId) throws ExceptionResponse {
        ComponentTypeSelect response = findOneValue(componentValueId);
        return response;
    }

    public Object updateComponentValues(long id, CompTypeSelectUpdateRequest request) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        if (request == null  ){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_VALUE_MUST_NOT_EMPTY ),
                    HttpStatus.BAD_REQUEST);
        }
        ComponentTypeSelect oldValue = componentTypeSelectRepository.getOne(id);
        if (oldValue == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.VALUE_NOT_EXIST ),
                    HttpStatus.BAD_REQUEST);
        }
        if (request.getComponentTypeId() != null){
            ComponentType compType = componentTypeRepository.findById((long) request.getComponentTypeId());
            if (compType == null){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_EXIST ),
                        HttpStatus.BAD_REQUEST);
            }
            if (!compType.isSelect()){
                throw new ExceptionResponse(
                        new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.COMPONENT_TYPE_NOT_SELECT ),
                        HttpStatus.BAD_REQUEST);
            }
            oldValue.setComponentTypeId(request.getComponentTypeId());
        }
        if (request.getValue() != null){
            oldValue.setValue(request.getValue());
        }
        if (request.getSortOrder() != null){
            oldValue.setSortOrder(request.getSortOrder());
        }
        return componentTypeSelectRepository.save(oldValue);
    }

    public Object deleteComponentValue(long id) throws ExceptionResponse{
        if (!CommonUtils.checkRoleAccessFromAdmin(CommonUtils.getUserRoles())) {
            throw new ExceptionResponse(
                    ObjectError.NO_PERMISSION,
                    MessageResponses.YOU_DONT_HAVE_PERMISSION,
                    HttpStatus.FORBIDDEN
            );
        }
        ComponentTypeSelect value = componentTypeSelectRepository.getOne(id);
        if (value == null){
            throw new ExceptionResponse(
                    new ObjectError(ObjectError.ERROR_PARAM, MessageResponses.VALUE_NOT_EXIST ),
                    HttpStatus.BAD_REQUEST);
        }
        value.setDelete(true);
        componentTypeSelectRepository.save(value);
        return MessageResponses.SUCCESS;
    }
    //endregion

    //region Shared
    private ComponentTypeSelect findOneValue(long componentValueId) throws ExceptionResponse {
        ComponentTypeSelect value = componentTypeSelectRepository.getOne(componentValueId);
        if (value == null) {
            throw new ExceptionResponse(
                    ObjectError.ERROR_PARAM,
                    MessageResponses.COMPONENT_TYPE_VALUE_NOT_EXIST,
                    HttpStatus.BAD_REQUEST
            );
        }
        return value;
    }
    //endregion
}
