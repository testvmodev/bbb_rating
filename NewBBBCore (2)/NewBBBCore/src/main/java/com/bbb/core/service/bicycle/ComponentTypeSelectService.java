package com.bbb.core.service.bicycle;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.model.request.bicycle.CompTypeSelectCreateRequest;
import com.bbb.core.model.request.bicycle.CompTypeSelectUpdateRequest;
import com.bbb.core.model.request.component.ComponentValuesGetRequest;

public interface ComponentTypeSelectService {
    Object createComponentValues(CompTypeSelectCreateRequest request) throws ExceptionResponse;

    Object getDetailComponentValues(ComponentValuesGetRequest request) throws ExceptionResponse;

    Object getOneComponentValue(long componentValueId) throws ExceptionResponse;

    Object updateComponentValue(long id, CompTypeSelectUpdateRequest request) throws ExceptionResponse;

    Object deleteComponentValue(long id) throws ExceptionResponse;
}
