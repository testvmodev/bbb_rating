package com.bbb.core.model.other;

import com.fasterxml.jackson.annotation.JsonProperty;
//import com.google.gson.annotations.SerializedName;

public class ValidationRules {
    @JsonProperty(value = "ValueType")
//    @SerializedName(value = "ValueType")
    private String valueType;

    @JsonProperty(value = "MaxValues")
//    @SerializedName(value = "MaxValues")
    private int maxValues;

    @JsonProperty(value = "SelectionMode")
//    @SerializedName(value = "SelectionMode")
    private String selectionMode;

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public int getMaxValues() {
        return maxValues;
    }

    public void setMaxValues(int maxValues) {
        this.maxValues = maxValues;
    }

    public String getSelectionMode() {
        return selectionMode;
    }

    public void setSelectionMode(String selectionMode) {
        this.selectionMode = selectionMode;
    }
}
