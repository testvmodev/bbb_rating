package com.bbb.core.model.otherservice.response.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDateTime;

public class UserDetailResponse {
    private String email;
    @JsonProperty("display_name")
    private String displayName;
//    @JsonProperty("last_login")
//    private LocalDateTime lastLogin;
//    @JsonProperty("date_created")
//    private LocalDateTime dateCreated;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

//    public LocalDateTime getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(LocalDateTime lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public LocalDateTime getDateCreated() {
//        return dateCreated;
//    }
//
//    public void setDateCreated(LocalDateTime dateCreated) {
//        this.dateCreated = dateCreated;
//    }
}
