package com.bbb.core.model.response.market.marketlisting;

import com.bbb.core.model.database.type.*;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class MarketListingResponse {
    @Id
    private int id;
    private int marketListingId;
    private long inventoryId;
    private long bicycleId;
//    private long modelId;
//    private long brandId;
//    private long yearId;
//    private long typeId;
//    private Long sizeId;

    private String bicycleName;
    private String bicycleModelName;
    private String bicycleBrandName;
    private String bicycleYearName;
    private String bicycleTypeName;
//    private String bicycleSizeName;

    //comp
    private String sizeName;
    private Long brakeTypeId;
    private String brakeName;
    private Long frameMaterialId;
    private String frameMaterialName;
    private String gender;
    private String suspension;
    private String wheelSize;
    private String frameSize;
    //end

    private long marketPlaceId;
    private MarketType type;


    private String imageDefault;
    private StatusMarketListing statusMarketListing;

    private StatusInventory statusInventory;
    private StageInventory stageInventory;
    private Float msrpPrice;
    private Float bbbValue;
    private Float overrideTradeInPrice;
    private Float initialListPrice;
    private Float currentListedPrice;
    private Float cogsPrice;
    private Float flatPriceChange;
    private float discountedPrice;
    private Float bestOfferAutoAcceptPrice;
    private Float minimumOfferAutoAcceptPrice;

    private String partnerId;
    private String sellerId;
    private String storefrontId;
    private String inventoryName;
    private Long typeInventoryId;
    private String typeInventoryName;
    private String title;
    private ConditionInventory condition;
    private String serialNumber;

    private String zipCode;
    private String countryName;
    private String countryCode;
    private String stateName;
    private String stateCode;
    private String cityName;
    private OutboundShippingType shippingType;
//    private float rating;

    private boolean isDelete;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Transient
    private Boolean isFavourite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public long getBicycleId() {
        return bicycleId;
    }

    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

//    public long getModelId() {
//        return modelId;
//    }
//
//    public void setModelId(long modelId) {
//        this.modelId = modelId;
//    }
//
//    public long getBrandId() {
//        return brandId;
//    }
//
//    public void setBrandId(long brandId) {
//        this.brandId = brandId;
//    }
//
//    public long getYearId() {
//        return yearId;
//    }
//
//    public void setYearId(long yearId) {
//        this.yearId = yearId;
//    }
//
//    public long getTypeId() {
//        return typeId;
//    }
//
//    public void setTypeId(long typeId) {
//        this.typeId = typeId;
//    }
//
//    public Long getSizeId() {
//        return sizeId;
//    }
//
//    public void setSizeId(Long sizeId) {
//        this.sizeId = sizeId;
//    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleModelName() {
        return bicycleModelName;
    }

    public void setBicycleModelName(String bicycleModelName) {
        this.bicycleModelName = bicycleModelName;
    }

    public String getBicycleBrandName() {
        return bicycleBrandName;
    }

    public void setBicycleBrandName(String bicycleBrandName) {
        this.bicycleBrandName = bicycleBrandName;
    }

    public String getBicycleYearName() {
        return bicycleYearName;
    }

    public void setBicycleYearName(String bicycleYearName) {
        this.bicycleYearName = bicycleYearName;
    }

    public String getBicycleTypeName() {
        return bicycleTypeName;
    }

    public void setBicycleTypeName(String bicycleTypeName) {
        this.bicycleTypeName = bicycleTypeName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Long getBrakeTypeId() {
        return brakeTypeId;
    }

    public void setBrakeTypeId(Long brakeTypeId) {
        this.brakeTypeId = brakeTypeId;
    }

    public String getBrakeName() {
        return brakeName;
    }

    public void setBrakeName(String brakeName) {
        this.brakeName = brakeName;
    }

    public Long getFrameMaterialId() {
        return frameMaterialId;
    }

    public void setFrameMaterialId(Long frameMaterialId) {
        this.frameMaterialId = frameMaterialId;
    }

    public String getFrameMaterialName() {
        return frameMaterialName;
    }

    public void setFrameMaterialName(String frameMaterialName) {
        this.frameMaterialName = frameMaterialName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSuspension() {
        return suspension;
    }

    public void setSuspension(String suspension) {
        this.suspension = suspension;
    }

    public String getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(String wheelSize) {
        this.wheelSize = wheelSize;
    }

    public String getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(String imageDefault) {
        this.imageDefault = imageDefault;
    }

    public StatusMarketListing getStatusMarketListing() {
        return statusMarketListing;
    }

    public void setStatusMarketListing(StatusMarketListing statusMarketListing) {
        this.statusMarketListing = statusMarketListing;
    }

    public Float getMsrpPrice() {
        return msrpPrice;
    }

    public void setMsrpPrice(Float msrpPrice) {
        this.msrpPrice = msrpPrice;
    }

    public Float getBbbValue() {
        return bbbValue;
    }

    public void setBbbValue(Float bbbValue) {
        this.bbbValue = bbbValue;
    }

    public Float getOverrideTradeInPrice() {
        return overrideTradeInPrice;
    }

    public void setOverrideTradeInPrice(Float overrideTradeInPrice) {
        this.overrideTradeInPrice = overrideTradeInPrice;
    }

    public Float getInitialListPrice() {
        return initialListPrice;
    }

    public void setInitialListPrice(Float initialListPrice) {
        this.initialListPrice = initialListPrice;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public Float getCogsPrice() {
        return cogsPrice;
    }

    public void setCogsPrice(Float cogsPrice) {
        this.cogsPrice = cogsPrice;
    }

    public Float getFlatPriceChange() {
        return flatPriceChange;
    }

    public void setFlatPriceChange(Float flatPriceChange) {
        this.flatPriceChange = flatPriceChange;
    }

    public float getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(float discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Float getBestOfferAutoAcceptPrice() {
        return bestOfferAutoAcceptPrice;
    }

    public void setBestOfferAutoAcceptPrice(Float bestOfferAutoAcceptPrice) {
        this.bestOfferAutoAcceptPrice = bestOfferAutoAcceptPrice;
    }

    public Float getMinimumOfferAutoAcceptPrice() {
        return minimumOfferAutoAcceptPrice;
    }

    public void setMinimumOfferAutoAcceptPrice(Float minimumOfferAutoAcceptPrice) {
        this.minimumOfferAutoAcceptPrice = minimumOfferAutoAcceptPrice;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }


    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public Long getTypeInventoryId() {
        return typeInventoryId;
    }

    public void setTypeInventoryId(Long typeInventoryId) {
        this.typeInventoryId = typeInventoryId;
    }

    public String getTypeInventoryName() {
        return typeInventoryName;
    }

    public void setTypeInventoryName(String typeInventoryName) {
        this.typeInventoryName = typeInventoryName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ConditionInventory getCondition() {
        return condition;
    }

    public void setCondition(ConditionInventory condition) {
        this.condition = condition;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public long getMarketPlaceId() {
        return marketPlaceId;
    }

    public void setMarketPlaceId(long marketPlaceId) {
        this.marketPlaceId = marketPlaceId;
    }

    public MarketType getType() {
        return type;
    }

    public void setType(MarketType type) {
        this.type = type;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    //support old response model
    @Deprecated
    public String getBicycleSizeName() {
        return frameSize;
    }

//    public void setBicycleSizeName(String bicycleSizeName) {
//        this.bicycleSizeName = bicycleSizeName;
//    }

    public int getMarketListingId() {
        return marketListingId;
    }

    public void setMarketListingId(int marketListingId) {
        this.marketListingId = marketListingId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public String getLocation() {
        return cityName + ", " + stateCode +", " + zipCode;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(String frameSize) {
        this.frameSize = frameSize;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public StatusInventory getStatusInventory() {
        return statusInventory;
    }

    public void setStatusInventory(StatusInventory statusInventory) {
        this.statusInventory = statusInventory;
    }

    public StageInventory getStageInventory() {
        return stageInventory;
    }

    public void setStageInventory(StageInventory stageInventory) {
        this.stageInventory = stageInventory;
    }

    public OutboundShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(OutboundShippingType shippingType) {
        this.shippingType = shippingType;
    }

//    public float getRating() {
//        return rating;
//    }
//
//    public void setRating(float rating) {
//        this.rating = rating;
//    }
}
