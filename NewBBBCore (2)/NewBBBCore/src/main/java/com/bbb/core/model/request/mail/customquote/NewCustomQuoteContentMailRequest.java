package com.bbb.core.model.request.mail.customquote;

public class NewCustomQuoteContentMailRequest {
    private String shop_name;
    private String bike_name;
    private String employee_email;

    public NewCustomQuoteContentMailRequest(String shop_name, String bike_name, String employee_email) {
        this.shop_name = shop_name;
        this.bike_name = bike_name;
        this.employee_email = employee_email;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getBike_name() {
        return bike_name;
    }

    public void setBike_name(String bike_name) {
        this.bike_name = bike_name;
    }

    public String getEmployee_email() {
        return employee_email;
    }

    public void setEmployee_email(String employee_email) {
        this.employee_email = employee_email;
    }
}
