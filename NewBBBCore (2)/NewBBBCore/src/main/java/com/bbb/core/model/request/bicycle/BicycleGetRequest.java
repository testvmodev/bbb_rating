package com.bbb.core.model.request.bicycle;

import com.bbb.core.model.request.sort.BicycleSortField;
import com.bbb.core.model.request.sort.Sort;
import org.springframework.data.domain.Pageable;

public class BicycleGetRequest {
    private BicycleFilterRequest filter;
    private Pageable pageable;
    private BicycleSortField sortField;
    private Sort sortType;

    public BicycleFilterRequest getFilter() {
        return filter;
    }

    public void setFilter(BicycleFilterRequest filter) {
        this.filter = filter;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public BicycleSortField getSortField() {
        return sortField;
    }

    public void setSortField(BicycleSortField sortField) {
        this.sortField = sortField;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }
}
