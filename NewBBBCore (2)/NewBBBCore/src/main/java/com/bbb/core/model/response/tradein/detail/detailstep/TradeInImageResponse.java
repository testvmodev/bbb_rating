package com.bbb.core.model.response.tradein.detail.detailstep;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TradeInImageResponse {
    @Id
    private long imageId;
    private long imageTypeId;
    private String imageTypeName;
    private String fullLink;

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(long imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    public String getFullLink() {
        return fullLink;
    }

    public void setFullLink(String fullLink) {
        this.fullLink = fullLink;
    }

    public String getImageTypeName() {
        return imageTypeName;
    }

    public void setImageTypeName(String imageTypeName) {
        this.imageTypeName = imageTypeName;
    }
}
