package com.bbb.core.model.database;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class InventoryCompType {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected long id;
  protected String name;
  protected boolean isSelect;
  private float additionalValue;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public boolean isSelect() {
    return isSelect;
  }

  public void setSelect(boolean select) {
    isSelect = select;
  }

  public float getAdditionalValue() {
    return additionalValue;
  }

  public void setAdditionalValue(float additionalValue) {
    this.additionalValue = additionalValue;
  }
}
