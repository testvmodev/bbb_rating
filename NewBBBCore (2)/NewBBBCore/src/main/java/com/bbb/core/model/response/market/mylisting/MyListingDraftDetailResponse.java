package com.bbb.core.model.response.market.mylisting;

import com.bbb.core.model.database.MyListingImageDraft;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.List;

@Entity
public class MyListingDraftDetailResponse {
    @Id
    @Column(name = "id")
    private long myListingDraftId;
    private String content;
    @CreatedDate
    @Generated(value = GenerationTime.INSERT)
    private LocalDateTime createdTime;
    @LastModifiedDate
    @Generated(GenerationTime.ALWAYS)
    private LocalDateTime lastUpdate;
    private boolean isDelete;
    private Float currentListedPrice;
    private String title;
    private Boolean isBestOffer;
    private String createdBy;
    @Transient
    private List<MyListingImageDraft> imageDrafts;
    private String storefrontId;

    public long getMyListingDraftId() {
        return myListingDraftId;
    }

    public void setMyListingDraftId(long myListingDraftId) {
        this.myListingDraftId = myListingDraftId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @JsonIgnore
    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Float getCurrentListedPrice() {
        return currentListedPrice;
    }

    public void setCurrentListedPrice(Float currentListedPrice) {
        this.currentListedPrice = currentListedPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getBestOffer() {
        return isBestOffer;
    }

    public void setBestOffer(Boolean bestOffer) {
        isBestOffer = bestOffer;
    }

    @JsonIgnore
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<MyListingImageDraft> getImageDrafts() {
        return imageDrafts;
    }

    public void setImageDrafts(List<MyListingImageDraft> imageDrafts) {
        this.imageDrafts = imageDrafts;
    }

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }
}
