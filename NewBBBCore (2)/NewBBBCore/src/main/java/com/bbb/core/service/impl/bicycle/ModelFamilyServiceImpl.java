package com.bbb.core.service.impl.bicycle;

import com.bbb.core.manager.bicycle.ModelFamilyManager;
import com.bbb.core.service.bicycle.ModelFamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelFamilyServiceImpl implements ModelFamilyService {
    @Autowired
    private ModelFamilyManager manager;
    @Override
    public Object getModelFamilies(long brandId) {
        return manager.getModelFamilies(brandId);
    }
}
