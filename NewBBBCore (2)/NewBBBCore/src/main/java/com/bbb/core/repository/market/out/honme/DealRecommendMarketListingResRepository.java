package com.bbb.core.repository.market.out.honme;

import com.bbb.core.model.response.market.home.DealRecommendMarketListingRes;
import com.bbb.core.model.response.market.home.DealRecommendResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRecommendMarketListingResRepository extends JpaRepository<DealRecommendMarketListingRes, Long> {
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.status = 'Listed' " +
            "ORDER BY " +
            "market_listing.tracking_count DESC, market_listing.time_listed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendMarketListingRes> findAllDeals(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "market_listing.is_delete = 0 AND " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND " +
            "(:isHasMarketListingIds = 0 OR market_listing.id NOT IN :marketListingIdsNotIn)" +
            "ORDER BY " +
            "market_listing.time_listed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendMarketListingRes> findAllMarketListingRecommendCommons(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "isHasMarketListingIds") boolean isHasMarketListingIds,
            @Param(value = "marketListingIdsNotIn") List<Long> marketListingIdsNotIn,
            @Param(value = "offset") long offset,
            @Param(value = "size") long size
    );



    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "favourite JOIN market_listing ON favourite.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "favourite.user_id = :userId AND " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.status = 'Listed' AND " +
            "favourite.is_delete = 0 AND " +
            "market_listing.is_delete = 0 AND " +
            "(:isHasMarketListingIds = 0 OR market_listing.id NOT IN :marketListingIdsNotIn)" +
            "ORDER BY " +
            "favourite.created_time DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendMarketListingRes> findAllFavourites(
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "isHasMarketListingIds") boolean isHasMarketListingIds,
            @Param(value = "marketListingIdsNotIn") List<Long> marketListingIdsNotIn,
            @Param(value = "userId") String userId,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );

    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "tracking JOIN market_listing ON tracking.market_listing_id = market_listing.id " +
            "JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "tracking.user_id = :userId AND " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "market_listing.status = 'Listed' AND " +
            "market_listing.is_delete = 0 AND " +
            "tracking.market_listing_id IS NOT NULL AND " +
            "(:hasMarketListingIds = 0 OR tracking.market_listing_id NOT IN :marketListingIds) " +
            "ORDER BY " +
            "tracking.last_viewed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendMarketListingRes> findAllRecents(
            @Param(value = "hasMarketListingIds") boolean hasMarketListingIds,
            @Param(value = "marketListingIds") List<Long> marketListingIds,
            @Param(value = "marketPlaceId") long marketPlaceId,
            @Param(value = "userId") String userId,
            @Param(value = "offset") int offset,
            @Param(value = "size") int size
    );


    //select value guide
    @Query(nativeQuery = true, value = "SELECT " +
            "market_listing.id AS market_listing_id, " +
            "market_listing.inventory_id AS inventory_id, " +
            "inventory.title AS inventory_title, " +
            "inventory.bicycle_type_name AS bicycle_type_name, " +
            "inventory.current_listed_price AS current_listed_price, " +
            "inventory.discounted_price AS discounted_price, " +
            "inventory.image_default AS image_default, " +
            "inventory.bicycle_size_name AS bicycle_size_name " +
            "FROM " +
            "market_listing JOIN inventory ON market_listing.inventory_id = inventory.id " +
            "WHERE " +
            "market_listing.status = :#{T(com.bbb.core.model.database.type.StatusMarketListing).LISTED.getValue()} AND " +
            "market_listing.market_place_id = :marketPlaceId AND " +
            "(:isNullMarketListingId = 1 OR market_listing.id NOT IN :marketListingIds) " +
            "ORDER BY market_listing.time_listed DESC " +
            "OFFSET :offset ROWS FETCH NEXT :size ROWS ONLY"
    )
    List<DealRecommendMarketListingRes> findRecommendationValueGuide(
      @Param(value = "marketPlaceId") long marketPlaceId,
      @Param(value = "isNullMarketListingId") boolean isNullMarketListingId,
      @Param(value = "marketListingIds") List<Long> marketListingIds,
      @Param(value = "offset") int offset,
      @Param(value = "size") int size

    );
}
