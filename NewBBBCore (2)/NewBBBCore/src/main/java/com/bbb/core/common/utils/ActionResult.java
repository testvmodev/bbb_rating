package com.bbb.core.common.utils;

public interface ActionResult<T> {
    T call();
}
