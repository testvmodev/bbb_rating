package com.bbb.core.model.request.tradein.fedex.detail.ship;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class FedexLabelSpecification {
    @JacksonXmlProperty(localName = "LabelFormatType")
    private String labelFormatType;
    @JacksonXmlProperty(localName = "ImageType")
    private String imageType;
    @JacksonXmlProperty(localName = "LabelStockType")
    private String labelStockType;

    public FedexLabelSpecification() {}

    public FedexLabelSpecification(String labelFormatType, String imageType, String labelStockType) {
        this.labelFormatType = labelFormatType;
        this.imageType = imageType;
        this.labelStockType = labelStockType;
    }

    public String getLabelFormatType() {
        return labelFormatType;
    }

    public void setLabelFormatType(String labelFormatType) {
        this.labelFormatType = labelFormatType;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getLabelStockType() {
        return labelStockType;
    }

    public void setLabelStockType(String labelStockType) {
        this.labelStockType = labelStockType;
    }
}
