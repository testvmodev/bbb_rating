package com.bbb.core.model.response.onlinestore;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OnlineStoreResponse {
    @Id
    private String storefrontId;
    private int listedMarketListings;
    private int soldMarketListings;

    public String getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(String storefrontId) {
        this.storefrontId = storefrontId;
    }

    public int getListedMarketListings() {
        return listedMarketListings;
    }

    public void setListedMarketListings(int listedMarketListings) {
        this.listedMarketListings = listedMarketListings;
    }

    public int getSoldMarketListings() {
        return soldMarketListings;
    }

    public void setSoldMarketListings(int soldMarketListings) {
        this.soldMarketListings = soldMarketListings;
    }
}
