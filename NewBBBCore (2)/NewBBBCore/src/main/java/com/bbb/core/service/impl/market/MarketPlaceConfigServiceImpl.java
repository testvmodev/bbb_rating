package com.bbb.core.service.impl.market;

import com.bbb.core.common.exception.ExceptionResponse;
import com.bbb.core.manager.market.MarketPlaceConfigManager;
import com.bbb.core.model.request.market.MarketPlaceConfigCreateRequest;
import com.bbb.core.service.market.MarketPlaceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarketPlaceConfigServiceImpl implements MarketPlaceConfigService {
    @Autowired
    private MarketPlaceConfigManager manager;

    public Object getAllMarketPlaceConfig() {
        return manager.getAllMarketPlaceConfig();
    }

    public Object getMarketPlaceConfigDetail(long id) throws ExceptionResponse {
        return manager.getMarketPlaceConfigDetail(id);
    }

    public Object postMarketPlaceConfig(MarketPlaceConfigCreateRequest request) throws ExceptionResponse {
        return manager.postMarketPlaceConfig(request);
    }

    public Object updateMarketPlaceConfig(long id, MarketPlaceConfigCreateRequest request) throws ExceptionResponse {
        return manager.updateMarketPlaceConfig(id, request);
    }

    public Object deleteMarketPlaceConfig(long id) throws ExceptionResponse {
        return manager.deleteMarketPlaceConfig(id);
    }
}
