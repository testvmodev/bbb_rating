package com.bbb.core.model.response.ebay.additems;

import com.bbb.core.model.response.ebay.Error;
import com.bbb.core.model.response.ebay.Fees;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.joda.time.LocalDateTime;

import java.util.List;

public class AddItemResponseContainer {
    @JacksonXmlProperty(localName = "ItemID")
    private Long itemID;
    @JacksonXmlProperty(localName = "StartTime")
    private LocalDateTime startTime;
    @JacksonXmlProperty(localName = "EndTime")
    private LocalDateTime endTime;
    @JacksonXmlProperty(localName = "Fees")
    private Fees fees;
    @JacksonXmlProperty(localName = "DiscountReason")
    private String discountReason;
    @JacksonXmlProperty(localName = "CategoryID")
    private long categoryID;

    @JacksonXmlProperty(localName = "CorrelationID")
    private String correlationID;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Errors")
    private List<Error> errors;

    public Long getItemID() {
        return itemID;
    }

    public void setItemID(Long itemID) {
        this.itemID = itemID;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Fees getFees() {
        return fees;
    }

    public void setFees(Fees fees) {
        this.fees = fees;
    }

    public String getDiscountReason() {
        return discountReason;
    }

    public void setDiscountReason(String discountReason) {
        this.discountReason = discountReason;
    }

    public long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(long categoryID) {
        this.categoryID = categoryID;
    }

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
