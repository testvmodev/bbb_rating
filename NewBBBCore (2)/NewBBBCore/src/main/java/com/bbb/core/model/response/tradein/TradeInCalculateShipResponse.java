package com.bbb.core.model.response.tradein;

import com.bbb.core.model.database.type.CarrierType;

public class TradeInCalculateShipResponse {
    private float totalCharge;
    private String currency;
    private CarrierType carrierType;

    public float getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(float totalCharge) {
        this.totalCharge = totalCharge;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CarrierType getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(CarrierType carrierType) {
        this.carrierType = carrierType;
    }
}
