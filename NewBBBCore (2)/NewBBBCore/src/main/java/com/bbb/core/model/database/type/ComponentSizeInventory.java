package com.bbb.core.model.database.type;

public enum ComponentSizeInventory {
    XS("XS"),
    S("S"),
    M("M"),
    L("L"),
    XL("XL"),
    XXL("XXL");
    private final String value;

    ComponentSizeInventory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ComponentSizeInventory findByValue(String value) {
        if (value == null) {
            return null;
        }
        switch (value) {
            case "XS":
                return XS;
            case "S":
                return S;
            case "L":
                return L;
            case "XL":
                return XL;
            case "XXL":
                return XXL;
            default:
                return null;
        }
    }
}
