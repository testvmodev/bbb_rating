package com.bbb.core.repository;

import com.bbb.core.model.BrandModels;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BrandModelsRepository extends PagingAndSortingRepository<BrandModels, Long> {
    @Query(nativeQuery = true, value = "SELECT bicycle_brand.id, bicycle_brand.name, bicycle_model.id, bicycle_model.name FROM bicycle_model JOIN bicycle_brand on  bicycle_model.brand_id = bicycle_brand.id")
    List<BrandModels> finAllTem();
}
