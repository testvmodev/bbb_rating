package com.bbb.core.common.aws.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.bbb.core.common.aws.AwsResourceConfig;
import com.bbb.core.common.utils.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class S3Manager {
    private static final Log LOG = LogFactory.getLog(S3Manager.class);
//    private Map<String, ObjectUpload> listUpload = new HashMap<>();

    @Autowired
    private BucketManager bucketManager;
    @Autowired
    private AwsResourceConfig awsResourceConfig;

    public void upload(MultipartFile multipartFile, String fileNameUpload) {
        upload(multipartFile, awsResourceConfig.getDefaultS3Bucket(), fileNameUpload);
    }

    public void upload(MultipartFile multipartFile, String bucket, String fileNameUpload) {
        try {
            AmazonS3 amazonS3Client = bucketManager.getAmazonS3ClientByBucketDefault();
            upload(amazonS3Client, multipartFile.getInputStream(), fileNameUpload, bucket, multipartFile.getSize(), multipartFile.getContentType());
//            listUpload.remove(fileNameUpload);
        } catch (IOException e) {
//            listUpload.putIfAbsent(fileNameUpload, new ObjectUpload(multipartFile, fileNameUpload, bucket));
        }
    }

    public void upload(InputStream inputStream, long size, String contentType,
                       String fileNameUpload
    ) {
        upload(inputStream, size, contentType, fileNameUpload, awsResourceConfig.getDefaultS3Bucket());
    }

    private void upload(InputStream inputStream, long size, String contentType,
                       String fileNameUpload, String bucket
    ) {
        AmazonS3 amazonS3Client = bucketManager.getAmazonS3ClientByBucketDefault();
        upload(amazonS3Client,inputStream, fileNameUpload, bucket, size, contentType);
    }

    private void upload(AmazonS3 amazonS3Client, InputStream inputStream, String fileNameUpload, String bucket, long size, String contentType) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(size);
        metadata.setContentType(contentType);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileNameUpload, inputStream, metadata);
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        amazonS3Client.putObject(putObjectRequest);
        IOUtils.closeQuietly(inputStream, LOG);
    }

    public void upload(byte[] imageData, MediaType mediaType, String folder, String key) {
        AmazonS3 amazonS3Client = bucketManager.getAmazonS3ClientByBucketDefault();
        String fileName = folder + "/" + key;

        upload(amazonS3Client, imageData, mediaType, fileName);
    }

    private void upload(AmazonS3 amazonS3Client, byte[] imageData, MediaType mediaType, String fileNameUpload) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(mediaType.toString());
        metadata.setContentLength(imageData.length);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(imageData);

        PutObjectRequest putObjectRequest = new PutObjectRequest(awsResourceConfig.getDefaultS3Bucket(), fileNameUpload, inputStream, metadata);
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

        amazonS3Client.putObject(putObjectRequest);
        IOUtils.closeQuietly(inputStream, LOG);
    }

    public ObjectListing getListObject(String prefix) {
        return bucketManager.getAmazonS3ClientByBucketDefault().listObjects(
                new ListObjectsRequest().
                        withBucketName(awsResourceConfig.getDefaultS3Bucket()).
                        withPrefix(prefix)
        );
    }

    public void deleteObject(List<String> keys) {
        List<DeleteObjectsRequest.KeyVersion> keyVersions =
                keys.stream().map(DeleteObjectsRequest.KeyVersion::new).collect(Collectors.toList());
        DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(awsResourceConfig.getDefaultS3Bucket())
                .withKeys(keyVersions)
                .withQuiet(false);
        bucketManager.getAmazonS3ClientByBucketDefault().deleteObjects(deleteObjectsRequest);
    }

    public void deleteObject(String key) {
        bucketManager.getAmazonS3ClientByBucketDefault().deleteObject(awsResourceConfig.getDefaultS3Bucket(), key);
    }

    @Async
    public void deleteObjectAsync(String folder, List<String> keys) {
        List<DeleteObjectsRequest.KeyVersion> keyVersions =
                keys.stream().map(key -> new DeleteObjectsRequest.KeyVersion(folder + "/" + key)).collect(Collectors.toList());
        DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(awsResourceConfig.getDefaultS3Bucket())
                .withKeys(keyVersions)
                .withQuiet(false);
        bucketManager.getAmazonS3ClientByBucketDefault().deleteObjects(deleteObjectsRequest);
    }

    public void deleteObject(String folder, String key) {
        bucketManager.getAmazonS3ClientByBucketDefault().deleteObject(awsResourceConfig.getDefaultS3Bucket(), folder + "/" + key);
    }

    @Async
    public void renameObjectsAsync(String folder, List<Pair<String, String>> keys) {
        if (keys == null || keys.size() < 1) return;
        //copy then delete old one
        for (Pair<String, String> pair : keys) {
            CopyObjectRequest request = new CopyObjectRequest(
                    awsResourceConfig.getDefaultS3Bucket(), folder + "/" + pair.getFirst(),
                    awsResourceConfig.getDefaultS3Bucket(), folder + "/" + pair.getSecond()
            );
            request.setNewObjectMetadata(bucketManager.getAmazonS3ClientByBucketDefault().getObjectMetadata(
                    awsResourceConfig.getDefaultS3Bucket(), folder + "/" + pair.getFirst()
            ));
            request.setAccessControlList(bucketManager.getAmazonS3ClientByBucketDefault().getObjectAcl(
                    awsResourceConfig.getDefaultS3Bucket(), folder + "/" + pair.getFirst()
            ));
            bucketManager.getAmazonS3ClientByBucketDefault().copyObject(request);

        }
        deleteObjectAsync(folder, keys.stream().map(p -> p.getFirst()).collect(Collectors.toList()));
    }

    public S3Object download(String bucket, String key) {
        AmazonS3 s3Client = bucketManager.getAmazonS3ClientByBucketDefault();
        return s3Client.getObject(bucket, key);
    }

    public S3Object download(String key) {
        return download(awsResourceConfig.getDefaultS3Bucket(), key);
    }
}
