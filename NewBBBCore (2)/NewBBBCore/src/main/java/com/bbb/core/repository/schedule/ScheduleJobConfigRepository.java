package com.bbb.core.repository.schedule;

import com.bbb.core.model.database.ScheduleJobConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScheduleJobConfigRepository extends CrudRepository<ScheduleJobConfig, Long> {
    @Query(nativeQuery = true, value = "SELECT TOP 1 * " +
            "FROM schedule_job_config " +
            "WHERE job_type = :type " +
            "AND is_delete = 0"
    )
    ScheduleJobConfig findOne(@Param("type") String type);

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM schedule_job_config " +
            "WHERE is_delete = 0"
    )
    List<ScheduleJobConfig> findAll();

    @Query(nativeQuery = true, value = "SELECT * " +
            "FROM schedule_job_config " +
            "WHERE is_delete = 0 " +
            "AND (:isEnable IS NULL OR is_enable = :isEnable)"
    )
    List<ScheduleJobConfig> findAll(
            @Param("isEnable") Boolean isEnable
    );
}
