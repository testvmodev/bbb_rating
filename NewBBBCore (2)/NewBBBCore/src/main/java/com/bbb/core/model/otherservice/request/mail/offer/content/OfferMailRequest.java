package com.bbb.core.model.otherservice.request.mail.offer.content;

public class OfferMailRequest {
    private String amount;

    public OfferMailRequest(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
