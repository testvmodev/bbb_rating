/*
 Navicat Premium Data Transfer

 Source Server         : bbb
 Source Server Type    : SQL Server
 Source Server Version : 13004451
 Source Host           : new-bbb.cno8nspm9xh2.ap-southeast-1.rds.amazonaws.com:1433
 Source Catalog        : bbb_duc
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 13004451
 File Encoding         : 65001

 Date: 26/03/2018 17:53:15
*/


-- ----------------------------
-- Table structure for bicycle
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle]
GO

CREATE TABLE [dbo].[bicycle] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [brand_id] int  NOT NULL,
  [model_id] int  NOT NULL,
  [type_id] int  NOT NULL,
  [year_id] int  NOT NULL,
  [retail_price] decimal(10,2)  NULL,
  [description] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [last_update] datetime  NULL,
  [active] bit  NULL,
  [image_default] varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [is_delete] bit DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_brake_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_brake_type]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_brake_type]
GO

CREATE TABLE [dbo].[bicycle_brake_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_brake_type] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_brand
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_brand]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_brand]
GO

CREATE TABLE [dbo].[bicycle_brand] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [brand_logo] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [value_modifier] decimal(10,2)  NULL,
  [last_update] datetime  NULL,
  [is_approved] bit DEFAULT ((1)) NOT NULL,
  [is_delete] bit DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_brand] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_component
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_component]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_component]
GO

CREATE TABLE [dbo].[bicycle_component] (
  [bicycle_id] int  NOT NULL,
  [component_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_component] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_component_upgrade
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_component_upgrade]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_component_upgrade]
GO

CREATE TABLE [dbo].[bicycle_component_upgrade] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [type_id] int DEFAULT ((0)) NOT NULL,
  [bicycle_id] int  NOT NULL,
  [description] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [cost] money  NULL,
  [ageIn_month_range] int  NULL,
  [premium] bit  NULL,
  [upgrade_date] datetime  NULL
)
GO

ALTER TABLE [dbo].[bicycle_component_upgrade] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_condition
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_condition]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_condition]
GO

CREATE TABLE [dbo].[bicycle_condition] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [value_mo] decimal(10,2)  NOT NULL,
  [adjustment_range] decimal(10,2)  NULL
)
GO

ALTER TABLE [dbo].[bicycle_condition] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_image
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_image]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_image]
GO

CREATE TABLE [dbo].[bicycle_image] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [bicycle_id] int  NOT NULL,
  [uri] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_image] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_model
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_model]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_model]
GO

CREATE TABLE [dbo].[bicycle_model] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [last_update] datetime  NOT NULL,
  [brand_id] int  NOT NULL,
  [is_approved] bit DEFAULT ((1)) NOT NULL,
  [is_delete] bit DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_model] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_review
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_review]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_review]
GO

CREATE TABLE [dbo].[bicycle_review] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [review] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [review_time] datetime  NULL,
  [review_url] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [source_id] int  NOT NULL,
  [column] int  NULL,
  [bicycle_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_review] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_size
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_size]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_size]
GO

CREATE TABLE [dbo].[bicycle_size] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [sort_order] int  NOT NULL,
  [is_standard] bit DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_size] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_type]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_type]
GO

CREATE TABLE [dbo].[bicycle_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [type_image] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [sort_order] int DEFAULT ((0)) NOT NULL,
  [defaule_image] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [value_modifier] int DEFAULT ((0)) NOT NULL,
  [no_image] bit DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_type] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for bicycle_year
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[bicycle_year]') AND type IN ('U'))
  DROP TABLE [dbo].[bicycle_year]
GO

CREATE TABLE [dbo].[bicycle_year] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [is_approved] bit DEFAULT ((1)) NOT NULL
)
GO

ALTER TABLE [dbo].[bicycle_year] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for component
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[component]') AND type IN ('U'))
  DROP TABLE [dbo].[component]
GO

CREATE TABLE [dbo].[component] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [type_id] int  NOT NULL,
  [value_id] int DEFAULT ((0)) NOT NULL
)
GO

ALTER TABLE [dbo].[component] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for component_category
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[component_category]') AND type IN ('U'))
  DROP TABLE [dbo].[component_category]
GO

CREATE TABLE [dbo].[component_category] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[component_category] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for component_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[component_type]') AND type IN ('U'))
  DROP TABLE [dbo].[component_type]
GO

CREATE TABLE [dbo].[component_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [sort_order] int  NOT NULL,
  [category_id] int DEFAULT ((0)) NOT NULL,
  [allow_valuation] bit  NULL,
  [show_premium] bit  NULL,
  [standard_value] decimal(10,2)  NULL,
  [premium_value] decimal(10,2)  NULL,
  [sales_force_id] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[component_type] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for component_value
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[component_value]') AND type IN ('U'))
  DROP TABLE [dbo].[component_value]
GO

CREATE TABLE [dbo].[component_value] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [type_id] int DEFAULT ((0)) NOT NULL,
  [value_mo] decimal(10,2)  NOT NULL
)
GO

ALTER TABLE [dbo].[component_value] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for inventory
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory]') AND type IN ('U'))
  DROP TABLE [dbo].[inventory]
GO

CREATE TABLE [dbo].[inventory] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [bicycle_id] int  NOT NULL,
  [bicycle_model_name] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [bicycle_brand_name] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [bicycle_year_name] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [bicycle_type_name] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [description] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [image_default_id] int  NULL,
  [image_default] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [status] tinyint DEFAULT ((0)) NOT NULL,
  [msrp_price] float(53)  NULL,
  [trade_in_price] float(53)  NULL,
  [override_trade_in_price] float(53)  NULL,
  [initial_list_price] float(53)  NULL,
  [current_listed_price] float(53)  NULL,
  [additional_components_price] float(53)  NULL,
  [cogs_price] float(53)  NULL,
  [created_time] datetime DEFAULT (getdate()) NULL,
  [last_update] datetime DEFAULT NULL NULL,
  [is_delete] bit DEFAULT ((0)) NOT NULL,
  [partner_id] int  NULL,
  [stage] tinyint DEFAULT ((0)) NOT NULL,
  [serial_number] nvarchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [name] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [flat_price_change] int  NOT NULL,
  [discounted_price] bit  NOT NULL,
  [listing_duration] int  NULL,
  [type_id] int  NULL
)
GO

ALTER TABLE [dbo].[inventory] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for inventory_component
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_component]') AND type IN ('U'))
  DROP TABLE [dbo].[inventory_component]
GO

CREATE TABLE [dbo].[inventory_component] (
  [inventory_id] int  NOT NULL,
  [component_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[inventory_component] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for inventory_image
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_image]') AND type IN ('U'))
  DROP TABLE [dbo].[inventory_image]
GO

CREATE TABLE [dbo].[inventory_image] (
  [id] int  NOT NULL,
  [inventory_id] int  NOT NULL,
  [image] nvarchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[inventory_image] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for inventory_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_type]') AND type IN ('U'))
  DROP TABLE [dbo].[inventory_type]
GO

CREATE TABLE [dbo].[inventory_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[inventory_type] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for market_listing
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[market_listing]') AND type IN ('U'))
  DROP TABLE [dbo].[market_listing]
GO

CREATE TABLE [dbo].[market_listing] (
  [id] int  NOT NULL,
  [inventory_id] int  NOT NULL,
  [status] tinyint  NULL,
  [created_time] datetime  NULL,
  [last_update] datetime  NULL,
  [type] tinyint  NOT NULL,
  [time_listed] datetime  NULL,
  [time_delisted] datetime  NULL,
  [time_sold] datetime  NULL
)
GO

ALTER TABLE [dbo].[market_listing] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for rating
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[rating]') AND type IN ('U'))
  DROP TABLE [dbo].[rating]
GO

CREATE TABLE [dbo].[rating] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [rate] int  NOT NULL,
  [rate_time] datetime  NULL,
  [bicycle_id] int  NOT NULL,
  [user_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[rating] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for review_source
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[review_source]') AND type IN ('U'))
  DROP TABLE [dbo].[review_source]
GO

CREATE TABLE [dbo].[review_source] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [image] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[review_source] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for role
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[role]') AND type IN ('U'))
  DROP TABLE [dbo].[role]
GO

CREATE TABLE [dbo].[role] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[role] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_message
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_message]') AND type IN ('U'))
  DROP TABLE [dbo].[user_message]
GO

CREATE TABLE [dbo].[user_message] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [from_user_id] int  NOT NULL,
  [to_user_Id] int  NOT NULL,
  [subject] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [body] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [created_time] datetime DEFAULT (getdate()) NULL,
  [is_read] bit  NULL,
  [message_type] int  NULL
)
GO

ALTER TABLE [dbo].[user_message] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_password_reset
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_password_reset]') AND type IN ('U'))
  DROP TABLE [dbo].[user_password_reset]
GO

CREATE TABLE [dbo].[user_password_reset] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [user_id] int  NOT NULL,
  [secret_code] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [expire_time] datetime  NOT NULL,
  [email] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[user_password_reset] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_profile
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_profile]') AND type IN ('U'))
  DROP TABLE [dbo].[user_profile]
GO

CREATE TABLE [dbo].[user_profile] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [first_name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [last_name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [email] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [street_address] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [city] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [state] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [postal_code] varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [provider_id] int  NOT NULL,
  [created_time] datetime DEFAULT (getdate()) NOT NULL,
  [paypal_id] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [paypal_email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [paypal_status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [paypal_url] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [avatar_url] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [status] int  NOT NULL,
  [password] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[user_profile] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_provider
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_provider]') AND type IN ('U'))
  DROP TABLE [dbo].[user_provider]
GO

CREATE TABLE [dbo].[user_provider] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[user_provider] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_role
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_role]') AND type IN ('U'))
  DROP TABLE [dbo].[user_role]
GO

CREATE TABLE [dbo].[user_role] (
  [role_id] int  NOT NULL,
  [user_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[user_role] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for user_session
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user_session]') AND type IN ('U'))
  DROP TABLE [dbo].[user_session]
GO

CREATE TABLE [dbo].[user_session] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [device_type] int DEFAULT ((0)) NOT NULL,
  [last_login] datetime  NULL,
  [token] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [device_token] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [user_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[user_session] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Primary Key structure for table bicycle
-- ----------------------------
ALTER TABLE [dbo].[bicycle] ADD CONSTRAINT [PK__bicycle__3213E83F909ADCD8] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_brake_type
-- ----------------------------
ALTER TABLE [dbo].[bicycle_brake_type] ADD CONSTRAINT [PK__bicycle___3213E83F81FA1F36] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table bicycle_brand
-- ----------------------------
CREATE TRIGGER [dbo].[trigger_update_time]
  ON [dbo].[bicycle_brand]
  WITH EXECUTE AS CALLER
  FOR INSERT, UPDATE
AS
  IF @@ROWCOUNT=0 RETURN

DECLARE @countInsert int
DECLARE @coundUpdate int
SELECT @countInsert = COUNT(*) from INSERTED
if @countInsert > 0
  BEGIN
    DECLARE @id int
    SELECT @id =id from INSERTED
    UPDATE dbo.bicycle_brand SET last_update = GETDATE() WHERE dbo.bicycle_brand.id = @id
  END
else
  BEGIN
    DECLARE @idUpdate int
    SELECT @idUpdate =id from UPDATED
    UPDATE dbo.bicycle_brand SET last_update = GETDATE() WHERE dbo.bicycle_brand.id = @idUpdate
  END
GO


-- ----------------------------
-- Primary Key structure for table bicycle_brand
-- ----------------------------
ALTER TABLE [dbo].[bicycle_brand] ADD CONSTRAINT [PK__bicycle___3213E83F7B99AC54] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_component
-- ----------------------------
ALTER TABLE [dbo].[bicycle_component] ADD CONSTRAINT [PK__bicycle___328A472356CD914C] PRIMARY KEY CLUSTERED ([bicycle_id], [component_id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_component_upgrade
-- ----------------------------
ALTER TABLE [dbo].[bicycle_component_upgrade] ADD CONSTRAINT [PK__bicycle___3213E83F24770130] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_condition
-- ----------------------------
ALTER TABLE [dbo].[bicycle_condition] ADD CONSTRAINT [PK__bicycle___3213E83FE05A5787] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_image
-- ----------------------------
ALTER TABLE [dbo].[bicycle_image] ADD CONSTRAINT [PK__bicycle___3213E83F4387B1DB] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table bicycle_model
-- ----------------------------
CREATE TRIGGER [dbo].[trigger_model_update_time]
  ON [dbo].[bicycle_model]
  WITH EXECUTE AS CALLER
  FOR INSERT, UPDATE
AS
  IF @@ROWCOUNT=0 RETURN

DECLARE @countInsert int
DECLARE @coundUpdate int
SELECT @countInsert = COUNT(*) from INSERTED
if @countInsert > 0
  BEGIN
    DECLARE @id int
    SELECT @id =id from INSERTED
    UPDATE dbo.bicycle_model SET last_update = GETDATE() WHERE dbo.bicycle_model.id = @id
  END
else
  BEGIN
    DECLARE @idUpdate int
    SELECT @idUpdate =id from UPDATED
    UPDATE dbo.bicycle_model SET last_update = GETDATE() WHERE dbo.bicycle_model.id = @idUpdate
  END
GO


-- ----------------------------
-- Primary Key structure for table bicycle_model
-- ----------------------------
ALTER TABLE [dbo].[bicycle_model] ADD CONSTRAINT [PK__bicycle___3213E83F74F489CF] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_size
-- ----------------------------
ALTER TABLE [dbo].[bicycle_size] ADD CONSTRAINT [PK__bicycle___3213E83FEEBB23C4] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_type
-- ----------------------------
ALTER TABLE [dbo].[bicycle_type] ADD CONSTRAINT [PK__bicycle___3213E83F708B8A1D] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table bicycle_year
-- ----------------------------
ALTER TABLE [dbo].[bicycle_year] ADD CONSTRAINT [PK__bicycle___3213E83F107B2D37] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table component
-- ----------------------------
ALTER TABLE [dbo].[component] ADD CONSTRAINT [PK__componen__3213E83F453F574B] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table component_category
-- ----------------------------
ALTER TABLE [dbo].[component_category] ADD CONSTRAINT [PK__componen__3213E83F82A89AED] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table component_type
-- ----------------------------
ALTER TABLE [dbo].[component_type] ADD CONSTRAINT [PK__componen__3213E83FF7983DD6] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table component_value
-- ----------------------------
ALTER TABLE [dbo].[component_value] ADD CONSTRAINT [PK__componen__3213E83F01C9B778] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table inventory
-- ----------------------------
CREATE TRIGGER [dbo].[trigger_update_time_inventory]
  ON [dbo].[inventory]
  WITH EXECUTE AS CALLER
  FOR INSERT, UPDATE
AS
  IF @@ROWCOUNT=0 RETURN

DECLARE @countInsert int
DECLARE @coundUpdate int
SELECT @countInsert = COUNT(*) from INSERTED
if @countInsert > 0
  BEGIN
    DECLARE @id int
    SELECT @id =id from INSERTED
    UPDATE dbo.inventory SET last_update = GETDATE() WHERE dbo.inventory.id = @id
  END
else
  BEGIN
    DECLARE @idUpdate int
    SELECT @idUpdate =id from UPDATED
    UPDATE dbo.inventory SET last_update = GETDATE() WHERE dbo.inventory.id = @idUpdate
  END
GO


-- ----------------------------
-- Primary Key structure for table inventory
-- ----------------------------
ALTER TABLE [dbo].[inventory] ADD CONSTRAINT [PK__inventor__3213E83F061118F1] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table inventory_component
-- ----------------------------
ALTER TABLE [dbo].[inventory_component] ADD CONSTRAINT [PK__inventor__3F71D1EC91F90072] PRIMARY KEY CLUSTERED ([inventory_id], [component_id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table inventory_image
-- ----------------------------
ALTER TABLE [dbo].[inventory_image] ADD CONSTRAINT [PK__inventor__3213E83F7E97BDA4] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table inventory_type
-- ----------------------------
ALTER TABLE [dbo].[inventory_type] ADD CONSTRAINT [PK__inventor__3213E83FA4DE1809] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Triggers structure for table market_listing
-- ----------------------------
CREATE TRIGGER [dbo].[trigger_update_time_maket_listing]
  ON [dbo].[market_listing]
  WITH EXECUTE AS CALLER
  FOR INSERT, UPDATE
AS
  IF @@ROWCOUNT=0 RETURN

DECLARE @countInsert int
DECLARE @coundUpdate int
SELECT @countInsert = COUNT(*) from INSERTED
if @countInsert > 0
  BEGIN
    DECLARE @id int
    SELECT @id =id from INSERTED
    UPDATE dbo.market_listing SET last_update = GETDATE() WHERE dbo.market_listing.id = @id
  END
else
  BEGIN
    DECLARE @idUpdate int
    SELECT @idUpdate =id from UPDATED
    UPDATE dbo.market_listing SET last_update = GETDATE() WHERE dbo.market_listing.id = @idUpdate
  END
GO


-- ----------------------------
-- Primary Key structure for table market_listing
-- ----------------------------
ALTER TABLE [dbo].[market_listing] ADD CONSTRAINT [PK__market_l__3213E83F725E161E] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table review_source
-- ----------------------------
ALTER TABLE [dbo].[review_source] ADD CONSTRAINT [PK__review_s__3213E83F70B9489D] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE [dbo].[role] ADD CONSTRAINT [PK__role__3213E83F7FBC90A1] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_message
-- ----------------------------
ALTER TABLE [dbo].[user_message] ADD CONSTRAINT [PK__user_mes__3213E83FB647E55E] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_password_reset
-- ----------------------------
ALTER TABLE [dbo].[user_password_reset] ADD CONSTRAINT [PK__user_pas__3213E83F6C95AFB8] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_profile
-- ----------------------------
ALTER TABLE [dbo].[user_profile] ADD CONSTRAINT [PK__user_pro__3213E83F4FCB3579] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Indexes structure for table user_provider
-- ----------------------------
CREATE UNIQUE NONCLUSTERED INDEX [user_provider_id]
  ON [dbo].[user_provider] (
    [id] ASC
  )
GO


-- ----------------------------
-- Primary Key structure for table user_provider
-- ----------------------------
ALTER TABLE [dbo].[user_provider] ADD CONSTRAINT [PK__user_pro__3213E83F1F8A244C] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_role
-- ----------------------------
ALTER TABLE [dbo].[user_role] ADD CONSTRAINT [PK__user_rol__9D9286BC7BA9D902] PRIMARY KEY CLUSTERED ([role_id], [user_id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user_session
-- ----------------------------
ALTER TABLE [dbo].[user_session] ADD CONSTRAINT [PK__user_ses__3213E83F81E8CF73] PRIMARY KEY CLUSTERED ([id])
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
  ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle
-- ----------------------------
ALTER TABLE [dbo].[bicycle] ADD CONSTRAINT [fk_bicycle_brand] FOREIGN KEY ([brand_id]) REFERENCES [dbo].[bicycle_brand] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle] ADD CONSTRAINT [fk_bicycle_type] FOREIGN KEY ([type_id]) REFERENCES [dbo].[bicycle_type] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle] ADD CONSTRAINT [fk_bicycle_model] FOREIGN KEY ([model_id]) REFERENCES [dbo].[bicycle_model] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle] ADD CONSTRAINT [fk_bicycle_year] FOREIGN KEY ([year_id]) REFERENCES [dbo].[bicycle_year] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle_component
-- ----------------------------
ALTER TABLE [dbo].[bicycle_component] ADD CONSTRAINT [fk_bicycle_com53753] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle_component] ADD CONSTRAINT [fk_bicycle_com956667] FOREIGN KEY ([component_id]) REFERENCES [dbo].[component] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle_component_upgrade
-- ----------------------------
ALTER TABLE [dbo].[bicycle_component_upgrade] ADD CONSTRAINT [fk_comp_update_bicycle] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle_component_upgrade] ADD CONSTRAINT [fk_comp_upgrade_type] FOREIGN KEY ([type_id]) REFERENCES [dbo].[bicycle_type] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle_image
-- ----------------------------
ALTER TABLE [dbo].[bicycle_image] ADD CONSTRAINT [fk_image_bicycle] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle_model
-- ----------------------------
ALTER TABLE [dbo].[bicycle_model] ADD CONSTRAINT [fk_model_brand] FOREIGN KEY ([brand_id]) REFERENCES [dbo].[bicycle_brand] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table bicycle_review
-- ----------------------------
ALTER TABLE [dbo].[bicycle_review] ADD CONSTRAINT [fk_review_bicycle] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[bicycle_review] ADD CONSTRAINT [fk_review_source] FOREIGN KEY ([source_id]) REFERENCES [dbo].[review_source] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table component
-- ----------------------------
ALTER TABLE [dbo].[component] ADD CONSTRAINT [fk_comp_type] FOREIGN KEY ([type_id]) REFERENCES [dbo].[component_type] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[component] ADD CONSTRAINT [fk_comp_value] FOREIGN KEY ([value_id]) REFERENCES [dbo].[component_value] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table component_type
-- ----------------------------
ALTER TABLE [dbo].[component_type] ADD CONSTRAINT [fk_component_typeCategory] FOREIGN KEY ([category_id]) REFERENCES [dbo].[component_category] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table component_value
-- ----------------------------
ALTER TABLE [dbo].[component_value] ADD CONSTRAINT [fk_comp_value_type] FOREIGN KEY ([type_id]) REFERENCES [dbo].[component_type] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table inventory
-- ----------------------------
ALTER TABLE [dbo].[inventory] ADD CONSTRAINT [inventory_key_1] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[inventory] ADD CONSTRAINT [inventory_key_2] FOREIGN KEY ([type_id]) REFERENCES [dbo].[inventory_type] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table inventory_image
-- ----------------------------
ALTER TABLE [dbo].[inventory_image] ADD CONSTRAINT [inventory_image_1] FOREIGN KEY ([inventory_id]) REFERENCES [dbo].[inventory] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table market_listing
-- ----------------------------
ALTER TABLE [dbo].[market_listing] ADD CONSTRAINT [market_listing_1] FOREIGN KEY ([inventory_id]) REFERENCES [dbo].[inventory] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table rating
-- ----------------------------
ALTER TABLE [dbo].[rating] ADD CONSTRAINT [fk_rating_ricycle] FOREIGN KEY ([bicycle_id]) REFERENCES [dbo].[bicycle] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[rating] ADD CONSTRAINT [fk_rating_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table user_message
-- ----------------------------
ALTER TABLE [dbo].[user_message] ADD CONSTRAINT [fk_message_user] FOREIGN KEY ([from_user_id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[user_message] ADD CONSTRAINT [fk_message_user2] FOREIGN KEY ([to_user_Id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table user_password_reset
-- ----------------------------
ALTER TABLE [dbo].[user_password_reset] ADD CONSTRAINT [fk_password_reset_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table user_profile
-- ----------------------------
ALTER TABLE [dbo].[user_profile] ADD CONSTRAINT [fk_user_provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[user_provider] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table user_role
-- ----------------------------
ALTER TABLE [dbo].[user_role] ADD CONSTRAINT [fk_user_role_rol] FOREIGN KEY ([role_id]) REFERENCES [dbo].[role] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[user_role] ADD CONSTRAINT [fk_user_role_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table user_session
-- ----------------------------
ALTER TABLE [dbo].[user_session] ADD CONSTRAINT [fk_session_user] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user_profile] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

