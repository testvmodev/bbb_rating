FROM openjdk:8-jdk-alpine
LABEL author="manh.nguyen@vmodev.com"

ENV SPRING_PROFILE="-Dspring.profiles.active=production"

RUN mkdir -p /app
WORKDIR /app

ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib ./lib
COPY ${DEPENDENCY}/META-INF ./META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes ./

EXPOSE 1900

# CMD ["java","$JAVA_OPTS","-cp",".:lib/*","com.bbb.core.Main"]
ENTRYPOINT java $SPRING_PROFILE -cp .:lib/* com.bbb.core.Main
