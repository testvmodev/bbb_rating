#!/usr/bin/env bash
echo '~~~~~~ Starting build development bbb-core-dev ~~~~~~~~~'

ENV=dev
HOST=507406836182.dkr.ecr.us-east-1.amazonaws.com
IMAGE=bbb-core

cd ../ &&mvn clean package && docker build --cache-from=$HOST/$IMAGE:$ENV -t $HOST/$IMAGE:$ENV -f .docker/$ENV.dockerfile .

echo '~~~~~~ Ending build dockerfile ~~~~~~~~~~'